<?
class view_assinante extends TView {

	function show() {
		$view = $this;
		require_once(DIR_TEMPLATES."assinante/frm_assinante.php");
	
	}

	public function getId() { 
		if($this->getModel() != null) 
			return $this->getModel()->getId();

		return null;
	}


	public function getEmail() { 
		if($this->getModel() != null) 
			return $this->getModel()->getass_email();

		return null;
	}

	public function getSenha() { 
		if($this->getModel() != null) 
			return $this->getModel()->getass_senha();

		return null;
	}

	public function getConfirmado() { 
		if($this->getModel() != null) 
			return $this->getModel()->getass_confirmado();

		return null;
	}

	public function getExcluido() { 
		if($this->getModel() != null) 
			return $this->getModel()->getass_excluido();

		return null;
	}

	/**
	 * Monta HTML com a listagem de áreas separadas por clientes e áreas de colaboração.
	 * Caso conexão seja por meio de API cria-se campos tipo hidden.
	 * Caso seja passado o parâmetro usuario, a função retorna os clientes do usuário informado.
	 * Caso não seja passado o parâmetro usuario, a função retorna os clientes do usuário atualmente logado no sistema.
	 * André Alves - 27/01/2012
	 * 
	 * @param int $usuario Id do usuário
	 * @return string Lista com os clientes
	 */
	function listarAreas($usuario = '') {

		$lista_clientes = $this->getController()->obterClientesDoUsuario();
		$buffer = "";

		if(checkapi()) {

			$lista_areas = $this->getController()->obterAreasDoUsuario();
			foreach($lista_areas->Rows() as $area) {
				$buffer .="<input type='hidden' name='areas[]' value='".$area->area_id."' />";	
			}

		} else {

			if($lista_clientes != null) {
				foreach($lista_clientes->Rows() as $cliente) {
					$lista_areas = $this->getController()->obterAreasDoUsuario($cliente->CLIE_Codigo);
					
					$buffer .= '<table class="formatted">
								<thead>
									<tr>
										<th width="15">&nbsp;</th>
										<th class="cliente">'.ucwords(strtolower(str_replace('zzz','',$cliente->CLIE_Nome))).'</th>
			 						</tr>
								</thead>
								<tbody>';
								
								if ($lista_areas != null) {
									foreach($lista_areas->Rows() as $area) { 
										$buffer .= '<tr>
														<td><input type="checkbox" name="areas[]" class="area" value="'.$area->area_id.'" /></td>
														<td>'.$area->are_descricao.'</td>
													</tr>';
									}
								} else {
									$buffer .= '	<tr>
														<td></td>
														<td>';
									$buffer .= translate("Nenhum registro encontrado");
									$buffer .="		 </td>
													</tr>";
								}

					$buffer.='  </tbody>
							  </table>';

				}
			} else {
				$buffer .= translate("Nenhum registro encontrado");
			}

		}

		return $buffer.'<br /><br />';
	}

	public function getAreas() {

		$table_area = $this->getController()->obterAreasDoUsuario();
		$buffer = "";
		if ($table_area != null){
			$buffer= "<table>";
			for ($j = 0; $j < $table_area->RowCount(); $j++) {
				if ($j % 2 == 0)
					$buffer .= "<tr>";
				$row_area = $table_area->getRow($j);
								 if(checkapi ())
					$buffer .="<td><input type='hidden' name='areas[]' value='".$row_area->area_id."' ></td>";
								 else
								 $buffer .="<td>&nbsp;&nbsp;&nbsp;&nbsp;<input type='checkbox' name='areas[]' value='".$row_area->area_id."' >".$row_area->are_descricao."<br></td>";
				if ($j % 2 == 1)
					$buffer .= "</tr>";
			}
			$buffer .= "</table>";
		}

		return $buffer;

	}

	public function getClientes() {

		$lista_clientes = $this->getController()->obterClientesDoUsuario();
		$buffer = "";

		if($lista_clientes != null) {
				foreach($lista_clientes->Rows() as $cliente) {

					if($cliente->CLIE_Codigo != 'COLAB') {
						$buffer .= '<option value="'.$cliente->CLIE_Codigo.'">'.$cliente->CLIE_Nome.'</option>';	
					}
				}
			}

		return $buffer;

	}

	public function getOptin() {
		return $this->getController()->getOptin();
	}

}
?>
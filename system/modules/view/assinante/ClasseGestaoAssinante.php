<?php

class GestaoAssinante {

	function MontarTabelaArea($area) {
		unset($_SESSION['xemail']);
		//echo "<form id=formArea name=formArea method=post	>";
?>
		<a target="_BLANK" href="index.php?action=show&secao=gestao_assinante&pagina=0&tipo=listar&file=exportar.txt">
			<span style="float:right; margin: 10px 0px 10px 0px;" id="exportar">Exportar</span>
		</a>

		<br/>
		<table class="formatted">
			<thead>
				<tr>
					<td><div align=left><span> E-mail</span></div></td>
					<td><span></span></td>
					<td><span><?= translate('Situação'); ?></span></td>
					<td></td>
					<td><span></span></td>
				</tr>
			</thead>
<?
		//require_once("view/NovaConexao.php");
		$conexao = new NovaConexao();
		$conexao->Conecta();
		$sql = "
			select
				DISTINCT a.assinante_id, a.ass_email, b.excluido
			from
				assinantes a, assinante_area b
			where
				a.assinante_id = b.assinante_id";
		//Consulta a ser feita no MySQL
		if ($area != 0)
			$sql .= " and b.area_id=" . $area . " ORDER BY a.ass_email";
		//echo $sql;
		#Número de registros paginados por página
		$registros_pagina = 20;
		#Resgatamos a página que estiver sendo acessada pela paginação
		$lista = (int) $_GET["pagina"];

		#Se for a página inicial da consulta, a variável $lista será nula
		if (!$lista) {
			$pc = "1";
		} #Caso contrário, declaramos o valor atual da variável $lista
		else {
			$pc = $lista;
		}
		$inicio = $pc - 1;
		$inicio = $inicio * $registros_pagina;

		#Limitamos a nossa consulta do MySQL para exibir apenas a quantidade máxima configurada mais acima
		$resultado = $conexao->Query($sql . " LIMIT " . $inicio . "," . $registros_pagina . "");
		#Vamos agora consultar a quantidade total de registros
		$todos = $conexao->Query($sql);
		$this->exportacao($todos);
		#Armazenamos a quantidade total de registros
		$tr = mysqli_num_rows($todos);
		#Armazenamos o resultado da quantidade total de registros pela quantidade de registros por página
		$tp = $tr / $registros_pagina;
		#Se não houverem registros a se exibir, é acusado o retorno abaixo
		if (mysqli_num_rows($resultado) < 1) {
			echo "Nenhum registro encontrado";
		} else {
?>
			<tbody>
<?
			#Caso contrário é exibido o resultado da consulta
			#Exibimos o resultado dos registros encontrados na consulta
			while ($linha = mysqli_fetch_array($resultado)) {
				$coluna = $linha["ass_email"];
				$assinante_id = $linha["assinante_id"];
				$ass_excluido = $linha["excluido"];
				// echo "$coluna<br />";
?>
				<tr>
					<td>
						<p><? echo "  " . $coluna; ?></p></td>
					<td></td>
					<td width="30">
<?
				// <input name=excluir[] type=checkbox id=excluir[] value=echo $array->assinante_id;
?>
<?
				if ($ass_excluido == 1) {
					echo translate("inativo");
				} else {
					echo translate("ativo");
				} 
?>
					</td>

					<td width="20">
						<a href="index.php?action=show&secao=gestao_assinante&pagina=0&tipo=alterar&id=<? echo $assinante_id ?>&email=<? echo $coluna ?>&excluido=<? echo $ass_excluido ?>">
							<img src="images/page_edit.png" width="16" height="16" border="0" />
						</a>
					</td>
					<td width="20">
						<a href="index.php?action=show&secao=gestao_assinante&pagina=0&amp;tipo=excluir&amp;id=<? echo $assinante_id ?>&amp;email=<? echo $coluna ?>">
							<img src="images/delete.png" width="16" height="16" border="0" onclick="return Confirma()" />
						</a>
					</td>
				</tr>
<?
			}
		}
?>
			</tbody>
			<tfoot>
				<tr>
					<table width="100%">
						<tr>
							<td class="footerassinante" align="center">
<?
		#E por fim montamos os links da paginação
		$tp = ceil($tp);
		if ($pc > 1) {
			$anterior = $pc - 1;
			//echo "<a href=\"?lista=$anterior\">[Anterior]</a> ";
			echo "<a href='index.php?action=show&secao=gestao_assinante&pagina=" . $anterior . "&tipo=listar'>" . "[Anterior]" . "</a>";
		}
		for ($i = $pc - 5; $i < $pc; $i++) {
			if ($i <= 0) {
			} else {
				echo "<a href='index.php?action=show&secao=gestao_assinante&pagina=" . $i . "&tipo=listar'>";
				if ($i == $pc) {
					echo "<b>[$i]</b>";
				} else {
					echo "[$i]";
				}
				echo "</a> ";
			}
		}
		for ($i = $pc; $i <= $pc + 5; $i++) {
			if ($i == $tp) {
				echo " <a href='index.php?action=show&secao=gestao_assinante&pagina=" . $i . "&tipo=listar'>";
				if ($i == "$pc") {
					echo "<b>[$i]</b>";
				} else {
					echo "[$i]";
				}
				echo "</a> ";
				break;
			} else {
				echo "<a href='index.php?action=show&secao=gestao_assinante&pagina=" . $i . "&tipo=listar'>";
				if ($i == "$pc") {
					echo "<b>[$i]</b>";
				} else {
					echo "[$i]";
				}
				echo "</a> ";
?>
<?
				if ($i == $pc + 5 && $tp > $pc + 5) {
					echo " ... <a href='index.php?action=show&secao=gestao_assinante&pagina=" . $i . "&tipo=listar'>[" . $tp . "]</a>";
				}
			}
		}
		if ($pc < $tp) {
			$proxima = $pc + 1;
			echo "<a href='index.php?action=show&secao=gestao_assinante&pagina=" . $proxima . "&tipo=listar'>" . "[Próxima]" . "</a>";
		}
?>
							</td>
						</tr>
					</table>
				</tr>
			</tfoot>
		</table>
<?
	}

	function MontarTabelaEmail($email) {
		$_SESSION['xemail'] = true;
		//echo"<form id=formArea name=formArea method=post	>";
?>
		<a href="index.php?action=show&secao=gestao_assinante&pagina=0&tipo=listar&file=exportar.txt">
			<span style="float:right; margin: 10px 0px 10px 0px;" id="exportar">Exportar</span>
		</a>
		<br/>
		<table class="formatted">
			<thead>
				<tr>
					<td><div align=left><span> E-mail</span></div></td>
					<td><span></span></td>
					<td><span><?= translate('Situação'); ?></span></td>
					<td><span></span></td>
					<td></td>
				</tr>
			</thead>
<?
		//require_once("view/NovaConexao.php");
		$conexao = new NovaConexao();
		$conexao->Conecta();
		$usuario_id = controller_seguranca::getInstance()->identificarUsuario()->getId();
		$sql = '
			SELECT
				assinante_id,
				ass_email,
				ass_excluido 
			FROM
				assinantes 
			WHERE
				assinante_id IN (
					SELECT 
						assinante_id 
					FROM
						assinante_area 
					WHERE area_id IN (
						SELECT 
							area_id 
						FROM
							`vw_usuarios_areas_clientes` 
						WHERE
							usuario_id = ' . $usuario_id . '
							AND tipo = "C"
					)
				)
				AND ass_email = "' . $email . '"
		';
		//Consulta a ser feita no MySQL
		//$sql = "select assinante_id,ass_email,ass_excluido from ASSINANTE where ass_email like '%" . $email . "%' ";

		#Número de registros paginados por página
		$registros_pagina = 50;

		#Resgatamos a página que estiver sendo acessada pela paginação
		$lista = (int) $_GET["pagina"];

		#Se for a página inicial da consulta, a variável $lista será nula
		if (!$lista) {
			$pc = "1";
		} #Caso contrário, declaramos o valor atual da variável $lista
		else {
			$pc = $lista;
		}
		$inicio = $pc - 1;
		$inicio = $inicio * $registros_pagina;

		#Limitamos a nossa consulta do MySQL para exibir apenas a quantidade máxima configurada mais acima
		$resultado = $conexao->Query($sql . " LIMIT " . $inicio . "," . $registros_pagina . "");

		#Vamos agora consultar a quantidade total de registros
		$todos = $conexao->Query($sql);
		$this->exportacao($todos);
		#Armazenamos a quantidade total de registros
		$tr = mysqli_num_rows($todos);

		#Armazenamos o resultado da quantidade total de registros pela quantidade de registros por página
		$tp = $tr / $registros_pagina;

		#Se não houverem registros a se exibir, é acusado o retorno abaixo
		if (mysqli_num_rows($resultado) < 1) {
			echo "Nenhum registro encontrado";
		} else {
?>
			<tbody>
<?
			#Caso contrário é exibido o resultado da consulta
			#Exibimos o resultado dos registros encontrados na consulta
			while ($linha = mysqli_fetch_array($resultado)) {
				$coluna = $linha["ass_email"];
				$assinante_id = $linha["assinante_id"];
				$ass_excluido = $linha["ass_excluido"];
				// echo "$coluna<br />";
?>
				<tr>
					<td>
						<p><? echo "  " . $coluna; ?></p></td>
					<td></td>
					<td width="30">
<?
				// <input name=excluir[] type=checkbox id=excluir[] value= echo $array->assinante_id;
?>
<?
				if ($ass_excluido == 1) {
					echo "inativo";
				} else {
					echo "ativo";
				}
?>
					</td>
					<td width="20">
						<a href="index.php?action=show&secao=gestao_assinante&pagina=0&tipo=alterar&id=<? echo $assinante_id ?>&email=<? echo $coluna ?>&excluido=<? echo $ass_excluido ?>">
							<img src="images/page_edit.png" width="16" height="16" border="0" />
						</a>
					</td>
					<td width="20">
						<a href="index.php?action=show&secao=gestao_assinante&pagina=0&amp;tipo=excluir&amp;id=<? echo $assinante_id ?>&amp;email=<? echo $coluna ?>">
							<img src="images/delete.png" width="16" height="16" border="0" onclick="return Confirma()" />
						</a>
					</td>
				</tr>
			</tbody>
<?
			}
		}
?>
			<tfoot>
				<table width="100%">
					<tr>
						<td class="footerassinante" align="center">
<?
		#E por fim montamos os links da paginação
		$tp = ceil($tp);
		if ($pc > 1) {
			$anterior = $pc - 1;
			//echo "<a href=\"?lista=$anterior\">[Anterior]</a> ";
			echo "<a href='index.php?action=show&secao=gestao_assinante&pagina=" . $anterior . "&tipo=listar'>[Anterior]</a>";
		}
		for ($i = $pc - 5; $i < $pc; $i++) {
			if ($i <= 0) {
			} else {
				echo "<a href='index.php?action=show&secao=gestao_assinante&pagina=" . $i . "&tipo=listar'>";
				if ($i == $pc) {
					echo "<b>[$i]</b>";
				} else {
					echo "[$i]";
				}
				echo "</a> ";
			}
		}
		for ($i = $pc; $i <= $pc + 5; $i++) {
			if ($i == $tp) {
				echo " <a href='index.php?action=show&secao=gestao_assinante&pagina=" . $i . "&tipo=listar'>";
				if ($i == "$pc") {
					echo "<b>[$i]</b>";
				} else {
					echo "[$i]";
				}
				echo "</a> ";
				break;
			} else {
				echo "<a href='index.php?action=show&secao=gestao_assinante&pagina=" . $i . "&tipo=listar'>";
				if ($i == "$pc") {
					echo "<b>[$i]</b>";
				} else {
					echo "[$i]";
				}
				echo "</a> ";
				if ($i == $pc + 5 && $tp > $pc + 5) {
					echo " ... <a href='index.php?action=show&secao=gestao_assinante&pagina=" . $i . "&tipo=listar'>[" . $tp . "]</a>";
				}
			}
		}
		if ($pc < $tp) {
			$proxima = $pc + 1;
			echo "<a href='index.php?action=show&secao=gestao_assinante&pagina=" . $proxima . "&tipo=listar'>[Próxima]</a>";
		}
?>
						</td>
					</tr>
				</table>
			</tfoot>
		</table>
<?
	}

	/*
	function InativarEmail($excluir) {
		//require_once("view/NovaConexao.php");
		$conexao = new NovaConexao();
		$conexao->Conecta();
		foreach($excluir as $numero) {
			$sql2 =" update ASSINANTE set ass_excluido =1 where assinante_id=".$numero;
			$conexao->Query($sql2);
		}
	}
	*/
	function Inativo($numero) {
		//require_once("view/NovaConexao.php");
		$conexao = new NovaConexao();
		$conexao->Conecta();
		$sql3 = " select * from assinantes where assinante_id=" . $numero . " and ass_excluido=1";
		$resultado = $conexao->Query($sql3);
		$linhas = $conexao->Linhas($resultado);
		if ($linhas > 0) {
			echo "checked=checked";
		} else {
			echo "";
		}
	}

	//lista o email para alteração

	function AjustaEmail() {
		//require_once("view/NovaConexao.php");
		$conexao = new NovaConexao();
		$conexao->Conecta();
		$sql6 = "SELECT assinante_id,ass_email FROM assinantes";
		$resultado = $conexao->Query($sql6);
		if ($resultado != null) {
			$count = mysqli_num_rows($resultado);

			for ($h = 0; $h < $count; $h++) {
				$arr = mysqli_fetch_object($resultado);
				$numero = $arr->ass_email;
				$id = $arr->assinante_id;
				$texto = $numero;
				$letra = strstr($texto, '<');
				$letra1 = strstr($texto, '>');
				$texto = str_replace("<", '', $texto);
				$texto = str_replace(">", '', $texto);
				$sql = "update assinantes SET ass_email='" . $texto . "' where assinante_id=" . $id . "";
				$conexao->Query($sql);
			}
		}
	}
	function exportacao($emails) {
		$buffer = '';
		$arquivo = "exportar.txt"; //nome do arquivo
		$url = DIR_UPLOAD . $arquivo; //aqui coloca la ruta donde se encuentra el archivo
		while ($linha = mysqli_fetch_array($emails)) {
			$buffer .= $linha["ass_email"] . "\r\n";
		}
		$fp = fopen($url, "a");
		file_put_contents($url, "");
		fwrite($fp, $buffer . PHP_EOL);
		fclose($fp);
	}
}
?>
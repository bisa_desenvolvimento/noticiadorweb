<?
class view_inserir_email extends TView
{
	function show() {
		$view = $this;    	
        require_once(DIR_TEMPLATES."assinante/frm_inserir_email.php");
	
	}

	public function inserirEmail($email, $noticia_id){
	
		$table_area = $this->getController()->obterAreas($noticia_id);		
		$areas="";
		if ($table_area != null){							
			for($j = 0; $j < $table_area->RowCount(); $j++) {
				$row_area = $table_area->getRow($j);
				$areas[$j]= $row_area->area_id;
			}
		}
		$this->getController()->inseriMailAssinanteArea($email, $areas);
	
	}

}
?>
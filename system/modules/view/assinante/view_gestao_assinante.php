<?

class View_gestao_assinante extends TView {

	function show() {
		$view = $this;
		require_once(DIR_TEMPLATES . "assinante/frm_gestao_assinante.php");
	}

	public function montarTabelaCadastrados() {
		$table = $this->getController()->obterTodos();

		$controller_arquivo = new controller_arquivo();
		$controller_arquivo->setConexao(TConexao::getInstance());


		$assinantes = "";
		if ($table != null) {

			$buffer = "<form name='fomularioassinante' action='#' target='_self' method='post' onsubmit='return confirm(\"Confirma atualizar registros?\")'>";
			$buffer .= "<table class=\"formatted\">";
			$buffer .= "
						<thead>
							<tr>
									<td colspan=3>" . translate("Assinante/E-mail") . "</td>
							</tr>
							<tr>
									<td>&nbsp;&nbsp;</td>
									<td>" . translate("Email") . "</td>
									<td></td>
							</tr>
						</thead>
					<tbody>";
			$buffer .= "<tr>";
			$class = "class=\"odd\"";
			for ($i = 0; $i < $table->RowCount(); $i++) {
				$row = $table->getRow($i);
				$assinantes .= $row->assinante_id . ",";
				$buffer .= "<tr $class>";
				$buffer .= "<td colspan=3><strong>" . $row->ass_email . "</strong></td>";
				$buffer .= "</tr>";
				$table_area = $this->getController()->obterAreas($row->assinante_id);
				if ($table_area != null) {
					for ($j = 0; $j < $table_area->RowCount(); $j++) {
						if ($j % 2 == 0) {
							$class = "";
						} else {
							$class = "class=\"odd\"";
						}
						$row_area = $table_area->getRow($j);
						$buffer .= "<tr $class>";
						$buffer .= "<td>&nbsp;&nbsp;</td>";
						$buffer .= "<td>" . $row_area->are_descricao . "</td>";
						$buffer .= "<td></td>";
						$buffer .= "</tr>";
					}
				}
				if ($i % 2 == 1) {
					$class = "";
				} else {
					$class = "class=\"odd\"";
				}
			}
			$buffer .= "
							</tbody>
							<tfoot>
									<tr>
											<td colspan=6></td>
									</tr>
							</tfoot>
							</table>
							<br>";
			$buffer .= "</form>";


			return $buffer;
		} else {
			return "Nenhum registro encontrado";
		}
	}

	public function montarTabelaCadastrados1($area) {
		$table = $this->getController()->obterTodos();

		$controller_arquivo = new controller_arquivo();
		$controller_arquivo->setConexao(TConexao::getInstance());

		//criado por Nilson 04/06/2008
		//require_once("NovaConexao.php");
		$conexao = new NovaConexao();
		$conexao->Conecta();
		$query = $conexao->Query("select are_descricao from AREA where area_id=" . $area);
		$linhas = $conexao->Linhas($query);
		$array = mysql_fetch_object($query);
		//fim


		$assinantes = "";
		if ($table != null) {

			$buffer = "<form name='fomularioassinante' action='#' target='_self' method='post' onsubmit='return confirm(\"Confirma atualizar registros?\")'>";
			$buffer .= "<table class=\"formatted\">";
			$buffer .= "<thead>
									  <tr>
												 <td colspan=3>" . translate("�rea/E-mail") . "</td>
									  </tr>
									  <tr>
												 <td>&nbsp;&nbsp;</td>
												 <td>" . translate("�rea") . "</td>
												 <td></td>
									  </tr>
							</thead>
							<tbody>";
			$buffer .= "<tr>";
			$class = "class=\"odd\"";
			for ($i = 0; $i < $linhas/*$table->RowCount()*/; $i++) {


				$row = $table->getRow($i);
				$assinantes .= $row->assinante_id . ",";
				$buffer .= "<tr $class>";
				$buffer .= "<td colspan=3><strong>" . $array->are_descricao/*$row->ass_email}*/ . "</strong></td>";
				$buffer .= "</tr>";
				$table_area = $this->getController()->obterEmails($area/*$row->assinante_id*/);

				if ($table_area != null) {
					for ($j = 0; $j < $table_area->RowCount(); $j++) {
						if ($j % 2 == 0) {
							$class = "";
						} else {
							$class = "class=\"odd\"";
						}
						$row_area = $table_area->getRow($j);
						$buffer .= "<tr $class>";
						$buffer .= "<td>&nbsp;&nbsp;</td>";
						$buffer .= "<td>" . $row_area->ass_email . "</td>";
						$buffer .= "<td></td>";
						$buffer .= "</tr>";
					}
				}
				if ($i % 2 == 1) {
					$class = "";
				} else {
					$class = "class=\"odd\"";
				}
			}
			$buffer .= "</tbody>
							<tfoot>
									  <tr>
												 <td colspan=6></td>
									  </tr>
							</tfoot>
							</table>
							<br>";
			$buffer .= "</form>";


			return $buffer;
		} else {
			return "N�o tem nemguma �rea de interesse associada";
		}
	}


	public function montarComboAreas() {
		$combo = "";
		$table_area = $this->getController()->obterAreasEditorDoUsuario();
		if ($table_area != null) {
			for ($j = 0; $j < $table_area->RowCount(); $j++) {
				$row_area = $table_area->getRow($j);
				$combo .= "<option value='" . $row_area->area_id . "'>" . $row_area->are_descricao . "</option>";
			}

		}
		echo $combo;
	}


	public function montarComboAreasAssinante($assinante) {
		$usuario = controller_seguranca::getInstance()->identificarUsuario();
		$usuario_id = $usuario->getID();
		$table = $this->getController()->obterAreasAssinante($assinante);

		$buffer = "
			<br/>
			<table class=\"formatted\">
				<thead>
					<tr bgcolor=\"#647179\">
						<td>" . translate("C�digo") . "</td>
						<td>" . translate("Descri��o") . "</td>
						<td>" . translate("Observa��o") . "</td>
						<td>" . translate("Bloquear") . "</td>
					</tr>
				</thead>
				<tbody>
		";

		if ($table != null) {

			$class = "class=\"odd\"";
			$i = 0;
			foreach ($table->Rows() as $row) {
				$area = new Area();
				$area->bindAreaAssinante($row);

				if ($area->getAtivacao() == 1) {
					$checked = "checked";
				} else {
					$checked = "";
				}


				$buffer .= "<tr $class>";

				$buffer .= "<td>" . $area->getID() . "</td>";
				$buffer .= "<td>" . $area->getDescricao() . "</td>";
				$buffer .= "<td>" . $area->getObservacao() . "</td>";
				$buffer .= '<td><input type="checkbox" name="bloqueo[]"  value="' . $area->getID() . '"' . $checked . '/></td>';
				$buffer .= '<input type="hidden" name="area[]" value="' . $area->getID() . '"/>';
				$buffer .= "</tr>";
				if ($i % 2 == 1) {
					$class = "";
				} else {
					$class = "class=\"odd\"";
				}
				$i++;
			}


		} else {
			$buffer = produce_icon("cross.png") . "&nbsp;&nbsp;N�o tem nemguma �rea de interesse associada";
		}

		$buffer .= "
				</tbody>
				<tfoot>
				</tfoot>
			</table>
			<br>
		";


		$buffer .= "
					<table width='100%' border=0>
						<tr>
							<td width=20>Email</td>
							<td width=251>
								<input type=text name=emailnovo id=emailnovo size=40 value='" . $_GET['email'] . "'/>
							</td>
							<td width='100%' style=\"float:right\">
								<a onclick=\"return Confirma()\" href=\"index.php?action=show&secao=gestao_assinante&pagina=0&amp;tipo=excluir&amp;id={$assinante}&amp;email={$_GET['email']}\"><img src=\"images/delete.png\" width=\"16\" height=\"16\" border=\"0\" /> Excluir email</a>
							</td>

						</tr>

						<tr>
							<td width=61><input id=\"btnAlterar\" type=submit name=Submit value=Alterar /></td>
						</tr>
					</table>
		";

		return $buffer;


	}


	public function AlterandoEmailComBloqueiosView() {
		$this->getController()->AlterandoEmailComBloqueiosController();
	}

	public function AlterandoEmailView() {
		$this->getController()->AlterandoEmailController();
	}

	public function AlterarEmailView() {

		echo "
			<br>
			<br>
		";
		//echo"<form id=formeditar name=formeditar method=post action= onsubmit=  AlterandoEmail() >";
		echo "
			<table width=307 border=0>
				<tr>
					<td width=20>Email</td>
					<td width=251>
						<input type=text name=emailnovo id=emailnovo size=40 value='" . $_GET['email'] . "'/>
					</td>
		";
		//echo" </div></td>";
		echo "
				</tr>
				<tr>
					<td width=20>Situa��o</td>
					<td width=220 >
						<select name=menu id=menu>
		";
		if ($_GET['excluido'] == 0) {
			echo "
							<option value ='0'>Ativo</option>
							<option value ='1'>Inativo</option>
			";
		} else {
			echo "
							<option value ='1'>Inativo</option>
							<option value ='0'>Ativo</option>
			";
		}
		echo "
						</select>
					</td>
				</tr>
				<tr>
					<td width=61><input type=submit name=Submit value=Alterar /></td>
				</tr>
			</table>
		";

	}

	public function ExcluirEmailView($id) {
		$this->getController()->ExcluirEmailController($id);

	}

}

?>
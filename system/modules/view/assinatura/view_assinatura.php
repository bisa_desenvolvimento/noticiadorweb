<?
class view_assinatura extends TView
{
	function show() {
		$view = $this;    	
        require_once(DIR_TEMPLATES."assinatura/frm_assinatura.php");
	
	}

	public function getId() { 
		if($this->getModel() != null) 
			return $this->getModel()->getId();

		return null;
	}

    function getass_labelTexto() {
		if($this->getModel() != null) 
			return $this->getModel()->getass_labelTexto();

		return null;
    }
    
    function getass_fonteTexto() {
		if($this->getModel() != null) 
			return $this->getModel()->getass_fonteTexto();

		return null;
    }

    function getass_corTexto() {
		if($this->getModel() != null) 
			return $this->getModel()->getass_corTexto();

		return null;
    }
        
    function getass_corBotao() {
		if($this->getModel() != null) 
			return $this->getModel()->getass_corBotao();

		return null;
    }    
    
    function getass_labelBotao() {
		if($this->getModel() != null) 
			return $this->getModel()->getass_labelBotao();

		return null;
    }
    
    function getass_larguraCaixa() {
		if($this->getModel() != null) 
			return $this->getModel()->getass_larguraCaixa();

		return null;
    }

	public function obterCores($cor="") { 
		$combo = "";
		
		$combo .= "<option value='blue'";
		if ($cor=="blue"){$combo .= " selected";}
		$combo .= " class='blue'>Azul Royal</option>";
		
		$combo .= "<option value='navy'";
		if ($cor=="navy"){$combo .= " selected";}
		$combo .= " class='navy'>Azul Escuro</option>";			

		$combo .= "<option value='teal'";
		if ($cor=="teal"){$combo .= " selected";}
		$combo .= " class='teal'>Azul</option>";		
						
		$combo .= "<option value='white'";
		if ($cor=="white"){$combo .= " selected";}
		$combo .= " class='white'>Branco</option>";		
		
		$combo .= "<option value='gray'";
		if ($cor=="gray"){$combo .= " selected";}
		$combo .= " class='gray'>Cinza</option>";
		
		$combo .= "<option value='silver'";
		if ($cor=="silver"){$combo .= " selected";}
		$combo .= " class='silver'>Cinza Claro</option>";
		
		$combo .= "<option value='maroon'";
		if ($cor=="maroon"){$combo .= " selected";}
		$combo .= " class='maroon'>Marrom</option>";				
		
		$combo .= "<option value='olive'";
		if ($cor=="olive"){$combo .= " selected";}
		$combo .= " class='olive'>Oliva</option>";	
		
		$combo .= "<option value='black'";
		if ($cor=="black"){$combo .= " selected";}		
		$combo .= " class='black'>Preto</option>";	
			
		$combo .= "<option value='green'";
		if ($cor=="green"){$combo .= " selected";}
		$combo .= " class='green'>Verde</option>";	
					
		echo $combo;
	}
	
	public function gerarCodigo($assinatura_id) {
          return $this->getController()->geraCodigo($assinatura_id);
        }
	
	public function obterFontes($fonte="") { 
		$combo = "";

		$combo .= "<option value='Arial'";
		if ($fonte=="Arial"){$combo .= " selected";}
		$combo .= ">Arial</option>";
		
		$combo .= "<option value='Comic Sans'";
		if ($fonte=="Comic Sans"){$combo .= " selected";}
		$combo .= ">Comic Sans</option>";			
		
		$combo .= "<option value='Cursive'";
		if ($fonte=="Cursive"){$combo .= " selected";}
		$combo .= ">Cursive</option>";			
		
		$combo .= "<option value='Fantasy'";
		if ($fonte=="Fantasy"){$combo .= " selected";}
		$combo .= ">Fantasy</option>";			
	
		$combo .= "<option value='Helvetica'";
		if ($fonte=="Helvetica"){$combo .= " selected";}
		$combo .= ">Helvetica</option>";

		$combo .= "<option value='Impact'";
		if ($fonte=="Impact"){$combo .= " selected";}
		$combo .= ">Impact</option>";							
		
		$combo .= "<option value='Monospace'";
		if ($fonte=="Monospace"){$combo .= " selected";}
		$combo .= ">Monospace</option>";			

		$combo .= "<option value='Roman'";
		if ($fonte=="Roman"){$combo .= " selected";}
		$combo .= ">Roman</option>";			
		
		$combo .= "<option value='Sans-serif'";
		if ($fonte=="Sans-serif"){$combo .= " selected";}
		$combo .= ">Sans-serif</option>";	
		
		$combo .= "<option value='Serif'";
		if ($fonte=="Serif"){$combo .= " selected";}
		$combo .= ">Serif</option>";			

		$combo .= "<option value='Times'";
		if ($fonte=="Times"){$combo .= " selected";}
		$combo .= ">Times</option>";	
		
		$combo .= "<option value='Times New Roman'";
		if ($fonte=="Times New Roman"){$combo .= " selected";}
		$combo .= ">Times New Roman</option>";		
		
		
					
		echo $combo;
	}	
  
  public function getAreas($assinatura_id=""){
	
		$table_area = $this->getController()->obterAreas();
		
		if ($table_area != null){				
			$buffer= "";
			for($j = 0; $j < $table_area->RowCount(); $j++) {
				$row_area = $table_area->getRow($j);
				$buffer .="<input type='checkbox' name='area[]' id='area[]' value='".$row_area->area_id."' ";
				if ($assinatura_id!="")
					$buffer .= $this->getController()->checarAssinaturaArea($assinatura_id, $row_area->area_id);
				$buffer .=" >". $row_area->are_descricao."<br>";				
			}
		}
		
		return $buffer;
	
	}
  
  public function obterCodigo() {
    return $this->getController()->obterCodigo();
  }

   public function getOptin() {
        return $this->getController()->getOptin();
    }
    
    /**
     * Monta HTML com a listagem de �reas separadas por clientes e �reas de colabora��o.
     * Caso conex�o seja por meio de API cria-se campos tipo hidden.
     * Caso seja passado o par�metro usuario, a fun��o retorna os clientes do usu�rio informado.
     * Caso n�o seja passado o par�metro usuario, a fun��o retorna os clientes do usu�rio atualmente logado no sistema.
     * Andr� Alves - 27/01/2012
     * 
     * @param int $usuario Id do usu�rio
     * @return string Lista com os clientes
     */
    function listarAreas($assinatura = '') {
        
        $lista_clientes = $this->getController()->obterClientesDoUsuario();
        $buffer = "";
        
        if(checkapi()) {
            
            $lista_areas = $this->getController()->obterAreasDoUsuario();
            foreach($lista_areas->Rows() as $area) {
                $buffer .="<input type='hidden' name='area[]' value='".$area->area_id."' />";    
            }
            
        } else {
            
            if($lista_clientes != null) {
                foreach($lista_clientes->Rows() as $cliente) {
                    $lista_areas = $this->getController()->obterAreasDoUsuario($cliente->CLIE_Codigo);
                    
                    $buffer .= '<table class="formatted">
        			            <thead>
        							<tr>
        								<th width="15">&nbsp;</th>
                                        <th class="cliente">'.ucwords(strtolower(str_replace('zzz','',$cliente->CLIE_Nome))).'</th>
             			            </tr>
        						</thead>
        						<tbody>';
                                
                                if($lista_areas != null) {
                                    foreach($lista_areas->Rows() as $area) { 
                                        $buffer .= '<tr>
                                                        <td><input type="checkbox" name="area[]" class="area" value="'.$area->area_id.'" '.$this->getController()->checarAssinaturaArea($assinatura, $area->area_id).' /></td>
                                                        <td>'.$area->are_descricao.'</td>
                                                    </tr>';
                                    }	
                                } else {
                                    $buffer .= '    <tr>
                                                        <td></td>
                                                        <td>';
                                    $buffer .= translate("Nenhum registro encontrado");
                                    $buffer .="         </td>
                                                    </tr>";
                                }   
                                
                    $buffer.='  </tbody>
                              </table>';
                    
                }
            } else {
                $buffer .= translate("Nenhum registro encontrado");
            }
            
        }
        
        return $buffer.'<br /><br />';        
    
    }

}
?>
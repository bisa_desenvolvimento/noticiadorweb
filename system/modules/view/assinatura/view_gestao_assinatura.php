<?
class View_gestao_assinatura extends TView
{
	
	function show() { 
		$view=$this;
		require_once(DIR_TEMPLATES."assinatura/frm_gestao_assinatura.php");
	}
	
	 public function montarTabelaCadastrados() {
        $table = $this->getController()->obterTodos();

        $controller_arquivo = new controller_arquivo();
        $controller_arquivo->setConexao(TConexao::getInstance());
		

		$assinaturas = "";
        if ($table != null) {

			$buffer = "<form name='fomularioAssinatura' action='#' target='_self' method='post' onsubmit='return confirm(\"Confirma atualizar registros?\")'>";
			$buffer .= "<table class=\"formatted\">
						<thead>
							<tr>
								<td>".translate("C�d")."</td>
								<td>".translate("Texto Label")."</td>
								<td>".translate("Cor Label")."</td>
								<td>".translate("Texto Bot�o")."</td>
								<td>".translate("Cor Bot�o")."</td>
								<td>".translate("Respons�vel")."</td>
                <td width=40></td>
							</tr>
						</thead>
						<tbody>";		
			$class = "class=\"odd\"";	
            for($i = 0; $i < $table->RowCount(); $i++) {
            	$row = $table->getRow($i);
            	$assinaturas .= $row->assinatura_id.",";
				
				$buffer .= "<tr $class>";
				
        $buffer .= "<td>".$row->assinatura_id."</td>";
        $buffer .= "<td>".$row->ass_labelTexto."</td>";
				$buffer .= "<td>".$row->ass_corTexto."</td>";
                                $buffer .= "<td>".$row->ass_labelBotao."</td>";
                                $buffer .= "<td>".$row->ass_corBotao."</td>";
                          	$buffer .= "<td>".$row->usu_email."</td>";
				
        $urlAlteracao = "index.php?".PARAMETER_NAME_ACTION."=load&";
				$urlAlteracao .= PARAMETER_NAME_FILE."=assinatura";
				$urlAlteracao .= "&id=".$row->assinatura_id;
        
        $urlExclusao = "index.php?".PARAMETER_NAME_ACTION."=delete&";
				$urlExclusao .= PARAMETER_NAME_FILE."=assinatura";
				$urlExclusao .= "&id=".$row->assinatura_id;
        
        $buffer .="<td>
                     <a href=\"$urlAlteracao\"><img src=\"".DIR_ICONS."page_edit.png\" width=\"16\" height=\"16\" title=\"Editar\" /></a>
                     <a href=\"$urlExclusao\"><img src=\"".DIR_ICONS."delete.png\" title=\"Excluir\" /></a>
                   </td>";
        
        
        $buffer .= "</tr>";
        
				if($i%2 == 1) {
					$class = "";
				} else {
					$class = "class=\"odd\"";		
				}
            }
            $buffer .= "</tbody>
						<tfoot>
							<tr>
								<td colspan=8></td>
							</tr>
						</tfoot>
						</table>						
						<br>";
            $buffer .= "</form>";
		
            
            return $buffer;
        } else {
            return "Nenhum registro encontrado";
        }
    }
	
}
?>
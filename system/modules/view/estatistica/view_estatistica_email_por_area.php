<?
class View_estatistica_email_por_area extends TView {

    function show() {
        $view = $this;
        require_once(DIR_TEMPLATES."estatistica/frm_estatisca_email_por_area.php");
    }
    
    public function montarTabela() {
        $buffer = '<table class="formatted">
                       <thead>
                           <tr>
                               <th>Áreas</th>
                               <th scope="col">Total e-mails</th>
                           </tr>
                        </thead>
                        <tbody>';
        $table = $this->getController()->obterAreas();
        for($i = 0; $i < $table->RowCount(); $i++) {
            $row = $table->getRow($i);
        
            $buffer .= '    <tr class="odd">
                                <td>'.$row->are_descricao.'</td>
                                <td>'.$this->getController()->obterQuantidadeEmails($row->area_id).'</td>
                            </tr>';
        }
        
        $buffer .= '        </tbody>
                    </table>';
        
        return $buffer;
    }
  
}
?>
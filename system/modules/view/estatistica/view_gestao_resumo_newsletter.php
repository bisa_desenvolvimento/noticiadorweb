<?

class View_gestao_resumo_newsletter extends TView
{
	function show() {
		$view=$this;
		require_once(DIR_TEMPLATES."estatistica/frm_gestao_resumo_newsletter.php");
	}

	public function montarTabelaNoticiasCadastradas() {
		$table = $this->getController()->obterNoticias();
		/*$controller_arquivo = new controller_arquivo();
		$controller_arquivo->setConexao(TConexao::getInstance());*/

		if ($table != null) {

			$class = "class=\"odd\"";
			$buffer = "<br>
			<thead>
				<tr>
					<td colspan='3'>".translate("Título")."</td>
					<td>".translate("Despachada")."</td>
					<td>".translate("E-mails")."</td>
					<td>".translate("Enviados")."</td>
					<td>".translate("Lidos")."</td>
					<td width=\"70px\" colspan='2'>".translate("Ações")."</td>
					
				</tr>
			</thead>
			<tbody>";

			 //echo "<pre>"; print_r($table);die();

				foreach ($table as $row) {


					$data = date("d-m-Y", strtotime($row->data));

					$visualizados = ($row->visualizados != 0) ? $row->visualizados : 0;

					$buffer .= "
					<tr $class >
						<td class=\"nottitulo\" colspan='3'>".$row->titulo."</td>

						<td>".$data."</td>
						<td>".$row->total."</td>
						<td>".round((($row->enviados / $row->total)*100), 2)."%</td>

						<td>".$visualizados."</td>
						<td>
							
							<span class=\"resumo\" onClick=\"gerarNews('".$row->chave."')\">
								<img src=\"".DIR_ICONS."page.png\" width=\"16\" height=\"16\" title=\"Ver Relatário de Envio da Newsletter\" class=\"imgResumo\"/>
							</span>

							<span class=\"envio_email\">
								<img src=\"".DIR_ICONS."send_email.png\" width=\"25\" height=\"20\" title=\"Enviar Relatário de Envio da Newsletter por Email\" class=\"send_email\" onClick=\"enviarEmail('".$row->chave."')\"/>
							</span>
						</td>
					</tr>
					";



				}

				$buffer .= "</tbody>";

				return $buffer;

			} else {
				return "<h3>".translate("Nenhum registro encontrado")."</h3>";
			}
		}



	}
?>
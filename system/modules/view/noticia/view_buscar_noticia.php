<?
class view_buscar_noticia extends TView {
	function show() {
		$view = $this;    	
        require_once(DIR_TEMPLATES."noticia/frm_buscar_noticia.php");
	}
	public function buscarNoticia($array, $limitIni, $limitFim){
		$controller_arquivo = new controller_arquivo();
        $controller_arquivo->setConexao(TConexao::getInstance());
		
		$table_noticia = $this->getController()->buscarNoticia(str_replace("'","''",$array), $limitIni, $limitFim);
		if ($table_noticia != null){ 
			$buffer="";
			for($i = 0; $i < $table_noticia->RowCount(); $i++) { 
				$row_noticia = $table_noticia->getRow($i);
				
				$url = "index.php?".PARAMETER_NAME_ACTION."=show&";
				$url .= PARAMETER_NAME_FILE."=exibir_noticia";
				$url .= "&noticia_id=".$row_noticia->noticia_id;
				$buffer .= "<div id='corpoNoticia'>".produce_link($url, "<b>".date("d/m/Y", strtotime($row_noticia->not_data))."</b> ".substr($row_noticia->not_titulo.": ".$row_noticia->not_chamada,0,100)."...","",null, null, null, translate("Saiba mais")).'</div>';
				
			}
			return $buffer;
		}
	}


	public function getNumRowsBuscarNot($array){
		return $this->getController()->getNumRowsBuscarNot($array);
	}
	
	public function getLink($arquivo,$id, $nomeId="id"){
		$buffer="";
        $controller_arquivo = new controller_arquivo();
        $controller_arquivo->setConexao(TConexao::getInstance());

		$url = "index.php?".PARAMETER_NAME_ACTION."=show&";
		$url .= PARAMETER_NAME_FILE."=".$arquivo;
		$url .= "&".$nomeId."=".$id;
				
		return $url;
	}
	
	public function getPaginacao ($tr,$rpp,$pg, $Qstring, $linkURL) {
//		$tr eh total_records e $rpp eh registros por pagina
		if ($tr%$rpp==0){ $pages = intval($tr / $rpp)-1; } else { $pages = intval($tr / $rpp); }
//		calcula quantas paginas serao necessarias
		if ($tr>0){
//			echo "".translate("Total de Registros Encontrados").": $tr";  
			$NumRegistroInicial = ($pg*$rpp)+1;
			if ($pg <> $pages) {$NumRegistroFinal = ($pg*$rpp)+$rpp;} else {$NumRegistroFinal = $tr;}
			echo $NumRegistroInicial ."&nbsp; - &nbsp;". $NumRegistroFinal  ."&nbsp;".translate("de")."&nbsp;".$tr. "&nbsp;".translate("noticias encontradas");
			echo "<br>";
//			echo translate("Total de Paginas").":&nbsp;&nbsp;&nbsp;&nbsp;";  
			if ($pg <> 0) {
			   $showpage = $pg - 1;
			   echo '<a id=mulink href="'.$linkURL.'&pg=0&$Qstring"><img src="'.DIR_IMAGES.'page_left.jpg" border=0 alt="left" width="11" height="17" hspace="2" align="absmiddle" /></a>';
			   echo '<a id=mulink href="'.$linkURL.'&pg='.$showpage."&".$Qstring.'"><img src="'.DIR_IMAGES.'page_left2.jpg" alt="left2" border=0 width="19" height="17" hspace="2" align="absmiddle" /></a>&nbsp;';
			}
			for ($i = $pg-5; $i<$pg; $i++) {
				$showpage=$i+1;
				if ($i>=0) {
				   echo '<a id=mulink href="'.$linkURL.'?pg='.$i."&".$Qstring.'">'.$showpage.'</a>';
				   echo '&nbsp;&nbsp;';
				}
			}
			for ($i = $pg; ($i<=$pages AND $i<=($pg+5)); $i++) {
				$showpage=$i+1;
				if ($i == $pg) {
				   echo '<font face=Arial size=2 color=ff0000><b>'.$showpage.'&nbsp;&nbsp;</b></font>';
				} else {
				   echo '<a id=mulink href="'.$linkURL.'&pg='.$i."&".$Qstring.'">'.$showpage.'</a>';
				   echo '&nbsp;&nbsp;';
				}
			}
			if ($pg < $pages) {
				$showpage = $pg + 1;
				echo '<a id=mulink href="'.$linkURL.'&pg='.$showpage."&".$Qstring.'"> <img src="'.DIR_IMAGES.'page_right2.jpg" alt="right2" border=0 width="19" height="17" hspace="2" align="absmiddle" /></a>';
				echo '<a id=mulink href="'.$linkURL.'&pg='.$pages."&".$Qstring.'"> <img src="'.DIR_IMAGES.'page_right.jpg" alt="right" border=0 width="11" height="17" hspace="2" align="absmiddle" /></a>';
			}
	  	}
	}
}
?>
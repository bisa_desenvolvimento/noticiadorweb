<?
class View_gestao_noticia extends TView {
	function show() { 
		$view=$this;
		require_once(DIR_TEMPLATES."noticia/frm_gestao_noticia.php");
	}
	
	public function montarTabelaNoticiasCadastradas($pag) {
		
        $table = $this->getController()->obterNoticias($pag);
		
        $controller_arquivo = new controller_arquivo();
        $controller_arquivo->setConexao(TConexao::getInstance());

		$noticias = "";
        if ($table != null) {
        	$buffer = "<br>
			            <thead>
							<tr>
								<td>".translate("Título")."</td>
								<td>".translate("Quem Publicou")."</td>
								<td>".translate("Lidos")."</td>
                                <td>".translate("Publicação")."</td>
								<td>".translate("Limite")."</td>
								<td></td>
							</tr>
						</thead>
						<tbody>";
        	$class = "class=\"odd\"";
                $tituloTwitter ='';
                $idTwitter='';

			for($i = 0; $i < $table->RowCount(); $i++) {
            	$row = $table->getRow($i);
                $aceita = $row->not_autorizada;
            	$noticias .= $row->noticia_id.",";
            	if($i==0){
                $tituloTwitter= $row->noticia_id;
                $idTwitter= $row->not_titulo;
                }
                
                if(isset($_GET['id'])) {
                    if($row->noticia_id == $_GET['id']) {
                        $tituloTwitter= $row->noticia_id;
                        $idTwitter= $row->not_titulo;
                    }
                } else {
                    if($i == 0){
                        $tituloTwitter= $row->noticia_id;
                        $idTwitter= $row->not_titulo;
                    }    
                }
                
                
                
                $twittarNoticiaid = $row->noticia_id;
                $twittarTitulo = $row->not_titulo;
  	            $urlComentario = "index.php?".PARAMETER_NAME_ACTION."=show&";
				$urlComentario .= PARAMETER_NAME_FILE."=comentario";
				$urlComentario .= "&id=".$row->noticia_id;
				
				$urlAreas = "index.php?".PARAMETER_NAME_ACTION."=show&";
				$urlAreas .= PARAMETER_NAME_FILE."=alterar_areas_noticia";
				$urlAreas .= "&id=".$row->noticia_id;
				
				$urlExclusao = "index.php?".PARAMETER_NAME_ACTION."=delete&";
				$urlExclusao .= PARAMETER_NAME_FILE."=noticia";
				$urlExclusao .= "&id=".$row->noticia_id;
				
				$urlAlteracao = "index.php?".PARAMETER_NAME_ACTION."=load&";
				$urlAlteracao .= PARAMETER_NAME_FILE."=noticia";
				$urlAlteracao .= "&id=".$row->noticia_id;
            	
                $urlNoticia = "index.php?".PARAMETER_NAME_ACTION."=show&";
                $urlNoticia .= PARAMETER_NAME_FILE."=exibir_noticia";
                $urlNoticia .= "&noticia_id=".$row->noticia_id;
                
                $publicacao = date("d/m/Y",strtotime($row->not_publicacao)) == '31/12/1969' ? '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--' : date("d/m/Y",strtotime($row->not_publicacao));
                $validade = date("d/m/Y",strtotime($row->not_validade)) == '31/12/1969' ? '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--' : date("d/m/Y",strtotime($row->not_validade)); 
                
                $palavras = explode(' ',$row->not_titulo);
                $frase = '';
                foreach($palavras as $pal) {
                  $frase .= substr($pal,0,24).' ';
                }
                if(!$row->autorizada) {
                    $class = 'class="autorizar"';
                }

                //Buscar comentarios pendentes na notícia
                $comentarioPendente = $this->getController()->verificarComentariosPendentes($row->noticia_id);
                $comentarioIcon = $comentarioPendente ?"atencao.png": "minhas_informacoes.png";

                $buffer .= "<tr $class >
					<td class=\"nottitulo\" id=\"".$row->noticia_id."\"><a href='".$urlNoticia."' target='_blank'>".translate($frase)."</a></td>
					<td>".$row->usu_nome."</td>
					<td>".$row->not_acessos."</td>
          			<td>".$publicacao."</td>
					<td>".$validade."</td>
					<td width=90>
						<a href=\"$urlAlteracao\"><img src=\"".DIR_ICONS."page_edit.png\" width=\"16\" height=\"16\" title=\"Editar\" /></a>
						<a href=\"$urlComentario\"><img src=\"".DIR_ICONS.$comentarioIcon."\" width=\"16\" height=\"16\" title=\"".translate("Comentários")."\" /></a>
						<a href=\"$urlAreas\"><img src=\"".DIR_ICONS."area_de_interesse.png\" width=\"16\" height=\"16\" title=\"".translate("Alterar áreas")."\" /></a>
						<a href=\"$urlExclusao\" onclick=\"return confirm('Tem certeza que deseja excluir essa notícia ?')\"><img src=\"".DIR_ICONS."delete.png\" title=\"Excluir\" /></a>";

                                $buffer.='<span class="compartilhar">'.translate("Compartilhar").'</span>
                                                <div class="redes" style="display:none;">
                                                    <a href="https://twitter.com/share" class="twitter-share-button" data-url="'.URL.'index.php?action=show&secao=exibir_noticia&noticia_id='.$twittarNoticiaid.'" data-text="'.$twittarTitulo.'" data-count="horizontal" >Tweet</a><br/>
                                                    <a name="fb_share" type="icon_link"   share_url="'.URL.'index.php?action=show&secao=exibir_noticia&noticia_id='.$twittarNoticiaid.'">'.  translate("Compartilhar").'</a>
                                                </div>
                                                </div>';

                                         $buffer.="</td>
				</tr>";
				if($i%2 == 0) {
					$class = "";
				} else {
					$class = "class=\"odd\"";		
				}				
			}
			$buffer .= "</tbody>
						<tfoot>
							<tr>
								<td colspan='6' class='paginacao'>".$this->montarPaginacao()."</td>
							</tr>
						</tfoot>";
                        if(isset ($_SESSION['twitter'])){

                            //div do twitter que pergunta quando é cadastrada uma nova noticia se o usuário quer twittar
                            /*$buffer.='<div id="dialog-modal" title="Notícia salva com sucesso!">
                            <p>Cada ocação que sua noticia seja compartilhada será sumada no contardor, proporcionandole uma estadistica.</p>
                            <div align="left"><a href="https://twitter.com/share" id="twitterbotao" class="twitter-share-button" data-url="'.URL.'index.php?action=show&secao=exibir_noticia&noticia_id='.$tituloTwitter.'" data-text="'.$idTwitter.'"data-count="horizontal" >Tweet</a></div>
                            <a style="float: left; margin:23px 0px 18px 0px;" href="javascript:popup(\'https://www.facebook.com/sharer.php?u='.URL.'index.php?action=show%2526secao=exibir_noticia%2526noticia_id='.$tituloTwitter.'&t='.urlencode($idTwitter).'\')" target="blank">'.produce_icon("connect_favicon.png").translate("Compartilhar").'</div>';*/
                            $texto_not_salva_sucesso = '<div id="dialog-modal" title="Notícia salva com sucesso!">
                            <p>Para compartilhar sua notícia, clique nos botões de compartilhamento na página da mesma. Clique abaixo para acessar a página da notícia.</p>
                            <p>Cada ocasião que sua notícia for compartilhada será adiciona ao contador, porporcionando-lhe uma estatística.</p>
                            <div align="center"><a href="'.URL.'index.php?action=show&secao=exibir_noticia&noticia_id='.$_SESSION['twitter'].'" target="_blank"><strong>Acessar notícia</strong></a></div>';
							//$texto_not_salva_sucesso = utf8_encode($texto_not_salva_sucesso);
							$buffer .= $texto_not_salva_sucesso;
                        }
                        unset ($_SESSION['twitter']);

     		return $buffer;
        } else {
            return "<h3>".translate("Nenhum registro encontrado")."</h3>";
        }
        
    }
			
			
	
	public function montarTabelaCadastrados() {
        $table = $this->getController()->obterTodos();
		$table_area = $this->getController()->obterAreas();
        $controller_arquivo = new controller_arquivo();
        $controller_arquivo->setConexao(TConexao::getInstance());

		$noticias = "";
        if ($table != null) {

			$buffer = "<form name='fomularioNoticia' action='#' target='_self' method='post' onsubmit='return confirm(\"Confirma atualizar registros?\")'>";
            $buffer .= "<table align=\"center\">";
			$buffer .= "<tr>";
			$buffer .= "<td>".translate("Excluir")."</td>";
			$buffer .= "<td>".translate("Data")."</td>";
			$buffer .= "<td>".translate("Data")."</td>";
			$buffer .= "<td>".translate("Titulo")."</td>";
			$buffer .= "<td>".translate("Quem Publicou")."</td>";
            $buffer .= "<td>".translate("Publicação")."</td>";
			$buffer .= "<td>".translate("Limite")."</td>";
			$buffer .= "<td>".translate("Comentarios")."</td>";

			if ($table_area != null){
				for($i = 0; $i < $table_area->RowCount(); $i++) {
    	        	$row = $table_area->getRow($i);
					$buffer .= "<td>".$row->are_descricao."</td>";
				}
			}
			$buffer .= "</tr>";			
            for($i = 0; $i < $table->RowCount(); $i++) {
            	$row = $table->getRow($i);
            	$noticias .= $row->noticia_id.",";
				$buffer .= "<tr>";
				$buffer .= "<td><input type='checkbox' name='excluir[]' value='".$row->noticia_id."'/></td>";
				$buffer .= "<td>".$row->not_data."</td>";
            	$buffer .= "<td>".$row->noticia_id."</td>";
				$url = "index.php?".PARAMETER_NAME_ACTION."=load&";
				$url .= PARAMETER_NAME_FILE."=noticia";
				$url .= "&id=".$row->noticia_id;
            	$buffer .= "<td>".produce_link($url, $row->not_titulo)."</td>";
            	$buffer .= "<td>".$row->usu_nome."</td>";
              $buffer .= "<td>".$row->not_publicacao."</td>";				
            	$buffer .= "<td>".$row->not_validade."</td>";

				$url = "index.php?".PARAMETER_NAME_ACTION."=show&";
				$url .= PARAMETER_NAME_FILE."=comentario";
				$url .= "&id=".$row->noticia_id;
												
				$buffer .= "<td>".produce_link($url, translate("Comentarios"))."</td>";

				
				if ($table_area != null){				
					for($j = 0; $j < $table_area->RowCount(); $j++) {
						$row_area = $table_area->getRow($j);
						$buffer .="<td><input type='checkbox' name='area_".$row_area->area_id."[]' value='".$row->noticia_id."' " . $this->getController()->obterNoticiaArea($row->noticia_id, $row_area->area_id) . " ></td>";
					}
				}
            	$buffer .= "</tr>";
            }
			if ($noticias!=""){
				$noticias = substr($noticias,0,strlen($noticias)-1);

			}
            $buffer .= "</table>";
			$buffer .= "<input type='submit'  name='atualizar_area_noticia' value='Atualizar' class=\"botao\"/>";
			$buffer .= "<input type='hidden' name='noticias' value='". $noticias ."' />";
            $buffer .= "</form>";
		
            
            return $buffer;
        } else {
            return "<h3>".translate("Nenhum registro encontrado")."</h3>";
        }
    }
	public function atualizarNoticias($delete) {
		$table_area = $this->getController()->obterAreas();		
		$k = 0;
		if ($table_area != null){				
				// para todos as areas do usuario logado
				for($j = 0; $j < $table_area->RowCount(); $j++) {
					$row_area = $table_area->getRow($j);
					// se foi selecionado algum checkbox para a area atual
					if (isset($_POST['area_'.$row_area->area_id])){
						// para todos os elementos contidos na area atual
						for($i=0;$i<sizeof($_POST['area_'.$row_area->area_id]);$i++){						
							$insert[$k]=$row_area->area_id."_".$_POST['area_'.$row_area->area_id][$i];
							$k++;
						}// fim for
					}// fim if
					
				}// fim primeiro for
		}// fim do if da consulta de areas

	 	$this->getController()->atualizarAreas($delete, $insert);
	}
	 
	public function apagarNoticias($delete) { 
	 	$this->getController()->excluirNoticias($delete);
	}

		
	public function montarTabelaAreas() { 
		$combo = "";
		$table_area = $this->getController()->obterAreas();
		if ($table_area != null){	
			for($j = 0; $j < $table_area->RowCount(); $j++) {
					if($j%3 == 0)
						$combo .= "<tr>";
					$row_area = $table_area->getRow($j);
					$combo .= "<td><input type='checkbox' name='FArea[]' value='".$row_area->area_id."'>
								   <label for=\"FArea[]\">".$row_area->are_descricao ."</label>
							   </td>";
					if($j%3 == 2)
						$combo .= "</tr>";
			}

		}			
		echo $combo;
	}	

	public function montarComboDatasNoticias() { 
		$combo = "";
		$datas = $this->getController()->obterDatasNoticias();
		if ($datas!= null){	
			for($j = 0; $j < $datas->RowCount(); $j++) {
					$row_data = $datas->getRow($j);
					$combo .= "<option value='".$row_data->meses."'>".$row_data->meses."</option>";
			}

		}			
		echo $combo;
	}
    
    ## André Alves - 27/04/11 - 1381  
    public function montarPaginacao() {
        
        $total = $_SESSION['totalnoticias']/NOTICIAS_PAGINA;
        settype($total, "integer");
        
        $paginas = "";
        for($i=1;$i<=$total;$i++) {
            
            $url = "index.php?".PARAMETER_NAME_ACTION."=show&";
	        $url .= PARAMETER_NAME_FILE."=gestao_noticia";
            $url .= "&pag=".$i;
            
            if ((isset($_REQUEST['FDataInicial']))&&($_REQUEST['FDataInicial']!="")){
    			$url .= "&FDataInicial=".$_REQUEST['FDataInicial'];
    		}
    		if (isset($_REQUEST['FDataFinal']) &&($_REQUEST['FDataFinal']!="")){
    			$url .= "&FDataFinal=".$_REQUEST['FDataFinal'];		
    		}
    		if ((isset($_REQUEST['FMes']))&&($_REQUEST['FMes']!="0")){
    			$url .= "&FMes=".$_REQUEST['FMes'];
    		}
    		if ((isset($_REQUEST['FArea']))&&($_REQUEST['FArea']!='0')){
    			$url .= "&FArea=".$_REQUEST['FArea'];
    		}
            if ((isset($_REQUEST['FPalavra']))&&($_REQUEST['FPalavra']!="")){
                $url .= "&FPalavra=".$_REQUEST['FPalavra'];
            }
            
            if((isset($_GET['pag']) && $_GET['pag'] == $i) || (!isset($_GET['pag']) && $i == 1)) {
                $paginas .= $i;
            } else {
                $paginas .= '<a href="'.$url.'">'.$i.'</a>';
            }
            
            if($i < $total) 
                $paginas .= " - ";
        }
  
        return $paginas;
    }
    
    public function montarComboAreasNoticia() {
		$combo = "";
		$table_area = $this->getController()->obterAreasDoUsuario();
		if ($table_area != null){
			for($j = 0; $j < $table_area->RowCount(); $j++) {
				$row_area = $table_area->getRow($j);
				$combo .= "<option value='".$row_area->area_id."' 
						".(isset($_REQUEST['FArea']) && $_REQUEST['FArea'] == $row_area->area_id ? "selected" : "").">
						".$row_area->are_descricao."</option>";
			}
		}
		echo $combo;
    }
	 

}
?>
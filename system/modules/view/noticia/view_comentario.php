<?
class view_comentario extends TView
{
	function show() {
		$view = $this;    	
        require_once(DIR_TEMPLATES."noticia/frm_comentario.php");
	
	}

	function obterNoticia($noticia_id) {
		
		$table_noticia = $this->getController()->obterNoticia($noticia_id);	
		if ($table_noticia != null){
			$row_noticia = $table_noticia->getRow(0);
			return $row_noticia->not_titulo;//$row_noticia->noticia_id." - ".$row_noticia->not_titulo;
		}
	}	
	
	function tabelaComentarios($noticia_id,$not_titulo) {
			echo $this->getArrayDivs($noticia_id);
			
			$controller_arquivo = new controller_arquivo();
			$controller_arquivo->setConexao(TConexao::getInstance());
			$url = "index.php?".PARAMETER_NAME_ACTION."=salvar&";
			$url .= PARAMETER_NAME_FILE."=comentario";
	
			$buffer = "<form name='fomularioComentario' action='$url' target='_self' method='post' onsubmit='return confirm(\"Confirma atualiza��o dos registros?\")'>";
			$buffer .= "<input type='hidden' name='noticia_id' id='noticia_id' value='".$noticia_id."'/>";
			$buffer .= "<input type='hidden' name='not_titulo' id='not_titulo' value='".$not_titulo."'/>";
			$buffer .= "<table class=\"formatted\">
						<thead>
							<tr>
								<td>".translate("Data")."</td>
															
								<td>".translate("Coment�rio e Informa��es")."</td>
								
								<td>".translate("Exibir?")."</td>
							</tr>
						</thead>
						<tbody>";

		
			$table_comentario = $this->getController()->obterComentarios($noticia_id);	
			
			if ($table_comentario!=null){
				$class = "class=\"odd\"";
				for($j = 0; $j < $table_comentario->RowCount(); $j++) {
					$checked='';
                                       

                                        $row_comentario = $table_comentario->getRow($j);
					if($this->getController()->obterComentarioAprovado($row_comentario->comentario_id)=="checked"){
                                        $checked="checked";
                                        }
                                        $buffer .= "<tr $class>";
					$buffer .= "<td>".date("H:i d/m/Y", strtotime($row_comentario->com_data))."</td>";										
					// $buffer .= "<td>".$row_comentario->com_nome."</td>";
					// $buffer .= "<td>".$row_comentario->com_email."</td>";
					// $buffer .= "<td>".$row_comentario->com_fone."</td>";
					$buffer .= "<td><a href='#' onclick='show(\"coment_".$row_comentario->comentario_id."\", divsNome);show(\"nome_".$row_comentario->comentario_id."\", divsNome);show(\"email_".$row_comentario->comentario_id."\", divsEmail);show(\"fone_".$row_comentario->comentario_id."\", divsFone);show(\"info_".$row_comentario->comentario_id."\", divsInfo)'>".translate("Clique aqui")."</a></td>";                    
					$buffer .= "<td><input type='checkbox' name='comentarios[]' value='".$row_comentario->comentario_id."' $checked  >";
                                        if($checked=="checked")
                                        $buffer.="<input type='hidden' name='aceptar[]' value='".$row_comentario->comentario_id."'></td>";
					$buffer .= "</tr>";					

					$buffer .="<tr id='coment_".$row_comentario->comentario_id."' style='visibility:hidden; position:absolute'>";
					$buffer .= "<td colspan='7'><h1 class=\"default_title\"> Coment�rio</h1>".nl2br($row_comentario->com_comentario)."</td>";
					$buffer .= "</tr>";

					$buffer .="<tr id='nome_".$row_comentario->comentario_id."' style='visibility:hidden; position:absolute'>";
                    $buffer .= "<td colspan='7'><h1 class=\"default_title\"> Nome </h1>".nl2br( $row_comentario->com_nome)."</td>";
                    $buffer .= "</tr>";

                    $buffer .="<tr id='email_".$row_comentario->comentario_id."' style='visibility:hidden; position:absolute'>";
                    $buffer .= "<td colspan='7'><h1 class=\"default_title\"> Email </h1>".nl2br( $row_comentario->com_email)."</td>";
                    $buffer .= "</tr>";

                    $buffer .="<tr id='fone_".$row_comentario->comentario_id."' style='visibility:hidden; position:absolute'>";
                    $buffer .= "<td colspan='7'><h1 class=\"default_title\"> Fone</h1>".nl2br( $row_comentario->com_fone)."</td>";
                    $buffer .= "</tr>";

                    $buffer .="<tr id='info_".$row_comentario->comentario_id."' style='visibility:hidden; position:absolute'>";
                    $buffer .= "<td colspan='7'><h1 class=\"default_title\"> Informa��es do usu�rio</h1>".nl2br( $row_comentario->com_infousuario)."</td>";
                    $buffer .= "</tr>";                   



                    if($j%2 == 1) {
						$class = "";
					} else {
						$class = "class=\"odd\"";		
					}
					
				}
				
			} else {
		    return "<h3>".translate("Nenhum coment�rio encontrado")."</h3>";
			}
			$buffer .= "</tbody>
						<tfoot>
							<tr>
								<td colspan=7></td>
							</tr>
						</tfoot>
						</table>						
						<br>";
			$buffer .= "<input type='submit'  name='button' id='button' value='Atualizar'/>";
            $buffer .= "</form>";

			return $buffer;
	}	
	
	function obterComentarios($noticia_id) {
			echo $this->getArrayDivs($noticia_id);
	
			$buffer = "<form name='fomularioComentario' action='#' target='_self' method='post' onsubmit='return confirm(\"Confirma atualizar registros?\")'>";
            $buffer .= "<table border=0>";
			$buffer .= "<tr>";
			$buffer .= "<td>Cod.</td>";
			$buffer .= "<td>".translate("Data")."</td>";			
			$buffer .= "<td>".translate("Nome")."</td>";
			$buffer .= "<td>".translate("E-mail")."</td>";
			$buffer .= "<td>".translate("Fone")."</td>";
			$buffer .= "<td>".translate("Coment�rio")."</td>";
            $buffer .= "<td>".translate("Informa��es")."</td>";
			$buffer .= "<td>".translate("Exibir?")."</td>";
			$buffer .= "</tr>";

		
		$table_comentario = $this->getController()->obterComentarios($noticia_id);	
			
			if ($table_comentario!=null){ 
				for($j = 0; $j < $table_comentario->RowCount(); $j++) {
					$row_comentario = $table_comentario->getRow($j);
					$buffer .="<tr>";
					$buffer .= "<td>".$row_comentario->comentario_id."</td>";
					$buffer .= "<td>".date("d/m/Y h:i ", strtotime($row_comentario->com_data))."</td>";										
					$buffer .= "<td>".$row_comentario->com_nome."</td>";
					$buffer .= "<td>".$row_comentario->com_email."</td>";
					$buffer .= "<td>".$row_comentario->com_fone."</td>";
					$buffer .= "<td><a href='#' onclick='show(\"coment_".$row_comentario->comentario_id."\", divsComments)'>".translate("Clique aqui")."</a></td>";
                    $buffer .= "<td><a href='#' onclick='show(\"info_".$row_comentario->comentario_id."\", divsInfo)'>".translate("Clique aqui")."</a></td>";
					$buffer .="<td><input type='checkbox' name='comentarios[]' value='".$row_comentario->comentario_id."' " . $this->getController()->obterComentarioAprovado($row_comentario->comentario_id). " ></td>";
                    $buffer .="<tr id='coment_".$row_comentario->comentario_id."' style='visibility:hidden; position:absolute'>";
                    $buffer .= "<td colspan='7'><h1 class=\"default_title\"> Coment�rio</h1>".nl2br($row_comentario->com_comentario)."</td>";
                    $buffer .="<tr id='info_".$row_comentario->comentario_id."' style='visibility:hidden; position:absolute'>";
                    $buffer .= "<td colspan='7'><h1 class=\"default_title\"> Informa��es do usu�rio</h1>".nl2br( $row_comentario->com_infousuario)."</td>";
                    $buffer .= "</tr>";
					
				}
				
			} 
			$buffer .= "</table>";
			$buffer .= "<input type='submit'  name='atualizar_comentario' value='Atualizar' class=\"botao\"/>";
            $buffer .= "</form>";

			return $buffer;
	}

	function getArrayDivs($noticia_id){
			$table_comentario = $this->getController()->obterComentarios($noticia_id);

			$buffer="<script> var divsComments =  new Array();";
			if ($table_comentario!=null){ 
				for($j = 0; $j < $table_comentario->RowCount(); $j++) {
					$row_comentario = $table_comentario->getRow($j);
					$buffer.="divsComments[$j]='coment_".$row_comentario->comentario_id."';";
				}
			}
			

            $buffer.="var divsInfo = new Array();";
            if ($table_comentario!=null){
                for($j = 0; $j < $table_comentario->RowCount(); $j++) {
                    $row_comentario = $table_comentario->getRow($j);
                    $buffer.="divsInfo[$j]='info_".utf8_encode($row_comentario->comentario_id)."';";
                }
            }
            

            $buffer.="var divsNome = new Array();";
            if ($table_comentario!=null){
                for($j = 0; $j < $table_comentario->RowCount(); $j++) {
                    $row_comentario = $table_comentario->getRow($j);
                    $buffer.="divsNome[$j]='nome_".utf8_encode($row_comentario->comentario_id)."';";
                }
            }
            

            $buffer.="var divsEmail = new Array();";
            if ($table_comentario!=null){
                for($j = 0; $j < $table_comentario->RowCount(); $j++) {
                    $row_comentario = $table_comentario->getRow($j);
                    $buffer.="divsEmail[$j]='email_".utf8_encode($row_comentario->comentario_id)."';";
                }
            }
            

            $buffer.="var divsFone = new Array();";
            if ($table_comentario!=null){
                for($j = 0; $j < $table_comentario->RowCount(); $j++) {
                    $row_comentario = $table_comentario->getRow($j);
                    $buffer.="divsFone[$j]='fone_".utf8_encode($row_comentario->comentario_id)."';";
                }
            }
            $buffer.="</script>";
		 return $buffer;
	}
	
	function atualizarComentarios($array, $noticia_id){
			$this->getController()->atualizarComentarios($array, $noticia_id);		
	}
}
?>
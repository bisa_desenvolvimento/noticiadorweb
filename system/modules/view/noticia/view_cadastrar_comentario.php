<?
class view_cadastrar_comentario extends TView
{
	function show() {
		$view = $this;    	
        require_once(DIR_TEMPLATES."noticia/frm_cadastrar_comentario.php");
	
	}

	function enviarComentario($noticia_id) {
	
		$this->getController()->enviarComentario($noticia_id);	
	}
	
	function tituloNoticia($noticia_id) {
	
		return $this->getController()->tituloNoticia($noticia_id);	
	}


	function obterComentarios($noticia_id) {
	
		
		$table_comentario = $this->getController()->obterComentarios($noticia_id);	
			$buffer ="";
			if ($table_comentario!=null){ // IF_3
				for($j = 0; $j < $table_comentario->RowCount(); $j++) {
					$row_comentario = $table_comentario->getRow($j);
					$buffer .= "<div id='corpoComentario'>"."<b>".date("d/m/Y h:i ", strtotime($row_comentario->com_data))."</b> ".translate("Nome")." :".$row_comentario->com_nome."<br>E-mail: ".$row_comentario->com_email."<br>".$row_comentario->com_comentario.'</div>';
					
				}
				
			} 
			return $buffer;
	}
	
	public function getLink($arquivo,$id, $nomeId="id"){
		$buffer="";
        $controller_arquivo = new controller_arquivo();
        $controller_arquivo->setConexao(TConexao::getInstance());

		$url = "index.php?".PARAMETER_NAME_ACTION."=show&";
		$url .= PARAMETER_NAME_FILE."=".$arquivo;
		$url .= "&".$nomeId."=".$id;
				
		return $url;

	}

}
?>
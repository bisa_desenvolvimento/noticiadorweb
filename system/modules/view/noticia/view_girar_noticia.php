<?
class view_girar_noticia extends TView
{
	function show() {
		$view = $this;    	
        require_once(DIR_TEMPLATES."noticia/frm_girar_noticia.php");
	
	}
	
	public function getNoticiasJanela($janela_id){
	
        $controller_arquivo = new controller_arquivo();
        $controller_arquivo->setConexao(TConexao::getInstance());
		$buffer = "";
		$scriptinicio = "";
		$scriptfim = "";
		$table_janela = $this->getController()->obterJanelas($janela_id);
		if ($table_janela != null){
			$row_janela = $table_janela->getRow(0);
			$table_noticia = $this->getController()->obterNoticiasJanela($janela_id, $row_janela->jan_quantidade);
			if ($table_noticia != null){
				if ($row_janela->jan_tipo=='G'){
						$scriptinicio = "<script>";
				}					
				for($j = 0; $j < $table_noticia->RowCount(); $j++) {
					$row_noticia = $table_noticia->getRow($j);					
					$url = "index.php?".PARAMETER_NAME_ACTION."=show&";
					$url .= PARAMETER_NAME_FILE."=visualizar_noticia";
					$url .= "&id=".$row_noticia->noticia_id;
					$rolar=""; //GIRAR NOTICIAS
					if ($row_janela->jan_tipo=='G'){
						$rolar = "pausecontent[$j]='";
					}		
					$indicar = "";
					if ($row_janela->jan_indicar){
						$urlIndicar = URL.$this->getLink("indicar_noticia",$row_noticia->noticia_id);
						$indicar = produce_link($urlIndicar, "", array("target"=>"_blank"),null, null, null, " - Indique");
					}
					$comentar = "";
					if ($row_janela->jan_comentar){
						$urlComentar = URL.$this->getLink("cadastrar_comentario",$row_noticia->noticia_id);
						$comentar = produce_link($urlComentar, "", array("target"=>"_blank"),null, null, null, " - Comente");
					}
					$visualizarComentario = "";
					if ($row_janela->jan_visualizarComentario){
						$urlVerComentarios = URL.$this->getLink("cadastrar_comentario",$row_noticia->noticia_id);
						$visualizarComentario = produce_link($urlVerComentarios, "", array("target"=>"_blank"),null, null, null, " - Ver Comentários");
					}
					$data = "";
					if ($row_janela->jan_data){
     					   $data = date("d/m/Y", strtotime($row_noticia->not_data))." ";
					}
					if (!$row_janela->jan_exibirApenasTitulo){
						$buffer .= $rolar.produce_link($url, "<b>".$data.$row_noticia->not_titulo.": "."</b> ".$row_noticia->not_chamada."<br>",array("target"=>"_blank"),null, null, null, "Veja mais").$indicar.$comentar.$visualizarComentario."<br><br>";
					}else{
						$buffer .= $rolar.produce_link($url, "<b>".$data.$row_noticia->not_titulo."</b> "."<br>",array("target"=>"_blank"),null, null, null, "Veja mais").$indicar.$comentar.$visualizarComentario."<br><br>";
					}
					if ($row_janela->jan_tipo=='G')
						$buffer .= "';";
//            		$buffer .= "<br>";
				
				}
				$scriptfim = "";
				if ($row_janela->jan_tipo=='G'){
						$scriptfim = "new pausescroller(pausecontent, \"pscroller2\", \"someclass\", 2000)";						
						$scriptfim .= "</script>";
				}					
				$scriptfim .= "<html>
							<body bgcolor='$row_janela->jan_cor'>&nbsp;</body>
						</html>";

				
			}
			
		}
		
		return $scriptinicio.$buffer.$scriptfim;	

	}
	
	public function getLink($arquivo,$id, $nomeId="id"){
		$buffer="";
        $controller_arquivo = new controller_arquivo();
        $controller_arquivo->setConexao(TConexao::getInstance());

		$url = "index.php?".PARAMETER_NAME_ACTION."=show&";
		$url .= PARAMETER_NAME_FILE."=".$arquivo;
		$url .= "&".$nomeId."=".$id;
				
		return $url;

	}	

}
?>
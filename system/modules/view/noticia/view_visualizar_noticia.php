<?
class view_visualizar_noticia extends TView
{
	function show() {
		$view = $this;    	
        require_once(DIR_TEMPLATES."noticia/frm_visualizar_noticia.php");
	
	}
	
	function inserirEmail() {
	
	
	}
	
	public function getNoticia($janela_id){
		$buffer="";


		$table_noticia = $this->getController()->obterNoticia($janela_id);
		if ($table_noticia != null){
			$row_noticia = $table_noticia->getRow(0);
			$imagem = "";
            		if($row_noticia->not_thumbnail != "") {
                           $url = URL.DIR_UPLOAD.$row_noticia->not_thumbnail;
                           $imagem = "<img src='$url'border='0'>";
		        }
			$this->getController()->contarAcessos($janela_id);
				$buffer='<div id="tituloNoticia"><h1 class="default_title">'.$row_noticia->not_titulo.'</h1></div>';
				$buffer.='<div id="corpoNoticia">'.$imagem." ".$row_noticia->not_texto.'</div><br /><br />';
				$buffer.='<div id="linkNoticia"><a href="'.$row_noticia->not_link.'" target="_blank">'.$row_noticia->not_link.'</a></div>';
				$buffer.='</br>';
				$buffer.='<label>Data Divulga��o: </label>'.DateExibe($row_noticia->not_data);//$buffer.='<div id="dataNoticia"><label>Data Divulga��o:</label>'.DateExibe($row_noticia->not_data).'</div>';
				$buffer.='<div id="acessosNoticia"><label>Esta not�cia j� foi visualizada '.$row_noticia->not_acessos.' vezes </label></div>';
			
		}
		return $buffer;

	}
	
	public function getLink($arquivo,$id, $nomeId="id"){
		$buffer="";
        $controller_arquivo = new controller_arquivo();
        $controller_arquivo->setConexao(TConexao::getInstance());

		$url = "index.php?".PARAMETER_NAME_ACTION."=show&";
		$url .= PARAMETER_NAME_FILE."=".$arquivo;
		$url .= "&".$nomeId."=".$id;
				
		return $url;

	}
	
	public function	getNoticiaLateral($noticia_id){
		$controller_arquivo = new controller_arquivo();
        $controller_arquivo->setConexao(TConexao::getInstance());

		$buffer="";
		$not_in = $noticia_id.",";
		$table_area = $this->getController()->obterAreasLaterais($noticia_id);
		if ($table_area != null){ // IF_1

			for($i = 0; $i < $table_area->RowCount(); $i++) {  // todas as �reas que a noticia pertence - FOR_1
				$row_area = $table_area->getRow($i);
				$not_in_aux = substr($not_in,0,strlen($not_in)-1);			
				if ($this->getController()->obterNoticiasLaterais("",$row_area->area_id, 5, $not_in_aux)){// verifica se ainda tem not�cias para a �rea atual diferente da anterior IF_2
						$buffer.='<div id="areaLateral">'.$row_area->are_descricao.'</div>';
						$table_noticia = $this->getController()->obterNoticiasLaterais("",$row_area->area_id, 5, $not_in_aux);
						if ($table_noticia!=null){ // IF_3
							for($j = 0; $j < $table_noticia->RowCount(); $j++) {// todas as not�cias v�lidas da �rea atual, que nao tenham aparecido ainda
								$row_noticia = $table_noticia->getRow($j);
								$not_in .= $row_noticia->noticia_id.",";// guarda as noticias anteriores para n�o se repetirem
								$url = "index.php?".PARAMETER_NAME_ACTION."=show&";
								$url .= PARAMETER_NAME_FILE."=visualizar_noticia";
								$url .= "&id=".$row_noticia->noticia_id;
								$buffer .= "<div id='corpoNoticia'>".produce_link($url, "<b>".date("d/m/Y", strtotime($row_noticia->not_data))."</b> ".substr($row_noticia->not_titulo.": ".$row_noticia->not_chamada,0,100)."...","",null, null, null, "Veja mais").'</div>';
								
							}// END FOR_2
							
						} // END IF_3
				}// END IF_2
			}// END FOR_1
							
		} // END IF_1
		return $buffer;		
		
	}
	


}
?>
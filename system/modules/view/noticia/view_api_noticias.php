<?

class View_api_noticias extends TView {

	public function show() {
	}

	function load($key){
	}

	function save($model){
	}

	function delete($key){
	}

	function create(){
	}

	function scriptBeforeAll() {
		require_once(DIR_TEMPLATES."noticia/ScriptBeforeAll.php");
	}

	function scriptBeforeShareBar() {
		require_once(DIR_TEMPLATES."noticia/ScriptBeforeShareBar.php");
	}

	function shareBar() {
		require_once(DIR_TEMPLATES."noticia/ShareBar.php");
	}

	function scriptAfterShareBar() {
		require_once(DIR_TEMPLATES."noticia/ScriptAfterShareBar.php");
	}

	function scriptBeforeMetaDados() {
		require_once(DIR_TEMPLATES."noticia/ScriptBeforeMetaDados.php");
	}

	function metaDados($dados_requisicao) {
		$link = (isset($dados_requisicao["link"])) ? $dados_requisicao["link"]: "";
		$url = (isset($dados_requisicao["url"])) ? $dados_requisicao["url"] : "";
		$noticia_id = (isset($dados_requisicao['noticia_id'])) ? $dados_requisicao['noticia_id'] : "";
		$noticia_titulo = (isset($dados_requisicao["noticia_titulo"])) ? $dados_requisicao["noticia_titulo"] : "";
		$autor = (isset($dados_requisicao["autor"])) ? $dados_requisicao["autor"] : "";
		$data = (isset($dados_requisicao["data"])) ? $dados_requisicao["data"] : "";
		$acessos = (isset($dados_requisicao["acessos"])) ? $dados_requisicao["acessos"] : "";
		require_once(DIR_TEMPLATES."noticia/MetaDados.php");
	}

	function scriptBeforeFormComentar() {
		require_once(DIR_TEMPLATES."noticia/ScriptBeforeFormComentar.php");
	}

	function formComentar() {
		$areas = $this->getController()->obterAreasNoticia($_GET['noticia_id']);
		require_once(DIR_TEMPLATES."noticia/FormComentar.php");
	}

	function scriptAfterFormComentar() {
		require_once(DIR_TEMPLATES."noticia/ScriptAfterFormComentar.php");
	}

	function shareComment() {
		require_once(DIR_TEMPLATES."noticia/ShareComment.php");
	}

	function scriptAfterAll() {
		require_once(DIR_TEMPLATES."noticia/ScriptAfterAll.php");
	}

}
?>
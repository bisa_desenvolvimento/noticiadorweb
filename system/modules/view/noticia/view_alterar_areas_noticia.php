<?
class View_alterar_areas_noticia extends TView
{
	function show() { 
		$view = $this;
		require_once(DIR_TEMPLATES."noticia/frm_areas_noticia.php");
	}	
	
	function obterNoticia($noticia_id) {		
		$table_noticia = $this->getController()->obterNoticia($noticia_id);	
		if ($table_noticia != null){
			$row_noticia = $table_noticia->getRow(0);
			return $row_noticia->not_titulo;//$row_noticia->noticia_id." - ".$row_noticia->not_titulo;
		}
	}	
	
	function listarAreasCadastradas($idNoticia) {
	    $tableAreas = $this->getController()->obterAreas();
	    
		$controller_arquivo = new controller_arquivo();
		$controller_arquivo->setConexao(TConexao::getInstance());
		$url = "index.php?".PARAMETER_NAME_ACTION."=save&";
		$url .= PARAMETER_NAME_FILE."=alterar_areas_noticia";
		$buffer = "<form name='fomularioAreaNoticia' action='$url' target='_self' method='post' onsubmit='return confirm(\"Confirma atualização das áreas?\")'>";
		$buffer .= "<input type='hidden' name='noticia_id' id='noticia_id' value='".$idNoticia."'/>";
		$buffer .= "<table class=\"formatted\">
					<thead>
						<tr>
							<td>".translate("Código")."</td>
							<td>".translate("Descrição")."</td>
							<td>".translate("Observação")."</td>
							<td>".translate("Incluir?")."</td>
						</tr>
					</thead>
					<tbody>";
		if ($tableAreas != null) {
        	$class = "class=\"odd\"";
        	$i = 0;
            foreach($tableAreas->Rows() as $row) { 
                $area = new Area();
                $area->bind($row);
                
                $buffer .= "<tr $class>";
				$buffer .= "<td>".$area->getID()."</td>";										
				$buffer .= "<td>".$area->getDescricao()."</td>";
				$buffer .= "<td>".$area->getObservacao()."</td>";
				$buffer .="<td><input type='checkbox' name='areas[]' value='".$area->getID()."' " . $this->getController()->obterAreasCadastradas($idNoticia, $area->getID()). " ></td>";	
				$buffer .= "</tr>";					
				if($i%2 == 1) {
					$class = "";
				} else {
					$class = "class=\"odd\"";		
				}				
				$i++;
			}

        } else {
            $buffer .= produce_icon("cross.png")."&nbsp;&nbsp;Nenhuma área encontrada";
        }
		$buffer .= "</tbody>
					<tfoot>
						<tr>
							<td colspan=6></td>
						</tr>
					</tfoot>
					</table>						
					<br>";
		$buffer .= "<input type='submit'  name='button' id='button' value='Atualizar'/>";
        $buffer .= "</form>";
        
	    return $buffer;
	}
    
    /**
     * Monta HTML com a listagem de áreas separadas por clientes e áreas de colaboração.
     * Caso conexão seja por meio de API cria-se campos tipo hidden.
     * Caso seja passado o parâmetro usuario, a função retorna os clientes do usuário informado.
     * Caso não seja passado o parâmetro usuario, a função retorna os clientes do usuário atualmente logado no sistema.
     * André Alves - 27/01/2012
     * 
     * @param int $usuario Id do usuário
     * @return string Lista com os clientes
     */
    function listarAreas($idNoticia, $usuario = '') {
        // var_dump($this->getController()->obterClientesDoUsuario()) or die();
        $lista_clientes = $this->getController()->obterClientesDoUsuario();
        
        $url = "index.php?".PARAMETER_NAME_ACTION."=save&";
		$url .= PARAMETER_NAME_FILE."=alterar_areas_noticia";
		$buffer = "<form name='fomularioAreaNoticia' action='$url' target='_self' method='post' onsubmit='return confirm(\"Confirma atualização das áreas?\")'>";
		$buffer .= "<input type='hidden' name='noticia_id' id='noticia_id' value='".$idNoticia."'/>";        
        
        
        if(checkapi()) {
            
            $lista_areas = $this->getController()->obterAreasDoUsuario();
            foreach($lista_areas->Rows() as $area) {
                $buffer .="<input type='hidden' name='areas[]' value='".$area->area_id."' />";    
            }
            
        } else {
            
            if($lista_clientes != null) {
                foreach($lista_clientes->Rows() as $cliente) {
                    $lista_areas = $this->getController()->obterAreasDoUsuario($cliente->CLIE_Codigo);
                    
                    $buffer .= '<table class="formatted">
        			            <thead>
        							<tr>
        								<th width="15">&nbsp;</th>
                                        <th>'.ucwords(strtolower(str_replace('zzz','',$cliente->CLIE_Nome))).'</th>
             			            </tr>
        						</thead>
        						<tbody>';
                                
                                if($lista_areas != null) {
                                    foreach($lista_areas->Rows() as $area) { 
                                        $buffer .= '<tr>
                                                        <td><input type="checkbox" name="area[]" class="area[]"'.$this->getController()->obterAreasCadastradas($idNoticia, $area->area_id).'value="'.$area->area_id.'" /></td>
                                                        <td>'.$area->are_descricao.'</td>
                                                    </tr>';
                                    }	
                                } else {
                                    $buffer .= '    <tr>
                                                        <td></td>
                                                        <td>';
                                    $buffer .= translate("Nenhum registro encontrado");
                                    $buffer .="         </td>
                                                    </tr>";
                                }   
                                
                    $buffer.='  </tbody>
                              </table>';
                    
                }
            } else {
                $buffer .= translate("Nenhum registro encontrado");
            }
            
        }
        
        $buffer .= "<input type='submit'  name='button' id='button' value='Atualizar'/>";
        $buffer .= "</form>";
        
        return $buffer.'<br /><br />';        
    
    }
	
}
?>
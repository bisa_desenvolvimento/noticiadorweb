<?
class view_noticia extends TView
{
	function show() {
		$view = $this;    	
        require_once(DIR_TEMPLATES."noticia/frm_noticia.php");
	
	}

	public function getId() { 
		if($this->getModel() != null) 
			return $this->getModel()->getId();

		return null;
	}

	public function getAutor_id() { 
		if($this->getModel() != null) 
			return $this->getModel()->getnot_autor_id();

		return null;
	}

	public function getTitulo() { 
		if($this->getModel() != null) 
			return $this->getModel()->getnot_titulo();

		return null;
	}

	public function getData() {
		if($this->getModel() != null)
			return $this->getModel()->getnot_data();

		return null;
	}

  public function getPublicacao() {
		if($this->getModel() != null)
			return $this->getModel()->getnot_publicacao();

		return null;
	}

	public function getValidade() {
		if($this->getModel() != null)
			return $this->getModel()->getnot_validade();

		return null;
	}

	public function getChamada() {
		if($this->getModel() != null)
			return $this->getModel()->getnot_chamada();

		return null;
	}

	public function getTexto() {
		if($this->getModel() != null)
			return $this->getModel()->getnot_texto();

		return null;
	}

	public function getLink() {
		if($this->getModel() != null)
			return $this->getModel()->getnot_link();

		return null;
	}

	public function getAcessos() {
		if($this->getModel() != null)
			return $this->getModel()->getnot_acessos();

		return null;
	}

	public function getThumbnail() {
		if($this->getModel() != null)
			return $this->getModel()->getnot_thumbnail();

		return null;
	}

	public function getLinkAbrir() {
		if($this->getModel() != null){
			 $link = $this->getModel()->getnot_link_abrir();
			 if($link != "noticiador"){
			 	$clientes = $this->getController()->obterNomeCliente($link);

			 	foreach($clientes->Rows() as $cliente){
			 		return $cliente->CLIE_Nome;
			 	}
			 }
		}
		return null;
	}

    /**
     * Monta HTML com a listagem de áreas separadas por clientes e áreas de colaboração.
     * Caso conexão seja por meio de API cria-se campos tipo hidden.
     * Caso seja passado o parâmetro usuario, a função retorna os clientes do usuário informado.
     * Caso não seja passado o parâmetro usuario, a função retorna os clientes do usuário atualmente logado no sistema.
     * André Alves - 27/01/2012
     *
     * @param int $usuario Id do usuário
     * @return string Lista com os clientes
     */
    function listarAreas($usuario = '') {
        $res = controller_seguranca::getInstance()->identificarUsuario();
        $lista_clientes = $this->getController()->obterClientesDoUsuario();
        $buffer = "";

        if(checkapi()) {

            $lista_areas = $this->getController()->obterAreasDoUsuario();
            foreach($lista_areas->Rows() as $area) {
                $buffer .="<input type='hidden' name='areas[]' value='".$area->area_id."' />";
            }

        } else {

            if($lista_clientes != null) {
                foreach($lista_clientes->Rows() as $cliente) {
					if (($res->getPerfil() == '2') && $cliente->CLIE_Nome != "zzzÁREAS DE COLABORAÇÃO") {
                		continue;
                	}
                    $lista_areas = $this->getController()->obterAreasDoUsuario($cliente->CLIE_Codigo);

                    if(isset($_GET['id'])) {
                        $lista_areas_noticia = $this->getController()->obterAreasDaNoticia($_GET['id']);
                    }

                    $buffer .= '<table class="formatted">
        			            <thead>
        							<tr>
        								<th width="15">&nbsp;</th>
                                        <th>'.ucwords(strtolower(str_replace('zzz','',$cliente->CLIE_Nome))).'</th>
             			            </tr>
        						</thead>
        						<tbody>';

                                if($lista_areas != null) {
                                    foreach($lista_areas->Rows() as $area) {
                                        $checked = '';
                                        if(@in_array($area->area_id, $lista_areas_noticia)) {
                                            $checked = 'checked';
                                        }

                                        $buffer .= '<tr>
                                                        <td><input type="checkbox" name="area[]" class="area[]" value="'.$area->area_id.'" '.$checked.' /></td>
                                                        <td>'.translate($area->are_descricao).'</td>
                                                    </tr>';
                                    }
                                } else {
                                    $buffer .= '    <tr>
                                                        <td></td>
                                                        <td>';
                                    $buffer .= translate("Nenhum registro encontrado.");
                                    $buffer .="         </td>
                                                    </tr>";
                                }

                    $buffer.='  </tbody>
                              </table>';

                }
            } else {
                $buffer .= translate("Nenhum registro encontrado");
            }

        }
        
        return $buffer.'<br /><br />';        
    
    }
  
  function listarAreasCadastradas($filtro = null) {
	    $table = $this->getController()->obterAreasDoUsuario();
            $buffer = "";
            
            $buffer .= "<table class=\"formatted\">
					<thead>
						<tr>
							<td>".translate("Código")."</td>
							<td>".translate("Descrição")."</td>
							<td>".translate("Observação")."</td>
							<td><input type='checkbox' id='todos'>&nbsp;&nbsp;".translate("Todas")."</td>
						</tr>
					</thead>
					<tbody>";
            if ($table != null) {
            
                $class = "class=\"odd\"";
        	$i = 0;
            foreach($table->Rows() as $row) { 
                $area = new Area();
                $area->bind($row);
                
                $buffer .= "<tr $class>";
                               
		$buffer .= "<td>".$area->getID()."</td>";										
		$buffer .= "<td>".translate($area->getDescricao())."</td>";
		$buffer .= "<td>".translate($area->getObservacao())."</td>";
                if(checkapi())
	        $buffer .= '<td><input type="hidden" name="area[]" id="area[]" value="'.$area->getID().'" /> </td>';
	        else
                $buffer .= '<td><input type="checkbox" name="area[]" id="area[]" value="'.$area->getID().'" /> </td>';
                $buffer .= "</tr>";					
				if($i%2 == 1) {
					$class = "";
				} else {
					$class = "class=\"odd\"";		
				}
                                $i++;
            }	
			
            
            
            } else {
                $buffer = produce_icon("cross.png")."&nbsp;&nbsp;Nenhum registro encontrado";
              }

              $buffer .= "</tbody>
					<tfoot>
						<tr>
							<td colspan=6></td>
						</tr>
					</tfoot>
					</table>						
					<br><br><br>";
		
                return $buffer;
                
                
                
                /*
                 * 
$buffer .= "<table class=\"formatted\">
					<thead>
						<tr>
							<td>".translate("Código")."</td>
							<td>".translate("Descrição")."</td>
							<td>".translate("Observação")."</td>
							<td>".translate("Incluir?")."</td>
						</tr>
					</thead>
					<tbody>";
		if ($tableAreas != null) {
        	$class = "class=\"odd\"";
        	$i = 0;
            foreach($tableAreas->Rows() as $row) { 
                $area = new Area();
                $area->bind($row);
                
                $buffer .= "<tr $class>";
				$buffer .= "<td>".$area->getID()."</td>";										
				$buffer .= "<td>".$area->getDescricao()."</td>";
				$buffer .= "<td>".$area->getObservacao()."</td>";
				$buffer .="<td><input type='checkbox' name='areas[]' value='".$area->getID()."' " . $this->getController()->obterAreasCadastradas($idNoticia, $area->getID()). " ></td>";	
				$buffer .= "</tr>";					
				if($i%2 == 1) {
					$class = "";
				} else {
					$class = "class=\"odd\"";		
				}				
				$i++;
			}

        } else {
            $buffer .= produce_icon("cross.png")."&nbsp;&nbsp;Nenhuma área encontrada";
        }
		$buffer .= "</tbody>
					<tfoot>
						<tr>
							<td colspan=6></td>
						</tr>
					</tfoot>
					</table>                 * 
                 */
	}

	function linksClientes(){
		$clientes = $this->getController()->obterClientesDoUsuario();
		
		$buffer = "";

		
		

		foreach($clientes->Rows() as $cliente){
			$link=$this->getController()->obterLink($cliente->CLIE_Codigo);
			
			
			foreach($link->Rows() as $l){
				if($l->link_Noticias == null){			
					$buffer = $buffer . '<h1 class="links">null</h1>' ;
					
				}else{
					$buffer = $buffer . '<h1 class="links">' . $l->link_Noticias . '</h1>' ;
					
				}
				$buffer = $buffer . '<h1 class="nomes">' . $cliente->CLIE_Nome . '</h1>' ;
				
			}

		}
		
		return $buffer;
	}

	function listarClientes($usuario = ''){
		$clientes = $this->getController()->obterClientesDoUsuario();
		$buffer = "<option value='noticiador' style='height: 28px;'>NoticiadorWeb</option>";
		$model = $this->getModel();

		if($model == null){
			foreach($clientes->Rows() as $cliente){
				$link = $this->getController()->obterLink($cliente->CLIE_Codigo);
				foreach($link->Rows() as $l){
					if($l->link_Noticias == null || $l->link_Noticias == ""){
						$l->link_Noticias = "null";
					}
					$buffer = $buffer . "<option style='height: 28px;' value='".$l->link_Noticias."'>" . $cliente->CLIE_Nome . "</option>";
				}
			}
			return $buffer;
		}else{
			$result = $this->getController()->obterNomeCliente($model->getnot_link_abrir());
			$nome = "";
			foreach($result->Rows() as $r){
				$nome = $r->CLIE_Nome;
			}

			foreach($clientes->Rows() as $cliente){
				$link = $this->getController()->obterLink($cliente->CLIE_Codigo);
				foreach($link->Rows() as $l){
					if($l->link_Noticias == null || $l->link_Noticias == ""){
						$l->link_Noticias = "null";
					}
					if($nome == $cliente->CLIE_Nome){
						$buffer = $buffer . "<option style='height: 28px;' value='".$l->link_Noticias."' selected>" . $cliente->CLIE_Nome . "</option>";
					}else{
						$buffer = $buffer . "<option style='height: 28px;' value='".$l->link_Noticias."'>" . $cliente->CLIE_Nome . "</option>";
					}	
				}
			}
			return $buffer;  
		}
	}

	function listarClientes2($usuario = ''){
		$clientes = $this->getController()->obterClientesDoUsuario();
		$buffer = "<option value='noticiador' style='height: 28px;'>NoticiadorWeb</option>";

		foreach($clientes->Rows() as $cliente){
			$link = $this->getController()->obterLink($cliente->CLIE_Codigo);
			foreach($link->Rows() as $l){
				if($l->link_Noticias == null || $l->link_Noticias == ""){
					$l->link_Noticias = "null";
				}
				$buffer = $buffer . "<option style='height: 28px;' value='".$l->link_Noticias."'>" . $cliente->CLIE_Nome . "</option>";
			}
		}
		return $buffer;
	}


}
?>
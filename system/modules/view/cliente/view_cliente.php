<?
class view_cliente extends TView
{
	function show() {
		$view = $this;    	
        require_once(DIR_TEMPLATES."cliente/frm_cliente.php");
	
	}

	public function getId() { 
		if($this->getModel() != null) 
			return $this->getModel()->getId();

		return null;
	}

	public function getNome() { 
		if($this->getModel() != null) 
			return $this->getModel()->getCli_nome();

		return null;
	}

	public function getCodigo() { 
		if($this->getModel() != null) 
			return $this->getModel()->getCli_codigo();

		return null;
	}

	public function getContrato() { 
		if($this->getModel() != null) 
			return $this->getModel()->getCli_contrato();

		return null;
	}

	public function getSite() { 
		if($this->getModel() != null) 
			return $this->getModel()->getCli_site();

		return null;
	}
	
	public function getEditores(){
	
		$table_editores = $this->getController()->obterEditores();
		
		if ($table_editores != null){				
			$buffer= "<table>";
			for($j = 0; $j < $table_editores->RowCount(); $j++) {
				$row_editor = $table_editores->getRow($j);
				if($j%2 == 0)
					$buffer .= "<tr>";
				$buffer .="<td><input type='checkbox' name='emails[]' value='".$row_editor->usuario_id."' >". $row_editor->usu_nome."</td>";				
				if($j%2 == 1)
					$buffer .= "</tr>";
			}
			$buffer .= "</table>";
		}
		
		return $buffer;
	
	}

}
?>
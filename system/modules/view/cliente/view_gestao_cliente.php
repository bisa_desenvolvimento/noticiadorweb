<?
class View_gestao_cliente extends TView
{
	function show() { 
		$view = $this;
		require_once(DIR_TEMPLATES."cliente/frm_cliente.php");
	}
    
    function showGestao() { 
		$view = $this;
		require_once(DIR_TEMPLATES."cliente/frm_gestao_cliente.php");
	}



    function listarClientes() {
	    $table = $this->getController()->obterClientes();
		
        $buffer = '<table class="formatted" id="paginacaoTable">
			            <thead>
							<tr>
								<th>&nbsp;</th>
                                <th>'.translate("Cliente").'</th>
     			            </tr>
						</thead>
						<tbody>';
     	
         if ($table != null) {

            foreach($table->Rows() as $row) { 
                
                $buffer .= '<tr>
                                <td><input type="radio" name="id" value="'.$row->CLIE_Codigo.'"></td>
                                <td>'.translate($row->CLIE_Nome).'</td>
                            </tr>';
		
            }	

            $buffer.="  </tbody>
                     </table>";

            return $buffer;
        } else {
            $buffer = produce_icon("cross.png")."&nbsp;&nbsp;".translate("Nenhum registro encontrado");
        }

	    return $buffer;
	}
    
    function listarAreas($tipo, $cliente = '') {
        
        switch($tipo){ 
        	case 'T':
                $titulo = '�reas';
                $table = $this->getController()->obterAreas();
                $checked = '';
                $paginacao = '';
        	break;
        
        	case 'A':
                $titulo = '�reas Associadas';
                $table = $this->getController()->obterAreasAssociadas($cliente);
                $checked = 'checked="checked"';
                $paginacao = ''; 
        	break;
        
        	case 'N':
                $titulo = '�reas N�o Associadas';
                $table = $this->getController()->obterAreasNaoAssociadas($cliente);
                $checked = '';
                $paginacao = 'id="tabila"';
        	break;

        }

        	    
        $buffer = '<table class="formatted" '.$paginacao.'>
			            <thead>
							<tr>
								<th width="20">&nbsp;</th>
                                <th>'.translate($titulo).'</th>
     			            </tr>
						</thead>
						<tbody>';
     	
         if ($table != null) {
            foreach($table->Rows() as $row) { 
                $buffer .= '<tr>
                                <td><input type="checkbox" name="area" class="area" '.$checked.' value="'.$row->area_id.'"></td>
                                <td>'.translate($row->are_descricao).'</td>
                            </tr>';
		
            }	
        } else {
            $buffer .= '    <tr>
                                <td></td>
                                <td>';
            $buffer .= translate("Nenhum registro encontrado");
            $buffer .="         </td>
                            </tr>";
        }
        
        $buffer .="      </tbody>
                     </table>";
	    return $buffer;
	}

    function pegarLink($id){

        return $this->getController()->pegarLinkCliente($id);
        
    }
     
}
?>
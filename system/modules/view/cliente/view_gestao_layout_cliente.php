<?
class View_gestao_layout_cliente extends View_gestao_cliente {

    function show() { 
		$view = $this;
		require_once(DIR_TEMPLATES."cliente/frm_cliente.php");
	}
    
    function showLayout() { 
		$view = $this;
		require_once(DIR_TEMPLATES."cliente/frm_gestao_layout_cliente.php");
	}
    
    function getCodigo($cli) {
        return $this->getController()->getCodigo($cli);
    }
    
    public function montarTabela() {
        $buffer = '<table style="display:none">
                       <thead>
                           <tr>
                               <td></td>
                               <th scope="col"></th>
                           </tr>
                        </thead>
                        <tbody>';
        $table = $this->getController()->obterAreas();
        for($i = 0; $i < $table->RowCount(); $i++) {
            $row = $table->getRow($i);
        
            $buffer .= '    <tr>
                                <th scope="row">'.$row->are_descricao.'</th>
                                <td>'.$this->getController()->obterQuantidadeEmails($row->area_id).'</td>
                            </tr>';
        }
        
        $buffer .= '        </tbody>
                    </table>';
        
        return $buffer;    
    }
  
}
?>
<?
class View_sugestao extends view_noticia {

    function show() {
        $view = $this;
        require_once(DIR_TEMPLATES."sugestao/frm_sugestao.php");
    }
    
    function listarAreas($usuario = '') {
        
        $lista_clientes = $this->getController()->obterClientesDoUsuario($usuario);
        $buffer = "";
        $buffer .= '<br /><br /><label>'.translate("&Aacute;reas").':</label>
                    <p>'.translate("Selecione abaixo em quais &Aacute;reas voc&ecirc; deseja inserir esta not&iacute;cia.").'</p>';
            
            if($lista_clientes != null) {
                foreach($lista_clientes->Rows() as $cliente) {
                    $lista_areas = $this->getController()->obterAreasDoUsuario($cliente->CLIE_Codigo,$usuario);
                    
                    $buffer .= '<table class="formatted">
        			            <thead>
        							<tr>
        								<th width="15">&nbsp;</th>
                                        <th>'.ucwords(strtolower(str_replace('zzz','',$cliente->CLIE_Nome))).'</th>
             			            </tr>
        						</thead>
        						<tbody>';
                                
                                if($lista_areas != null) {
                                    foreach($lista_areas->Rows() as $area) { 
                                        $buffer .= '<tr>
                                                        <td><input type="checkbox" name="area[]" class="area[]" value="'.$area->area_id.'" /></td>
                                                        <td>'.$area->are_descricao.'</td>
                                                    </tr>';
                                    }	
                                } else {
                                    $buffer .= '    <tr>
                                                        <td></td>
                                                        <td>';
                                    $buffer .= translate("Nenhum registro encontrado");
                                    $buffer .="         </td>
                                                    </tr>";
                                }   
                                
                    $buffer.='  </tbody>
                              </table>';
                    
                }
            } else {
                $buffer .= translate("Nenhum registro encontrado");
            }
            
            $buffer .= '<input type="hidden" name="autor" id="autor" value="'.$usuario.'" />'; 
            $buffer .= '<input type="submit" value="" class="botaoEnviar" id="enviar" />';
        
        return $buffer.'<br />';        
    
    }
    
    function busca() {
        
        error_reporting(E_ALL);
        
        $login = 'sugestaopauta@bisa.com.br';
        $senha = 'tecnologi@';
        
        $str_conexao = '{imap.gmail.com:993/imap/ssl}';
        if (!extension_loaded('imap')) {
            die('Modulo PHP/IMAP nao foi carregado');
        }
        
        // Abrindo conexao
        $mbox = imap_open($str_conexao, $login, $senha);
        if (!$mbox) {
            die('Erro ao conectar: '.imap_last_error());
        }
        
        $imap = imap_check($mbox);
        $size = $imap->Nmsgs;
        $emails = array();
        
        if($size) {
        
            for($i=$size; $i>0; $i--) {
                $overview = imap_fetch_overview($mbox, $i);
                if($overview[0]->seen) {
                    break;
                }
                $emails[] = $i; 
            }
            
            krsort($emails);
            
            foreach($emails as $msgno) {
                
                $noticia = array();                
                $overview = imap_fetch_overview($mbox, $msgno);
                
                if(strpos($overview[0]->from,'<')) {
                    $from = explode('<',$overview[0]->from);
                    $from = explode('>',$from[1]);
                    $noticia['autor'] = $from[0];
                } else {
                    $noticia['autor'] = $overview[0]->from;    
                }
                
                $noticia['titulo'] = addslashes($this->corrige($overview[0]->subject));
                
                $noticia['publicacao'] = date('Y-m-d');
                if(date('m') > 7) {
                    $noticia['limite'] = date('Y-12-31',strtotime("+1 year"));    
                } else {
                    $noticia['limite'] = date('Y-12-31');
                }
                        
                $dataTxt = $this->get_part($mbox, $msgno, "TEXT/PLAIN"); // GET TEXT BODY
                $dataHtml = $this->get_part($mbox, $msgno, "TEXT/HTML"); // GET HTML BODY
                
                if ($dataHtml != "") {
                    $mensagem = $dataHtml;
                } else {
                    $mensagem = '<html><head><title>Messagebody</title></head><body bgcolor="white">'.preg_replace("/\n/","<br>",$dataTxt).'</body></html>';
                }
                
                if(strpos($mensagem,']]')) {
                    $mensagem = explode(']]',$mensagem);
                    $noticia['resumo'] = str_replace('<br>',' ',addslashes(str_replace('[[','',$mensagem[0])));
                    $noticia['noticia'] = addslashes($mensagem[1]);   
                } else {
                    $noticia['resumo'] = '';
                    $noticia['noticia'] = addslashes($mensagem);
                }
                
                $noticia_id = $this->getController()->inserirNoticia($noticia);
                
                //
                //ENVIAR EMAIL INFORMANDO A ADI��O DA NOT�CIA
                //
                
                $to = $noticia['autor'];
                $subject = "Not�cia pr�-cadastrada no Noticiador Web";
                $message = 'Identificamos o cadastro da not�cia <strong>'.$noticia['titulo'].'</strong> no nosso email.<br /><br />
                            Para autorizar a mesma e selecionar as �reas de interesse por favor clique no link abaixo:<br /><br />
                            <a href="'.URL.'index.php?action=load&secao=sugestao&id='.$noticia_id.'&email='.$noticia['autor'].'">'.URL.'index.php?action=load&secao=sugestao&id='.$noticia_id.'</a><br /><br />
                            Noticiador Web<br /><br />
                            <small><em>Essa � uma mensagem autom�tica n�o responda.</em></small>';
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $rpath = 'no-reply@' . STMP_HOST;
                imap_mail($to,$subject,$message,$headers,'','',$rpath);
                echo $msgno.'. '.$noticia['titulo'].'<br />';

            }
        
        }
        
        die('<br />--<br />fim');
        
    }
    
    private function corrige($subject) {
        
        $subjecto=imap_mime_header_decode($subject);
        $k=0;$sub='';$control=0;
        while(@$subjecto[$k]->text!=''){            
            if($subjecto[$k]->charset=="utf-8"){                
                $sub.=utf8_decode($subjecto[$k]->text);
                $k++;
            }else{
                $sub.=$subjecto[$k]->text;
                $k++;   
            }
        }
        
        return $sub;
    }
    
    private function get_mime_type(&$structure) {
       $primary_mime_type = array("TEXT", "MULTIPART","MESSAGE", "APPLICATION", "AUDIO","IMAGE", "VIDEO", "OTHER");
       if($structure->subtype) {
       	return $primary_mime_type[(int) $structure->type] . '/' .$structure->subtype;
       }
       	return "TEXT/PLAIN";
    }
       
    private function get_part($stream, $msg_number, $mime_type, $structure = false,$part_number    = false) {
       
       	if(!$structure) {
       		$structure = imap_fetchstructure($stream, $msg_number);
       	}
       	if($structure) {
       		if($mime_type == $this->get_mime_type($structure)) {
       			if(!$part_number) {
       				$part_number = "1";
       			}
       			$text = imap_fetchbody($stream, $msg_number, $part_number);
       			if($structure->encoding == 3) {
       				return imap_base64($text);
       			} else if($structure->encoding == 4) {
       				return imap_qprint($text);
       			} else {
       			return $text;
       		}
       	}
            $prefix = "";
    		if($structure->type == 1) /* multipart */ {
       		while(list($index, $sub_structure) = each($structure->parts)) {
       			if($part_number) {
       				$prefix = $part_number . '.';
       			}
       			$data = $this->get_part($stream, $msg_number, $mime_type, $sub_structure,$prefix .    ($index + 1));
       			if($data) {
       				return $data;
       			}
       		} // END OF WHILE
       		} // END OF MULTIPART
       	} // END OF STRUTURE
       	return false;
    } // END OF FUNCTION
    
}
?>
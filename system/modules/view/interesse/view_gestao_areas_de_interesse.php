<?
class View_gestao_areas_de_interesse extends TView
{
	function show() { 
		$view = $this;
		require_once(DIR_TEMPLATES."interesse/frm_area_interesse.php");
	}
	
	function listarEditores() {
		$table = $this->getController()->obterListaEditores();
		$buffer = "";
		if ($table != null) {
		    //$buffer .= "<ol class=\"tagol\">\n";
            $buffer .= "<table id='paginacaoTable'><thead></thead><tbody>\n";
		    for($i = 0; $i < $table->RowCount(); $i++) {
                $row = $table->getRow($i);
		    	$name = "cbUsuario_".$row->usuario_id;
		    	$checked = "";
		    	/*if (!empty($row->area_id))
		    		$checked = "checked=\"true\"";*/
		    		
		    	$checkbox = "<input type=\"checkbox\" name=\"$name\" id=\"$name\" value=\"$row->usuario_id\" $checked/>\n";
		        //$buffer .= "<li class=\"tagli\">\n";
               $buffer .= "<tr><td>\n";
		        $buffer .= $checkbox."<label for=\"$name\" style='display: inline'>".$row->usu_nome."</label>";
		        //$buffer .= "</li>\n";
                $buffer .= "</td></tr>\n";
		    }
		    //$buffer .= "</ol>\n";
            $buffer .= "</tbody></table>\n";
            
            $buffer .= '<div id="pager" class="pager">
                    	<form>
                    		<img src="images/first.png" class="first"/>
                    		<img src="images/prev.png" class="prev"/>
                    		<input type="text" class="pagedisplay"/>
                    		<img src="images/next.png" class="next"/>
                    		<img src="images/last.png" class="last"/>
                    		<select class="pagesize">
                    			<option selected="selected"  value="25">25</option>
                    
                    			<option value="50">50</option>
                    			<option value="75">75</option>
                    			<option  value="100">100</option>
                    		</select>
                    	</form>
                        </div>';
		}
		
	    return $buffer;
	}
	
	function listarAreasCadastradas($filtro = null) {
	    $table = $this->getController()->obterAreasEditorDoUsuario();
		
		$buffer = "<table class='formatted'>
			            <thead>
							<tr>
								<td>".translate("�rea")."</td>
								<td>".translate("Opera��o")."</td>
                                			</tr>
						</thead>
						<tbody>";
        	$class = "class=\"odd\"";
		if ($table != null) {
            foreach($table->Rows() as $row) { 
                $area = new Area();
                $area->bind($row);

                $link = produce_link(null, produce_icon("page_edit.png"), array("class"=>"linkCarregar"), "load", null, $area->getID());
                $link_remove = produce_link(null, produce_icon("delete.png"), array("class"=>"linkApagar", "OnClick" => "return confirm('".translate("Deseja realmente excluir este registro ?")."')"), "delete", null, $area->getID());
                $buffer .= "<tr $class>
		<td>".$area->getDescricao()."</td>
                <td>".$link_remove."&nbsp;&nbsp;".$link."</td>
					
		            </tr>";
		
            }	
            $buffer.="</tbody></table>";
            return $buffer;
        } else {
            $buffer = produce_icon("cross.png")."&nbsp;&nbsp;Nenhum registro encontrado";
        }
		
	    return $buffer;
	}
    
    
    /**
     * Retorna html contendo uma tabela com a listagem dos clientes
     * Andr� Alves - 24/01/2011
     * 
     * @param int $cliente Id do modelo caso esteja sendo editado
     * @return string Tabela com a listagem dos clientes
     */
    function listarClientes($cliente = '') {
	    $table = $this->getController()->obterClientes();        
		
        $buffer = '<table class="formatted" id="paginacaoTable">
			            <thead>
							<tr>
								<td>&nbsp;</td>
                                <td>'.translate("Cliente").'</td>
     			            </tr>
						</thead>
						<tbody>';
     	
         if ($table != null) {

            foreach($table->Rows() as $row) { 
                
                $checked = '';
                if($row->CLIE_Codigo == $cliente) {
                    $checked = 'checked="checked"';
                }
                
                $buffer .= '<tr>
                                <td><input type="radio" name="radCliente" class="radCliente" '.$checked .' value="'.$row->CLIE_Codigo.'"></td>
                                <td>'.$row->CLIE_Nome.'</td>
                            </tr>';
		
            }	

            $buffer.="  </tbody>
                     </table>";

            return $buffer;
        } else {
            $buffer = produce_icon("cross.png")."&nbsp;&nbsp;".translate("Nenhum registro encontrado");
        }

	    return $buffer;
	}
    
    /**
     * Monta HTML com a listagem de �reas separadas por clientes e �reas de colabora��o.
     * Caso conex�o seja por meio de API cria-se campos tipo hidden.
     * Caso seja passado o par�metro usuario, a fun��o retorna os clientes do usu�rio informado.
     * Caso n�o seja passado o par�metro usuario, a fun��o retorna os clientes do usu�rio atualmente logado no sistema.
     * Andr� Alves - 27/01/2012
     * 
     * @param int $usuario Id do usu�rio
     * @return string Lista com os clientes
     */
    function listarAreas($usuario = '') {
        
        $lista_clientes = $this->getController()->obterClientesDoUsuario();
        $buffer = "";
        
        if(checkapi()) {
            
            $lista_areas = $this->getController()->obterAreasDoUsuario();
            foreach($lista_areas->Rows() as $area) {
                $buffer .="<input type='hidden' name='areas[]' value='".$area->area_id."' />";    
            }
            
        } else {
            
            if($lista_clientes != null) {
                foreach($lista_clientes->Rows() as $cliente) {
                    
                    if($cliente->CLIE_Codigo != 'COLAB') {
                        $lista_areas = $this->getController()->obterAreasDoUsuario($cliente->CLIE_Codigo);
                        
                        $buffer .= '<table class="formatted">
            			            <thead>
            							<tr>
            								<th>'.ucwords(strtolower(str_replace('zzz','',$cliente->CLIE_Nome))).'</th>
                                            <th width="40">&nbsp;</th>
                 			            </tr>
            						</thead>
            						<tbody>';
                                    
                                    if($lista_areas != null) {
                                        foreach($lista_areas->Rows() as $area) { 
                                            $link = produce_link(null, produce_icon("page_edit.png"), array("class"=>"linkCarregar"), "load", null, $area->area_id);
                                            $link_remove = produce_link(null, produce_icon("delete.png"), array("class"=>"linkApagar", "OnClick" => "return confirm('".translate("Deseja realmente excluir este registro ?")."')"), "delete", null, $area->area_id);
                                            $buffer .= '<tr>
                                                            <td>'.$area->are_descricao.'</td>
                                                            <td>'.$link_remove.'&nbsp;&nbsp;'.$link.'</td>
                                                            
                                                        </tr>';
                                        }	
                                    } else {
                                        $buffer .= '    <tr>
                                                            <td></td>
                                                            <td>';
                                        $buffer .= translate("Nenhum registro encontrado");
                                        $buffer .="         </td>
                                                        </tr>";
                                    }   
                                    
                        $buffer.='  </tbody>
                                  </table>';
                    }
                    
                }
            } else {
                $buffer .= translate("Nenhum registro encontrado");
            }
            
        }
        
        return $buffer.'<br /><br />';        
    
    }
    
	
	function getDescricao() {
	    if ($this->getModel() != null) {
            return $this->getModel()->getDescricao();
        }
	}
	
	function getObservacao() {
	    if ($this->getModel() != null) {
            return $this->getModel()->getObservacao();
        }
	}
    
     /* Jorge Roberto Mantis 5941*/
    function getCliente() {
        if ($this->getModel() != null) {
            return $this->getModel()->getCliente();            
        }
    }
}
?>
<?

class View_gestao_colaborador extends TView {

    function show() {
        $view=$this;
        require_once(DIR_TEMPLATES."colaborador/frm_gestao_colaborador.php");
    }

    public function atualizarUsuario($usuario_id, $clientes_associados, $clientes_nao_associados, $perfil_id, $permissoes) {
        
		$this->getController()->atualizarClientes($usuario_id, $clientes_associados, $clientes_nao_associados);
		$this->getController()->atualizarPerfil($usuario_id, $perfil_id);

		if($perfil_id == '4') {
			$this->getController()->atualizarPermissoes($usuario_id, $permissoes);    
		}

		echo msg_alert("Usuario atualizado com sucesso!");
		echo redirect("index.php?action=show&secao=gestao_colaborador");

    }

	public function apagarUsuario($usuario_id) {
		$this->getController()->excluirUsuario($usuario_id);
		echo msg_alert("Usuario atualizado com sucesso!");
		echo redirect("index.php?action=show&secao=gestao_colaborador");
	}

	public function getPerfilUsuario($usuario_id, $perfil_id) { 
	  return $this->getController()->getPerfilUsuario($usuario_id, $perfil_id);
	}

	public function montaComboPerfil($usuario_id) { 
		$combo = "";
		$table_perfil = $this->getController()->obterComboPerfis();
		$combo = "<select name='FPerfil_id' id='FPerfil_id'>";
		if ($table_perfil != null) {     
			for($j = 0; $j < $table_perfil->RowCount(); $j++) {
				$row_perfil = $table_perfil->getRow($j);
				$combo .= "<option value='".$row_perfil->perfil_id."' ".$this->getPerfilUsuario($usuario_id,$row_perfil->perfil_id).">".$row_perfil->per_descricao ."</option>";
			}
		}                       
		$combo .= "</select>";
		return $combo;
	}

    function listarAreasNaoSelecionadas($filtro = null) {
        $table = $this->getController()->obterAreasNaoSelecionadas($filtro);
        $buffer = "";
        if ($table != null) {
            foreach ($table->Rows() as $row) {
                $area = new Area();
                $area->bind($row);
                $buffer .= '<tr>';
                $buffer .= '<td><input type="checkbox" name="area[]"  value="' . $area->getID() . '">'. $area->getDescricao() . '</td>';
                $buffer .= '</tr>';
            }
            
            return $buffer;
        } else {
            $buffer = produce_icon("cross.png") . "&nbsp;&nbsp;Nenhum registro encontrado";
        }

        return $buffer;
    }
    
    
    function listarClientes($tipo, $usuario = '') {
        
        switch($tipo){ 
        	case 'A':
                $titulo = 'Clientes Associados';
                $table = $this->getController()->obterClientesAssociados($usuario);
				$nome_input = "associado";
                $checked = 'checked="checked"';
                $paginacao = ''; 
        	break;
        
        	case 'N':
                $titulo = 'Clientes Não Associados';
                $table = $this->getController()->obterClientesNaoAssociados($usuario);
				$nome_input = "nao_associado";
                $checked = '';
                $paginacao = 'id="paginacaoTable"';
        	break;
        }
        	    
        $buffer = '<table class="formatted" '.$paginacao.'>
			            <thead>
							<tr>
								<td width="20">&nbsp;</td>
                                <td>'.translate($titulo).'</td>
     			            </tr>
						</thead>
						<tbody>';
     	$class = 'class="odd"';

        if ($table != null) {
            foreach($table->Rows() as $row) { 
                $buffer .= '<tr '.$class.'>
                                <td><input type="checkbox" name="cliente_'.$nome_input.'" class="cliente_'.$nome_input.'" '.$checked.' value="'.$row->CLIE_Codigo.'"></td>
                                <td>'.$row->CLIE_Nome.'</td>
                            </tr>';

            }
        } else {
            $buffer .= '    <tr>
                                <td></td>
                                <td>';
            $buffer .= translate("Nenhum registro encontrado");
            $buffer .="         </td>
                            </tr>";
        }

        $buffer .="      </tbody>
                     </table>";
	    return $buffer;
	}

    function listarUsuarios($perfil = "") {
        
        $table = $this->getController()->obterUsuarios();
        $buffer = '<table class="formatted" id="paginacaoTable">
			            <thead>
							<tr>
								<td>'.translate('Nome').'</td>
                                <td>'.translate('Email').'</td>';
                                
        if(!isset($perfil)) {
            $buffer .= '        <td>'.translate('Perfil').'</td>'; 
        } 
 
        $buffer .= '             <td>&nbsp;</td>
     			            </tr>
						</thead>
						<tbody>';
     	$class = 'class="odd"';

        if ($table != null) {
            foreach($table->Rows() as $row) { 
                $buffer .= '<tr '.$class.'>
                                <td>'.$row->usu_nome.'</td>
                                <td>'.$row->usu_email.'
                                    <input type="hidden" value="'.$row->usu_email.'" id="usuario" />
                                    <input type="hidden" value="'.$row->usu_senha.'" id="senha" />
                                </td>';
                if(!isset($perfil)) {
                    $buffer .= '<td>'.
                                    $this->montaComboPerfil($row->usuario_id).
                                    '<input type="hidden" name="usu_id" id="usu_id" value="'.$row->usuario_id.'" />
                                </td>'; 
                } 
                $buffer .= '    <td align="right">';
                if(isset($perfil)) {
                    $buffer .= '    <a href="index.php?action=show&secao=gestao_colaborador&FUSUA_Email='.$row->usu_email.'&id='.$row->usuario_id.'"><img width="16" height="16" title="Editar" src="images/icons/page_edit.png"></a>'; 
                }
                
                $usuario = controller_seguranca::getInstance()->identificarUsuario();      
                if($usuario->getPerfil() == 1) {
                    $buffer .= '    <a href="javascript:return false"><img width="16" height="16" title="Enviar senha" src="images/icons/key.png" onclick="alert(\'A senha do usuário '.$row->usu_email.' é '.base64_decode($row->usu_senha).'. \')"></a>';    
                }
                
                $buffer .= '        <a href="javascript:confirma('.$row->usuario_id.')"><img width="16" height="16" title="Excluir" src="images/icons/delete.png"></a>
                                </td>
                            </tr>';
            }
        } else {
            $buffer .= '    <tr>
                                <td></td>
                                <td>';
            $buffer .= translate("Nenhum registro encontrado");
            $buffer .="         </td>
                                <td colspan='2'>&nbsp;</td>
                            </tr>";
        }

        $buffer .='      </tbody>
                     </table>';

        if($table != null && count($table->Rows()) > 25) {

			$buffer .='
				<div id="pager" class="pager" >
					 <img src="images/first.png" class="first"/>
					 <img src="images/prev.png" class="prev"/>
					 <input type="text" class="pagedisplay"/>
					 <img src="images/next.png" class="next"/>
					 <img src="images/last.png" class="last"/>
					 <select class="pagesize">
						 <option selected="selected"  value="25">25</option>
						 <option value="50">50</option>
						 <option value="75">75</option>
						 <option  value="100">100</option>
					 </select>
				</div>';
        }

        return $buffer;
	}

}

?>
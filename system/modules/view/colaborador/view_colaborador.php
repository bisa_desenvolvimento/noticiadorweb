<?

class view_colaborador extends TView {

    function show() {
        $view = $this;
        if (isset($_POST['FUSUA_Email'])) {
            require_once(DIR_TEMPLATES . "colaborador/frm_colaborador_areas.php");
        } else {
            require_once(DIR_TEMPLATES . "colaborador/frm_colaborador.php");
        }
    }

    public function getUSUA_Email() {
        if ($this->getModel() != null)
            return $this->getModel()->getUSUA_Email();

        return null;
    }

    public function getUSUA_Nome() {
        if ($this->getModel() != null)
            return $this->getModel()->getUSUA_Nome();

        return null;
    }

    public function getUSUA_Empresa() {
        if ($this->getModel() != null)
            return $this->getModel()->getUSUA_Empresa();

        return null;
    }

    public function getUSUA_Endereco() {
        if ($this->getModel() != null)
            return $this->getModel()->getUSUA_Endereco();

        return null;
    }

    public function getUSUA_Bairro() {
        if ($this->getModel() != null)
            return $this->getModel()->getUSUA_Bairro();

        return null;
    }

    public function getUSUA_Cidade() {
        if ($this->getModel() != null)
            return $this->getModel()->getUSUA_Cidade();

        return null;
    }

    public function getUSUA_CEP() {
        if ($this->getModel() != null)
            return $this->getModel()->getUSUA_CEP();

        return null;
    }

    public function getUSUA_Idioma() {
        if ($this->getModel() != null)
            return $this->getModel()->getUSUA_Idioma();

        return null;
    }

    public function getUSUA_EmailEmpresa() {
        if ($this->getModel() != null)
            return $this->getModel()->getUSUA_EmailEmpresa();

        return null;
    }

    public function getUSUA_Fone() {
        if ($this->getModel() != null)
            return $this->getModel()->getUSUA_Fone();

        return null;
    }

    public function getUSUA_Cargo() {
        if ($this->getModel() != null)
            return $this->getModel()->getUSUA_Cargo();

        return null;
    }

    public function getUSUA_UF() {
        if ($this->getModel() != null)
            return $this->getModel()->getUSUA_UF();
        return null;
    }

    public function getUSUA_ComoSoube() {
        if ($this->getModel() != null)
            return $this->getModel()->getUSUA_ComoSoube();

        return null;
    }

    public function getUSUA_CompSoube() {
        if ($this->getModel() != null)
            return $this->getModel()->getUSUA_CompSoube();

        return null;
    }

    public function getUSUA_FlagEditor() {
        if ($this->getModel() != null)
            return $this->getModel()->geUSUA_FlagEditor();

        return null;
    }

    public function getUSUA_Senha() {
        if ($this->getModel() != null)
            return $this->getModel()->getUSUA_Senha();

        return null;
    }

    public function getUSUA_ComoSoubeOutros() {
        if ($this->getModel() != null)
            return $this->getModel()->getUSUA_ComoSoubeOutros();

        return null;
    }

    public function getUSUA_ComoSoubePOutros() {
        if ($this->getModel() != null)
            return $this->getModel()->getUSUA_ComoSoubePOutros();

        return null;
    }

    public function obterEstados($uf="") {
        $combo = "";
        $estados = $this->getController()->obterEstados();
        if ($estados != null) {
            for ($j = 0; $j < $estados->RowCount(); $j++) {
                $row_data = $estados->getRow($j);
                $combo .= "<option value='" . $row_data->uf_id . "'";
                if ($uf == $row_data->uf_id) {
                    $combo .= " selected";
                }
                $combo .= ">" . $row_data->uf_nome . "</option>";
            }
        }
        echo $combo;
    }

    public function obterAreas() {
        $combo = "<div id='scroll'>";
        $usuario = controller_seguranca::getInstance()->identificarUsuario();

        if ($usuario == NULL) {
            $usuario_id = '0';
        } else {
            $usuario_id = $usuario->getID();
        }

        $table_area = $this->getController()->obterAreas($usuario_id);

        if ($table_area != null) {
            for ($j = 0; $j < $table_area->RowCount(); $j++) {
                $row_area = $table_area->getRow($j);
                $combo .="<input type='checkbox' name='area[]' value='" . $row_area->area_id . "' >" . $row_area->are_descricao . "<br>";
            }
        }
        $combo .= "</div>";
        echo $combo;
    }

    function listarAreasCadastradas($filtro = null) {
        $table = $this->getController()->obterAreasCadastradas($filtro);
        $buffer = "";
        if ($table != null) {
            $buffer = '<table id="paginacaoTable" class="formatted">';
            $buffer .= "<thead>";
            $buffer .= "<tr>";
            $buffer .= "<td>&nbsp;</td>";
            $buffer .= "<td>" . translate("�rea") . "</td>";
            $buffer .= "</tr>";
            $buffer .= "</thead>";
            $buffer .= "<tbody>";

            foreach ($table->Rows() as $row) {
                $area = new Area();
                $area->bind($row);
                //print_r($area);
                $buffer .= '<tr>';
                $buffer .= '<td><input type="checkbox" name="area[]" id="area[]" value="' . $area->getID() . '" /> </td>';
                $buffer .= '<td>' . $area->getDescricao() . '</td>';
                $buffer .= '</tr>';
            }

            $buffer .= "</tbody></table>";

            return $buffer;
        } else {
            $buffer = produce_icon("cross.png") . "&nbsp;&nbsp;Nenhum registro encontrado";
        }

        return $buffer;
    }

    function listarAreasSelecionadas($filtro = null) {
        $table = $this->getController()->obterAreasCadastradas($filtro);
        $buffer = "";
        if ($table != null) {
            $buffer = '<table class="formatted">';
            $buffer .= "<thead>";
            $buffer .= "<tr>";
            $buffer .= "<td>&nbsp;</td>";
            $buffer .= "<td>" . translate("�rea") . "</td>";
            $buffer .= "</tr>";
            $buffer .= "</thead>";
            $buffer .= "<tbody>";

            foreach ($table->Rows() as $row) {
                $area = new Area();
                $area->bind($row);
                //print_r($area);
                $buffer .= '<tr>';
                $buffer .= '<td><input type="checkbox" checked="checked" name="area[]" id="area[]" value="' . $area->getID() . '" /> </td>';
                $buffer .= '<td>' . $area->getDescricao() . '</td>';
                $buffer .= '</tr>';
            }

            $buffer .= "</tbody></table>";

            return $buffer;
        } else {
            $buffer = produce_icon("cross.png") . "&nbsp;&nbsp;Nenhum registro encontrado";
        }

        return $buffer;
    }

    function listarAreasNaoSelecionadas($filtro = null) {
        $table = $this->getController()->obterAreasNaoSelecionadas($filtro);

        $buffer = "";
        if ($table != null) {
            $buffer = '<table id="paginacaoTable" class="formatted">';
            $buffer .= "<thead>";
            $buffer .= "<tr>";
            $buffer .= "<td>&nbsp;</td>";
            $buffer .= "<td>" . translate("�rea") . "</td>";
            $buffer .= "</tr>";
            $buffer .= "</thead>";
            $buffer .= "<tbody>";

            foreach ($table->Rows() as $row) {
                $area = new Area();
                $area->bind($row);
                //print_r($area);
                $buffer .= '<tr>';
                $buffer .= '<td><input type="checkbox" name="area[]" id="area[]" value="' . $area->getID() . '" /> </td>';
                $buffer .= '<td>' . $area->getDescricao() . '</td>';
                $buffer .= '</tr>';
            }

            $buffer .= "</tbody></table>";
                return $buffer;
        } else {
            $buffer = produce_icon("cross.png") . "&nbsp;&nbsp;Nenhum registro encontrado";
        }

        return $buffer;
    }

}

?>
<?
class view_adm_gestao_perfil extends TView {
	function show() {
		$view = $this;
		require_once(DIR_TEMPLATES."system/frm_adm_perfil.php");
	}
	
	function getDescricao() {
		if ($this->getModel() != null)
			return $this->getModel()->getDescricao();
			
		return null;
	}
	
	function obterPerfisCadastrados() {
		$table = obterFullTable("PERFIL");
		
		$buffer = "";
		if ($table != null) {
			$buffer = "<ol class=\"tagol\">";
			foreach($table->Rows() as $row) {
				$perfil = new Perfil();
				$perfil->bind($row);
				
				$link = produce_link(null, produce_icon("page_edit.png")."&nbsp;&nbsp;".$perfil->getDescricao(), null, "load", null, $perfil->getID());
				$link_remove = produce_link(null, produce_icon("delete.png"), array("OnClick" => "return confirm('Deseja realmente excluir este registro ?')"), "delete", null, $perfil->getID());
				 				
				$buffer .= "<li class=\"tagli\">$link_remove&nbsp;&nbsp;$link</li>";
			}
			$buffer .= "</ol>";
		}
		
		return $buffer;
	}
	
	function obterListaArquivosEAcoes() {
		if ($this->getModel() != null)
			$table = $this->getController()->obterListaArquivosEAcoes($this->getModel()->getID());
		else
			$table = $this->getController()->obterListaArquivosEAcoes();
		
		$buffer = "";
		if ($table != null) {
		    $buffer .= "<dl>\n";
		    $last_arquivo_id = -1;
		    for($i = 0; $i < $table->RowCount(); $i++) {
		    	$row = $table->getRow($i);
		    	$name = "cbArquivoAcao[]";
		    	$value = $row->arquivo_id."_".$row->acao_id;
		    	
		    	$checked = "";
		    	$checkedAnonimo = "";
		    	if (!empty($row->perfil_id))
		    		$checked = "checked=\"true\"";
		    	
		    	if ($last_arquivo_id != $row->arquivo_id) {
		    		if ($last_arquivo_id == -1) {
		    		    $buffer .= "<dt class=\"tagdt\">".$row->arq_descricao."</dt>";
		    		    $buffer .= "<dd>";
		    		} else {
		    		    $buffer .= "</dd>";
		    		    $buffer .= "<dt class=\"tagdt\">".$row->arq_descricao."</dt>";
   		    		    $buffer .= "<dd>";
		    		}
		    		
		    		$last_arquivo_id = $row->arquivo_id;
		    	}
		    			
		    	$checkbox = "<input type=\"checkbox\" name=\"$name\" id=\"$name\" value=\"$value\" $checked/>\n";
		    	
		        //$buffer .= "<dd>\n";
		        $buffer .= $checkbox."<label for=\"$name\" class=\"labelListaPerfil\">".$row->aca_descricao."</label><br/>";
		        //$buffer .= "</dd>\n";
		    }
		    $buffer .= "</dl>\n";
		}
		
	    return $buffer;
	}
}
?>
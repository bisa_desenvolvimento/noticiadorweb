<?
class view_codigomenu extends TView {	
	
	public function show() {
		$view = $this;
		require_once(DIR_TEMPLATES."principal.php");
	}
	
	public function obterMenu() {
	    $perfil = $this->getController()->perfilUsuario();  
	    $buffer = "";	    
	    if($perfil != 2) {
		    $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=areamenu";
		    $buffer .= "<li class=\"button\"><a href=".$url."><img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("�rea")."</a></li>";
		    $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=assinantemenu";
		    $buffer .= "<li class=\"button\"><a href=".$url."><img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Assinantes/E-mails")."</a></li>";	    
		    $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=codigomenu";
		    $buffer .= "<li class=\"button\"><a href=".$url."><img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Scripts para Home Pages")."</a></li>";
		    
			    $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=janela";
			    $buffer .= "<li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Gerar C�digo de Janela")."</a></li>";
			    $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=gestao_janela";
			    $buffer .= "<li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Gerenciar C�digo Janela")."</a></li>";
			    $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=assinatura";
			    $buffer .= "<li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Gerar C�digo Assinatura")."</a></li>";
  			    $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=gestao_assinatura";
			    $buffer .= "<li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Gerenciar C�digo Assinatura")."</a></li>";
		    
			$url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=colaboradormenu";
		    $buffer .= "<li class=\"button\"><a href=".$url."><img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Colaborador")."</a></li>";	    
		    $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=newslettermenu";
		    $buffer .= "<li class=\"button\"><a href=".$url."><img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Newsletter")."</a></li>";
	    }
	    $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=noticiamenu";
	    $buffer .= "<li class=\"button\"><a href=".$url."><img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Not�cia")."</a></li>";
	    if($perfil == 1) {
	  	    $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=usuariomenu";
		    $buffer .= "<li class=\"button\"><a href=".$url."><img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Usu�rios")."</a></li>";
	    }
	    
	    
        return $buffer;
	}
}
?>

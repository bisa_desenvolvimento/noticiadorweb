<?
class view_adm_gestao_arquivos extends TView {
	function show() {
		$view = $this;
		require_once(DIR_TEMPLATES."system/frm_adm_arquivos.php");
	}
	
	function getArquivo() {
		if ($this->getModel() != null)
			return $this->getModel()->getArquivo();
			
		return null;
	}
	
	function getDescricao() {
		if ($this->getModel() != null)
			return $this->getModel()->getDescricao();
			
		return null;
	}
	
	function obterListaAntecessores() {
		$table = $this->getController()->obterListaArquivosCadastrados();
		
		$buffer = "";
		$buffer .= "<select name=\"cmdAntecessor\">\n";
		$buffer .= "<option value=\"-1\"> - Nenhum - </option>\n";
		if ($table != null){
			foreach($table->Rows() as $row) {
		    	$checked = "";
		    	if ($this->getModel() != null)
    		    	if ($row->arquivo_id == $this->getModel()->getParent())
    		    		$checked = "checked=\"true\"";
		    	$buffer .= "<option value=\"$row->arquivo_id\">".$row->arq_arquivo." (".$row->arq_descricao.")</option>";
		    }
		}
		$buffer .= "</select>\n";
		
		return $buffer;
	}
	
	function obterListaArquivosCadastrados() {
		$table = obterFullTable("ARQUIVO");
		
		$buffer = "";
		if ($table != null) {
			$buffer = "<ol class=\"tagol\">";
			foreach($table->Rows() as $row) {
				$arquivo = new Arquivo();
				$arquivo->bind($row);
				
				$link = produce_link(null, produce_icon("page_edit.png")."&nbsp;&nbsp;".$arquivo->getDescricao(), null, "load", null, $arquivo->getID());
				$link_remove = produce_link(null, produce_icon("delete.png"), array("OnClick" => "return confirm('Deseja realmente excluir este registro ?')"), "delete", null, $arquivo->getID());
				 				
				$buffer .= "<li class=\"tagli\">$link_remove&nbsp;&nbsp;$link</li>";
			}
			$buffer .= "</ol>";
		}
		
		return $buffer;
	}
	
	function obterListaAcoes() {
		if ($this->getModel() != null)
			$table = $this->getController()->obterListaAcoes($this->getModel()->getID());
		else
			$table = $this->getController()->obterListaAcoes();
		
		$buffer = "";
		if ($table != null) {
		    $buffer .= "<ol class=\"tagol\">\n";
		    for($i = 0; $i < $table->RowCount(); $i++) {
		    	$row = $table->getRow($i);
		    	$name = "cbAcao_".$row->acao_id;
		    	$name = "cbAcao[]";
		    	
		    	$checked = "";
		    	$checkedAnonimo = "";
		    	if (!empty($row->arquivo_id))
		    		$checked = "checked=\"true\"";
		    	
		    			
		    	$checkbox = "<input type=\"checkbox\" name=\"$name\" id=\"$name\" value=\"$row->acao_id\" $checked/>\n";
		    	
		        $buffer .= "<li class=\"tagli\">\n";
		        $buffer .= $checkbox."<label for=\"$name\">".$row->aca_descricao."</label>";
		        $buffer .= "</li>\n";
		    }
		    $buffer .= "</ol>\n";
		}
		
	    return $buffer;
	}
}
?>
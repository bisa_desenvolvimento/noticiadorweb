<?
class view_principal extends TView {    
        
        public function show() {

			$view = $this;
			//print "chamando show<br><br>";

			if(isset($_GET['url'])){ 
				switch($_GET['url']){
					case "2":
						$page = DIR_TEMPLATES.'system/quem_somos.php';
						break;
					case "3":
						$page = DIR_TEMPLATES.'system/produtos.php';
						break;
                    case "4":
						$page = DIR_TEMPLATES.'system/marketing_de_relacionamento.php';
						break;
                    case "5":
						$page = DIR_TEMPLATES.'system/plugin.php';
						break;
                    case "6":
						$page = DIR_TEMPLATES.'system/passo_a_passo_utilizacao.php';
						break;
                    case "7":
            $page = DIR_TEMPLATES.'system/relacao_clientes.php';
            break;
					default:
						$page = "";
						break;
				}
        
				//$controller_arquivo = new controller_arquivo();
				//$controller_arquivo -> setConexao(TConexao::getInstance());
				//$usuario = controller_seguranca :: getInstance() -> identificarUsuario();
			}

			require_once(DIR_TEMPLATES."system/principal.php");
        }

        public function clientes_noticiador() {
          // $conn = mysqli_connect("bd.boletoswebbisa.com.br", "bisaweb", "bisa2000", "bisaweb_registrador");
          // $query = mysqli_query($conn, "SELECT DISTINCT CLIE_Codigo, IF(CLIE_Sigla != '', CLIE_Sigla, CLIE_Nome), CLIE_Nome, CLIE_Logomarca FROM acrwregistrador WHERE SIST_Codigo = 'NWE' AND CLIE_Logomarca IS NOT NULL ORDER BY CLIE_Nome ASC");
          
          $conn = mysqli_connect("mysql.monetaweb.com.br", "moneta", "m0n3t@w3b", "monetaweb_bisaweb_bisaweb");
          $query = mysqli_query($conn, "SELECT DISTINCT clie_codigo, IF(CLIE_Sigla != '', CLIE_Sigla, CLIE_Nome), CLIE_Nome, CLIE_Logomarca FROM vw_acrwregistrador WHERE SIST_Codigo = 'NWE' AND CLIE_Logomarca IS NOT NULL AND (contasVencidas < 1 OR CONT_PrazoCarencia > NOW()) ORDER BY CLIE_Nome ASC");

          $buffer = "";
          while ($row = mysqli_fetch_row($query)) {
            $buffer .= "<div class='container-cliente image' align='center'>"; /*class='container-image'*/
              $buffer .= "<img src='".DIR_TEMPLATES."system/relacao_clientes_logos.php?CLIE_Codigo=".$row[0]."' width='141' height='90' alt='".$row[2]."'>";
              $buffer .= "<div class='title'><hr>".$row[1]."</div>";
            $buffer .= "</div>";

            /* EST� DESFORMATANDO POR CAUSA DA QNTD DE LINHAS QUE S�O DIFERENTES */
          }
          //$buffer .= "<hr>";
          $query = mysqli_query($conn, "SELECT DISTINCT clie_codigo, IF(CLIE_Sigla != '', CLIE_Sigla, CLIE_Nome), CLIE_Logomarca, CLIE_Sigla FROM vw_acrwregistrador WHERE SIST_Codigo = 'NWE' AND CLIE_Logomarca IS NULL AND (contasVencidas < 1 OR CONT_PrazoCarencia > NOW()) ORDER BY CLIE_Nome ASC");

          $buffer .= "<br><br><br>";
          while ($row = mysqli_fetch_row($query)) {
            $buffer .= "<div class='container-cliente no-image' align='center'>";
              $buffer .= "<table width='100%' height='100%'>";
                $buffer .= "<tr valign='middle'><td>";
                  $buffer .= $row[1];
                $buffer .= "</td></tr>";
              $buffer .= "</table>";
            $buffer .= "</div>";
          }
          $buffer .= "<br><br>";

          return $buffer;
        }
        
        public function obterMenu() {
            /*$table = $this->getController()->obterMenu();
            $buffer = "";        
            if ($table != null) {
                for($i = 0; $i < $table->RowCount(); $i++) {
                    $row = $table->getRow($i);
                    $url = "index.php?".PARAMETER_NAME_ACTION."=".$row->aca_descricao."&".PARAMETER_NAME_FILE."=".$row->arq_chave;
                    
                    //$url = produce_link(null, $row->arq_descricao, null, $row->aca_descricao, $row->arq_chave);
                    //produce_link($url, $content, $attributes = null, $action=null, $file=null, $id = null)
                    
                    //$buffer .= "<li>".produce_link($url, $row->arq_descricao)."</li>";
                    $buffer .= "<li class=\"button\"><a href=".$url."><img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".$row->arq_descricao."</a></li>";
                    //<li class="button"><a href="index.php?url=2"><img src="<?=DIR_ICONSarrow_right.png" width="20" height="20" /> Quem somos</a></li>         
                }
            }*/
          $perfil = $this->perfilUsuario();
          if($perfil == 5){
            $perfil = 2;
          }
          $usuario = controller_seguranca::getInstance()->identificarUsuario();
          if($usuario != null)
            $usuario_id = $usuario->getID();
          
          $buffer = "";
          
          $buffer .= "<li class=\"button menpri\"><a href=#><img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Not�cia")."</a></li>";
          
          $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=noticia";
          $buffer .= "<ul class=\"mensec\"><li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Cadastrar Not�cias")."</a></li>";
          $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=gestao_noticia";
          $buffer .= "<li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Gerenciar Not�cias")."</a></li>";
          if($perfil == 5) {
              if($this->getController()->integracaoQualinfo()) {
                $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=importacao_noticia";
                $buffer .= "<li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Importar Not�cias")."</a></li>";			    
              }
            $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=cliente";
            $buffer .= "<li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Cadastrar Cliente p/ Importar Not�cias")."</a></li>";
          }
          
          
          $buffer .= "</ul><li class=\"button menpri\"><a href=#><img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Assinantes/E-mails")."</a></li>";
          $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=assinante";
          $buffer .= "<ul class=\"mensec\"><li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Cadastrar Assinante")."</a></li>";
          
          if($perfil != 2) {
            $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=gestao_assinante"."&pagina=0"."&tipo=listar";
            $buffer .= "<li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Gerenciar Assinantes")."</a></li>";
          }
          
          if($perfil == 1) {
            $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=ajustar_email_assinantes";
            $buffer .= "<li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Ajustar Emails")."</a></li>";
            //$url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=importar_contatos_email";
            //$buffer .= "<li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Importar meus contatos de E-mails")."</a></li>";
       
          }
          
          if($perfil != 2) {
            
            $buffer .= "</ul><li class=\"button menpri\"><a href=#><img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Newsletter")."</a></li>";
            
            $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=gestao_newsletter";
            $buffer .= "<ul class=\"mensec\"><li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Despachar Newsletter")."</a></li>";
            $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=newsletter";
            $buffer .= "<li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Gerenciar Layout de Newsletter")."</a></li>";
            
            $buffer .= "</ul><li class=\"button menpri\"><a href=#><img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("�rea")."</a></li>";
            
            $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=gestao_areas_de_interesse";
            $buffer .= "<ul class=\"mensec\"><li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Gerenciar �reas")."</a></li>";
                
            $buffer .= "</ul><li class=\"button menpri\"><a href=#><img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Scripts para Home Pages")."</a></li>";
            $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=janela";
            $buffer .= "<ul class=\"mensec\"><li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Gerar C�digo de Janela")."</a></li>";
            $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=gestao_janela";
            $buffer .= "<li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Gerenciar C�digo Janela")."</a></li>";
            
            
            $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=janela_livre";
            $buffer .= "<li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Gerar C�digo Janela Livre")."</a></li>";
            $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=gestao_janela_livre";
            $buffer .= "<li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Gerenciar C�digo Janela Livre")."</a></li>";
            
            
            $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=assinatura";
            $buffer .= "<li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Gerar C�digo Assinatura")."</a></li>";
            $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=gestao_assinatura";
            $buffer .= "<li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Gerenciar C�digo Assinatura")."</a></li>";
          
            $buffer .= "</ul><li class=\"button menpri\"><a href=#><img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Usu�rio")."</a></li>";
            $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=colaborador";
            $buffer .= "<ul class=\"mensec\"><li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Cadastrar Usu�rio")."</a></li>";
            $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=gestao_colaborador";
            $buffer .= "<li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Gerenciar Usu�rio")."</a></li>";
          
            $buffer .= "</ul><li class=\"button menpri\"><a href=#><img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Cliente")."</a></li>";
            $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=gestao_cliente";
            $buffer .= "<ul class=\"mensec\"><li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Gerenciar Cliente")."</a></li>";
            //$url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=gestao_layout_cliente";
            //$buffer .= "<li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Gerenciar Layout")."</a></li>";
          
            $buffer .= "</ul><li class=\"button menpri\"><a href=#><img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Estat�sticas")."</a></li>";
            $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=estatistica_email_por_area";
            $buffer .= "<ul class=\"mensec\"><li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Emails por �rea")."</a></li>";           
            $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=gestao_resumo_newsletter";
            $buffer .= "<li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Resumo Newsletter enviadas")."</a></li>";
          
          }
          
          $buffer .= "</ul>";
          
          $url = "index.php?".PARAMETER_NAME_ACTION."=load&".PARAMETER_NAME_FILE."=colaborador&id=".@$usuario_id;
          $buffer .= "<li class=\"button\"><a href=".$url."><img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Dados Cadastrais")."</a></li>";
          
//          if($perfil == 1) {
//            $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=conversor";
//            $buffer .= "<li class=\"button\"><a href=".$url."><img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Conversor")."</a></li>";
//          }
          
//                    $buffer .= "<li class=\"button menpri\"><a href=#><img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />Usu�rios</a></li>";
//                        $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=usuario";
//                        $buffer .= "<ul class=\"mensec\"><li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />Cadastrar Usu�rio</a></li>";
//                        $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=gestao_usuario";
//                        $buffer .= "<li class=\"button\"><a href=".$url.">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />Gerenciar Usu�rios</a></li></ul>";           


          return $buffer;
        }
}
?>

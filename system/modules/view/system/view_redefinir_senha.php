<?
class view_redefinir_senha extends TView
{
	function show() {
		$view = $this;    	
    
    if(isset($_GET['email']) && isset($_GET['cod'])) {
      if ($this->getController()->codigoCorreto($_GET['email'],$_GET['cod'])) { 
        require_once(DIR_TEMPLATES."system/form_redefinir_senha.php");  
      } else {
        require_once(DIR_TEMPLATES."system/codigo_invalido.php");
      }  
    } else {
      require_once(DIR_TEMPLATES."system/form_redefinir_senha.php");
    }
     
	}
 
}
?>
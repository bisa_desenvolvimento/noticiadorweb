<?
class View_gestao_janela_livre extends TView
{
	
	function show() { 
		$view=$this;
		require_once(DIR_TEMPLATES."janela/frm_gestao_janela_livre.php");
	}
	
	 public function montarTabelaCadastrados() {
        $table = $this->getController()->obterTodos();

        $controller_arquivo = new controller_arquivo();
        $controller_arquivo->setConexao(TConexao::getInstance());
		

		$janelas = "";
        if ($table != null) {

			$buffer = "<form name='fomulariojanela' action='#' target='_self' method='post' onsubmit='return confirm(\"Confirma atualizar registros?\")'>";
			$buffer .= "<table class=\"formatted\">
						<thead>
							<tr>
								<td>".translate("C�d")."</td>
                <td>".translate("Nome")."</td>
								<td>".translate("�reas")."</td>
                <td width=40></td>
							</tr>
						</thead>
						<tbody>";		
			$class = "class=\"odd\"";	
            for($i = 0; $i < $table->RowCount(); $i++) {
            	$row = $table->getRow($i);
            	$janelas .= $row->jan_liv_id.",";
				
				$buffer .= "<tr $class>";
//				$buffer .= "<td><input type='checkbox' name='excluir[]' value='".$row->janela_id."'/></td>";
				$url = "index.php?".PARAMETER_NAME_ACTION."=load&";
				$url .= PARAMETER_NAME_FILE."=janela_livre";
				$url .= "&id=".$row->jan_liv_id;
				
            	$buffer .= "<td>".$row->jan_liv_id."</td>";
              $buffer .= "<td>".$row->jan_liv_nome."</td>";
            	$table_area = $this->getController()->obterAreas($row->jan_liv_id);
				$areas = "";
				if ($table_area != null){				
					for($j = 0; $j < $table_area->RowCount(); $j++) {
						$row_area = $table_area->getRow($j);
						$areas .= $row_area->are_descricao.", ";						
					}
				}
				if ($areas!=""){
					$areas = substr($areas,0, strlen($areas)-2);
				}
				$buffer .="<td>". $areas ."</td>";
        
        $urlAlteracao = "index.php?".PARAMETER_NAME_ACTION."=load&";
				$urlAlteracao .= PARAMETER_NAME_FILE."=janela_livre";
				$urlAlteracao .= "&id=".$row->jan_liv_id;
        
        $urlExclusao = "index.php?".PARAMETER_NAME_ACTION."=delete&";
				$urlExclusao .= PARAMETER_NAME_FILE."=janela_livre";
				$urlExclusao .= "&id=".$row->jan_liv_id;
        
        $buffer .="<td>
                     <a href=\"$urlAlteracao\"><img src=\"".DIR_ICONS."page_edit.png\" width=\"16\" height=\"16\" title=\"Editar\" /></a>
                     <a href=\"$urlExclusao\"><img src=\"".DIR_ICONS."delete.png\" title=\"Excluir\" /></a>
                   </td>";
        
				$buffer .= "</tr>";
				if($i%2 == 1) {
					$class = "";
				} else {
					$class = "class=\"odd\"";		
				}
            }
            $buffer .= "</tbody>
						<tfoot>
							<tr>
								<td colspan=9></td>
							</tr>
						</tfoot>
						</table>						
						<br>";
            $buffer .= "</form>";
		
            
            return $buffer;
        } else {
            return "Nenhum registro encontrado";
        }
    }
	
}
?>
<?
class view_janela_livre extends TView
{
	function show() {
		$view = $this;    	
        require_once(DIR_TEMPLATES."janela/frm_janela_livre.php");	
	}

	public function getId() { 
		if($this->getModel() != null) 
			return $this->getModel()->getId();

		return null;
	}

    public function getjan_nome() { 
		if($this->getModel() != null) 
			return $this->getModel()->getjan_nome();

		return null;
	}
    
    public function getjan_quantidade() { 
		if($this->getModel() != null) 
			return $this->getModel()->getjan_quantidade();

		return null;
	}

	public function geraCodigo($janela_id=""){
		return $this->getController()->geraCodigoJanela($janela_id);
	}

  
  public function obterCodigo() {
    return $this->getController()->obterCodigo();
  }
  
  /**
     * Monta HTML com a listagem de áreas separadas por clientes e áreas de colaboração.
     * Caso conexão seja por meio de API cria-se campos tipo hidden.
     * Caso seja passado o parâmetro usuario, a função retorna os clientes do usuário informado.
     * Caso não seja passado o parâmetro usuario, a função retorna os clientes do usuário atualmente logado no sistema.
     * André Alves - 27/01/2012
     * 
     * @param int $usuario Id do usuário
     * @return string Lista com os clientes
     */
    function listarAreas($janela = '') {
        
        $lista_clientes = $this->getController()->obterClientesDoUsuario();
        $buffer = "";
        
        if(checkapi()) {
            
            $lista_areas = $this->getController()->obterAreasDoUsuario();
            foreach($lista_areas->Rows() as $area) {
                $buffer .="<input type='hidden' name='areas[]' value='".$area->area_id."' />";    
            }
            
        } else {
            
            if($lista_clientes != null) {
                foreach($lista_clientes->Rows() as $cliente) {
                    $lista_areas = $this->getController()->obterAreasDoUsuario($cliente->CLIE_Codigo);
                    
                    $buffer .= '<table class="formatted">
        			            <thead>
        							<tr>
        								<th width="15">&nbsp;</th>
                                        <th class="cliente">'.ucwords(strtolower(str_replace('zzz','',$cliente->CLIE_Nome))).'</th>
             			            </tr>
        						</thead>
        						<tbody>';
                                
                                if($lista_areas != null) {
                                    foreach($lista_areas->Rows() as $area) { 
                                        $buffer .= '<tr>
                                                        <td><input type="checkbox" name="areas[]" class="area" value="'.$area->area_id.'" '.$this->getController()->checarJanelaArea($janela, $area->area_id).'/></td>
                                                        <td>'.$area->are_descricao.'</td>
                                                    </tr>';
                                    }	
                                } else {
                                    $buffer .= '    <tr>
                                                        <td></td>
                                                        <td>';
                                    $buffer .= translate("Nenhum registro encontrado");
                                    $buffer .="         </td>
                                                    </tr>";
                                }   
                                
                    $buffer.='  </tbody>
                              </table>';
                    
                }
            } else {
                $buffer .= translate("Nenhum registro encontrado");
            }
            
        }
        
        return $buffer.'<br /><br />';        
    
    }

}
?>
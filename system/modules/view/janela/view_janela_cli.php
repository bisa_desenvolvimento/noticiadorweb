<?
class view_janela_cli extends TView
{
  function show() {
  	$view = $this;    	
    require_once(DIR_TEMPLATES."janela/janela_cli.php");
	}
  
  function getDadosJanela($id) {
    return $this->getController()->getDadosJanela($id);
  }
  
  function getNoticias($sql) {
    return $this->getController()->getNoticias($sql);
  }
    
}
?>
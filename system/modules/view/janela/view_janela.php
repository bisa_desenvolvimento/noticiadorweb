<?
class view_janela extends TView
{
	function show() {
		$view = $this;    	
        require_once(DIR_TEMPLATES."janela/frm_janela.php");
	
	}

	public function getId() { 
		if($this->getModel() != null) 
			return $this->getModel()->getId();

		return null;
	}

  public function getjan_nome() { 
		if($this->getModel() != null) 
			return $this->getModel()->getjan_nome();

		return null;
	}

	public function getjan_altura() { 
		if($this->getModel() != null) 
			return $this->getModel()->getjan_altura();

		return null;
	}

	public function getjan_largura() { 
		if($this->getModel() != null) 
			return $this->getModel()->getjan_largura();

		return null;
	}

	public function getjan_cor() { 
		if($this->getModel() != null) 
			return $this->getModel()->getjan_cor();

		return null;
	}

	public function getjan_tipo() { 
		if($this->getModel() != null) 
			return $this->getModel()->getjan_tipo();

		return null;
	}


	public function getjan_quantidade() { 
		if($this->getModel() != null) 
			return $this->getModel()->getjan_quantidade();

		return null;
	}

	public function getjan_exibirApenasTitulo() { 
		if($this->getModel() != null) 
			return $this->getModel()->getjan_exibirApenasTitulo();

		return null;
	}
	
	public function getjan_data() {
		if($this->getModel() != null) 
			return $this->getModel()->getjan_data();

		return null;
	}
  
  public function getjan_borda() {
		if($this->getModel() != null) 
			return $this->getModel()->getjan_borda();

		return null;
	}
	
	public function getjan_scroll() {
		if($this->getModel() != null)
			return $this->getModel()->getjan_scroll();

		return null;
	}
	
	public function getjan_logo() { 
		if($this->getModel() != null) 
			return $this->getModel()->getjan_logo();

		return null;
	}

	
	public function getjan_foto() { 
		if($this->getModel() != null) 
			return $this->getModel()->getjan_foto();

		return null;
	}
		
	public function getjan_indicar() { 
		if($this->getModel() != null) 
			return $this->getModel()->getjan_indicar();

		return null;
	}

	public function getjan_comentar() { 
		if($this->getModel() != null) 
			return $this->getModel()->getjan_comentar();

		return null;
	}

	public function getjan_visualizarComentario() { 
		if($this->getModel() != null) 
			return $this->getModel()->getjan_visualizarComentario();

		return null;
	}

	public function getjan_inserirNoticia() { 
		if($this->getModel() != null) 
			return $this->getModel()->getjan_inserirNoticia();

		return null;
	}	


	public function getAreas($janela_id=""){
	
		$table_area = $this->getController()->obterAreas();
		
		if ($table_area != null){				
			$buffer= "";
			for($j = 0; $j < $table_area->RowCount(); $j++) {
				$row_area = $table_area->getRow($j);
				$buffer .="<input type='checkbox' name='area[]' id='area[]' value='".$row_area->area_id."' ";
				if ($janela_id!="")
					$buffer .= $this->getController()->checarJanelaArea($janela_id, $row_area->area_id);
				$buffer .=" >". $row_area->are_descricao."<br>";				
			}
                return $buffer;
		}else{
                return "Ninguna Area encontrada";
                }
		
		
	
	}
	
	public function geraCodigo($janela_id=""){
		return $this->getController()->geraCodigoJanela($janela_id);
	}
	
	public function obterCores($cor="") { 
		$combo = "";
		
		$combo .= "<option value='blue'";
		if ($cor=="blue"){$combo .= " selected";}
		$combo .= " class='blue'>Azul Royal</option>";
		
		$combo .= "<option value='navy'";
		if ($cor=="navy"){$combo .= " selected";}
		$combo .= " class='navy'>Azul Escuro</option>";			

		$combo .= "<option value='teal'";
		if ($cor=="teal"){$combo .= " selected";}
		$combo .= " class='teal'>Azul</option>";		
						
		$combo .= "<option value='white'";
		if ($cor=="white"){$combo .= " selected";}
		$combo .= " class='white'>Branco</option>";		
		
		$combo .= "<option value='gray'";
		if ($cor=="gray"){$combo .= " selected";}
		$combo .= " class='gray'>Cinza</option>";
		
		$combo .= "<option value='silver'";
		if ($cor=="silver"){$combo .= " selected";}
		$combo .= " class='silver'>Cinza Claro</option>";
		
		$combo .= "<option value='maroon'";
		if ($cor=="maroon"){$combo .= " selected";}
		$combo .= " class='maroon'>Marrom</option>";				
		
		$combo .= "<option value='olive'";
		if ($cor=="olive"){$combo .= " selected";}
		$combo .= " class='olive'>Oliva</option>";	
		
		$combo .= "<option value='black'";
		if ($cor=="black"){$combo .= " selected";}		
		$combo .= " class='black'>Preto</option>";	
			
		$combo .= "<option value='green'";
		if ($cor=="green"){$combo .= " selected";}
		$combo .= " class='green'>Verde</option>";	
					
		echo $combo;
	}
  
  public function obterCodigo() {
    return $this->getController()->obterCodigo();
  }
  
  /**
     * Monta HTML com a listagem de �reas separadas por clientes e �reas de colabora��o.
     * Caso conex�o seja por meio de API cria-se campos tipo hidden.
     * Caso seja passado o par�metro usuario, a fun��o retorna os clientes do usu�rio informado.
     * Caso n�o seja passado o par�metro usuario, a fun��o retorna os clientes do usu�rio atualmente logado no sistema.
     * Andr� Alves - 27/01/2012
     * 
     * @param int $usuario Id do usu�rio
     * @return string Lista com os clientes
     */
    function listarAreas($janela = '') {
        
        $lista_clientes = $this->getController()->obterClientesDoUsuario();
        $buffer = "";
        
        if(checkapi()) {
            
            $lista_areas = $this->getController()->obterAreasDoUsuario();
            foreach($lista_areas->Rows() as $area) {
                $buffer .="<input type='hidden' name='areas[]' value='".$area->area_id."' />";    
            }
            
        } else {
            
            if($lista_clientes != null) {
                foreach($lista_clientes->Rows() as $cliente) {
                    $lista_areas = $this->getController()->obterAreasDoUsuario($cliente->CLIE_Codigo);
                    
                    $buffer .= '<table class="formatted">
        			            <thead>
        							<tr>
        								<th width="15">&nbsp;</th>
                                        <th class="cliente">'.ucwords(strtolower(str_replace('zzz','',$cliente->CLIE_Nome))).'</th>
             			            </tr>
        						</thead>
        						<tbody>';
                                
                                if($lista_areas != null) {
                                    foreach($lista_areas->Rows() as $area) { 
                                        $buffer .= '<tr>
                                                        <td><input type="checkbox" name="areas[]" class="area" value="'.$area->area_id.'" '.$this->getController()->checarJanelaArea($janela, $area->area_id).'/></td>
                                                        <td>'.$area->are_descricao.'</td>
                                                    </tr>';
                                    }	
                                } else {
                                    $buffer .= '    <tr>
                                                        <td></td>
                                                        <td>';
                                    $buffer .= translate("Nenhum registro encontrado");
                                    $buffer .="         </td>
                                                    </tr>";
                                }   
                                
                    $buffer.='  </tbody>
                              </table>';
                    
                }
            } else {
                $buffer .= translate("Nenhum registro encontrado");
            }
            
        }
        
        return $buffer.'<br /><br />';        
    
    }

}
?>
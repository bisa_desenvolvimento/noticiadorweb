<?
class view_newsletter extends TView {
	function show() {
		$view = $this;
		require_once(DIR_TEMPLATES."newsletter/frm_newsletter.php");
	}
	
	function getNome() {
		if ($this->getModel() != null)
			return $this->getModel()->getNome();
			
		return null;
	}
	
	function getDescricao() {
		if ($this->getModel() != null)
			return $this->getModel()->getDescricao();
			
		return null;
	}
        function getRodape(){
            if ($this->getModel() != null)
			return $this->getModel()->getRodape();

		return null;
    }

    function getCliente() {
		if ($this->getModel() != null)
			return $this->getModel()->getCliente();
			
		return null;
	}
	
	function exibirModelosCadastrados() {
		$table = $this->getController()->obterModelosCadastrados();
		
		$buffer = "<table class=\"formatted\">
						<thead>
							<tr>
								<td>".translate("Nome")."</td>
								<td>".translate("Descri��o")."</td>
								<td></td>
							</tr>
						</thead>
						<tbody>";
		
		if ($table != null) {
			//$buffer = "<ol>";			
			$modelo = new Modelo();
			$class = "class=\"odd\"";
			for($i = 0; $i < $table->RowCount(); $i++) {
				$modelo->bind($table->getRow($i));
							
				$link = produce_link(null, produce_icon("page_edit.png"), array("class"=>"linkCarregar"), "load", null, $modelo->getID());
				$link_remove = produce_link(null, produce_icon("delete.png"), array("class"=>"linkApagar", "OnClick" => "return confirm('".translate("Deseja realmente excluir este registro ?")."')"), "delete", null, $modelo->getID());
				
				$buffer .= "<tr $class>";
				$buffer .= "<td>".translate($modelo->getNome())."</td>";										
				$buffer .= "<td>".translate($modelo->getDescricao())."</td>";
				$buffer .= "<td>$link_remove&nbsp;&nbsp;$link</td>";
				$buffer .= "</tr>";	
				 				
				//$buffer .= "<li>$link_remove&nbsp;&nbsp;$link</li>";
				
				if($i%2 == 1) {
					$class = "";
				} else {
					$class = "class=\"odd\"";		
				}
			}	
			
			//$buffer .= "</ol>";
					
		} else {
			return produce_icon("cross.png")."&nbsp;&nbsp;Nenhum registro encontrado";	
		}
		$buffer .= "</tbody>
						<tfoot>
							<tr>
								<td colspan=3></td>
							</tr>
						</tfoot>
						</table>						
						<br>";
		return $buffer;	
	}
        
        function validarModeloExistente(){
            $table=$this->getController()->obterTodosModelosCadastrados();  
            if ($table != null) {
			
			$buffer =array();
			for($i = 0; $i < $table->RowCount(); $i++){
				$row = $table->getRow($i);
				
				$buffer[$i]= $row->mod_nome;
				
			}
			
			return $buffer;
		} else {
			return "Nenhum modelo cadastrado";
		}
        }

        function obterAreasPraModelo($idModelo) {
	    $table = $this->getController()->obterAreasCadastradas();
            $buffer = "";

            $buffer .= "<table class=\"formatted\">
					<thead>
						<tr>
							<td>".translate("C�digo")."</td>
							<td>".translate("Descri��o")."</td>
							<td>".translate("Observa��o")."</td>
							<td>".translate("Selecionar")."</td>
						</tr>
					</thead>
					<tbody>";
            if ($table != null) {

                $class = "class=\"odd\"";
        	$i = 0;
            foreach($table->Rows() as $row) {
                $area = new Area();
                $area->bind($row);

                $buffer .= "<tr $class>";

		$buffer .= "<td>".$area->getID()."</td>";
		$buffer .= "<td>".translate($area->getDescricao())."</td>";
		$buffer .= "<td>".translate($area->getObservacao())."</td>";
                $checked ='';
                if($idModelo){
                    $checked = $this->getController()->obterAreasChecked($idModelo, $area->getID());
                }
	        $buffer .= '<td><input type="checkbox" name="area[]" id="area[]" '.$checked .' value="'.$area->getID().'" /> </td>';
                $buffer .= "</tr>";
				if($i%2 == 1) {
					$class = "";
				} else {
					$class = "class=\"odd\"";
				}
                                $i++;
            }



            } else {
                $buffer = produce_icon("cross.png")."&nbsp;&nbsp;Nenhum registro encontrado";
              }

              $buffer .= "</tbody>
					<tfoot>
						<tr>
							<td colspan=6></td>
						</tr>
					</tfoot>
					</table>
					<br><br><br>";

                return $buffer;}
                
    /**
     * Retorna html contendo uma tabela com a listagem dos clientes
     * Andr� Alves - 24/01/2011
     * 
     * @param int $cliente Id do modelo caso esteja sendo editado
     * @return string Tabela com a listagem dos clientes
     */
    function listarClientes($cliente) {
	    $table = $this->getController()->obterClientes();
        $buffer = '<table class="formatted" id="paginacaoTable">
			            <thead>	
							<tr>
								<td>&nbsp;</td>
                                <td>'.translate("Cliente").'</td>
     			            </tr>
						</thead>
						<tbody>';
     	
         if ($table != null) {

            foreach($table->Rows() as $row) {
                $checked = '';
                if($row->CLIE_Codigo == $cliente) {
                    $checked = 'checked="checked"';
                }

                $buffer .= '<tr>
                                <td><input type="radio" name="radCliente" class="radCliente" '.$checked .' value="'.$row->CLIE_Codigo.'"></td>
                                <td>'.translate($row->CLIE_Nome).'</td>
                            </tr>';

            }

            $buffer.="  </tbody>
                     </table>";

            return $buffer;
        } else {
            $buffer = produce_icon("cross.png")."&nbsp;&nbsp;".translate("Nenhum registro encontrado");
        }

	    return $buffer;
	}
}
?>
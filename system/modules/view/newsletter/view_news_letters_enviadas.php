<?
class view_news_letters_enviadas extends TView {
	function show() {
		$view = $this;
		require_once(DIR_TEMPLATES."newsletter/frm_newsletters_enviadas.php");
	}
	
	function mostrar() {
		$table = $this->getController()->listarNewslettersEnviadas();
		
		if ($table != null)
		{
			$newsletter = new newsletter();
			$buffer = "<ol class=\"tagol\">";
			foreach($table->Rows() as $row) {
				$newsletter->bind($row);
				$link = DIR_USERS."historico/".$newsletter->getChave().".html";
				$buffer .= "<li class=\"tagli\"><a href=\"$link\">".produce_icon("page.png")."&nbsp;&nbsp;&nbsp;".$newsletter->getDataEnvio()."</a></li>";	
			}
			$buffer .= "</ol>";
			return $buffer;
		} 
		
		return "<h3>Nenhuma newsletter foi encontrada</h3>";
	}
}
?>
<?
class View_gestao_newsletter extends TView {
	function show() { 
		require_once(DIR_TEMPLATES."noticia/frm_gestao_noticia.php");
	}
	
	function showSelecionarModel() {
	    $view = $this;
	    require_once(DIR_TEMPLATES."newsletter/frm_selecionar_modelo.php");
	}
	
	function showSelecionarNoticia() {
	    $view = $this;
	    require_once(DIR_TEMPLATES."newsletter/frm_selecionar_noticia.php");
	}
	
	function showSelecionarDestino() {
	    $view = $this;
	    require_once(DIR_TEMPLATES."newsletter/frm_selecionar_destino.php");
	}
	
	function obterModelosCadastrados($modelo) {
		$table = $this->getController()->obterModelosCadastrados();
		$buffer = "<div >";
		$buffer .= "<table width=\"500\" class=\"formatted\" >";
    
		if ($table != null) {
				
			for($i = 0; $i < $table->RowCount(); $i++){
				$row = $table->getRow($i);
				$value = $row->modelo_id;
				$nome = $row->mod_nome;
				$desc = $row->mod_descricao;
                $rodape = "";
                $rtable = $this->getController()->obterRodape($value);
                if($rtable != NULL) {
                    $rrow = $rtable->getRow(0);
                    $rodape = $rrow->rod_texto;
                }
                
                $checked = ($modelo == $value) ? 'checked="checked"' : ''; 
                $preview =DIR_MODELOS.$nome."/preview.jpg";
				if (file_exists($preview)) {       
					$buffer .= "<div id=\"alinhartabela\">
                                    <img src=\"$preview\" class=\"modelo\" id=\"$value\" border=\"0\" width=\"100\" height=\"100\" />
                                    <input type='hidden' value='".$rodape."' id='".$value."rodape' />
                                    <input type=\"radio\" name=\"rdModelo\" class='modelo' id='".$value."' value=\"$value\" ".$checked.">".$nome."
                                </div>";
				} else {
                    if(strstr(MODELOS_PORTAL, "'".$nome."'")){
                        $buffer .= "<div id=\"alinhartabela\">
                                        <img src=\"$desc\" class=\"modelo\" id=\"$value\" border=\"0\" width=\"100\" height=\"100\" />
                                        <input type='hidden' value='".$rodape."' id='".$value."rodape' />
                                        <input type=\"radio\" name=\"rdModelo\" class='modelo' id='".$value."' value=\"$value\" ".$checked.">".$nome."
                                    </div>";
                    }else{
                        //$buffer .= "<input type=\"radio\" name=\"rdModelo\" id=\"$value\" class=\"modelo\" value=\"$value\" ".$checked.">$nome (".translate("Pre-visualização não está disponível").")";
                        $buffer .= "<div id=\"alinhartabela\">
                                        <img src=\"".DIR_USERS."layouts/no-preview.jpg\" class=\"modelo\" id=\"$value\" border=\"0\" width=\"100\" height=\"100\" />
                                        <input type='hidden' value='".$rodape."' id='".$value."rodape' />
                                        <input type=\"radio\" name=\"rdModelo\" class='modelo' id='".$value."' value=\"$value\" ".$checked.">".$nome."
                                    </div>";
                    }
				}
			}
		}
    
		$fixos = $this->getController()->obterModelosFixos();
		for($i = 0; $i < $fixos->RowCount(); $i++){
			$row = $fixos->getRow($i);
			$value = $row->modelo_id;
			$nome = $row->mod_nome;
            $rodape = "";
            $rtable = $this->getController()->obterRodape($value);
            if($rtable != NULL) {
                $rrow = $rtable->getRow(0);
                $rodape = $rrow->rod_texto;
            }
			$preview =DIR_MODELOS.$nome."/preview.jpg";
			if (file_exists($preview)) {       
				$buffer .= "<div id=\"alinhartabela\"><img src=\"$preview\" class=\"modelo\" id=\"$value\" border=\"0\" width=\"100\" height=\"100\" /><input type='hidden' value='".$rodape."' id='".$value."rodape' /><input type=\"radio\" name=\"rdModelo\" class='modelo' id='".$value."' value=\"$value\">".$nome."</div>";
			} else {
				$buffer .= "<input type=\"radio\" name=\"$nome\" id=\"rdModelo\" class=\"modelo\" value=\"$value\">".$nome." (".translate("Pre-visualização não está disponível").")";
			}
		}
        
		$buffer .= "</table>";
		return $buffer;
    
	}

	function listarNoticias($filtro=null, $selected=null,$inicio=null,$fin=null) {
		$table = $this->getController()->obterNoticiasCadastradas($filtro,$selected,$inicio,$fin);
		$_SESSION['ordem']=$selected;
		if ($table != null) {
                     $buffer = "<table id='paginacaoTable'><thead></thead><tbody>\n";
			for($i = 0; $i < $table->RowCount(); $i++){
				$row = $table->getRow($i);
				$value = $row->noticia_id;
				$titulo = $row->not_titulo;
				$chamada = $row->not_chamada;

				if($titulo == '')
					$titulo = "[ [ NOTÍCIA SEM TÍTULO ] ]";

				//$titulo = utf8_encode($titulo);
				//$chamada = utf8_encode($chamada);

				$checkBoxName = "rdNoticia_$value";
				$textBoxName = "txtOrdem_$value";
				$ordem="";
				//echo $selected;
				
				//ordema depois da pesquisa no javascritp pra nao perder o ordem
				if(!empty($selected)){
					$selectedarray= explode(',', $selected);
					//guarda o ordem das noticias pra o proformulario 
					for ($index = 0; $index < count($selectedarray); $index++) {
						if($selectedarray[$index]==$value){
							$ordem=$index+1;
						}
					}
				}
                                
				$checked	= "";
				if ($row->checked == "1")
				{
					$checked = "checked";
				}
				$buffer .= "<tr><td>\n";
				$buffer .= "<input type=\"checkbox\" $checked name=\"$checkBoxName\" value=\"$value\" onClick=\"addCheckBoxSelecionadoSelecionados(this, document.getElementById('$textBoxName')); selecionarOrdem(this, '$textBoxName'); \" class=\"inputText noticias\">&nbsp;<input type=\"text\"  id=\"$textBoxName\" name=\"$textBoxName\" size=\"2\"  class=\"inputText\" value=\"$ordem\" readonly=\"readonly\" maxlength=\"2\"/>&nbsp;$titulo";				//$buffer .= "<dt><input type=\"checkbox\" $checked name=\"$checkBoxName\" value=\"$value\" onClick=\"addCheckBoxSelecionadoSelecionados(this, $checkBoxName); selecionarOrdem(this, '$textBoxName');\" class=\"inputText\">&nbsp;<input type=\"text\"  id=\"$textBoxName\" name=\"$textBoxName\" size=\"1\"  class=\"inputText\"/>$titulo</dt>";
                                $buffer .= "</td></tr>\n";
                                
                        }
                         $buffer .= "</tbody></table>";

                         
                         $buffer .= '<script type="text/javascript">
                                        //Seleção de checkbox múltiplas páginas
                                        areas = new Array();
                                            
                                        $(".noticias:checked").each(function(){
                                        	areas.push($(this).val());
                                        });
                                        
                                        $(".noticias").click(function(){
                                            if($(this).is(":checked")) {
                                                areas.push($(this).val());    
                                            } else {
                                                val = $(this).val();
                                                areas = $.grep(areas, function (a) { return a != val; });
                                            }
                                            $("#areas").val(areas);
                                        });
                                    </script>';
                         
                         
                       // var_dump($_GET);
                        
                         
			return $buffer;
			
		} else {
			return "Nenhuma noticia cadastrada";
		}
	}
	/*
        public function  pegavalor($value,$textBoxName){
            echo "<script>pegarOrdem('".$value."','".$textBoxName."');</script>";
            //pegarOrdem(\"$value\",\"$textBoxName\")
        }
        
        public function test(){
            
            return "0";
        }

*/
	function listarAreasCadastradas() {
		$table = $this->getController()->obterAreasCadastradas();
		
		if ($table != null) {
			$buffer = "<table>";
			for($i = 0; $i < $table->RowCount(); $i++){
				if($i%2 == 0) {
					$buffer .= "<tr>";
				}
				$row = $table->getRow($i);
				$value = $row->area_id;
				$descricao = $row->are_descricao;
				$observacao = $row->are_observacao;
				
				$buffer .= "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"checkbox\" id=\"areacadastradas\" name=\"rdArea_$value\" value=\"$value\" alt=\"$observacao\" size=\"2\"class=\"inputText\">&nbsp;$descricao<br /></td>";
				if($i%2 == 1) {
					$buffer .= "</tr>";
				}
			}
			$buffer .= "</table>";
			
			return $buffer;
			
		} else {
			return "Nenhuma área cadastrada";
		}
	}
	
    /**
     * Monta HTML com a listagem de áreas separadas por clientes e áreas de colaboração.
     * Caso conexão seja por meio de API cria-se campos tipo hidden.
     * Caso seja passado o parâmetro usuario, a função retorna os clientes do usuário informado.
     * Caso não seja passado o parâmetro usuario, a função retorna os clientes do usuário atualmente logado no sistema.
     * André Alves - 27/01/2012
     * 
     * @param int $usuario Id do usuário
     * @return string Lista com os clientes
     */
    function listarAreas($usuario = '') {
        
        $lista_clientes = $this->getController()->obterClientesDoUsuario();
		
        $buffer = "";
        
        if(checkapi()) {
            
            $lista_areas = $this->getController()->obterAreasDoUsuario();
            foreach($lista_areas->Rows() as $area) {
                $buffer .='<input type="hidden" name="rdArea_'.$area->area_id.'" value="'.$area->area_id.'" >';
            }
            
        } else {
            
            if($lista_clientes != null) {
                foreach($lista_clientes->Rows() as $cliente) {
                    $lista_areas = $this->getController()->obterAreasDoUsuario($cliente->CLIE_Codigo);
                    
                    $buffer .= '<table class="formatted">
        			            <thead>
        							<tr>
        								<th width="15">&nbsp;</th>
                                        <th>'.ucwords(strtolower(str_replace('zzz','',$cliente->CLIE_Nome))).'<span style="float:right;">'. translate('Selecionar todas').' <input type="checkbox" name="selecionar_areas_cliente" class="selecao"></span></th>
                                           
                                        </tr>
        						</thead>
        						<tbody>';
                                
                                if($lista_areas != null) {
                                    foreach($lista_areas->Rows() as $area) { 
                                        
                                        $buffer .= '<tr>
                                                        <td><input type="checkbox" name="rdArea_'.$area->area_id.'" value="'.$area->area_id.'" alt="'.$area->are_observacao.'" size="2"></td>
                                                        <td>'.$area->are_descricao.'</td>
                                                    </tr>';
                                    }	
                                } else {
                                    $buffer .= '    <tr>
                                                        <td></td>
                                                        <td>';
                                    $buffer .= translate("Nenhum registro encontrado");
                                    $buffer .="         </td>
                                                    </tr>";
                                }   
                                
                    $buffer.='  </tbody>
                              </table>';
                    
                }
            } else {
                $buffer .= translate("Nenhum registro encontrado");
            }
            
        }
        
        return $buffer.'<br /><br />';        
    
    }
    
	function listarCamposAdicionais() {
		$modeloSelecionado = $_POST["rdModelo"];
		
		$contents = $this->getController()->obterConteudoModelo($modeloSelecionado);
		
		$buffer = "";
		if ($contents != null) {
			$matches = array();
			preg_match_all("/<@(.+)@>/", $contents, $matches);
			
			foreach($matches[1] as $field) {
				$infos = explode(",", trim(substr(trim($field), stripos(trim($field), "(") + 1, stripos(trim($field), ")") - strlen(trim($field)))));
				
				$descricao = substr($infos[0], 1, strlen($infos[0]) - 2);
				$name = sha1($descricao);
				
				switch (trim($infos[1])) {
					case "email":
					case "number":
					case "text":
						$buffer .= "<label for=\"$name\">".$descricao.": </label><br />";
						$buffer .= "<input type=\"text\" name=\"$name\" class=\"inputText\"><br />\n";
						break;
				}
			}
		}
		
		return $buffer;
	}

}
?>
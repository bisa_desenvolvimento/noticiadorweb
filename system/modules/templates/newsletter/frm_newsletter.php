<html>
<head>
<!--<meta charset="UTF-8">-->
</head>

<?=add_html_head() ?>
<meta http-equiv='cache-control' content='no-cache' />
<meta http-equiv='expires' content='0' />
<meta http-equiv='pragma' content='no-cache' />

<div class="rounded_content_top">
	&nbsp;
</div>
<div class="rounded_content_middle">
	<div class="rounded_content">
	<h1 class="default_title"><?=translate("Cadastrar Novo Modelo")?></h1>
	
        <form action="<?=$view->add_form_process_url()?>" id="formulario" method="post"  enctype="multipart/form-data" onkeyup="validaEspacos(document.getElementById('txtNome').value)"  onsubmit="return validarForm(1);">
	  	<? add_action_data("save", null, $view->getID()) ?>
	  	<fieldset>
                    <div id="headertxt"></div>
                    <div id="contenttxt"></div>
                    <div id="footertxt"></div>

                  <div id="dadosModelo">
                      <div style="float:right"><?=produce_icon("arrow_down.png")?>&nbsp;&nbsp;<?=produce_link(DIR_MODELOS."modelo.zip", translate("Baixar modelo padr�o")) ?></div>
	  		<label for="txtNome"><?=translate("Nome do modelo (sem espa�os em branco)")?></label><br/>
	  		<input type="text" name="txtNome" id="txtNome" value="<?=$view->getNome()?>" class="inputText" maxlength="100" size="50" />
	  		<br />
	  		<br />
	  		<label for="txtDescricao"><?=translate("Descri��o")?></label><br />
	  		<textarea id="txtDescricao" name="txtDescricao" id="txtDescricao" class="width100percent clearRight" ><?=$view->getDescricao() ?></textarea>
	  		<br/>
	  		<br/>
	  		<!--label for="txtDescricao"><?=translate("Rodape")?></label><br />
                        <textarea id="txtRodape" name="txtRodape" style="height:  80px;" id="txtDescricao" class="width100percent clearRight" ><?//=$view->getRodape() ?></textarea-->
                         
	  		<? 
                        $show='block';
                        if(!$view->getDescricao()){
                            $show = 'none';
                            ?>
                        <label for="txtDescricao"><?=translate("Como deseja criar o modelo?")?></label><br />
                        <input type="radio" name="tipoModelo"  class="tipoModelo" value="0"><?=translate("Eu j� tenho um modelo feito baseado no padr�o.")?><br/>
                        <input type="radio" name="tipoModelo"  class="tipoModelo" value="1"><?=translate('Eu quero fazer um modelo do zero.')?><br/>
                        <br/>
                        <div id="pegarZip" style="display:none;"><label for="fileUpload"><?=translate("Arquivo zip contendo o modelo, este arquivo deve seguir a estrutura do modelo")?></label><br />
                        <input type="file" name="fileUpload" id="fileUpload"class="inputText"/>
                        
                        </div>
                        
                        <?}?>
                        
                      
	  	</div>
                   <!-- /*
                     *** Jorge Roberto Mantis 5658 ***
                      */-->
                    <div id="gestaoModeloZero" style="display:<?=$show?>;">
                        <br/>
                        <label for="txtDescricao"><?=translate("Modelo do conte�do:")?></label><br />
                        <input type="radio" name="tipoConteudo" checked value="0"><?=translate("Not�cia completa")?><br/>
                        <input type="radio" name="tipoConteudo"  value="1"><?=translate('T�tulo + resumo')?><br/>

                        <label for="txtDescricao"><?=translate("Endere�o no cabe�alho:")?></label><br />
                        <input  name="headerLink" class="inputText"><br/>

                        <label for="txtDescricao"><?=translate("Endere�o no rodap�:")?></label><br />
                        <input name="footerLink" class="inputText"><br/>
                    </div>

                     <div id="areasInteresse" style="display:<?=$show?>;">
                        <br/>
                        <label for="txtDescricao"><?=translate("Selecione o cliente")?></label><br />
                        <?=$view->listarClientes($view->getCliente());?>
                        <input type="button" name="btnEnviar" id="btnEnviar"  class="botaoEnviar" style="display: <?=$show?> ;"/>
                     </div>
                    
                    <input type="button" id="btnAvancar" value="" class="botaoEnviar mostrar" style="display:none;"/>
                    <input type="button" id="btnVoltar" value="" class="botaoVoltar mostrar" style="display:none;"/>
                    

                        <br/>
                        <div id="mainbody" title="asdads">
                        <div>
                        <span>Criar um novo modelo</span><br/>
                        <span id="status"></span>
                        <label>Clique aqui pra pegar ou trocar a <br/>imagem do <span>Cabe��rio</span> de seu computador.</label><div id="header" class="imagemUpload Header"></div><div id="headerPreview" class="header"></div>
                        <label>Clique aqui pra pegar ou trocar a <br/>imagem do <span>Conte�do</span> de seu computador.</label><div id="content" class="imagemUpload Content"></div><div id="contentPreview" class="content"></div>
                        <label>Clique aqui pra pegar ou trocar a <br/>imagem do <span>Rodap�</span> de seu computador.</label><div id="footer" class="imagemUpload Footer"></div><div id="footerPreview" class="footer"></div>
                        </div>
                         
                        </div>
                       
                        
     	  	</fieldset>
        </form>
	</div>
</div>


<div id="gerenciarModelo">
		<div class="rounded_content_bottom">
	&nbsp;
</div>	
<div class="rounded_content_top">
	&nbsp;
</div>
<div class="rounded_content_middle">
	<div class="rounded_content">
            <form>
	  	<fieldset>
			<h1 class="default_title"><?=translate("Modelos Cadastrados")?></h1>
	  		<?=$view->exibirModelosCadastrados() ?>
	  	</fieldset>
	  </form>

        </div>
</div>
    </div>
</head>
<body>


<style>
 #status{
	font-family:Arial; padding:5px;
}



#mainbody label,#mainbody label,#mainbody label{
    margin: 76px 0px 0px 218px;
    position: absolute;
    font-size: 15px;
    z-index: 1;
}
#mainbody label span,#mainbody label span,#mainbody label span{
 font-size: 22px;
 color: #5A7A99;
}
.erro{
    float:right;
    margin: -13px 200px 0px 0px;
    background:#f0c6c3;
    border:1px solid #cc6622;
}


.fechar{
    float: right; margin: -6px 0px 0px 600px; position: absolute; z-index: 10000000000000;
}

</style>
<script type="text/javascript">
	function validarForm(tipoModelo) {
		if (!validarTexto(document.getElementById("txtNome"))) {
			alert("<?=translate('Digite um nome')?>");
                        document.getElementById("txtNome").focus();
			return false;
		}
                if (!validarTexto(document.getElementById("txtDescricao"))) {
			alert("<?=translate('Digite uma descri��o')?>");
			return false;
		}

                

                 if(tipoModelo==1){
                 if((!$("#pegarZip").is(":hidden"))){
                    if(!validaUploadZip()){
                        return false;
                    }
                }}


                if(tipoModelo==3){
                    if(!$("#txtheader").length>0){
                    alert("<?=translate('Selecione uma imagem pro header')?>");
                    return false;
                    }
                    if(!$("#txtcontent").length>0){
                        alert("<?=translate('Selecione uma imagem pro conte�do')?>");
                        return false;
                    }
                    if(!$("#txtfooter").length>0){
                        alert("<?=translate('Selecione uma imagem pro rodape')?>");
                        return false;
                    }
                }

                /*if((!$("#radCliente").is(":hidden"))){
                      var areas = new Array();
                    $('input:radio:checked').each(function() {
                    areas.push($(this).val());
                    });
                    areas = areas.toString();

                    if((areas.length>0)) {
                    }else{alert("<?= translate("Selecione um Cliente.") ?>");
                        return false;
                    }
                }*/

                if((!$(".radCliente").is(":hidden"))){
                    if($(".radCliente").is(":checked")) { } else {
                        alert("<?= translate("Selecione um Cliente.") ?>");
                        return false;
                    } 

                }

                return true;
	}
	
	function validarTexto(obj) {
		if (obj.value.length > 0){
			return true;}
		
		return false;
	}
        
    function validaEspacos(txtnome){
            var nome="";
            for(var i=0;i<=txtnome.length;i++){
               if(txtnome.charAt(i)==" "){
                }
                else{
                    nome+=txtnome.charAt(i);    
                }
            }
            document.getElementById('txtNome').value=nome;
            return true;
        }
        
    function validaArquivo(arquivo){
          if(arquivo.length<=0){
                return false;
            }
            return true;
        }
        
    function validaUploadZip(){
                
                var strFilePath = document.getElementById("fileUpload").value;
                var arrTmp = strFilePath.split(".");
                var strExtension = arrTmp[arrTmp.length-1].toLowerCase();
                var blnExists = false;

                if(strFilePath==""){
                alert('<?=translate("Tem que selecionar o modelo desejado!")?>');
                document.getElementById("fileUpload").focus();
                return false;
                }
                else{
                if (strExtension=="zip") 
                return true;
                else
                alert('<?=translate("O modelo selecionado n�o � um arquivo .zip")?>');
                return false;
                }               
        }
</script>


        <script type="text/javascript" src="scripts/fileuploader.js" ></script>
        <script>

                            $(document).ready(function(){
                                
                                //converto os divs em botao pra upload
                                createButtonUpload();
                                //cria o dialog
                                createDialog();

                                $("#btnAvancar").live("click",function(){
                                    if(validarForm(2)){
                                    $("#dadosModelo,#gerenciarModelo").slideUp();

                                    setTimeout(function(){
                                    $("#mainbody").fadeIn();
                                   $( "#mainbody" ).dialog( "open" );
                                     
                                    }, 500);
                                    }
                                });
                                $("#btnVoltar").live("click",function(){
                                    
                                  $("#areasInteresse").slideUp();
                                    setTimeout(function(){
                                    $("#mainbody").fadeIn();
                                    $("#mainbody" ).dialog( "open" );
                                    }, 500);
                                 
                                });

                                $("#btnEnviar").live("click",function(){
                                    $("#formulario").submit();
                                });

                                $(".tipoModelo").live("click",function(){
                                    if($(this).val()==0){
                                    $("#btnAvancar").hide();
                                    $("#pegarZip").slideDown();
                                    $("#areasInteresse").slideDown();

                                    $("#btnEnviar").show();
                                    }
                                    if($(this).val()==1){
                                    $(".botao").hide();
                                    $("#pegarZip").slideUp();
                                    $("#areasInteresse").slideUp();
                                    $("#btnAvancar").slideDown();
                                    }
                                });

                                $(".fechar").live("click",function(){
                               $(this).next("img").remove();
                               var div = "#txt"+$(this).parent().prev("div").attr("id");
                              
                               $(div).remove();
                               $(this).parent().removeClass("borderimg").prev("div").show().prev("label").show();
                               $(this).closest("ul").remove();

                               $(this).remove();
                                });
                            
                            });


        function createButtonUpload(){
                    var imagens = new Array("header","content","footer");
                    for(var i in imagens){
                        var status=$('#status').hide();
                    var nome =imagens[i];
  
                 var uploader = new qq.FileUploader({
                element: document.getElementById(nome),
                action: 'index.php?action=save&secao=newsletter&tipo='+nome,
                          // validation
                // ex. ['jpg', 'jpeg', 'png', 'gif'] or []
  
                allowedExtensions: ['jpg'],
                // this option isn't supported in all browsers
                sizeLimit: 9000000, // max size
                minSizeLimit: 1, // min size
                multiple: false,

                // set to true to output server response to console
                debug: false,
                messages: {
                typeError: "<?=translate("{file} n�o � formato JPG")?>",
                sizeError: "<?=translate("{file} O tamanho maximo da imagen � 1 MB")?>",
                minSizeError: "<?=translate("{file} O tamanho minimo da imagen � 1 KB")?>",
                emptyError:  "<?=translate("{file} O arquivo est� vacio, por favor selecione outro.")?>",
                onLeave: "<?=translate("Voce ainda est� fazendo upload de uma imagen, deseja sair?")?>"
                 },
//              
//                showMessage: function(message){
//                    if(message=="File is too large")
//                    alert("<?//=translate("O tamanho maximo da imagen � 1 MB")?>"); },
                // events
                // you can return false to abort submit
                onSubmit: function(id, fileName){
                
                },

                onComplete: function(id, fileName, response){
                    //On completion clear the status
				status.text('');
				//Add uploaded file to list

                            // console.log(fileName);
				if(response.file==="header"||response.file==="content"||response.file==="footer"){
                                    $("#"+response.file).hide().prev("label").hide();
                                    $("ul.qq-upload-list").remove();
                                    $("#"+response.file+"Preview").append('<img src="<?=DIR_LAYOUT?>hd_logout_button2.png" title="Apagar" class="fechar"><img  id="'+response.file+'img" src="<?=DIR_USERS?>layouts/uploadajax/'+fileName+'" alt="Pode clicar pra trocar de imagem." />').addClass("borderimg");
                                    //if()
                                    //alert(("#"+response.file+"Preview").wid);
                                    $("#"+response.file+"txt").html('<input type="hidden" value="'+fileName+'" id="txt'+response.file+'" name="txt'+response.file+'">');
				} else{
                                  //  status.html(response.file).addClass("erro");
				}
                }

            });

                }   
               
        }

        //fun��o para mudar o nome do arquivo de cabe�ario
        //function changeHeaderName() {
        //    rename(DIR_MODELOS. "header", DIR_MODELOS."/header.jpg");
        //}
        //fun��o para mudar o nome do arquivo de conte�do
        //function changeContentName() {
        //    rename(DIR_MODELOS. "content", DIR_MODELOS."/content.jpg");
        //}
        //fun��o para mudar o nome do arquivo de rodap�
        //function changeFooterName() {
        //    rename(getElementById("footer"), DIR_MODELOS."/footer.jpg");
        //}

        //inicializa o dialogo com os parametros
  function createDialog(){
  $( "#mainbody" ).dialog({
			resizable: false,
			height:640,
                        width: 640,
                        autoOpen: false,
			modal: true,
                        closeOnEscape: false,
			buttons: {
				"Voltar": function() {
                                $(this).dialog("close");
                                $("#dadosModelo").slideDown();
                                $("#btnVoltar").hide();
                                 $("#btnAvancar").show();
				},
				Avancar: function() {

                                if(validarForm(3)){
                                    $(this).dialog("close").fadeOut();
                                    $("#gestaoModeloZero").slideDown();
                               $("#areasInteresse").slideDown();

                               $("#btnVoltar").show();
                               $("#btnAvancar").hide();
                               $("#btnEnviar").show();
                                }
				}
			}
                       	}).parent('.ui-dialog').find('.ui-dialog-titlebar').hide();
         
  }

</script>

	
<?=add_html_tail() ?>

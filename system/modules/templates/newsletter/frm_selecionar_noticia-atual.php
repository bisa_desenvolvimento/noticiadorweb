<?=add_html_head() ?>	
<?php
	$view->getController()->atualizarOpcoesEnvio($_POST['ddlFormaDeEnvio'], $_POST['rdModelo']);
?>
<div class="rounded_content_top">
	&nbsp;
</div>
<div class="rounded_content_middle">
	<div class="rounded_content">
	
		<h1 class="default_title"><?=translate("Selecione as Not�cias da Newsletter")?></h1>						
				<form method="post" action="<?=$view->add_form_process_url(); ?>"  name="formulario" onsubmit="return validarForm(this);">
					<input type="hidden" name="gerarChave" value="nao"/>
			<? add_action_data("selecionarDestino", null, $view->getID()); ?>
			<? add_hidden_last_post() ?>
			<? 
				$ano = ""; $numero = "";
				$table = $view->getController()->obterDadosAdicionais($_POST['rdModelo']);
				if ($table != null) {
					 $rowModelo = $table->getRow(0);
					 $ano = $rowModelo->rod_ano;
					 $numero = $rowModelo->rod_numero+1;					 
				}
				 
			?>
			<fieldset>
				<table cellspacing=""> 
				<tr>
					<td>
						<label for="txtTitulo"><?=translate("T�tulo da Newsletter"); ?>: </label>
						<input type="text" name="txtTitulo" class="inputText" size="65" maxlength="250"/>&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<td>
						<label for="txtAno"><?=translate("Ano"); ?>: </label>
						<input type="text" name="txtAno" class="inputText" size="15" maxlength="5" value="<?=$ano?>" />&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<td>
						<label for="txtNumero"><?=translate("N�mero"); ?>: </label>
						<input type="text" name="txtNumero" class="inputText" size="15" maxlength="5" value="<?=$numero?>" />
					</td>
				</tr>
				</table><br /><br />
			</fieldset>
			<fieldset>
							 
	   			<div id="listaNoticias">
				<?=$view->listarNoticias()?>
				</div>
								<div id="pager" class="pager" >
								
							<img src="images/first.png" class="first"/>
							<img src="images/prev.png" class="prev"/>
							<input type="text" class="pagedisplay"/>
							<img src="images/next.png" class="next"/>
							<img src="images/last.png" class="last"/>
							<select class="pagesize">
								<option selected="selected"  value="25">25</option>
					
								<option value="50">50</option>
								<option value="75">75</option>
								<option  value="100">100</option>
							</select>
								
					  <input type="hidden" name="areas" id="areas" value="" />
								
								</div>		
				
								<label id="pesquisa" style="cursor: pointer"><strong><?=translate("Pesquisa Avan�ada")?></strong></label>
								<div id="avancada" style="display:none;">
								<table style="border: 1px gray;">
								<tr>
								<td>
								<label for="filtroAreas"><?=translate("Filtrar Not�cias por:")?></label><br />
								<input type="text" name="filtroAreas" id="filtroAreas"class="inputText" size="50" onkeydown="carregarNoticias()"/>
								</td>
								<td>&nbsp;&nbsp;&nbsp;</td>
								
								<td><label for="publicar"><?=translate("Partir de")?>:</label>
						<br />
												<input type="text"  value="<?echo date('Y/m/d');?>" name="FDatapublicacao" id="FDatapublicacao" onKeyUp="this.value = formatar(this.value,'9999/99/99')"  maxlength="10" size="18" onchange="carregarNoticias()"/>
				</td>
								<td>&nbsp;&nbsp;&nbsp;</td>		
								<td><label for="publicar"><?=translate("At�")?>:</label>
						<br />
												<input type="text"   value="" name="FDatafinal" id="FDatafinal" onkeyup="this.value = formatar(this.value,'9999/99/99')"  maxlength="10" size="14" onchange="carregarNoticias()"/>
				   </td>
							   
							   </tr>
								</table>
								   
								</div>
								<input type="submit" value="<?=translate("Avan�ar") ?>" class="botao"/>
								<br />
							   
								
								
			</fieldset>
		</form>
<script>
	
		
	function validarForm(form){
		// Verifica sele��o do modelo
		var noticiaSelecionada = false;
		for(i = 0; i < form.elements.length; i++) {
			name = "" + form.elements[i].name;
			if (name.indexOf("rdNoticia") != -1) {
				if (form.elements[i].checked)
					noticiaSelecionada = true;
			}
		}

				if (form.elements["txtTitulo"].value == "") {
			alert('<?=translate("Digite o t�tulo da newsletter"); ?>');
						form.elements["txtTitulo"].focus();
			return false;
		}

		if (!noticiaSelecionada) {
			alert('<?=translate("Selecione a(s) noticia(s) da newsletter"); ?>');
			return false;
		}


		return true;
	}

	  
		
		function carregarNoticias(){
			var paginacao ="<form><img src='images/first.png' class='first'/><img src='images/prev.png' class='prev'/>";
			paginacao+="<input type='text' class='pagedisplay'/>";
			paginacao+=				"<img src='images/next.png' class='next' class='pagedisplay'/>";
			paginacao+=				"<img src='images/last.png' class='last' class='pagedisplay'/>";
			paginacao+=				"<select class='pagesize class='pagedisplay''>";
			paginacao+= 		"<option selected='selected'  value='25'>25</option>";
			paginacao+=		  	"<option value='50'>50</option>";
			paginacao+=  		"<option value='75'>75</option>";
			paginacao+=				 "<option  value='100'>100</option>";
			paginacao+=				"</select>";
			paginacao+=			"</form>";
		updateListComData('<?=carregarArquivo() ?>', 'ajaxLista', 'listaNoticias',document.getElementById('filtroAreas').value ,document.getElementById('FDatapublicacao').value,document.getElementById('FDatafinal').value);
		//alert(document.getElementById('filtroAreas').value )
		//frm.filtroAreas.value
	
	}
		
		
$(document).ready(function(){
	

										$("#pesquisa").click(function(){
											$("#avancada").slideToggle("slow");
//											$("#listaNoticias").slideToggle("slow");
											$("#pager").hide();
										 });
										 
									   
											  
	  $("#FDatapublicacao, #FDatafinal").datepicker({
		dateFormat: 'yy/mm/dd',
		dayNames: [
		'<?=translate('Domingo')?>','<?=translate('Segunda')?>','<?=translate('Terça')?>','<?=translate('Quarta')?>','<?=translate('Quinta')?>','<?=translate('Sexta')?>','<?=translate('Sábado')?>','<?=translate('Domingo')?>'
		],
		
		dayNamesMin: ['<?=translate('D')?>','<?=translate('S')?>','<?=translate('T')?>','<?=translate('Q')?>','<?=translate('Q')?>','<?=translate('S')?>','<?=translate('S')?>','<?=translate('D')?>']

		,
		dayNamesShort: [
		'<?=translate('Dom')?>','<?=translate('Seg')?>','<?=translate('Ter')?>','<?=translate('Qua')?>','<?=translate('Qui')?>','<?=translate('Sex')?>','<?=translate('Sáb')?>','<?=translate('Dom')?>'
		],
		monthNames: [
		'<?=translate('Janeiro')?>','<?=translate('Fevereiro')?>','<?=translate('Março')?>','<?=translate('Abril')?>','<?=translate('Maio')?>','<?=translate('Junho')?>','<?=translate('Julho')?>','<?=translate('Agosto')?>','<?=translate('Setembro')?>',
		'<?=translate('Outubro')?>','<?=translate('Novembro')?>','<?=translate('Dezembro')?>'
		],
		monthNamesShort: [
		'<?=translate('Jan')?>','<?=translate('Fev')?>','<?=translate('Mar')?>','<?=translate('Abr')?>','<?=translate('Mai')?>','<?=translate('Jun')?>','<?=translate('Jul')?>','<?=translate('Ago')?>','<?=translate('Set')?>',
		'<?=translate('Out')?>','<?=translate('Nov')?>','<?=translate('Dez')?>'
		],
		nextText: 'Pr�ximo',
		prevText: 'Anterior',
		changeMonth: true,
  	changeYear: true,
		minDate: "-5Y", maxDate: 0
	  });  
	  
	  
	
  
});
		
</script>		
	</div>
</div>
<?=add_html_tail() ?>

<?=add_html_head() ?>
<?php

	$table = $view->getController()->obterOpcoesEnvio();
	if ($table != null) {
		$rowOpcoes = $table->getRow(0);
		//$tipoEnvio = $rowOpcoes->news_tipo_envio;
		$modelo = $rowOpcoes->news_modelo;
	}

?>	
<div class="rounded_content_top">
	&nbsp;
</div>
<div class="rounded_content_middle">
	<div class="rounded_content">
		<h1 class="default_title"><?=translate("Selecione o Modelo da Newsletter")?></h1>	
				<form method="post" action="<?=$view->add_form_process_url(); ?>" onSubmit="return validarForm(this);" onkeyup="validaCaracteres(document.getElementById('txtrodape').value)" >
					<input type="hidden" name="gerarChave" value="sim"/>
			<? add_action_data("selecionarNoticia", null, $view->getID());
						add_hidden_api();
						?>
			<fieldset>
				<!--
				<label for="ddlFormaDeEnvio"><?/* translate("Como as not�cias devem ser enviadas?") */?></label>
				<br/>
				
								<input type="radio" name="ddlFormaDeEnvio" value="0" <?/* ($tipoEnvio == 0) ? 'checked="checked"' : '' */?> /><?/* translate("Mat�ria completa") */?><br/>
								<input type="radio" name="ddlFormaDeEnvio" value="1" <?/* ($tipoEnvio == 1) ? 'checked="checked"' : '' */?> /><?/* translate('Resumos + link "Saiba mais..."') */?><br/>
								<input type="radio" name="ddlFormaDeEnvio" value="3" <?/* ($tipoEnvio == 3) ? 'checked="checked"' : '' */?> /><?/* translate('Resumos + link + Miniatura de imagem "Saiba mais..."') */?><br/>
								<input type="radio" name="ddlFormaDeEnvio" value="2" <?/* ($tipoEnvio == 2) ? 'checked="checked"' : '' */?> /><?/* translate("Usar links de mat�rias no in�cio da newsletter")*/?>
				<br />
				<br />
				<br />
				-->

				<label for="ddlFormaDeEnvio"><?=translate("Selecione o layout:") ?></label>
				<br/>
				<?=$view->obterModelosCadastrados($modelo) ?><br/>
								<div style="clear:both;"></div>
							   
								<br />
								<div id="rodape" style="display:none">
									 <label id="adicionarodape" style="cursor: pointer"><?=translate("Rodape: ") ?></label>
								<br/>
									<textarea id="txtrodape" name="rodape" rows="3" cols="83"></textarea>
								</div>
								<br />
				<input type="submit" value="<?=translate("Avan�ar") ?>" class="botao"/>
			</fieldset>
		</form>
<script>

	var idlastrodape = "";

	$(document).ready(function(){

		$("img.modelo").click(
			function(){
				//alert("idlastrodape: " + idlastrodape);
				var valuetxt = $("#txtrodape").val();
				$("#" + idlastrodape + "rodape").val(valuetxt);
				//alert("valuetxt: " + valuetxt);
				var id = $(this).attr("id");
				$("input[id='" + id + "']").attr("checked", "true");
				var rodape = "#" + id + "rodape";
				var value = $(rodape).val();
				$("#txtrodape").val(value);
				$('#rodape').show();
				idlastrodape = id;
				//alert("idlastrodape: " + idlastrodape);
			}
		);
		$("input.modelo").click(
			function(){
				//alert("idlastrodape: " + idlastrodape);
				var valuetxt = $("#txtrodape").val();
				$("#" + idlastrodape + "rodape").val(valuetxt);
				//alert("valuetxt: " + valuetxt);
				var id = $(this).attr("id");
				var rodape = "#" + id + "rodape";
				var value = $(rodape).val();
				$("#txtrodape").val(value);
				$('#rodape').show();
				idlastrodape = id;
				//alert("idlastrodape: " + idlastrodape);
			}
		);
		if ($("input[name='rdModelo']:checked").length > 0) {
			//alert($("input[name='rdModelo']:checked").length);
			var valueid = $("input[name='rdModelo']:checked").attr('id');
			//alert(valueid);
			var id = valueid;
			var rodape = "#" + id + "rodape";
			var value = $(rodape).val();
			$("#txtrodape").val(value);
			$('#rodape').show();
			idlastrodape = id;
			//alert("idlastrodape: " + idlastrodape);
		}

		//mostra o campo pra rodape

	});

	function validarForm(form) {
		// Verifica sele��o do modelo
		var modeloSelecionado = false;

		//var tipoenvio= false;

		for(i = 0; i < form.elements.length; i++) {
			//if (form.elements[i].name == "rdModelo") {
			//	if (form.elements[i].checked)
			if ($("input[name='rdModelo']:checked").length) {
				modeloSelecionado = true;
			}

		}

		/*
		for(i = 0; i < form.elements.length; i++) {
			if (form.elements[i].name == "ddlFormaDeEnvio") {
				if (form.elements[i].checked)
					tipoenvio = true;
			}
		}
		*/

		/*
		if(!tipoenvio){
			alert('<?=translate("Selecione como as not�cias devem ser enviadas"); ?>');
			return false;
		}
		*/

		if (!modeloSelecionado) {
			alert('<?=translate("Selecione o modelo da newsletter"); ?>');
			return false;
		}

		return true;
	}

	function validaUpload(){
		var zip="zip";
		var objInput = frm.thumbnail;
		var strFilePath = objInput.value;
		var arrTmp = strFilePath.split(".");
		var strExtension = arrTmp[arrTmp.length-1].toLowerCase();
		var blnExists = false;

		if(strFilePath==""){
			blnExists=true;
		}else{

			if (strExtension ==zip) {
				blnExists = true;
			}

		}
		if (!blnExists)
			alert('<?=translate("O modelo selecionado n�o � um arquivo zip")?>');
		return blnExists;
	}

	function validaCaracteres(txtnome){
		var campo="";
		for(var i=0;i<=txtnome.length;i++){
			if(txtnome.charAt(i)=="#"){
			}else{
				campo+=txtnome.charAt(i);
			}
		}
		document.getElementById('txtrodape').value=campo;
		return true;
	}

	</script>
	</div>
</div>
<?=add_html_tail() ?>

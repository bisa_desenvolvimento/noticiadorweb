<?=add_html_head() ?>

<div class="rounded_content_top">
	
</div>
<div class="rounded_content_middle">
    <div class="rounded_content">
        <h1 class="default_title"><?=translate("Selecione as �reas que deseja deixar.   ")?></h1>
        <form action="<?=$view->add_form_process_url(); ?>" method="post">
	<? add_action_data("delete", null, $view->getID()); ?>
	<input type="hidden" name="email" value="<?=$_GET["id"] ?>" />
	<fieldset>
		<legend><?=translate("Selecione as �reas que deseja deixar.")?></legend></br>
		<?=$view->listarAreas() ?>
                </br>
                <input type="submit" id="enviar" value="<?=translate("Remover assinatura") ?>" class="botao" />
	</fieldset>
        </form>
    </div>
    </div>
<script>
    $(document).ready(function(){
        $("#enviar").click(function(){
        var areas = new Array();
        $('input:checkbox[class="removerarea"]:checked').each(function() {areas.push($(this).val());});
        areas = areas.toString();

        if(areas.length > 0) {
           return true;
        } else {
            alert("<?= translate("Selecione ao menos uma �rea de interesse.") ?>");
            return false;
        }
        });
   });

</script>

<?=add_html_tail() ?>

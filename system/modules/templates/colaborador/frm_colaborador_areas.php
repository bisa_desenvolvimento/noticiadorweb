<?=add_html_head() ?>	
<div class="rounded_content_top">
	&nbsp;
</div>
<div class="rounded_content_middle">
	<div class="rounded_content">
	<h1 class="default_title"><?=translate("Cadastrar Colaborador")?></h1>
		
  	<form name="formulario" method="post" action="<?=$view->add_form_process_url(); ?>" target="iframeColaborador" onsubmit="return validaForm(0)&&checaSenha()">
			<? add_action_data("save", null, $view->getID()); ?>
    			   
            <input type="hidden" name="FUSUA_Email" id="FUSUA_Email" value="<?=$_POST['FUSUA_Email']?>" />
            <input type="hidden" name="FUSUA_Senha" id="FUSUA_Senha" value="<?=$_POST['FUSUA_Senha']?>" />
            <input type="hidden" name="FUSUA_Idioma" id="FUSUA_Idioma" value="<?=$_POST['FUSUA_Idioma']?>" />
            <input type="hidden" name="FUSUA_Nome" id="FUSUA_Nome" value="<?=$_POST['FUSUA_Nome']?>" />
            <input type="hidden" name="FUSUA_Empresa" id="FUSUA_Empresa" value="<?=$_POST['FUSUA_Empresa']?>" />
            <input type="hidden" name="FUSUA_EmailEmpresa" id="FUSUA_EmailEmpresa" value="<?=$_POST['FUSUA_EmailEmpresa']?>" />
            <input type="hidden" name="FUSUA_Fone" id="FUSUA_Fone" value="<?=$_POST['FUSUA_Fone']?>" />
            <input type="hidden" name="FUSUA_Cargo" id="FUSUA_Cargo" value="<?=$_POST['FUSUA_Cargo']?>" />
            <input type="hidden" name="FUSUA_Endereco" id="FUSUA_Endereco" value="<?=$_POST['FUSUA_Endereco']?>" />
            <input type="hidden" name="FUSUA_Bairro" id="FUSUA_Bairro" value="<?=$_POST['FUSUA_Bairro']?>" />
            <input type="hidden" name="FUSUA_Cidade" id="FUSUA_Cidade" value="<?=$_POST['FUSUA_Cidade']?>" />
            <input type="hidden" name="FUSUA_UF" id="FUSUA_UF" value="<?=$_POST['FUSUA_UF']?>" />
            <input type="hidden" name="" id="" value="<?=$_POST['']?>" />
            <input type="hidden" name="" id="" value="<?=$_POST['']?>" />
            <input type="hidden" name="" id="" value="<?=$_POST['']?>" />
             
					 	<fieldset>
              <label><?=translate("�reas de Interesse")?>:</label>
              <p><?=translate("Selecione abaixo, quais �reas/Editorias que voc� deseja inserir not�cias enquanto colaborador. Caso queira selecionar apenas algumas �reas, digite uma parte do nome no campo Filtro.")?></p>
              <label for="filtroAreas">Filtro:</label>
      	    	<input type="text" name="filtroAreas" class="inputText" onkeyup="updateList('<?=$_GET[PARAMETER_NAME_FILE] ?>', 'ajaxLista', 'listaAreasCadastradas', this.value)" />
      	    	<br />
      	    	<br />
              <div id="listaAreasCadastradas">
      	    	<?=$view->listarAreasCadastradas() ?>
      	    	</div>
						</fieldset>					
					
			</fieldset>
      
			<input type="submit" name="Submit" value="Salvar" class="botao"> 
      </form>
	  
	  <iframe name="iframeColaborador" width="0" height="0"></iframe>
<script>
	frm = document.formulario;
	
	frm.FUSUA_Email.nomeDesc='E-mail';
	frm.FUSUA_Senha.nomeDesc='Senha';
	frm.FUSUA_SenhaConfirma.nomeDesc='Confirma��o de Senha';
	frm.FUSUA_Nome.nomeDesc='Nome do Colaborador';
   frm.FUSUA_Idioma.nomeDesc='';
	<? if(!$view->getID()){?>
		frm.FUSUA_ComoSoubePOutros.nomeDesc='Obs de como soube do Noticiador';
		frm.FUSUA_ComoSoubeOutros.nomeDesc='Obs de como soube da Bisa';
	<? }?>
	frm.FUSUA_Email.required='true';
	frm.FUSUA_Senha.required='true';
	frm.FUSUA_SenhaConfirma.required='true';
	frm.FUSUA_Nome.required='true';
	
	frm.FUSUA_Email.isMail='true';	
	
	function obrigaCampo(Obj){
		Obj.focus();
		Obj.required='true';
		
	}

	function desobrigaCampo(Obj){
		if (Obj.name=='FUSUA_CompSoube'){
			frm.FUSUA_ComoSoubePOutros.required='false';	
		}else{
			frm.FUSUA_ComoSoubeOutros.required='false';	
		}		
		
	}


	function checaSenha(){
		if (frm.FUSUA_SenhaConfirma.value==frm.FUSUA_Senha.value){
			return true;
		}else{
			alert('Senha n�o confere');
			frm.FUSUA_Senha.focus();
			return false;
			
		}		
		
	}
	
	function mudarSenha(){
		if(document.getElementById('divSenha').style.display=='none'){
			document.getElementById('divSenha').style.display='block';
			frm.FUSUA_Senha.value='';
			frm.FUSUA_Senha.focus();
			frm.FUSUA_SenhaConfirma.value='';
			frm.alterasenha.value=1;
		}else{
			frm.alterasenha.value='';
			document.getElementById('divSenha').style.display='none';
		}
	}
	

</script>
	</div>
</div>
<?=add_html_tail() ?>
		

<?php

    if ((sizeof($_POST)>0)&&(isset($_POST['atualizar_usuario']))){
        if ((isset($_POST['excluir']))&&($_POST['excluir']!="")){               
            $view->apagarUsuario($_POST['excluir']);
        }else{
            $view->atualizarUsuario($_POST['usu_id'], $_POST['clientes_associados'], $_POST['clientes_nao_associados'], $_POST['FPerfil_id'], $_POST);
        }
    }

?>
<?=add_html_head() ?>   
<div class="rounded_content_top">
        &nbsp;
</div>
<div class="rounded_content_middle">
        <div class="rounded_content">
     
        <h1 class="default_title"><?=translate("Gerenciar Usuários")?></h1>
                <form name="formulario" method="post" action="" >
                        <fieldset>
<?
                                $usuario = controller_seguranca::getInstance()->identificarUsuario();
                                if (!isset($_REQUEST['FUSUA_Email']) || empty($_REQUEST['FUSUA_Email'])) {
?>
                                <label for="FUSUA_Email"><?=translate("Filtrar/Localizar E-mail")?></label>
                                <input type="text" class="inputText" name="FUSUA_Email" id="FUSUA_Email" maxlength="100" value="" size="50" /><br /><br />
                                <label>Perfil:</label>&nbsp; 
<?
									if ($usuario->getPerfil() == 1) {
?>
                                <input type="radio" name="FUSUA_Perfil" class="checks" id="FUSUA_Perfil" value="1" />&nbsp;Administrador&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="FUSUA_Perfil" class="checks"  id="FUSUA_Perfil" value="3" />&nbsp;Editor&nbsp;&nbsp;&nbsp;
<?
									}
?>                              <!--MARIO CARDOSO-->
                                <input type="radio" name="FUSUA_Perfil"  class="checks" id="FUSUA_Perfil" value="2" />&nbsp;Colaborador&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="FUSUA_Perfil"  class="checks" id="FUSUA_Perfil" value="5" />&nbsp;Blogueiro&nbsp;&nbsp;&nbsp;
                                <input type="submit" value="" class="botaoEnviar" style="position: absolute; top: 63px; left: 304px;" onclick="return buscar()" />
                                <br/>
                                <br/>

<?
								}
?>

<? 
        $perfil = null;    
        if(isset($_POST['FUSUA_Perfil']) && !empty($_POST['FUSUA_Perfil'])) {
            $perfil = $_POST['FUSUA_Perfil']; 
            
        }

		//print "parte 1<br>";
		if(isset($_REQUEST['FUSUA_Email'])){

			//print "parte 2<br>";
            //echo $view->montarTabelaCadastrados($usuario);
            echo $view->listarUsuarios($perfil);
            
            $usuarios = $view->listarUsuarios($perfil);
            
			if(!isset($_POST['FUSUA_Perfil']) && !strpos($usuarios,'Nenhum registro encontrado')) {
				//print "parte 3<br>";

                $table = $view->getController()->obterUsuarioPorEmail($_REQUEST['FUSUA_Email']);
                $usu = $table->Rows();
                $usu = $usu[0];
                
                echo "<br /><br />";
                echo $view->listarClientes('A',$usu->usuario_id);
                echo "<br /><br />";
                echo $view->listarClientes('N',$usu->usuario_id);       
                
                echo '<input type="hidden" name="clientes_associados" id="clientes_associados" value="" />';
                echo '<input type="hidden" name="clientes_nao_associados" id="clientes_nao_associados" value="" />';
                echo '<input type="hidden" name="usuario_id" value="'.$usuario->getId().'" />';
                echo '<input type="submit"  name="atualizar_usuario" value="" class="botaoAtualizar" />';
?>

<?
			}
?>

<?
		}
?>

                </fieldset>

                </form>

<script>
        frm = document.formulario;

         function buscar(){
                    if((frm.FUSUA_Email.value=='' && !$(".checks").is(":checked"))) {
                    alert('Preencha o e-mail ou selecione um perfil para a busca');
                    return false;
            } else {
                    return true;
            }
        }

        $(document).ready(function(){
            $("#enviarsenha").live("click",function(){
                var url='index.php?action=enviarsenha&secao=gestao_colaborador';
                var senha=$("#senha").val();
                var usuario=$("#usuario").val();
                var email= $("#enviarsenha").attr('class');
                $.get(url, "senha="+senha+"&usuario="+usuario+"&email="+email, function(){alert("A senha do usuário foi enviada para o seu email.")}, "html");
            });

            //Seleção de clientes
            clientes_associados = new Array();
            $("input[type=checkbox][name='cliente_associado']:not(:checked)").each(
				function(){
					clientes_associados.push($(this).val());
					console.log($(this).val());
				}
			);
            $("input[type=checkbox][name='cliente_associado']").click(
				function(){
					if($(this).is(':not(:checked)')) {
						clientes_associados.push($(this).val());    
					} else {
						var val = $(this).val();
						clientes_associados = $.grep(
							clientes_associados
							, function (a) { return a != val; }
						);
					}
					$('#clientes_associados').val(clientes_associados);
					console.log($('#clientes_associados').val());
				}
			);

            //Seleção de clientes
            clientes_nao_associados = new Array();
            $("input[type=checkbox][name='cliente_nao_associado']:checked").each(
				function(){
					clientes_nao_associados.push($(this).val());
					console.log($(this).val());
				}
			);
            $("input[type=checkbox][name='cliente_nao_associado']").click(
				function(){
					if($(this).is(':checked')) {
						clientes_nao_associados.push($(this).val());    
					} else {
						var val = $(this).val();
						clientes_nao_associados = $.grep(
							clientes_nao_associados
							, function (a) { return a != val; }
						);
					}
					$('#clientes_nao_associados').val(clientes_nao_associados);
					console.log($('#clientes_nao_associados').val());
				}
			);

        });
        
        function confirma(usuario) {
            if(confirm("Deseja excluir o usuário?")) {
                parent.location.href= "index.php?action=delete&secao=gestao_colaborador&id="+usuario;
            } else {
                return false;
            }
        }
        
</script>

        </div>
</div>
<?=add_html_tail() ?>

<?= add_html_head() ?>	
<div class="rounded_content_top">
    &nbsp;
</div>
<div class="rounded_content_middle">
    <div class="rounded_content">
        <h1 class="default_title"><?= translate("Cadastrar Colaborador") ?></h1>
        <? $test = $_SESSION["SESSION_LANG"];
        $usuario = controller_seguranca::getInstance()->identificarUsuario();
        ?>
        <form name="formulario" id="formulario" method="post" action="<?= $view->add_form_process_url(); ?>" target="iframeColaborador">
            <? add_action_data("save", null, $view->getID()); ?>
            <fieldset class="colaboradordados">
                <p><?= translate("Seja bem vindo ao NoticiadorWeb 2.0.") ?></p>
                <p><?= translate("Cadastrando-se como Colaborador voc� poder� contribuir com not�cias e informa��es para quaisquer clientes do NoticiadorWeb/BISAWeb. As suas not�cias ap�s cadastradas precisar�o ser autorizadas pelo Editor Respons�vel da respectiva �rea/Cliente.") ?></p>
                <p><?= translate("Insira seus dados abaixo depois fa�a login/senha.") ?></p>
                <label for="FUSUA_Email">E-mail*:</label>
                <input type="text" class="inputText" name="FUSUA_Email" <? if ($view->getID()) {
                echo " readonly ";
            } ?> id="FUSUA_Email" value="<?= $view->getUSUA_Email() ?>" size="80" />
<? if ($view->getID()) {
    echo "<a href='#' onclick='mudarSenha()'>" . translate("Alterar Senha") . "</a>";
} ?>							
                <div id='divSenha' style='display:<? if ($view->getID()) {
    echo "none";
} ?>;'>
                    <input type="hidden" name="alterasenha" id="alterasenha" value="" />
                    <label for="FUSUA_Senha" name="teste">Senha*:</label>			
                    <input type="password" class="inputText" name="FUSUA_Senha" id="FUSUA_Senha" maxlength="20" value="<?= $view->getUSUA_Senha() ?>" /> 
                    <br><label for="FUSUA_SenhaConfirma"><?= translate("Confirma��o*:") ?></label>
                    <input type="password" class="inputText" name="FUSUA_SenhaConfirma" id="FUSUA_SenhaConfirma" maxlength="20" value="<?= $view->getUSUA_Senha() ?>" /> 
                </div>
                <label for="FUSUA_Idioma">Idioma:</label>
                <select name="FUSUA_Idioma" class="inputText" id="FUSUA_Idioma">					   
                    <?if($view-> getUSUA_Idioma()=='es-ES'){?>
                    <option value="es-ES">Espanhol</option>
                    <option value="pt-BR"><?= translate("Portugu�s") ?></option>
                        <?}else{?>
                    <option value="pt-BR"><?= translate("Portugu�s") ?></option>
                    <option value="es-ES">Espanhol</option>
                    <?}?>
                </select>

                <label for="FUSUA_Nome">Nome*:</label>
                <input type="text" class="inputText" name="FUSUA_Nome" id="FUSUA_Nome" maxlength="100" value="<?= $view->getUSUA_Nome() ?>" size="80" /> 
                <br>
                <label for="FUSUA_Empresa">Empresa/Entidade:</label>
                <input type="text" class="inputText" name="FUSUA_Empresa" id="FUSUA_Empresa" maxlength="60" value="<?= $view->getUSUA_Empresa() ?>" size="80" /> 
                <br><label for="FUSUA_EmailEmpresa">E-mail Empresa:</label>
                <input type="text" class="inputText" name="FUSUA_EmailEmpresa" id="FUSUA_EmailEmpresa" value="<?= $view->getUSUA_EmailEmpresa() ?>" size="80" />
                <label for="FUSUA_Fone"><?= translate("Fone Contato") ?></label>
                <input type="text" class="inputText" name="FUSUA_Fone" id="FUSUA_Fone" value="<?= $view->getUSUA_Fone() ?>" onkeyup="this.value = formatar(this.value,'(99) 9999-9999')" maxlength="14" /><br />
                <label for="FUSUA_Cargo"><?= translate("Cargo") ?>:</label>
                <input type="text" class="inputText" name="FUSUA_Cargo" id="FUSUA_Cargo" maxlength="30" value="<?= $view->getUSUA_Cargo() ?>" size="40"/><br /> 
            </fieldset>

            <fieldset class="colaboradordados">
                <label for="FUSUA_Endereco"><?= translate("Endere�o:") ?></label>
                <input type="text" class="inputText" name="FUSUA_Endereco"  id="FUSUA_Endereco" maxlength="100" value="<?= $view->getUSUA_Endereco() ?>" size="80" /> 

                <br><label for="FUSUA_Bairro"><?= translate("Bairro") ?>:</label>
                <input type="text" class="inputText" name="FUSUA_Bairro" id="FUSUA_Bairro" maxlength="50" value="<?= $view->getUSUA_Bairro() ?>" size="40" />

                <br><label for="FUSUA_Cidade"><?= translate("Cidade") ?>:</label>
                <input type="text" class="inputText" name="FUSUA_Cidade" id="FUSUA_Cidade" maxlength="30" value="<?= $view->getUSUA_Cidade() ?>" size="40" />

                <br><label for="FUSUA_UF"><?= translate("Estado") ?>:</label>
                <select name="FUSUA_UF" class="inputText" id="FUSUA_UF">					   
                    <option value=""></option>
<? $view->obterEstados($view->getUSUA_UF()) ?>
                </select>
                <br>
                <label for="FUSUA_CEP"><?= translate("CEP:") ?></label>
                <input name="FUSUA_CEP" class="inputText" type="text" onkeyup="this.value = formatar(this.value,'99.999-999')"  maxlength="10" id="FUSUA_CEP" value="<?= $view->getUSUA_CEP() ?>" />
            </fieldset>


<? if (!$view->getID()) { ?>
                <fieldset class="colaboradordados">
                    <label>Como soube da Bisa:</label>
                    <input type="radio" name="FUSUA_ComoSoube" id="FUSUA_ComoSoube" <? if ($view->getUSUA_ComoSoube() == "A")
        echo " checked " ?> value="A" /><?= translate("An�ncio em Jornal/Revista") ?><br />
                    <input type="radio" name="FUSUA_ComoSoube" id="FUSUA_ComoSoube" <? if ($view->getUSUA_ComoSoube() == "R")
        echo " checked " ?> value="R" /><?= translate("Recebeu / Pegou Jornal da Bisa") ?><br />
                    <input type="radio" name="FUSUA_ComoSoube" id="FUSUA_ComoSoube" <? if ($view->getUSUA_ComoSoube() == "S")
        echo " checked " ?> value="S" /><?= translate("Sites de Buscas") ?><br />
                    <input type="radio" name="FUSUA_ComoSoube" id="FUSUA_ComoSoube" <? if ($view->getUSUA_ComoSoube() == "U")
        echo " checked " ?>  value="U" />Um amigo indicou<br />
                    <input type="radio" name="FUSUA_ComoSoube" id="FUSUA_ComoSoube" value="E" <? if ($view->getUSUA_ComoSoube() == "E")
        echo " checked " ?> /><?= translate("Recebeu email da Bisa (bisa mail)") ?><br />
                    <input type="radio" name="FUSUA_ComoSoube"  id="FUSUA_ComoSoube" <? if ($view->getUSUA_ComoSoube() == "O")
        echo " checked " ?> value="O" /><?= translate("Outros") ?>:  <input type="text" class="inputText" name="FUSUA_ComoSoubeOutros" value="<?= $view->getUSUA_ComoSoubeOutros() ?>" maxlength="100" size="80" /><br />
                </fieldset>

                <fieldset class="colaboradordados">

                    <label><?= translate("Como soube do Notificador Web") ?>:</label>
                    <input type="radio" name="FUSUA_CompSoube" id="FUSUA_CompSoube" <? if ($view->getUSUA_CompSoube() == "A")
        echo " checked " ?>   value="A" /><?= translate("An�ncio em Jornal/Revista") ?> <br>
                    <input type="radio" name="FUSUA_CompSoube" id="FUSUA_CompSoube" <? if ($view->getUSUA_CompSoube() == "R")
        echo " checked " ?>  value="R" /><?= translate("Recebeu / Pegou Jornal da Bisa") ?> <br>
                    <input type="radio" name="FUSUA_CompSoube" id="FUSUA_CompSoube" <? if ($view->getUSUA_CompSoube() == "S")
        echo " checked " ?> value="S"/><?= translate("Sites de Buscas") ?> 
                    <br />
                    <input type="radio" name="FUSUA_CompSoube" id="FUSUA_CompSoube" <? if ($view->getUSUA_CompSoube() == "U")
        echo " checked " ?> value="U" /><?= translate("Um amigo indicou") ?>  <br />
                    <input type="radio" name="FUSUA_CompSoube" id="FUSUA_CompSoube" <? if ($view->getUSUA_CompSoube() == "E")
                    echo " checked " ?> value="E" /><?= translate("Recebeu email da Bisa (bisa mail)") ?><br />
                    <input type="radio" name="FUSUA_CompSoube" id="FUSUA_CompSoube" <? if ($view->getUSUA_CompSoube() == "H")
                    echo " checked " ?>  value="H" />Home Page<br />
                    <input type="radio" name="FUSUA_CompSoube" id="FUSUA_CompSoubeO" <? if ($view->getUSUA_CompSoube() == "O")
                    echo " checked " ?>  value="O" /><?= translate("Outros") ?>: <input type="text" class="inputText" name="FUSUA_ComoSoubePOutros"  maxlength="100" value="<?= $view->getUSUA_ComoSoubePOutros() ?>" size="80" /><br />

                    <? } ?>
                <input type="button" name="Areas" class="mostrar" onclick="return false;" value=<?= translate("Continuar") ?> class="botao" />

            </fieldset>

            <fieldset class="colaboradorarea">
                <label><?= translate("�reas de Interesse") ?>:</label>
                <p><?= translate("Selecione abaixo, quais �reas/Editorias que voc� deseja inserir not�cias enquanto colaborador. Caso queira selecionar apenas algumas �reas, digite uma parte do nome no campo Filtro.") ?></p>

<?
if ($view->getID()) {
    echo $view->listarAreasSelecionadas();
}
?>
<?
// cuando o usuario se cadastra pra colaborar sim login
if ($usuario==null){
    ?>
                <br />
                <label for="filtroAreas">Filtro:</label>
                <input type="text" value=""  name="filtroAreas" class="inputText" onkeyup="updateList('<?= $_GET[PARAMETER_NAME_FILE] ?>', 'ajaxLista', 'listaAreasCadastradas', this.value)" />
                <br />
                <br />
                <div id="listaAreasCadastradas">
                <?=$view->listarAreasNaoSelecionadas()?>
                <div id="pager" class="pager" >
                        <img src="images/first.png" class="first"/>
                        <img src="images/prev.png" class="prev"/>
                        <input type="text" class="pagedisplay"/>
                        <img src="images/next.png" class="next"/>
                        <img src="images/last.png" class="last"/>
                        <select class="pagesize">
                            <option selected="selected"  value="25">25</option>
                            <option value="50">50</option>
                            <option value="75">75</option>
                            <option  value="100">100</option>
                        </select>
                    </div>
                </div>
                <?
} else {
    //quando � o administrador ou o colaborador quem quer modificar suas areas
    if($usuario->getPerfil()!=3){
  ?>
                <br />
                <label for="filtroAreas">Filtro:</label>
                <input type="text" value=""  name="filtroAreas" class="inputText" onkeyup="updateList('<?= $_GET[PARAMETER_NAME_FILE] ?>', 'ajaxLista', 'listaAreasCadastradas', this.value)" />
                <br />
                <br />
                <div id="listaAreasCadastradas">
                <?=$view->listarAreasNaoSelecionadas()?>
                <div id="pager" class="pager" >
                        <img src="images/first.png" class="first"/>
                        <img src="images/prev.png" class="prev"/>
                        <input type="text" class="pagedisplay"/>
                        <img src="images/next.png" class="next"/>
                        <img src="images/last.png" class="last"/>
                        <select class="pagesize">
                            <option selected="selected"  value="25">25</option>
                            <option value="50">50</option>
                            <option value="75">75</option>
                            <option  value="100">100</option>
                        </select>
                    </div>  
                </div>
                <?  }else{
                   if(!$view->getID()){
                       //quando o editor quer modificar as areas
                       echo $view->listarAreasCadastradas();
                   }
                }
}
?>  
                <br/>
                <input type="button" name="Voltar" class="voltar" value="<?= translate("Voltar") ?>" class="botao" style="float: left; margin: 5px 10px 0 0;" />
                <input type="submit" name="Submit" class="enviar" value="<?= translate("Salvar") ?>" class="botao" /> 

            </fieldset>




        </form>

        <iframe name="iframeColaborador" width="0" height="0" frameborder="0"></iframe>
        <script>
	frm = document.formulario;

	function obrigaCampo(Obj){
		Obj.focus();
		Obj.required='true';

	}

	function desobrigaCampo(Obj){
		if (Obj.name=='FUSUA_CompSoube'){
			frm.FUSUA_ComoSoubePOutros.required='false';
		} else {
			frm.FUSUA_ComoSoubeOutros.required='false';
		}

	}


	function checaSenha(){
		if (frm.FUSUA_SenhaConfirma.value==frm.FUSUA_Senha.value){
			return true;
		} else {
			alert('Senha n�o confere');
			frm.FUSUA_Senha.focus();
			return false;

		}

	}

	function mudarSenha(){
		if (document.getElementById('divSenha').style.display=='none'){
			document.getElementById('divSenha').style.display='block';
			frm.FUSUA_Senha.value='';
			frm.FUSUA_Senha.focus();
			frm.FUSUA_SenhaConfirma.value='';
			frm.alterasenha.value=1;
		} else {
			frm.alterasenha.value='';
			document.getElementById('divSenha').style.display='none';
		}
	}

	function validaFormEspecifico(lang) {

		var validate = true;

		var frm = document.formulario;

		var radioOutros = false;
		var radioPOutros = false;

		var checkArea = false;
        

		//alert(frm.elements.length);

		for (i = 0; i < frm.elements.length; i++) {

			//alert(i);

			//alert(frm.elements[i].name);

			if (frm.elements[i].name == "FUSUA_Email" && trim(frm.elements[i].value) == "") {

				if (lang=="pt-BR"){
					alert("Por favor, preencha o campo E-mail");
				}
				if (lang=="es-ES"){
					alert("Por favor llene el campo E-mail");           
				}
				frm.elements[i].focus();
				validate = false;
				break;
			}

			if (frm.elements[i].name == "FUSUA_Email" && trim(frm.elements[i].value) != "") {

				var email = frm.elements[i].value;

				var re = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

				var invalido = false;

				if (email.search('@') == -1) {
					invalido = true;
				}
				if (email.search(re) == -1) {
					invalido = true;
				}

				if (invalido) {
					if (lang=="pt-BR"){
						alert("Por favor, preencha o campo E-mail com um endere�o de email v�lido");
					}
					if (lang=="es-ES"){
						alert("Por favor llene el campo E-mail com um endere�o de email v�lido");           
					}
					frm.elements[i].focus();
					validate = false;
					break;
				}

			}

			if (frm.elements[i].name == "FUSUA_Senha" && trim(frm.elements[i].value) == "") {

				if (lang=="pt-BR"){
					alert("Por favor, preencha o campo Senha");
				}
				if (lang=="es-ES"){
					alert("Por favor llene el campo Senha");           
				}
				frm.elements[i].focus();
				validate = false;
				break;
			}

			if (frm.elements[i].name == "FUSUA_SenhaConfirma" && trim(frm.elements[i].value) == "") {

				if (lang=="pt-BR"){
					alert("Por favor, preencha o campo de confirma��o da senha");
				}
				if (lang=="es-ES"){
					alert("Por favor llene el campo de confirma��o da senha ");           
				}
				frm.elements[i].focus();
				validate = false;
				break;
			}

			if (frm.elements[i].name == "FUSUA_Nome" && trim(frm.elements[i].value) == "") {

				if (lang=="pt-BR"){
					alert("Por favor, preencha o campo Nome");
				}
				if (lang=="es-ES"){
					alert("Por favor llene el campo Nome");           
				}
				frm.elements[i].focus();
				validate = false;
				break;
			}

			if (!radioOutros) {
				if (frm.elements[i].type == "radio" && frm.elements[i].name == "FUSUA_ComoSoube"){

					var objRadio = document.getElementsByName(frm.elements[i].name);
					var tamanho = objRadio.length;
					var marcado = false;

					for (indice=0; indice < tamanho; indice++) {
						if (objRadio[indice].checked == true) {

							marcado = true;
							if (objRadio[indice].value == "O") {
								radioOutros = true;
							}
						}
					}

					if (marcado == false) {

						if (lang=="pt-BR"){
							alert("� necess�rio marcar uma das op��es do campo Como soube da Bisa");
						}
						if (lang=="es-ES"){
							alert("Es necesario marcar una de las opciones de campo Como soube da Bisa");           
						}

						frm.elements[i].focus();
						validate = false;
						break;

					}
				}
			}

			if (radioOutros) {
				if (frm.elements[i].name == "FUSUA_ComoSoubeOutros" && trim(frm.elements[i].value) == "" ) {

					if (lang=="pt-BR"){
						alert("Por favor, preencha o campo Outros");
					}
					if (lang=="es-ES"){
						alert("Por favor llene el campo Outros");           
					}
					frm.elements[i].focus();
					validate = false;
					break;
				}
			}

			if (!radioPOutros) {
				if (frm.elements[i].type == "radio" && frm.elements[i].name == "FUSUA_CompSoube") {

					var objRadio = document.getElementsByName(frm.elements[i].name);
					var tamanho = objRadio.length;
					var marcado = false;

					for (indice=0; indice < tamanho; indice++){
						if (objRadio[indice].checked == true){

							marcado = true;
							if (objRadio[indice].value == "O") {
								radioPOutros = true;
							}
						}
					}

					if (marcado == false){

						if (lang=="pt-BR"){
							alert("� necess�rio marcar uma das op��es do campo Como soube do Notificador Web");
						}
						if (lang=="es-ES"){
							alert("Es necesario marcar una de las opciones de campo Como soube do Notificador Web");           
						}

						frm.elements[i].focus();
						validate = false;
						break;

					}
				}
			}

			if (radioPOutros) {
				if (frm.elements[i].name == "FUSUA_ComoSoubePOutros" && trim(frm.elements[i].value) == "" && document.getElementById('FUSUA_CompSoubeO').checked) {

					if (lang=="pt-BR"){
						alert("Por favor, preencha o campo Outros");
					}
					if (lang=="es-ES"){
						alert("Por favor llene el campo Outros");           
					}
					frm.elements[i].focus();
					validate = false;
					break;
				}
			}

		}

		return validate;
	}

	function enviarDadosUsuario() {

		var frm = document.formulario;

		//alert(frm.elements.length);

		var areasSelecionadas = false;

		for (i = 0; i < frm.elements.length; i++) {

			//alert(frm.elements[i].name);

			if (frm.elements[i].type == "checkbox" && frm.elements[i].name == "area[]"){

				//alert("aqui");

				var objRadio = document.getElementsByName(frm.elements[i].name);
				var tamanho = objRadio.length;

				//alert(tamanho);

				for (indice=0; indice < tamanho; indice++) {
					if (objRadio[indice].checked == true) {

						areasSelecionadas = true;
					}
				}

				if (!areasSelecionadas) {

					alert("� necess�rio selecionar pelo menos uma �rea de interesse!");

					objRadio[0].focus();
					break;

				}
			}
		}

		if (areasSelecionadas) {
			document.formulario.submit();
		}

	}

	$(document).ready(function(){


		$(".colaboradorarea").css("display", "none");

		$('.mostrar').click(function() {

			if (validaFormEspecifico('<? echo $test ?>')) {

				if (checaSenha()) {
					$(".colaboradordados").css("display", "none");
					$(".colaboradorarea").css("display", "block");
					$(this).hide();
				}

			}

		});

		$('.voltar').click(function() {

			$(".colaboradordados").css("display", "block");
			$(".colaboradorarea").css("display", "none");
			$(".mostrar").show();
		});

		$('.enviar').click(

			function() {
				enviarDadosUsuario();
			}
		);

	});

        </script>
    </div>
</div>
<?= add_html_tail() ?>
		

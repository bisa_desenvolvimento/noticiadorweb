<?
if ((sizeof($_POST) > 0) && (isset($_POST['atualizar_area_noticia']))) {
    if ((isset($_POST['excluir'])) && ($_POST['excluir'] != "")) {
        $view->apagarNoticias($_POST['excluir']);
    }
    $view->atualizarNoticias($_POST['noticias']);
}

$pag = isset($_GET['pag']) ? $_GET['pag'] : '1';

$urlAction = "index.php?".PARAMETER_NAME_ACTION."=show&";
$urlAction .= PARAMETER_NAME_FILE."=gestao_noticia";
?>
<?= add_html_head() ?>

<div class="rounded_content_top">
    &nbsp;
</div>
<div class="rounded_content_middle topBar">
    <div class="rounded_content">
        <div class="topBar_title">
            <img src="<?= DIR_ICONS ?>lupa.png" width="28" height="28"  />
            <h1><?= translate("Selecionar Notícia") ?></h1>
        </div>
        <div class="topBar_content">
            <form name="formulario" method="post" action="<?=$urlAction?>" onsubmit="return validaForm(0)">     
                <table width="100%" border="0">
                    <tr>
                        <td colspan="3" height="10">
                            <div align="left" style="padding: 3px; padding-top: 7px;">
                                <strong><?= translate("Palavra-chave") ?></strong>
                                <input type="text" name="FPalavra" id="FPalavra" value="<?php if(isset($_REQUEST['FPalavra'])) { echo $_REQUEST['FPalavra']; } ?>" onkeypress="showCheck()"  size="35" />
                                <strong><?= translate("Conteúdo Exato") ?></strong>
                                <input type="checkbox" id="FExato" name="FExato" value="FExato">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div align="left" style="padding: 3px;">
                                <strong><?= translate("área") ?></strong>
                            </div>
                        </td>
                        <td colspan="3">
                            <div align="left" style="padding: 3px;">
                                <strong><?= translate("Data") ?> </strong>(<?= translate("dd/mm/aaaa") ?>)
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <select name="FArea" id="select2" class="comboArea">
                                <option value="0"><?=translate("Todas")?></option>
                                <?$view->montarComboAreasNoticia(); ?>
                            </select>
                        </td>
                        <td>
                            <label for="textfield"></label>
                            <input type="text" name="FDataInicial" id="FDataInicial" maxlength="10" value="<?php if(isset($_REQUEST['FDataInicial'])) { echo $_REQUEST['FDataInicial']; } ?>" size="10" class="txtData" onkeyup="this.value = formatar(this.value,'99/99/9999')"/> a 
                            <input type="text" name="FDataFinal" id="FDataFinal" size="10" maxlength="10" value="<?php if(isset($_REQUEST['FDataFinal'])) { echo $_REQUEST['FDataFinal']; } ?>" class="txtData" onkeyup="this.value = formatar(this.value,'99/99/9999')"/>
                        </td>
                        <td colspan="2" align="center">
                            <label for="button"></label>
                            <input type="submit" name="button" id="button" value="Buscar" style="cursor: pointer;" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
                    <div class="rounded_content_bottom topBarBottom">
                        &nbsp;
                    </div>

                    <div class="rounded_content_top">
                        &nbsp;
                    </div>
                    <div class="rounded_content_middle">
                        <div class="rounded_content">
                            <h1 class="default_title"><?= translate("Gerenciar Notícias") ?></h1>
                            
                            <table class="formatted">
                            <?echo $view->montarTabelaNoticiasCadastradas($pag); ?>            
                            </table>
                        </div>
                        
                    </div>
                        <div id="compatilhar-modal" title="Compartilhar" >
                            
                            <div id="twitter"></div>
                        </div>

                    <? echo $view->montarTabelaCadastrados(); ?>
<script>
        frm = document.formulario;
        frm.FDataInicial.nomeDesc='Data Inicial';   
        frm.FDataFinal.nomeDesc='Data Final';   
        frm.FDataInicial.isDate='true'; 
        frm.FDataFinal.isDate='true';
        function popup(URL){
       window.open(URL,"ventana1","width=600,height=400,scrollbars=NO")
          }

    $(document).ready(function(){
        
            $.fx.speeds._default = 200;
                $( ".compatilhar-modal").dialog({
            autoOpen: false,
            show: "bounce",
            modal: true
                        });
                $( "#dialog-modal" ).dialog({
            height: 160,
            modal: true
        });
        
        $( ".compartilhar" ).click(function(){
            $(this).next(".redes").slideToggle();
        });
      
    });

</script>
<script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script>
<?= add_html_tail() ?>
<?=add_html_head() ?>
<script type="text/javascript">
    function optionCheck() {
        if(document.getElementById("FLinkNoticia").checked) {
            document.getElementById("FLinkmateriacompleta").value = "";
            document.getElementById("FLinkmateriacompleta").style.display = "none";
            
       
        }else if(!document.getElementById("FLinkNoticia").checked){
            
            document.getElementById("FLinkmateriacompleta").style.display = "inline";
       
        }else {
            document.getElementById("FLinkmateriacompleta").style.display = "inline";
        }
    }

    function validation() {
        // IkaroSales 12/08/2016
        var titulo    = document.getElementById("FTitulo");
        var data      = document.getElementById("FDatapublicacao");
        var dataFinal = document.getElementById("FDatafinal");
        var resumo    = document.getElementById("FResumomateria");
        var noticia   = document.getElementById("FMateriacompleta");
        var links = document.getElementsByClassName("links");
        var nomes = document.getElementsByClassName("nomes");
        var clientes = document.getElementById("clientes");

        var cliente = clientes.options[clientes.selectedIndex].value;
        

        if(titulo.value.trim() === ''){
            titulo.focus(); alert("Por favor, preencha o Título da Notícia.");
            return false;
        } else if(data.value.trim() === '') {
            data.focus(); alert("Por favor, preencha a Data da publicação.");
            return false;
        } else if(dataFinal.value.trim() === '') {
            dataFinal.focus(); alert("Por favor, preencha o Data final da publicação.");
            return false;
        } else if(resumo.value.trim() === '') {
            resumo.focus(); alert("Por favor, preencha o Resumo da Notícia.");
            return false;
        }else if(cliente!="noticiador"){
            
            if(cliente=="null"){
                alert("Prezado usuário,\n" 
                      +"Quando você seleciona no campo 'Visualizar notícia em' um visualizador de notícias diferente do 'NoticiadorWeb', é necessário que seja informado no cadastro do cliente qual o endereço do site. "+
                    "Para informar o endereço do site, siga o passo a passo abaixo:\n\n"+

                    "1 - Cliente;\n"+
                    "2 - Gerenciar Clientes\n"+
                    "3 - Informe o endereço do site no espaço 'Digite aqui o link das noticias docliente.\n'"+
                    "4 - Salvar");
                    return false;
            }
            /*for(i = 0; i < nomes.length; i++){
                           
                if (nomes[i].innerHTML==cliente){
                    alert("entrou");
                    if(links[i].innerHTML=="null"){
                        alert("O cliente ainda não possui link associado. Para associar, siga o caminho da Página inicial:      Principal->Cliente->Gerenciar;     Cliente escolha o cliente e então associe o link de noticias dele");
                        return false;
                   }
                } 
            }*/

        } else if(noticia.value == noticia.value) {
            var content = CKEDITOR.instances.FMateriacompleta.getData();
            var cont = content.replace(/(<([^>]+)>)/ig, ""); // Retirar tags HTML.
            if(cont.trim() === '' || cont.trim().length < 50) {
                CKEDITOR.instances.FMateriacompleta.focus();
                alert("Por favor, preencha a Notícia. Utilize no mínimo 50 caracteres");
                return false;
            }
        } else{

            return true;
        }

        return true;


    }
</script>

<!-- <script type="text/javascript"> Jorge Roberto 14/10/2016

function validation() {
    var titulo = document.getElementById("FTitulo").value;
    var data = document.getElementById("FDatapublicacao").value;
    var dataFinal = document.getElementById("FDatafinal").value;
    var resumo = document.getElementById("FResumomateria").value;
    var noticia = document.getElementById("FMateriacompleta").value;
    var linkCliente = document.getElementById("FLinkNoticiaCliente");
    var links = document.getElementsByClassName("links");


    
    


    if(titulo.trim() === ''){
        alert("Por favor, preencha o Título da Notícia.");
        return false;
    } else if(data.trim() === '') {
        alert("Por favor, preencha a Data da publicação.");
        return false;
    } else if(dataFinal.trim() === '') {
        alert("Por favor, preencha o Data final da publicação.");
        return false;
    } else if(resumo.trim() === '') {
        alert("Por favor, preencha o Resumo da Notícia.");
        return false;
    }else if(linkCliente.checked){
        alert(links.length);
        for(i = 0; i < links.length; i++){
               alert(links[i].innerHTML);

            if (links[i].textContent=="null"){
               alert("O cliente ainda não possui link associado. Para associar, siga o caminho da Página inicial:      Principal->Cliente->Gerenciar;     Cliente escolha o cliente e então associe o link de noticias dele");
                return false;
            }
            return false;
        }

    }
            
    } else if(noticia.trim().length <= 50) {
        alert("Por favor, preencha a Notícia.\nUtilize no mínimo 50 caracteres");
        return false;
    } else {
        return true;
    }
}
</script> -->

<div class="rounded_content_top">&nbsp;</div>
<div class="rounded_content_middle">
    <div class="rounded_content">
    <?
        if(isset($_SESSION["SESSION_LANG"])) {
            $test = $_SESSION["SESSION_LANG"];
        }
    ?>
    <h1 class="default_title"><?=translate("Cadastrar Notícia")?></h1>
    <form name="formulario" id="formulario" enctype="multipart/form-data" method="post" action="<?=$view->add_form_process_url();?>" onsubmit="return validation()" >
        <? add_action_data("save", null, $view->getId()); add_hidden_api();?>
        <input name="FId" id="FId" type="hidden" value="<?=$view->getId() ?>"/>
        <table width="100%" class="form noticia" border="0">
            <tr>
                <td>
                    <label for="titulo"><?=translate("Título")?>:</label><br />
                    <input name="FTitulo" type="text" id="FTitulo" size="53" value="<? if(isset($_SESSION['datosback'])&&!isset($_GET['id'])){echo $_SESSION['datosback']['FTitulo'];} else { ?><?=$view->getTitulo()?><?}?>" maxlength="250"/>
                </td>
                <td>
                    <label for="publicar"><?=translate("Publicar a partir de")?>:</label><br />
                    <input type="text"  class="inputText" value="<? if ($view->getPublicacao()){ echo DateExibe($view->getPublicacao()); } else { echo date('d/m/Y');}?>" name="FDatapublicacao" id="FDatapublicacao" onKeyUp="this.value = formatar(this.value,'99/99/9999')"  maxlength="10" size="18" />
                </td>
                <td>
                    <label for="publicar"><?=translate("Publicar até")?>:</label><br />
                    <input type="text"  class="inputText" value="<? if(isset($_SESSION['datosback'])&&!isset($_GET['id'])){echo $_SESSION['datosback']['FDatafinal'];} else{if ($view->getValidade()){echo DateExibe($view->getValidade());} }?>" name="FDatafinal" id="FDatafinal" onkeyup="this.value = formatar(this.value,'99/99/9999')"  maxlength="10" size="14"/>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="available_chars"><?=translate("caracteres recomendados: ")?><label id="charResumo">255</label></div>
                    <label for="resumo"><?=translate("Resumo")?>: </label><br />
                    <textarea id="FResumomateria" name="FResumomateria" class="width100percent clearRight" onkeyup="LimitarCaracter(255, this.name, 'charResumo')" id="FResumomateria"  rows="5"><?if(isset($_SESSION['datosback'])&&!isset($_GET['id'])){echo $_SESSION['datosback']['FResumomateria'];}else{?><?=$view->getChamada()?><?}?></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <label for="noticia"><?=translate("Notícia Completa")?>:</label><br />
                    <textarea class="FMateriacompleta ckeditor" cols="80" id="FMateriacompleta" name="FMateriacompleta" rows="10"><?if(isset($_SESSION['datosback'])&&!isset($_GET['id'])){echo $_SESSION['datosback']['FMateriacompleta'];}else{?><?=$view->getTexto()?><?}?></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="1"></td>
                <td></td>
                <td></td>
            </tr>
            
            <tr>
                <td colspan="2">
                    <?  $thumbY = false;
                        $thumbnail = $view->getThumbnail(); 
                        if(isset($_GET['id']) && $thumbnail ) {
                            $urlThumb = "";
                            if (strpos($thumbnail, "thumb_") !== false)
                                $urlThumb = URL."plugins/timthumb.php?src=".URL."users/public/thumb/".$thumbnail."&w=100&h=100";
                            else
                                $urlThumb = URL."plugins/timthumb.php?src=".URL.$thumbnail."&w=100&h=100";
                            if (strpos($thumbnail, "https://") !== false)
                                $urlThumb = URL."plugins/timthumb.php?src=".$thumbnail."&w=100&h=100";
                            $thumbY = true;
                        } else {
                            $urlThumb = '';
                        } ?>
                    <input id="FThumbnailVal" name="FThumbnailVal" type="hidden" value="<?=$thumbnail ?>"/>
                    <div id="thumbY">
                        <br><label for="FThumbnail"><?=translate("Imagem em miniatura (thumbnail)")?>:</label><br/>
                        <img id="imgFThumbnail" src="<?=$urlThumb?>" alt="thumbnail" />
                        <input type="button" id="apagar-thumbnail" thumb="<?=$thumbnail?>" value="Apagar Thumbnail" class="botao">
                    </div>
                    <!--div id="thumbN">
                        <label for="FThumbnail"><?=translate("Imagem em miniatura (thumbnail)")?>:</label><br/>
                        <input type="file" name="FThumbnail" id="FThumbnail" size="80" accept=".jpg,.jpeg,.png,.gif" />
                    </div-->
                </td>
<!--                <td>-->
<!--                    <br/><br/><input type="button" value="" class="botaoEnviar mostrar">-->
<!--                </td>-->
            </tr>
        </table> 
        <br />

        <label for="FLinkmateriacompleta"><?=translate("Linkddd da Matéria Completa")?>:</label><br>
                    <input type="text" class="inputText" value="<? if(isset($_SESSION['datosback'])&&!isset($_GET['id'])){ echo $_SESSION['datosback']['FLinkmateriacompleta']; } else { ?><?=$view->getLink()?>
                    <? } ?>" name="FLinkmateriacompleta" id="FLinkmateriacompleta" maxlength="255" size="100%" />
                    
                    <div>
                        <input type="checkbox" name="FLinkNoticia" id="FLinkNoticia" onclick="optionCheck()" value="FLinkNoticia"> 
                        Preencher com o link da notícia que será geradoddddd
                    </div>
                    <div  id="links" style="display:flex">
                        <?echo $view->linksClientes()?>
                        
                    </div>
                    <div style="margin-bottom:20px; margin-top:20px;">
                        
                        Visualizar notícia em: 
                        
                            <select id="clientes" name="clientesN"  style="height: 30px;" >
                                <?=$view->listarClientes();?>
                            </select>
                    </div>
                   

        <fieldset class="area">
            <label><?=translate("áreas")?>:</label>
                <p><?=translate("Selecione abaixo em quais áreas você deseja inserir esta notícia.")?></p>
                <div id="listaAreasCadastradas">
                    <!-- <?=$view->listarAreasCadastradas()?> -->
                    <?=$view->listarAreas()?>
                </div>
        </fieldset>

                    
            
      
        <div class="clear"></div>
        <input type="button" value="" class="botaoVoltar" id="voltar" style="margin-top: 0px!important;" />
        <input type="submit" value="" class="botaoEnviar" id="enviar" style="margin-top: 0px!important;" />
        <!-- Button Send -->
    </form>
        <div class="clear"></div>
    </div>
</div>      
<!--iframe id="processa" name="processa" width="0" height="0"></iframe-->

<!--script que busca clientes relativos as áreas selecionadas e adiciona ao select-->
<script type="text/javascript">
    var select = document.getElementById('clientes');
    var areas = document.getElementsByName('area[]');
    
    
</script>

<script>
    frm = document.formulario;
        
    frm.FTitulo.nomeDesc='Título';  
    frm.FDatafinal.nomeDesc='Data Final';   
    frm.FResumomateria.nomeDesc='Resumo da Matéria';    
    frm.FMateriacompleta.nomeDesc='Matéria Completa';   
        
  
    $(document).ready(function(){
<? if (!$thumbY) { ?>
        $("#thumbY").hide();
        //$("#thumbN").show();
<? } ?>

<? if ($thumbY) { ?>
    $("#thumbY").show();
    //$("#thumbN").hide();
<? } ?>
$("#apagar-thumbnail").click(function(){
    <? if (strpos($thumbnail, "thumb_") !== false) { ?>
        image = $(this).attr('thumb');
        $.ajax({
            type: "POST",
            url: "<?=URL?>/index.php?action=apagarImagem&secao=noticia",
            data: { image: image, noticia: <? echo isset($_GET['id']) ? $_GET['id'] : '""'; ?> }
        }) .done (
            function( msg ) {
                if (msg.trim() == 'ok') {
                    //$("#thumbN").show();
                    $("#thumbY").hide();
                }
            }
        );
    <? } else { ?>
        $("#thumbY").hide();
        $("#imgFThumbnail").attr("src", "");
        $("#FThumbnailVal").val("");
    <? } ?>
});
    var config = {
        height: 100,
        toolbar: [['Image']]
    };

       // CKEDITOR.replace( 'FResumomateria',config);
       
       // oculta o botao voltar e as áreas de interesse
        //$("#voltar").css("display", "none");
        //$(".area").css("display", "none");

     // se ta cadastrando pela integracao ou está atualizando uma noticia
    <? if($view->getID()||checkapi()){ ?>
        
        $(".mostrar").css("display", "none");
        $("#enviar").css("display", "block");
        $('.botaoEnviar').click(function() {
            if(!validaNoticia()){
                return false;
            }
        });

    <? }else{?>
        // se está cadastrando uma nova noticia
    //$("#enviar").css("display", "none");
    $('.mostrar').click(function() {
       if(!validaNoticia()){
            return false;
        }
    });
       <? }?>
      // seleciona todas as áreas
        $('#todos').live("click",function(){
            if(!$(this).is(':checked')){
                $('input:checkbox').attr('checked',false);
            }else{$('input:checkbox').attr('checked','checked');}
        });
        
      $('#enviar').click(function (){
      
      var areas = new Array();
      $('input:checkbox[name="area[]"]:checked').each(function() {areas.push($(this).val());});
      areas = areas.toString();
        
    if(areas.length > 0) {
        } else {
            if(!$('.area').is(':hidden')){ 
            alert("<?=translate("Selecione ao menos uma área de interesse.")?>");
            return false;
            }
            else{
            return true;
            }
         
        } 
            
      });    
      $('#voltar').click(function() {
        /*$(".noticia").css("display", "block");
        $(".area").css("display", "none");
        $(".mostrar").css("display", "block");
        $("#enviar").css("display", "none");
        $("#voltar").css("display", "none");*/
        history.back(-1);
        console.log('volta');
      });
      
    
      
        $("#FDatapublicacao").change(function(){
        data = $("#FDatapublicacao").val();
        ano = data.substring(6);
        
        $("#FDatafinal").val("31/12/"+ano);
                
      });
      
      $("#FDatafinal").blur(function(){
        data = $("#FDatapublicacao").val();
        ano = data.substring(6);
        
        $("#FDatafinal").val("31/12/"+ano);
                
      });
      
      $("#FDatapublicacao, #FDatafinal").datepicker({
        dateFormat: 'dd/mm/yy',
        dayNames: [
        '<?=translate('Domingo')?>','<?=translate('Segunda')?>','<?=translate('Terça')?>','<?=translate('Quarta')?>','<?=translate('Quinta')?>','<?=translate('Sexta')?>','<?=translate('Sábado')?>','<?=translate('Domingo')?>'
        ],
        
        dayNamesMin: ['<?=translate('D')?>','<?=translate('S')?>','<?=translate('T')?>','<?=translate('Q')?>','<?=translate('Q')?>','<?=translate('S')?>','<?=translate('S')?>','<?=translate('D')?>']

        ,
        dayNamesShort: [
        '<?=translate('Dom')?>','<?=translate('Seg')?>','<?=translate('Ter')?>','<?=translate('Qua')?>','<?=translate('Qui')?>','<?=translate('Sex')?>','<?=translate('Sáb')?>','<?=translate('Dom')?>'
        ],
        monthNames: [
        '<?=translate('Janeiro')?>','<?=translate('Fevereiro')?>','<?=translate('Março')?>','<?=translate('Abril')?>','<?=translate('Maio')?>','<?=translate('Junho')?>','<?=translate('Julho')?>','<?=translate('Agosto')?>','<?=translate('Setembro')?>',
        '<?=translate('Outubro')?>','<?=translate('Novembro')?>','<?=translate('Dezembro')?>'
        ],
        monthNamesShort: [
        '<?=translate('Jan')?>','<?=translate('Fev')?>','<?=translate('Mar')?>','<?=translate('Abr')?>','<?=translate('Mai')?>','<?=translate('Jun')?>','<?=translate('Jul')?>','<?=translate('Ago')?>','<?=translate('Set')?>',
        '<?=translate('Out')?>','<?=translate('Nov')?>','<?=translate('Dez')?>'
        ],
        nextText: 'Próximo',
        prevText: 'Anterior',
        changeMonth: true,
    changeYear: true,
        minDate: 0, maxDate: "+10Y"
      });
      
      
      
        
  });

function validaUpload(){
var arrExtensions=new Array("bmp", "gif", "jpg", "jpeg", "png");
var objInput = frm.thumbnail;
var strFilePath = objInput.value;
var arrTmp = strFilePath.split(".");
var strExtension = arrTmp[arrTmp.length-1].toLowerCase();
var blnExists = false;

if(strFilePath==""){
 blnExists=true;
}
else{
   
for (var i=0; i<arrExtensions.length; i++) {
if (strExtension == arrExtensions[i]) {
blnExists = true;
break;
}
}
}
if (!blnExists)
alert('<?=translate("O arquivo para Thumbnail não é uma imagem")?>');
return blnExists;
} 






function validaNoticia(){
           
             data1 = $('#FDatapublicacao').val();
        data2 = $('#FDatafinal').val();


            if(data2 == '') {
                alert('<?= translate("Informe uma data limite.") ?>')
                frm.FDatafinal.focus();
                return false;
            } else {
                if(parseInt( data2.split( "/" )[2].toString() + data2.split( "/" )[1].toString() + data2.split( "/" )[0].toString() ) < parseInt( data1.split( "/" )[2].toString() + data1.split( "/" )[1].toString() + data1.split( "/" )[0].toString())) {
                    alert('<?= translate("A data limite deve ser maior que a data de publicação") ?>');
                    return false;
                } else {

                        if(frm.FTitulo.value=='')
                        {
                            alert('<?= translate("Por favor preencha o campo título") ?>');
                            frm.FTitulo.focus();
                            return false;
                        }
                        else{

                            if(frm.FDatafinal.value==''){
                                alert('<?= translate("Por favor preencha o campo data") ?>');
                                frm.Fdatafinal.focus();
                                return false;
                            }
                            else
                            {
                                if(frm.FResumomateria.value==''){
                                    alert('<?= translate("Por favor preencha o campo Resumo da Matéria") ?>');
                                    frm.FResumomateria.focus();
                                    return false;
                                }
                                else
                                {
                                    if(CKEDITOR.instances.FMateriacompleta.getData()==''){

                                        alert('<?= translate("Por favor preencha o campo Materia Completa") ?>');
                                        CKEDITOR.instances.FMateriacompleta.focus();
                                        return false;
                                    }
                                    else{
                                        // mostra as areas de interesse se está cadastrando uma nova noticia
                                        <?if(!checkapi()&&!$view->getID()){?>
                                        $(".noticia").css("display", "none");
                                        $(".area").css("display", "block");
                                        $(".mostrar").css("display", "none");
                                        $("#enviar").css("display", "block");
                                        $("#enviar").css("margin-top", "-25px");
                                        $("#enviar").css("margin-right", "0px");
                                        $("#voltar").css("display", "block");
                                        $("#voltar").css("margin-top", "-25px");
                                        $("#voltar").css("margin-right","100px");
                                        <?}else{?>return true;<?}?>
                                    }
                                }

                            }

                        }


                }


            }
     
}

document.getElementsByName('upload').onclick = function() {
    alert('Hey!');
}

</script>
<script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script>



<?=add_html_tail() ?>

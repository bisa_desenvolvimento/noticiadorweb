
<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>

<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

<script src="https://platform.linkedin.com/in.js" type="text/javascript"></script>

<style type="text/css" media="screen">

#nweb_share-buttons {
	width: 101%;
}

#nweb_share-buttons ul.nweb_shareit {
	text-align: left;
	list-style: none;
	height: 24px !important;
	margin: 0px;
	padding: 0px;
	border: 1px solid #DCDCE4;
	border-image: initial;
}

#nweb_share-buttons ul.nweb_shareit li.tweet {
	width: 109px;
	height: 19px;
	padding: 2px 0 3px 2px;
}

#nweb_share-buttons ul.nweb_shareit li.face {
	border-right: none;
	width: 100px;
	height: 19px;
	padding: 2px 0 5px 2px !important;
}

#nweb_share-buttons li.linkedinButton {
	width: 105px;
	padding: 1px 0 4px 0;
}

#nweb_share-buttons ul.nweb_shareit li {
	float: left;
	margin: 0;
}

#nweb_share-buttons ul.nweb_shareit li.plusone {
	width: 70px;
	padding: 2px 0 2px 0;
}

#nweb_share-links {
	display: table;
	text-align: right;
	width: 99%;
	height: 24px !important;
	margin-top: 10px !important;
	margin-bottom: 10px !important;
	padding: 5px;
	border: 1px solid #DCDCE4;
	border-image: initial;
}

#nweb_share-links span {
	margin-left: 5px !important;
}

#nweb_share-links span img {
	margin: 0px !important;
	padding: 0px !important;
}

#nweb_share-links #nweb_print, #nweb_share-links #nweb_mail {cursor: pointer;}

#nweb_share-links #nweb_print:hover, #nweb_share-links #nweb_mail:hover {text-decoration: underline;}

#nweb_share-links #nweb_container-indicar {
	display: none;
	margin: 0px;
	padding: 5px;
	padding-top: 0px;
	padding-right:  0px;
}

#nweb_share-links #nweb_indicar {
	display: none;
	float: right;
	text-align: right;
	padding: 5px;
	margin: 0px;
	margin-top: 5px;
	background-color: #FFF;
	z-index: 2000;
}

#nweb_share-links #nweb_indicar input {
	float: right;
	padding: 1px;
	margin-left: 5px;
	border: 1px solid black;
	width: 240px;
}

#nweb_share-links #nweb_indicar input[type=submit] {
	width: 244px;
}

</style>

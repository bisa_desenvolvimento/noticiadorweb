<?php

    $_SESSION["SESSION_LANG"]="pt-BR";
    $news = $this->getController()->obterNoticia($_GET['noticia_id']);
    $janela=false;
    if(isset ($_GET['janela_id'])){
      $janela=true;
      $areas = $this->getController()->obterAreasJanela($_GET['janela_id']);
    }else{
      $areas = $this->getController()->obterAreasNoticia($_GET['noticia_id']);
    }
    $comentarios = $this->getController()->obterComentarioAprovado($_GET['noticia_id']);
//    $numCom = $comentarios->rowCount();
    if(($news->not_link_abrir!="noticiador") && ($news->not_link_abrir != NULL) ){
      header("Location: ".$news->not_link_abrir."?noticia_id=".$news->noticia_id);
      exit;
    }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en" lang="en">
 
  <head>

    <meta name="author" content="André Alves, Juan Gómez" />
    <!-- Coloca esta petición de presentación donde creas oportuno. -->
<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
<script src="https://platform.linkedin.com/in.js" type="text/javascript"></script>
    <style type="text/css" media="screen">
    <!--
      * {margin: 0; padding: 0;}
      body {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;}
      h1 {margin-bottom: 5px; color: #04264C; *font-size: 22px;}
      h2 {margin-bottom: 5px; color: #04264C; font-size: 15px;}
      strong {color: #04264C;}
      input, textarea {margin-bottom: 15px;}
      input[type="image"] {margin-bottom: 0;}
      
      #header {min-height: 90px;}
      #container {width: 800px; margin: 0 auto;}
      #content {width: 520px; margin: 0 30px; float: left;}
      #sidebar {width: 220px; float: right;}
      #dados {border-bottom: 5px solid #04264C; margin-bottom: 15px; padding-bottom: 7px;}
      #print, #mail {cursor: pointer;}
      #print:hover, #mail:hover {text-decoration: underline;}
      #indicar {margin-top: 15px; margin-bottom: -15px;}
      #indicar {display: none;}
      #btn_inserir {text-align: right; display: block; margin-bottom: 10px;}
      
      #btn_inserir img {border: 0;}
      #sidebar ul {margin-bottom: 15px; list-style-position: inside; list-style-image: url("images/layout/marcador.png");} 
      #sidebar ul li {margin-bottom: 5px;}
      #sidebar ul li a {color: #000000; text-decoration: none;}
      #sidebar ul li a:hover {text-decoration: underline;}
      .area {display: block; background-color: #04264C; padding: 5px; color: #FFFFFF; font-weight: bold; margin-bottom: 5px;}
      .a2a_kit {float: right;}
      .coment {margin: 0 20px 10px; background-color: #eeeeee; padding: 15px;}
    -->
  .coment pre {
    white-space: pre-wrap;       /* css-3 */
    white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
    white-space: pre-wrap;      /* Opera 4-6 */
    white-space: -o-pre-wrap;    /* Opera 7 */
    word-wrap: break-word; 
  }
  ul.shareit {
  list-style: none;
  float: left;
  width: 418px;
  height: 24px !important;
  margin: 10px 10px 10px 0;
  padding: 0;
  border: 1px solid #DCDCE4;
  border-image: initial;
  }
  ul.shareit li.tweet {
  width: 109px;
  height: 19px;
  padding: 2px 0 3px 2px;
  }
  ul.shareit li.face {
  border-right: none;
  width: 100px;
  height: 19px;
  padding: 2px 0 5px 2px !important;
  }
  li.linkedinButton {
  width: 105px;
  padding: 1px 0 4px 0;
  }

  ul.shareit li {
  float: left;
  margin: 0;
  }
  ul.shareit li.plusone {
  width: 70px;
  padding: 2px 0 2px 0;
  }
/*Mario Cardoso, 26/08, matis 1813
/*p img{
  float:left;
  width:100% !important;
  height:auto !important;
}*/
    </style>
    
    <style type="text/css" media="print">
    
      * {margin: 0; padding: 0;}
      body {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;}
      h1 {margin-bottom: 5px; *font-size: 22px;}
      h2 {margin-bottom: 5px; font-size: 15px;}
    #news {border-bottom: 5px solid #000000; margin-bottom: 15px; padding-bottom: 7px;}
      #header, #comentarios, #sidebar, #print, #mail, #btn_inserir, #indicar, #share-buttons {display: none;}
    
    </style>
        <script>
            function is_email(email){
                er = /^[a-zA-Z0-9][a-zA-Z0-9._-]+@([a-zA-Z0-9._-]+.)[a-zA-Z-0-9]{2}/;
                if(er.exec(email)){
                  return true;
                } else {
                  alert('E-mail inválido.');
                  return false;
                }
            }
        </script>
   <script src="https://www.google.com/recaptcha/api.js" async defer></script>     


    <title>NoticiadorWeb - <?if($news){echo translate($news->not_titulo);}else{echo translate("Notícia não encontrada.");}?></title>
  </head>

 
  <body>
  
    <div id="container">
      <div id="header">
        <img src="images/layout/topo_news.jpg" />
      </div>
      <div id="content">
        <a href="<?=URL?>" id="btn_inserir"><img src="images/layout/inserir_noticia.jpg" alt="<?=translate("Inserir Notícia")?>" /></a>
        
        <? if(!$news) {  ?>
        
        <div id="news">
          <br /><h2><?=translate("Notícia não encontrada.")?></h2>
        </div>
                  
        <? } else { 
        $this->getController()->contarVisualizacao($_GET['noticia_id']);
            ?>

        <div id="news">
          <h1><?=translate($news->not_titulo)?></h1>
          <div id="share-buttons" style="margin-top: -4px;">
                  <ul class="shareit">
                      <li class="tweet">
                          <!-- Botao Twitter -->
                          <a href="https://twitter.com/share" class="twitter-share-button" data-text="<?=$news->not_titulo;?>" data-lang="pt" data-hashtags="NoticiadorWeb">Tweetar</a>                    
                      </li>
                      <li class="face">
                          <!-- Botao Curtir Facebook -->
                          <iframe src="https://www.facebook.com/plugins/like.php?href=<?= URL ?>index.php%3Faction%3Dshow%26secao%3Dexibir_noticia%26noticia_id%3D<?= $_GET['noticia_id'] ?>&amp;send=false&amp;layout=button_count&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:21px;" allowTransparency="true" ></iframe>
                      </li>
                      <li class="linkedinButton">
                          <!-- Botao linkscape -->
                          <script type="IN/Share" data-counter="right"></script>
                      </li>
                      <li class="plusone">
                          <!-- Botao Google +1. -->
                          <plusone size="medium"></plusone>
                      </li>
                  </ul>
              </div>
          <div style="clear: both;"></div>
          <p><?=translate($news->not_texto)?></p>
          <br /><br />
        </div>
        
        <div id="dados">
<?php

  $urlNot = (!strlen(stristr($news->not_link, "https://")) > 0) ? "https://".$news->not_link : $news->not_link;
  $strLinkMaterialOriginal = "<a href='".str_replace('https://', '', $urlNot) ."' target='_blank'>".str_replace('https://', '', $urlNot)."</a>";

?>
          <p><strong><?=translate("Link da Matéria Original:")?></strong> <?=$strLinkMaterialOriginal?></p>
          <p><strong><?=translate("Autor:")?></strong> <?=$news->usu_nome?></p>
          <p><strong><?=translate("Data de Publicação:")?></strong> <?=DateExibe($news->not_data);?></p>
          <p><?=translate("Esta notícia já foi visualizada ")?><strong><?=$news->not_acessos?></strong><?=translate(" vezes.")?></p>
          <br/>
          <span id="print" onclick="window.print();return false"><img src="images/icons/print.png" width="15" /> <?=translate("Imprimir notícia")?></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  
          <span id="mail"><img src="images/icons/mail.png" width="15" /> <?=translate("Enviar notícia por email")?></span>      

           <div id="indicar">
             <form name="formulario" method="post" action="index.php?secao=exibir_noticia">
               <input type="hidden" name="action" value="indicar"/>
               <input type="text" name="Inome" id="Inome" size="30" /><?=translate("Seu nome")?> <br />
               <input type="text" name="Iemail" id="Iemail" size="30" /> <?=translate("Seu e-mail")?><br />
               <input type="text" name="Ianome" id="Ianome" size="30" /> <?=translate("Nome do seu amigo")?><br />
               <input type="text" name="Iaemail" id="Iaemail" size="30" /> <?=translate("E-mail do seu amigo")?><br />
               <input type="hidden" name="Inoticia" id="Inoticia" value="<?=$_GET['noticia_id']?>" />
               <input type="hidden" name="Ilink" id="Ilink" value="<? $value= URL."index.php?action=show&secao=exibir_noticia&noticia_id=".$_GET['noticia_id'];if($janela){$value.="&janela_id=".$_GET['janela_id'];} echo $value;?>" />
               <input type="submit" value="Indicar Notícia" />                            
             </form>
           </div>
          
        </div>
        
        <div id="comentarios">
          <h2><?=translate("Comentários")?></h2><br />

          <? if (!empty($comentarios)) {
          for($i=0;$i<$comentarios->rowCount();$i++) {
           $comentario = $comentarios->getRow($i); ?>

          <div class="coment">
            <p><strong><?=$comentario->com_nome?></strong> <?=translate("disse:")?></p>
            <p><pre><?=translate($comentario->com_comentario)?></pre></p>
          </div>
          <? }} ?>
          
          <br /><p><strong><?=translate("Deixe um comentário")?></strong></p><br />
          
          <form name="formulario" method="post" action="index.php?secao=comentario&action=save">
            <input type="hidden" name="Fnoticia" id="Fnoticia" value="<?=$_GET["noticia_id"]?>"/>

            <table>
                <tr>
                    <td valign="top"><label style="margin-right:  15px;"><?=translate("Nome")?></label></td>
                    <td><input type="text" name="Fnome" id="Fnome" size="22" value="<?=@$_SESSION['comentario']['Fnome']?>" /></td>
                </tr>
                <tr>
                    <td valign="top"><label style="margin-right:  15px;"><?=translate("E-mail")?></label></td>
                    <td><input type="text" name="Femail" id="Femail" size="22" value="<?=@$_SESSION['comentario']['Femail']?>" /></td>
                </tr>

                <tr>
                    <td valign="top"><label style="margin-right:  15px;"><?=translate("Telefone")?></label></td>
                    <td><input type="text" name="Ffone" id="Ffone" size="22" value="<?=@$_SESSION['comentario']['Ffone']?>" /></td>
                </tr>

            </table>
            
            <? $checked = isset($_SESSION['comentario']['Fnotificacao']) ? 'checked="checked"' : '' ?>
            <input type="checkbox" name="Fnotificacao" value="1" id="Fnotificacao" <?=$checked?> /> <?=translate("Desejo receber notificações se alguém mais comenta a noticia.")?><br />
            <textarea name="Fcomentario" id="Fcomentario" cols="62" rows="10"><?=@$_SESSION['comentario']['Fcomentario']?></textarea><br />
            <? for($i=0;$i<$areas->rowCount();$i++) {
           $area = $areas->getRow($i); ?>
           <input name="areas[]" type="hidden" value="<?=$area->area_id?>" />
           <? } ?>
            <input name="not_titulo" type="hidden" value="<?=$news->not_titulo?>" />
            
            <?
                // require_once(DIR_PLUGINS.'/recaptchalib.php');
                // $publickey = "6LcE_dISAAAAAMBqArHSkKWPOFZJ3g-KljkRp52l";
                // echo recaptcha_get_html($publickey, @$_SESSION['comentario']['erro']);
            ?>
            <div class="g-recaptcha" data-sitekey="6LdS7GgUAAAAAKPo7sTFfuLGUJs6pWECFD0Kzrzs"></div>
            
            <br />
            <input type="submit" id="enviar" value="<?=translate("Enviar comentário")?>" onclick="return validacao()"/>
          </form>
          
        </div>
        
        <? } 
            unset($_SESSION['comentario']);
        ?>    
      
      </div>
      <div id="sidebar">
      
        <span class="area"><?=translate("Buscar")?></span>
        <p><?=translate("Exibir notícias com o texto abaixo:")?></p>
        <form method="post" action="<?=URL?>index.php?action=show&secao=buscar_noticia">
          <input type="text" id="txtPalvraBusca" name="txtPalvraBusca" size="27" /><br>
          &nbsp;
          <input type="checkbox" id="txtExato" name="txtExato" value="txtExato"><?=translate("Conteúdo Exato")?>
          &nbsp;&nbsp;
          <input type="image" src="images/icons/search.jpg" value="Enviar" alt="Enviar" />
        </form>
        
        <span class="area"><?=translate("Cadastrar email")?></span>
        <p><?=translate("Insira aqui o seu email para receber newsletter destas áreas:")?></p>
        <form name="frm_news" id="frm_news" method="post" action="<?=URL?>index.php?secao=assinar_newsletter&action=save">
           <input type="text" name="nome" value="Seu nome" size="27" onfocus="this.value=''" />
           <input type="text" name="email" value="Seu email" size="27" onfocus="this.value=''" />
           <input name="assinatura" type="hidden" value="0" />
           <? for($i=0;$i<$areas->rowCount();$i++) {
           $area = $areas->getRow($i); ?>
           <input name="areas[]" type="hidden" value="<?=$area->area_id?>" />
           <? } ?>
           <input type="submit" value='<?=translate("Cadastrar")?>' onclick="is_email(email.value);"/>
        </form>
        
        <?  if($janela){
            for($i=0;$i<$areas->rowCount();$i++) {
           $area = $areas->getRow($i); ?>
          
        <span class="area"><?=$area->are_descricao?></span>
        <ul>
        
          <?
              $noticias = $this->getController()->obterNoticiasArea($area->area_id);
             for($j=0;$j<$noticias->rowCount();$j++) {
                $noticia = $noticias->getRow($j); ?>
                <li>
                  <strong><?=DateExibe($noticia->not_data)?></strong>
                  <a href="<?=URL."index.php?action=show&secao=exibir_noticia&noticia_id=".$noticia->noticia_id."&janela_id=".$_GET['janela_id']?>"><?=$noticia->not_titulo?></a>
                </li>
          <? } ?>
        
        </ul>
        <? }} ?>
      
      </div>
    </div>
  
  </body>
</html>
<script type="text/javascript" src="scripts/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="scripts/jquery-ui-1.8.14.custom.min.js"></script>
<link rel="stylesheet" href="scripts/jquery-ui-1.8.14.custom.css" type="text/css"> 
    <script type="text/javascript" src="https://platform.twitter.com/widgets.js">
</script>
    <script type="text/javascript">
    $("#mail").click(function(){
            $("#indicar").slideToggle();
         });
    
    
      function validacao(){
          if($("#Fnome").val() == "") {
               alert('<?=translate("Favor informar seu nome.")?>');
               $(this).focus();
               return false;
            }
           
            if($("#Ffone").val() == "") {
               $(this).focus();
               alert('<?=translate("Favor informar seu telefone.")?>');
               return false;
            }
            if($("#Fcomentario").val() == "") {
               $(this).focus();
               alert('<?=translate("O comentário não pode ser enviado em branco.")?>');
               return false;
            }
             if(!is_email($("#Femail").val())) {
            return false;
            }
          return true;
      }

         $("#print").click(function(){
            window.print();
         });
</script>

<?
  if (isset ($_GET['twitter'])) {
?>

<div id="compatilhar-modal" title="Comentario salvo com sucesso!" >
  <p>Compartilhar no Twitter:</p>
  <div style="float: right; margin-bottom: 25px;">Deseja compartilhar o comentario no Twitter?<br/><br/>
    <div align="center" id="twitter"><?='<a href="https://twitter.com/share" class="twitter-share-button" data-url="'.URL.'index.php?action=show&secao=exibir_noticia&noticia_id='.$_GET['noticia_id'].'" data-text="'.$_GET['twitter'].'" data-count="horizontal" >Tweet</a>'?></div>
  </div>
</div>

<script type="text/javascript">
  $( "#compatilhar-modal" ).dialog({
    autoOpen: false,
    modal: true
  });
  $("#compatilhar-modal").dialog("open");
</script>
<?
  }
?>

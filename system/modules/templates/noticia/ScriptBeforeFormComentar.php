
<style type="text/css" media="screen">

	#nweb_FormComentar {margin-top: 15px; margin-bottom: 15px; padding: 10px; border-top: 5px solid #04264C; border-bottom: 5px solid #04264C;}

	#nweb_FormComentar p {color: black !important; padding: 0px; margin: 0px;}

	#nweb_FormComentar #nweb_FormComentarEnviar {
		float: right;
	}

	#recaptcha_widget_div {
		display: flex !important;
	}

	#recaptcha_widget_div #recaptcha_area {
		display: inline-block !important;
	}

</style>

<?=add_html_head() ?>	
<div class="rounded_content_top">
	&nbsp;
</div>
<div class="rounded_content_middle">
	<div class="rounded_content">
    
        <h1 class="default_title"><?=translate("Gerenciar Clientes")?></h1>
        
        <form method="get" action="<?=$view->add_form_process_url(); ?>" name="formulario">
			
            <input type="hidden" name="action" value="load" />
            <? if($_GET['secao'] == 'gestao_cliente') { ?>
            <input type="hidden" name="secao" value="gestao_cliente" />
            <? } else { ?>
            <input type="hidden" name="secao" value="gestao_layout_cliente" />
            <? } ?>
        
            <?=$view->listarClientes();?>
            
            <input type="submit" class="botaoSelecionar" value="" />
            <br /><br /><br />
                        
        </form>

    </div>
</div>		

<?=add_html_tail() ?>
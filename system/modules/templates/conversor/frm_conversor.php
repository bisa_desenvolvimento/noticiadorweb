<?= add_html_head() ?>
<div class="rounded_content_top">
    &nbsp;
</div>
<div class="rounded_content_middle">
    <div class="rounded_content">
        <h1 class="default_title"><?= translate("Conversor") ?></h1>

        <?= translate("Selecione abaixo as �reas que deseja converter do NoticiadorWeb 1.0 para o 2.0") ?>
        <br /><br />
        <form name='fomularioConversao' action='conversao/conversao.class.php' target='_self' method='post' onsubmit='return validaconvercao()'>

            <? echo $view->montarTabelaAreas(); ?>
            <div id="pager" class="pager" >
                <img src="images/first.png" class="first"/>
                <img src="images/prev.png" class="prev"/>
                <input type="text" class="pagedisplay"/>
                <img src="images/next.png" class="next"/>
                <img src="images/last.png" class="last"/>
                <select class="pagesize">
                    <option selected="selected"  value="25">25</option>
                    <option value="50">50</option>
                    <option value="75">75</option>
                    <option  value="100">100</option>
                </select>
            </div>  
            <input type="submit" value="Converter" />
        </form>

    </div>
</div>
<script>
    function validaconvercao(){
        var areas = new Array();
        $('input:checkbox[id="conversao"]:checked').each(function() {areas.push($(this).val());});
        areas = areas.toString();

        if(areas.length > 0) {
            confirm("<?= translate("Confirma a convers�o das �reas selecionadas?") ?>");
        } else {
            alert("<?= translate("Selecione ao menos uma �rea de interesse.") ?>");
            return false;
        }
    }
   
</script>
<?= add_html_tail() ?>
<?= add_html_head() ?>
<div class="rounded_content_top"></div>
<div class="rounded_content_middle">
    <div class="rounded_content">
        <h1 class="default_title"><?= translate('Ajuste de E-mails de Assinantes') ?></h1>
        <form name="formulario" method="post" action="" onSubmit="">
        <fieldset>
            <div id="mensagemajax"></div>
        <div id="carregando">

             </div>
            <div id="ajustarajax">
                
               <!-- <label><?= translate('Se voc� deseja fazer uma limpeza em seus assinantees favor de clickar o bot�o "Ajustar Emails"') ?></label> --> 
               <!-- Bruno Magnata 22/08/16 Manti[4600 ] -->
               <label><?= translate('Esta rotina ir� exibir para voc� a rela��o de e-mails que constam no cadastro que est�o com algum problema em sua sintaxe.</br> Ap�s a exibi��o, voc� pode decidir EXCLUIR os e-mails.</br>
                                     Importante: Esta rotina serve apenas para EXCLUIR os e-mails inv�lidos, se voc� deseja alterar os e-mails, use a rotina "Menu / Assinantes de e-Mails / Gerenciar e-Mails"') ?></label>
                <?
            $usuario = controller_seguranca::getInstance()->identificarUsuario();
                ?>
            </div>
            <br/>
            <input type="button" value="Ajustar Emails" class="botaoProcurar" />
            </fieldset>
            <table>
            </table>
        </form>
    </div>
</div>
<div id="ajustar-modal" title="Ajustar Assinantes" >
    <div align="center" id="ajustar"></div>
</div>
<?= add_html_tail() ?>
<script type="text/javascript">
var assinante_id='';
var emails_id='';
var conteudo='';
var email_descricao='';
var excluido='';
var areas='<?=$view->getAreasUsuario($usuario->getId())."0"?>';
var total = '<?=$view->getTotalEmails()?>';
var iconcarregar='<?= produce_icon("wait30.gif"); ?>'+'<p style="margin-top:5px;">Carregando...</p>';
var carregando ='<label>Espere por favor, esta a��o pode demorar umos minutos dependendo da quantidade de seus emails!</label><br/><p style="float: left;">'+iconcarregar;
var paginacao=0;
var header = "<br><table class='formatted' id='paginacaoTable'><thead><tr><td width='50'>#</td><td width='275'>Email</td><td width='100'>Status</td><td><div style='float: right;'>&nbsp;<a href='#'><img src=\"<?=DIR_IMAGES?>layout/hd_logout_button.png\" title=\"Excluir\" id='excluirtodos' /></a>&nbsp;<input type='checkbox' id='todos'></div></td></tr></thead><tbody id='botarAssinante'></tbody></table>";

// botao pra selecionar todos os emails pra exclir
        $('#todos').live("click",function(){
            if(!$(this).is(':checked')){
                $('input:[name=assinante]').attr('checked',false);
            }else{$('input:[name=assinante]').attr('checked','checked');}
        });
//coloca como deselecionado o botao #todos se mudar de tela na paginacao
    $('.next, .last, .prev, .first'). click(function(){
    $('#todos').attr('checked',false); });


//cuando clicka o botao excluir todos pega todos os emails selecionados e coloca no dialog
    $('#excluirtodos').live("click",function(){
//pega todos os checkbox e salva na variavel assinantes
    var assinantes = $('.assinante').get();
    var emails = '';
    for (i in assinantes){
 //pega todos os checkbox com status checked e concatena na variable emails
      if($(assinantes[i]).is(':checked')){
          emails+= $('#'+assinantes[i].value+'h').val()+'<br/>';
          emails_id+=assinantes[i].value+',';
       }

    }
    emails_id+='0';
    if(emails!=''){
      conteudo='<span class="ui-icon ui-icon-circle-close" style="float: left"></span><label style="float: left;">  Voce deseja excluir o email?: </label><br/><br/><p align="left">'+emails+'</p>';
      //coloca no dialog os emails com o mensagem de exclu��o
       showDialog(conteudo,1,2);
      }else{
        conteudo='<span class="ui-icon ui-icon-notice" style="float: left"></span><label style="float: left;">  Deve selecionar os email pra excluir!</label>';
        showDialog(conteudo,2,0);
      }
    });

    //quando clicka no botao exlcuir pega o id do assinante atual pra fazer
    $('.excluir').live("click",function(){
        assinante_id=$(this).attr('id');
        // procura o input hidden com o value do email, id=1h
        emaildescricao=$('#'+assinante_id+'h').val();
        conteudo='<span class="ui-icon ui-icon-circle-close" style="float: left"></span><label style="float: left;">  Voce deseja excluir o email?: </label>'+emaildescricao;
        //coloca no dialog os emails com o mensagem de exclu��o
        showDialog(conteudo,1,1);
        return false;
    });

    //quando clicka no bot�o editar
    $('.editar').live('click',function(){
        assinante_id=$(this).attr('id');
        excluido=$(this).attr('name');
        // procura o input hidden com o value do email, id=1h
        emaildescricao=$('#'+assinante_id+'h').val();
        var bloqueo = '';
        if(excluido=='0'){
        bloqueo='<select name=menu id=bloquear><option value ="0">Ativo</option><option value ="1">Inativo</option>';
        }else{
        bloqueo='<select name=menu id=bloquear><option value ="1">Inativo</option><option value ="0">Ativo</option>';
        }
        conteudo='<span class="ui-icon ui-icon-pencil" style="float: left"></span><label style="float: left;">Atualizar o email:</label><br/><br/><label style="float: left;">Email:</label><input type=text id="emailDescricao" value="'+emaildescricao+'" size="40"<br><label style="float: left;"><?=translate('Situa��o:')?></label><p align="left">'+bloqueo+'</p>';
        showDialog(conteudo, 3,0);
        return false;
    });

    function showDialog(data,tipo,quantidade){
        /* data: conteudo do mensagem pra inserir no div
        * tipo: tipo de dialog 1: pra exclus�o, 2: pra alert 0:pra nenhum
        * quantidade: pra saber se foi clickado pra eliminar um email ou muitos
        *
        */
        $('#ajustar').html(data);
        if(tipo==1){
        //inicializa o dialogo com os parametros
                $( "#ajustar-modal" ).dialog({
			resizable: false,
			height:180,
                        autoOpen: false,
			modal: true,
			buttons: {
				"Excluir": function() {                                       
                                    if(quantidade==1){
                                    // Bruno Magnata 15/09/16 Mantis[4600]
                                    operacaoAjax("index.php?action=deleteEmail&secao=ajustar_email_assinantes&area="+areas+"&id=",assinante_id);
                                    }else{
                                    operacaoAjax("index.php?action=delete&secao=ajustar_email_assinantes&id=",emails_id);
                                    }
                                    $(this).dialog({buttons:{}});
				},
				Cancelar: function() {
				emails_id='';
                                $( this ).dialog( "close" );
				}
			}
                       	});}
         if(tipo==2){
         $( "#ajustar-modal" ).dialog({
			resizable:false,
                        height: 100,
                        modal: true,
                        buttons: {}
		});
         }
         if(tipo==3){
        $( "#ajustar-modal" ).dialog({
			resizable: false,
			height:150,
                        autoOpen: false,
			modal: true,
			buttons: {
				"Atuali�ar": function() {
                                    var datos_assinante=assinante_id+','+$('#emailDescricao').val()+', ,1,'+$('#bloquear').val();
                                    operacaoAjax("index.php?action=save&secao=ajustar_email_assinantes&id=",datos_assinante);
                                    $(this).dialog({buttons:{}});
				},
				Cancelar: function() {
				emails_id='';
                                $( this ).dialog( "close" );
				}
			}
                       	});
         }

        $( "#ajustar-modal" ).dialog( "open" );

    }



function operacaoAjax(url,id_emails){
    $.ajax(
                {
                    url: url+id_emails,
                    type: "get",
                    dataType: "html",
                    error: function(xhr, ajaxOptions, thrownError){
                    alert(xhr.status);
                    alert(thrownError);
                    $('#mensagemajax').html('<p>AJAX - erro()</p> Pagina nao encontrada!!');    },
                    beforeSend: function(){
                    $('#ajustar').html(iconcarregar);
                    },
                    success: function(strData){
                        $('#ajustar').html(strData);
                        setTimeout(function (){
                        $('#ajustar-modal').dialog( "close" );
                        removeTR(id_emails);
                        }, 3000);

                    }
                }
            );
    }

    function removeTR(id_emails){
        var emails = id_emails.split(",");
        for (i in emails){
            $('#'+emails[i]+'tr').slideUp().remove();
        }
        assinante_id='';
        emails_id='';
    }

</script>

<script>
    $('.botaoProcurar').live("click",function(){
    $('#ajustarajax').html(header);
    pegarValidacaoComAjax(0);
    $(this).hide();
    $('#todos,#excluirtodos').hide();
        });

    function pegarValidacaoComAjax(numenvio){
//                        console.log("Paginacao: "+paginacao);
//                        console.log("Total: "+total);
//                        console.log("Numero de envio: "+numenvio);
                        var fim = false;
                        if(numenvio!=0){
                        paginacao=numenvio*500;
                        if(paginacao>total){
                            fim=true;
                        }
                        }
               if(!fim){
                             $.ajax(
                {
                    url: "index.php?action=carregar&secao=ajustar_email_assinantes&total="+Math.round(paginacao)+"&areas="+areas+"",
                    type: "get",
                    dataType: "html",
                  //  contentType: "application/json; charset=utf-8",

                    error: function(xhr, ajaxOptions, thrownError){
//                        console.log( "XHR status: " +xhr.status);
//                        console.log( "Tipo de erro: " + xhr.statusText );
                    },
                    beforeSend: function(){
                    $('#mensagemajax').remove()
                    $('#carregando').html(carregando);
                    },
                    complete: function(){
                        numenvio++;
                        pegarValidacaoComAjax(numenvio);
                    },
                    success: function(strData){
                        if(strData!='1'){
                            $('#botarAssinante').slideDown("slow").append(strData);
                        }
                      //  console.log(strData);
                         $('.excluir,.editar,.assinante').hide();
                    }
                }
            );
               }else{
                   $('#carregando').remove();
                   $('.excluir,.editar,.assinante,#todos,#excluirtodos').fadeIn("slow");
               }

            }
            function preloader(porcentage){
                //var porcentagefinal=Math.round(porcentage);
                var porcentagefinal= porcentage.toFixed(2);
                //bota o porcentagem no progressbar
                $("#progressbar").progressbar({ value: porcentage });
                if(porcentagefinal<100){
                    codigo ='<center><strong><?= translate('Porcentage:') ?></strong> '+porcentagefinal+'%<br/>'+emails[posicao]+'<br/>'+posicao+' de '+ total+'</center>';
                    $('#procesando').html(codigo);
                }
                //quando nao fica multiplo de 10 para 100 se e maior bota 100%
                else{
                    codigo ='<center><strong><?= translate('Porcentage:') ?></strong> 100%<br/></center>';
                    $('#procesando').html(codigo);
                }
            }

</script>
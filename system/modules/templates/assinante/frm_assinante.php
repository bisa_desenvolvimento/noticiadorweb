<?= add_html_head() ?>

<?php

ini_set('display_errors', 0);
error_reporting(0);

$urlRedirect = "";

$mensagem_modulo_assinante = "";
if (isset($_SESSION["avisos-modulo-assinante"])) {
	if ($_SESSION["avisos-modulo-assinante"] != "") {
		$mensagem_modulo_assinante = $_SESSION["avisos-modulo-assinante"];
		$_SESSION["avisos-modulo-assinante"] = "";
	}
}

$emails_invalidos = "";
if (isset($_SESSION["emails_invalidos"])) {
	if ($_SESSION["emails_invalidos"] != "") {
		$emails_invalidos = $_SESSION["emails_invalidos"];
		unset($_SESSION["emails_invalidos"]);
		$invalidos = true;
	}
}

$email_valido = "";
if (isset($_SESSION["email_valido"])) {
	if ($_SESSION["email_valido"] != "") {
		$email_valido = $_SESSION["email_valido"];
		unset($_SESSION["email_valido"]);
		$validos = true;
	}
}

$email_ja_cadastrado = "";
if (isset($_SESSION["email_ja_cadastrado"])) {
	if ($_SESSION["email_ja_cadastrado"] != "") {
		$email_ja_cadastrado = $_SESSION["email_ja_cadastrado"];
		unset($_SESSION["email_ja_cadastrado"]);
		$cadastrados = true;
	}
}

require_once "simplexml.class.php";

if (isset($_GET['code'])) {
	$authcode = $_GET['code'];
	$clientid = '388155536840-gp0n31jruvltam623bh1of3jup9nmg24.apps.googleusercontent.com';
	$clientsecret = 'J3_yWmPRc1YTX2YlZ8gq8PDg';
	$redirecturi = URL . "index.php?action=show&secao=assinante";

	$fields = array(
		'code' 			=>  urlencode($authcode),
		'client_id' 	=>  urlencode($clientid),
		'client_secret' =>  urlencode($clientsecret),
		'redirect_uri' 	=>  urlencode($redirecturi),
		'grant_type' 	=>  urlencode('authorization_code')
	);

	$fields_string = '';
	foreach ($fields as $key => $value) {
		$fields_string .= $key . '=' . $value . '&';
	}
	$fields_string = rtrim($fields_string, '&');

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'https://accounts.google.com/o/oauth2/token');
	curl_setopt($ch, CURLOPT_POST, 5);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$result = curl_exec($ch);
	curl_close($ch);

	$response    = json_decode($result);
	$accesstoken = $response->access_token;

	if ($accesstoken != '') {
		$_SESSION['token'] = $accesstoken;
	}

	$xmlRespo = file_get_contents("https://www.google.com/m8/feeds/contacts/default/full?oauth_token=" . $_SESSION['token']);
	$xml = new SimpleXMLElement($xmlRespo);
	$xml->registerXPathNamespace('gd', 'https://schemas.google.com/g/2005');
	$result = $xml->xpath('//gd:email');

	$all = "";
	foreach ($result as $title) {
		$all .= $title->attributes()->address . "\n";
	}
}
?>

<style>
	#msg-mod-ass {
		margin-left: 20px;
		font-weight: bold;
		/*color: red;*/
	}

	#msg-inv-email {
		margin-left: 20px;
		font-weight: bold;
		display: none;
	}

	#msg-val-email {
		margin-left: 20px;
		font-weight: bold;
		display: none;
	}

	#msg-jac-email {
		margin-left: 20px;
		font-weight: bold;
		display: none;
	}

	#mar-sup-msg-mod-ass {
		margin-top: 20px;
	}

	#mar-inf-msg-mod-ass {
		margin-bottom: 20px;
	}

	.gmail a {
		padding-left: 10x;
		padding-right: 10px;
	}

	textarea {
		resize: none;
	}
</style>

<script>
    $("document").ready(

        function() {

            $("#page2").css("display", "none");

            $("#mostrar").click(

                function() {

                    // na integracao nao precisa de validar as áreas

                    <?
                    if (!checkapi()) {
                    ?>

                        $('#msg-mod-ass').html("");

                        var areas = new Array();
                        $('input:checkbox[name="areas[]"]:checked').each(
                            function() {
                                areas.push($(this).val());
                            }
                        );
                        areas = areas.toString();

                        if (areas.length > 0) {

                        <?
                    }
                        ?>

                        // let frm = document.formulario;
                        // let emailValue = frm.FEmails.value;

                        let myForm = document.forms['formulario'];
                        let myTextarea = myForm.elements['FEmails'];
                        let textValue = myTextarea.value.trim().length;

                        if (textValue === 0) {
                            alert("<?= translate('Informe o(s) e-mail(s)!') ?>");
                        } else {

                            var lista = $('#FEmails').val();

                            lista = lista.replace(/^\s*[\r\n]/gm, "");

                            var emails = lista.split(/\s|;|,|<|>|\//g);

                            var validos = '';
                            var listaValidos = '';
                            var totalvalidos = 0;
                            var listaInvalidos = '';
                            var totalinvalidos = 0;

                            var re = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;



                            for (i = 0; i < emails.length; i++) {

                                if (emails[i].indexOf("*") != -1) {
                                    //alert("*: " + emails[i]);
                                    listaInvalidos += emails[i] + '<br />';
                                    totalinvalidos++;
                                } else {

                                    if (emails[i].search(re) != -1) {
                                        //alert("email: " + emails[i]);
                                        if (emails[i].indexOf(".") == -1) {
                                            //alert("email: " + emails[i]);
                                            //alert("email: " + emails[i].indexOf("."));												
                                            listaInvalidos += emails[i] + '<br />';
                                            totalinvalidos++;

                                        } else {
                                            validos += emails[i] + ',';
                                            listaValidos += emails[i] + '<br />';
                                            totalvalidos++;
                                        }
                                    } else {
                                        if (emails[i].search('@') != -1) {
                                            //alert(emails[i].search('@'));
                                            listaInvalidos += emails[i] + '<br />';
                                            totalinvalidos++;
                                        }
                                    }

                                }
                            }

                            if (totalvalidos > 500) {

                                alert("<?= translate('Forneca até500 endereços de email por vez!') ?>");
                            } else {

                                $("#EmaVali").val(validos);

                                var dados = '<table width="100%"><tr><td width="50%" valign="top" style="border-right:1px #cccccc solid; padding-right:5px"><strong><?= translate('E-mails Válidos:') ?></strong>' + totalvalidos + '<br /><br />' +
                                    listaValidos + '</td><td width="50%" valign="top" style="border-left:1px #cccccc solid; padding-left: 5px"><strong><?= translate('E-mails Inválidos:') ?></strong>' + totalinvalidos + '<br /><br />' +
                                    listaInvalidos + '</td></tr></table>';

                                $("#dados").html(dados);

                                $("#page1").css("display", "none");
                                $("#page2").css("display", "block");
                            }

                        }

                        <?
                        if (!checkapi()) {
                        ?>

                        } else {
                            alert("<?= translate("Selecione ao menos uma área de interesse.") ?>");
                        }

                    <?
                        }
                    ?>

                }
            );

            $("#voltar").click(function() {
                $("#page1").css("display", "block");
                $("#page2").css("display", "none");
            });

            $("#optin").click(function() {
                $("#dialog-form").dialog("open");
            });

            $("#invalidos").click(function() {

                <?php if ($invalidos) {   ?> showhideelement("msg-inv-email");
                <?php } ?>

            });
            $("#validos").click(function() {

                <?php if ($validos) {     ?> showhideelement("msg-val-email");
                <?php } ?>


            });
            $("#cadastrados").click(function() {

                <?php if ($cadastrados) { ?> showhideelement("msg-jac-email");
                <?php } ?>
            });

            $("#mostrar").click(function() {

                <?php if ($cadastrados) { ?> $("#msg-jac-email").css("display", "none");
                <?php } ?>
                <?php if ($validos) {     ?> $("#msg-val-email").css("display", "none");
                <?php } ?>
                <?php if ($invalidos) {   ?> $("#msg-inv-email").css("display", "none");
                <?php } ?>
            });


            function showhideelement(id) {

                var e = document.getElementById(id);

                if (e.style.display == 'block')
                    e.style.display = 'none';
                else
                    e.style.display = 'block';
            }

            $("#dialog-form").dialog({
                autoOpen: false,
                height: 750,
                width: 1000,
                modal: true
                /*
                , buttons: {
                	"Create an account": function() {
                		var bValid = true;
                		allFields.removeClass( "ui-state-error" );

                		bValid = bValid && checkLength( name, "username", 3, 16 );
                		bValid = bValid && checkLength( email, "email", 6, 80 );
                		bValid = bValid && checkLength( password, "password", 5, 16 );

                		bValid = bValid && checkRegexp( name, /^[a-z]([0-9a-z_])+$/i, "Username may consist of a-z, 0-9, underscores, begin with a letter." );
                		// From jquery.validate.js (by joern), contributed by Scott Gonzalez: http://projects.scottsplayground.com/email_address_validation/
                		bValid = bValid && checkRegexp( email, /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "eg. ui@jquery.com" );
                		bValid = bValid && checkRegexp( password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9" );

                		if ( bValid ) {
                			$( "#users tbody" ).append( "<tr>" +
                				"<td>" + name.val() + "</td>" + 
                				"<td>" + email.val() + "</td>" + 
                				"<td>" + password.val() + "</td>" +
                			"</tr>" ); 
                			$( this ).dialog( "close" );
                		}
                	},
                	Cancel: function() {
                		$( this ).dialog( "close" );
                	}
                }
                , close: function() {
                	allFields.val( "" ).removeClass( "ui-state-error" );
                }
                */
            });

        }

    );
</script>

<div class="rounded_content_top">&nbsp;</div>
<div class="rounded_content_middle">
	<div class="rounded_content">
		<h1 class="default_title"><?= translate("Cadastrar Assinantes/E-mails") ?></h1>
		<div id="msg"></div>
		<div id="msg-mod-ass"><?php echo $mensagem_modulo_assinante; ?></div>

		<table class="formatted" style="display:none" id="msg-inv-email">

			<thead>
				<tr>
					<th width="200">Emails inválidos:</th>
					<th class="cliente"></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($emails_invalidos as $value) { ?>
					<tr>
						<td><?php echo $value . "<br>"; ?></td>

					</tr>
				<?php } ?>
			</tbody>

		</table>

		<table class="formatted" style="display:none" id="msg-val-email">

			<thead>
				<tr>
					<th width="200">Emails válidos:</th>
					<th class="cliente"></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($email_valido as $value) { ?>
					<tr>
						<td><?php echo $value . "<br>"; ?></td>

					</tr>
				<?php } ?>
			</tbody>

		</table>

		<table class="formatted" style="display:none" id="msg-jac-email">

			<thead>
				<tr>
					<th width="200">Emails já cadastrados:</th>
					<th class="cliente"></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($email_ja_cadastrado as $value) { ?>
					<tr>
						<td><?php echo $value . "<br>"; ?></td>

					</tr>
				<?php } ?>
			</tbody>

		</table>


		<form name="formulario" method="post" action="<?= URL . $view->add_form_process_url(); ?>">
			<?
			add_action_data("save", null, $view->getId());
			add_hidden_api();
			?>
			<input type="hidden" name="origem_requisicao" value="<?= URL . $view->add_form_process_url(); ?>">
			<fieldset>

				<span id="page1">
					<label for="FEmails"><? if (!checkapi()) echo translate("Selecione abaixo as áreas de Interesse onde você quer inserir os emails") . "</br>" ?></label><br />
					<label for="FEmails">E-mails</label>
					<br>
					<?= translate("Você pode cadastrar até500 endereços por vez.") ?>
					<br>
					<br>
					<?= translate("Separe os endereços de email utilizando uma das opções abaixo:") ?>
					<br>
					<blockquote style="padding-left: 25px;">
						- <?= translate("vírgula") ?>; <br>
						- <?= translate("ponto e vírgula") ?>; <br>
						- <?= translate("por linha (digitando um endereço por linha)") ?>.
					</blockquote><br>
					<input type="hidden" name="EmaVali" id="EmaVali" />
					<script type="text/javascript">
						onload = function() {
							optionSelect();
						}

						function optionSelect() {
							var comboMail = document.getElementById("comboMail");
							var valor = comboMail.options[comboMail.selectedIndex].value;
							if (valor == "writeMail") {
								document.getElementById("listMail").style.display = "block";
								//document.getElementById("gmail").style.display = "none";
							}
                            // else if (valor == "importMail") {
								// window.open("https://accounts.google.com/o/oauth2/auth?client_id=388155536840-gp0n31jruvltam623bh1of3jup9nmg24.apps.googleusercontent.com&redirect_uri=https://localhost/0004623_corrigir_a_funcao_de_importacao_de_enderecos_de_uma_conta_do_gmail/index.php?action=show%26secao=assinante&scope=https://www.google.com/m8/feeds/&response_type=code", "_self");
								//document.getElementById("gmail").style.display = "block";
								// document.getElementById("listMail").style.display = "none";
							// }
						}
					</script>

					<select id="comboMail" onchange="optionSelect()">
						<option value="writeMail" selected>Lista de emails (um por linha)</option>
<!--						<option value="importMail">Importar contatos do Gmail</option>-->
					</select><br><br>

					<!--div id="gmail">
		<span class="gmail">
		<a href="https://accounts.google.com/o/oauth2/auth?client_id=388155536840-gp0n31jruvltam623bh1of3jup9nmg24.apps.googleusercontent.com&redirect_uri=https://localhost/0004623_corrigir_a_funcao_de_importacao_de_enderecos_de_uma_conta_do_gmail/index.php?action=show%26secao=assinante&scope=https://www.google.com/m8/feeds/&response_type=code"><?= translate("Importar contatos do Gmail"); ?></a>
		</span><br><br>
	</div-->

					<div id="listMail">
						<textarea name="FEmails" cols="50" rows="20" class="width100percent clearRight" style="font-size: 11px;" id="FEmails">
  <?php
	if (isset($all)) {
		echo $all;
	} else if (isset($_SESSION["manterEmailsDigitados"])) {
		echo $_SESSION["manterEmailsDigitados"];
		$_SESSION["manterEmailsDigitados"] = null;
	}
	?>
</textarea>
					</div>

					<a href="#" id="optin" rel="mod-optin">
						<?= translate("Mensagem de opt-in (Termo de Autorização)"); ?>

					</a>

					<br><br>
					<?= $view->listarAreas() ?>

					<input type="button" value="" class="botaoEnviar" id="mostrar" />
					<!-- <input type="button" value="Mostrar2" class="" id="mostrar2" /> -->
				</span>
				<span id="page2">
					<div id="dados"></div>
					<input type="submit" value="" class="botaoEnviar" id="enviar" />
					<input type="button" value="" class="botaoVoltar" id="voltar" />
				</span>
			</fieldset>
		</form>

		<div id="dialog-form" title="Mensagem de opt-in (Termo de Autorização)">
			<p style="font-size: 12px; text-align: left;"><?= translate("Essa é uma mensagem obrigatória que será enviada para os novos assinantes.<br />Caso deseje uma mensagem personalizada selecione o cliente abaixo e informe a nova mensagem no campo abaixo.<br />Você pode utilizar as tags <em>[nome]</em> e <em>[email]</em> para ter seu nome e e-mail dinamicamente inseridos na mensagem."); ?></p>

			<form name="formularioOptin" method="post" action="<?= URL . $view->add_form_process_url() . "&action=optin"; ?>">
				<?php
				$usuario = controller_seguranca::getInstance()->identificarUsuario();
				$email = $usuario->getEmail();

				//$objAssinarNewsletter = new Controller_assinar_newsletter();
				//$texto = $objAssinarNewsletter->getOptin($_SESSION["USUARIO"],'','','');
				$texto = $this->getOptin();

				?>
				<script>
					<?php if ($cadastrados) { ?> showhideelement("msg-jac-email");
					<?php } ?>
					<?php if ($validos) {     ?> showhideelement("msg-val-email");
					<?php } ?>
					<?php if ($invalidos) {   ?> showhideelement("msg-inv-email");
					<?php } ?>
				</script>
				<input type="hidden" name="cliente" value="<? print $email; ?>" />
				<textarea style="display:none;" name="manterEmails" id="manterEmails"></textarea>
				<script>
					$(document).ready(function() {
						$("#optin").live("click", function() {
							$("#manterEmails").val($("#FEmails").val());
						});
					});
				</script>
				<textarea id="txt-optin" name="txtoptin" cols="150" rows="40" style="font-size: 12px;margin-left:-175px"><?php print $texto; ?></textarea>
				<input type="submit" value="" class="botaoSalvar" id="enviar" />
			</form>
		</div>






	</div>

</div>

<?= add_html_tail() ?>
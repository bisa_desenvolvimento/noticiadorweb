<?php
session_start();
require_once ('simplexml.class.php');

$authcode = $_GET['code'];
$clientid = '388155536840-gp0n31jruvltam623bh1of3jup9nmg24.apps.googleusercontent.com';
$clientsecret = 'J3_yWmPRc1YTX2YlZ8gq8PDg';
$redirecturi = 'https://localhost/0004623_corrigir_a_funcao_de_importacao_de_enderecos_de_uma_conta_do_gmail/index.php?action=show&secao=assinante';

$url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

$fields = array(
	'code' 			=>  urlencode($authcode),
	'client_id' 	=>  urlencode($clientid),
	'client_secret' =>  urlencode($clientsecret),
	'redirect_uri' 	=>  urlencode(htmlspecialchars($redirecturi)),
	'grant_type' 	=>  urlencode('authorization_code')
);

$fields_string = '';
foreach($fields as $key=>$value) {
	$fields_string .= $key.'='.$value.'&';
}
$fields_string = rtrim($fields_string,'&');

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,'https://accounts.google.com/o/oauth2/token');
curl_setopt($ch, CURLOPT_POST,5);
curl_setopt($ch, CURLOPT_POSTFIELDS,$fields_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$result = curl_exec($ch);
curl_close($ch);

$response    = json_decode($result);
$accesstoken = $response->access_token;

if($accesstoken != '') {
	$_SESSION['token'] = $accesstoken;
}

$xmlRespo = file_get_contents('https://www.google.com/m8/feeds/contacts/default/full?oauth_token='.$_SESSION['token']);
$xml = new SimpleXMLElement($xmlRespo);
$xml->registerXPathNamespace('gd', 'https://schemas.google.com/g/2005');
$result = $xml->xpath('//gd:email');

foreach ($result as $title) {
	$email = $title->attributes()->address;	
	echo msg_alert($email);
	echo $email;
}
$_SESSION['email'] = $email;
?>
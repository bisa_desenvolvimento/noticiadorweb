<?

//funcao pra baixar o arquivo txt de exportacao de assinantes de um jeito seguro adicionada no TUtil.php
//require_once(DIR_PLUGINS.'/Download.php');
baixatxt(DIR_UPLOAD);

?>
<?= add_html_head() ?>


<script>
	$(document).ready(function () {
		$("#exportar").button();
	});
</script>

<div class="rounded_content_top"></div>

<div class="rounded_content_middle">
	<div class="rounded_content">
		<h1 class="default_title">
<?

//var_dump($_SESSION)."<br/>";
//var_dump($_GET);
//var_dump($_REQUEST);
//phpinfo();

?>
		<br/>Gerenciar Assinantes</h1>

		<form name="formulario" method="post" action="index.php?action=show&secao=gestao_assinante&pagina=0&tipo=listar" onSubmit="return  validarBusca()">
			<fieldset>
				<ul class="tabs">
					<li><a href="#tab1"><?= translate('Pesquisa por E-mail:'); ?></a></li>
					<li><a href="#tab2"><?= translate('Pesquisa por área:'); ?></a></li>
				</ul>
				<div class="tab_container">
					<div id="tab1" class="tab_content">
						<label for="FMes"><?= translate(' E-mails:'); ?></label>
						<br/>
						
						<input type="text" name="FEmail" id="FEmail" class="inputText" maxlength="100" size="50"/><input type="submit" value="Procurar" class="botaoProcurar"/>
					</div>
					<div id="tab2" class="tab_content">
						<label for="FArea"><?= translate('Listar E-mails por área:'); ?></label>
						<br/>
						<select name="FArea" id="FArea" class="rounded_content_middle">
							<? //echo "<option value='0'>Todas</option>";?>
							<? $view->montarComboAreas() ?>
						</select>
						<input type="submit" value="<?= translate('Procurar'); ?>" class="botaoProcurar"/>
					</div>
				</div>


				<br/>
			</fieldset>
		</form>
<?

function ListarEmail() {
	include("ClasseGestaoAssinante.php");
	$classeGestaoAssinante = new GestaoAssinante();
	$classeGestaoAssinante->MontarTabelaEmail($_SESSION['email']);
}

function ListarArea() {
	 include("ClasseGestaoAssinante.php");
	$classeGestaoAssinante = new GestaoAssinante();
	$classeGestaoAssinante->MontarTabelaArea($_SESSION['area']);
}

?>

		<script>
			frm = document.formulario;
			function validarBusca() {
				if ((frm.FEmail.value.length <= 0) && (!$('#tab1').is(':hidden'))) {
					alert("<?=translate('Preencha algum campo de busca!')?>");
					return false;
				}
				return true;
			}

			function EmailAlterado() {
				/* validação do email */
				var emailNovo = $("#emailnovo").val();
				if (!/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i.test(emailNovo)) {
					alert('<?= translate("Formato de email inválido!") ?>');
					return false;
				}

				/* verifica se o email já existe */
				//var sqlutilizado;
				//var idemailalterar;
				var existe;
				var c;
				$.ajax({
					url: 'index.php?secao=gestao_assinante&action=verificarEmail'
					, type: 'POST'
					, dataType: 'json'
					, data: {
						email: emailNovo
					}
					, async: false
					, success:function(e) {
						//sqlutilizado = e.sql;
						//idemailalterar = e.idalterar;
						//$("#sqlutilizado").html("");
						//$("#sqlutilizado").html(sqlutilizado + "<br><br>" + idemailalterar);
						existe = e.existe;
						$("#btnAlterar").attr("disabled", false).val("Alterar");
					}
					, beforeSend:function(){
						$("#btnAlterar").attr("disabled", true).val("Verificando...");
					}
					, cache: false
				});

				if (existe) {
					alert('O email ' + emailNovo + ' já existe no banco de dados. Se alterar o email atual irá gerar duplicidade. Recomendamos EXCLUIR este email errado.');
					return false;
				} else {
					alert('<?=translate("Email Alterado com sucesso!")?>');
					return true;
				}
			}

			function Confirma() {
				var confirma = confirm('<?=translate("Confirma Exclusão deste e-mail?")?>');
				if (confirma) {
					alert("Removido!")
					return true
				} else {
					alert("Remoção cancelada!")
					return false
				}
			}
			$(document).ready(function () {
				$(".tab_content").hide();
				$("ul.tabs li:first").addClass("active").show();
				$(".tab_content:first").show();

				$("ul.tabs li").click(function () {
					$("ul.tabs li").removeClass("active");
					$(this).addClass("active");
					$(".tab_content").hide();

					var activeTab = $(this).find("a").attr("href");
					$(activeTab).fadeIn();
					return false;
				});
			});

		</script>

		<form id="form" name="form1" method="post" action="" onsubmit="<? // if (isset($_REQUEST['excluir'])){InativarEmail($_REQUEST['excluir']);}?>">

<?

if (($_GET['action'] == "show")) {

	$indice = 0;
	$tent = 0;

	if ((sizeof($_REQUEST) > 0) && (isset($_REQUEST['FArea']))) {

		$_SESSION['pag'] = $_GET['pagina'];
		$_SESSION['area'] = $_REQUEST['FArea'];
		$_SESSION['email'] = $_REQUEST['FEmail'];

		if (($_REQUEST['FEmail'] != "")) {
			ListarEmail();
			$tent = 1;
			$indice = 1;
			// echo $view->montarTabelaCadastrados();
			$_SESSION['listar'] = "Email";
		} else {
			ListarArea();
			$indice = 1;
			$tent = 1;
			$_SESSION['listar'] = "Area";
			//echo $view->montarTabelaCadastrados1($_REQUEST['FArea']);
		}
	}

	if (($indice == 0) and ($_GET['pagina'] > 0) and ($tent == 0)) {
		if (($_SESSION['area'] != "") and ($_SESSION['listar'] == "Area")) {

			ListarArea();
			// echo $view->montarTabelaCadastrados();

		}

		if (($_SESSION['email'] != "") and ($_SESSION['listar'] == "Email")) {

			ListarEmail();
			// echo $view->montarTabelaCadastrados();

		}

	}

}

?>

		</form>

		<div id="sqlutilizado"></div>
		<form id="formalterando" name="formalterando" method="post" action="index.php?action=show&secao=gestao_assinante&pagina=0&tipo=alterando&id=<? if (isset ($_GET['id'])) { echo $_GET['id']; } ?>" onsubmit="return EmailAlterado()">

<?

//lista o email para alterar só por area
if (($_GET['action'] == "show") and ($_GET['tipo'] == "alterar" and ($_SESSION['email'] == ""))) {
	$view->AlterarEmailView();
	$_SESSION['idalterar'] = $_GET['id'];
}

//monta o combo de areas para bloqueos e modificação de email
if (($_GET['action'] == "show") and ($_GET['tipo'] == "alterar") and ($_SESSION['email'] != "")) {
	echo $view->montarComboAreasAssinante($_GET['id']);
	$_SESSION['idalterar'] = $_GET['id'];
}

//Execulta quando o tipo da transação for alterando so email e ativacao
if (($_GET['action'] == "show") and ($_GET['tipo'] == "alterando") and (!isset ($_SESSION['xemail']))) {
	$_SESSION['excluido'] = $_REQUEST['menu'];
	$_SESSION['emailalterar'] = $_REQUEST['emailnovo'];
	$view->AlterandoEmailView();
	if ($_SESSION['listar'] == "Email")
		ListarEmail();
	else
		ListarArea();
}

//Execulta quando o tipo da transaça alterando por email o bloqueo de todas as areas e email
if (($_GET['action'] == "show") and ($_GET['tipo'] == "alterando") and (isset ($_SESSION['xemail']))) {
	$view->AlterandoEmailComBloqueiosView();
}

//Exclui no banco de dados o id de email do assinante selecionado
if (($_GET['action'] == "show") and ($_GET['tipo'] == "excluir")) {
	$view->ExcluirEmailView($_GET['id']);
}

?>

		</form>
	</div>
</div>

<?= add_html_tail() ?>
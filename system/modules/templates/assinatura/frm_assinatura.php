<? $assinatura_id = ($view->getId() == '') ? '0' : $_GET['id'];
   echo add_html_head() ?>
<div class="rounded_content_top">
	&nbsp;
        
</div>

<script type="text/javascript">
   $(document).ready(function(){
    $('#Fass_corTexto, #Fass_corTextoBotao, #Fass_corBotao').ColorPicker({
    	onSubmit: function(hsb, hex, rgb, el) {
    		$(el).val(hex);
    		$(el).ColorPickerHide();
    	},
    	onBeforeShow: function () {
    		$(this).ColorPickerSetColor(this.value);
    	}
    })
    .bind('keyup', function(){
    	$(this).ColorPickerSetColor(this.value);
    });
   });     
</script>

<div class="rounded_content_middle">
	<div class="rounded_content">
	<h1 class="default_title"><?=translate("Geração de Código de Assinatura para Home-Page")?></h1>
<?if (isset($_GET["idAss"])) {
     if ($_GET["idAss"] != "") {?>
		<div id="msg"><? print $view->gerarCodigo($_GET["idAss"]);?></div>
<?   }
  } else {
      $lang = $_SESSION["SESSION_LANG"];
         ?>
		<form name="formulario" method="post" action="<?=URL.$view->add_form_process_url(); ?>" onsubmit="return validaForm(0,'<?echo $lang?>')">
			<? add_action_data("save", null, $view->getId()); ?>
			<fieldset id="page1">
        
        <table width="520px">
          <tr>
            <td colspan="2">
              <label for="Fass_labelTexto"><?=translate("Texto do Rótulo")?></label>
				      <input type="text" class="inputText" name="Fass_labelTexto" size="80" id="Fass_labelTexto" value="<? if($view->getId()!=""){echo $view->getass_labelTexto();}else{echo "Receba nossa Newsletter:";}?>" />
            </td>
            <td>
              <label for="Fass_corTexto"><?=translate("Cor do Rótulo")?></label>
				      <input type="text" class="inputText" name="Fass_corTexto" id="Fass_corTexto"  maxlength="40" value="<? if($view->getId()!=""){echo $view->getass_corTexto();}else{echo "000000";}?>" />
            </td>
          </tr>
          <tr>
            <td width="33%">
              <label for="Fass_labelBotao"><?=translate("Texto do Botão")?></label>
  				    <input type="text" class="inputText" name="Fass_labelBotao" id="Fass_labelBotao"  maxlength="40" value="<? if($view->getId()!=""){echo $view->getass_labelBotao();}else{echo "Cadastrar";}?>" />
            </td>
            <td width="33%">
              <label for="Fass_corBotao"><?=translate("Cor do Texto do Botão")?></label>
				      <input type="text" class="inputText" name="Fass_corTextoBotao" id="Fass_corTextoBotao"  maxlength="40" value="<? if($view->getId()!=""){echo $view->getass_fonteTexto();}else{echo "000000";}?>" />
            </td>
            <td width="33%">
              <label for="Fass_corBotao"><?=translate("Cor do Fundo do Botão")?></label>
				      <input type="text" class="inputText" name="Fass_corBotao" id="Fass_corBotao"  maxlength="40" value="<? if($view->getId()!=""){echo $view->getass_corBotao();}else{echo "cccccc";}?>" />
            </td>
          </tr>
          <tr>
            <td width="33%">
              <label for="Fass_larguraCaixa"><?=translate("Largura Caixa do E-mail")?></label>
				      <input type="text" class="inputText" name="Fass_larguraCaixa" id="Fass_larguraCaixa"  maxlength="40" value="<? if($view->getId()!=""){echo $view->getass_larguraCaixa();}else{echo "50";}?>" />
            </td>
          </tr>
        </table>
      
				<br />
				
				<label for="Fareas"><?=translate("Selecione a(s) área(s) que o email deve ser inserido")?></label>	
				<?=$view->listarAreas($assinatura_id) ?>	
				</td>
				<td>&nbsp;&nbsp;&nbsp;</td>
        
        <div id="msg"></div>
                          
			</fieldset>
      
      <fieldset id="page2">
        <?=translate("Informe abaixo o email da pessoa responsável por receber as notificações de novas assinaturas de newsletter por meio do script. Essa pessoa receberá também uma cópia do código gerado abaixo. Caso já esteja satisfeito com o resultado, coloque o email e click no botão SALVAR/ENVIAR ou Volte para modificar os parâmetros.")?>
        <br /><br /><div id="code"></div><br />
        <label for="Fjan_altura"><?=translate("E-mail")?></label>
				<input type="text" class="inputText" name="Fass_email" id="Fass_email" size="50" />
                                <!--label><a href="#" id="optin" rel="mod-optin"><?=translate("Mensagem de opt-in (Termo de Autorização)"); ?></a></label-->
      </fieldset>
                    <fieldset>
      <table width="100%">
        <tr><td>
          <input type="button" id="confirmar" value="" class="botaoEnviar"/>
          <input type="submit" id="enviar" value="" class="botaoEnviar"/>
          <input type="button" id="visualizar" value="" class="botaoVisualizar"/>
          <input type="button" id="voltar" value="" class="botaoVoltar"/>
				</td></tr>
      </table>
                        </fieldset>
	</form>

                <div id="mod-optin" title="">
            <p style="font-size: 12px; text-align: left;"><?=translate("Essa é uma mensagem obrigatória que será enviada para os novos assinantes. Caso deseje uma mensagem personalizada, informe-a abaixo.<br />Você pode utilizar as tags <em>[nome]</em> e <em>[email]</em> para ter seu nome e e-mail dinamicamente inseridos na mensagem."); ?></p>
                <textarea id="txt-optin" name="txtoptin" cols="110" rows="30" style="font-size: 12px;">
                <?=$view->getOptin(); ?>
                </textarea><br/>
                <button type="button" id="set-optin" style="float: right; margin-top: 10px;" onclick="atualizaMensagem()">Trocar mensagem</button>
                </div>
            <script>
                    $("#optin").click(function(){
                        $("#mod-optin").dialog("open");
                    });
                    function atualizaMensagem(){
                        var texto = $("#txt-optin").val();
                        // utilizando a rotina existente na secao assinante
                        var url = '<?= URL.'index.php?action=optin&secao=assinatura'?>';
                        $.post(url, {txtoptin: texto}, function(data) {
                                alert("<?=translate('Mensagem atualizada com sucesso!')?>");
                                $("#mod-optin").dialog("close");
                               // console.log(data);
                        });
                    }
        
            </script>
      
<script>

  $(document).ready(function(){

    $( "#mod-optin" ).dialog({
			resizable: false,
			height:550,
                        width: 730,
                        autoOpen: false,
			modal: true,
                        closeOnEscape: false
                       	});


    $('#enviar').css('display','none');
    $('#voltar').css('display','none');
    $('#confirmar').css('display','none');
    $('#page2').css('display','none');
    
    $('#visualizar').click(function(){
      
      var label = $("#Fass_labelTexto").val();
      var corTexto = $("#Fass_corTexto").val(); 
      var largura = $("#Fass_larguraCaixa").val();
      var labelBotao = $("#Fass_labelBotao").val();
      var corBotao = $("#Fass_corBotao").val();
      var corTextoBotao = $("#Fass_corTextoBotao").val();
      var areas = new Array();
        $('input:checkbox[name="area[]"]:checked').each(function() {areas.push($(this).val());});
        areas = areas.toString();
        
      if(areas.length > 0) {
         /*.post("<?=URL?>index.php?action=show&secao=assinatura_prev", { label: label, corTexto: corTexto,
           largura: largura, labelBotao: labelBotao, corBotao: corBotao, corTextoBotao: corTextoBotao, areas: areas },
           function(data){
             $('#msg').html(data);
           });*/  
         
         var mensagem = '<label style="color:#'+corTexto+';">'+label+'</label><br />'+
                        '<input name="nome" type="text" value="Seu nome" size="'+largura+'" /><br />'+
                        '<input name="email" type="text" value="Seu email" size="'+largura+'" />'+
                        '<input name="enviar" type="button" style="color:#'+corTextoBotao+'; background-color:#'+corBotao+'; border:1px #'+corBotao+' solid" value="'+labelBotao+'" />';
         
         $('#msg').html(mensagem);
         $('#confirmar').css('display','block');
      } else {
        alert("<?=translate("Selecione ao menos uma área de interesse.")?>");
      }      
    });
    
    $('#confirmar').click(function(){
      
      var url = "<?=URL?>";      
      var label = $("#Fass_labelTexto").val();
      var corTexto = $("#Fass_corTexto").val(); 
      var largura = $("#Fass_larguraCaixa").val();
      var labelBotao = $("#Fass_labelBotao").val();
      var corBotao = $("#Fass_corBotao").val();
      var corTextoBotao = $("#Fass_corTextoBotao").val();
      var areas = new Array();
        $('input:checkbox[name="area[]"]:checked').each(function() {areas.push($(this).val());});
        areas = areas.toString();

      <? if(isset($_GET['id'])) { ?>
        var id = '<?=$_GET['id']?>'; 
      <? } else { ?>
        var id = '<?=$view->obterCodigo()?>'; 
      <? } ?>

      if (id=="")
          id=1;
        
      if(areas.length > 0) {
         var mensagem = '<div id="newsletter"><br />'+
                        '<form teste="teste" name="frm_news" id="frm_news" method="post" action="'+url+'index.php?secao=assinar_newsletter&action=save">'+
                        '<input name="assinatura" type="hidden" value="'+id+'" />'+
                        '<label style="color:#'+corTexto+';">'+label+'</label><br />'+
                        '<input name="nome" type="text" value="Seu nome" size="'+largura+'" /><br />'+
                        '<input name="email" type="text" value="Seu email" size="'+largura+'" />';
                        
                        
         $('input:checkbox[name="area[]"]:checked').each(function() {
            mensagem += '<input name="areas[]" type="hidden" value="'+$(this).val()+'" />';
         });
         
         mensagem += '<input name="enviar" type="button" style="color:#'+corTextoBotao+'; background-color:#'+corBotao+'; border:1px #'+corBotao+' solid" value="'+labelBotao+'" onclick="is_email(email.value);" />'+
                     '</form>'+
                     '</div>'+
                     '<script>'+
                        'function is_email(email){ er = /^[a-zA-Z0-9][a-zA-Z0-9._-]+@([a-zA-Z0-9._-]+.)[a-zA-Z-0-9]{2}/;'+
                        'if(er.exec(email)) {'+
                          'document.frm_news.submit();'+
                        '} else {'+ 
                          'alert("<?=translate("E-mail inválido.")?>");'+ 
                          '}'+ 
                        '}'+
                     '</script'+
                     '>';  
         
         $('#code').html(htmlEntities(mensagem));
         $('#confirmar').css('display','block');
      } else {
        alert("<?=translate("Selecione ao menos uma área de interesse.")?>");
      }      
      
      $('#page1').css('display','none');
      $('#page2').css('display','block');
      $('#enviar').css('display','block');
      $('#voltar').css('display','block');
      $('#confirmar').css('display','none');
      $('#visualizar').css('display','none');
    });
    
    $('#voltar').click(function(){
      $('#page1').css('display','block');
      $('#page2').css('display','none');
      $('#enviar').css('display','none');
      $('#voltar').css('display','none');
      $('#confirmar').css('display','block');
      $('#visualizar').css('display','block');
    });
    
    $('input:radio[name=Fjan_tipo]').change(function(){
      tipo = $("input:radio[name=Fjan_tipo]:checked").val();
      if(tipo == 'F') {
        $('#tdscroll').css('display','inline');        
      } else {
        $('#tdscroll').css('display','none');
      }
    });
    
    $('input:text, input:radio, input:checkbox').keypress(function(e){
      if(e.which == 13){
        e.preventDefault();
      }
    });
  });
  
  function htmlEntities(texto){
    var i,carac,letra,novo='';
    for(i=0;i<texto.length;i++){ 
        carac = texto[i].charCodeAt(0);
        if( (carac > 47 && carac < 58) || (carac > 62 && carac < 127) ){
            novo += texto[i];
        }else{
            novo += "&#" + texto[i].charCodeAt(0) + ";";
        }
    }
    return novo;
  }

	frm = document.formulario;

	frm.Fass_labelTexto.nomeDesc='Texto do Rótulo';
	frm.Fass_corTexto.nomeDesc='Cor do Rótulo';
	frm.Fass_labelBotao.nomeDesc='Texto do Botão';
        frm.Fass_corTextoBotao='Cor do Texto do Botão';
	frm.Fass_corBotao.nomeDesc='Cor de Fundo do Botão';

	frm.Fass_labelTexto.required="true";
	frm.Fass_corTexto.required="true";
	frm.Fass_corTextoBotao.required="true";
	frm.Fass_labelBotao.required="true";
	frm.Fass_corBotao.required="true";

</script>
<?}?>
<iframe name="iframeJanela" width="0" height="0" frameborder="0"></iframe>
	</div>
</div>
<?=add_html_tail() ?>
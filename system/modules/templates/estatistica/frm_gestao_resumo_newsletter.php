<?
$pag = isset($_GET['pag']) ? $_GET['pag'] : '1';

$urlAction = "index.php?".PARAMETER_NAME_ACTION."=show&";
$urlAction .= PARAMETER_NAME_FILE."=gestao_noticia";
?>
<?= add_html_head() ?>

<!-- <meta http-equiv="refresh" content="60"> -->
<style type="text/css">
    #formEnviar{
        display: none;
    }
	#formEnviar textarea {
        width: 480px;
        height: 128px;
        font-size: 14px;
        border-radius: 5px;
        margin-top: 10px;
    }
	#dialog {
		display: none;
	}
</style>


<div class="rounded_content_top">
    &nbsp;
</div>
<div class="rounded_content_middle">
    <div class="rounded_content">
        <h1 class="default_title"><?= translate("Resumo das newsletter despachadas") ?></h1>

        <table class="formatted conteudo">

            <?=$view->montarTabelaNoticiasCadastradas(); ?>
        </table>
    </div>

</div>
<div id="dialog-modal" title="Resumo da newsletter!"></div>

<div id="dialog" title="Endereço(s) de email de destino">
	<br>
	Digite o(s) endereço(s) de email(s) de destino.
	<br>
	<br>
	Você pode separar os endereços de email utilizando uma das opções abaixo:
	<br>
	<blockquote style="padding-left: 25px;">
		- vírgula;
		<br>
		- ponto e vírgula;
		<br>
		- por linha (digitando um endereço por linha).
	</blockquote>
	<br>
	<form action="javascript:;" id="formEnviar">
	<textarea id="emails_destino"></textarea>
	</form>
	<div id="aviso_erro_email"></div>
</div>


<script type="text/javascript">

    $(document).ready(function(){

		setInterval(function () {
			$.ajax({
				url : "index.php?action=carregarnoticia&secao=gestao_resumo_newsletter",
				type: "GET",
				success: function (result) {
						$("conteudo").html(result);
					
				}
			});
		}, 60000);

    });

	$(".imgResumo").css("cursor", "pointer");

	function gerarNews(chave) {

		$(this).attr("src", "images/icons/carregando.gif");

		var url = "index.php?action=load&secao=gestao_resumo_newsletter";

		$.ajax({
			method: "POST",
			url: url,
			data: {
				chave: chave
				, id: chave
			}
			, beforeSend: function(){

				$(".imgResumo").click(
					function(){
						$(this).attr("src", "images/icons/carregando.gif");
					}
				);

			}

		})
		.done(function( msg ) {

			$( "#dialog-modal" ).dialog({
				height: 800,
				width: 700,
				modal: true
			}).html(msg);

			$(".imgResumo").attr("src","images/icons/page.png");

		});
	}

	$(".send_email").css("cursor", "pointer");

	function enviarEmail(chave){

		$(".send_email").click(
			function(){

				var icone_clicado = $(this);

				$("#formEnviar").css("display", "block");

				$("#dialog").dialog({

					resizable: false
					, width: 500
					, height: 600
					, modal: true
					, buttons: {
						"Enviar": function() {

							$("#aviso_erro_email").html("");

							var emails_destino = $('#emails_destino').val();

							if (emails_destino == "") {

								$("#aviso_erro_email").html("<br /><br /><?=translate('Informe ao menos um endereço de email!') ?>");

							} else {

								emails_destino = emails_destino.replace(/^\s*[\r\n]/gm, "");

								var emails = emails_destino.split(/\s|;|,|<|>|\//g);

								var emails_validos = '';
								var listaValidos = '';
								var totalValidos = 0;
								var listaInvalidos = '';
								var totalInvalidos = 0;

								var re = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

								var exitem_emails_invalidos = false;

								for (i = 0; i < emails.length; i++) {

									if (emails[i].search(re) != -1) {

										emails_validos += emails[i] + ',';
										listaValidos += emails[i] + '<br />';
										totalValidos++;

									} else {

										listaInvalidos += emails[i] + '<br />';
										totalInvalidos++;
										exitem_emails_invalidos = true;

									}
								}

								if (exitem_emails_invalidos) {

									var dados = '<br /><br /><?=translate('Corrija os erros listados abaixo:') ?><br /><br />' +
										'<table width="100%"><tr><td width="50%" valign="top" style="border-right:1px #cccccc solid; padding-right:5px"><strong><?=translate('E-mails Válidos: ') ?></strong>' +
										totalValidos +
										'<br /><br />' +
										listaValidos +
										'</td><td width="50%" valign="top" style="border-left:1px #cccccc solid; padding-left: 5px"><strong><?=translate('E-mails Inválidos: ') ?></strong>' +
										totalInvalidos +
										'<br /><br />' +
										listaInvalidos +
										'</td></tr></table>';

									$("#aviso_erro_email").html(dados);

								} else {

									$(this).dialog("close");

									var url = "index.php?action=load&secao=gestao_resumo_newsletter";

									$.ajax({
										method: "POST"
										, url: url
										, data: {
											chave: chave
											, id: chave
											, emails_destino: emails_validos
										}
										, beforeSend: function(){

											icone_clicado.attr("src","images/icons/carregando.gif");

											$('#emails_destino').val("");

										}
									})
									.done(
										function(msg) {

											$("#dialog-modal").dialog(
												{
													width: 700
													, height: 800
													, modal: true
												}
											)
											.html(msg);

											icone_clicado.attr("src", "images/icons/send_email.png");

										}
									);

								}

							}

						}

						, Cancel: function() {
							$(this).dialog("close");
						}
					}
				});

			}
		);
	}

</script>


<?= add_html_tail() ?>

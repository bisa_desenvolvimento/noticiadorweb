<?=add_html_head() ?>	
<div class="rounded_content_top">
	&nbsp;
</div>
<div class="rounded_content_middle">
	<div class="rounded_content">
	<h1 class="default_title"><?=translate("Cadastrar �rea")?></h1>
		<!--h3><!?=translate("Informe os dados da �rea de interesse ou clique sobre alguma listada para editar") ?></h3-->
<script type="text/javascript">
	function validarForm(obj) {
		if (!validarTexto(document.getElementById('txtDescricao'))) {
			alert("<?=translate('Digite uma descri��o');?>");
			return false;
		}
		return true;
	}
	function validarTexto(obj) {
		if (obj.value.length > 0){
                    return true;
                }else
		{
		return false;}
	}
</script>
			    <form action="<?=$view->add_form_process_url(); ?>" method="post" OnSubmit="return validarForm(this);">
                            <fieldset>
                            <ul class="tabs">
                            <li><a href="#tab1"><?=translate("Cadastrar Area")?></a></li>
                            <li><a href="#tab2"><?=translate("Gerenciar Areas")?></a></li>
                            </ul>
'                            <div class="tab_container">
                            <div id="tab1" class="tab_content">
			            <legend><?=translate("Dados da �rea")?></legend>
			            <label for="txtDescricao"><?=translate("Descri��o")?></label>
			            <input type="text" class="inputText" name="txtDescricao" id="txtDescricao" value="<?=$view->getDescricao() ?>" />
			            <label for="txtObservacao"><?=translate("Observa��o")?></label>
			            <textarea id="txtObservacao" class="inputText" name="txtObservacao" id="txtObservacao"><?=$view->getObservacao() ?></textarea><br /><br />
			            <legend><?=translate("Selecione o cliente")?></legend>
			            <div id="listaClientes">
                            <!--  Jorge Roberto Mantis 5941 -->
			                <?=$view->listarClientes($view->getCliente());?>
			            </div>
                                <input type="submit" value="<?=translate("Salvar");?>" class="botao"/>
                                <? add_action_data("save", null, $view->getID()); ?>
                              </div>
                            
                            <div id="tab2" class="tab_content">
    			    	<legend><?=translate("�reas cadastradas")?></legend>
                                <br/>
                                <? $usuario = controller_seguranca::getInstance()->identificarUsuario();
                                 if($usuario->getPerfil() == 1) { ?>
                                
                                <label for="filtroAreas">Filtro:</label>
    			    	<input type="text" name="filtroAreas" class="inputText" onKeyUp="updateList('<?=$_GET[PARAMETER_NAME_FILE] ?>', 'ajaxLista', 'listaAreasCadastradas', this.value)" />
                                <br/>
                                <br/>
    			    	<?}?>
    			    	<br/>
    			    	<div id="listaAreasCadastradas">
    			    	<?//=$view->listarAreasCadastradas() ?>
                        <?=$view->listarAreas(); ?>
    			    	</div>
                            </div>
                            </div>
                            <input type="hidden" value="<?=$usuario->getPerfil()?>" name="usuario_id" id="usuario_id">
     			    </fieldset>
                            </form>
	</div>
</div>
<?=add_html_tail() ?>

<script>
$(".tab_content").hide();
    $("ul.tabs li:first").addClass("active").show();
    $(".tab_content:first").show();

    $("ul.tabs li").click(function()
       {
        $("ul.tabs li").removeClass("active");
        $(this).addClass("active");
        $(".tab_content").hide();

        var activeTab = $(this).find("a").attr("href");
        $(activeTab).fadeIn();
        return false;
    });
  
</script>
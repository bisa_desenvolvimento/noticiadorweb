<? $janela_id = ($view->getId() == '') ? '0' : $_GET['id'];
   echo add_html_head() ?>
<div class="rounded_content_top">
	&nbsp;
</div>

<script type="text/javascript">
   $(document).ready(function(){
    $('#Fjan_cor').ColorPicker({
    	onSubmit: function(hsb, hex, rgb, el) {
    		$(el).val(hex);
    		$(el).ColorPickerHide();
    	},
    	onBeforeShow: function () {
    		$(this).ColorPickerSetColor(this.value);
    	}
    })
    .bind('keyup', function(){
    	$(this).ColorPickerSetColor(this.value);
    });
   });     
</script>

<div class="rounded_content_middle">
	<div class="rounded_content">
	<h1 class="default_title"><?=translate("Gera��o de C�digo para Home-Page")?></h1>
		<form name="formulario" method="post" action="<?=$view->add_form_process_url(); ?>" target="iframeJanela" onsubmit="return validaForm(0)" id="formulario" accept-charset="utf-8">
			<? add_action_data("save", null, $view->getId()); ?>
			<fieldset id="page1">
				<table>
				<tr><td>
        
              <table width="520px">
                <tr>
                  <td colspan="2">
                    <label for="Fjan_nome"><?=translate("Nome do Script")?></label>
      				      <input type="text" class="inputText" name="Fjan_nome" size="63" id="Fjan_nome" value="<? if($view->getId()!=""){echo $view->getjan_nome();}else{echo "";};?>" />
                  </td>
                </tr>
                <tr>
                  <td width="33%">
                    <label for="Fjan_altura"><?=translate("Altura")?></label>
      				      <input type="text" class="inputText" name="Fjan_altura" id="Fjan_altura"  maxlength="4" value="<? if($view->getId()!=""){echo $view->getjan_altura();}else{echo "300";};?>" />
                  </td>
                  <td width="33%">
                    <label for="Fjan_largura"><?=translate("Largura")?></label>
      				      <input type="text" class="inputText" name="Fjan_largura" id="Fjan_largura"  maxlength="4" value="<? if($view->getId()!=""){echo $view->getjan_largura();}else{echo "220";}?>" />
                  </td>
                  <td width="33%">
                    <label for="Fjan_cor"><?=translate("Cor de Fundo")?></label>				
      				      <input type="text" class="inputText" name="Fjan_cor" id="Fjan_cor"  maxlength="20" value="<? if($view->getId()!=""){echo $view->getjan_cor();}else{echo "FFFFFF";}?>" />
                  </td>
                </tr>
                
                <tr>
                  <td>
                    <label for="Fjan_data"><?=translate("Exibir data:")?></label>
            				<input type="radio" name="Fjan_data" id="Fjan_data" value="1" <? if (($view->getjan_data()==1)&&($view->getId()!="")){echo "checked";} else {echo "checked";}?> /><?=translate("Sim")?>
            				<input type="radio" name="Fjan_data" id="Fjan_data" value="0" <? if (($view->getjan_data()==0)&&($view->getId()!="")){echo "checked";}?> /><?=translate("N�o")?>
                  </td>
                  <td>
                    <label for="Fjan_logo"><?=translate("Exibir logo noticiador:")?></label>
            				<input type="radio" name="Fjan_logo" id="Fjan_logo" value="1" <? if (($view->getjan_data()==1)&&($view->getId()!="")){echo "checked";} else {echo "checked";}?> /><?=translate("Sim")?>
            				<input type="radio" name="Fjan_logo" id="Fjan_logo" value="0" <? if (($view->getjan_data()==0)&&($view->getId()!="")){echo "checked";}?> /><?=translate("N�o")?>              
                  </td>
                  <td>
                    <label for="Fjan_quantidade"><?=translate("Quantidade de Not�cias")?></label>				
      				      <input type="text" class="inputText" name="Fjan_quantidade" id="Fjan_quantidade"  maxlength="5" value="<? if($view->getId()!=""){echo $view->getjan_quantidade();}else{echo "5";}?>" />
                  </td>            
                </tr>
                
                <tr>
                  <td valign="top">
                    <label for="Fjan_quantidade"><?=translate("Exibir Bot�es:")?></label>
                    <input type='checkbox' name='Fjan_inserirNoticia' value="1" <? if ($view->getjan_inserirNoticia()==1){echo "checked";}?> />&nbsp;<?=translate("Inserir Not�cia")?><br />
                    <input type='checkbox' name='Fjan_visualizarComentario' value="1" <? if ($view->getjan_visualizarComentario()==1){echo "checked";}?> />&nbsp;<?=translate("Visualizar Coment�rio")?>
                  </td>
                  <td>
                    <input type='checkbox' name='Fjan_comentar' value="1" <? if ($view->getjan_comentar()==1){echo "checked";}?> />&nbsp;<?=translate("Comentar")?><br />
                    <input type='checkbox' name='Fjan_indicar' value="1" <? if ($view->getjan_indicar()==1){echo "checked";}?> />&nbsp;<?=translate("Indicar")?>
                  </td>
                  <td>
                    <label for="Fjan_tipo"><?=translate("Exibir not�cias:")?></label>				
            				<input type="radio" name="Fjan_tipo" id="Fjan_tipoG" value="G" <? if (($view->getjan_tipo()=='G')&&($view->getId()!="")){echo "checked";}else{ echo" checked";}?> /><?=translate("Girando")?>
            				<input type="radio" name="Fjan_tipo" id="Fjan_tipoF" value="F" <? if ($view->getjan_tipo()=='F'){echo "checked";}?> /><?=translate("Fixas")?>
                  </td>
                </tr>
                <tr>
                  <td>
                    <label for="Fjan_exibirApenasTitulo"><?=translate("Exibir apenas t�tulo")?></label>				
            				<input type="radio" name="Fjan_exibirApenasTitulo" id="Fjan_exibirApenasTitulo" <? if ($view->getjan_exibirApenasTitulo()==1){echo "checked";}?> value="1" /><?=translate("Sim")?>
            				<input type="radio" name="Fjan_exibirApenasTitulo" id="Fjan_exibirApenasTitulo" <? if (($view->getjan_exibirApenasTitulo()==0)&&($view->getId()!="")){echo "checked";}else{echo "checked";}?> value="0" /><?=translate("N�o")?>
                  </td>
                  <td>
                    <label for="Fjan_borda"><?=translate("Exibir borda:")?></label>
            				<input type="radio" name="Fjan_borda" id="Fjan_borda" value="1" <? if (($view->getjan_borda()==1)&&($view->getId()!="")){echo "checked";} else {echo "checked";}?> /><?=translate("Sim")?>
            				<input type="radio" name="Fjan_borda" id="Fjan_borda" value="0" <? if (($view->getjan_borda()==0)&&($view->getId()!="")){echo "checked";}?> /><?=translate("N�o")?>
                  </td>
                  <td id="tdscroll">
                    <label for="Fjan_scroll"><?=translate("Exibir barra de rolagem:")?></label>
            				<input type="radio" name="Fjan_scroll" id="Fjan_scrollS" value="1" <? if (($view->getjan_scroll()==1)&&($view->getId()!="")){echo "checked";} else {echo "checked";}?> /><?=translate("Sim")?>
            				<input type="radio" name="Fjan_scroll" id="Fjan_scrollN" value="0" <? if (($view->getjan_scroll()==0)&&($view->getId()!="")){echo "checked";}?> /><?=translate("N�o")?>
                  </td>
                  
                </tr>
                <tr></tr>
              </table>
              				
      				<br />
      				<label for="Fareas"><?=translate("Selecione as �reas de interesse")?></label>
                    <?=$view->listarAreas($janela_id) ?>			
      				</td>
      				<td>&nbsp;&nbsp;&nbsp;</td>
      				<td valign="center">
				  		    <?// if($view->getId() != "")
      							//echo $view->geraCodigo($view->getId())
      						?>
      						<br />
      				</td></tr>
      				
      				</table><br />
              
              
              <div id="msg"></div>
              
      			</fieldset>
      
            <fieldset id="page2">
              <?=translate("O c�digo gerado abaixo, deve ser enviado para o email do WebMaster respons�vel pela home page. Caso j� esteja satisfeito com o resultado, coloque o email e click no bot�o SALVAR/ENVIAR ou Volte para modificar os par�metros.")?>
              <br /><br /><div id="code"></div><br />
              <label for="Fjan_altura"><?=translate("E-mail")?></label>
      				<input type="text" class="inputText" name="Fjan_email" id="Fjan_email" size="50" />
            </fieldset>
                        
            <table width="100%">
            <tr><td>
                <input type="button" id="confirmar" value="" class="botaoEnviar"/>
                <input type="submit" id="enviar" value="" title="Salvar Script" class="botaoSalvar"/>
                <input type="button" id="visualizar" value="" class="botaoVisualizar"/>
                <input type="button" id="voltar" value="" class="botaoVoltar"/>
      				</td></tr>
            </table>
	</form>					
  
  
  
  <!--
<div>
    <iframe name="Noticiador Web" src="templates/janela/janela.php?width=400&height=500" frameborder="0" width="400" height="500" scrolling="no"></iframe>
  </div>
-->
  
<script>

  $(document).ready(function(){

    $('#enviar').css('display','none');
    $('#voltar').css('display','none');
    $('#confirmar').css('display','none');
    $('#page2').css('display','none');
    $('#tdscroll').css('display','none');
    
    $('#visualizar').click(function(){
      
      var altura = $("#Fjan_altura").val();
      var largura = $("#Fjan_largura").val(); 
      var tipo = $("input:radio[name=Fjan_tipo]:checked").val();
      var cor = $("#Fjan_cor").val();
      var scrooll = $("input:radio[name=Fjan_scroll]:checked").val();
      var quantidade = $("#Fjan_quantidade").val();
      var logo = $("input:radio[name=Fjan_logo]:checked").val();
      var data = $("input:radio[name=Fjan_data]:checked").val();
      var borda = $("input:radio[name=Fjan_borda]:checked").val();
      var titulo = $("input:radio[name=Fjan_exibirApenasTitulo]:checked").val();
      var foto = $("input:radio[name=Fjan_foto]:checked").val();
        if(foto == undefined) { foto = 0; }
      var posicao = $("input:radio[name=Fjan_foto]:checked").val();
        if(posicao == undefined) { posicao = 0; }
      var inserir = $("input:checkbox[name=Fjan_inserirNoticia]:checked").val();
        if(inserir == undefined) { inserir = 0; }
      var comentar = $("input:checkbox[name=Fjan_comentar]:checked").val();
        if(comentar == undefined) { comentar = 0; }
      var visualizar = $("input:checkbox[name=Fjan_visualizarComentario]:checked").val();
        if(visualizar == undefined) { visualizar = 0; }
      var indicar = $("input:checkbox[name=Fjan_indicar]:checked").val();
        if(indicar == undefined) { indicar = 0; }
      var areas = new Array();
        $('input:checkbox[name="areas[]"]:checked').each(function() {areas.push($(this).val());});
        areas = areas.toString();
        
        if(areas.length > 0) {
           $.post("<?=URL?>/index.php?action=show&secao=janela_cli", { preview: 1, altura: altura, largura: largura,
           cor: cor, scrooll: scrooll, quantidade: quantidade, tipo: tipo, logo: logo, data: data, titulo: titulo,
           foto: foto, posicao: posicao, inserir: inserir, comentar: comentar, visualizar: visualizar, indicar: indicar,
           areas: areas, borda: borda },
           function(data){
             $('#msg').html(data);
           });  
           $('#confirmar').css('display','block');
        } else {
          alert("<?=translate("Selecione ao menos uma �rea de interesse.")?>");
        }      
    });
    
    $('#confirmar').click(function(){
      
      var url = "<?=URL?>";
      var largura = $("#Fjan_largura").val();
      largura = parseInt(largura)+10;
      if($("input:checkbox[name=Fjan_inserirNoticia]:checked").val() == undefined) {
        var altura = $("#Fjan_altura").val();
        altura = parseInt(altura)+5;
      } else {
        var altura = $("#Fjan_altura").val();
        altura = parseInt(altura)+30;
      }
      
      <? if(isset($_GET['id'])) { ?>
        var id = '<?=$_GET['id']?>'; 
      <? } else { ?>
        var id = '<?=$view->obterCodigo()?>'; 
      <? } ?>
      
      var mensagem = '<iframe name="Noticiador Web" src="'+url+'index.php?action=show&secao=janela_cli&id='+id+'" frameborder="0" width="'+largura+'" height="'+altura+'" scrolling="no"></iframe>';
      
      $('#code').html(htmlEntities(mensagem));
      
      $('#page1').css('display','none');
      $('#page2').css('display','block');
      $('#enviar').css('display','block');
      $('#voltar').css('display','block');
      $('#confirmar').css('display','none');
      $('#visualizar').css('display','none');
    });
    
    $('#voltar').click(function(){
      $('#page1').css('display','block');
      $('#page2').css('display','none');
      $('#enviar').css('display','none');
      $('#voltar').css('display','none');
      $('#confirmar').css('display','block');
      $('#visualizar').css('display','block');
    });
    
    $('input:radio[name=Fjan_tipo]').change(function(){
      tipo = $("input:radio[name=Fjan_tipo]:checked").val();
      if(tipo == 'F') {
        $('#tdscroll').css('display','inline');        
      } else {
        $('#tdscroll').css('display','none');
      }
    });
    
    $('input:text, input:radio, input:checkbox').keypress(function(e){
      if(e.which == 13){
        e.preventDefault();
      }
    });
    
    
  });

  function htmlEntities(texto){
    var i,carac,letra,novo='';
    for(i=0;i<texto.length;i++){ 
        carac = texto[i].charCodeAt(0);
        if( (carac > 47 && carac < 58) || (carac > 62 && carac < 127) ){
            novo += texto[i];
        }else{
            novo += "&#" + texto[i].charCodeAt(0) + ";";
        }
    }
    return novo;
  }

	frm = document.formulario;
	
  frm.Fjan_nome.nomeDesc='Nome do Script';
	frm.Fjan_altura.nomeDesc='Altura';	
	frm.Fjan_largura.nomeDesc='Largura';	
	frm.Fjan_cor.nomeDesc='Cor';	
	frm.Fjan_tipo.nomeDesc='Tipo';	
	frm.Fjan_quantidade.nomeDesc='Quantidade';		
	frm.Fjan_exibirApenasTitulo.nomeDesc='Exibir Apenas T�tulo';		
	
  frm.Fjan_nome.required="true";
	frm.Fjan_altura.required="true";
	frm.Fjan_largura.required="true";
	frm.Fjan_cor.required="true";
	frm.Fjan_tipo.required="true";
	frm.Fjan_quantidade.required="true";
	frm.Fjan_exibirApenasTitulo.required="true";
  
</script>
<iframe name="iframeJanela" width="0" height="0" frameborder="0"></iframe>
	</div>
</div>		
<?=add_html_tail() ?>

<?php

if(isset($_POST['preview'])) {  
  $largura = $_REQUEST['largura']-4;
  $altura = $_REQUEST['altura']-4;
  $cor = $_REQUEST['cor'];
  $logo = $_REQUEST['logo'] ? "block" : "none"; 
  $resumo = $_REQUEST['titulo'] ? "none" : "block";
  $dataCss = $_REQUEST['data'] ? "inline" : "none";
  $borda = $_REQUEST['borda'] ? "2" : "0"; 
  $comentar = $_REQUEST['comentar'] ? "inline" : "none";
  $visualizar = $_REQUEST['visualizar'] ? "inline" : "none";
  $indicar = $_REQUEST['indicar'] ? "inline" : "none";
  $inserir = $_REQUEST['inserir'];
  $inserirdisplay = $_REQUEST['inserir'] ? "block" : "none";
  $foto = $_REQUEST['foto'] ? "inline" : "none";
  $posicao = $_REQUEST['posicao'] == 'GE' ? "left" : "right";
  $tipo = $_REQUEST['tipo'] == 'G' ? true : false;
  if($tipo) {
    $scroll = "hidden";
  } else {
    $scroll = $_REQUEST['scrooll'] ? "auto" : "hidden";
  }
  $quantidade = $_REQUEST['quantidade'];
  $areas = $_REQUEST['areas'];  
} elseif(isset($_GET['id'])) {  
  $dados = $view->getDadosJanela($_GET['id']);
  
  $largura = $dados->jan_largura;
  $altura = $dados->jan_altura;
  $cor = $dados->jan_cor;
  $logo = $dados->jan_logo ? "block" : "none"; 
  $resumo = $dados->jan_exibirApenasTitulo ? "none" : "block";
  $dataCss = $dados->jan_data ? "inline" : "none"; 
  $borda = $dados->jan_borda ? "2" : "0";
  $comentar = $dados->jan_comentar ? "inline" : "none";
  $visualizar = $dados->jan_visualizarComentario ? "inline" : "none";
  $indicar = $dados->jan_indicar ? "inline" : "none";
  $inserir = $dados->jan_inserirNoticia;
  $inserirdisplay = $dados->jan_inserirNoticia ? "block" : "none";
  $foto = $dados->jan_foto ? "inline" : "none";
  $posicao = $dados->jan_fotoPosicao == 'GE' ? "left" : "right";
  $tipo = $dados->jan_tipo == 'G' ? true : false;
  if($tipo) {
    $scroll = "hidden";
  } else {
    $scroll = $dados->jan_scroll ? "auto" : "hidden";
  }
  $quantidade = $dados->jan_quantidade;
  $areas = "SELECT area_id FROM janela_area WHERE janela_id = '".$_GET['id']."'";
} else {  
  echo "Falha na obtenção das notícias.";
  exit;  
}

    $sql = "SELECT noticia_id, not_data, not_titulo, not_chamada, not_thumbnail FROM noticia 
            NATURAL JOIN noticia_area 
            WHERE";
    // se a variavel home ? declarada pelo get, ? o iframe do home, e mostra as ultimas noticias
    if (!isset($_GET['home'])) {
        $sql.=" area_id IN (" . $areas . ") AND ";    
    } else {
        $quantidade = 50;
    }
    
    $sql.=" not_validade >= '" . date('Y-m-d') . "'
              AND not_aut = 1
              AND not_excluida = 0
            GROUP BY noticia_id 
            ORDER BY not_data DESC
            LIMIT 0," . $quantidade . ";";
    $news = $view->getNoticias($sql);
    $qtdnews = $news->RowCount();

if (strpos(URL, "teste") === false)
  header("Content-Type: text/html; charset=us-ascii");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta charset="UTF-8">

<title>NoticiadorWeb 2.0</title>

<? if($tipo) { ?>
<script type="text/javascript" src="<?=URL?>scripts/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="<?=URL?>scripts/jcarousellite_1.0.1.min.js"></script>
<script type="text/javascript">
   function janela(url){
      window.open(url, "bisanews", "scrollbars=1,height=700,width=817,resizable");
   } 

      $(document).ready(function(){

       $("#carrossel").jCarouselLite({
      auto: 2000,
      speed: 5000,
      vertical: true,
      scroll: 1,
      visible: <?=$qtdnews?>           
    })
  });

</script>
    <? }else ?><script>
 function janela(url){
      window.open(url, "bisanews", "scrollbars=1,height=700,width=817,resizable");
   }
</script>
</head>

<body style="margin: 0; /*height: 0;*/ font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; padding: 0">
<div id="janela" style="margin: 0; border: <?=$borda?>px solid #04264C; width: <?=$largura?>px; /*height: <?=$altura?>px;*/ display: block; background-color: #<?=$cor?>; padding: 10px 5px 0 0; overflow: <?=$scroll?>; <?php if ($inserir) echo 'border-bottom: 0;';?>">
<img id="logo" src="images/layout/logojanela.jpg" style="margin: -10px 10px 5px 0px !important; display: <?=$logo?>; padding: 0;"/>
<div style="clear: both; margin: 0; padding: 0;"></div>
<!-- iframe@body -->
<div id="carrossel" style="margin: 0; padding: 0;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;">
<body style="margin: 0; /*height: 0;*/ font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; padding: 0">
<ul style="<?if($tipo) {echo 'margin-left: -25px;';}else {echo 'margin:0;';}?>list-style-type: none;padding: 0;">
  <?php 
    if($qtdnews > 0) {
      for($i = 0; $i < $news->RowCount(); $i++) {
        $row = $news->getRow($i);
        $data = $row->not_data;
        $data = substr($data,8,2)."/".substr($data,5,2)."/".substr($data,0,4);
        
        echo '<li style="margin: 0 0 10px 20px;padding: 0;">';
        if($row->not_thumbnail != '')
          echo '<img src="'.$row->not_thumbnail.'" style="display: '.$resumo.'; width: 50px; margin: 0 5px 0 0; display: '.$foto.'; float: '.$posicao.'; border: 0; padding: 0;"/>';
          if(isset($_POST['preview'])) {
            echo '<span class="titulo" style="font-weight: bold; margin: 0; padding: 0;">'.translate($row->not_titulo).'</span>';
          } else {
            if (!isset($_GET['home'])) {
              $urlLink = URL.'index.php?action=show&secao=exibir_noticia&noticia_id='.$row->noticia_id.'&janela_id='.$_GET['id'];
              echo '<span class="titulo" style="font-weight: bold; margin: 0; padding: 0;"><a href="'.$urlLink.'" target="_blank" style="color: #000000; text-decoration: none;margin: 0; padding: 0;">'.translate($row->not_titulo).'</a></span>';
            } else {
              $urlLink = URL.'index.php?action=show&secao=exibir_noticia&noticia_id='.$row->noticia_id;
              echo '<span class="titulo" style="font-weight: bold;margin: 0; padding: 0;"><a href="'.$urlLink.';" target="_blank" style="color: #000000; text-decoration: none;margin: 0; padding: 0;">'.translate($row->not_titulo).'</a></span>';
            }
          }
          echo '<span class="data" style="display:'.$dataCss.'; font-style: italic;margin: 0; padding: 0;"><br />'.$data.'</span>  
                <span class="comentar" style="display:'.$comentar.';margin: 0; padding: 0;">| <a href="'.@$urlLink.'"  style="color: #000000; text-decoration: none;margin: 0; padding: 0;">'.translate("Comentar").'</a></span> 
                <span class="visualizar" style="display:'.$visualizar.'";margin: 0; padding: 0;>| <a href="'.@$urlLink.'" style="color: #000000; text-decoration: none;margin: 0; padding: 0;">'.translate("Visualizar Comentários").'</a></span>
                <span class="indicar" style="display:'.$indicar.'";margin: 0; padding: 0;>| <a href="'.@$urlLink.'" style="color: #000000; text-decoration: none;margin: 0; padding: 0;">Indicar</a></span>   
                <br /><span class="resumo" style="display:'.$resumo.';margin: 0; padding: 0;">'.translate($row->not_chamada).'</span>
              </li>';
      }  
    } else {
      echo '<h3 style="margin: 0; padding: 0;"><strong>".translate("Nenhum registro encontrado")."<strong></h3>';
    } 
  ?>
  
</ul>
</body>
</div>
<!-- /iframe@body -->
</div>
<div id="buttonadd" style="display: <?=$inserirdisplay?>; width: <?=$largura+5?>px; height: 22px; border: 2px solid #04264C; border-top: 0; padding-top: 3px; background-color: <?=$cor?>;"><a href="#"><img src="images/layout/button_inserir_noticia.gif" style="float: right; border: 0;"/></a></div>


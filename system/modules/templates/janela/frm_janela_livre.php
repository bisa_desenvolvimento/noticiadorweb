<? $janela_id = ($view->getId() == '') ? '0' : $_GET['id'];
   echo add_html_head() ?>

<div class="rounded_content_top">
	&nbsp;
</div>

<script type="text/javascript">
   $(document).ready(function(){
    $('#Fjan_cor').ColorPicker({
    	onSubmit: function(hsb, hex, rgb, el) {
    		$(el).val(hex);
    		$(el).ColorPickerHide();
    	},
    	onBeforeShow: function () {
    		$(this).ColorPickerSetColor(this.value);
    	}
    })
    .bind('keyup', function(){
    	$(this).ColorPickerSetColor(this.value);
    });
   });     
</script>

<div class="rounded_content_middle">
	<div class="rounded_content">
	<h1 class="default_title"><?=translate("Geração de Código para Home-Page Livre")?></h1>
		<form name="formulario" method="post" action="<?=$view->add_form_process_url(); ?>" id="formulario" accept-charset="utf-8">
			<? add_action_data("save", null, $view->getId()); ?>
			<fieldset id="page1">
				<table>
				<tr><td>
        
              <table width="520px">
				<tr>
					<td>
                    <label for="Fjan_nome"><?=translate("Nome do Script")?></label>
      				<input type="text" class="inputText" name="Fjan_nome" size="63" id="Fjan_nome" value="<? if($view->getId()!=""){echo $view->getjan_nome();}else{echo "";};?>" />
					</td>
					<td style="width: 10%;">
						&nbsp;
					</td>
					<td style="text-align: justify;">
						Clique nos links abaixo para ler as instruções de implementação dos nossos plugins:
					</td>
				</tr>
				<tr>
					<td>
                    <label for="Fjan_quantidade"><?=translate("Quantidade de notícias")?></label>
                    <input type="text" class="inputText" name="Fjan_quantidade" size="20" id="Fjan_quantidade" value="<? if($view->getId()!=""){echo $view->getjan_quantidade();}else{echo "";};?>" />
					</td>
					<td>
						&nbsp;
					</td>
					<td style="text-align: center;">
						<a href="#" onclick="$('#nweb_plugin_janela_livre').dialog('open'); return false;">JQuery - Janela Livre</a>
					</td>
				</tr>
				<tr>
					<td>
						&nbsp;
					</td>
					<td>
						&nbsp;
					</td>
					<td style="text-align: center;">
						<a href="#" onclick="$('#nweb_plugin_api_noticias').dialog('open'); return false;">PHP - API Notícias</a>
					</td>
				</tr>
                
              </table>
              				
      				<br />
      				<label for="Fareas"><?=translate("Selecione as áreas de interesse")?></label>
                    <?=$view->listarAreas($janela_id) ?>			
      				</td>
      				<td>&nbsp;&nbsp;&nbsp;</td>
      				<td valign="center">
				  		    <?// if($view->getId() != "")
      							//echo $view->geraCodigo($view->getId())
      						?>
      						<br />
      				</td></tr>
      				
      				</table>
              
              
              <div id="msg"></div>
              
      			</fieldset>
      
            <fieldset id="page2">
              <?=translate("Informe abaixo o email para o qual serão enviadas as instruções de como utilizar o código na página.")?>
              <br /><br />
              <label for="Fjan_altura"><?=translate("E-mail")?></label>
      				<input type="text" class="inputText" name="Fjan_email" id="Fjan_email" size="50" />
            </fieldset>
                        
            <br />                        
            <table width="100%">
            <tr><td>
                <input type="button" id="salvar" value="Salvar" class="botaoSalvar"/>
                <!--input type="submit" id="enviar" value="" class="botaoEnviar"/>
                <input type="button" id="visualizar" value="" class="botaoVisualizar"/>
                <input type="button" id="voltar" value="" class="botaoVoltar"/-->
      				</td></tr>
            </table>
	</form>					
  
  <br />
  
  
  <!--
<div>
    <iframe name="Noticiador Web" src="templates/janela/janela.php?width=400&height=500" frameborder="0" width="400" height="500" scrolling="no"></iframe>
  </div>
-->
  
<script>

    $(document).ready(function(){
        
        $('#salvar').click(function(){
            
            var nome = $("#Fjan_nome").val();
            var quantidade = $("#Fjan_quantidade").val();
/*          var email = $("#Fjan_email").val(); */
            var areas = new Array();
                $('input:checkbox[name="areas[]"]:checked').each(function() {areas.push($(this).val());});
                areas = areas.toString();
                
            if(areas.length > 0) {
               if(nome != '' && quantidade != '' /* && email != '' */) {
                    $('#formulario').submit();
               } else {
                    alert("<?=translate("Todos os campos são obrigatários.")?>");      
               }               
            } else {
              alert("<?=translate("Selecione ao menos uma área de interesse.")?>");
            }     
            
              
        });
        
        
        
    });


/*
  $(document).ready(function(){

    $('#enviar').css('display','none');
    $('#voltar').css('display','none');
    $('#confirmar').css('display','none');
    $('#page2').css('display','none');
    $('#tdscroll').css('display','none');
    
    $('#visualizar').click(function(){
      
      var altura = $("#Fjan_altura").val();
      var largura = $("#Fjan_largura").val(); 
      var tipo = $("input:radio[name=Fjan_tipo]:checked").val();
      var cor = $("#Fjan_cor").val();
      var scrooll = $("input:radio[name=Fjan_scroll]:checked").val();
      var quantidade = $("#Fjan_quantidade").val();
      var logo = $("input:radio[name=Fjan_logo]:checked").val();
      var data = $("input:radio[name=Fjan_data]:checked").val();
      var borda = $("input:radio[name=Fjan_borda]:checked").val();
      var titulo = $("input:radio[name=Fjan_exibirApenasTitulo]:checked").val();
      var foto = $("input:radio[name=Fjan_foto]:checked").val();
        if(foto == undefined) { foto = 0; }
      var posicao = $("input:radio[name=Fjan_foto]:checked").val();
        if(posicao == undefined) { posicao = 0; }
      var inserir = $("input:checkbox[name=Fjan_inserirNoticia]:checked").val();
        if(inserir == undefined) { inserir = 0; }
      var comentar = $("input:checkbox[name=Fjan_comentar]:checked").val();
        if(comentar == undefined) { comentar = 0; }
      var visualizar = $("input:checkbox[name=Fjan_visualizarComentario]:checked").val();
        if(visualizar == undefined) { visualizar = 0; }
      var indicar = $("input:checkbox[name=Fjan_indicar]:checked").val();
        if(indicar == undefined) { indicar = 0; }
      var areas = new Array();
        $('input:checkbox[name="areas[]"]:checked').each(function() {areas.push($(this).val());});
        areas = areas.toString();
        
        if(areas.length > 0) {
           $.post("<?=URL?>/index.php?action=show&secao=janela_cli", { preview: 1, altura: altura, largura: largura,
           cor: cor, scrooll: scrooll, quantidade: quantidade, tipo: tipo, logo: logo, data: data, titulo: titulo,
           foto: foto, posicao: posicao, inserir: inserir, comentar: comentar, visualizar: visualizar, indicar: indicar,
           areas: areas, borda: borda },
           function(data){
             $('#msg').html(data);
           });  
           $('#confirmar').css('display','block');
        } else {
          alert("<?=translate("Selecione ao menos uma área de interesse.")?>");
        }      
    });
    
    $('#confirmar').click(function(){
      
      var url = "<?=URL?>";
      var largura = $("#Fjan_largura").val();
      largura = parseInt(largura)+10;
      if($("input:checkbox[name=Fjan_inserirNoticia]:checked").val() == undefined) {
        var altura = $("#Fjan_altura").val();
        altura = parseInt(altura)+5;
      } else {
        var altura = $("#Fjan_altura").val();
        altura = parseInt(altura)+30;
      }
      
      <? if(isset($_GET['id'])) { ?>
        var id = '<?=$_GET['id']?>'; 
      <? } else { ?>
        var id = '<?//=$view->obterCodigo()?>'; 
      <? } ?>
      
      var mensagem = '<iframe name="Noticiador Web" src="'+url+'index.php?action=show&secao=janela_cli&id='+id+'" frameborder="0" width="'+largura+'" height="'+altura+'" scrolling="no"></iframe>';
      
      $('#code').html(htmlEntities(mensagem));
      
      $('#page1').css('display','none');
      $('#page2').css('display','block');
      $('#enviar').css('display','block');
      $('#voltar').css('display','block');
      $('#confirmar').css('display','none');
      $('#visualizar').css('display','none');
    });
    
    $('#voltar').click(function(){
      $('#page1').css('display','block');
      $('#page2').css('display','none');
      $('#enviar').css('display','none');
      $('#voltar').css('display','none');
      $('#confirmar').css('display','block');
      $('#visualizar').css('display','block');
    });
    
    $('input:radio[name=Fjan_tipo]').change(function(){
      tipo = $("input:radio[name=Fjan_tipo]:checked").val();
      if(tipo == 'F') {
        $('#tdscroll').css('display','inline');        
      } else {
        $('#tdscroll').css('display','none');
      }
    });
    
    $('input:text, input:radio, input:checkbox').keypress(function(e){
      if(e.which == 13){
        e.preventDefault();
      }
    });
    
    
  });

  function htmlEntities(texto){
    var i,carac,letra,novo='';
    for(i=0;i<texto.length;i++){ 
        carac = texto[i].charCodeAt(0);
        if( (carac > 47 && carac < 58) || (carac > 62 && carac < 127) ){
            novo += texto[i];
        }else{
            novo += "&#" + texto[i].charCodeAt(0) + ";";
        }
    }
    return novo;
  }

	frm = document.formulario;
	
  frm.Fjan_nome.nomeDesc='Nome do Script';
	frm.Fjan_altura.nomeDesc='Altura';	
	frm.Fjan_largura.nomeDesc='Largura';	
	frm.Fjan_cor.nomeDesc='Cor';	
	frm.Fjan_tipo.nomeDesc='Tipo';	
	frm.Fjan_quantidade.nomeDesc='Quantidade';		
	frm.Fjan_exibirApenasTitulo.nomeDesc='Exibir Apenas Título';		
	
  frm.Fjan_nome.required="true";
	frm.Fjan_altura.required="true";
	frm.Fjan_largura.required="true";
	frm.Fjan_cor.required="true";
	frm.Fjan_tipo.required="true";
	frm.Fjan_quantidade.required="true";
	frm.Fjan_exibirApenasTitulo.required="true";
  */
</script>
<!--iframe name="iframeJanela" width="0" height="0" frameborder="0"></iframe-->
	</div>
</div>

	<style>
		.ui-dialog .ui-dialog-content iframe.iframe_instrucoes {
			width: 800px;
			height: 600px;
			overflow: scroll;
			overflow-x: hidden !important;
			overflow-y: scroll !important;
		}
		.ui-dialog .ui-dialog-content iframe {
			width: 800px;
			height: 600px;
			overflow: scroll;
			overflow-x: hidden !important;
			overflow-y: scroll !important;
		}
		.ui-dialog .ui-dialog-content iframe::-webkit-scrollbar {  
			display: block;
			visibility: visible;
		}
		.ui-dialog .ui-dialog-content iframe::-webkit-scrollbar {
			width: 1em;
		}

		.ui-dialog .ui-dialog-content iframe::-webkit-scrollbar-track {
			-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		}

		.ui-dialog .ui-dialog-content iframe::-webkit-scrollbar-thumb {
			background-color: darkgrey;
			outline: 1px solid slategrey;
		}
	</style>

	<div id="nweb_plugin_janela_livre" title="Plugin JQuery - Janela Livre" >
		<iframe scrolling="yes" class="iframe_instrucoes" src="<?php echo URL . "system/modules/templates/janela/frm_instrucoes_plugin_janela_livre.php";?>"></iframe>
	</div>

	<div id="nweb_plugin_api_noticias" title="Plugin PHP - API Notícias" >
		<iframe scrolling="yes" class="iframe_instrucoes" src="<?php echo URL . "system/modules/templates/janela/frm_instrucoes_plugin_api_noticias.php";?>"></iframe>
	</div>

	<script type="text/javascript">
		$( "#nweb_plugin_janela_livre" ).dialog({
			autoOpen: false,
			modal: true,
			width: 850,
			height: 650,
			overflow: "auto"
		});
		$( "#nweb_plugin_janela_livre.iframe_instrucoes" ).css("overflow-y","scroll");
		$( "#nweb_plugin_api_noticias" ).dialog({
			autoOpen: false,
			modal: true,
			width: 850,
			height: 650,
			overflow: "auto"
		});
		$( "#nweb_plugin_api_noticias.iframe_instrucoes" ).css("overflow-y","scroll");
	</script>

<?=add_html_tail() ?>

<html>
<head>
</head>
<body>
	<div id="cke_pastebin" style="position: absolute; top: 8px; width: 1px; height: 1px; overflow: hidden; left: -1000px;">
		<div>
			<b><font size="4">Implementa&ccedil;&atilde;o em Wordpress.</font></b></div>
		<div>
			<div>
				&nbsp;</div>
			<div>
				<font size="4">1&deg;</font><b>&nbsp;- Cole o c&oacute;digo abaixo, destacado em vermelho, antes do come&ccedil;o do html no arquivo header.php dentro das tags PHP:</b></div>
		</div>
		<div>
			<font color="#cc0000"><b><span style="background-color: rgb(241, 194, 50);">&lt;?php</span><span style="background-color: rgb(255, 255, 255);">&nbsp;</span></b></font><font color="#ff0000"><b><span style="background-color: rgb(255, 255, 0);">Tag de abertura do c&oacute;digo PHP no come&ccedil;o do arquivo header.php,</span></b></font><span style="background-color: rgb(255, 255, 0);"><b style="color: rgb(255, 0, 0);">&nbsp;tamb&eacute;m</b><b style="color: rgb(255, 0, 0);">&nbsp;pode aparecer na forma &#39;&lt;?&#39;</b></span></div>
		<div>
			&nbsp;</div>
		<div>
			&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<font color="#ff0000" style="background-color: rgb(255, 255, 0);"><b>Cole esse trecho de c&oacute;digo abaixo de outros c&oacute;digos PHP que j&aacute; existam no come&ccedil;o do arquivo</b></font></div>
		<div>
			&nbsp;</div>
		<div>
			<div>
				<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">// C&oacute;digo do plugin php da API de not&iacute;cias do NoticiadorWeb</font></b></div>
			<div>
				<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">try {</font></b></div>
			<div>
				<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">$arrStrScripts = @json_decode(</font></b></div>
			<div>
				<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">file_get_contents(</font></b></div>
			<div>
				<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">&quot;<a href="https://noticiadorweb.com.br/plugins/noticiadorweb/apinoticias/obterScriptPluginAPINoticiasNoticiadorWeb.php" style="color: rgb(17, 85, 204);" target="_blank">https://noticiadorweb.com.br/<wbr />plugins/noticiadorweb/<wbr />apinoticias/<wbr />obterScriptPluginAPINoticiasNo<wbr />ticiadorWeb.php</a>&quot;</font></b></div>
			<div>
				<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">, FILE_TEXT</font></b></div>
			<div>
				<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">)</font></b></div>
			<div>
				<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">, true</font></b></div>
			<div>
				<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">);</font></b></div>
			<div>
				<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">@eval($arrStrScripts[&quot;<wbr />strScriptPluginAPINoticiasNoti<wbr />ciadorWeb&quot;]);</font></b></div>
			<div>
				<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">} catch (Exception $e) {</font></b></div>
			<div>
				<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">echo &#39;&lt;!-- Exce&ccedil;&atilde;o: &#39;, &nbsp;$e-&gt;getMessage(), &quot;--&gt;&quot;;</font></b></div>
			<div>
				<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">}</font></b></div>
			<div>
				<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">// C&oacute;digo do plugin php da API de not&iacute;cias do NoticiadorWeb</font></b></div>
			<div>
				&nbsp;</div>
			<div>
				<b><span style="color: rgb(204, 0, 0); background-color: rgb(241, 194, 50);">?&gt;</span></b><font color="#cc0000"><b>&nbsp;</b></font><font color="#ff0000" style="background-color: rgb(255, 255, 0);"><b>Tag de fechamento do c&oacute;digo PHP no come&ccedil;o do arquivo header.php</b></font></div>
			<div>
				&nbsp;</div>
			<div>
				<b><span style="background-color: rgb(159, 197, 232);"><font color="#20124d">&lt;!DOCTYPE html PUBLIC &quot;-//W3C//DTD XHTML 1.0 Transitional//EN&quot; &quot;<a href="https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" style="color: rgb(17, 85, 204);" target="_blank">https://www.w3.org/TR/xhtml1/<wbr />DTD/xhtml1-transitional.dtd</a>&quot;&gt;</font></span></b>&nbsp;<b style="color: rgb(255, 0, 0); background-color: rgb(255, 255, 0);"><wbr />Come&ccedil;o do html do arquivo header.php</b></div>
		</div>
		<div>
			&nbsp;</div>
		<div>
			<font size="4">2&deg;</font><b>&nbsp;- O c&oacute;digo a seguir ir&aacute; montar o t&iacute;tulo da p&aacute;gina, cole antes da fun&ccedil;&atilde;o do wordpress que monta o t&iacute;tulo da p&aacute;gina.</b></div>
		<div>
			<b>Essa fun&ccedil;&atilde;o aceita como par&acirc;metro n&atilde;o obrigat&oacute;rio uma string no formato JSON &nbsp;para passar par&acirc;metros.</b></div>
		<div>
			<b>Um par&acirc;metro que ela aceita pela string JSON &eacute; o &quot;strSeparador&quot;, passando tamb&eacute;m uma string que ser&aacute; utilizada como separador ao montar o t&iacute;tulo.</b></div>
		<div>
			<b>No arquivo header.php, cole o trecho de c&oacute;digo destacado abaixo em vermelho dentro da tag &lt;title&gt;&lt;/title&gt;:</b></div>
		<div>
			&nbsp;</div>
		<div>
			<div>
				<font color="#20124d" style="background-color: rgb(159, 197, 232);"><b>&lt;title&gt;</b></font><b style="color: rgb(204, 0, 0); background-color: rgb(241, 194, 50);">&lt;?php</b></div>
			<div>
				&nbsp;</div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><b><font color="#ff0000">// C&oacute;digo do plugin da API de not&iacute;cias do NoticiadorWeb</font></b></span></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><b><font color="#ff0000">try {</font></b></span></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><b style="color: rgb(255, 0, 0);">if (class_exists(<wbr />PluginAPINoticiasNoticiadorWeb<wbr />)) {</b></span></div>
			<div>
				<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">$strParametros =&nbsp;&#39;{&quot;strSeparador&quot;: &quot; | &quot;}&#39;;</b></font></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>echo PluginAPINoticiasNoticiadorWeb ::&nbsp;</b></font><b><font color="#ff0000">montarTituloPagina</font></b><font color="#ff0000"><b>($<wbr />strParametros);</b></font></span></div>
			<div>
				<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">}</b></font></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><b><font color="#ff0000">} catch (Exception $e) {</font></b></span></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><b><font color="#ff0000">echo &#39;&lt;!-- Exce&ccedil;&atilde;o: &#39;, &nbsp;$e-&gt;getMessage(), &quot;--&gt;&quot;;</font></b></span></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><b><font color="#ff0000">}</font></b></span></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><b><font color="#ff0000">// C&oacute;digo do plugin da API de not&iacute;cias do NoticiadorWeb</font></b></span></div>
			<div>
				&nbsp;</div>
			<div>
				<span style="background-color: rgb(241, 194, 50);"><font color="#cc0000"><b>$theme-&gt;meta_title();</b></font></span>&nbsp;<span style="background-color: rgb(255, 255, 0);"><b><font color="#ff0000">&lt;-- Essa &eacute; uma das poss&iacute;veis fun&ccedil;&otilde;es do Wordpress que podem estar sendo utilizadas na tag &lt;title&gt;&lt;/title&gt; do tema, veja que o c&oacute;digo do plugin foi colado antes</font></b></span></div>
			<div>
				&nbsp;</div>
			<div>
				<span style="background-color: rgb(241, 194, 50);"><font color="#cc0000"><b>?&gt;</b></font></span><span style="background-color: rgb(159, 197, 232);"><b><font color="#20124d">&lt;/title&gt;</font></b></span></div>
		</div>
		<div>
			&nbsp;</div>
		<div>
			<font size="4">3&deg;</font><b>&nbsp;- Ainda no arquivo header.php, cole o seguinte c&oacute;digo antes da tag de fechamento &lt;/head&gt;, uma parte dentro das tags PHP e a outra parte fora, no html:</b></div>
		<div>
			&nbsp;</div>
		<div>
			<span style="background-color: rgb(241, 194, 50);"><b><font color="#cc0000">&lt;?php</font></b></span></div>
		<div>
			<div>
				&nbsp;</div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>// C&oacute;digo do plugin da API de not&iacute;cias do NoticiadorWeb</b></font></span></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>try {</b></font></span></div>
			<div>
				<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">if (class_exists(<wbr />PluginAPINoticiasNoticiadorWeb<wbr />)) {</b></font></div>
			<div>
				<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">echo PluginAPINoticiasNoticiadorWeb :: montarScriptsHeader();</b></font></div>
			<div>
				<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">}</b></font></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>} catch (Exception $e) {</b></font></span></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>echo &#39;&lt;!-- Exce&ccedil;&atilde;o: &#39;, &nbsp;$e-&gt;getMessage(), &quot;--&gt;&quot;;</b></font></span></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>}</b></font></span></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>// C&oacute;digo do plugin da API de not&iacute;cias do NoticiadorWeb</b></font></span></div>
			<div>
				&nbsp;</div>
			<div>
				<span style="background-color: rgb(241, 194, 50);"><font color="#cc0000"><b>?&gt;</b></font></span></div>
			<div>
				&nbsp;</div>
			<div>
				&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<span style="background-color: rgb(255, 255, 0);"><b><font color="#ff0000">Esse trecho de c&oacute;digo abaixo &eacute; a chamada que dever&aacute; apontar pra um arquivo CSS no site do cliente que voc&ecirc; utilizar&aacute; para fazer a personaliza&ccedil;&atilde;o do layout da p&aacute;gina.</font></b></span></div>
			<div>
				&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<span style="background-color: rgb(255, 255, 0);"><b><font color="#ff0000">Recomendo que mantenha o mesmo nome para esse arquivo em todos os clientes para facilitar seu trabalho de implementa&ccedil;&atilde;o.</font></b></span></div>
			<div>
				&nbsp;</div>
			<div>
				<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">&lt;!-- CSS customizado do plugin php da API de not&iacute;cias do NoticiadorWeb--&gt;</font></b></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><span style="font-weight: bold;">&lt;link rel=&#39;stylesheet&#39; id=&#39;noticiadorweb-<wbr />pluginapinoticias-</span><span style="font-weight: bold;">css&#39; &nbsp;href=&#39;</span></font></span><span style="font-weight: bold; background-color: rgb(255, 255, 0);"><font color="#ff0000">COLAR_AQUI_CAMINHO_<wbr />PARA_ARQUIVO_CSS_NO_SITE_DO_<wbr />CLIENTE</font></span><a href="https://www.portaldolocador.com/wp-content/themes/portal2013/css/plugin.apinoticias.noticiadorweb.custom.css" style="color: rgb(17, 85, 204); background-color: rgb(234, 209, 220);" target="_blank"><b><font color="#ff0000">/plugin.apinoticias.<wbr />noticiadorweb.custom.css</font></b></a><span style="font-weight: bold; color: rgb(32, 18, 77); background-color: rgb(159, 197, 232);">&#39; type=&#39;text/css&#39; media=&#39;all&#39; /&gt;</span></div>
			<div>
				<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">&lt;!-- CSS customizado do plugin php da API de not&iacute;cias do NoticiadorWeb--&gt;</font></b></div>
		</div>
		<div>
			<div>
				&nbsp;</div>
			<div>
				<span style="background-color: rgb(159, 197, 232);"><b><font color="#20124d">&lt;/head&gt;</font></b></span></div>
		</div>
		<div>
			&nbsp;</div>
		<div>
			<font size="4">4&deg;</font><b>&nbsp;- No arquivo footer.php, cole o c&oacute;digo destacado em vermelho a seguir antes da tag de fechamento &lt;/body&gt;:</b></div>
		<div>
			&nbsp;</div>
		<div>
			<div>
				<span style="background-color: rgb(241, 194, 50);"><b><font color="#cc0000">&lt;?php</font></b></span></div>
			<div>
				&nbsp;</div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>// C&oacute;digo do plugin da API de not&iacute;cias do NoticiadorWeb</b></font></span></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>try {</b></font></span></div>
			<div>
				<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">if (class_exists(<wbr />PluginAPINoticiasNoticiadorWeb<wbr />)) {</b></font></div>
			<div>
				<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">echo PluginAPINoticiasNoticiadorWeb :: montarScriptsFooter();</b></font></div>
			<div>
				<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">}</b></font></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>} catch (Exception $e) {</b></font></span></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>echo &#39;&lt;!-- Exce&ccedil;&atilde;o: &#39;, &nbsp;$e-&gt;getMessage(), &quot;--&gt;&quot;;</b></font></span></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>}</b></font></span></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>// C&oacute;digo do plugin da API de not&iacute;cias do NoticiadorWeb</b></font></span></div>
			<div>
				&nbsp;</div>
			<div>
				<span style="background-color: rgb(241, 194, 50);"><font color="#cc0000"><b>?&gt;</b></font></span></div>
			<div>
				&nbsp;</div>
			<div>
				<span style="background-color: rgb(159, 197, 232);"><font color="#20124d"><b>&lt;/body&gt;</b></font></span></div>
		</div>
		<div>
			&nbsp;</div>
		<div>
			<font size="4">5&deg;</font><b>&nbsp;- O &uacute;ltimo passo &eacute; colar em um post ou page do wordpress o trecho de c&oacute;digo a seguir destacado em vermelho utilizando as tags de insert_php.</b></div>
		<p>
			<span style="font-weight: bold;">Essa fun&ccedil;&atilde;o aceita como par&acirc;metro n&atilde;o obrigat&oacute;rio uma string no formato JSON &nbsp;para passar par&acirc;metros.</span></p>
		<div>
			<span style="font-weight: bold;">Par&acirc;metros que podem ser passados pela string JSON:</span></div>
		<div>
			<div>
				<b>intIDGN : C&oacute;digo de Janela Livre gerado no Noticiador;</b></div>
			<div>
				<b>intQtdNotListagem : Quantidade de not&iacute;cias exibidas na listagem, o valor &quot;0&quot; lista todas as not&iacute;cias e &eacute; o valor default;</b></div>
			<div>
				<b>intQtdColListagem : Quantidade de colunas por linha na listagem, o valor default &eacute; &quot;2&quot;;</b></div>
			<div>
				<b>intQtdNotRel : Quantidade de not&iacute;cias relacionadas exibidas, o valor default &eacute; &quot;4&quot;;</b></div>
			<div>
				<b>strDefaultThumbnail : String do caminho de uma imagem miniatura padr&atilde;o utilizada quando a not&iacute;cia n&atilde;o tiver miniatura definida;</b></div>
			<div>
				<b>booContarViews : Ativa&ccedil;&atilde;o da contagem de visualiza&ccedil;&otilde;es, aceita valores true / false, o valor default &eacute; true;</b></div>
			<div>
				<span style="font-weight: bold;">strCharsetComent : Ativa&ccedil;&atilde;o de corre&ccedil;&atilde;o do charset do coment&aacute;rio submetido pelo formul&aacute;rio de coment&aacute;rio, aceita o valor &quot;utf8&quot;</span><span style="font-weight: bold;">;</span></div>
			<div>
				&nbsp;</div>
			<div>
				<div>
					<span style="background-color: rgb(241, 194, 50);"><font color="#cc0000"><b>[insert_php]</b></font></span></div>
				<div>
					&nbsp;</div>
				<div>
					<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>// C&oacute;digo do plugin da API de not&iacute;cias do NoticiadorWeb</b></font></span></div>
				<div>
					<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>try {</b></font></span></div>
				<div>
					<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">if (class_exists(<wbr />PluginAPINoticiasNoticiadorWeb<wbr />)) {</b></font></div>
				<div>
					<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>$strParametros = &#39;{&quot;intIDGN&quot;: &quot;31&quot;</b></font><b style="color: rgb(255, 0, 0);">, &quot;intQtdColListagem&quot;: &quot;3&quot;</b><b style="color: rgb(255, 0, 0);">, &quot;strDefaultThumbnail&quot;: &quot;<a href="https://www.portaldolocador.com/wp-content/themes/portal2013/images/logopdl.png" style="color: rgb(17, 85, 204);" target="_blank">https://www.portaldolocador.<wbr />com/wp-content/themes/<wbr />portal2013/images/logopdl.png</a>&quot;<wbr />}&#39;;</b></span></div>
				<div>
					<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">echo PluginAPINoticiasNoticiadorWeb :: montarPagina($strParametros);</b></font></div>
				<div>
					<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">}</b></font></div>
				<div>
					<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>} catch (Exception $e) {</b></font></span></div>
				<div>
					<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>echo &#39;&lt;!-- Exce&ccedil;&atilde;o: &#39;, &nbsp;$e-&gt;getMessage(), &quot;--&gt;&quot;;</b></font></span></div>
				<div>
					<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>}</b></font></span></div>
				<div>
					<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>// C&oacute;digo do plugin da API de not&iacute;cias do NoticiadorWeb</b></font></span></div>
				<div>
					&nbsp;</div>
				<div>
					<span style="background-color: rgb(241, 194, 50);"><b><font color="#cc0000">[/insert_php]</font></b></span></div>
			</div>
			<div>
				&nbsp;</div>
			<div>
				<div>
					&nbsp;</div>
				<div>
					&nbsp;</div>
				<div>
					<b><font size="4">Implementa&ccedil;&atilde;o em site PHP.</font></b></div>
				<div>
					<div>
						&nbsp;</div>
					<div>
						<b>Para implementar em qualquer p&aacute;gina PHP, voc&ecirc; deve seguir as primeiras 4 etapas da implementa&ccedil;&atilde;o em wordpress, tendo o cuidado de colar os c&oacute;digos nas &aacute;reas de cabe&ccedil;alho e rodap&eacute; correspondentes do layout do site do cliente.</b></div>
				</div>
			</div>
			<div>
				<div style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; background-color: rgb(255, 255, 255);">
					<div>
						<b>A &uacute;nica mudan&ccedil;a &eacute; na quinta etapa, na qual voc&ecirc; precisa procurar a &aacute;rea onde colar o c&oacute;digo que monta a p&aacute;gina de not&iacute;cia, e colocar entre tags PHP.</b></div>
					<div>
						<b>Cole o c&oacute;digo abaixo, destacado em vermelho dentro das tags PHP:</b></div>
				</div>
				<div style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; background-color: rgb(255, 255, 255);">
					&nbsp;</div>
				<div style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; background-color: rgb(255, 255, 255);">
					<div>
						<b><font color="#cc0000" style="background-color: rgb(241, 194, 50);">&lt;?php</font></b></div>
					<div>
						&nbsp;</div>
					<div>
						<div>
							<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>// C&oacute;digo do plugin da API de not&iacute;cias do NoticiadorWeb</b></font></span></div>
						<div>
							<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>try {</b></font></span></div>
						<div>
							<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">if (class_exists(<wbr />PluginAPINoticiasNoticiadorWeb<wbr />)) {</b></font></div>
						<div>
							<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>$strParametros = &#39;{&quot;intIDGN&quot;: &quot;31&quot;</b></font><b style="color: rgb(255, 0, 0);">, &quot;intQtdColListagem&quot;: &quot;3&quot;</b><b style="color: rgb(255, 0, 0);">, &quot;strDefaultThumbnail&quot;: &quot;<a href="https://www.portaldolocador.com/wp-content/themes/portal2013/images/logopdl.png" style="color: rgb(17, 85, 204);" target="_blank">https://www.portaldolocador.<wbr />com/wp-content/themes/<wbr />portal2013/images/logopdl.png</a>&quot;<wbr />}&#39;;</b></span></div>
						<div>
							<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">echo PluginAPINoticiasNoticiadorWeb :: montarPagina($strParametros);</b></font></div>
						<div>
							<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">}</b></font></div>
						<div>
							<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>} catch (Exception $e) {</b></font></span></div>
						<div>
							<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>echo &#39;&lt;!-- Exce&ccedil;&atilde;o: &#39;, &nbsp;$e-&gt;getMessage(), &quot;--&gt;&quot;;</b></font></span></div>
						<div>
							<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>}</b></font></span></div>
						<div>
							<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>// C&oacute;digo do plugin da API de not&iacute;cias do NoticiadorWeb</b></font></span></div>
					</div>
					<div>
						&nbsp;</div>
					<div>
						<b style="color: rgb(204, 0, 0); background-color: rgb(241, 194, 50);">?&gt;</b></div>
					<div>
						&nbsp;</div>
				</div>
			</div>
		</div>
	</div>
<div>
	<div>
		<b><font size="4">Implementa&ccedil;&atilde;o em Wordpress.</font></b></div>
	<div>
		<div>
			&nbsp;</div>
		<div>
			<font size="4">1&deg;</font><b>&nbsp;- Cole o c&oacute;digo abaixo, destacado em vermelho, antes do come&ccedil;o do html no arquivo header.php dentro das tags PHP:</b></div>
	</div>
	<div>
		<font color="#cc0000"><b><span style="background-color: rgb(241, 194, 50);">&lt;?php</span><span style="background-color: rgb(255, 255, 255);">&nbsp;</span></b></font><font color="#ff0000"><b><span style="background-color: rgb(255, 255, 0);">Tag de abertura do c&oacute;digo PHP no come&ccedil;o do arquivo header.php,</span></b></font><span style="background-color: rgb(255, 255, 0);"><b style="color: rgb(255, 0, 0);">&nbsp;tamb&eacute;m</b><b style="color: rgb(255, 0, 0);">&nbsp;pode aparecer na forma &#39;&lt;?&#39;</b></span></div>
	<div>
		&nbsp;</div>
	<div>
		&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<font color="#ff0000" style="background-color: rgb(255, 255, 0);"><b>Cole esse trecho de c&oacute;digo abaixo de outros c&oacute;digos PHP que j&aacute; existam no come&ccedil;o do arquivo</b></font></div>
	<div>
		&nbsp;</div>
	<div>
		<div>
			<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">// C&oacute;digo do plugin php da API de not&iacute;cias do NoticiadorWeb</font></b></div>
		<div>
			<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">try {</font></b></div>
		<div>
			<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">$arrStrScripts = @json_decode(</font></b></div>
		<div>
			<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">file_get_contents(</font></b></div>
		<div>
			<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">&quot;<a href="https://noticiadorweb.com.br/plugins/noticiadorweb/apinoticias/obterScriptPluginAPINoticiasNoticiadorWeb.php" style="color: rgb(17, 85, 204);" target="_blank">https://noticiadorweb.com.br/<wbr />plugins/noticiadorweb/<wbr />apinoticias/<wbr />obterScriptPluginAPINoticiasNo<wbr />ticiadorWeb.php</a>&quot;</font></b></div>
		<div>
			<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">, FILE_TEXT</font></b></div>
		<div>
			<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">)</font></b></div>
		<div>
			<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">, true</font></b></div>
		<div>
			<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">);</font></b></div>
		<div>
			<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">@eval($arrStrScripts[&quot;<wbr />strScriptPluginAPINoticiasNoti<wbr />ciadorWeb&quot;]);</font></b></div>
		<div>
			<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">} catch (Exception $e) {</font></b></div>
		<div>
			<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">echo &#39;&lt;!-- Exce&ccedil;&atilde;o: &#39;, &nbsp;$e-&gt;getMessage(), &quot;--&gt;&quot;;</font></b></div>
		<div>
			<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">}</font></b></div>
		<div>
			<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">// C&oacute;digo do plugin php da API de not&iacute;cias do NoticiadorWeb</font></b></div>
		<div>
			&nbsp;</div>
		<div>
			<b><span style="color: rgb(204, 0, 0); background-color: rgb(241, 194, 50);">?&gt;</span></b><font color="#cc0000"><b>&nbsp;</b></font><font color="#ff0000" style="background-color: rgb(255, 255, 0);"><b>Tag de fechamento do c&oacute;digo PHP no come&ccedil;o do arquivo header.php</b></font></div>
		<div>
			&nbsp;</div>
		<div>
			<b><span style="background-color: rgb(159, 197, 232);"><font color="#20124d">&lt;!DOCTYPE html PUBLIC &quot;-//W3C//DTD XHTML 1.0 Transitional//EN&quot; &quot;<a href="https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" style="color: rgb(17, 85, 204);" target="_blank">https://www.w3.org/TR/xhtml1/<wbr />DTD/xhtml1-transitional.dtd</a>&quot;&gt;</font></span></b>&nbsp;<b style="color: rgb(255, 0, 0); background-color: rgb(255, 255, 0);"><wbr />Come&ccedil;o do html do arquivo header.php</b></div>
	</div>
	<div>
		&nbsp;</div>
	<div>
		<font size="4">2&deg;</font><b>&nbsp;- O c&oacute;digo a seguir ir&aacute; montar o t&iacute;tulo da p&aacute;gina, cole antes da fun&ccedil;&atilde;o do wordpress que monta o t&iacute;tulo da p&aacute;gina.</b></div>
	<div>
		<b>Essa fun&ccedil;&atilde;o aceita como par&acirc;metro n&atilde;o obrigat&oacute;rio uma string no formato JSON &nbsp;para passar par&acirc;metros.</b></div>
	<div>
		<b>Um par&acirc;metro que ela aceita pela string JSON &eacute; o &quot;strSeparador&quot;, passando tamb&eacute;m uma string que ser&aacute; utilizada como separador ao montar o t&iacute;tulo.</b></div>
	<div>
		<b>No arquivo header.php, cole o trecho de c&oacute;digo destacado abaixo em vermelho dentro da tag &lt;title&gt;&lt;/title&gt;:</b></div>
	<div>
		&nbsp;</div>
	<div>
		<div>
			<font color="#20124d" style="background-color: rgb(159, 197, 232);"><b>&lt;title&gt;</b></font><b style="color: rgb(204, 0, 0); background-color: rgb(241, 194, 50);">&lt;?php</b></div>
		<div>
			&nbsp;</div>
		<div>
			<span style="background-color: rgb(234, 209, 220);"><b><font color="#ff0000">// C&oacute;digo do plugin da API de not&iacute;cias do NoticiadorWeb</font></b></span></div>
		<div>
			<span style="background-color: rgb(234, 209, 220);"><b><font color="#ff0000">try {</font></b></span></div>
		<div>
			<span style="background-color: rgb(234, 209, 220);"><b style="color: rgb(255, 0, 0);">if (class_exists(<wbr />PluginAPINoticiasNoticiadorWeb<wbr />)) {</b></span></div>
		<div>
			<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">$strParametros =&nbsp;&#39;{&quot;strSeparador&quot;: &quot; | &quot;}&#39;;</b></font></div>
		<div>
			<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>echo PluginAPINoticiasNoticiadorWeb ::&nbsp;</b></font><b><font color="#ff0000">montarTituloPagina</font></b><font color="#ff0000"><b>($<wbr />strParametros);</b></font></span></div>
		<div>
			<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">}</b></font></div>
		<div>
			<span style="background-color: rgb(234, 209, 220);"><b><font color="#ff0000">} catch (Exception $e) {</font></b></span></div>
		<div>
			<span style="background-color: rgb(234, 209, 220);"><b><font color="#ff0000">echo &#39;&lt;!-- Exce&ccedil;&atilde;o: &#39;, &nbsp;$e-&gt;getMessage(), &quot;--&gt;&quot;;</font></b></span></div>
		<div>
			<span style="background-color: rgb(234, 209, 220);"><b><font color="#ff0000">}</font></b></span></div>
		<div>
			<span style="background-color: rgb(234, 209, 220);"><b><font color="#ff0000">// C&oacute;digo do plugin da API de not&iacute;cias do NoticiadorWeb</font></b></span></div>
		<div>
			&nbsp;</div>
		<div>
			<span style="background-color: rgb(241, 194, 50);"><font color="#cc0000"><b>$theme-&gt;meta_title();</b></font></span>&nbsp;<span style="background-color: rgb(255, 255, 0);"><b><font color="#ff0000">&lt;-- Essa &eacute; uma das poss&iacute;veis fun&ccedil;&otilde;es do Wordpress que podem estar sendo utilizadas na tag &lt;title&gt;&lt;/title&gt; do tema, veja que o c&oacute;digo do plugin foi colado antes</font></b></span></div>
		<div>
			&nbsp;</div>
		<div>
			<span style="background-color: rgb(241, 194, 50);"><font color="#cc0000"><b>?&gt;</b></font></span><span style="background-color: rgb(159, 197, 232);"><b><font color="#20124d">&lt;/title&gt;</font></b></span></div>
	</div>
	<div>
		&nbsp;</div>
	<div>
		<font size="4">3&deg;</font><b>&nbsp;- Ainda no arquivo header.php, cole o seguinte c&oacute;digo antes da tag de fechamento &lt;/head&gt;, uma parte dentro das tags PHP e a outra parte fora, no html:</b></div>
	<div>
		&nbsp;</div>
	<div>
		<span style="background-color: rgb(241, 194, 50);"><b><font color="#cc0000">&lt;?php</font></b></span></div>
	<div>
		<div>
			&nbsp;</div>
		<div>
			<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>// C&oacute;digo do plugin da API de not&iacute;cias do NoticiadorWeb</b></font></span></div>
		<div>
			<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>try {</b></font></span></div>
		<div>
			<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">if (class_exists(<wbr />PluginAPINoticiasNoticiadorWeb<wbr />)) {</b></font></div>
		<div>
			<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">echo PluginAPINoticiasNoticiadorWeb :: montarScriptsHeader();</b></font></div>
		<div>
			<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">}</b></font></div>
		<div>
			<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>} catch (Exception $e) {</b></font></span></div>
		<div>
			<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>echo &#39;&lt;!-- Exce&ccedil;&atilde;o: &#39;, &nbsp;$e-&gt;getMessage(), &quot;--&gt;&quot;;</b></font></span></div>
		<div>
			<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>}</b></font></span></div>
		<div>
			<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>// C&oacute;digo do plugin da API de not&iacute;cias do NoticiadorWeb</b></font></span></div>
		<div>
			&nbsp;</div>
		<div>
			<span style="background-color: rgb(241, 194, 50);"><font color="#cc0000"><b>?&gt;</b></font></span></div>
		<div>
			&nbsp;</div>
		<div>
			&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<span style="background-color: rgb(255, 255, 0);"><b><font color="#ff0000">Esse trecho de c&oacute;digo abaixo &eacute; a chamada que dever&aacute; apontar pra um arquivo CSS no site do cliente que voc&ecirc; utilizar&aacute; para fazer a personaliza&ccedil;&atilde;o do layout da p&aacute;gina.</font></b></span></div>
		<div>
			&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<span style="background-color: rgb(255, 255, 0);"><b><font color="#ff0000">Recomendo que mantenha o mesmo nome para esse arquivo em todos os clientes para facilitar seu trabalho de implementa&ccedil;&atilde;o.</font></b></span></div>
		<div>
			&nbsp;</div>
		<div>
			<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">&lt;!-- CSS customizado do plugin php da API de not&iacute;cias do NoticiadorWeb--&gt;</font></b></div>
		<div>
			<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><span style="font-weight: bold;">&lt;link rel=&#39;stylesheet&#39; id=&#39;noticiadorweb-<wbr />pluginapinoticias-</span><span style="font-weight: bold;">css&#39; &nbsp;href=&#39;</span></font></span><span style="font-weight: bold; background-color: rgb(255, 255, 0);"><font color="#ff0000">COLAR_AQUI_CAMINHO_<wbr />PARA_ARQUIVO_CSS_NO_SITE_DO_<wbr />CLIENTE</font></span><a href="https://www.portaldolocador.com/wp-content/themes/portal2013/css/plugin.apinoticias.noticiadorweb.custom.css" style="color: rgb(17, 85, 204); background-color: rgb(234, 209, 220);" target="_blank"><b><font color="#ff0000">/plugin.apinoticias.<wbr />noticiadorweb.custom.css</font></b></a><span style="font-weight: bold; color: rgb(32, 18, 77); background-color: rgb(159, 197, 232);">&#39; type=&#39;text/css&#39; media=&#39;all&#39; /&gt;</span></div>
		<div>
			<b><font color="#ff0000" style="background-color: rgb(234, 209, 220);">&lt;!-- CSS customizado do plugin php da API de not&iacute;cias do NoticiadorWeb--&gt;</font></b></div>
	</div>
	<div>
		<div>
			&nbsp;</div>
		<div>
			<span style="background-color: rgb(159, 197, 232);"><b><font color="#20124d">&lt;/head&gt;</font></b></span></div>
	</div>
	<div>
		&nbsp;</div>
	<div>
		<font size="4">4&deg;</font><b>&nbsp;- No arquivo footer.php, cole o c&oacute;digo destacado em vermelho a seguir antes da tag de fechamento &lt;/body&gt;:</b></div>
	<div>
		&nbsp;</div>
	<div>
		<div>
			<span style="background-color: rgb(241, 194, 50);"><b><font color="#cc0000">&lt;?php</font></b></span></div>
		<div>
			&nbsp;</div>
		<div>
			<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>// C&oacute;digo do plugin da API de not&iacute;cias do NoticiadorWeb</b></font></span></div>
		<div>
			<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>try {</b></font></span></div>
		<div>
			<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">if (class_exists(<wbr />PluginAPINoticiasNoticiadorWeb<wbr />)) {</b></font></div>
		<div>
			<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">echo PluginAPINoticiasNoticiadorWeb :: montarScriptsFooter();</b></font></div>
		<div>
			<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">}</b></font></div>
		<div>
			<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>} catch (Exception $e) {</b></font></span></div>
		<div>
			<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>echo &#39;&lt;!-- Exce&ccedil;&atilde;o: &#39;, &nbsp;$e-&gt;getMessage(), &quot;--&gt;&quot;;</b></font></span></div>
		<div>
			<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>}</b></font></span></div>
		<div>
			<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>// C&oacute;digo do plugin da API de not&iacute;cias do NoticiadorWeb</b></font></span></div>
		<div>
			&nbsp;</div>
		<div>
			<span style="background-color: rgb(241, 194, 50);"><font color="#cc0000"><b>?&gt;</b></font></span></div>
		<div>
			&nbsp;</div>
		<div>
			<span style="background-color: rgb(159, 197, 232);"><font color="#20124d"><b>&lt;/body&gt;</b></font></span></div>
	</div>
	<div>
		&nbsp;</div>
	<div>
		<font size="4">5&deg;</font><b>&nbsp;- O &uacute;ltimo passo &eacute; colar em um post ou page do wordpress o trecho de c&oacute;digo a seguir destacado em vermelho utilizando as tags de insert_php.</b></div>
	<span style="font-weight: bold;">Essa fun&ccedil;&atilde;o aceita como par&acirc;metro n&atilde;o obrigat&oacute;rio uma string no formato JSON &nbsp;para passar par&acirc;metros.</span>
	<div>
		<span style="font-weight: bold;">Par&acirc;metros que podem ser passados pela string JSON:</span></div>
	<div>
		<div>
			<b>intIDGN : C&oacute;digo de Janela Livre gerado no Noticiador;</b></div>
		<div>
			<b>intQtdNotListagem : Quantidade de not&iacute;cias exibidas na listagem, o valor &quot;0&quot; lista todas as not&iacute;cias e &eacute; o valor default;</b></div>
		<div>
			<b>intQtdColListagem : Quantidade de colunas por linha na listagem, o valor default &eacute; &quot;2&quot;;</b></div>
		<div>
			<b>intQtdNotRel : Quantidade de not&iacute;cias relacionadas exibidas, o valor default &eacute; &quot;4&quot;;</b></div>
		<div>
			<b>strDefaultThumbnail : String do caminho de uma imagem miniatura padr&atilde;o utilizada quando a not&iacute;cia n&atilde;o tiver miniatura definida;</b></div>
		<div>
			<b>booContarViews : Ativa&ccedil;&atilde;o da contagem de visualiza&ccedil;&otilde;es, aceita valores true / false, o valor default &eacute; true;</b></div>
		<div>
			<span style="font-weight: bold;">strCharsetComent : Ativa&ccedil;&atilde;o de corre&ccedil;&atilde;o do charset do coment&aacute;rio submetido pelo formul&aacute;rio de coment&aacute;rio, aceita o valor &quot;utf8&quot;</span><span style="font-weight: bold;">;</span></div>
		<div>
			&nbsp;</div>
		<div>
			<div>
				<span style="background-color: rgb(241, 194, 50);"><font color="#cc0000"><b>[insert_php]</b></font></span></div>
			<div>
				&nbsp;</div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>// C&oacute;digo do plugin da API de not&iacute;cias do NoticiadorWeb</b></font></span></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>try {</b></font></span></div>
			<div>
				<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">if (class_exists(<wbr />PluginAPINoticiasNoticiadorWeb<wbr />)) {</b></font></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>$strParametros = &#39;{&quot;intIDGN&quot;: &quot;31&quot;</b></font><b style="color: rgb(255, 0, 0);">, &quot;intQtdColListagem&quot;: &quot;3&quot;</b><b style="color: rgb(255, 0, 0);">, &quot;strDefaultThumbnail&quot;: &quot;<a href="https://www.portaldolocador.com/wp-content/themes/portal2013/images/logopdl.png" style="color: rgb(17, 85, 204);" target="_blank">https://www.portaldolocador.<wbr />com/wp-content/themes/<wbr />portal2013/images/logopdl.png</a>&quot;<wbr />}&#39;;</b></span></div>
			<div>
				<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">echo PluginAPINoticiasNoticiadorWeb :: montarPagina($strParametros);</b></font></div>
			<div>
				<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">}</b></font></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>} catch (Exception $e) {</b></font></span></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>echo &#39;&lt;!-- Exce&ccedil;&atilde;o: &#39;, &nbsp;$e-&gt;getMessage(), &quot;--&gt;&quot;;</b></font></span></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>}</b></font></span></div>
			<div>
				<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>// C&oacute;digo do plugin da API de not&iacute;cias do NoticiadorWeb</b></font></span></div>
			<div>
				&nbsp;</div>
			<div>
				<span style="background-color: rgb(241, 194, 50);"><b><font color="#cc0000">[/insert_php]</font></b></span></div>
		</div>
		<div>
			&nbsp;</div>
		<div>
			<div>
				&nbsp;</div>
			<div>
				&nbsp;</div>
			<div>
				<b><font size="4">Implementa&ccedil;&atilde;o em site PHP.</font></b></div>
			<div>
				<div>
					&nbsp;</div>
				<div>
					<b>Para implementar em qualquer p&aacute;gina PHP, voc&ecirc; deve seguir as primeiras 4 etapas da implementa&ccedil;&atilde;o em wordpress, tendo o cuidado de colar os c&oacute;digos nas &aacute;reas de cabe&ccedil;alho e rodap&eacute; correspondentes do layout do site do cliente.</b></div>
			</div>
		</div>
		<div>
			<div style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; background-color: rgb(255, 255, 255);">
				<div>
					<b>A &uacute;nica mudan&ccedil;a &eacute; na quinta etapa, na qual voc&ecirc; precisa procurar a &aacute;rea onde colar o c&oacute;digo que monta a p&aacute;gina de not&iacute;cia, e colocar entre tags PHP.</b></div>
				<div>
					<b>Cole o c&oacute;digo abaixo, destacado em vermelho dentro das tags PHP:</b></div>
			</div>
			<div style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; background-color: rgb(255, 255, 255);">
				&nbsp;</div>
			<div style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; background-color: rgb(255, 255, 255);">
				<div>
					<b><font color="#cc0000" style="background-color: rgb(241, 194, 50);">&lt;?php</font></b></div>
				<div>
					&nbsp;</div>
				<div>
					<div>
						<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>// C&oacute;digo do plugin da API de not&iacute;cias do NoticiadorWeb</b></font></span></div>
					<div>
						<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>try {</b></font></span></div>
					<div>
						<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">if (class_exists(<wbr />PluginAPINoticiasNoticiadorWeb<wbr />)) {</b></font></div>
					<div>
						<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>$strParametros = &#39;{&quot;intIDGN&quot;: &quot;31&quot;</b></font><b style="color: rgb(255, 0, 0);">, &quot;intQtdColListagem&quot;: &quot;3&quot;</b><b style="color: rgb(255, 0, 0);">, &quot;strDefaultThumbnail&quot;: &quot;<a href="https://www.portaldolocador.com/wp-content/themes/portal2013/images/logopdl.png" style="color: rgb(17, 85, 204);" target="_blank">https://www.portaldolocador.<wbr />com/wp-content/themes/<wbr />portal2013/images/logopdl.png</a>&quot;<wbr />}&#39;;</b></span></div>
					<div>
						<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">echo PluginAPINoticiasNoticiadorWeb :: montarPagina($strParametros);</b></font></div>
					<div>
						<font color="#ff0000"><b style="background-color: rgb(234, 209, 220);">}</b></font></div>
					<div>
						<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>} catch (Exception $e) {</b></font></span></div>
					<div>
						<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>echo &#39;&lt;!-- Exce&ccedil;&atilde;o: &#39;, &nbsp;$e-&gt;getMessage(), &quot;--&gt;&quot;;</b></font></span></div>
					<div>
						<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>}</b></font></span></div>
					<div>
						<span style="background-color: rgb(234, 209, 220);"><font color="#ff0000"><b>// C&oacute;digo do plugin da API de not&iacute;cias do NoticiadorWeb</b></font></span></div>
				</div>
				<div>
					&nbsp;</div>
				<div>
					<b style="color: rgb(204, 0, 0); background-color: rgb(241, 194, 50);">?&gt;</b></div>
				<div>
					&nbsp;</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>
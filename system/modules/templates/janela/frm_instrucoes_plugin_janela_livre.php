<html>
<head>
</head>
<body>
					<h1 class="default_title">Plugin de Notícias Janela Livre do NoticiadorWeb</h1>

					<p>
						Você acabou de gerar um código para ser utilizado no plugin de notícias do NoticiadorWeb. O código gerado para o seu plugin é: <span style="font-size: 14px; font-weight: bold">$janela_id</span>
					</p>

					<p>
						Seguem as instruções para utilização do plugin de janela livre do NoticiadorWeb:
					</p>

					<p>
						Para utilizar o plugin de Janela Livre do NoticiadorWeb primeiro chame a biblioteca do plugin no site onde irá utilizá-lo a partir do endereço abaixo:
						<br />
						https://noticiadorweb.com.br/scripts/jquery.noticiadorweb.js

					</p>

					<p>
						Em seguida, crie uma <code>&lt;div&gt;</code> vazia na página e defina um id para a mesma.
					</p>

					<p>
						<code>
							&lt;div id="id"&gt;&lt;/div&gt;
						</code>
					</p>

					<p>
						Depois, chame o plugin do NoticiadorWeb nesse id. Segue abaixo um exemplo de chamada do plugin.
					</p>

					<pre>
						<code>

jQuery(document).ready(

	function(){

		jQuery('#nwebPdLDestaques').noticiadorWeb(
			{
				"idScript": 0
				, "qtdNoticias": 3
				, "urlExibicaoNoticia": 'https://www.siteondeserainstaladooplugin.com/noticias/index.php'
				, "urlParametrosListagem": true
				, "classContainerNoticias": 'nw_lista_noticias'
				, "htmlContainerNoticias": '' +
					'&lt;ul class=&quot;nw_lista_noticias&quot;&gt;' +
					'&lt;/ul&gt;'
				, "htmlNoticia": '' +
					'&lt;li class=&quot;nw_noticia&quot;&gt;' +
					'&lt;span class=&quot;nw_thumbnail&quot;&gt;' +
					'&lt;a class=&quot;nw_link_noticia&quot; href=&quot;#linknoticia#&quot; target=&quot;_blank&quot;&gt;' +
					'#thumbnail#' +
					'&lt;/a&gt;' +
					'&lt;/span&gt;' +
					'&lt;span class=&quot;nw_titulo&quot;&gt;' +
					'&lt;a class=&quot;nw_link_noticia&quot; href=&quot;#linknoticia#&quot; target=&quot;_blank&quot;&gt;' +
					'#titulo#' +
					'&lt;/a&gt;' +
					'&lt;/span&gt;' +
					'&lt;span class=&quot;nw_resumo&quot;&gt;' +
					'&lt;a class=&quot;nw_link_noticia&quot; href=&quot;#linknoticia#&quot; target=&quot;_blank&quot;&gt;' +
					'#resumo#' +
					'&lt;/a&gt;' +
					'&lt;span class=&quot;nw_leiamais&quot;&gt;' +
					'&lt;a class=&quot;nw_link_noticia&quot; href=&quot;#linknoticiador#&quot; target=&quot;_blank&quot;&gt;' +
					' Leia Mais...' +
					'&lt;/a&gt;' +
					'&lt;/span&gt;' +
					'&lt;/span&gt;' +
					'&lt;/li&gt;'
				, "urlAltThumb" : 'https://www.siteondeserainstaladooplugin.com/images/imagempadrao.png'
				, "thumbWidth": 305
				, "thumbHeight": 250
			}
		);

	}

);

						</code>
					</pre>

					<p>
						Os parâmetros possíveis de se utilizar no noticiador são:
					</p>

					<table>
						<tr>
							<th style="font-weight: bold!important;">'parametro' : 'valor padrão'</th>
							<th style="font-weight: bold!important;">=></th>
							<th style="font-weight: bold!important;">descrição</th>
						</tr>
						<tr>
							<td>'idScript' : ''</td>
							<td>=></td>
							<td>id do script gerado no NoticiadorWeb</td>
						</tr>
						<tr>
							<td>'qtdNoticias' : ''</td>
							<td>=></td>
							<td>quantidade de notícias a ser exibida no script (0 equivale a todas notícias da área)</td>
						</tr>
						<tr>
							<td>'htmlNoticia' : ''</td>
							<td>=></td>
							<td>html do modelo a ser exibido com tags descritas abaixo</td>
						</tr>
						<tr>
							<td>'thumbWidth' : 100</td>
							<td>=></td>
							<td>largura do thumbnail</td>
						</tr>
						<tr>
							<td>'thumbHeight' : 100</td>
							<td>=></td>
							<td>altura do thumbnail</td>
						</tr>
						<tr>
							<td>'paginacao' : false</td>
							<td>=></td>
							<td>se o script possui paginação</td>
						</tr>
						<tr>
							<td>'pgQtdNoticiasPagina' : 1</td>
							<td>=></td>
							<td>quantidade de notícias a ser exibida por página</td>
						</tr>
						<tr>
							<td>'pgQtdPaginasExibidas' : 5</td>
							<td>=></td>
							<td>quantidade de páginas a ser exibida no script antes das setas</td>
						</tr>
						<tr>
							<td>'pgBorda' : false</td>
							<td>=></td>
							<td>número da página tem borda</td>
						</tr>
						<tr>
							<td>'pgBordaCor' : 'none'</td>
							<td>=></td>
							<td>cor da borda no número da página</td>
						</tr>
						<tr>
							<td>'pgBordaHoverCor' : 'none'</td>
							<td>=></td>
							<td>cor da borda no número da página no hover</td>
						</tr>
						<tr>
							<td>'pgTextoCor' : '#666'</td>
							<td>=></td>
							<td>cor do texto no número da página</td>
						</tr>
						<tr>
							<td>'pgFundoCor' : 'none'</td>
							<td>=></td>
							<td>cor do fundo do texto no número da página</td>
						</tr>
						<tr>
							<td>'pgTextoHoverCor' : '#000'</td>
							<td>=></td>
							<td>cor do texto no número da página no hover</td>
						</tr>
						<tr>
							<td>'pgFundoHoverCor' : 'none'</td>
							<td>=></td>
							<td>cor do fundo do texto no número da página no hover</td>
						</tr>
					</table>

					<p>
						O html pode conter as seguintes tags:
					</p>

					<table>
						<tr>
							<td>#data#</td>
							<td>=></td>
							<td>data de postagem da notícia</td>
						</tr>
						<tr>
							<td>#id#</td>
							<td>=></td>
							<td>id da notícia</td>
						</tr>
						<tr>
							<td>#link#</td>
							<td>=></td>
							<td>link informado no cadastro da notícia</td>
						</tr>
						<tr>
							<td>#linknoticiador#</td>
							<td>=></td>
							<td>link da notícia no noticiador</td>
						</tr>
						<tr>
							<td>#resumo#</td>
							<td>=></td>
							<td>resumo da notícia</td>
						</tr>
						<tr>
							<td>#thumbnail#</td>
							<td>=></td>
							<td>link do thumbnail</td>
						</tr>
						<tr>
							<td>#titulo#</td>
							<td>=></td>
							<td>título da notícia</td>
						</tr>
					</table>

					<p>
						Para mais parâmetros, consulte a biblioteca do plugin.
					</p>
</body>
</html>
<?=add_html_head("Administra��o de arquivos do sistema") ?>
<script type="text/javascript">
	function validarForm(obj) {
		if (!validarTexto(document.getElementById("txtNome"))) {
			alert('Digite um nome de arquivo');
			return false;
		}
		
		if (!validarTexto(document.getElementById("txtDescricao"))) {
			alert('Digite uma descri��o');
			return false;
		}

		return true;
	}
	
	function validarTexto(obj) {
		if (obj.value.length > 0)
			return true;
		
		return false;
	}
</script>

<form action="<?=$view->add_form_process_url() ?>" method="post" onSubmit="return validarForm(this);">
	<?=add_action_data("save", null, $view->getID()) ?>
	<fieldset>
		<legend>Dados do arquivo</legend>
		<label for="txtNome">Arquivos</label><br />
		<input type="text" name="txtNome" id="txtNome" value="<?=$view->getArquivo() ?>"  class="inputText"/>
		<br /><br />
		<label for="txtDescricao">Descri��o</label><br />
		<input type="text" name="txtDescricao" id="txtDescricao" value="<?=$view->getDescricao() ?>" class="inputText"/>
		
		<fieldset>
			<legend>A��es permitidas</legend>
			<?=$view->obterListaAcoes() ?>
		</fieldset>
		<input type="submit" value="Salvar" class="botao" />
	</fieldset>
	<fieldset>
		<legend>Arquivos cadastrados</legend>
		<?=$view->obterListaArquivosCadastrados() ?>
	</fieldset>
</form>
<?=add_html_tail() ?>

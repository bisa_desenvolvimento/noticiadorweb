<div class="rounded_content_top"></div>
<div class="rounded_content_middle">
	<div class="rounded_content">
    
        <h1 class="default_title">Quem somos</h1><br />
                
        <p><?=translate("NoticiadorWeb  é um produto da BISA (www.bisa.com.br), empresa que atua no mercado de T.I.C. -  Tecnologia, Informação e Comunicação desde o ano de 1991. A sede da Bisa é em Recife, mas ela está;  presente em 22 estados brasileiros. O acompanhamento de seus serviços é feito  através de técnicos residentes que supervisionam os sistemas de cada cliente.  São mais de 220 entidades que confiam seus sistemas a nossa empresa. Com o  intuito de levar solução para os seus clientes, a Bisa desenvolve trabalhos nas  seguintes áreas:")?></p>
        <ul>
          <li><?=translate("Consultoria")?></li>
          <li><?=translate("Desenvolvimento de Sistemas")?></li>
          <li><?=translate("Eleições e Votações Eletrônicas")?></li>
          <li><?=translate("Organização de Congressos e Eventos")?></li>
          <li><?=translate("Confecção de Websites")?></li>
        </ul>
        <p><?=translate("Para conhecer mais sobre os produtos e serviços da BISA Tecnologia de Informação, acesse o site:")?> <a https://ttp://www.bisa.com.br/" target="_blank">www.bisa.com.br</a></p>

   </div>
</div>	
<div class="rounded_content_bottom"></div>
<?php
  if(!isset($_SESSION["USUARIO"])) {
    include 'nova.php';
    return;
  }
?>

<? add_html_headPrincipal(null, $view->obterMenu()); ?>
<? if(isset($_GET['url'])){ ?>
	<div class="style7">
		<? include $page; ?>
	</div>
<? } else { ?>
		<div class="rounded_content_top">&nbsp;</div>
		<div class="rounded_content_middle">
			<div class="rounded_content">
				<h1 class="default_title"><?=translate("Seja bem-vindo ao site do NoticiadorWeb!")?></h1>
				<p><?=translate("Crie uma nova forma de comunicação com seus clientes, parceiros, eleitores, amigos, etc. Clique aqui para ver alguns exemplos de Newsletters que você pode criar utilizando o NoticiadorWeb.")?></p>
			</div>
			<br />
			<div id="index_funcionalidades">
				<h1 class="default_title"><?=translate("Funcionalidades")?></h1>
				<div class="home-carousel">
				<ul>
					<li>
						<h4><img src="<?=URL?>/images/icons/bullet_funcionalidades.jpg"/> Boletins eletrônicos</h4>
						<p>Pode ser criada uma variedade de lay-outs de newsletters para se comunicar diferente com cada um dos públicos-alvos das notícias que são enviadas</p>
					</li>
					<li>
						<h4><img src="<?=URL?>/images/icons/bullet_funcionalidades.jpg"/> Atualização de notícias no(s) site(s)</h4>
						<p>Todas as notícias que são inseridas no NoticiadorWeb podem ir automaticamente para uma área no website do publicador. Fazendo com que não seja necessário fazer duas atividades</p>
					</li>
					<li>
						<h4><img src="<?=URL?>/images/icons/bullet_funcionalidades.jpg"/> Colaboração</h4>
						<p>Épossível distribuir a criação de notícias com diversas pessoas. Basta que estes colaboradores insiram as suas "Sugestões de Pauta" que ficarão aguardando pelo crivo do EDITOR liberar ou não a notícia</p>
					</li>
					<li>
						<h4><img src="<?=URL?>/images/icons/bullet_funcionalidades.jpg"/> Banco de notícias/Gestão de notícias</h4>
						<p>Com a utilização do NoticiadorWeb é criado um histórico de notícias da empresa. E, estas informações podem ser recuperadas a qualquer momento, usando diversos critários de busca e seleção</p>
					</li>
					<li>
						<h4><img src="<?=URL?>/images/icons/bullet_funcionalidades.jpg"/> Segmentação do púlico-alvo</h4>
						<p>Épossível criar "áreas de Notícias e de Emails" onde determinadas notícias serão publicadas e enviadas apenas para um determinado público</p>
					</li>
					<li>
						<h4><img src="<?=URL?>/images/icons/bullet_funcionalidades.jpg"/> Integração com as Redes Sociais</h4>
						<p>Ao inserir uma notícia, é possível propagá-la de uma forma extremamente fácil nas 4 principais redes sociais: Twitter, Facebook, Linked In e Google+ (outras redes também podem ser acrescidas, solicite-nos!)</p>
					</li>
					<li>
						<h4><img src="<?=URL?>/images/icons/bullet_funcionalidades.jpg"/> Estatísticas</h4>
						<p>As notícias quando clickadas atualizam um contador de visualizações. Nestas notícias também é possível acompanhar a quantidade de re-twitters, de "curtir" do facebook, de compartilhar no Linked In e no Google+</p>
					</li>
					<li>
						<h4><img src="<?=URL?>/images/icons/bullet_funcionalidades.jpg"/> Integração com o website com regras de Opt-in/Anti-spam para cadastro na newsletter</h4>
						<p>O NoticiadorWeb disponibiliza alguns scripts para serem inseridos no website da empresa para que automaticamente um email seja inserido no banco de dados. Ao inserir este email, é enviada uma mensagem comunicando que não se trata de
							spam e pedindo autorização para o envio de boletins</p>
					</li>
				</ul>
        	</div>
				<div id="index_funcionalidades_preview">
					<img src="<?=DIR_LAYOUT?>index_funcionalidades_preview.jpg" />
				</div>
			</div>
			<div id="index_noticias">
				<img src="<?=DIR_LAYOUT?>index_noticias_title.jpg" width="243" height="23" />
				
                <iframe name="Noticiador Web" src="<?=URL?>index.php?action=show&secao=janela_cli&id=141&home=true" frameborder="0" width="235" height="250" scrolling="no"></iframe>
			</div>
			<div id="index_teste_gratuito">
				<?=translate("Épossível testar o NoticiadorWeb por 30 dias, sem custos.")?><br />
				<?=translate("Entre em contato coma a Bisa e solicite que seja criada uma senha de acesso.")?>
			</div>
		</div>
		<div class="rounded_gray_content_bottom">&nbsp;</div>
				<!--table width="217" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table width="202" border="0" align="center" cellpadding="0" cellspacing="3">
								<tr>
									<td width="196" class="style6">Seja bem-vindo ao site do NoticiadorWeb!</td>
								</tr>
								<tr>
									<td class="style7">Crie uma <strong>nova</strong> forma de comunicação com seus clientes,   parceiros, eleitores, amigos, etc. Clique aqui para ver alguns exemplos de   Newsletters que você pode criar utilizando o NoticiadorWeb.</td>
								</tr>
							</table>
						</td>
						<td>
							<img src="<?=DIR_IMAGES ?>bisa-2_r4_c3.jpg" width="341" height="220" />
						</td>
					</tr>
				</table-->

	<? }?>

	<script type="text/javascript">
        $(document).ready(function() {
            $('.home-carousel').jCarouselLite({
                auto: 6000,
                speed: 2000,
                visible: 1,
            });
        });
	</script>
	
<? add_html_tailPrincipal(); ?>
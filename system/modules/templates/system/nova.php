<!DOCTYPE html>
<html class="no-js" lang="pt-BR" itemscope itemtype="https://schema.org/WebPage">

<!-- head -->

<!-- Mirrored from www.noticiadorweb.bisaweb.com.br/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Jul 2019 13:05:20 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>

  <!-- meta -->
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <meta name="description" content="moneta" />

  <link rel="shortcut icon"
    href="tema/kdsolutions.com.br/bisaweb/juridico/wp-content/uploads/2018/10/Logo_MonetaWeb-1-1-e1539984072351.png" />
  <link rel="apple-touch-icon"
    href="tema/kdsolutions.com.br/bisaweb/juridico/wp-content/uploads/2018/10/Logo_MonetaWeb-1-1-e1539984072351.png" />

  <!-- wp_head() -->
  <title>Noticiador Web &#8211; Desenvolvido por Bisaweb</title>
  <!-- script | dynamic -->
  <script id="mfn-dnmc-config-js">
    //<![CDATA[
    window.mfn_ajax = "tema/wp-admin/admin-ajax.html";
    window.mfn = { mobile_init: 1240, nicescroll: 40, parallax: "translate3d", responsive: 1, retina_js: 0 };
    window.mfn_lightbox = { disable: false, disableMobile: false, title: false, };
    window.mfn_sliders = { blog: 0, clients: 0, offer: 0, portfolio: 0, shop: 0, slider: 0, testimonials: 0 };
//]]>
  </script>
  <link rel='dns-prefetch' href='https://fonts.googleapis.com/' />
  <link rel='dns-prefetch' href='https://s.w.org/' />
  <link rel="alternate" type="application/rss+xml" title="Feed para Noticiador Web &raquo;" href="feed/index.html" />
  <link rel="alternate" type="application/rss+xml" title="Feed de comentários para Noticiador Web &raquo;"
    href="comments/feed/index.html" />
  <script type="text/javascript">
    window._wpemojiSettings = { "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/", "ext": ".png", "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/", "svgExt": ".svg", "source": { "concatemoji": "https:\/\/noticiadorweb.bisaweb.com.br\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.0.4" } };
    !function (a, b, c) { function d(a, b) { var c = String.fromCharCode; l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, a), 0, 0); var d = k.toDataURL(); l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, b), 0, 0); var e = k.toDataURL(); return d === e } function e(a) { var b; if (!l || !l.fillText) return !1; switch (l.textBaseline = "top", l.font = "600 32px Arial", a) { case "flag": return !(b = d([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819])) && (b = d([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]), !b); case "emoji": return b = d([55358, 56760, 9792, 65039], [55358, 56760, 8203, 9792, 65039]), !b }return !1 } function f(a) { var c = b.createElement("script"); c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c) } var g, h, i, j, k = b.createElement("canvas"), l = k.getContext && k.getContext("2d"); for (j = Array("flag", "emoji"), c.supports = { everything: !0, everythingExceptFlag: !0 }, i = 0; i < j.length; i++)c.supports[j[i]] = e(j[i]), c.supports.everything = c.supports.everything && c.supports[j[i]], "flag" !== j[i] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[j[i]]); c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function () { c.DOMReady = !0 }, c.supports.everything || (h = function () { c.readyCallback() }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", h, !1), a.addEventListener("load", h, !1)) : (a.attachEvent("onload", h), b.attachEvent("onreadystatechange", function () { "complete" === b.readyState && c.readyCallback() })), g = c.source || {}, g.concatemoji ? f(g.concatemoji) : g.wpemoji && g.twemoji && (f(g.twemoji), f(g.wpemoji))) }(window, document, window._wpemojiSettings);
  </script>
  <style type="text/css">
    img.wp-smiley,
    img.emoji {
      display: inline !important;
      border: none !important;
      box-shadow: none !important;
      height: 1em !important;
      width: 1em !important;
      margin: 0 .07em !important;
      vertical-align: -0.1em !important;
      background: none !important;
      padding: 0 !important;
    }

    #login_form {
      width: 600px;
      display: flex;
      justify-content: space-between;
    }

    .titulo-not {
      font-size: 30px;
      line-height: 34px;
      font-weight: 800;
      letter-spacing: 0px;
    }
  </style>
  <link rel='stylesheet' id='wp-block-library-css' href='tema/wp-includes/css/dist/block-library/style.min5560.css?ver=5.0.4'
    type='text/css' media='all' />
  <link rel='stylesheet' id='image-hover-effects-css-css'
    href='tema/wp-content/plugins/mega-addons-for-visual-composer/css/ihover5560.css?ver=5.0.4' type='text/css'
    media='all' />
  <link rel='stylesheet' id='style-css-css'
    href='tema/wp-content/plugins/mega-addons-for-visual-composer/css/style5560.css?ver=5.0.4' type='text/css' media='all' />
  <link rel='stylesheet' id='font-awesome-latest-css'
    href='tema/wp-content/plugins/mega-addons-for-visual-composer/css/font-awesome/css/font-awesome5560.css?ver=5.0.4'
    type='text/css' media='all' />
  <link rel='stylesheet' id='jquery-ui-css'
    href='tema/wp-content/plugins/wd-mailchimp/css/jquery-ui-1.10.3.custom5560.css?ver=5.0.4' type='text/css' media='all' />
  <link rel='stylesheet' id='jquery-ui-spinner-css'
    href='tema/wp-content/plugins/wd-mailchimp/css/jquery-ui-spinner5560.css?ver=5.0.4' type='text/css' media='all' />
  <link rel='stylesheet' id='mwd-mailchimp-frontend-css'
    href='tema/wp-content/plugins/wd-mailchimp/css/frontend/mwd-mailchimp-frontendb144.css?ver=1.0.19' type='text/css'
    media='all' />
  <link rel='stylesheet' id='mwd-font-awesome-css'
    href='tema/wp-content/plugins/wd-mailchimp/css/frontend/font-awesome/font-awesomeb144.css?ver=1.0.19' type='text/css'
    media='all' />
  <link rel='stylesheet' id='mwd-animate-css'
    href='tema/wp-content/plugins/wd-mailchimp/css/frontend/mwd-animateb144.css?ver=1.0.19' type='text/css' media='all' />
  <link rel='stylesheet' id='mwd_googlefonts-css'
    href='https://fonts.googleapis.com/css?family=Open+Sans|Oswald|Droid+Sans|Lato|Open+Sans+Condensed|PT+Sans|Ubuntu|PT+Sans+Narrow|Yanone+Kaffeesatz|Roboto+Condensed|Source+Sans+Pro|Nunito|Francois+One|Roboto|Raleway|Arimo|Cuprum|Play|Dosis|Abel|Droid+Serif|Arvo|Lora|Rokkitt|PT+Serif|Bitter|Merriweather|Vollkorn|Cantata+One|Kreon|Josefin+Slab|Playfair+Display|Bree+Serif|Crimson+Text|Old+Standard+TT|Sanchez|Crete+Round|Cardo|Noticia+Text|Judson|Lobster|Unkempt|Changa+One|Special+Elite|Chewy|Comfortaa|Boogaloo|Fredoka+One|Luckiest+Guy|Cherry+Cream+Soda|Lobster+Two|Righteous|Squada+One|Black+Ops+One|Happy+Monkey|Passion+One|Nova+Square|Metamorphous|Poiret+One|Bevan|Shadows+Into+Light|The+Girl+Next+Door|Coming+Soon|Dancing+Script|Pacifico|Crafty+Girls|Calligraffitti|Rock+Salt|Amatic+SC|Leckerli+One|Tangerine|Reenie+Beanie|Satisfy|Gloria+Hallelujah|Permanent+Marker|Covered+By+Your+Grace|Walter+Turncoat|Patrick+Hand|Schoolbell|Indie+Flower&amp;subset=greek,latin,greek-ext,vietnamese,cyrillic-ext,latin-ext,cyrillic'
    type='text/css' media='all' />
  <link rel='stylesheet' id='style-css' href='tema/wp-content/themes/betheme-n/styleae03.css?ver=20.8.1' type='text/css'
    media='all' />
  <link rel='stylesheet' id='mfn-base-css' href='tema/wp-content/themes/betheme-n/css/baseae03.css?ver=20.8.1'
    type='text/css' media='all' />
  <link rel='stylesheet' id='mfn-layout-css' href='tema/wp-content/themes/betheme-n/css/layoutae03.css?ver=20.8.1'
    type='text/css' media='all' />
  <link rel='stylesheet' id='mfn-shortcodes-css' href='tema/wp-content/themes/betheme-n/css/shortcodesae03.css?ver=20.8.1'
    type='text/css' media='all' />
  <link rel='stylesheet' id='mfn-animations-css'
    href='tema/wp-content/themes/betheme-n/assets/animations/animations.minae03.css?ver=20.8.1' type='text/css'
    media='all' />
  <link rel='stylesheet' id='mfn-jquery-ui-css'
    href='tema/wp-content/themes/betheme-n/assets/ui/jquery.ui.allae03.css?ver=20.8.1' type='text/css' media='all' />
  <link rel='stylesheet' id='mfn-jplayer-css'
    href='tema/wp-content/themes/betheme-n/assets/jplayer/css/jplayer.blue.mondayae03.css?ver=20.8.1' type='text/css'
    media='all' />
  <link rel='stylesheet' id='mfn-responsive-css' href='tema/wp-content/themes/betheme-n/css/responsiveae03.css?ver=20.8.1'
    type='text/css' media='all' />
  <link rel='stylesheet' id='Open+Sans-css'
    href='https://fonts.googleapis.com/css?family=Open+Sans%3A1%2C300%2C400%2C400italic%2C500%2C700%2C700italic%2C800&amp;ver=5.0.4'
    type='text/css' media='all' />
  <link rel='stylesheet' id='Roboto-css'
    href='https://fonts.googleapis.com/css?family=Roboto%3A1%2C300%2C400%2C400italic%2C500%2C700%2C700italic%2C800&amp;ver=5.0.4'
    type='text/css' media='all' />
  <link rel='stylesheet' id='js_composer_front-css'
    href='tema/wp-content/plugins/js_composer/assets/css/js_composer.min7e15.css?ver=5.5.4' type='text/css' media='all' />
  <script type='text/javascript' src='tema/wp-includes/js/jquery/jqueryb8ff.js?ver=1.12.4'></script>
  <script type='text/javascript' src='tema/wp-includes/js/jquery/jquery-migrate.min330a.js?ver=1.4.1'></script>
  <script type='text/javascript' src='tema/wp-includes/js/jquery/ui/core.mine899.js?ver=1.11.4'></script>
  <script type='text/javascript'
    src='tema/wp-content/plugins/mega-addons-for-visual-composer/js/script5560.js?ver=5.0.4'></script>
  <script type='text/javascript'
    src='tema/wp-content/plugins/wd-mailchimp/js/file-upload-frontend5560.js?ver=5.0.4'></script>
  <script type='text/javascript'>
    /* <![CDATA[ */
    var mwd_objectL10n = { "plugin_url": "http:\/\/www.noticiadorweb.bisaweb.com.br\/wp-content\/plugins\/wd-mailchimp" };
/* ]]> */
  </script>
  <script type='text/javascript' src='wp-content/plugins/wd-mailchimp/js/mwd_main_frontendb144.js?ver=1.0.19'></script>
  <link rel='https://api.w.org/' href='wp-json/index.html' />
  <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
  <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="tema/wp-includes/wlwmanifest.xml" />
  <meta name="generator" content="WordPress 5.0.4" />
  <link rel="canonical" href="index.html" />
  <link rel='shortlink' href='index.html' />
  <link rel="alternate" type="application/json+oembed"
    href="tema/wp-json/oembed/1.0/embedb38a.json?url=http%3A%2F%2Fwww.noticiadorweb.bisaweb.com.br%2F" />
  <link rel="alternate" type="text/xml+oembed"
    href="tema/wp-json/oembed/1.0/embedc415?url=http%3A%2F%2Fwww.noticiadorweb.bisaweb.com.br%2F&amp;format=xml" />
  <!-- style | background -->
  <style id="mfn-dnmc-bg-css">
    #Subheader {
      background-image: url(tema/kdsolutions.com.br/bisaweb/moneta/wp-content/uploads/2018/08/header-moneta.png);
      background-repeat: no-repeat;
      background-position: center top
    }
  </style>

 <link rel='stylesheet' href='tema/wp-content/themes/betheme-n/css/custom.css' type='text/css' media='all' />  

  <!--[if lt IE 9]>
<script id="mfn-html5" src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
  <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress." />
  <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://www.noticiadorweb.bisaweb.com.br/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]-->
  <style type="text/css" data-type="vc_shortcodes-custom-css">
    .vc_custom_1539998204540 {
      margin-top: -23px !important;
      border-top-width: 4px !important;
      background-image: url(tema/kdsolutions.com.br/bisaweb/noticiador/wp-content/uploads/2018/10/background0641.jpg?id=88) !important;
      background-position: center !important;
      background-repeat: no-repeat !important;
      background-size: cover !important;
      border-top-color: #ffcc00 !important;
      border-top-style: solid !important;
    }

    .vc_custom_1539977630731 {
      background-color: #f7f7f7 !important;
      background-position: center !important;
      background-repeat: no-repeat !important;
      background-size: cover !important;
    }

    .vc_custom_1544548071354 {
      margin-top: -23px !important;
      border-top-width: 4px !important;
      border-bottom-width: 4px !important;
      background-image: url(https://www.noticiadorweb.bisaweb.com.br/wp-content/uploads/2018/12/Rectangle-6-1.png?id=138) !important;
      background-position: center !important;
      background-repeat: no-repeat !important;
      background-size: cover !important;
      border-top-color: #ffcc00 !important;
      border-top-style: solid !important;
      border-bottom-color: #ffcc00 !important;
      border-bottom-style: solid !important;
    }

    .vc_custom_1539987329345 {
      margin-top: -23px !important;
      border-top-width: 4px !important;
      border-bottom-width: 4px !important;
      background-color: #ffffff !important;
      background-position: center !important;
      background-repeat: no-repeat !important;
      background-size: cover !important;
      border-top-color: #ffcc00 !important;
      border-top-style: solid !important;
      border-bottom-color: #ffcc00 !important;
      border-bottom-style: solid !important;
    }

    .vc_custom_1544548048336 {
      background-position: center !important;
      background-repeat: no-repeat !important;
      background-size: cover !important;
    }

    .vc_custom_1539977748140 {
      background-color: #f7f7f7 !important;
      background-position: center !important;
      background-repeat: no-repeat !important;
      background-size: cover !important;
    }

    .vc_custom_1539978709825 {
      background-color: #ffffff !important;
      background-position: center !important;
      background-repeat: no-repeat !important;
      background-size: cover !important;
    }

    .vc_custom_1544548025733 {
      background-image: url(https://www.noticiadorweb.bisaweb.com.br/wp-content/uploads/2018/12/helloquence-61189-unsplash-1.png?id=140) !important;
      background-position: center !important;
      background-repeat: no-repeat !important;
      background-size: cover !important;
    }
  </style><noscript>
    <style type="text/css">
      .wpb_animate_when_almost_visible {
        opacity: 1;
      }
    </style>
  </noscript>
</head>

<!-- body -->

<body
  class="home page-template-default page page-id-7  color-custom style-default button-default layout-full-width header-classic minimalist-header-no sticky-white ab-hide subheader-both-center menuo-arrows menuo-last menuo-right footer-copy-center mobile-tb-center mobile-mini-mr-ll be-reg-2081 wpb-js-composer js-comp-ver-5.5.4 vc_responsive">
  <div id="Wrapper">
    <div id="Header_wrapper">
      <header id="Header">

        <div class="header_placeholder"></div>

        <div id="Top_bar" class="loading">

          <div class="container">
            <div class="column one">

              <div class="top_bar_left clearfix">

                <!-- Logo -->
                <div class="logo"><a id="logo" href="index.php" title="Noticiador Web" data-height="60"
                    data-padding="15"><img class="logo-main scale-with-grid"
                      src="tema/wp-content/uploads/2018/12/Logo_MonetaWeb.png"
                      data-retina="https://www.noticiadorweb.bisaweb.com.br/wp-content/uploads/2018/12/Logo_MonetaWeb.png"
                      data-height="63" alt="Logo_MonetaWeb" /><img class="logo-sticky scale-with-grid"
                      src="tema/wp-content/uploads/2018/12/Logo_MonetaWeb.png"
                      data-retina="https://www.noticiadorweb.bisaweb.com.br/wp-content/uploads/2018/12/Logo_MonetaWeb.png"
                      data-height="63" alt="Logo_MonetaWeb" /><img class="logo-mobile scale-with-grid"
                      src="tema/wp-content/uploads/2018/12/Logo_MonetaWeb.png"
                      data-retina="https://www.noticiadorweb.bisaweb.com.br/wp-content/uploads/2018/12/Logo_MonetaWeb.png"
                      data-height="63" alt="Logo_MonetaWeb" /><img class="logo-mobile-sticky scale-with-grid"
                      src="tema/wp-content/uploads/2018/12/Logo_MonetaWeb.png"
                      data-retina="https://www.noticiadorweb.bisaweb.com.br/wp-content/uploads/2018/12/Logo_MonetaWeb.png"
                      data-height="63" alt="Logo_MonetaWeb" /></a></div>
                      <div class="menu_wrapper">
                  <nav id="menu">
                    <ul id="menu-menu" class="menu menu-main">
                      <!-- <li id="menu-item-135" class="menu-item menu-item-type-post_type menu-item-object-page"><a
                          href="blog-noticiador-web/index.html"><span>Blog da Newsletter Institucional</span></a></li> -->
                      <li id="menu-item-114" class="menu-item menu-item-type-custom menu-item-object-custom">
                      
                          <form 
                            id="login_form" 
                            name="login_form" 
                            method="post" 
                            action="index.php">
                            <? add_action_data("processUser"); ?>		
                  
                                <div class="col-sm-6 cfsFieldCol" data-name="usuario" data-type="text">
                                    <div class="cfsFieldShell cfsField_text">
                                      <input style="border-color: #999" type="text" name="txtLogin" placeholder="" id="login_user" />
                                    </div>
                                </div>

                                <div class="col-sm-6 cfsFieldCol" data-name="usuario" data-type="text">
                                    <div class="cfsFieldShell cfsField_text">
                                      <input style="border-color: #999" name="txtSenha" type="password" placeholder=""  id="login_pass" />
                                    </div>
                                </div>
                                
                                <div class="col-sm-6 cfsFieldCol" data-name="usuario" data-type="text">
                                    <div class="cfsFieldShell cfsField_text">
                                      <input type="submit" value="Entrar" class="botaoLogin"/>
                                    </div>
                                </div>
                               
    
                   
                </form>
                      </li>
                      <li id="menu-item-103"
                        class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children">
                        <a href="#"><span>Atalhos</span></a>
                        <ul class="sub-menu">
                          <li id="menu-item-104"
                            class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home">
                            <a href="index.php#caracteristicasdosistema"><span>Características do Sistema</span></a>
                          </li>
                          <li id="menu-item-105"
                            class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home">
                            <a href="index.php#depoimentos"><span>Depoimentos de clientes</span></a></li>
                          <li id="menu-item-106"
                            class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home">
                            <a href="index.php#numeros"><span>Dados Estatísticos</span></a></li>
                          <li id="menu-item-108"
                            class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home">
                            <a href="index.php#contato"><span>Solicitar Demonstração</span></a></li>
                          <li id="menu-item-107"
                            class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home">
                            <a href="index.php#blog"><span>Blog da Newsletter Institucional</span></a></li>
                      
                          <li id="menu-item-110"
                            class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home">
                            <a href="index.php#newsletter"><span>Newsletter</span></a></li>
                          <li id="menu-item-111"
                            class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home">
                            <a href="index.php#contato"><span>Contato</span></a></li>
                        </ul>
                      </li>
                    </ul>
                  </nav><a class="responsive-menu-toggle " href="#"><i class="icon-menu-fine"></i></a>
                </div>

                <div class="secondary_menu_wrapper">
                  <!-- #secondary-menu -->
                  <nav id="secondary-menu" class="menu-menu-container">
                    <ul id="menu-menu-1" class="secondary-menu">
                      <!-- <li id="menu-item-135"
                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-135"><a
                          href="blog-noticiador-web/index.html">Blog da Newsletter Institucional</a></li> -->
                      <li id="menu-item-114"
                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-114"><a
                          href="#">Login</a></li>
                      <li id="menu-item-103"
                        class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children menu-item-103">
                        <a href="#">Atalhos</a>
                        <ul class="sub-menu">
                          <li id="menu-item-104"
                            class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-104">
                            <a href="index.php#caracteristicasdosistema">Características do Sistema</a></li>
                          <li id="menu-item-105"
                            class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-105">
                            <a href="index.php#depoimentos">Depoimentos de clientes</a></li>
                          <li id="menu-item-106"
                            class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-106">
                            <a href="index.php#numeros">Dados Estatísticos</a></li>
                          <li id="menu-item-108"
                            class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-108">
                            <a href="index.php#contato">Solicitar Demonstração</a></li>
                          <li id="menu-item-107"
                            class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-107">
                            <a href="index.php#blog">Blog da Newsletter Institucional</a></li>
                          <li id="menu-item-109"
                            class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-109">
                            <a href="index.php#noticiasbisa">Notícias Bisa Tecnologia</a></li>
                          <li id="menu-item-110"
                            class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-110">
                            <a href="index.php#newsletter">Newsletter</a></li>
                          <li id="menu-item-111"
                            class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-111">
                            <a href="index.php#contato">Contato</a></li>
                        </ul>
                      </li>
                    </ul>
                  </nav>
                </div>

               

                <div class="banner_wrapper">
                </div>

                <div class="search_wrapper">
                  <!-- #searchform -->


                  <form method="get" id="searchform" action="https://www.noticiadorweb.bisaweb.com.br/">


                    <i class="icon_search icon-search-fine"></i>
                    <a href="#" class="icon_close"><i class="icon-cancel-fine"></i></a>

                    <input type="text" class="field" name="s" placeholder="Enter your search" />
                    <input type="submit" class="submit" value="" style="display:none;" />

                  </form>
                </div>

              </div>


            </div>
          </div>
        </div>
      </header>


    </div>


    <!-- mfn_hook_content_before -->
    <!-- mfn_hook_content_before -->
    <!-- #Content -->
    <div id="Content">
      <div class="content_wrapper clearfix">

        <!-- .sections_group -->
        <div class="sections_group">

          <div class="entry-content" itemprop="mainContentOfPage">

            <div class="section the_content has_content">
              <div class="section_wrapper">
                <div class="the_content_wrapper">
                  <div data-vc-full-width="true" data-vc-full-width-init="false"
                    class="vc_row wpb_row vc_row-fluid vc_custom_1539998204540 vc_row-has-fill">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                      <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                          <div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span>
                          </div>
                          <div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span>
                          </div>

                          <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                              <h2 style="text-align: center;"><span style="color: #ffffff;">Distribuidor de
                                  Notícias</span><br />
                                <img class="alignnone size-full wp-image-80"
                                  src="wp-content/uploads/2018/10/Rectangle-4.5.png" alt="" width="63" height="5"
                                  srcset="https://www.noticiadorweb.bisaweb.com.br/wp-content/uploads/2018/10/Rectangle-4.5.png 63w, https://www.noticiadorweb.bisaweb.com.br/wp-content/uploads/2018/10/Rectangle-4.5-50x4.png 50w"
                                  sizes="(max-width: 63px) 100vw, 63px" /></h2>
                              <div style="text-align: center;">
                                <div><span style="color: #ffffff;">Sistema de comunicação integrada e distribuída que
                                    divulga conteúdo em vários sites parceiros ao mesmo tempo, potencializando muito o
                                    alcance de suas informações.<br />
                                    O sistema é alimentado através de uma plataforma online onde é possível formatar o
                                    texto, inserir imagens e outras mídias e escolher quais os sites onde as notícias
                                    serão publicadas.<br />
                                  </span></div>
                                <div></div>
                                <div></div>
                              </div>

                            </div>
                          </div>
                          <style type="text/css">
                            .vc_btn3-style-gradient-custom.vc_btn-gradient-btn-5d2c7a0da1fc9:hover {
                              color: #ffffff;
                              background-color: #04264c;
                              border: none;
                              background-position: 100% 0;
                            }
                          </style>
                          <style type="text/css">
                            .vc_btn3-style-gradient-custom.vc_btn-gradient-btn-5d2c7a0da1fc9 {
                              color: #ffffff;
                              border: none;
                              background-color: #04264c;
                              background-image: -webkit-linear-gradient(left, #04264c 0%, #04264c 50%, #04264c 100%);
                              background-image: linear-gradient(to right, #04264c 0%, #04264c 50%, #04264c 100%);
                              -webkit-transition: all .2s ease-in-out;
                              transition: all .2s ease-in-out;
                              background-size: 200% 100%;
                            }
                          </style>
                          <div class="vc_btn3-container vc_btn3-center">
                            <a data-vc-gradient-1="#04264c" data-vc-gradient-2="#04264c"
                              class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-gradient-custom vc_btn-gradient-btn-5d2c7a0da1fc9"
                              href="index.html#contato" title="">Solicite uma demonstração</a></div>
                          <div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span>
                          </div>
                          <div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="vc_row-full-width vc_clearfix"></div>
                  <div id="caracteristicasdosistema" data-vc-full-width="true" data-vc-full-width-init="false"
                    class="vc_row wpb_row vc_row-fluid vc_custom_1539977630731 vc_row-has-fill">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                      <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                          <div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span>
                          </div>

                          <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                              <h2 style="text-align: center;">Características do sistema</h2>
                              <h2 style="text-align: center;"><img class="alignnone size-full wp-image-80"
                                  src="wp-content/uploads/2018/10/Rectangle-4.5.png" alt="" width="63" height="5"
                                  srcset="https://www.noticiadorweb.bisaweb.com.br/wp-content/uploads/2018/10/Rectangle-4.5.png 63w, https://www.noticiadorweb.bisaweb.com.br/wp-content/uploads/2018/10/Rectangle-4.5-50x4.png 50w"
                                  sizes="(max-width: 63px) 100vw, 63px" /></h2>

                            </div>
                          </div>
                          <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="mega_info_box">
                                    <div class="mega-info-header">
                                      <i class="fa fa-window-restore" aria-hidden="true"
                                        style="border: 2px solid #ffcc00; border-radius: 50%; background: #ffffff; width: 80px; height: 80px; line-height: 76px; font-size: 30px; color: #ffcc00;"></i>
                                    </div>
                                    <div class="mega-info-footer">
                                      <h3 class="mega-info-title" style="font-size: 24px; line-height: ;">
                                        Boletins Eletrônicos </h3>
                                      <p class="mega-info-desc" style="font-size: ;">
                                        Pode ser criada uma variedade de layouts de Newsletters para se comunicar
                                        diferente com cada um dos públicos-alvos das notícias que são enviadas. </p>
                                      <a class="mega-info-btn" href="#" style="background: ; display: none;">
                                      </a>
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="mega_info_box">
                                    <div class="mega-info-header">
                                      <i class="fa fa-bullhorn" aria-hidden="true"
                                        style="border: 2px solid #ffcc00; border-radius: 50%; background: #ffffff; width: 80px; height: 80px; line-height: 76px; font-size: 30px; color: #ffcc00;"></i>
                                    </div>
                                    <div class="mega-info-footer">
                                      <h3 class="mega-info-title" style="font-size: 24px; line-height: ;">
                                        Atualizações de Notícias para o seu Website </h3>
                                      <p class="mega-info-desc" style="font-size: ;">
                                        Todas as notícias que são inseridas no NoticiadorWeb podem ir automaticamente
                                        para uma área no website do publicador. Fazendo com que não seja necessário
                                        fazer duas atividades - como um gestor de conteúdo. </p>
                                      <a class="mega-info-btn" href="#" style="background: ; display: none;">
                                      </a>
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="mega_info_box">
                                    <div class="mega-info-header">
                                      <i class="fa fa-bell-o" aria-hidden="true"
                                        style="border: 2px solid #ffcc00; border-radius: 50%; background: #ffffff; width: 80px; height: 80px; line-height: 76px; font-size: 30px; color: #ffcc00;"></i>
                                    </div>
                                    <div class="mega-info-footer">
                                      <h3 class="mega-info-title" style="font-size: 24px; line-height: ;">
                                        Distribuição de notícias para outros blogs/websites/portais </h3>
                                      <p class="mega-info-desc" style="font-size: ;">
                                        Você posta uma única vez a matéria e automaticamente são atualizados os diversos
                                        websites que se tem parceria - com esses recursos você pode ampliar
                                        consideravelmente o alcance do seu conteúdo gerando muito mais visibilidade à
                                        sua marca. </p>
                                      <a class="mega-info-btn" href="#" style="background: ; display: none;">
                                      </a>
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="mega_info_box">
                                    <div class="mega-info-header">
                                      <i class="fa fa-rss" aria-hidden="true"
                                        style="border: 2px solid #ffcc00; border-radius: 50%; background: #ffffff; width: 80px; height: 80px; line-height: 76px; font-size: 30px; color: #ffcc00;"></i>
                                    </div>
                                    <div class="mega-info-footer">
                                      <h3 class="mega-info-title" style="font-size: 24px; line-height: ;">
                                        Integração com as Redes Sociais </h3>
                                      <p class="mega-info-desc" style="font-size: ;">
                                        Ao inserir uma notícia, é possível propagá-la de uma forma extremamente fácil
                                        nas 4 principais redes sociais: Twitter, Facebook, LinkedIn e Google+ (outras
                                        redes também podem ser acrescidas, solicite-nos!). </p>
                                      <a class="mega-info-btn" href="#" style="background: ; display: none;">
                                      </a>
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="mega_info_box">
                                    <div class="mega-info-header">
                                      <i class="fa fa-pencil" aria-hidden="true"
                                        style="border: 2px solid #ffcc00; border-radius: 50%; background: #ffffff; width: 80px; height: 80px; line-height: 76px; font-size: 30px; color: #ffcc00;"></i>
                                    </div>
                                    <div class="mega-info-footer">
                                      <h3 class="mega-info-title" style="font-size: 24px; line-height: ;">
                                        Colaboração </h3>
                                      <p class="mega-info-desc" style="font-size: ;">
                                        É possível distribuir a criação de notícias com diversas pessoas. Basta que
                                        estes colaboradores insiram as suas "Sugestões de Pauta" que ficarão aguardando
                                        pelo crivo do EDITOR liberar ou não a notícia. </p>
                                      <a class="mega-info-btn" href="#" style="background: ; display: none;">
                                      </a>
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="mega_info_box">
                                    <div class="mega-info-header">
                                      <i class="fa fa-id-badge" aria-hidden="true"
                                        style="border: 2px solid #ffcc00; border-radius: 50%; background: #ffffff; width: 80px; height: 80px; line-height: 76px; font-size: 30px; color: #ffcc00;"></i>
                                    </div>
                                    <div class="mega-info-footer">
                                      <h3 class="mega-info-title" style="font-size: 24px; line-height: ;">
                                        Segmentação do público-alvo </h3>
                                      <p class="mega-info-desc" style="font-size: ;">
                                        É possível criar "Áreas de Notícias e de E-mail" onde determinadas notícias
                                        serão publicadas e enviadas apenas para um determinado público. </p>
                                      <a class="mega-info-btn" href="#" style="background: ; display: none;">
                                      </a>
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="mega_info_box">
                                    <div class="mega-info-header">
                                      <i class="fa fa-bar-chart" aria-hidden="true"
                                        style="border: 2px solid #ffcc00; border-radius: 50%; background: #ffffff; width: 80px; height: 80px; line-height: 76px; font-size: 30px; color: #ffcc00;"></i>
                                    </div>
                                    <div class="mega-info-footer">
                                      <h3 class="mega-info-title" style="font-size: 24px; line-height: ;">
                                        Estatísticas </h3>
                                      <p class="mega-info-desc" style="font-size: ;">
                                        As notícias quando clicadas atualizam um contador de visualizações. Nestas
                                        notícias também é possível acompanhar a quantidade de retweets, de "curtir" do
                                        facebook, de compartilhar no LinkedIn e no Google+. </p>
                                      <a class="mega-info-btn" href="#" style="background: ; display: none;">
                                      </a>
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="mega_info_box">
                                    <div class="mega-info-header">
                                      <i class="fa fa-newspaper-o" aria-hidden="true"
                                        style="border: 2px solid #ffcc00; border-radius: 50%; background: #ffffff; width: 80px; height: 80px; line-height: 76px; font-size: 30px; color: #ffcc00;"></i>
                                    </div>
                                    <div class="mega-info-footer">
                                      <h3 class="mega-info-title" style="font-size: 24px; line-height: ;">
                                        Banco de notícias/Gestão de notícias </h3>
                                      <p class="mega-info-desc" style="font-size: ;">
                                        Com a utilização do NoticiadorWeb é criado um histórico de notícias da empresa.
                                        E, estas informações podem ser recuperadas a qualquer momento, usando diversos
                                        critérios de busca e seleção. </p>
                                      <a class="mega-info-btn" href="#" style="background: ; display: none;">
                                      </a>
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="mega_info_box">
                                    <div class="mega-info-header">
                                      <i class="fa fa-users" aria-hidden="true"
                                        style="border: 2px solid #ffcc00; border-radius: 50%; background: #ffffff; width: 80px; height: 80px; line-height: 76px; font-size: 30px; color: #ffcc00;"></i>
                                    </div>
                                    <div class="mega-info-footer">
                                      <h3 class="mega-info-title" style="font-size: 24px; line-height: ;">
                                        Treinamentos </h3>
                                      <p class="mega-info-desc" style="font-size: ;">
                                        Focados em compartilhar conhecimentos, realizamos 3 treinamentos anuais para
                                        apresentação das novas funcionalidades advindas da evolução do sistema. Além
                                        disto, oferecemos vídeos-aula, treinamentos remotos e palestras sobre temas
                                        gerais ligados a gestão financeira. Pois conhecimento só é conhecimento se for
                                        compartilhado. </p>
                                      <a class="mega-info-btn" href="#" style="background: ; display: none;">
                                      </a>
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="mega_info_box">
                                    <div class="mega-info-header">
                                      <i class="fa fa-line-chart" aria-hidden="true"
                                        style="border: 2px solid #ffcc00; border-radius: 50%; background: #ffffff; width: 80px; height: 80px; line-height: 76px; font-size: 30px; color: #ffcc00;"></i>
                                    </div>
                                    <div class="mega-info-footer">
                                      <h3 class="mega-info-title" style="font-size: 24px; line-height: ;">
                                        Evolução </h3>
                                      <p class="mega-info-desc" style="font-size: ;">
                                        A BisaWeb possui uma equipe de desenvolvimento focada em desenvolver novas
                                        funcionalidades para o sistema, de maneira que ele acompanhe a evolução do
                                        mundo. Além disto, recebemos sugestões de melhorias de nossos clientes, e as
                                        desenvolvemos no sistema, pois as necessidades de nossos clientes são nossas
                                        necessidades. Evoluir é preciso. </p>
                                      <a class="mega-info-btn" href="#" style="background: ; display: none;">
                                      </a>
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="mega_info_box">
                                    <div class="mega-info-header">
                                      <i class="fa fa-laptop" aria-hidden="true"
                                        style="border: 2px solid #ffcc00; border-radius: 50%; background: #ffffff; width: 80px; height: 80px; line-height: 76px; font-size: 30px; color: #ffcc00;"></i>
                                    </div>
                                    <div class="mega-info-footer">
                                      <h3 class="mega-info-title" style="font-size: 24px; line-height: ;">
                                        Sistema leve </h3>
                                      <p class="mega-info-desc" style="font-size: ;">
                                        Com um banco de dados criado de maneira otimizada, aplicando-se todas as
                                        metodologias técnicas para tal, o acesso aos dados ocorre de maneira rápida.
                                        Para o usuário, o tempo de resposta é satisfatório, dependendo unicamente do
                                        link de acesso a internet. </p>
                                      <a class="mega-info-btn" href="#" style="background: ; display: none;">
                                      </a>
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="mega_info_box">
                                    <div class="mega-info-header">
                                      <i class="fa fa-cloud" aria-hidden="true"
                                        style="border: 2px solid #ffcc00; border-radius: 50%; background: #ffffff; width: 80px; height: 80px; line-height: 76px; font-size: 30px; color: #ffcc00;"></i>
                                    </div>
                                    <div class="mega-info-footer">
                                      <h3 class="mega-info-title" style="font-size: 24px; line-height: ;">
                                        Sistema em nuvem </h3>
                                      <p class="mega-info-desc" style="font-size: ;">
                                        As atualizações são realizadas na base principal pela própria equipe da BisaWeb,
                                        após realização de testes diversos, e sem traumas para os clientes. A cada
                                        atualização, as novas funcionalidades são divulgadas para os clientes. </p>
                                      <a class="mega-info-btn" href="#" style="background: ; display: none;">
                                      </a>
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="mega_info_box">
                                    <div class="mega-info-header">
                                      <i class="fa fa-file-text-o" aria-hidden="true"
                                        style="border: 2px solid #ffcc00; border-radius: 50%; background: #ffffff; width: 80px; height: 80px; line-height: 76px; font-size: 30px; color: #ffcc00;"></i>
                                    </div>
                                    <div class="mega-info-footer">
                                      <h3 class="mega-info-title" style="font-size: 24px; line-height: ;">
                                        Contrato descomplicado </h3>
                                      <p class="mega-info-desc" style="font-size: ;">
                                        O contrato com a BisaWeb é mensal. Acreditamos que a melhor forma de mantermos
                                        nossos clientes conosco é investindo em gerar satisfação em usar nossos
                                        sistemas. </p>
                                      <a class="mega-info-btn" href="#" style="background: ; display: none;">
                                      </a>
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="mega_info_box">
                                    <div class="mega-info-header">
                                      <i class="fa fa-headphones" aria-hidden="true"
                                        style="border: 2px solid #ffcc00; border-radius: 50%; background: #ffffff; width: 80px; height: 80px; line-height: 76px; font-size: 30px; color: #ffcc00;"></i>
                                    </div>
                                    <div class="mega-info-footer">
                                      <h3 class="mega-info-title" style="font-size: 24px; line-height: ;">
                                        Suporte </h3>
                                      <p class="mega-info-desc" style="font-size: ;">
                                        O que garante a satisfação com qualquer sistema, é a capacidade de bom
                                        atendimento da equipe de suporte. Além de garantir o atendimento rápido e
                                        eficiente, realizamos pesquisas de satisfação periódicas com nossos clientes, e
                                        os mantemos informados sobre o resultado destas pesquisas. Atender nossos
                                        clientes e os auxiliar na solução de seus problemas é nossa missão, e nossa
                                        satisfação! Quem gosta do que faz, faz melhor. </p>
                                      <a class="mega-info-btn" href="#" style="background: ; display: none;">
                                      </a>
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="mega_info_box">
                                    <div class="mega-info-header">
                                      <i class="fa fa-database" aria-hidden="true"
                                        style="border: 2px solid #ffcc00; border-radius: 50%; background: #ffffff; width: 80px; height: 80px; line-height: 76px; font-size: 30px; color: #ffcc00;"></i>
                                    </div>
                                    <div class="mega-info-footer">
                                      <h3 class="mega-info-title" style="font-size: 24px; line-height: ;">
                                        Propriedade sobre o Banco de E-mails e Notícias </h3>
                                      <p class="mega-info-desc" style="font-size: ;">
                                        Temos o compromisso com nossos clientes com relação a manter a sua posse sobre
                                        seus dados, podendo eles solicitá-los a qualquer momento. Sendo necessário,
                                        realizamos a extração de informações de banco de e-mails e notícias, bastando
                                        para isto acionar a BisaWeb. </p>
                                      <a class="mega-info-btn" href="#" style="background: ; display: none;">
                                      </a>
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="vc_row-full-width vc_clearfix"></div>
                  <div id="depoimentos" data-vc-full-width="true" data-vc-full-width-init="false"
                    class="vc_row wpb_row vc_row-fluid vc_custom_1544548071354 vc_row-has-fill">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                      <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                          <div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span>
                          </div>

                          <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                              <h2 style="text-align: center;"><span style="color: #ffffff;">Depoimentos</span></h2>

                            </div>
                          </div>
                          <div class="testimonials_slider  hide-photos">
                            <ul class="testimonials_slider_ul">
                              <li>
                                <div class="single-photo-img"><img class="scale-with-grid"
                                    src="tema/wp-content/themes/betheme-n/images/testimonials-placeholder.png" alt="" />
                                </div>
                                <div class="bq_wrapper">
                                  <blockquote> <span style="color:#ffffff;">A maneira mais f&aacute;cil e &aacute;gil de
                                      gerenciar o seu sindicato, com relat&oacute;rios customiz&aacute;veis e
                                      estat&iacute;sticas atualizadas.</span></blockquote>
                                </div>
                                <div class="hr_dots"><span></span><span></span><span></span></div>
                                <div class="author">
                                  <h5></h5><span class="company"></span>
                                </div>
                              </li>
                            </ul>
                          </div>
                          <div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="vc_row-full-width vc_clearfix"></div>
                  <div id="numeros" data-vc-full-width="true" data-vc-full-width-init="false"
                    class="vc_row wpb_row vc_row-fluid vc_custom_1539987329345 vc_row-has-fill">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                      <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                          <div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span>
                          </div>

                          <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                              <div class="wpb_text_column wpb_content_element ">
                                <div class="wpb_wrapper">
                                  <h2 style="text-align: center;">Nossos números falam sobre nós</h2>
                                </div>
                              </div>
                              <div class="wpb_text_column wpb_content_element ">
                                <div class="wpb_wrapper">
                                  <p style="text-align: center;">Sapien nulla nisi dolor vehicula ut integer sociis ut
                                    non netus adipiscing porta ac pharetra ullamcorper ridiculus a a cubilia himenaeos
                                    eget. At hendrerit tincidunt ligula scelerisque eu vel pharetra sagittis vestibulum
                                    ut enim vestibulum.</p>
                                </div>
                              </div>

                            </div>
                          </div>
                          <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-4">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="quick_fact align_center animate-math">
                                    <div class="number-wrapper"><span class="label prefix">+</span><span class="number"
                                        data-to="100">100</span><span class="label postfix">mil</span></div>
                                    <hr class="hr_narrow" />
                                    <div class="desc">notícias cadastradas</div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-4">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="quick_fact align_center animate-math">
                                    <div class="number-wrapper"><span class="label prefix">+</span><span class="number"
                                        data-to="5">5</span><span class="label postfix">milhões</span></div>
                                    <hr class="hr_narrow" />
                                    <div class="desc">de endereços de e-mails</div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-4">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="quick_fact align_center animate-math">
                                    <div class="number-wrapper"><span class="label prefix">+</span><span class="number"
                                        data-to="300">300</span></div>
                                    <hr class="hr_narrow" />
                                    <div class="desc">Modelos de Newsletters</div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="vc_row-full-width vc_clearfix"></div>
                  <div id="blog" data-vc-full-width="true" data-vc-full-width-init="false"
                    class="vc_row wpb_row vc_row-fluid vc_custom_1544548048336 vc_row-has-fill">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                      <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                          <div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span>
                          </div>

                          <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                              <h2 style="text-align: center;">Blog da Newsletter Institucional</h2>
                              <h2 style="text-align: center;"><img class="alignnone size-full wp-image-80"
                                  src="tema/wp-content/uploads/2018/10/Rectangle-4.5.png" alt="" width="63" height="5"
                                  srcset="https://www.noticiadorweb.bisaweb.com.br/wp-content/uploads/2018/10/Rectangle-4.5.png 63w, https://www.noticiadorweb.bisaweb.com.br/wp-content/uploads/2018/10/Rectangle-4.5-50x4.png 50w"
                                  sizes="(max-width: 63px) 100vw, 63px" /></h2>
                              <p>&nbsp;</p>
                              

                            </div>
                          </div>
                         
                          <div class="column_filters">
                            <div class="blog_wrapper isotope_wrapper clearfix">
                              <div class="posts_group lm_wrapper col-2 classic hide-more">
                                <div id="listaTodas"></div>
                              </div>
                            </div>
                          </div>
                          <style type="text/css">
                            .vc_btn3-style-gradient-custom.vc_btn-gradient-btn-5d2c7a0da7a87:hover {
                              color: #ffffff;
                              background-color: #ffcc00;
                              border: none;
                              background-position: 100% 0;
                            }
                          </style>
                          <style type="text/css">
                            .vc_btn3-style-gradient-custom.vc_btn-gradient-btn-5d2c7a0da7a87 {
                              color: #ffffff;
                              border: none;
                              background-color: #ffcc00;
                              background-image: -webkit-linear-gradient(left, #ffcc00 0%, #ffcc00 50%, #ffcc00 100%);
                              background-image: linear-gradient(to right, #ffcc00 0%, #ffcc00 50%, #ffcc00 100%);
                              -webkit-transition: all .2s ease-in-out;
                              transition: all .2s ease-in-out;
                              background-size: 200% 100%;
                            }
                          </style>
                          <div class="vc_btn3-container vc_btn3-center">
                            <a href="blog.php" data-vc-gradient-1="#ffcc00" data-vc-gradient-2="#ffcc00"
                              class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-gradient-custom vc_btn-gradient-btn-5d2c7a0da7a87">Ver
                              todos</a></div>
                          <div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="vc_row-full-width vc_clearfix"></div>
                  <div id="contato" data-vc-full-width="true" data-vc-full-width-init="false"
                    class="vc_row wpb_row vc_row-fluid vc_custom_1539977748140 vc_row-has-fill">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                      <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                          <div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span>
                          </div>

                          <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                              <h2 style="text-align: center;">Entre em contato</h2>
                              <p style="text-align: center;">Solicite uma demonstração e conheça tudo que o JurídicoWeb
                                pode simplificar na sua gestão. Desde já, agradecemos pela oportunidade.</p>
                              <p style="text-align: center;">
                                <style type="text/css" id="cspFormShell_11_290001_style">
                                  #cspFormShell_11_290001 {}

                                  #cspFormShell_11_290001 label {
                                    width: 100%;
                                  }

                                  /*Fields shell basic*/
                                  #cspFormShell_11_290001 .cfsFieldShell input:not([type="checkbox"]):not([type="radio"]),
                                  #cspFormShell_11_290001 .cfsFieldShell textarea,
                                  #cspFormShell_11_290001 .cfsFieldShell .cfsListSelect,
                                  #cspFormShell_11_290001 .cfsFieldShell .cfsCheck,
                                  #cspFormShell_11_290001 .cfsFieldShell button {
                                    padding: 10px;
                                    width: 100%;
                                    font-size: 18px;
                                    line-height: normal;
                                    border: 1px solid rgba(51, 51, 51, 0.1);
                                    color: rgba(51, 51, 51, 0.7);
                                    background-color: #f7f7f7;
                                    hyphens: none;
                                    border-radius: 0;
                                    margin: 0;
                                    max-width: 100%;
                                    vertical-align: baseline;
                                    display: block;
                                    height: auto;
                                  }

                                  #cspFormShell_11_290001 .cfsFieldShell input[type="submit"]:not([type="checkbox"]):not([type="radio"]),
                                  #cspFormShell_11_290001 .cfsFieldShell input[type="reset"]:not([type="checkbox"]):not([type="radio"]),
                                  #cspFormShell_11_290001 .cfsFieldShell input[type="button"],
                                  #cspFormShell_11_290001 .cfsFieldShell button {
                                    cursor: pointer;
                                    font-weight: 700;
                                    border: 0 none;
                                    color: #fff;
                                    text-transform: uppercase;
                                    -webkit-appearance: none;
                                  }

                                  #cspFormShell_11_290001 .cfsFieldShell input[type="submit"]:not([type="checkbox"]):not([type="radio"]),
                                  #cspFormShell_11_290001 .cfsFieldShell input[type="button"],
                                  #cspFormShell_11_290001 .cfsFieldShell button {
                                    background-color: #333333;
                                    background-image: none;
                                  }

                                  #cspFormShell_11_290001 .cfsFieldShell input[type="submit"]:not([type="checkbox"]):not([type="radio"]):hover,
                                  #cspFormShell_11_290001 .cfsFieldShell input[type="button"]:hover,
                                  #cspFormShell_11_290001 .cfsFieldShell button:hover {
                                    background-color: rgba(51, 51, 51, 0.7);
                                  }

                                  #cspFormShell_11_290001 .cfsFieldShell input[type="reset"]:not([type="checkbox"]):not([type="radio"]) {
                                    background-color: #333333;
                                    background-image: none;
                                  }

                                  #cspFormShell_11_290001 .cfsFieldShell input[type="reset"]:not([type="checkbox"]):not([type="radio"]):hover {
                                    background-color: rgba(51, 51, 51, 0.7);
                                  }

                                  /* Placeholder text color -- selectors need to be separate to work. */
                                  #cspFormShell_11_290001 ::-webkit-input-placeholder {
                                    color: rgba(51, 51, 51, 0.7);
                                  }

                                  #cspFormShell_11_290001 :-moz-placeholder {
                                    color: rgba(51, 51, 51, 0.7);
                                  }

                                  #cspFormShell_11_290001 ::-moz-placeholder {
                                    color: rgba(51, 51, 51, 0.7);
                                    opacity: 1;
                                    /* Since FF19 lowers the opacity of the placeholder by default */
                                  }

                                  #cspFormShell_11_290001 :-ms-input-placeholder {
                                    color: rgba(51, 51, 51, 0.7);
                                  }

                                  #cspFormShell_11_290001 {
                                    width: 100%
                                  }

                                  #cspFormShell_11_290001 .cfsSuccessMsg {
                                    color: #4ae8ea !important
                                  }
                                </style>
                                <div id="cspFormShell_11_290001" class="cfsFormShell">
                                  <form
                                    name="form_contato" 
                                    class="csfForm" 
                                    method="post"
                                    action="https://www.noticiadorweb.com.br/index.php?secao=assinar_newsletter&action=save"
                                  >
                                    <input name="assinatura" type="hidden" value="129"/>
                                    <div class="row cfsFieldsRow">
                                      <div class="col-sm-6 cfsFieldCol" data-name="first_name" data-type="text">
                                        <div class="cfsFieldShell cfsField_text">
                                          <input 
                                            type="text"
                                            name="name" 
                                            value="" 
                                            data-name="first_name" 
                                            required
                                            placeholder="Nome"
                                            id="name" />
                                        </div>
                                      </div>
                                      <div class="col-sm-6 cfsFieldCol" data-name="last_name" data-type="text">
                                        <div class="cfsFieldShell cfsField_text"><input type="text"
                                            name="tel" value="" data-name="last_name"
                                            placeholder="Telefone" id="tel" /></div>
                                      </div>
                                    </div>
                                    <div class="row cfsFieldsRow">
                                      <div class="col-sm-6 cfsFieldCol" data-name="email" data-type="email">
                                        <div class="cfsFieldShell cfsField_email"><input type="email"
                                            name="email" value="" data-name="email" required
                                            placeholder="Email" id="email" /></div>
                                      </div>
                                      <div class="col-sm-6 cfsFieldCol" data-name="empresa" data-type="text">
                                        <div class="cfsFieldShell cfsField_text"><input type="text"
                                            name="empresa" value="" data-name="empresa" placeholder="Empresa" id="empresa" />
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row cfsFieldsRow">
                                      <div class="col-sm-12 cfsFieldCol" data-name="message" data-type="textarea">
                                        <div class="cfsFieldShell cfsField_textarea"><textarea name="message"
                                            data-name="message" id="message" required placeholder="Mensagem" rows="3"
                                            cols="50"></textarea></div>
                                      </div>
                                    </div>
                                    <div class="row cfsFieldsRow">
                                      <div class="col-sm-12 cfsFieldCol" data-name="send">
                                        <div class="cfsFieldShell">
                                          <button 
                                              onclick="is_email_contato(email.value)"; 
                                              
                                      
                                              id="btn_contato">
                                              Enviar
                                </button>
                                            </div>
                                      </div>
                                    </div>
                                  
                                    <div class="cfsContactMsg"></div>
                                    <input name="areas[]" type="hidden" value="1041" />
                                  </form>
                                </div><span style="display: none !important;"
                                  class="cfsFormDesc">eyJpZCI6IjExIiwibGFiZWwiOiJub3RpY2lhZG9yIGNvbnRhdG8iLCJhY3RpdmUiOiIxIiwib3JpZ2luYWxfaWQiOiIxIiwidW5pcXVlX2lkIjoid2VmajIiLCJwYXJhbXMiOnsiZW5hYmxlRm9yTWVtYmVyc2hpcCI6IjAiLCJ0cGwiOnsid2lkdGgiOiIxMDAiLCJ3aWR0aF9tZWFzdXJlIjoiJSIsImJnX3R5cGVfMCI6Im5vbmUiLCJiZ19pbWdfMCI6IiIsImJnX2NvbG9yXzAiOiIjODFkNzQyIiwiYmdfdHlwZV8xIjoiY29sb3IiLCJiZ19pbWdfMSI6IiIsImJnX2NvbG9yXzEiOiIjMzMzMzMzIiwiYmdfdHlwZV8yIjoiY29sb3IiLCJiZ19pbWdfMiI6IiIsImJnX2NvbG9yXzIiOiIjMzMzMzMzIiwiYmdfdHlwZV8zIjoiY29sb3IiLCJiZ19pbWdfMyI6IiIsImJnX2NvbG9yXzMiOiIjMzMzMzMzIiwiZmllbGRfZXJyb3JfaW52YWxpZCI6IiIsImZvcm1fc2VudF9tc2ciOiJUaGFuayB5b3UgZm9yIGNvbnRhY3RpbmcgdXMhIiwiZm9ybV9zZW50X21zZ19jb2xvciI6IiM0YWU4ZWEiLCJoaWRlX29uX3N1Ym1pdCI6IjEiLCJyZWRpcmVjdF9vbl9zdWJtaXQiOiIiLCJ0ZXN0X2VtYWlsIjoia2V5bGVhbXVub3pAZ21haWwuY29tIiwic2F2ZV9jb250YWN0cyI6IjEiLCJleHBfZGVsaW0iOiI7IiwiZmJfY29udmVydF9iYXNlIjoiIiwicHViX3Bvc3RfdHlwZSI6InBvc3QiLCJwdWJfcG9zdF9zdGF0dXMiOiJwdWJsaXNoIiwicmVnX3dwX2NyZWF0ZV91c2VyX3JvbGUiOiJzdWJzY3JpYmVyIiwiZmllbGRfd3JhcHBlciI6IjxkaXYgW2ZpZWxkX3NoZWxsX2NsYXNzZXNdIFtmaWVsZF9zaGVsbF9zdHlsZXNdPltmaWVsZF08XC9kaXY+In0sImZpZWxkcyI6W3siYnNfY2xhc3NfaWQiOiI2IiwibmFtZSI6ImZpcnN0X25hbWUiLCJsYWJlbCI6IiIsInBsYWNlaG9sZGVyIjoiTm9tZSIsInZhbHVlIjoiIiwiaHRtbCI6InRleHQiLCJtYW5kYXRvcnkiOiIxIiwibWluX3NpemUiOiIiLCJtYXhfc2l6ZSI6IiIsImFkZF9jbGFzc2VzIjoiIiwiYWRkX3N0eWxlcyI6IiIsImFkZF9hdHRyIjoiIiwidm5fb25seV9udW1iZXIiOiIwIiwidm5fb25seV9sZXR0ZXJzIjoiMCIsInZuX3BhdHRlcm4iOiIwIiwidmFsdWVfcHJlc2V0IjoiIiwidm5fZXF1YWwiOiIiLCJpY29uX2NsYXNzIjoiIiwiaWNvbl9zaXplIjoiIiwiaWNvbl9jb2xvciI6IiIsInRlcm1zIjoiIn0seyJic19jbGFzc19pZCI6IjYiLCJuYW1lIjoibGFzdF9uYW1lIiwibGFiZWwiOiIiLCJwbGFjZWhvbGRlciI6IlRlbGVmb25lIiwidmFsdWUiOiIiLCJodG1sIjoidGV4dCIsIm1hbmRhdG9yeSI6IjAiLCJtaW5fc2l6ZSI6IiIsIm1heF9zaXplIjoiIiwiYWRkX2NsYXNzZXMiOiIiLCJhZGRfc3R5bGVzIjoiIiwiYWRkX2F0dHIiOiIiLCJ2bl9vbmx5X251bWJlciI6IjAiLCJ2bl9vbmx5X2xldHRlcnMiOiIwIiwidm5fcGF0dGVybiI6IjAiLCJ2YWx1ZV9wcmVzZXQiOiIiLCJ2bl9lcXVhbCI6IiIsImljb25fY2xhc3MiOiIiLCJpY29uX3NpemUiOiIiLCJpY29uX2NvbG9yIjoiIiwidGVybXMiOiIifSx7ImJzX2NsYXNzX2lkIjoiNiIsIm5hbWUiOiJlbWFpbCIsImxhYmVsIjoiIiwicGxhY2Vob2xkZXIiOiJFbWFpbCIsInZhbHVlIjoiIiwiaHRtbCI6ImVtYWlsIiwibWFuZGF0b3J5IjoiMSIsIm1pbl9zaXplIjoiIiwibWF4X3NpemUiOiIiLCJhZGRfY2xhc3NlcyI6IiIsImFkZF9zdHlsZXMiOiIiLCJhZGRfYXR0ciI6IiIsInZuX29ubHlfbnVtYmVyIjoiMCIsInZuX29ubHlfbGV0dGVycyI6IjAiLCJ2bl9wYXR0ZXJuIjoiMCJ9LHsiYnNfY2xhc3NfaWQiOiI2IiwibmFtZSI6ImVtcHJlc2EiLCJsYWJlbCI6IiIsInBsYWNlaG9sZGVyIjoiRW1wcmVzYSIsInZhbHVlIjoiIiwidmFsdWVfcHJlc2V0IjoiIiwiaHRtbCI6InRleHQiLCJtYW5kYXRvcnkiOiIwIiwibWluX3NpemUiOiIiLCJtYXhfc2l6ZSI6IiIsImFkZF9jbGFzc2VzIjoiIiwiYWRkX3N0eWxlcyI6IiIsImFkZF9hdHRyIjoiIiwidm5fb25seV9udW1iZXIiOiIwIiwidm5fb25seV9sZXR0ZXJzIjoiMCIsInZuX3BhdHRlcm4iOiIiLCJ2bl9lcXVhbCI6IiIsImljb25fY2xhc3MiOiIiLCJpY29uX3NpemUiOiIiLCJpY29uX2NvbG9yIjoiIiwidGVybXMiOiIifSx7ImJzX2NsYXNzX2lkIjoiMTIiLCJuYW1lIjoibWVzc2FnZSIsImxhYmVsIjoiIiwicGxhY2Vob2xkZXIiOiJNZW5zYWdlbSIsInZhbHVlIjoiIiwiaHRtbCI6InRleHRhcmVhIiwibWFuZGF0b3J5IjoiMSIsIm1pbl9zaXplIjoiIiwibWF4X3NpemUiOiIiLCJhZGRfY2xhc3NlcyI6IiIsImFkZF9zdHlsZXMiOiIiLCJhZGRfYXR0ciI6IiIsInZuX29ubHlfbnVtYmVyIjoiMCIsInZuX29ubHlfbGV0dGVycyI6IjAiLCJ2bl9wYXR0ZXJuIjoiMCIsInZhbHVlX3ByZXNldCI6IiIsInZuX2VxdWFsIjoiIiwiaWNvbl9jbGFzcyI6IiIsImljb25fc2l6ZSI6IiIsImljb25fY29sb3IiOiIiLCJ0ZXJtcyI6IiJ9LHsiYnNfY2xhc3NfaWQiOiIxMiIsIm5hbWUiOiJzZW5kIiwibGFiZWwiOiJFbnZpYXIiLCJodG1sIjoic3VibWl0IiwiYWRkX2NsYXNzZXMiOiIiLCJhZGRfc3R5bGVzIjoiIiwiYWRkX2F0dHIiOiIiLCJpY29uX2NsYXNzIjoiIiwiaWNvbl9zaXplIjoiIiwiaWNvbl9jb2xvciI6IiIsInRlcm1zIjoiIn1dLCJvcHRzX2F0dHJzIjp7ImJnX251bWJlciI6IjQifX0sImltZ19wcmV2aWV3IjoiYmFzZS1jb250YWN0LmpwZyIsInZpZXdzIjoiNDUiLCJ1bmlxdWVfdmlld3MiOiIxNiIsImFjdGlvbnMiOiIwIiwic29ydF9vcmRlciI6IjEiLCJpc19wcm8iOiIwIiwiYWJfaWQiOiIwIiwiZGF0ZV9jcmVhdGVkIjoiMjAxNi0wNS0wMyAxNTowMTowMyIsImltZ19wcmV2aWV3X3VybCI6Imh0dHA6XC9cL3N1cHN5c3RpYy00MmQ3Lmt4Y2RuLmNvbVwvX2Fzc2V0c1wvZm9ybXNcL2ltZ1wvcHJldmlld1wvYmFzZS1jb250YWN0LmpwZyIsInZpZXdfaWQiOiIxMV8yOTAwMDEiLCJ2aWV3X2h0bWxfaWQiOiJjc3BGb3JtU2hlbGxfMTFfMjkwMDAxIiwiY29ubmVjdF9oYXNoIjoiMzUzOTlmZmU2Y2QzNjgyMjY1Mjc5YzRiZTM4OTcxNTkifQ==</span>
                              </p>

                            </div>
                          </div>
                          <div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="vc_row-full-width vc_clearfix"></div>
               
                  <div class="vc_row-full-width vc_clearfix"></div>
                  <div id="newsletter" data-vc-full-width="true" data-vc-full-width-init="false"
                    class="vc_row wpb_row vc_row-fluid vc_custom_1544548025733 vc_row-has-fill">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                      <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                          <div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span>
                          </div>

                          <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                              <h2 style="text-align: center;"><span style="color: #ffffff;">Receba nosso
                                  newsletter<br />
                                  <img class="alignnone size-full wp-image-80"
                                    src="tems/kdsolutions.com.br/bisaweb/noticiador/wp-content/uploads/2018/10/Rectangle-4.5.png"
                                    alt="" width="63" height="5"
                                    srcset="https://www.noticiadorweb.bisaweb.com.br/wp-content/uploads/2018/10/Rectangle-4.5.png 63w, https://www.noticiadorweb.bisaweb.com.br/wp-content/uploads/2018/10/Rectangle-4.5-50x4.png 50w"
                                    sizes="(max-width: 63px) 100vw, 63px" /><br />
                                </span></h2>
                              <div style="text-align: center;">
                                <div><span style="color: #ffffff;">Administra as Contas a Pagar da empresa, de maneira
                                    simples e objetiva.</span></div>
                              </div>

                            </div>
                          </div>

                          <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                              <p style="text-align: center;">
                              
                                <div id="mwd-pages1" class="wdform_page_navigation " show_title="false"
                                  show_numbers="true" type="none"></div>
                                  
                               
                                  

                                <form 
                                  name="frm_news" 
                                  action="https://noticiadorweb.com.br/index.php?secao=assinar_newsletter&action=save" 
                                  method="post"
                                  id="mwd-form1" 
                                  class="mwd-form"> 
                                  <input name="assinatura" type="hidden" value="129" />
                                  <div class="wdform-page-and-images"
                                    style="display:table; border-top:0px solid black;">
                                    <div id="1form_view1" class="wdform_page" page_title="Untitled page"
                                      next_title="Next" next_type="text" next_class="wdform-page-button"
                                      next_checkable="false" previous_title="Previous" previous_type="text"
                                      previous_class="wdform-page-button" previous_checkable="false">
                                      <div class="wdform_section">
                                        <div class="wdform_column ui-sortable">
                                          <div wdid="3" class="wdform_row ui-sortable-handle" style="">
                                            <div type="type_text" class="wdform-field" style="width:300px">
                                              <div class="wdform-label-section " style="float: left; width: 100px;">
                                                <span class="wdform-label">Nome</span></div>
                                              <div class="wdform-element-section" style=" width: 200px;">
                                                <input type="text" class="input_deactive" id="wdform_3_element1"
                                                  name="nome" value="" title="" style="width: 100%;"></div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="wdform_column ui-sortable">
                                          <div wdid="1" class="wdform_row ui-sortable-handle" style="">
                                            <div type="type_submitter_mail" class="wdform-field" style="width:300px">
                                              <div class="wdform-label-section " style="float: left;; width: 100px;">
                                                <span class="wdform-label">E-mail:</span><span
                                                  class="wdform-required">*</span></div>
                                              <div class="wdform-element-section " style=" width: 200px;">
                                              <input
                                                  type="text" class="input_deactive" id="wdform_1_element1"
                                                  name="email" value="" title="" style="width: 100%;"></div>
                                            </div>
                                            <input name="areas[]" type="hidden" value="1041" />
                                          </div>
                                        </div>
                                        <div class="wdform_column ui-sortable">
                                          <div wdid="2" class="wdform_row ui-sortable-handle">
                                            <div type="type_submit_reset" class="wdform-field mwd-subscribe-reset">
                                              <div class="wdform-label-section" style="display: table-cell;"></div>
                                              <div class="wdform-element-section " style="display: table-cell;"><button
                                                  type="button" class="button-submit"
                                                  onclick="is_email(email.value);">Enviar</button><button
                                                  type="button" class="button-reset"
                                                  onclick="mwd_check_required1('reset');"
                                                  style="display: none;">Reset</button></div>
                                            </div>
                                            <div class="mwd-clear"></div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="wdform_footer">
                                        <div style="width: 100%;">
                                          <div style="width: 100%; display: table;">
                                            <div style="display: table-row-group;">
                                              <div id="mwd_1page_nav1" style="display: table-row;"></div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="wdform_preload"></div>
                                </form>
                              </p>

                            </div>
                          </div>
                          <div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="vc_row-full-width vc_clearfix"></div>
                </div>
              </div>
            </div>
            <div class="section section-page-footer">
              <div class="section_wrapper clearfix">

                <div class="column one page-pager">
                </div>

              </div>
            </div>

          </div>


        </div>

        <!-- .four-columns - sidebar -->

      </div>
    </div>


    <!-- mfn_hook_content_after -->
    <!-- mfn_hook_content_after -->
    <!-- #Footer -->
    <footer id="Footer" class="clearfix">


      <div class="widgets_wrapper" style="padding:70px 0;">
        <div class="container">
          <div class="column two-third">
            <aside id="text-3" class="widget widget_text">
              <div class="textwidget">
                <h2>Desenvolvido por Bisaweb<br />
                  <img src="tema/kdsolutions.com.br/bisaweb/noticiador/wp-content/uploads/2018/10/Rectangle-4.5.png" />
                </h2>
                <div>
                  <p>Desenvolvemos ferramentas de tecnologia da informação para facilitar a melhoria das práticas nos
                    sistemas de gestão por todo e qualquer tipo de organização. Contamos com 26 anos de atuação no
                    mercado de software, e 3 certificações MPSBR:</p>
                </div>
                <div>Nível G em desenvolvimento  –  SV para  serviços  –  MPT nível 1 (testes)</div>
                <p>&nbsp;</p>
                <div><b>Conheça nosso </b><b><a href="https://www.bisaweb.com.br/">site</a></b></div>
              </div>
            </aside>
          </div>
          <div class="column one-third">
            <aside id="text-4" class="widget widget_text">
              <div class="textwidget">
                <h2>Contato<br />
                  <img src="tema/kdsolutions.com.br/bisaweb/noticiador/wp-content/uploads/2018/10/Rectangle-4.5.png" />
                </h2>
                <div></div>
                <div>
                  <div>
                    <p>(81) 3312-7070</p>
                    <p>(81) 98990-5518</p>
                  </div>
                  <div></div>
                  <div>suporte@bisa.com.br</div>
                  <div>Endereço: Rua João Suassuna, 51<br />
                    Boa Vista Recife – PE</div>
                  <p>&nbsp;</p>
                  <div>
                    <p><img class="alignnone size-full wp-image-91"
                        src="tema/kdsolutions.com.br/bisaweb/noticiador/wp-content/uploads/2018/10/facebook-square-2.jpg"
                        alt="" width="16" height="16" />    <img class="alignnone size-full wp-image-92"
                        src="tema/kdsolutions.com.br/bisaweb/noticiador/wp-content/uploads/2018/10/youtube-logotype-2.jpg"
                        alt="" width="16" height="16" /></p>
                  </div>
                </div>
              </div>
            </aside>
          </div>
        </div>
      </div>
      <?php 

                       

         $nome= $_POST['nome'] ?? "";
         $email= $_POST['email'] ?? "";
         $tel= $_POST['tel'] ?? "";
         $empresa = $_POST['empresa'] ?? "";
         $msg = $_POST['message'] ?? "";                   
         
     
         if (($nome) && ($email) && ($tel) && ($empresa) && ($msg)) {

           $corpo = "Nome: ".$nome."\n";
           $corpo .= "E-mail: ".$email."\n";
           $corpo .= "Telefone: ".$tel."\n";
           $corpo .= "Empresa: ".$empresa."\n";
           $corpo .= "Mensagem: ".$msg."\n";
           
   
   
           $from="Blog da Gestão fincanceira";
           $subject="Formulário de inscrição - Blog da Gestão fincanceira";
           mail("desenvolvimento9@bisa.com.br", $subject, $corpo, $from);
         }
        
        
      ?>


      <div class="footer_copy">
        <div class="container">
          <div class="column one">

            <a id="back_to_top" class="button button_js" href="#"><i class="icon-up-open-big"></i></a>
            <!-- Copyrights -->
            <div class="copyright">
              © 2018 Moneta Web - Todos os direitos reservados </div>

            <ul class="social"></ul>
          </div>
        </div>
      </div>





    </footer>

  </div><!-- #Wrapper -->




  <!-- mfn_hook_bottom -->
  <!-- mfn_hook_bottom -->
  <!-- wp_footer() -->
  <link rel='stylesheet' id='info-box-css-css'
    href='tema/wp-content/plugins/mega-addons-for-visual-composer/css/infobox5560.css?ver=5.0.4' type='text/css'
    media='all' />
  <link rel='stylesheet' id='cfs.frontend.bootstrap.partial-css'
    href='tema/supsystic-42d7.kxcdn.com/_assets/forms/css/frontend.bootstrap.partial.min00e2.html?ver=1.5.7'
    type='text/css' media='all' />
  <link rel='stylesheet' id='cfs.frontend.forms-css'
    href='tema/wp-content/plugins/contact-form-by-supsystic/modules/forms/css/frontend.forms00e2.css?ver=1.5.7'
    type='text/css' media='all' />
  <link rel='stylesheet' id='mwd-style-1-css'
    href='tema/wp-content/plugins/wd-mailchimp/css/frontend/mwd-style-1192c.css?ver=1034731462' type='text/css'
    media='all' />
  <script type='text/javascript' src='tema/wp-includes/js/jquery/ui/widget.mine899.js?ver=1.11.4'></script>
  <script type='text/javascript' src='tema/wp-includes/js/jquery/ui/mouse.mine899.js?ver=1.11.4'></script>
  <script type='text/javascript' src='tema/wp-includes/js/jquery/ui/slider.mine899.js?ver=1.11.4'></script>
  <script type='text/javascript' src='tema/wp-includes/js/jquery/ui/button.mine899.js?ver=1.11.4'></script>
  <script type='text/javascript' src='tema/wp-includes/js/jquery/ui/spinner.mine899.js?ver=1.11.4'></script>
  <script type='text/javascript' src='tema/wp-includes/js/jquery/ui/datepicker.mine899.js?ver=1.11.4'></script>
  <script type='text/javascript'>
    jQuery(document).ready(function (jQuery) { jQuery.datepicker.setDefaults({ "closeText": "Fechar", "currentText": "Hoje", "monthNames": ["janeiro", "fevereiro", "mar\u00e7o", "abril", "maio", "junho", "julho", "agosto", "setembro", "outubro", "novembro", "dezembro"], "monthNamesShort": ["jan", "fev", "mar", "abr", "maio", "jun", "jul", "ago", "set", "out", "nov", "dez"], "nextText": "Pr\u00f3ximo", "prevText": "Anterior", "dayNames": ["domingo", "segunda-feira", "ter\u00e7a-feira", "quarta-feira", "quinta-feira", "sexta-feira", "s\u00e1bado"], "dayNamesShort": ["dom", "seg", "ter", "qua", "qui", "sex", "s\u00e1b"], "dayNamesMin": ["D", "S", "T", "Q", "Q", "S", "S"], "dateFormat": "d \\dd\\e MM \\dd\\e yy", "firstDay": 0, "isRTL": false }); });
  </script>
  <script type='text/javascript' src='tema/wp-includes/js/jquery/ui/effect.mine899.js?ver=1.11.4'></script>
  <script type='text/javascript' src='tema/wp-includes/js/jquery/ui/effect-shake.mine899.js?ver=1.11.4'></script>
  <script type='text/javascript' src='tema/wp-includes/js/jquery/ui/sortable.mine899.js?ver=1.11.4'></script>
  <script type='text/javascript' src='tema/wp-includes/js/jquery/ui/tabs.mine899.js?ver=1.11.4'></script>
  <script type='text/javascript' src='tema/wp-includes/js/jquery/ui/accordion.mine899.js?ver=1.11.4'></script>
  <script type='text/javascript' src='tema/wp-content/themes/betheme-n/js/pluginsae03.js?ver=20.8.1'></script>
  <script type='text/javascript' src='tema/wp-content/themes/betheme-n/js/menuae03.js?ver=20.8.1'></script>
  <script type='text/javascript'
    src='tema/wp-content/themes/betheme-n/assets/animations/animations.minae03.js?ver=20.8.1'></script>
  <script type='text/javascript'
    src='tema/wp-content/themes/betheme-n/assets/jplayer/jplayer.minae03.js?ver=20.8.1'></script>
  <script type='text/javascript' src='tema/wp-content/themes/betheme-n/js/parallax/translate3dae03.js?ver=20.8.1'></script>
  <script type='text/javascript' src='tema/wp-content/themes/betheme-n/js/scriptsae03.js?ver=20.8.1'></script>
  <script type='text/javascript' src='tema/wp-includes/js/wp-embed.min5560.js?ver=5.0.4'></script>
  <script type='text/javascript'
    src='tema/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min7e15.js?ver=5.5.4'></script>
  <script type='text/javascript'
    src='tema/wp-content/plugins/contact-form-by-supsystic/js/common.min00e2.js?ver=1.5.7'></script>
  <script type='text/javascript'>
    /* <![CDATA[ */
    var CFS_DATA = { "siteUrl": "http:\/\/www.noticiadorweb.bisaweb.com.br\/", "imgPath": "http:\/\/www.noticiadorweb.bisaweb.com.br\/wp-content\/plugins\/contact-form-by-supsystic\/img\/", "cssPath": "http:\/\/www.noticiadorweb.bisaweb.com.br\/wp-content\/plugins\/contact-form-by-supsystic\/css\/", "loader": "http:\/\/www.noticiadorweb.bisaweb.com.br\/wp-content\/plugins\/contact-form-by-supsystic\/img\/loading.gif", "close": "http:\/\/www.noticiadorweb.bisaweb.com.br\/wp-content\/plugins\/contact-form-by-supsystic\/img\/cross.gif", "ajaxurl": "http:\/\/www.noticiadorweb.bisaweb.com.br\/wp-admin\/admin-ajax.php", "options": { "add_love_link": "0", "disable_autosave": "0", "love_link_html": "<a title=\"WordPress Form Plugin\" style=\"color: #26bfc1 !important; font-size: 9px; position: absolute; bottom: 15px; right: 15px;\" href=\"https:\/\/supsystic.com\/plugins\/contact-form-plugin\/?utm_source=plugin&utm_medium=love_link&utm_campaign=forms\" target=\"_blank\">WordPress Form Plugin<\/a>" }, "CFS_CODE": "cfs", "jsPath": "http:\/\/www.noticiadorweb.bisaweb.com.br\/wp-content\/plugins\/contact-form-by-supsystic\/js\/" };
/* ]]> */
  </script>
  <script type='text/javascript'
    src='tema/wp-content/plugins/contact-form-by-supsystic/js/core.min00e2.js?ver=1.5.7'></script>
  <script type='text/javascript'
    src='tema/wp-content/plugins/contact-form-by-supsystic/modules/forms/js/forms.modernizr.min00e2.js?ver=1.5.7'></script>
  <script type='text/javascript'
    src='tema/wp-content/plugins/contact-form-by-supsystic/modules/forms/js/frontend.forms00e2.js?ver=1.5.7'></script>

    <script>
    
      function is_email(email){ 
        er = /^[a-zA-Z0-9][a-zA-Z0-9._-]+@([a-zA-Z0-9._-]+.)[a-zA-Z-0-9]{2}/;
        if(er.exec(email)) {
          document.frm_news.submit();
        } else {
          alert("E-mail inválido.");
        }
      }

    function is_email_contato(email){
        console.log(email) 
        er = /^[a-zA-Z0-9][a-zA-Z0-9._-]+@([a-zA-Z0-9._-]+.)[a-zA-Z-0-9]{2}/;
        if(er.exec(email)) {
          document.form_contato.submit();
        } else {
          alert("E-mail inválido.");
        }
    }
	
    $(function(){
      $( "#btn_contato" ).click(function(){

        
          var nome = $('#name').val();
          var email = $('#email').val();
          var tel = $('#tel').val();
          var empresa = $('#empresa').val();
          var mensagem = $('#message').val();
          
          $.ajax({
              type: 'post',
              url: 'index.php',
              data:  {nome: nome, email: email, tel: tel, empresa: empresa, mensagem: mensagem},
              }).done(function () {
                alert("e=mail enviado com sucesso!");
              });
          });
      }); 

    </script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script><script type="text/javascript">


  //JS para exibi?o de todas as not?ias (RESUMOS) dentro do blog atualizacontabil.
  $.ajax({

      url: 'https://noticiadorweb.com.br/index.php?secao=api&action=resumo',
    type: 'POST',
    dataType: 'JSON',
    data: { 
		  areas: ['1000:2','1001:2']
    }

  }).always(function(response) {


      $.each(response, function(index, not) {        


           imagem = (not.imagem ? not.imagem : 'tema/wp-content/uploads/2018/12/Logo_MonetaWeb-1-1.png')
           titulo = (not.titulo ? not.titulo : 'Sem titulo')
           noticias = `
           <div
                                  class="post-item isotope-item clearfix author-keyle post-116 post type-post status-publish format-standard has-post-thumbnail hentry category-uncategorized">
                                  <div class="image_frame post-photo-wrapper scale-with-grid image">
                                    <div class="image_wrapper">
                                      <a href="blog.php/?noticia_id=${not.id}&idGN=0&qtdNot=0" target="_blank">
                                        <div class="mask"></div><img width="500" height="300"
                                          src="${imagem}"
                                          class="scale-with-grid wp-post-image" alt=""
                                          srcset="${imagem}"
                                          sizes="(max-width: 500px) 100vw, 300px" />
                                      </a>
                                    </div>
                                  </div>
                                  <div class="post-desc-wrapper">
                                    <div class="post-desc">
                                      <div class="post-head"></div>
                                      <div class="post-title">
                                        <h2 class="titulo-not"><a
                                            
                                            href="blog.php/?noticia_id=${not.id}&idGN=0&qtdNot=0">${titulo}</a></h2>
                                      </div>
                                      <div class="post-excerpt">${not.noticia}</div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
           
           ` 
          $('#listaTodas').append(noticias);
    });      

});
</script>
</body>

<!-- Mirrored from www.noticiadorweb.bisaweb.com.br/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Jul 2019 13:06:00 GMT -->

</html>
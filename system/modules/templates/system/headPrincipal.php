<? 

$usuario_logado = false;
$controller_arquivo = new controller_arquivo();
//$controller_arquivo = new controller_principal();
$controller_arquivo->setConexao(TConexao::getInstance());
$usuario = controller_seguranca::getInstance()->identificarUsuario();

$__view_menu = new view_principal($controller_arquivo);

        if ($usuario != null)
            $usuario_logado = true;

  if (!$usuario_logado) {
  if ($_SERVER["HTTP_HOST"] == "www.noticiadorweb.com.br.mx" || $_SERVER["HTTP_HOST"] == "noticiadorweb.com.br.mx") {
    $_SESSION["SESSION_LANG"] = 'es-ES';
  } else  {
    $_SESSION["SESSION_LANG"] = 'pt-BR';
  }
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <?php header("Content-type: text/html; charset=UTF-8"); ?>
        <meta charset="UTF-8">
        
        <title>NoticiadorWeb 2.0</title>
        <? add_html_includes(); ?>

        <script type="text/javascript">

            $(document).ready(
				function(){

					$('.mensec').hide();
					$('.menpri').click(function(){
						$(".mensec:visible").slideUp("fast");
						$(this).next().slideDown("fast");
						return false;
					});
			 
					$('#idioma').click(function() {
						$('#bandera').slideToggle(function(){
					  
						});
					});

				}
			);
        
        </script>

    </head>
        <body>

            <div id="hd">
                <div id="hd_content">
                    <h1><a href="#"><span>Noticiadorweb</span></a></h1>
                    <div id="slogan">
			<?=translate('Um produto para ajudar na sua comunica&ccedil;&atilde;o.')?>
                    </div>
                    <div id="dividioma" style="display: none;">
                        <a href="#" id="idioma">Idioma<?
        if (isset($_SESSION["SESSION_LANG"])) {
            echo "  " . $_SESSION["SESSION_LANG"];
        } else {
            echo " Português";
        } ?><br /></a>
                    <div id="bandera">
                        <img id="brazil"src="Brazil.png"/> <a href="#" id="portugues">Portugues</a><br/>
                        <img id="spain" src="Spain.png"/> <a href="#" id="espanol">Espanol</a>
                    </div>
                </div>
<?
        if ($usuario_logado) {
            setlocale(LC_TIME, 'pt_BR');
            $hoje = strftime(" %A, %d de %B de %Y", time()); // segunda, 02 de fevereiro de 2008
            $_SESSION['remetente'] = $usuario->getNome();
?>		
                <!-- Caso seja uma área pública, não colocar o div#login_control -->
            <div id="login_control">

			Oi, <span class="name"><?= $usuario->getNome() ?></span>. <a href="index.php?action=logoff&amp;arquivo=<?= sha1("logoff") ?>" title="Sair"><img src="<?= DIR_LAYOUT ?>hd_logout_button.png" border="0" /></a><br />
                    <span class="date"><?
                switch ($usuario->getPerfil()) {
                    case 1:
                        $perfil = 'Administrador';
                        break;
                    case 2:
                        $perfil = 'Colaborador';
                        break;
                    case 3:
                        $perfil = 'Editor';
                        break;
                    case 4:
                        $perfil = 'Personalizado';
                        break;
                    case 5:
                        $perfil = 'Blogueiro';
                        break;
                }

                echo $usuario->getEmail() . " - " . $perfil; ?></span>

                </div>
                <? } ?>
            </div>
        </div>
            <div id="bd">
                <? if (!$usuario_logado) { ?>
                <div id="column_menu">
                    <ul id="navigation_menu">
                        <li class="home">
                            <a href="index.php">
                                <span><? translate("Home") ?></span>
                            </a>
                        </li>
                        <li class="button">
                            <a href="index.php?url=2">
                                <img src="<?= DIR_ICONS ?>arrow_right.png" width="20" height="20" />
                                <?= translate("Quem somos"); ?>
                            </a>
                        </li>
                        <li class="button">
                            <a href="index.php?url=3">
                                <img src="<?= DIR_ICONS ?>arrow_right.png" width="20" height="20" />
                                <?= translate("Produtos") ?>
                            </a>
                        </li>
                        <li class="button">
                            <a href="index.php?url=4">
                                <img src="<?= DIR_ICONS ?>arrow_right.png" width="20" height="20" />
                                <?= translate("Marketing de Relacionamento") ?>
                            </a>
                        </li>
                        <li class="button">
                            <a href="index.php?url=6">
                                <img src="<?= DIR_ICONS ?>arrow_right.png" width="20" height="20" />
                                <?= translate("Passo a passo de utilização/Manuais") ?>
                            </a>
                        </li>
                        <li class="button">
                            <a href="index.php?url=7">
                                <img src="<?= DIR_ICONS ?>arrow_right.png" width="20" height="20"/>
                                <?= translate("Relações de Clientes") ?>
                            </a>
                        </li>
                        <li class="button"><a href="index.php"><img src="<?= DIR_ICONS ?>arrow_right.png" width="20" height="20" /> <?= translate("Como adquirir") ?></a></li>
                        <li class="button"><a href="index.php"><img src="<?= DIR_ICONS ?>arrow_right.png" width="20" height="20" /> <?= translate("Fale conosco") ?></a></li>
                    </ul>

                    <form id="login_form" name="login_form" method="post" action="index.php">
<? add_action_data("processUser"); ?>		
                    <ol> 
                        <label for="login_user" class="login_label"><?= translate("Usu&aacute;rio") ?></label>
                        <li class="input">
                            <input type="text" name="txtLogin" id="login_user" />
                        </li>
                        <label for="login_pass" class="login_label"><?= translate("Senha") ?></label>
                        <li class="input">
                            <input name="txtSenha" type="password" id="login_pass" />
                        </li>
                        <li class="control">
                            <input type="submit" value="" class="botaoLogin"/><a href="index.php?<?= PARAMETER_NAME_ACTION . "=show&" . PARAMETER_NAME_FILE ?>=resetar_senha"><?= translate("Esqueci a senha") ?></a></li>
                        <li class="control"><a href="index.php?<?= PARAMETER_NAME_ACTION . "=show&" . PARAMETER_NAME_FILE ?>=colaborador"><?= translate("Colaborar") ?></a></li>
                    </ol>
                </form>
<? } else { ?>
                    
                        
<?
        if(checkapi ()){
            ?>
    
     <div id="column_menu" style="float:left;position: absolute;display:none;">
     <ul id="navigation_menu">
       <li class="home"><a href="index.php?api=1"><span></span></a></li><?
        $menuli= "<li class=\"button\"><a href='index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=noticia&api=1'><img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Inserir noticia a ser enviada")."</a></li>";
        $menuli.=  "<li class=\"button\"><a href='index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=assinante&api=1'><img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Inserir emails para receber News")."</a></li>";
        $menuli.=  "<li class=\"button\"><a href='index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=gestao_newsletter&api=1'><img src=\"".DIR_ICONS."arrow_right.png\" width=\"20\" height=\"20\" />".translate("Despachar a Newsletter")."</a></li>";
        echo $menuli;
        }else{
            ?>
               <div id="column_menu">
               <ul id="navigation_menu">
               <li class="home"><a href="index.php"><span>Home</span></a></li><?
                        if (isset($menu))
                        print $menu; else
                        print $__view_menu->obterMenu() ?>

               </ul>
       <a href="https://www.bisa.com.br" id="bisa"><span><?php echo translate("Bisa - Tecnologia de Informação"); ?></span></a>
<? } }?>
            <img src="<?=DIR_LAYOUT?>menu_end_bottom.png"/>
            </div>
       <?if (checkapi()){?>
       <div class="column_contentapi" >
         
 <?}else{?>
           <div id="column_content" class="index">
               <?}?>
          

<?php
    if(isset($_POST['enviar'])) {
        
        $nome = $_POST['nome'];
        $email = $_POST['email'];
        $texto = $_POST['texto'];
        
        $to = 'desenvolvimento7@bisa.com.br';
        $subject = 'Sugestão de texto para Marketing de Relacionamento';
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: André Teste <teste@bisa.com.br>' . "\r\n";
        
        $message = '<strong>Nome: </strong>'.$nome.'<br />
                    <strong>Email: </strong>'.$email.'<br />
                    <strong>Texto: </strong>'.$texto.'<br />';
            
        //Email::sendEmail($to,$subject,$message);
        //mail($to,$subject,$message,$headers); 
    }
?>
<script type="text/javascript">
	$(document).ready(function(){
        $('#frm_sugerir').hide();
        $('#link_sugerir').click(function(){
            $('#frm_sugerir').slideDown();
        });
        
        $('dd').hide();
		$('dt a').click(function(){
	        if(!$(this).parent().next().is(':visible')) {
	           $("dd:visible").slideUp();  
	        }            
			$(this).parent().next().slideToggle();
			return false;
		});
        
    });
</script>
<style type="text/css">
<!--
	#frm_sugerir {width: 450px; margin: 0 auto; margin-bottom: 10px;}
    #frm_sugerir label {float: left; display: block; width: 70px;}
    #frm_sugerir input {margin-top: 4px;}
    #frm_sugerir input[type="submit"] {float: right; margin-right: 20px;}
    #google_translate_element {float: right;}
    dt {display: block; width: 100%; height: 20px; background-color: #ccc; margin-bottom: 5px;}
    dt a {color: #000; font-weight: bold; text-decoration: none; font-size: 11px; line-height: 20px; margin-left: 7px; display: block; width: 100%;}
    dd {border: 1px solid #ccc; margin-bottom: 5px; margin-top: -5px; padding: 5px;}
    em {font-style: italic;}
-->
</style>

<div class="rounded_content_top"></div>
<div class="rounded_content_middle">
	<div class="rounded_content">
        <!--div id="google_translate_element"></div><script>
        function googleTranslateElementInit() {
          new google.translate.TranslateElement({
            pageLanguage: 'en',
            includedLanguages: 'es,pt',
            autoDisplay: false,
            layout: google.translate.TranslateElement.InlineLayout.SIMPLE
          }, 'google_translate_element');
        }
        </script><script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script-->
        
        <div id="google_translate_element"></div><script>
        function googleTranslateElementInit() {
          new google.translate.TranslateElement({
            pageLanguage: 'en',
            includedLanguages: 'pt,es',
            multilanguagePage: true,
            layout: google.translate.TranslateElement.InlineLayout.SIMPLE
          }, 'google_translate_element');
        }
        </script><script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        
        <div class="goog-trans-section" id="goog-trans-section"></div>
        
        <h1 class="default_title">e-Mail Marketing e Marketing de Relacionamento</h1><br />
        <p>O NoticiadorWeb é uma ferramenta de e-Mail Marketing que tem diversas funcionalidades que ampliam bastante o escopo do conceito de Marketing Direto, e nossa perspectiva é que seja criado um relacionamento entre a empresa e seus clientes e prospects, fazendo com que seja abraçado também o Marketing de Relacionamento através da Internet.</p>
        <p>Diante desse propósito, fizemos a seleção de alguns textos que orientam e reafirmam estratégias acerca do Marketing em Geral. O conteúdo deste material é de responsabilidade de seus criadores, mas são compartilhados pelos responsáveis pelo NoticiadorWeb.</p>
        <p>Caso queira sugerir algum texto a ser acrescido, <span id="link_sugerir">clique aqui</span>...</p>
        
        <div id="frm_sugerir">
            <form action="https://<?=$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]?>" method="post">
                <label>Nome: </label>
                <input type="text" name="nome" id="nome" size="80" /><br /><br />
                
                <label>Email: </label>
                <input type="text" name="email" id="email" size="80" /><br /><br />
                
                <label>Mensagem: </label>
                <textarea name="texto" id="texto" cols="55" rows="10"></textarea>
                
                <input type="submit" id="enviar" name="enviar" value="Enviar" />
                <div style="clear: both;"></div>
            </form>
        </div>
        
        <div>
            <dt><a href="#">Como Projetar o seu Boletim de Notícias</a></dt>
            <dd>
                <p>Um objetivo implícito de toda comunicação positiva é aumentar o nível de confiança entre o remetente e o receptor. Os boletins de notícias executam admiravelmente esta capacidade.</p>
                <p>Considere alguns fatores chaves que determinam se uma mensagem é aceita e compreendida pelo receptor:</p>
                 <p>- Eu conheço o remetente?<br />
                 - Eu pedi esta informação?<br />
                 - Esta informação me ajuda?<br />
                 - Esta informação é personalizada para mim?<br />
                 - Eu estou esperando informações adicionais deste remetente?</p>
                <p>O receptor de um boletim de notícias deve saber e reconhecer o remetente. O remetente deve ser uma pessoa, não apenas uma empresa ou associação, para aumentar o nível de confiança.</p>
                <p>Pense no termo "boletim de notícias": ele deve conter uma notícia, o que é uma informação nova ou apresentada pela primeira vez ao leitor. Mas deve também ser uma carta - uma comunicação pessoal em que a notícia é apresentada de uma maneira original, preenchendo diretamente as necessidades do grupo dos receptores. Este é o trabalho de um editor, e um trabalho que você pode executar com grande vantagem independentemente de seu título profissional ou responsabilidade anterior.</p>
                <p>Conselho prático: Ponha o título de seu boletim de notícias no campo "Subject". Ponha o nome do editor no campo "From:". Faça seu boletim de notícias ser percebido como sendo enviado por alguém real.</p>
                <p>Um boletim de notícias é um serviço de assinatura. Independentemente de ser grátis ou pago, se o contato inicial foi na forma de uma oferta ou de um pedido de um leitor que procurou ativamente pelo serviço, então o receptor pediu a informação. Isto coloca o remetente na posição vantajosa de estar cumprindo um pedido.</p>
                <p>Conselho prático: Seja consistente e solícito no sincronismo e na entrega de seu boletim de notícias. Se for semanal, envie-o no mesmo horário e no mesmo dia de cada semana. Seja coerente com o horário da entrega assim que possível. Envie uma mensagem para entregar o que você prometeu, da forma mais confiável possível.</p>
                <p>Seu trabalho como editor é de certificar-se que a informação distribuída é útil ao leitor. Você deve compreender suas necessidades. Você deve compreender o valor de seu tempo. Faça sua informação sucinta e atenda a necessidades identificadas de um público-alvo específico.</p>
                <p>Lembre que a informação é um produto. A notícia é a mesma se divulgada pela CNN, NBC, Globo, Time, Jornal Nacional, Veja ou pelas principais publicações especializadas da sua indústria específica. O valor de um boletim de notícias é sua função de destacar, interpretar e analisar os fatos que podem estar disponíveis em centenas de outras fontes. Esta é a personalização que você pode e deve fornecer a seus receptores.</p>
                <p>Finalmente, se você conseguir os pontos precedentes, seu leitor encontrará valor em seu boletim de notícias e já estará esperando o próximo. Mas se não for este caso, assegure-se de disponibilizar um procedimento de cancelamento da assinatura que seja visível e fácil de usar.</p>
                <p>Além da função óbvia de fornecer uma saída para aqueles que não estão interessados,a opção de cancelamento transmite uma mensagem de sua confiança no valor das informações que está distribuindo e sua sensibilidade em relação à realidade da sobrecarga de informação a que a maioria das pessoas está exposta.</p>
            </dd>
           
            <dt><a href="#">Para que fazer um Boletim Eletrônico e como o NoticiadorWeb pode ajudá-lo</a></dt>
            <dd>
                <p><strong>Marketing Direto por e-mail</strong><br />
                é possível enviar mensagens totalmente personalizadas para cada um dos seus destinatários.</p> 
                <p><strong>Velocidade e Estatísticas de Abertura</strong><br />
                é possível fazer a entrega de milhares de mensagens em poucos minutos com acompanhamento de aberturas e clicks por hora, por região, faixa etária, nível socioeconômico e diversos outros filtros.</p> 
                <p><strong>Divulgação de Produtos e Serviços</strong><br />
                é possível enviar mensagens para divulgação de produtos e serviços com acompanhamento dos pontos que mais interessaram aos destinatários.</p>
                <p><strong>Pesquisa de Opinião</strong><br />
                é possível realizar pesquisa de opinião e conhecer melhor o perfil de seus clientes através de pesquisa por assuntos ou áreas de interesse.</p>
                <p><strong>Gerenciamento de Campanhas de Email Marketing (CRM)</strong><br />
                é possível através dos emails enviados e recursos de filtragem de dados, criar e gerenciar campanhas personalizadas de acordo com o perfil de cada cliente. Conhecer melhor e estreitar as relações com seus clientes é a chave para fidelização.</p> 
                <p><strong>Filtragem de dados para Pré-venda, Tele-marketing e abordagem direta</strong><br />
                é possível filtrar grande volume de clientes para uma segunda abordagem comercial mais direcionada. Com isso ocorre uma redução significativa no custo de uma 1ª abordagem comercial e uma seleção dos clientes interessados para um 2º contato.</p>
                <p><strong>Recebimento documentado de mensagens</strong><br />
                A cada abertura ou clique de sua mensagem você terá a informação do email, dia, hora, minuto e IP que abriu essa mensagem. Não haverá mais como seu destinatário dizer "Não recebi sua mensagem". Está tudo documentado.</p>
            </dd>
            
            <dt><a href="#">WebMarketing</a></dt>
            <dd>
                <p><em>por Paulo Roberto Kendzerski</em></p>
                <p>Webmarketing ou marketing digital é uma das expressões mais utilizadas nos últimos anos. Mas, afinal, o que quer dizer webmarketing? Antes de buscarmos a definição exata, precisamos entender a evolução da comunicação no mundo.</p>
                <p>Em 1990, os meios de comunicação eram telex, carta enviada pelos correios, TV aberta, telefone fixo, fax, vídeos VHS e cartão de visita. Dez anos depois, os meios de comunicação passaram a ser celular, e-mail, PowerPoint, CDs, internet, torpedos, TV a cabo e callcenter. Agora, em menos de quatro anos, a evolução na comunicação nos coloca frente a frente com celular MMS, DVD, sites inteligentes, CD-CARD, SMS marketing, e-mail marketing, VoIP, vídeos digitais, wi-fi, bluetooth, video on demand, ITV, newsletter e e-paper.</p>
                <p>Se perguntarmos para um profissional de comunicação da velha guarda, ele irá citar os aparelhos tradicionais como telex, fax e carta de apresentação. Talvez alguns atécitem celular e e-mail. Agora, perguntem para os "novos" profissionais. Surgirão nomes como Skype, Messenger, Orkut, celular multimídia, SMS marketing, e-mail marketing....</p>
                <p>Se você prestar atenção, irá perceber que a maioria dos meios de comunicação citados como atuais estão vinculados é internet. Verá que a comunicação deixou de ser através de um contato físico para ser por meio de presença virtual.</p>
                <p>As empresas que desenvolveram seu webmarketing de forma eficiente aliaram a essa possibilidade de presença em todos os locais que a ela possa interessar mobilidade e interatividade. Essas duas palavras expressam a principal finalidade de uma empresa para obter sucesso através do webmarketing.</p> 
                <p>Webmarketing são todas as ações feitas através da internet que visam:<br />
                - Ampliar os negócios da empresa (obtendo mais informações dos clientes atuais e prospectando novos clientes).<br />
                - Desenvolver campanhas de relacionamento digital com seu público-alvo.<br />
                - Fortalecer sua marca no mercado.<br />
                - Gerar negócios, on-line ou off-line.</p>
                <p>Como exemplo de vantagens do webmarketing em relação ao marketing tradicional, podemos citar:<br />
                - Alcance.<br />
                - Agilidade.<br />
                - Custo.<br />
                - Interatividade.</p>
                <p>Mas para uma empresa obter sucesso na sua estratégia de webmarketing é preciso planejar de forma eficiente suas ações. Consideramos quatro ações básicas para que essa estratégia alcance o resultado desejado:<br />
                1. Posicionar o site de forma eficiente nos mecanismos de busca.<br />
                2. Oferecer conteúdo qualificado (referencial e atualizado).<br />
                3. Segmentar seu público-alvo pelo perfil de comportamento e/ou áreas de interesse.<br />
                4. Desenvolver uma frequência ativa no relacionamento digital com o mercado.</p>
                <p>Aliado à possibilidade de medir todos os resultados de qualquer ação de webmarketing, temos aqui a verdadeira evolução na forma de comunicação das empresas. O resultado prático dessas ações será um site com excelente audiência de um público segmentado e qualificado, satisfeito com as informações que a sua empresa oferece.</p>
                <p>Mas cuidado! Webmarketing são ações feitas exclusivamente através da internet. Se você olhar para a rede verá que muito poucas empresas utilizam o webmarketing para atingirem o sucesso no mundo virtual.</p>
                <p>A grande maioria investe fora da rede para atrair potenciais clientes. Para nós, trata-se de um grande erro e um enorme desperdício de oportunidades. Se todo mundo sabe que na internet o investimento é menor e que todas as ações podem ser mensuradas, porque não explorarem esta possibilidade?</p>
                <p>A resposta, se me permitem a ousadia, está clara como a água que bebemos todos os dias. A internet é uma criança de apenas dez anos de idade no Brasil. E os profissionais que comandam as empresas, as agências de publicidade, os veículos de comunicação ainda estão atrelados aos antigos meios de comunicação. Tiveram sua formação profissional antes da internet. Não se reciclaram, não evoluíram digitalmente. Muitos mal sabem enviar e-mails. Que dirá fazer e-mail marketing ou criar campanhas digitais de relacionamento.</p>
                <p>Dessa forma, acabam perdendo oportunidades, vendo empresas de sucesso surgirem do nada e profissionais conquistando espaço no mundo dos negócios. A diferença é que esses conseguiram entender como funciona o webmarketing e estão aproveitando ao máximo esta nova forma de comunicação.</p>
                <p>Enquanto isso, outras empresas continuam atreladas ao fax, ao telefone, às visitas comerciais. Pense no webmarketing como uma evolução na forma de comunicação da sua empresa e o sucesso será alcançado.</p>                
            </dd>
            
            <dt><a href="#">Boas maneiras (normas de e-mail marketing da Abemd)</a></dt>
            <dd>
                <p>A Associação Brasileira de Marketing Direto (Abemd) elaborou esta relação de "Boas maneiras" para contribuir na estruturação de ações de e-mail marketing. Trata-se de uma série de recomendações que conduzem a uma utilização ética, pertinente e responsável do e-mail como ferramenta de marketing. Estas orientações se fundamentam no respeito aos destinatários das ações e, também, no uso adequado da internet, o que certamente contribuirá para as empresas alcançarem os resultados desejados e construírem um relacionamento sólido e de confiança mútua com clientes e prospects.</p>
                <p><strong>ética.</strong><br />
                Atuar dentro do Código de ética da ABEMD, que conceitua detalhadamente as boas práticas no Marketing Direto.</p>
                <p><strong>Opt in.</strong><br />
                O primeiro recebimento é muito importante, porque marca o início da relação. é preciso ter permissão para prosseguir o relacionamento, por meio do opt in do receptor, tanto quando ele procura como quando é procurado. Quando é a pessoa quem procura a empresa, o campo onde é feita a opção pelo recebimento da mensagem deve estar visível e com descrição clara do produto ou serviço oferecido. Quando é a empresa quem procura a pessoa, tratando-se do primeiro contato deve-se informar como foi possível chegar a ela, explicitar o produto ou serviço oferecido e apresentar de forma visível a alternativa opt in. Se a pessoa não responder o e-mail com essa alternativa assinalada, deve-se entender que não deseja receber novas mensagens.</p>
                <p><strong>Opt out.</strong><br />
                Toda mensagem precisa ter opt out. é prerrogativa do receptor decidir o momento em que não quer mais receber mensagens de determinado emissário.</p>
                <p><strong>Uso do endereço eletrônico.</strong><br />
                Quando houver cadastro prévio, deve ficar claro que o endereço eletrônico poderá ser utilizado para o envio de mensagens comerciais, ou seja, na geração de leads próprios ou, se for o caso, repassado também com a finalidade de envio de mensagens comerciais. E o receptor deve manifestar sua concordância com isso.</p>
                <p><strong>Tamanho dos arquivos.</strong><br />
                Procure sempre limitar o tamanho dos arquivos enviados, seja no corpo das mensagens ou nos anexos. Deve-se ter sempre em mente o público da mídia inferior em capacitação tecnológica (software, hardware e modalidade de conexão). Sugere-se mensagens no formato txt ou html, este último com tamanho máximo de 12 KB, e que as figuras (gif's) não estejam anexadas na mensagem, mas sim localizadas em servidor próprio.</p>
                <p><strong>Auto-executáveis.</strong><br />
                Não devem ser enviados arquivos com auto-funcionamento. Os auto-executáveis são arquivos que os programas gerenciadores de e-mail conseguem ler e interpretar, iniciando automaticamente algum processo que não é necessariamente desejado pelo receptor. Essa modalidade de arquivo também torna o sistema vulnerável à transmissão de vírus (voluntária ou não).</p>
                <p><strong>Relevância.</strong><br />
                O consumidor não se incomoda em receber uma mensagem de cunho comercial, desde que seja relevante para ele. Portanto, preocupe-se sempre com o conceito de relevância.</p>
                <p><strong>Frequência.</strong><br />
                Deve-se preferencialmente oferecer ao cliente que assinale a opção de sua preferência na frequência de recebimento de informações ou solicitar que ele opte entre as diversas alternativas de periodicidade que lhe são oferecidas. Quando não for possível oferecer que faça a opção, deve-se deixar claro qual a frequência de envio das mensagens.</p>
                <p><strong>Política de relacionamento.</strong><br />
                é sempre conveniente que se tenha clareza na política de relacionamento adotada, o que pode ser feito por meio de um contrato/compromisso assumido formalmente com o consumidor.</p>
            </dd>
            
            <dt><a href="#">O Melhor Horário para Enviar Email Marketing</a></dt>
            <dd>
                <p>Existem sempre estudos que nos indicam os momentos mais propícios para enviar conteúdos para os clientes, já vi matérias falando a respeito de melhor horário para: postar no Twitter, publicar conteúdo no Facebook, publicar um artigo no Blog, entre outras tantas variantes, só existe um único diferencial desses canais que é interação por demanda, no caso do email marketing a interação é demanda por anuência prévia.</p>
                <p>Analisando o email marketing como um canal, primeiramente chegamos à conclusão que em questão de ROI (retorno sobre investimento) é o mais alto de todos, porém essa singularidade só é alcançada quando a utilização dessa via de marketing é empregada corretamente. Por conta das propagandas com informações que são sempre anunciadas, onde o email marketing funciona melhor que qualquer outra forma de marketing digital e o investimento pode até ser praticamente zero, é o que faz os números de SPAMs crescerem absurdamente, e pior ainda cada vez criar uma descrença por parte dos clientes no canal.</p>
                <p>Em nossos emails da DragonSoft recebemos a média diária de 3000 emails considerados SPAMs, e porque mantermos vários apelidos, podemos verificar a larga quantidade de disparos indiscriminados, ou seja, recebemos muitas vezes dezenas de emails iguais somente com o destinatário alterado, e o mais incrível ainda, muitas vezes são criadas mutações de nossos endereços sem mesmo nunca terem existido. E indo além, essa prática está totalmente indiscriminada até pelos grandes lojistas virtuais, que estão utilizando através de "terceiros" e lotando as caixas postais com propaganda indesejada.</p>
                <p>É importante esclarecer que o email marketing só atinge sua marca como melhor canal de marketing quando utilizado com base de endereços eletrônicos opt-in (cadastramento), caso contrário, é mais um SPAM. E pensando um pouco funcionalmente que haja possibilidade de configurar o intervalo desses recebimentos, porque muitas vezes o ato de assinar uma lista de newsletter não é consentimento para recebimento de um email por dia.</p>
                <p>Com isso chegamos à conclusão que o melhor horário para enviar email marketing é simplesmente na hora em que o solicitante quer e não numa segunda-feira às 9:30h da manhã. E claro desmitificar que email marketing não é mina de ouro para as lojas virtuais, a mina de ouro são os clientes bem atendidos e satisfeitos, porque estes é que farão alavancagem da loja virtual através de comentário positivos, compartilhamento de informações e principalmente defensores da marca, nos mais diversos canais digitais.</p>
            </dd>

            <dt><a href="#">Anti-spam: Claves para enviar correos siempre deseados</a></dt>
            <dd>
                <p>Kirill Popov y Loren McDonald, dos expertos de la red Clickz, firman un inteligente artículo sobre el problema al que se enfrentan los profesionales del marketing online y su herramienta clave, el email: ser clasificado como spam. En el artículo se refieren al problema de ser clasificado como spam por el proveedor de servicios de Internet (ISP). Las quejas relativas a los envíos indeseados son el principal problema relacionado con la "enviabilidad" (deliverability) de los mensajes a través de los principales ISP, por delante de los problemas de contenido y codificación o las prácticas opt-in (el 25% de los profesionales del marketing encuestados por estos expertos considera que este es el principal problema).</p>
                <p>Los ISP suelen bloquear envíos cuando el 1%-3% de los receptores de un envío decide clasificarlo como spam. Si los emailings realizados siguen prácticas de permiso, ofrecen opt in y opt out y en general se atienen a las buenas prácticas, pero a pesar de eso siguen siendo clasificados como spam por el ISP, lo que ocurre es que hay un problema de relación con los destinatarios.</p>
                <p>Ante una situación de alta tasa de quejas o etiquetado spam en envíos basados en el permiso, Popov y McDonald recomiendan fijarse en varios elementos de los emails. Personalmente, aunque no se tengan problemas relativos a ser clasificado como spam, recomiendo esta serie de consejos a todo buen emailer:</p>
                <p><strong> - Están ocultos los enlaces de unsubscribe/desuscribir/opt out? ¿Se dan instrucciones claras para borrarse de la lista?</strong> Es mejor no esconder estos datos en forma de letra pequeña, ni complicar el proceso con confirmaciones o claves. Como mucho el proceso debe constar de dos pasos.</p>
                <p><strong> - Es muy antigua la lista a la que se manda el emailing?</strong> Si se envían emailings con poca frecuencia (de año en año), muchos de los inscritos puede que no reconozcan la marca ni se acuerden de por qué una vez se suscribieron. Si no mantienen una relación cercana con la marca, probablemente envíen el mensaje a la papelera de spam.</p>
                <p><strong> - Datos poco reveladores en el remitente y en el asunto del mensaje?</strong> Si se envía un email desde una dirección poco específica o de un servidor gratuito (@hotmail, @gmail, @ya") y además el asunto es demasiado general ("Para su información"?), el mensaje parecerá un spam. Para clasificar un mensaje como spam, lo primero en lo que se fija un usuario es en el remitente y lo segundo es el asunto del mensaje. Si la marca no es muy conocida, el producto o servicio es algo controvertido, o se hacen pocos envíos, se puede intentar poner la marca en el asunto del mensaje.</p>
                <p><strong> - Se ha dejado al usuario que exprese sus preferencias?</strong> Hay quien prefiere recibir los mensajes en formato texto en lugar de html con gráficos, o una frecuencia menor, o informarse sólo sobre una parte de la oferta. Si no se dio opción a elegir el formato en el momento de suscribirse, es conveniente facilitar al usuario una hoja de preferencias.</p>
                <p><strong> - Aparece en el mail la dirección del usuario?</strong> Muchas veces, por despiste, los usuarios se suscriben a una lista en distintas ocasiones utilizando diferentes direcciones de email. Si en un momento se borran de la lista pero siguen recibiendo mensajes a través de la otra dirección, probablemente se quejen. Para evitar esto, es mejor explicitar claramente en el mensaje la dirección del destinatario, para que no haya lugar a dudas. El usuario verá que también tiene que borrar esa otra dirección de email.</p>
                <p><strong> - Envíos sobre los que el usuario no se ha expresado?</strong> Hay que dejar claro en la suscripción lo que se enviará y con qué frecuencia se hará. Y después hay que cumplir lo dicho. No hay que enviar nada que los destinatarios no esperen recibir, por mucho que se crea que va a ser de su gusto. El email no es lugar para adivinar lo que pueda gustar a los usuarios.</p>
                <p><strong> - Se tarda mucho en borrar a los usuarios?</strong> Según la ley CAN-SPAM por la que se rigen los profesionales del marketing en Estados Unidos, sólo se puede tardar diez días en borrar a un usuario de una lista tras su petición. En España no se especifica este extremo en la ley contra el spam (no es una ley específica contra el spam, sino la Ley 34/2002 de 11 de julio, de servicios de la sociedad de la información y de comercio electrónico), aunque si se hace referencia en el artículo 11 (hasta donde yo sé), pero si se dice en la Ley de Protección de Datos, artículo 16 sobre el derecho de cancelación y rectificación de datos que "El responsable del tratamiento tendrá la obligación de hacer efectivo el derecho de rectificación o cancelación del interesado en el plazo de diez días." (AGPD) Yo personalmente recomiendo que se utilice alguna aplicación que borre automáticamente a los usuarios que lo soliciten. Recibir un mail no deseado después de haberse borrado del servicio es de lo más molesto.</p>
                <p><strong> - No se hace seguimiento de las quejas por spam?</strong> Es preferible seguir este asunto con cuidado y borrar de la lista a cada usuario que lo solicite o que clasifique los envíos como spam, aunque en su día se apuntara a la lista.</p>
                <p>Información legal y de interés sobre el spam: Malasartes y Agencia Española de Protección de Datos.</p>
                <p>Fonte: <a href="https://www.territoriocreativo.es/etc/2006/06/anti_spam_clave.html" target="_blank">Território Criativo (Espanha)</a></p>
            </dd>
            
            <dt><a href="#">The email marketing success checklist</a></dt>
            <dd>
                <p>Emails and newsletters to opt-in audiences are still working. What's not working as well are "cold" emails to rented or purchased lists, long-winding emails without a clear offer or call to action. Here is a list of things you should check:</p>
                <p><strong>Segmentation and targeting is everything</strong><br />
                Any communication is only successful if the message is target towards your audience. For different audiences you need different emails, content, offers and call to action. Even within one segment, try A/B split testing: 50% get subject line A and the other 50% get subject line B, etc.</p>
                <p><strong>Subject line is key</strong><br />
                Large percentage of email marketing success is driven by the subject line. The subject decides if the email gets opened or not, or worse, goes directly into the spam folder:<br />
                <p> - Make it short (50 characters or less)<br />
                    - Create curiosity<br />
                    - Match with your actual email content and offer</p>
                <p><strong>Short and simple</strong><br />
                Nobody wants to read long texts or emails (anymore). More and more people scan texts, they don't read. Make it easy and quick to get the essence of your email.<br />
                <p> - Short (No scrolling required)<br />
                    - Use bullets (versus paragraphs of lots of text blocks)<br />
                    - Use images to get attention or emotional effects<br />
                    - Clean, simple and professional design</p>
                <p><strong>An offer they can't resist with a clear call to action</strong><br />
                Probably the second most important driver for success is an attractive offer (what do I get?) targeted to your specific audience and a clear call to action (how do I get it?) in the email.<br />
                <p> - Sign up, register, or download<br />
                    - Buy now, free trial - special offer<br />
                    - Get a chance to win</p><br />
                <p><strong>Landing pages for a "safe" landing</strong><br />
                The goal of a landing page is to "land" people and don't let them take off without having signed up, registered or purchased something. Here is how:<br />
                <p> - Deliver what you have promised in the email<br />
                    - Don't distract with other offers, navigation, etc<br />
                    - Ask the minimum amount of information you need<br />
                    - Make is easy and obvious (buttons, etc.)<br />
                    - Simple and clean design of page</p>
                <p><strong>Measure success and always be testing</strong><br />
                Email marketing has many variables such as, time of day sent, day of the week, subject line, offers, design, etc. If possible, test different subjects lines, offers, layouts, and optimize your email campaigns over time. Here are the key metrics:<br />
                <p> - How many emails were delivered (did not bounce back)<br />
                    - How many were opened (use HTML to track)<br />
                    - How many clicked-through to the landing page<br />
                    - How many actually "accepted" the offer<br />
                    - Cost per lead/sales</p>
            </dd>
            
            
            
        </div>
        
   </div>
</div>	
<div class="rounded_content_bottom"></div>
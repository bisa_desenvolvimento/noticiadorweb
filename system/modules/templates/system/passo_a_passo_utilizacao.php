
<script type="text/javascript">
	$(document).ready(
	function() {
		$('#frm_sugerir').hide();
		$('#link_sugerir').click(
		function() {
			$('#frm_sugerir').slideDown();
		});
		$('dd').hide();
		$('dt a').click(
		function() {
			if (!$(this).parent().next().is(':visible')) {
				$("dd:visible").slideUp();  
			}            
			$(this).parent().next().slideToggle();
			return false;
		});
	});
</script>
<style type="text/css">
<!--
	#frm_sugerir {width: 450px; margin: 0 auto; margin-bottom: 10px;}
	#frm_sugerir label {float: left; display: block; width: 70px;}
	#frm_sugerir input {margin-top: 4px;}
	#frm_sugerir input[type="submit"] {float: right; margin-right: 20px;}
	#google_translate_element {float: right;}
	dt {display: block; width: 100%; height: 20px; background-color: #ccc; margin-bottom: 5px;}
	dt a {color: #000; font-weight: bold; text-decoration: none; font-size: 11px; line-height:20px;margin-left: 7px; display: block; width: 100%;}
	dd {border: 1px solid #ccc; margin-bottom: 5px; margin-top: -5px; padding: 5px;}
	em {font-style: italic;}
-->
</style>
<div class="rounded_content_top"></div>
<div class="rounded_content_middle">
	<div class="rounded_content">
		<!--div id="google_translate_element"></div><script>
        function googleTranslateElementInit() {
          new google.translate.TranslateElement({
            pageLanguage: 'en',
            includedLanguages: 'es,pt',
            autoDisplay: false,
            layout: google.translate.TranslateElement.InlineLayout.SIMPLE
          }, 'google_translate_element');
        }
        </script><script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script-->
        
		<div id="google_translate_element"></div><script>
			function googleTranslateElementInit() {
				new google.translate.TranslateElement({
				pageLanguage: 'en'
				, includedLanguages: 'pt,es'
				, multilanguagePage: true
				, layout: google.translate.TranslateElement.InlineLayout.SIMPLE
				}, 'google_translate_element');
			}
		</script><script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        
		<div class="goog-trans-section" id="goog-trans-section"></div> 
		<h1 class="default_title">Passo a passo de utilização</h1><br />
		<p> As instruções abaixo demonstram como utilizar as ferramentas e os recursos do noticiador web.</p>
		<div id="frm_sugerir">
			<form action="https://<?=$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]?>" method="post">
				<label>Nome: </label>
				<input type="text" name="nome" id="nome" size="80" /><br /><br />
				<label>Email: </label>
				<input type="text" name="email" id="email" size="80" /><br /><br />
				<label>Mensagem: </label>
				<textarea name="texto" id="texto" cols="55" rows="10"></textarea>
				<input type="submit" id="enviar" name="enviar" value="Enviar" />
				<div style="clear: both;"></div>
			</form>
		</div>
		<div>
			<dt><a href="#">Noticias</a></dt>
			<dd>
				<p><strong>Como cadastrar uma noticia utilizando o sistema do NoticiadorWeb</strong></p>
				<p><b>1º Passo:</b> Logar como colaborador, editor ou administrador para cadastrar uma 
				noticia.</p>
				<p><b>2º Passo:</b> Após ter logado com qualquer usuário, clicar na aba <b>NOTICIA</b> e
				clicar na sub aba <b>CADASTRAR NOTICIA</b>.
				<p><b>3º Passo:</b> Por um titulo para sua noticia, por um resumo para sua noticia, 
				por sua noticia por completa e por a fonte (Link) da sua matéria completa. 
				Após isso, selecione as áreas desejadas para sua noticia e clique em <b>ENVIAR</b>.</p>
				<br/>
				<p><strong>Como gerenciar sua noticia utilizando o sistema do NoticiadorWeb</strong></p>
				<p><b>1º Passo:</b> Pesquisar sua noticia por uma palavra chave, data da noticia 
				cadastrada e por suas áreas afins, em seguida apertar em <b>BUSCAR</b>.</p>
				<p><b>2º Passo:</b> Achando sua noticia você terá varias opções a usar como:<br/>
					- Editar sua notícia.<br/> 
					- Adicionar um comentário.<br/>
					- Alterar as áreas da sua notícia.<br/>
					- Excluir sua noticia e Compartilhar sua notícia.
				</p>
			</dd>
			<dt><a href="#">Assinantes e E-mails</a></dt>
			<dd>
				<p><strong>Como Cadastrar um assinante utilizando o sistema do NoticiadorWeb</strong></p> 
				<p><b>1º Passo:</b> Clicar na sub aba <b>CADASTRAR ASSINANTE</b>. 
				Escolher e marcar a caixinha da área do interesse de seus assinantes.</p>
				<p><b>2º Passo:</b> Escrever os E-mails dos assinantes LINHA POR LINHA sem separadores ou 
				caracteres inválidos, com um limite de 10 mil E-mails por vez. Em seguida clicar 
				em <b>ENVIAR</b>.</p>
				<br/>
				<p><strong>Como gerenciar um assinante utilizando o sistema do NoticiadorWeb</strong></p>
				<p>Clicando em <b>GERENCIAR ASSINANTE</b>, irá exibir duas abas: 
				<b>PESQUISAR POR E-MAIL</b> e <b>PESQUISAR POR ÁREAS</b>.</p>
				<p><b>1º Passo:</b> Pesquisar por  E-mails. Inserir os e-mails dos assinates 
				e logo em seguida clicar em <b>PROCURAR</b>.</p>
				<p><b>2º Passo:</b> Pesquisar por áreas. Em <b>LISTAR E-MAILS POR ÁREA</b>, selecionar 
				a área desejada e  logo em seguida clicar em <b>PROCURAR</b>.</p>
			</dd>      
			<dt><a href="#">Newsletter (Jornal Eletrônico online)</a></dt>
			<dd>
				<p><strong>Como despachar uma newsletter utilizando o sistema do NoticiadorWeb</strong></p>
				<p><b>1º Passo:</b> Selecionar como a noticia vai ser enviada
				<br/>
				Matéria completa
				<br/>
				Resumos + link "Saiba mais" (MAIS RECOMENDADO)
				<br/>
				Usar links de matéria no começo da newsletter
				<br/>
				</p>
				<p><b>2º Passo:</b> Escolher qual vai ser a layout da pagina e logo em 
				seguida clicar em <b>AVANÇAR</b>.</p>
				<p><b>3º Passo:</b> Selecionar título da notícia, ao selecionar nas caixinhas as noticias 
				desejadas, automaticamente irão ser numeradas em ordem numérica, 
				logo em seguida clicar em <b>AVANÇAR</b>.</p>            
				<p><b>Último Passo:</b> Marcar a caixinha "áreas conhecidas" e em seguida 
				selecionar as as áreas que sua noticia irá ser exibida ou listar os e-mails 
				a serem enviados e clicar em <b>ENVIAR</b>.</p>
			</dd>
			<dt><a href="#">Dados Cadastrais (Perfil do noticiadorweb cadastrado)</a></dt>
			<dd>
				<p>área destinada para palterações de perfis de usuários cadastrados no 
				noticiadorweb, tais como: Email, Senha, Nome, Bairro, Cep e etc.</p>
			</dd>
			<dt><a href="#">Usuário</a></dt>
			<dd>
				<p><strong>Como cadastrar um usuário utilizando o sistema do NoticiadorWeb</strong></p>
				<p><b>1º Passo:</b> Clicar na aba <b>USUÁRIO</b> no NoticiadorWeb, logo em seguida 
				clicar na sub aba <b>CADASTRAR USUÁRIO</b>.</p>
				<p><b>2º Passo:</b> Preencher o formulário com todas as informações necessárias e logo em 
				seguida clicar em <b>CONTINUAR</b>.</p>
				<p><strong>Como gerenciar um usuário utilizando o sistema do NoticiadorWeb logado</strong></p>
				<p><b>1º Passo:</b> Clicar na aba <b>USUÁRIO</b> no noticiadorWeb, logo em 
				seguida clicar na sub aba <b>GERENCIAR USUÁRIO</b>.</p>
				<p><b>2º Passo:</b> Localizar o usuário desejado por E-mail e fazer as alterações 
				dentre os perfis:</p>
				Administrador<br/>
				Editor<br/>
				Colaborador<br/>
				E logo em seguida clicar em <b>ENVIAR</b>.
			</dd> 

			<dt><a href="#">Manuais</a></dt>
			<dd>
				<p><strong>Manual da API de notícias</strong></p>
				<p><a href="https://noticiadorweb.com.br/plugins/noticiadorweb/apidocs/doc.html">Manual</a>.</p>
				<br>

				<p><strong>Widget Wordpress</strong></p>
				<p><a href="https://noticiadorweb.com.br/plugins/noticiadorweb/apidocs/widget.html">Manual</a>.</p>				
			</dd>                       
		</div>        
	</div>
</div>
<div class="rounded_content_bottom"></div>
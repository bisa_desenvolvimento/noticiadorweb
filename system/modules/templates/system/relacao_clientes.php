<style type="text/css">
  .container-cliente img { border: 2px solid #FFF; vertical-align: middle; }
  .container-cliente {
    border: 2px solid #E3E1E1;
    background-color: #FFF;
    margin: 3px;
    transition: .7s;
    cursor: default;
    padding: 5px;
    display: inline-table;
    text-align: center;
    vertical-align: middle;
  }
  .container-cliente:hover { border: 2px solid #999; }
  .image { width: 150px; height: auto; }
  .no-image { 
    width: 150px;
    height: 35px;
    vertical-align: middle;
    text-transform: uppercase;
    line-height: 10px;
  }

  .title { padding-top: 5px; display: inline-block; text-transform: uppercase; }

  hr {
    border: 0;
    height: 1px;
    width: 100%;
    background: #333;
    background-image: linear-gradient(to right, #EBEBEB, #EBEBEB, #EBEBEB);
  }
</style>

<div class="rounded_content_top"></div>
<div class="rounded_content_middle">
  <div class="rounded_content">
    <h1 class="default_title"><?=translate("Portfólio")?></h1><br>
    <span class="container_clientes" align="center">
      <?= $view->clientes_noticiador(); ?>
    </span>
    <br>
  </div>
</div>
<div class="rounded_content_bottom"></div>

<!-- IkaroSales -->
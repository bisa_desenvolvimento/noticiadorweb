<div class="rounded_content_top"></div>
<div class="rounded_content_middle">
	<div class="rounded_content">
    
        <h1 class="default_title">Produtos</h1><br />
    
        <p><?=translate("Os  principais produtos da BISA Tecnologia de Informação são:")?></p>
        <ul>
          <li><?=translate("NoticiadorWeb (Newsletters): Trata-se  de um serviço que simplifica o trabalho de criar uma comunicação com seus  clientes e público alvo, criando uma relação de informação e divulgação das  atividades da empresa.")?></li>
        </ul>
        <ul>
          <li><?=translate("Bisa Juris (Advogados): Solução completa para escritários de advocacia,  que é composto pelos produtos Advogado2000 (Gestão de Processos), JurídicoWeb  (Consulta de Andamentos na Internet) e Robô Jurídico (Busca automatica dos  andamentos de processos nos sites dos tribunais)")?></li>
        </ul>
        <p>&nbsp;</p>
        <ul>
          <li><?=translate("Bisa Moneta (Gestão Financeira): Pacote integrado de sistemas voltado  para área financeira. Contas2000 (Contas a Pagar), Receb2000 (Contas a Receber),  Finanças2000 (Controle Bancário e de Caixa) e Contab2000 (Contabilidade  Gerencial e Orçamentária)")?></li>
        </ul>
        <ul>
          <li><?=translate("Bisa Filiato (Sindicatos, OAB, Parlamentares): Cadastro de Sócios,  Conv&ecirc;nios, Planos de Sa&uacute;de, Jurídico, Planejamento Estrat&eacute;gico, Pesquisa de  Opinião, etc.")?></li>
        </ul>
        <p>&nbsp;</p>
        <p><?=translate("Para  conhecer mais sobre os produtos e serviços da BISA Tecnologia de Informação,  acesse o site: ")?><a https://ttp://www.bisaweb.com.br/sistemas"  target="_blank">www.bisaweb.com.br/sistemas</a></p>

  </div>
</div>	
<div class="rounded_content_bottom"></div>
<?=add_html_head() ?>	
<div class="rounded_content_top">
	&nbsp;
</div>
<div class="rounded_content_middle">
	<div class="rounded_content">
	 <h1 class="default_title"><?=translate("Resetar Senha")?></h1>
   
   <script type="text/javascript">
      function validarForm(obj) {
        if (!validarTexto(document.getElementById("txtEmail"))) {
        	alert("<?=translate('Digite seu email')?>");
        	return false;
        }
        
        return true;
      }
      
      function validarTexto(obj) {
        if (obj.value.length > 0)
        	return true;
        
        return false;
      }
   </script>
   
   <p><?=translate("Para sua seguran�a e privacidade as senha salvas em nosso sistema s�o criptografadas de forma que n�o podem ser recuperadas. Ser� necess�rio redefinir sua senha, para continuar digite o e-mail cadastrado no seu usu�rio utilizado para acessar o site.")?></p>
   
   <form method="post" action="<?=$view->add_form_process_url(); ?>" onsubmit="return validarForm(this);">
    <? add_action_data("save", null, $view->getID()); ?>
    
		<label for="txtEmail">Email:</label>
		<input type="text" name="txtEmail" id="txtEmail" maxlength="100" size="75" />
    
    <input type="submit" value="Enviar" />
   </form>
   
	</div>
</div>		
<?=add_html_tail() ?>
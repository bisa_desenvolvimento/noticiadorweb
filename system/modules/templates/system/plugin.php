<div class="rounded_content_top"></div>
<div class="rounded_content_middle">
	<div class="rounded_content">
    
        <h1 class="default_title">Plugin de Not�cias</h1><br />
        
        <p>O novo script de not�cias din�micas do NoticiadorWeb traz uma gama de poss�veis configura��es para poder assim ser integrado de forma mais homog�nia. 
    Para utilizar o novo script do NoticiadorWeb � necess�ria a utiliza��o da biblioteca jQuery e o plugin do NoticiadorWeb.</p>
    <p>Leia abaixo as instru��es de como configurar o novo script.</p>
    <p>Para utilizar o novo plugin din�mico do NoticiadorWeb deve-se primeiro criar uma <code>&lt;div&gt;</code> vazia na p�gina e a mesma deve possuir um id.</p>
    <p><code>&lt;div id="id"&gt;&lt;/div&gt;</code></p>
    <p>Depois s� chamar o plugin do NoticiadorWeb nesse id. Segue abaixo um exemplo de chamada ao plugin do noticiador.</p>
    <pre><code>
    $('#nweb-destaques').noticiadorWeb({
        "idScript":1,
        "qtdNoticias":0,
        "htmlNoticia":'&lt;a href="#linknoticiador#" target="_blank"&gt;'+
                         '&lt;img src="#thumbnail#" /&gt;'+
                         '&lt;span&gt;#titulo#&lt;/span&gt;'+
                      '&lt;/a&gt;',
        "thumbWidth": 600,
        "thumbHeight": 300
    });
    </code></pre>
    
    <p>Os par�metros poss�veis de se utilizar no noticiador s�o:</p>
    
    <table>
        <tr>
            <th style="font-weight: bold!important;">'parametro' : 'valor padr�o'</th>
            <th style="font-weight: bold!important;">=></th>
            <th style="font-weight: bold!important;">descri��o</th>
        </tr>
        <tr>
            <td>'idScript' : ''</td>
            <td>=></td>
            <td>id do script gerado no NoticiadorWeb</td>
        </tr>
        <tr>
            <td>'qtdNoticias' : ''</td>
            <td>=></td>
            <td>quantidade de not�cias a ser exibida no script (0 equivale a todas not�cias da �rea)</td>
        </tr>
        <tr>
            <td>'htmlNoticia' : ''</td>
            <td>=></td>
            <td>html do modelo a ser exibido com tags descritas abaixo</td>
        </tr>
        <tr>
            <td>'thumbWidth' : 100</td>
            <td>=></td>
            <td>largura do thumbnail</td>
        </tr>
        <tr>
            <td>'thumbHeight' : 100</td>
            <td>=></td>
            <td>altura do thumbnail</td>
        </tr>
        <tr>
            <td>'paginacao' : false</td>
            <td>=></td>
            <td>se o script possui pagina��o</td>
        </tr>
        <tr>
            <td>'pgQtdNoticiasPagina' : 1</td>
            <td>=></td>
            <td>quantidade de not�cias a ser exibida por p�gina</td>
        </tr>
        <tr>
            <td>'pgQtdPaginasExibidas' : 5</td>
            <td>=></td>
            <td>quantidade de p�ginas a ser exibida no script antes das setas</td>
        </tr>
        <tr>
            <td>'pgBorda' : false</td>
            <td>=></td>
            <td>n�mero da p�gina tem borda</td>
        </tr>
        <tr>
            <td>'pgBordaCor' : 'none'</td>
            <td>=></td>
            <td>cor da borda no n�mero da p�gina</td>
        </tr>
        <tr>
            <td>'pgBordaHoverCor' : 'none'</td>
            <td>=></td>
            <td>cor da borda no n�mero da p�gina no hover</td>
        </tr>
        <tr>
            <td>'pgTextoCor' : '#666'</td>
            <td>=></td>
            <td>cor do texto no n�mero da p�gina</td>
        </tr>
        <tr>
            <td>'pgFundoCor' : 'none'</td>
            <td>=></td>
            <td>cor do fundo do texto no n�mero da p�gina</td>
        </tr>
        <tr>
            <td>'pgTextoHoverCor' : '#000'</td>
            <td>=></td>
            <td>cor do texto no n�mero da p�gina no hover</td>
        </tr>
        <tr>
            <td>'pgFundoHoverCor' : 'none'</td>
            <td>=></td>
            <td>cor do fundo do texto no n�mero da p�gina no hover</td>
        </tr>
    </table>
    
    <br />
    <p>O html pode conter as seguintes tags:</p>
    
    <table>
        <tr>
            <td>#data#</td>
            <td>=></td>
            <td>data de postagem da not�cia</td>
        </tr>
        <tr>
            <td>#id#</td>
            <td>=></td>
            <td>id da not�cia</td>
        </tr>
        <tr>
            <td>#link#</td>
            <td>=></td>
            <td>link informado no cadastro da not�cia</td>
        </tr>
        <tr>
            <td>#linknoticiador#</td>
            <td>=></td>
            <td>link da not�cia no noticiador</td>
        </tr>
        <tr>
            <td>#resumo#</td>
            <td>=></td>
            <td>resumo da not�cia</td>
        </tr>
        <tr>
            <td>#thumbnail#</td>
            <td>=></td>
            <td>link do thumbnail</td>
        </tr>
        <tr>
            <td>#titulo#</td>
            <td>=></td>
            <td>t�tulo da not�cia</td>
        </tr>
    </table>        
        
        
   �</div>
</div>	
<div class="rounded_content_bottom"></div>
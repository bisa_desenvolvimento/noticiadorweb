<?=add_html_head("Administração de perfis") ?>
<script type="text/javascript">
	function validarForm(obj) {
		if (!validarTexto(document.getElementById("txtDescricao"))) {
			alert('Digite uma descrição');
			return false;
		}

		return true;
	}
	
	function validarTexto(obj) {
		if (obj.value.length > 0)
			return true;
		
		return false;
	}
</script>
<form action="<?=$view->add_form_process_url() ?>" method="post" onSubmit="return validarForm(this);">
	<?=add_action_data("save", null, $view->getID()) ?>
	<fieldset>
		<legend>Dados do perfil</legend>
		<label for="txtDescricao">Descrição</label><br />
		<input type="text" name="txtDescricao" id="txtDescricao" value="<?=$view->getDescricao() ?>" class="inputText"/>
		<fieldset>
			<legend>Aquivos e acoes</legend>
			<?=$view->obterListaArquivosEAcoes() ?>
		</fieldset>
		<input type="submit" value="<?=translate("Salvar") ?>" class="botao"/>
	</fieldset>
	<fieldset>
		<legend>Perfis cadastrados</legend>
		<?=$view->obterPerfisCadastrados() ?>
	</fieldset>
</form>
<?=add_html_tail() ?>

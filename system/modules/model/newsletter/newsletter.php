<?
class newsletter extends TModel {
	private $FDataEnvio;
	private $FChave;
	private $FModelo;
	
	function setDataEnvio($value) { $this->FDataEnvio = $value; }
	function getDataEnvio() { return $this->FDataEnvio; }
	
	function setChave($value) { $this->FChave = $value; }
	function getChave() { return $this->FChave; }
	
	function setModelo($value) { $this->FModelo = $value; }
	function getModelo() { return $this->FModelo; }
	
	function bind($row) {
		$this->setID($row->newsletter_id);
		$this->setChave($row->new_chave);
		$this->setModelo($row->modelo_id);
		$this->setDataEnvio($row->new_data_envio);
	}
}
?>
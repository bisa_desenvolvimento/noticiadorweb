<?

class Arquivo extends TModel {
    private $FChave;
    private $FArquivo;
    private $FDescricao;
    private $FParent;
    
    function getChave() {
        return $this->FChave;
    }
    
    function setChave($value) {
        $this->FChave = $value;
    }

    function getArquivo() {
        return $this->FArquivo;
    }
    
    function setArquivo($value) {
        $this->FArquivo = $value;
    }
    
    function getDescricao() {
        return $this->FDescricao;
    }
    
    function setDescricao($value) {
        $this->FDescricao = $value;
    }
    
    function getParent() {
        return $this->FParent;
    }
    
    function setParent($value) {
        $this->FParent = $value;
    }
    
    public function bind($object) {
        $this->setChave($object->arq_chave);
        $this->setArquivo($object->arq_arquivo);
        $this->setDescricao($object->arq_descricao);
        $this->setParent($object->arq_parent);
        $this->setID($object->arquivo_id);
    }
}

?>
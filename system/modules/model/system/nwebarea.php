<?

class template extends TModel {
    private $FAREA_Codigo;
    private $FAREA_Descricao;
    private $FAREA_Observacao;
    private $FUSUA_Email;

    function getAREA_Codigo() {
        return $this->FAREA_Codigo;
    }
    
    function setAREA_Codigo($value){
        $this->FAREA_Codigo = $value;
    }
    
    function getAREA_Descricao() {
        return $this->FAREA_Descricao;
    }
    
    function setAREA_Descricao($value){
        $this->FAREA_Descricao = $value;
    }
    
    function getAREA_Observacao() {
        return $this->FAREA_Observacao;
    }
    
    function setAREA_Observacao($value){
        $this->FAREA_Observacao = $value;
    }

    function getUSUA_Email() {
        return $this->FUSUA_Email;
    }
    
    function setUSUA_Email($value){
        $this->FUSUA_Email = $value;
    }
    
    function bind($object) { throw new Exception("Falta implementar"); }

}

?>
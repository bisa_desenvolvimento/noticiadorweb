<?
class Usuario extends TModel {
    private $FNome;
    private $FLogin;
    private $FSenha;
    private $FCookie;
    private $FEMail;
    private $FIdioma;
    private $FExcluido;
    private $FPerfil;
    
    function getNome() { return $this->FNome; }
    function setNome($value) { $this->FNome = $value; }
    
    function getLogin() { return $this->FLogin; }
    function setLogin($value) { $this->FLogin = $value; }
    
    function getSenha() { return $this->FSenha; }
    function setSenha($value) { $this->FSenha = $value; }
    
    function getCookie() { return $this->FCookie; }
    function setCookie($value) { $this->FCookie = $value; }

    function getEMail() { return $this->FMail; }
    function setEMail($value) { $this->FMail = $value; }

    function getExcluido() { return $this->FExcluido; }
    function setExcluido($value) { $this->FExcluido = $value; }
    
    function getIdioma() { return $this->FIdioma; }
    function setIdioma($value) { $this->FIdioma = $value; }
    
    function getPerfil() { return $this->FPerfil; }
    function setPerfil($value) { $this->FPerfil = $value; }
    
    function bind($object) {
        $this->setNome($object->usu_nome);
        $this->setLogin($object->usu_login);
        $this->setSenha($object->usu_senha);
        $this->setCookie($object->usu_cookie);
        $this->setEMail($object->usu_email);
        $this->setExcluido($object->usu_excluido);
        $this->setIdioma($object->usu_idioma);
        $this->setPerfil($object->usu_perfil);
        $this->setID($object->usuario_id);
    }
}
?>
<?
class Perfil extends TModel {
	private $FDescricao = null;
	
	function setDescricao($value) { $this->FDescricao = $value; }
	function getDescricao() { return $this->FDescricao; }
	
	function bind($row) {
		$this->setDescricao($row->per_descricao);
		$this->setID($row->perfil_id);
	}
}
?>
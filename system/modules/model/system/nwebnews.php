<?

class nwebnews extends TModel {    

	private $FNEWS_Codigo;
    private $FNEWS_Descricao;
    private $FNEWS_Titulo;
    private $FNEWS_PathHtmlConfig;
    private $FNEWS_PathHtmlNewsletter;
    private $FNEWS_PathImg;
    private $FNEWS_ConfigNewsletter;
    private $FNEWS_ScriptNewsletter;
    private $FNEWS_Largura;
	private $FNEWS_MargenEsq;	
	private $FNEWS_MargenDir;
	private $FNEWS_FormatacaoTitulo;
	private $FNEWS_FormatacaoMensagem;
	private $FNEWS_FormatacaoTituloNoticiaUnica;
	private $FNEWS_FormatacaoMensagemNoticiaUnica;


    function getNEWS_Codigo() {
        return $this->FNEWS_Codigo;
    }
    
    function setNEWS_Codigo($value){
        $this->FNEWS_Codigo = $value;
    }

    function getNEWS_Descricao() {
        return $this->FNEWS_Descricao;
    }
    
    function setNEWS_Descricao($value){
        $this->FNEWS_Descricao = $value;
    }

    function getNEWS_Titulo() {
        return $this->FNEWS_Titulo;
    }
    
    function setNEWS_Titulo($value){
        $this->FNEWS_Titulo = $value;
    }

    function getNEWS_PathHtmlConfig() {
        return $this->FNEWS_PathHtmlConfig;
    }
    
    function setNEWS_PathHtmlConfig($value){
        $this->FNEWS_PathHtmlConfig = $value;
    }

    function getNEWS_PathHtmlNewsletter() {
        return $this->FNEWS_PathHtmlNewsletter;
    }
    
    function setNEWS_PathHtmlNewsletter($value){
        $this->FNEWS_PathHtmlNewsletter = $value;
    }

    function getNEWS_PathImg() {
        return $this->FNEWS_PathImg;
    }
    
    function setNEWS_PathImg($value){
        $this->FNEWS_PathImg = $value;
    }

    function getNEWS_ConfigNewsletter() {
        return $this->FNEWS_ConfigNewsletter;
    }
    
    function setNEWS_ConfigNewsletter($value){
        $this->FNEWS_ConfigNewsletter = $value;
    }

    function getNEWS_ScriptNewsletter() {
        return $this->FNEWS_ScriptNewsletter;
    }
    
    function setNEWS_ScriptNewsletter($value){
        $this->FNEWS_ScriptNewsletter = $value;
    }

    function getNEWS_Largura() {
        return $this->FNEWS_Largura;
    }
    
    function setNEWS_Largura($value){
        $this->FNEWS_Largura = $value;
    }

    function getNEWS_MargenEsq() {
        return $this->FNEWS_MargenEsq;
    }
    
    function setNEWS_MargenEsq($value){
        $this->FNEWS_MargenEsq = $value;
    }

	function getNEWS_MargenDir() {
        return $this->FNEWS_MargenDir;
    }
    
    function setNEWS_MargenDir($value){
        $this->FNEWS_MargenDir = $value;
    }

	function getNEWS_FormatacaoTitulo() {
        return $this->FNEWS_FormatacaoTitulo;
    }
    
    function setNEWS_FormatacaoTitulo($value){
        $this->FNEWS_FormatacaoTitulo = $value;
    }

    function getNEWS_FormatacaoMensagem() {
        return $this->FNEWS_FormatacaoMensagem;
    }
    
    function setNEWS_FormatacaoMensagem($value){
        $this->FNEWS_FormatacaoMensagem = $value;
    }
	
	function getNEWS_FormatacaoTituloNoticiaUnica() {
        return $this->FNEWS_FormatacaoTituloNoticiaUnica;
    }
    
    function setNEWS_FormatacaoTituloNoticiaUnica($value){
        $this->FNEWS_FormatacaoTituloNoticiaUnica = $value;
    }

	function getNEWS_FormatacaoMensagemNoticiaUnica() {
        return $this->FNEWS_FormatacaoMensagemNoticiaUnica;
    }
    
    function setNEWS_FormatacaoMensagemNoticiaUnica($value){
        $this->FNEWS_FormatacaoMensagemNoticiaUnica = $value;
    }
    
    function bind($object) { throw new Exception("Falta implementar"); }
}

?>
<?
class Acao extends TModel {
    private $FDescricao;
    
    public function getDescricao() {
        return $this->FDescricao;
    }
    
    public function setDescricao($value) {
        $this->FDescricao = $value;
    }
    
    public function bind($arr) {
        $this->setDescricao($arr->aca_descricao);
        $this->setID($arr->acao_id);
    }
}
?>
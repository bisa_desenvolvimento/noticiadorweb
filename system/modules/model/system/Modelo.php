<?
class Modelo extends TModel {
	private $FNome 		= null;
	private $FDescricao = null;
    private $FEmpresa   = null;
    private $FCliente   = null;
	
	function getNome() {
		return $this->FNome;
	}
	
	function setNome($value) {
		$this->FNome = $value;
	}
	
	function getDescricao() {
		return $this->FDescricao;
	}
	
	function setDescricao($value) {
		$this->FDescricao = $value;
	}
	function getEmpresa() {
		return $this->FEmpresa;
	}

	function setEmpresa($value) {
		$this->FEmpresa = $value;
	}
	
    function getCliente() {
		return $this->FCliente;
	}
	
	function setCliente($value) {
		$this->FCliente = $value;
	}
    
	function bind($obj) {
		$this->setID($obj->modelo_id);
		$this->setNome($obj->mod_nome);
		$this->setDescricao($obj->mod_descricao);
        $this->setCliente($obj->cliente_id);
	}
}
?>
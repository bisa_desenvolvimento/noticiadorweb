<?

class nwebmate extends TModel {    
	private $FMATE_ID;
    private $FUSUA_Email;
    private $FMATE_Titulo;
    private $FMATE_DataInsercao;
    private $FMATE_DataFinal;
    private $FMATE_ResumoMateria;
    private $FMATE_MateriaCompleta;
    private $FMATE_NotificarPorEmail;
    private $FMATE_LinkMateriaCompleta;
	private $FMATE_Acessos;	


    function getMATE_ID() {
        return $this->FMATE_ID;
    }
    
    function setMATE_ID($value){
        $this->FMATE_ID = $value;
    }

    function getUSUA_Email() {
        return $this->FUSUA_Email;
    }
    
    function setUSUA_Email($value){
        $this->FUSUA_Email = $value;
    }

    function getMATE_Titulo() {
        return $this->FMATE_Titulo;
    }
    
    function setMATE_Titulo($value){
        $this->FMATE_Titulo = $value;
    }

    function getMATE_DataInsercao() {
        return $this->FMATE_DataInsercao;
    }
    
    function setMATE_DataInsercao($value){
        $this->FMATE_DataInsercao = $value;
    }

    function getMATE_DataFinal() {
        return $this->FMATE_DataFinal;
    }
    
    function setMATE_DataFinal($value){
        $this->FMATE_DataFinal = $value;
    }

    function getMATE_ResumoMateria() {
        return $this->FMATE_ResumoMateria;
    }
    
    function setMATE_ResumoMateria($value){
        $this->FMATE_ResumoMateria = $value;
    }

    function getMATE_MateriaCompleta() {
        return $this->FMATE_MateriaCompleta;
    }
    
    function setMATE_MateriaCompleta($value){
        $this->FMATE_MateriaCompleta = $value;
    }

    function getMATE_NotificarPorEmail() {
        return $this->FMATE_NotificarPorEmail;
    }
    
    function setMATE_NotificarPorEmail($value){
        $this->FMATE_NotificarPorEmail = $value;
    }

    function getMATE_LinkMateriaCompleta() {
        return $this->FMATE_LinkMateriaCompleta;
    }
    
    function setMATE_LinkMateriaCompleta($value){
        $this->FMATE_LinkMateriaCompleta = $value;
    }

    function getMATE_Acessos() {
        return $this->FMATE_Acessos;
    }
    
    function setMATE_Acessos($value){
        $this->FMATE_Acessos = $value;
    }

	function bind($object) { throw new Exception("Falta implementar"); }
}

?>
<?
class Area extends TModel
{
	private $FDescricao;
	private $FObservacao;
    private $FCliente;
    private $FAtivacao;

	function getDescricao() { return $this->FDescricao; }
	function setDescricao($value) { $this->FDescricao = $value; }

	function getObservacao() { return $this->FObservacao; }
	function setObservacao($value) { $this->FObservacao = $value; }
    
    function getCliente() { return $this->FCliente; }
	function setCliente($value) { $this->FCliente = $value; }

    function setAtivacao($value) { $this->FAtivacao = $value; }
    function getAtivacao() { return $this->FAtivacao; }
        
	function bind($object) { 
		$this->setID($object->area_id);
		$this->setDescricao($object->are_descricao);
		$this->setObservacao($object->are_observacao);        
 	}
        
        function bindAreaAssinante($object){
                $this->setID($object->area_id);
		$this->setDescricao($object->are_descricao);
		$this->setObservacao($object->are_observacao);
                $this->setAtivacao($object->excluido);
        }
}
?>
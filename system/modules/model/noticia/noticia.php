<?

class noticia extends TModel {    
	private $Fnoticia_id;
    private $Fnot_autor_id;
    private $Fnot_titulo;
    private $Fnot_data;
    private $Fnot_publicacao;
    private $Fnot_validade;
    private $Fnot_chamada;
    private $Fnot_texto;
    private $Fnot_autorizada;
    private $Fnot_link;
	private $Fnot_acessos;	
	private $Fnot_excluida;	
	private $Fnot_thumbnail;
    private $Fnot_link_abrir;		

    function getnoticia_id() {
        return $this->Fnoticia_id;
    }
    
    function setnoticia_id($value){
        $this->Fnoticia_id = $value;
    }

    function getnot_autor_id() {
        return $this->Fnot_autor_id;
    }
    
    function setnot_autor_id($value){
        $this->Fnot_autor_id = $value;
    }

    function getnot_titulo() {
        return $this->Fnot_titulo;
    }
    
    function setnot_titulo($value){
        $this->Fnot_titulo = $value;
    }

    function getnot_data() {
        return $this->Fnot_data;
    }
    
    function setnot_data($value){
        $this->Fnot_data = $value;
    }
    
    function getnot_publicacao() {
        return $this->Fnot_publicacao;
    }
    
    function setnot_publicacao($value) {
        $this->Fnot_publicacao = $value;
    }

    function getnot_validade() {
        return $this->Fnot_validade;
    }
    
    function setnot_validade($value){
        $this->Fnot_validade = $value;
    }

    function getnot_chamada() {
        return $this->Fnot_chamada;
    }
    
    function setnot_chamada($value){
        $this->Fnot_chamada = $value;
    }

    function getnot_texto() {
        return $this->Fnot_texto;
    }
    
    function setnot_texto($value){
        $this->Fnot_texto = $value;
    }

    function getnot_autorizada() {
        return $this->Fnot_autorizada;
    }
    
    function setnot_autorizada($value){
        $this->Fnot_autorizada = $value;
    }

    function getnot_link() {
        return $this->Fnot_link;
    }
    
    function setnot_link($value){
        $this->Fnot_link = $value;
    }

    function getnot_acessos() {
        return $this->Fnot_Acessos;
    }
    
    function setnot_Acessos($value){
        $this->Fnot_Acessos = $value;
    }
	
    function getnot_excluida() {
        return $this->Fnot_excluida;
    }
    
    function setnot_excluida($value){
        $this->Fnot_excluida = $value;
    }
	
    function getnot_thumbnail() {
        return $this->Fnot_thumbnail;
    }
    
    function setnot_thumbnail($value){
        $this->Fnot_thumbnail = $value;
    }

    function getnot_link_abrir() {
        return $this->Fnot_link_abrir;
    }
    
    function setnot_link_abrir($value){
        $this->Fnot_link_abrir = $value;
    }


	function bind($object) { 
		$this->setID($object->noticia_id);
		$this->setnot_titulo($object->not_titulo);
		$this->setnot_chamada($object->not_chamada);
		$this->setnot_texto($object->not_texto);
        $this->setnot_publicacao($object->not_publicacao);
		$this->setnot_validade($object->not_validade);
		$this->setnot_link($object->not_link);		
		$this->setnot_thumbnail($object->not_thumbnail);	
        $this->setnot_link_abrir($object->not_link_abrir);	
	}
}

?>
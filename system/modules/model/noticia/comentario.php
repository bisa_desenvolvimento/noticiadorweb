<?

class comentario extends TModel {    
    private $Fcom_nome;
    private $Fcom_email;
    private $Fcom_fone;
    private $Fcom_comentario;
    private $Fcom_data;
    private $Fnoticia_id;
    private $Fcom_notificacao;

    function getCom_nome() {
        return $this->Fcom_nome;
    }
    
    function setCom_nome($value){
        $this->Fcom_nome = $value;
    }
    
    function getCom_email() {
        return $this->Fcom_email;
    }
    
    function setCom_email($value){
        $this->Fcom_email = $value;
    }
    
    function getCom_fone() {
        return $this->Fcom_fone;
    }
    
    function setCom_fone($value){
        $this->Fcom_fone = $value;
    }
    
    function getCom_comentario() {
        return $this->Fcom_comentario;
    }
    
    function setCom_comentario($value){
        $this->Fcom_comentario = $value;
    }
    
    function getCom_data() {
        return $this->Fcom_data;
    }
    
    function setCom_data($value){
        $this->Fcom_data = $value;
    }
    function getCom_notificacao() {
   
        return $this->Fcom_notificacao;
    }

    function setCom_notificacao($value){
        $this->Fcom_notificacao = $value;
    }
    
    function getNoticia_id() {
        return $this->Fnoticia_id;
    }
    
    function setNoticia_id($value){
        $this->Fnoticia_id = $value;
    }
    
	function bind($object) { 
		$this->setCom_nome($object->com_nome);
		$this->setCom_email($object->com_email);
        $this->setCom_fone($object->com_fone);
        $this->setCom_comentario($object->com_comentario);
        $this->setCom_data($object->com_data);
        $this->setNoticia_id($object->noticia_id);
        $this->setCom_notificacao($object->notificacao);
	}
}

?>
<?

class assinante extends TModel {
	private $Fassinante_id;
	private $Fass_email;
	private $Fass_senha;
	private $Fass_confirmado;
	private $Fass_excluido;	
	private $FEmails;
	private $areas;

	function getassinante_id() {
		return $this->Fassinante_id;
	}

	function setassinante_id($value){
		$this->Fassinante_id = $value;
	}

	function getass_email() {
		return $this->Fass_email;
	}

	function setass_email($value){
		$this->Fass_email = $value;
	}

	function getass_senha() {
		return $this->Fass_senha;
	}

	function setass_senha($value){
		$this->Fass_senha = $value;
	}

	function getass_confirmado() {
		return $this->Fass_confirmado;
	}

	function setass_confirmado($value){
		$this->Fass_confirmado = $value;
	}

	function getass_excluido() {
		return $this->Fass_excluido;
	}

	function setass_excluido($value){
		$this->Fass_excluido = $value;
	}

	function getemails() {
		return $this->FEmails;
	}

	function setemails($value){
		$this->FEmails = $value;
	}

	function getareas() {
		return $this->areas;
	}

	function setareas($value){
		$this->areas = $value;
	}


	function bind($object) {
		$this->setID($object->assinante_id);
		$this->setass_senha($object->ass_senha);
		$this->setass_confirmado($object->ass_confirmado);
		$this->setass_email($object->ass_email);
		$this->setass_excluido($object->ass_excluido);
	}
}

?>
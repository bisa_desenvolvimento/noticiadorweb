<?

class assinatura extends TModel {    
    private $Fass_corTexto;
    private $Fass_labelTexto;
    private $Fass_fonteTexto;
	private $Fass_corBotao;    
    private $Fass_labelBotao;
    private $Fass_larguraCaixa;
	private $Fusuario_id;		
    private $Femail;
   
    function getass_labelTexto() {
        return $this->Fass_labelTexto;
    }
    
    function setass_labelTexto($value){
        $this->Fass_labelTexto = $value;
    }    
    
    function getass_fonteTexto() {
        return $this->Fass_fonteTexto;
    }
    
    function setass_fonteTexto($value){
        $this->Fass_fonteTexto = $value;
    }      

    function getass_corTexto() {
        return $this->Fass_corTexto;
    }
    
    function setass_corTexto($value){
        $this->Fass_corTexto = $value;
    }
    
    function getass_corBotao() {
        return $this->Fass_corBotao;
    }
    
    function setass_corBotao($value){
        $this->Fass_corBotao = $value;
    }    
    
    function getass_labelBotao() {
        return $this->Fass_labelBotao;
    }
    
    function setass_labelBotao($value){
        $this->Fass_labelBotao = $value;
    }      
    
    function getass_larguraCaixa() {
        return $this->Fass_larguraCaixa;
    }

    function setass_larguraCaixa($value){
        $this->Fass_larguraCaixa = $value;
    }
    
    function getusuario_id() {
        return $this->Fusuario_id;
    }
    
    function setusuario_id($value){
        $this->Fusuario_id = $value;
    }
    
    function setass_email($value){
        $this->Femail = $value;
    }

    function getass_email() {
        return $this->Femail;
    }
 
	function bind($object) { 		
		$this->setID($object->assinatura_id);
		$this->setass_corBotao($object->ass_corBotao);
		$this->setass_corTexto($object->ass_corTexto);
		$this->setass_fonteTexto($object->ass_fonteTexto);
		$this->setass_labelBotao($object->ass_labelBotao);
		$this->setass_labelTexto($object->ass_labelTexto);
		$this->setass_larguraCaixa($object->ass_larguraCaixa);
        $this->setass_email($object->ass_email);
		$this->setusuario_id($object->usuario_id);
                //nao pode fazer o bind pelo fato de nao ter o campo no banco de dados a tabela assinatura
                //$this->setemail($object->email);
		
	}
}

?>
<?

class janela extends TModel {    
	private $Fjanela_id;
    private $Fjan_nome;
    private $Fjan_altural;
    private $Fjan_largura;
    private $Fjan_cor;
	private $Fjan_tipo;	
	private $Fjan_quantidade;	
	private $Fjan_exibirApenasTitulo;
	private $Fjan_data;
    private $Fjan_borda;
	private $Fjan_scroll;
	private $Fjan_logo;
	private $Fjan_foto;
	private $Fjan_indicar;
	private $Fjan_comentar;
	private $Fjan_visualizarComentario;
	private $Fjan_inserirNoticia;
	private $Fusuario_id;		
    private $Femail;

    function getjanela_id() {
        return $this->Fjanela_id;
    }
    
    function setjanela_id($value){
        $this->Fjanela_id = $value;
    }

    function getjan_nome() {
        return $this->Fjan_nome;
    }
    
    function setjan_nome($value){
        $this->Fjan_nome = $value;
    }

    function getjan_altura() {
        return $this->Fjan_altura;
    }
    
    function setjan_altura($value){
        $this->Fjan_altura = $value;
    }

    function getjan_largura() {
        return $this->Fjan_largura;
    }
    
    function setjan_largura($value){
        $this->Fjan_largura = $value;
    }

    function getjan_cor() {
        return $this->Fjan_cor;
    }
    
    function setjan_cor($value){
        $this->Fjan_cor = $value;
    }

    function getjan_tipo() {
        return $this->Fjan_tipo;
    }
    
    function setjan_tipo($value){
        $this->Fjan_tipo = $value;
    }

    
    function getjan_quantidade() {
        return $this->Fjan_quantidade;
    }
    
    function setjan_quantidade($value){
        $this->Fjan_quantidade = $value;
    }

    function getjan_exibirApenasTitulo() {
        return $this->Fjan_exibirApenasTitulo;
    }
    
    function setjan_exibirApenasTitulo($value){
        $this->Fjan_exibirApenasTitulo = $value;
    }

    function getusuario_id() {
        return $this->Fusuario_id;
    }
    
    function setusuario_id($value){
        $this->Fusuario_id = $value;
    }
    
    function setjan_data($value){
        $this->Fjan_data = $value;
    }

    function getjan_data() {
        return $this->Fjan_data;
    }
    
    function setjan_borda($value){
        $this->Fjan_data = $value;
    }

    function getjan_borda() {
        return $this->Fjan_data;
    }
    
    function setjan_scroll($value){
        $this->Fjan_scroll = $value;
    }

    function getjan_scroll() {
        return $this->Fjan_scroll;
    }

    function setjan_logo($value){
        $this->Fjan_logo = $value;
    }

    function getjan_logo() {
        return $this->Fjan_logo;
    }
    
    function setjan_foto($value){
        $this->Fjan_foto = $value;
    }

    function getjan_foto() {
        return $this->Fjan_foto;
    }

    function setjan_indicar($value){
        $this->Fjan_indicar = $value;
    }

    function getjan_indicar() {
        return $this->Fjan_indicar;
    }
    
    function setjan_comentar($value){
        $this->Fjan_comentar = $value;
    }

    function getjan_comentar() {
        return $this->Fjan_comentar;
    }
    
    function setjan_visualizarComentario($value){
        $this->Fjan_visualizarComentario = $value;
    }

    function getjan_visualizarComentario() {
        return $this->Fjan_visualizarComentario;
    }
    
	function setjan_inserirNoticia($value){
        $this->Fjan_inserirNoticia = $value;
    }

    function getjan_inserirNoticia() {
        return $this->Fjan_inserirNoticia;
    }    
    
    function setemail($value){
        $this->Femail = $value;
    }

    function getemail() {
        return $this->Femail;
    }

	function bind($object) { 		
		$this->setID($object->janela_id);
        $this->setjan_nome($object->jan_nome);
		$this->setjan_largura($object->jan_largura);
		$this->setjan_cor($object->jan_cor);
		$this->setjan_altura($object->jan_altura);
		$this->setjan_tipo($object->jan_tipo);		
		$this->setjan_quantidade($object->jan_quantidade);
		$this->setjan_exibirApenasTitulo($object->jan_exibirApenasTitulo);
		$this->setjan_data($object->jan_data);
        $this->setjan_borda($object->jan_borda);
		$this->setjan_scroll($object->jan_scroll);		
		$this->setjan_logo($object->jan_logo);
		$this->setjan_foto($object->jan_foto);
		$this->setjan_indicar($object->jan_indicar);
		$this->setjan_inserirNoticia($object->jan_inserirNoticia);
		$this->setjan_comentar($object->jan_comentar);
		$this->setjan_visualizarComentario($object->jan_visualizarComentario);
		$this->setusuario_id($object->usuario_id);
        @$this->setemail($object->email);
		
	}
}

?>
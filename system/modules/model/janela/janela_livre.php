<?

class janela_livre extends TModel {
    
	private $Fjanela_id;
    private $Fjan_nome;
    private $Fjan_quantidade;	
	private $Fusuario_id;		
    private $Femail;

    function getjanela_id() {
        return $this->Fjanela_id;
    }
    
    function setjanela_id($value){
        $this->Fjanela_id = $value;
    }

    function getjan_nome() {
        return $this->Fjan_nome;
    }
    
    function setjan_nome($value){
        $this->Fjan_nome = $value;
    }

    function getjan_quantidade() {
        return $this->Fjan_quantidade;
    }
    
    function setjan_quantidade($value){
        $this->Fjan_quantidade = $value;
    }
    
    function getusuario_id() {
        return $this->Fusuario_id;
    }
    
    function setusuario_id($value){
        $this->Fusuario_id = $value;
    }
    
    function setemail($value){
        $this->Femail = $value;
    }

    function getemail() {
        return $this->Femail;
    }

	function bind($object) { 		
	   
       
		$this->setID($object->jan_liv_id);
        $this->setjan_nome($object->jan_liv_nome);
		$this->setjan_quantidade($object->jan_liv_quantidade);
		$this->setusuario_id($object->usuario_id);
        @$this->setemail($object->email);
	}
}

?>
<?

class colaborador extends TModel {
    private $Fusuario_id;
    private $FUSUA_Email;
    private $FUSUA_ComoSoube;
    private $FUSUA_CompSoube;
    private $FUSUA_FlagEditor;	
	private $FUSUA_Senha;
	private $FUSUA_Nome;
	private $FUSUA_Endereco;
	private $FUSUA_Bairro;
	private $FUSUA_Cidade;
	private $FUSUA_UF;
   private $FUSUA_Idioma;
	private $FUSUA_Fone;
	private $FUSUA_EmailEmpresa;
	private $FUSUA_Cargo;
	private $FUSUA_Empresa;
	private $FUSUA_ComoSoubeOutros;
	private $FUSUA_ComoSoubePOutros;
	private $FUSUA_CEP;


    function getusuario_id() {
        return $this->Fusuario_id;
    }
    
    function setusuario_id($value){
        $this->Fusuario_id = $value;
    }
	
    function getUSUA_Email() {
        return $this->FUSUA_Email;
    }
    
    function setUSUA_Email($value){
        $this->FUSUA_Email = $value;
    }

    function getUSUA_ComoSoube() {
        return $this->FUSUA_ComoSoube;
    }
    
    function setUSUA_ComoSoube($value){
        $this->FUSUA_ComoSoube = $value;
    }

    function getUSUA_CompSoube() {
        return $this->FUSUA_CompSoube;
    }
    
    function setUSUA_CompSoube($value){
        $this->FUSUA_CompSoube = $value;
    }

    function getUSUA_FlagEditor() {
        return $this->FUSUA_FlagEditor;
    }
    
    function setUSUA_FlagEditor($value){
        $this->FUSUA_FlagEditor = $value;
    }

    function getSUA_Senha() {
        return $this->FSUA_Senha;
    }
    
    function setSUA_Senha($value){
        $this->FSUA_Senha = $value;
    }


    function getUSUA_Nome() {
        return $this->FUSUA_Nome;
    }
    
    function setUSUA_Nome($value){
        $this->FUSUA_Nome = $value;
    }


    function getUSUA_Endereco() {
        return $this->FUSUA_Endereco;
    }
    
    function setUSUA_Endereco($value){
        $this->FUSUA_Endereco = $value;
    }

    function getUSUA_Bairro() {
        return $this->FUSUA_Bairro;
    }
    
    function setUSUA_Bairro($value){
        $this->FUSUA_Bairro = $value;
    }


    function getUSUA_Cidade() {
        return $this->FUSUA_Cidade;
    }
    
    function setUSUA_Cidade($value){
        $this->FUSUA_Cidade = $value;
    }


    function getUSUA_UF() {
        return $this->FUSUA_UF;
    }
    
    function setUSUA_UF($value){
        $this->FUSUA_UF = $value;
    }
    
    function getUSUA_Idioma(){
      return $this->FUSUA_Idioma;
      
    } 
    
    function setUSUA_Idioma($value){
    $this->FUSUA_Idioma = $value;  
      
    }
    
    function getUSUA_Fone() {
        return $this->FUSUA_Fone;
    }
    
    function setUSUA_Fone($value){
        $this->FUSUA_Fone = $value;
    }


    function getUSUA_EmailEmpresa() {
        return $this->FUSUA_EmailEmpresa;
    }
    
    function setUSUA_EmailEmpresa($value){
        $this->FUSUA_EmailEmpresa = $value;
    }


    function getUSUA_Cargo() {
        return $this->FUSUA_Cargo;
    }
    
    function setUSUA_Cargo($value){
        $this->FUSUA_Cargo = $value;
    }


    function getUSUA_Empresa() {
        return $this->FUSUA_Empresa;
    }
    
    function setUSUA_Empresa($value){
        $this->FUSUA_Empresa = $value;
    }
	
    function getUSUA_ComoSoubeOutros() {
        return $this->FUSUA_ComoSoubeOutros;
    }
    
    function setUSUA_ComoSoubeOutros($value){
        $this->FUSUA_ComoSoubeOutros = $value;
    }
	
	
	function getUSUA_ComoSoubePOutros() {
        return $this->FUSUA_ComoSoubePOutros;
    }
    
    function setUSUA_ComoSoubePOutros($value){
        $this->FUSUA_ComoSoubePOutros = $value;
    }

	function getUSUA_CEP() {
        return $this->FUSUA_CEP;
    }
    
    function setUSUA_CEP($value){
        $this->FUSUA_CEP= $value;
    }

	function getUSUA_Senha() {
        return $this->FUSUA_Senha;
    }
    
    function setUSUA_Senha($value){
        $this->FUSUA_Senha= $value;
    }

	function bind($object) { 
		$this->setID($object->usuario_id);
		$this->setUSUA_Email($object->usu_email);
		$this->setUSUA_ComoSoube($object->USUA_ComoSoube);
		$this->setUSUA_CompSoube($object->USUA_CompSoube);
		$this->setUSUA_FlagEditor($object->USUA_FlagEditor);	
		$this->setUSUA_Senha($object->usu_senha);
        $this->setUSUA_Idioma($object->usu_idioma);
		$this->setUSUA_Nome($object->usu_nome);
		$this->setUSUA_Endereco($object->USUA_Endereco);
		$this->setUSUA_Bairro($object->USUA_Bairro);
		$this->setUSUA_Cidade($object->USUA_Cidade);
		$this->setUSUA_UF($object->USUA_UF);
		$this->setUSUA_Fone($object->USUA_Fone);
		$this->setUSUA_EmailEmpresa($object->USUA_EmailEmpresa);
		$this->setUSUA_Cargo($object->USUA_Cargo);
		$this->setUSUA_Empresa($object->USUA_Empresa);
		$this->setUSUA_ComoSoubeOutros($object->USUA_ComoSoubeOutros);
		$this->setUSUA_ComoSoubePOutros($object->USUA_ComoSoubePOutros);
		$this->setUSUA_CEP($object->USUA_CEP);
	}
	
	
}

?>
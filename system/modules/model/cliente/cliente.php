<?

class cliente extends TModel {    
    private $FCli_codigo;
    private $FCli_contrato;
    private $FCli_site;
    private $FCli_nome;
    private $FEmails;

    function getCli_codigo() {
        return $this->FCli_codigo;
    }
    
    function setCli_codigo($value){
        $this->FCli_codigo = $value;
    }

    function getCli_contrato() {
        return $this->FCli_contrato;
    }
    
    function setCli_contrato($value){
        $this->FCli_contrato = $value;
    }

    function getCli_nome() {
        return $this->FCli_nome;
    }
    
    function setCli_nome($value){
        $this->FCli_nome = $value;
    }

    function getCli_site() {
        return $this->FCli_site;
    }
    
    function setCli_site($value){
        $this->FCli_site = $value;
    }
    
    function getEmails() {
        return $this->FEmails;
    }
    
    function setEmails($value){
        $this->FEmails = $value;
    }
    
	function bind($object) { 
		$this->setID($object->cliente_id);
		$this->setCli_codigo($object->cli_codigo);
		$this->setCli_contrato($object->cli_contrato);
		$this->setCli_site($object->cli_site);
		$this->setCli_nome($object->cli_nome);
	}
}

?>
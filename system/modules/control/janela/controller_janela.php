<?
class Controller_janela extends TController
{
	function show() { 
	$view = new view_janela($this);
	$view->show();
	}

	function save($model){
	   
      	$area = "";
        if (isset($_POST['areas'])){
            $area = $_POST['areas'];
        }
        
        if ($model->getjanela_id() == null) {
        
            $sql = "INSERT INTO janela ( 
                        usuario_id
                        , jan_nome
                        , jan_altura
                        , jan_largura
                        , jan_cor
                        , jan_tipo					
                        , jan_quantidade
                        , jan_exibirApenasTitulo
                        , jan_indicar
                        , jan_comentar
                        , jan_visualizarComentario
                        , jan_inserirNoticia
                        , jan_logo
                        , jan_fotoPosicao
                        , jan_data
                        , jan_borda
                        , jan_scroll
                        )";		
            $sql .=" VALUES ('".$model->getusuario_id()
                            ."' , '".$model->getjan_nome()."','"
                            .$model->getjan_altura()."','"
                            .$model->getjan_largura()."','"
                            .$model->getjan_cor(). "', '"
                            .$model->getjan_tipo(). "', '"
                            .$model->getjan_quantidade()."', '"
                            .$model->getjan_exibirApenasTitulo()."', '"
                            .$model->getjan_indicar()."', '"
                            .$model->getjan_comentar()."', '"
                            .$model->getjan_visualizarComentario()."', '"
                            .$model->getjan_inserirNoticia()."', '"
                            .$model->getjan_logo()."', '"
                            .$model->getjan_foto()."', '"
                            .$model->getjan_data()."','"
                            .$model->getjan_borda()."','"
                            .$model->getjan_scroll()." ')";
               echo $sql;             
            $error = $this->getConexao()->executeNonQuery($sql);
            $janela_id=$this->getConexao()->getLastID();
            
            if($error!=-1) { 				
                
                for($i=0;$i<sizeof($area);$i++) {										
                    $sql = 'INSERT INTO janela_area(janela_id, area_id) values('.$janela_id.','.$area[$i].')';
                    $error = $this->getConexao()->executeNonQuery($sql);
                }
                
                if($error!=-1) {	
                
                    $largura = $model->getjan_largura()+10;
                    if($model->getjan_inserirNoticia()) {
                        $altura = $model->getjan_altura()+30;     
                    } else {
                        $altura = $model->getjan_altura()+5;    
                    }
                    
                    $codigo = htmlspecialchars('<iframe name="Noticiador Web" src="'.URL.'index.php?action=show&secao=janela_cli&id='.$janela_id.'" frameborder="0" width="'.$largura.'" height="'.$altura.'" scrolling="no"></iframe>');
                    $para = $model->getemail();
                    $assunto = "Código de notícias do Noticiador Web";
                    $mensagem = "<strong>Este é o código gerado pelo Noticiador Web. Insira o código abaixo no local desejado de sua página.</strong><br /><br />".$codigo;
                    $headers = "MIME-Version: 1.0\r\n";
                    $headers .= "Content-type:text/html;charset=iso-8859-1\r\n";
                    $headers .= "From: Noticiador Web <bisa@bisa.com.br>\r\n";
                    Email::sendEmail($para,$assunto,$mensagem);
                    //mail($para,$assunto,$mensagem,$headers);
                    print msg_alert("Script salvo com sucesso.");
                    print "<script>parent.location.href='".URL."index.php?action=show&secao=janela'</script>";
                
                }else{
                    print msg_alert("Registro não foi salvo.".$this->getConexao()->getErro());	
                }
            }else{		
                print msg_alert("Registro não foi salvo.".$this->getConexao()->getErro());	
            }
        
        
        } else {
            // UPDATE
            $sql = "UPDATE janela SET ";
            $sql .="jan_nome = '". $model->getjan_nome() ."'";
            $sql .=", jan_altura = '". $model->getjan_altura() ."'";
            $sql .=", jan_largura = '". $model->getjan_largura()  ."'";
            $sql .=", jan_cor = '". $model->getjan_cor() ."'";
            $sql .=", jan_tipo = '". $model->getjan_tipo() ."'";
            $sql .=", jan_quantidade = ". $model->getjan_quantidade() ."";
            $sql .=", jan_exibirApenasTitulo = '". $model->getjan_exibirApenasTitulo() ."'";
            $sql .=", jan_indicar = ". $model->getjan_indicar() ."";
            $sql .=", jan_comentar = ". $model->getjan_comentar() ."";
            $sql .=", jan_visualizarComentario = ". $model->getjan_visualizarComentario() ."";
            $sql .=", jan_inserirNoticia = ". $model->getjan_inserirNoticia() ."";
            $sql .=", jan_logo = ". $model->getjan_logo() ."";
            $sql .=", jan_fotoPosicao = '". $model->getjan_foto() ."'";
            $sql .=", jan_data = ". $model->getjan_data() ."";
            $sql .=", jan_borda = ". $model->getjan_borda() ."";
            $sql .=", jan_scroll = ". $model->getjan_scroll() ."";
            $sql .=" WHERE  janela_id = ". $model->getjanela_id()."";
            
            $error = $this->getConexao()->executeNonQuery($sql);
            if ($error!=-1){
                if($this->atualizarJanelaAreas($area, $model->getjanela_id())){
                
                    $largura = $model->getjan_largura()+10;
                    if($model->getjan_inserirNoticia()) {
                        $altura = $model->getjan_altura()+30;
                    } else {
                        $altura = $model->getjan_altura()+5;          
                    }
                    
                    $codigo = htmlspecialchars('<iframe name="Noticiador Web" src="'.URL.'index.php?action=show&secao=janela_cli&id='.$model->getjanela_id().'" frameborder="0" width="'.$largura.'" height="'.$altura.'" scrolling="no"></iframe>');
                    $para = $model->getemail();
                    $assunto = "Código de notícias do Noticiador Web";
                    $mensagem = "<strong>Este é o código gerado pelo Noticiador Web. Insira o código abaixo no local desejado de sua página.</strong><br /><br />".$codigo;
                    $headers = "MIME-Version: 1.0\r\n";
                    $headers .= "Content-type:text/html;charset=iso-8859-1\r\n";
                    $headers .= "From: Noticiador Web <bisa@bisa.com.br>\r\n";
                    Email::sendEmail($para,$assunto,$mensagem);
                    //mail($para,$assunto,$mensagem,$headers);
                    
                    print msg_alert("Script salvo com sucesso.");          
                    print "<script>parent.location.href='index.php?action=show&secao=janela'</script>";
                
                }else{
                    print msg_alert("Registro não foi salvo.".$this->getConexao()->getErro());
                }
            
            }else{
                print msg_alert("Registro não foi salvo.".$this->getConexao()->getErro());
            }
        
        }	
    }
  
  function obterCodigo() {
    $sql = 'SELECT MAX(janela_id)+1 AS cod FROM janela;';
    $table = $this->getConexao()->executeQuery($sql);
    $row = $table->getRow(0);
    return $row->cod;
  }

	function load($key){
		$sql = "SELECT * FROM janela WHERE janela_id= ".$key;
		
		$table = $this->getConexao()->executeQuery($sql);
		if ($table->RowCount() > 0) {	
			$result = new janela();
			$result->bind($table->getRow(0));
			if ($result != null){					
          $view = new view_janela($this);
          $view->setModel($result);
          $view->show();
			}
		} 
		return null;

	}

	function delete($key){
		
		$sql = "DELETE FROM janela WHERE janela_id = ".$key;
		$error = $this->getConexao()->executeNonQuery($sql);
    
        $sql = "DELETE FROM janela_area WHERE janela_id = ".$key;
		$error2 = $this->getConexao()->executeNonQuery($sql);
    
		if ($error != -1 || $error2 != -1) {
			$controller_arquivo = new controller_arquivo();
        	$controller_arquivo->setConexao(TConexao::getInstance());
        
			print msg_alert("Código excluído com sucesso!");
			$url = "index.php?".PARAMETER_NAME_ACTION."=show&";
			$url .= PARAMETER_NAME_FILE."=gestao_janela";

			print "<script>parent.location.href='".$url."'</script>";
		} else {
			print msg_alert("O código não pôde ser excluído. ".$this->getConexao()->getErro());
		}

	}

	function create(){ }
	

	function checarJanelaArea($janela_id, $area_id){
	   
        $sql = "SELECT * FROM janela_area WHERE janela_id = ". $janela_id ." and area_id = ".$area_id;
		$janela = $this->getConexao()->executeQuery($sql);

        if ($janela->RowCount() > 0) {
        	return " checked";
        }
        
        return "";
	}

	function obterAreas() {
		$usuario = controller_seguranca::getInstance()->identificarUsuario();
        $sql = 'SELECT area.* FROM area, area_usuario
        		WHERE area.area_id = area_usuario.area_id
        		AND area_usuario.usuario_id = '.$usuario->getID().
        	  ' ORDER BY area.are_descricao';
        $table_area = $this->getConexao()->executeQuery($sql);

        if ($table_area->RowCount() > 0) {
        	return $table_area;
        }
        
        return null;
    }



	function atualizarJanelaAreas($area, $janela_id) {
        $sql = 'DELETE FROM janela_area WHERE janela_id='.$janela_id;		
        $this->getConexao()->executeNonQuery($sql);		

		for($i=0;$i<sizeof($area);$i++){										
			$sql = 'INSERT INTO janela_area(janela_id, area_id) values('.$janela_id.','.$area[$i].')';
			$this->getConexao()->executeNonQuery($sql);
		}
		return true;

    }
	
	function obterNoticiasJanela($janela_id, $limite) {
	
		$sql = 'select * FROM noticia WHERE noticia_id in 
		(select noticia_id from noticia_area where area_id in (select area_id from janela_area where janela_id =$janela_id))';
		$sql .= " not_validade >= sysdate() and not_excluida <> 1 ";
		$sql .= "order by not_data desc ";
		$sql .= " limit $limite";
		
		$noticia_janela = $this->getConexao()->executeQuery($sql);
		if ($noticia_janela->RowCount() > 0) {
        	return $noticia_janela;
        }
        
        return null;
		

    }
	function obterJanelas($janela_id=""){
		$sql = "SELECT * FROM janela";
		if ($janela_id!=""){
			$sql = " WHERE janela_id = ".$janela_id;
		}
		$janelas = $this->getConexao()->executeQuery($sql);
		if ($janelas->RowCount() > 0) {
        	return $janelas;
        }
        
        return null;
	}
	
	
	function geraCodigo($janela_id){
	
    $controller_arquivo = new controller_arquivo();
    $controller_arquivo->setConexao(TConexao::getInstance());

  	$sql = 'SELECT * FROM janela WHERE janela_id='.$janela_id;
        $table_janela = $this->getConexao()->executeQuery($sql);

        if ($table_janela->RowCount() > 0) {
			$row_janela = $table_janela->getRow(0);
			$altura = $row_janela->jan_altura;
			$largura = $row_janela->jan_largura;
			if ($row_janela->jan_tipo=='G'){
				$scroll = 'scrolling="NO" height="'.$altura.'"';
			} else {
			      if ($row_janela->jan_scroll == 1){
                                   $scroll = 'scrolling="Auto" height="'.$altura.'"';
                              } else {
                                   $scroll = 'scrolling="NO" height="'.$altura.'"';
                              }
                        }
        }
        
		
		$codigo_gerado = '&lt;table width="'.$largura.'" border="0" cellspacing="0" cellpadding="0"&gt;<br>';
		$codigo_gerado .=  "&nbsp;&nbsp;&nbsp;&lt;tr&gt;<br>";
		$codigo_gerado .=  "	&nbsp;&nbsp;&nbsp;&nbsp;&lt;td&gt;<br>";
		$codigo_gerado .=  '	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;iframe '.$scroll.' noresize width="'.$largura.'" frameborder=0  src="'.URL.'index.php?janela='.$janela_id.'&'.PARAMETER_NAME_FILE.'='.$controller_arquivo->getArquivoByNome("girar_noticia")->getChave().'"&gt;<br>';
		$codigo_gerado .=  "	   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/iframe&gt;<br>";
		$codigo_gerado .=  "	 &nbsp;&nbsp;&nbsp;&nbsp;&lt;/td&gt;<br>";
		$codigo_gerado .=  " &nbsp;&nbsp;&nbsp;&lt;/tr&gt;<br>";
		$codigo_gerado .=  "&lt;/table&gt;<br>";
		print 	$codigo_gerado;
	}
	
	function geraCodigoJanela($janela_id){
	
        $controller_arquivo = new controller_arquivo();
        $controller_arquivo->setConexao(TConexao::getInstance());

	
		$sql = 'SELECT * FROM janela WHERE janela_id='.$janela_id;
        $table_janela = $this->getConexao()->executeQuery($sql);

        if ($table_janela->RowCount() > 0) {
			$row_janela = $table_janela->getRow(0);
			$altura = $row_janela->jan_altura;
			$largura = $row_janela->jan_largura;
			if ($row_janela->jan_tipo=='G'){
				$scroll = 'scrolling="NO"';
			} else {
			      if ($row_janela->jan_scroll == 1){
                                   $scroll = 'scrolling="Auto"';
                              } else {
                                   $scroll = 'scrolling="NO"';
                              }
                        }
        }
        
		//scrolling="'.$scroll.'" 
		$codigo_gerado = '<table border="0" cellspacing="0" cellpadding="0"><br>';
		$codigo_gerado .=  "<tr><br>";
		$codigo_gerado .=  "<td><br>";
		$codigo_gerado .=  "<label for=\"Fjan_visualizacao\">".translate("Visualização")."</label><br/>";
		$codigo_gerado .=  '<iframe '.$scroll.' noresize height="'.$altura.'" width="'.$largura.'" frameborder=0  src="'.URL.'index.php?janela='.$janela_id.'&'.PARAMETER_NAME_FILE.'='.$controller_arquivo->getArquivoByNome("girar_noticia")->getChave().'"><br>';
		$codigo_gerado .=  "</iframe><br>";
		$codigo_gerado .=  "</td>";
		$codigo_gerado .=  "</tr>";
		$codigo_gerado .=  "<tr><td align=\"center\">
								<input type=\"submit\" value=\"Atualizar Visualização\"/>
					   		</td></tr>";
		$codigo_gerado .=  "</table>";
		return $codigo_gerado;
	}


	function process(){
 		parent::process();

		switch($this->getAction()) {
			 case "save":
	 			$usuario = controller_seguranca::getInstance()->identificarUsuario();
				
				$save = new janela();
				if (isset($_POST["id"]))
					$save->setjanela_id($_POST["id"]);
				$save->setjan_nome($_POST["Fjan_nome"]);	
				$save->setjan_altura($_POST["Fjan_altura"]);
				$save->setjan_largura($_POST["Fjan_largura"]);
				$save->setjan_cor($_POST["Fjan_cor"]);
				$save->setjan_tipo($_POST["Fjan_tipo"]);				
				$save->setjan_quantidade($_POST["Fjan_quantidade"]);
				$save->setjan_exibirApenasTitulo($_POST["Fjan_exibirApenasTitulo"]);
				if($_POST["Fjan_indicar"] != "") {
					$save->setjan_indicar($_POST["Fjan_indicar"]);
				} else {
					$save->setjan_indicar(0);
				}
				if($_POST["Fjan_comentar"] != "") {
					$save->setjan_comentar($_POST["Fjan_comentar"]);
				} else {
					$save->setjan_comentar(0);	
				}
				if($_POST["Fjan_visualizarComentario"] != "") {
					$save->setjan_visualizarComentario($_POST["Fjan_visualizarComentario"]);
				} else {
					$save->setjan_visualizarComentario(0);	
				}
				if($_POST["Fjan_inserirNoticia"] != "") {
					$save->setjan_inserirNoticia($_POST["Fjan_inserirNoticia"]);
				} else {
					$save->setjan_inserirNoticia(0);
				}
				if($_POST["Fjan_logo"] != "") {
					$save->setjan_logo($_POST["Fjan_logo"]);
				} else {
					$save->setjan_logo(0);	
				}
				if($_POST["Fjan_foto"] != "") {
					$save->setjan_foto($_POST["Fjan_foto"]);
				} else {
					$save->setjan_foto("N");
				}
				if ($usuario != null){
					$save->setusuario_id($usuario->getID());
				}else{
					$save->setusuario_id(1);
				}
                if($_POST["Fjan_data"] != "") {
					$save->setjan_data($_POST["Fjan_data"]);
				} else {
					$save->setjan_data(0);
				}
                if($_POST["Fjan_borda"] != "") {
					$save->setjan_borda($_POST["Fjan_borda"]);
				} else {
					$save->setjan_borda(0);
				}
				if($_POST["Fjan_scroll"] != "") {
					$save->setjan_scroll($_POST["Fjan_scroll"]);
				} else {
					$save->setjan_scroll(0);
				}
                $save->setemail($_POST["Fjan_email"]);  
				$this->save($save);
				break;
		}
	 }
     
     /**
     * Obter as áreas associadas para cada cliente passado no parâmetro cliente. As áreas sem associação com cliente receberão como parâmetro "COLAB"
     * Caso seja passado o parâmetro usuario, a função retorna os clientes do usuário informado.
     * Caso não seja passado o parâmetro usuario, a função retorna os clientes do usuário atualmente logado no sistema.
     * André Alves - 27/01/2012
     * 
     * @param string $cliente Id do cliente
     * @param int $usuario Id do usuário
     * @return object Lista com os clientes
     */
    /*function obterAreasDoUsuario($cliente = '', $usuario = '') { Função deslocada para o TController - Jorge Roberto Mantis 6383

        $where = '';

        if($usuario == '') {
            $res = controller_seguranca::getInstance()->identificarUsuario();
            $usuario = $res->getID();
        }

        if($cliente != '') {
            $where .= ' AND Clie_Codigo = "'.$cliente.'"';
        }

        $sql = 'SELECT
                  area_id,
                  are_descricao,
                  are_observacao
                FROM
                  vw_usuarios_areas_clientes
                WHERE usuario_id = '.$usuario.
                $where.'
                ORDER BY are_descricao';

        $result = $this->getConexao()->executeQuery($sql);

        if ($result != null) {
            return $result;
        }

        return null;
    }*/
    
    
    /**
     * Obter a lista dos clientes associados ao usuário.
     * Caso seja passado o parâmetro usuario, a função retorna os clientes do usuário informado.
     * Caso não seja passado o parâmetro usuario, a função retorna os clientes do usuário atualmente logado no sistema.
     * André Alves - 27/01/2012
     * 
     * @param int $usuario Id do usuário
     * @return object Lista com os clientes
     */
    function obterClientesDoUsuario($usuario = '') {
        
        if($usuario == '') {
            $res = controller_seguranca::getInstance()->identificarUsuario();
            $usuario = $res->getID();    
        }        
        
        $sql = 'SELECT 
                  CLIE_Codigo,
                  CLIE_Nome 
                FROM
                  vw_usuarios_areas_clientes 
                WHERE usuario_id = '.$usuario.' 
                GROUP BY  CLIE_Codigo,CLIE_Nome';
            
        $result = $this->getConexao()->executeQuery($sql);
        
        if ($result != null) {
            return $result;
        }        
        return null;   
    }

}
?>
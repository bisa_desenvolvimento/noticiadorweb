<?
class controller_gestao_janela extends TController
{
	function show() {
			$view = new view_gestao_janela($this);
			$view->show();	
	}
	function save($model){
		
	}
	
	function load($key){
	}
	
	function delete($key){
		
	}
	
	function create(){
		
	}	

	
	function obterTodos() {
		$usuario = controller_seguranca::getInstance()->identificarUsuario();
        $sql = 'SELECT janela.*, usuario.usu_email FROM janela, usuario WHERE janela.usuario_id = usuario.usuario_id ';
		$sql .= '  AND janela_id IS NOT NULL ';
		if ((isset($_POST['Fjan_nome']))&&($_POST['Fjan_nome']!="")){
			$sql .= " AND jan_nome LIKE '%".$_POST['Fjan_nome']."%'";
		}
		if ($usuario != null)
			$sql .= ' AND usuario.usuario_id ='.$usuario->getID();
		$sql .= ' ORDER BY janela_id';

        $table = $this->getConexao()->executeQuery($sql);
        
        if ($table->RowCount() > 0) {
        	return $table;
        }
        
        return null;
    }
	
	function obterAreas($janela_id="") {
	
        $sql = 'SELECT * FROM area ';
		
		if  ($janela_id!=""){
			$sql .= ' WHERE area_id in(SELECT area_id FROM janela_area WHERE janela_id='.$janela_id.')';
		}
		
		$sql .= ' ORDER BY are_descricao ';
		
        $table_area = $this->getConexao()->executeQuery($sql);

        if ($table_area->RowCount() > 0) {
        	return $table_area;
        }
        
        return null;
    }

}
?>
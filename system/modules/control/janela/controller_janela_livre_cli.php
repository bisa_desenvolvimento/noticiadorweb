<?

class Controller_janela_livre_cli extends TController {

/*

Você criou um novo código no NoticiadoWeb para colocar em sua página. Esse é um código sem formatação que retorna um objeto JSON de forma que você possa utilizar os elementos da maneira que desejar na sua página.

Para obter os dados das notícias é necessária uma requisição AJAX ano nosso servidor. O código para conexão, utilizando jQuery encontra-se abaixo:


<script>

    $.ajax({
        url: 'https://noticiadorweb/?action=show&secao=janela_livre_cli&callback=noticiadorweb',
        type: 'POST',
        dataType: 'jsonp',
        data: { id: 1, qtd: 10 }
    });
    
    noticiadorweb = function(noticia){
        console.log(noticia);
        console.log(noticia[0].titulo);
        $('code').html(noticia[1].titulo);
    };

</script>
 

A requisição retorna um array chamado "noticias" que contém objetos JSON com os seguintes elementos:

id: ID da notícia
titulo: Título da notícia
resumo: Resumo da notícia
data: Data da postagem da notícia
link: Link informado no campo Link da Matéria Completa no cadastro da notícia
thumbnail: Link da imagem em miniatura da notícia 
https://minup.net/OMwjkmm

*/

	public function show() {

		$mensagemDegub = "";

		$id = $_REQUEST['id'];

		$qtd = isset($_REQUEST['qtd']) ? intval($_REQUEST['qtd']) : 0;

		$qtdRes = isset($_REQUEST['qtdRes']) ? intval($_REQUEST['qtdRes']) : 0;

		//print $qtdRes;

		//$mensagemDegub = $mensagemDegub . "qtdRes: " . $qtdRes . "<br />";

		$qtdTxt = isset($_REQUEST['qtdTxt']) ? intval($_REQUEST['qtdTxt']) : 0;

		//print $qtdTxt;

		//$mensagemDegub = $mensagemDegub . "qtdTxt: " . $qtdTxt . "<br />";

		$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 0;

		$init = $qtd * $page;

		$sql = 'SELECT 
				  noticia.noticia_id AS id,
				  noticia.not_titulo AS titulo,
				  noticia.not_chamada AS resumo,
				  noticia.not_texto AS texto,
				  noticia.not_publicacao AS data,
				  noticia.not_link AS link,
				  noticia.not_thumbnail AS thumbnail 
				FROM
				  noticia 
				WHERE EXISTS 
				  (SELECT 
					* 
				  FROM
					noticia_area 
				  WHERE noticia.noticia_id = noticia_area.noticia_id 
					AND noticia_area.area_id IN 
					(SELECT 
					  area_id 
					FROM
					  janela_livre_area 
					WHERE janela_livre_id = '.$id.')) 
				  AND noticia.not_autorizada = 1 
				  AND noticia.not_excluida = 0 
				ORDER BY noticia.noticia_id DESC';

		if ($qtd != 0) {
			$sql .= ' LIMIT ' . $init.', ' . $qtd;
		}

		$table = $this->getConexao()->executeQuery($sql);
		$rows  = $table->Rows();

		$encoded_rows = array();

		$arrNoticias = array();

		foreach($rows as $row) {

			if ($qtdRes > 0) {
				$row->resumo = substr(strip_tags($row->resumo), 0, $qtdRes);
				$posUltimoEspaco = strrpos($row->resumo, " ");
				$row->resumo = substr($row->resumo, 0, $posUltimoEspaco);
				//$mensagemDegub = $mensagemDegub . "resumo: " . $row->resumo . "<br />";
			}

			if ($qtdTxt > 0) {
				$row->texto = substr(strip_tags($row->texto), 0, $qtdTxt);
				$posUltimoEspaco = strrpos($row->texto, " ");
				$row->texto = substr($row->texto, 0, $posUltimoEspaco);
				//$mensagemDegub = $mensagemDegub . "texto: " . $row->texto . "<br />";
			}

			$arrNoticias[] = array_map('utf8_encode', (array) $row);
		}

		$sql = 'SELECT 
				  COUNT(*) AS total
				FROM
				  noticia 
				WHERE EXISTS 
				  (SELECT 
					* 
				  FROM
					noticia_area 
				  WHERE noticia.noticia_id = noticia_area.noticia_id 
					AND noticia_area.area_id IN 
					(SELECT 
					  area_id 
					FROM
					  janela_livre_area 
					WHERE janela_livre_id = '.$id.')) 
				  AND noticia.not_autorizada = 1 
				  AND noticia.not_excluida = 0';

		$table = $this->getConexao()->executeQuery($sql);
		$rows  = $table->Rows();
		$total = $rows[0]->total;

		$encoded_rows['noticias'] = $arrNoticias;

		$encoded_rows['total'] = $total;

		$encoded_rows['mensagemDegub'] = $mensagemDegub;

		//echo '<pre>';
		//exit(var_dump(json_encode($encoded_rows)));

		$json = json_encode($encoded_rows);

		//echo "noticiadorweb($json);";

		header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Key');
		header('Access-Control-Allow-Origin: *');
		echo $json;
	}

	function save($model){
	}

	function load($key){

		$mensagemDegub = "";

		$id = $_REQUEST['id'];

		$qtdRes = isset($_REQUEST['qtdRes']) ? intval($_REQUEST['qtdRes']) : 0;

		//print $qtdRes;

		//$mensagemDegub = $mensagemDegub . "qtdRes: " . $qtdRes . "<br />";

		$qtdTxt = isset($_REQUEST['qtdTxt']) ? intval($_REQUEST['qtdTxt']) : 0;

		//print $qtdTxt;

		//$mensagemDegub = $mensagemDegub . "qtdTxt: " . $qtdTxt . "<br />";

		$sql = 'SELECT 
                  noticia.noticia_id AS id,
                  noticia.not_titulo AS titulo,
                  noticia.not_chamada AS resumo,
                  noticia.not_texto AS texto,
                  noticia.not_publicacao AS data,
                  noticia.not_link AS link,
                  noticia.not_thumbnail AS thumbnail 
                FROM
                  noticia 
                WHERE 
                  noticia.noticia_id = '.$id;

        $table = $this->getConexao()->executeQuery($sql);
        $rows  = $table->Rows();

        $encoded_rows = array();

		$arrDadosNoticia = array();

        foreach($rows as $row) {

			if ($qtdRes > 0) {
				$row->resumo = substr(strip_tags($row->resumo), 0, $qtdRes);
				$posUltimoEspaco = strrpos($row->resumo, " ");
				$row->resumo = substr($row->resumo, 0, $posUltimoEspaco);
				//$mensagemDegub = $mensagemDegub . "resumo: " . $row->resumo . "<br />";
			}

			if ($qtdTxt > 0) {
				$row->texto = substr(strip_tags($row->texto), 0, $qtdTxt);
				$posUltimoEspaco = strrpos($row->texto, " ");
				$row->texto = substr($row->texto, 0, $posUltimoEspaco);
				//$mensagemDegub = $mensagemDegub . "texto: " . $row->texto . "<br />";
			}
		
            $arrDadosNoticia[] = array_map('utf8_encode', (array)$row);
        }

		$encoded_rows['noticias'] = $arrDadosNoticia;



	    $sql = 'SELECT * FROM comentario WHERE com_exibe = 1 AND noticia_id = "'.$id.'"';
		$listaComentarios = $this->getConexao()->executeQuery($sql);

        //echo '<pre>';
        //exit(var_dump($listaComentarios));
        //echo '</pre>';

		$comentarios = array();

        for ($i = 0; $i < $listaComentarios->rowCount(); $i++) {
			$comentario = $listaComentarios->getRow($i);

			//echo '<pre>';
			//exit(var_dump($comentario));
			//echo '</pre>';

			$comentarios[$i] = array();

			$comentarios[$i]["nome"] = $comentario->com_nome;
			$comentarios[$i]["comentario"] = $comentario->com_comentario;

			//echo '<pre>';
			//exit(var_dump($comentarios));
			//echo '</pre>';

			$comentarios[$i] = array_map('utf8_encode', $comentarios[$i]);
        }

		$encoded_rows['comentarios'] = $comentarios;

		$encoded_rows['qtdComentarios'] = $listaComentarios->rowCount();



		$sql = "UPDATE noticia SET not_acessos = (not_acessos + 1) WHERE noticia_id = '".$id."';";
		$this->getConexao()->executeNonQuery($sql);



		$encoded_rows['mensagemDegub'] = $mensagemDegub;

        //echo '<pre>';
        //exit(var_dump($encoded_rows));
        //echo '</pre>';

        $json = json_encode($encoded_rows);

		//echo "noticiadorweb($json);";

		header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Key');
		header('Access-Control-Allow-Origin: *');
        echo $json;
	}

	function delete($key){
	}

	function create(){
	}

    function getDadosJanela($id) {
        $sql = "SELECT * FROM janela WHERE janela_id = '".$id."';";
        $table = $this->getConexao()->executeQuery($sql);
        if ($table->RowCount() > 0) {
            return $table->getRow(0);
        }
    }

}
?>
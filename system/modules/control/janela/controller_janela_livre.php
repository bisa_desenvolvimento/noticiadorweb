<?php

class Controller_janela_livre extends TController {
	
    function show() { 
	$view = new view_janela_livre($this);
	$view->show();
    }

    function save($model){
	   
      	$area = "";
        if (isset($_POST['areas'])){
            $area = $_POST['areas'];
        }
        
        if ($model->getjanela_id() == null) {
        
            $sql = "INSERT INTO janela_livre (
                        usuario_id, 
                        jan_liv_nome, 
                        jan_liv_quantidade
                   )";
            $sql .=" VALUES ('".$model->getusuario_id()
                            ."' , '".$model->getjan_nome()."','"
                            .$model->getjan_quantidade()."' )";
               //echo $sql;             
            $error = $this->getConexao()->executeNonQuery($sql);
            $janela_id = $this->getConexao()->getLastID();
            
            if($error!=-1) {
                
                for($i=0;$i<sizeof($area);$i++) {
                    $sql = 'INSERT INTO janela_livre_area(janela_livre_id, area_id) values('.$janela_id.','.$area[$i].')';
                    $error = $this->getConexao()->executeNonQuery($sql);
                }
                
                if($error!=-1) {
                
                    //$codigo = htmlspecialchars('');
                    $para = $model->getemail();

                    $assunto = "Código e Instruções do Plugin de Notícias Janela Livre do NoticiadorWeb";

                    $mensagem = "

					<h1 class=\"default_title\">Plugin de Notícias Janela Livre do NoticiadorWeb</h1>

					<p>
						Você acabou de gerar um código para ser utilizado no plugin de notícias do NoticiadorWeb. O código gerado para o seu plugin é: <span style=\"font-size: 14px; font-weight: bold\">$janela_id</span>
					</p>

					<p>
						Seguem as instruções para utilização do plugin de janela livre do NoticiadorWeb:
					</p>

					<p>
						Para utilizar o plugin de Janela Livre do NoticiadorWeb primeiro chame a biblioteca do plugin no site onde irá utilizá-lo a partir do endereço abaixo:
						<br />
						https://noticiadorweb.com.br/scripts/jquery.noticiadorweb.js

					</p>

					<p>
						Em seguida, crie uma <code>&lt;div&gt;</code> vazia na página e defina um id para a mesma.
					</p>

					<p>
						<code>
							&lt;div id=\"id\"&gt;&lt;/div&gt;
						</code>
					</p>

					<p>
						Depois, chame o plugin do NoticiadorWeb nesse id. Segue abaixo um exemplo de chamada do plugin.
					</p>

					<pre>
						<code>

	jQuery(document).ready(

		function(){

			jQuery('#nwebPdLDestaques').noticiadorWeb(
				{
					\"idScript\": 0
					, \"qtdNoticias\": 3
					, \"urlExibicaoNoticia\": 'https://www.siteondeserainstaladooplugin.com/noticias/index.php'
					, \"urlParametrosListagem\": true
					, \"classContainerNoticias\": 'nw_lista_noticias'
					, \"htmlContainerNoticias\": '' +
									'&lt;ul class=\&quot;nw_lista_noticias\&quot;&gt;' +
									'&lt;/ul&gt;'
					, \"htmlNoticia\": '' +
									'&lt;li class=\&quot;nw_noticia\&quot;&gt;' +
									'&lt;span class=\&quot;nw_thumbnail\&quot;&gt;' +
									'&lt;a class=\&quot;nw_link_noticia\&quot; href=\&quot;#linknoticia#\&quot; target=\&quot;_blank\&quot;&gt;' +
									'#thumbnail#' +
									'&lt;/a&gt;' +
									'&lt;/span&gt;' +
									'&lt;span class=\&quot;nw_titulo\&quot;&gt;' +
									'&lt;a class=\&quot;nw_link_noticia\&quot; href=\&quot;#linknoticia#\&quot; target=\&quot;_blank\&quot;&gt;' +
									'#titulo#' +
									'&lt;/a&gt;' +
									'&lt;/span&gt;' +
									'&lt;span class=\&quot;nw_resumo\&quot;&gt;' +
									'&lt;a class=\&quot;nw_link_noticia\&quot; href=\&quot;#linknoticia#\&quot; target=\&quot;_blank\&quot;&gt;' +
									'#resumo#' +
									'&lt;/a&gt;' +
									'&lt;span class=\&quot;nw_leiamais\&quot;&gt;' +
									'&lt;a class=\&quot;nw_link_noticia\&quot; href=\&quot;#linknoticiador#\&quot; target=\&quot;_blank\&quot;&gt;' +
									' Leia Mais...' +
									'&lt;/a&gt;' +
									'&lt;/span&gt;' +
									'&lt;/span&gt;' +
									'&lt;/li&gt;'
					, \"urlAltThumb\" : 'https://www.siteondeserainstaladooplugin.com/images/imagempadrao.png'
					, \"thumbWidth\": 305
					, \"thumbHeight\": 250
				}
			);

		}

	);

						</code>
					</pre>

					<p>
						Os parâmetros possíveis de se utilizar no noticiador são:
					</p>

					<table>
						<tr>
							<th style=\"font-weight: bold!important;\">'parametro' : 'valor padrão'</th>
							<th style=\"font-weight: bold!important;\">=></th>
							<th style=\"font-weight: bold!important;\">descrição</th>
						</tr>
						<tr>
							<td>'idScript' : ''</td>
							<td>=></td>
							<td>id do script gerado no NoticiadorWeb</td>
						</tr>
						<tr>
							<td>'qtdNoticias' : ''</td>
							<td>=></td>
							<td>quantidade de notícias a ser exibida no script (0 equivale a todas notícias da área)</td>
						</tr>
						<tr>
							<td>'htmlNoticia' : ''</td>
							<td>=></td>
							<td>html do modelo a ser exibido com tags descritas abaixo</td>
						</tr>
						<tr>
							<td>'thumbWidth' : 100</td>
							<td>=></td>
							<td>largura do thumbnail</td>
						</tr>
						<tr>
							<td>'thumbHeight' : 100</td>
							<td>=></td>
							<td>altura do thumbnail</td>
						</tr>
						<tr>
							<td>'paginacao' : false</td>
							<td>=></td>
							<td>se o script possui paginação</td>
						</tr>
						<tr>
							<td>'pgQtdNoticiasPagina' : 1</td>
							<td>=></td>
							<td>quantidade de notícias a ser exibida por página</td>
						</tr>
						<tr>
							<td>'pgQtdPaginasExibidas' : 5</td>
							<td>=></td>
							<td>quantidade de páginas a ser exibida no script antes das setas</td>
						</tr>
						<tr>
							<td>'pgBorda' : false</td>
							<td>=></td>
							<td>número da página tem borda</td>
						</tr>
						<tr>
							<td>'pgBordaCor' : 'none'</td>
							<td>=></td>
							<td>cor da borda no número da página</td>
						</tr>
						<tr>
							<td>'pgBordaHoverCor' : 'none'</td>
							<td>=></td>
							<td>cor da borda no número da página no hover</td>
						</tr>
						<tr>
							<td>'pgTextoCor' : '#666'</td>
							<td>=></td>
							<td>cor do texto no número da página</td>
						</tr>
						<tr>
							<td>'pgFundoCor' : 'none'</td>
							<td>=></td>
							<td>cor do fundo do texto no número da página</td>
						</tr>
						<tr>
							<td>'pgTextoHoverCor' : '#000'</td>
							<td>=></td>
							<td>cor do texto no número da página no hover</td>
						</tr>
						<tr>
							<td>'pgFundoHoverCor' : 'none'</td>
							<td>=></td>
							<td>cor do fundo do texto no número da página no hover</td>
						</tr>
					</table>

					<p>
						O html pode conter as seguintes tags:
					</p>

					<table>
						<tr>
							<td>#data#</td>
							<td>=></td>
							<td>data de postagem da notícia</td>
						</tr>
						<tr>
							<td>#id#</td>
							<td>=></td>
							<td>id da notícia</td>
						</tr>
						<tr>
							<td>#link#</td>
							<td>=></td>
							<td>link informado no cadastro da notícia</td>
						</tr>
						<tr>
							<td>#linknoticiador#</td>
							<td>=></td>
							<td>link da notícia no noticiador</td>
						</tr>
						<tr>
							<td>#resumo#</td>
							<td>=></td>
							<td>resumo da notícia</td>
						</tr>
						<tr>
							<td>#thumbnail#</td>
							<td>=></td>
							<td>link do thumbnail</td>
						</tr>
						<tr>
							<td>#titulo#</td>
							<td>=></td>
							<td>título da notícia</td>
						</tr>
					</table>

					<p>
						Para mais parâmetros, consulte a biblioteca do plugin.
					</p>

								 ";

                    $headers = "MIME-Version: 1.0\r\n";
                    $headers .= "Content-type:text/html;charset=iso-8859-1\r\n";
                    $headers .= "From: Noticiador Web <bisa@bisa.com.br>\r\n";
                    //mail($para,$assunto,$mensagem,$headers);

                    if($model->getemail() != '') {
						print msg_alert("O código de grupo de notícias para o plugin de janela livre foi gerado com sucesso! Você receberá instruções no endereço de email informado.");
						Email::sendEmail($para, $assunto, $mensagem);
                    } else {
                    	print msg_alert("O código de grupo de notícias para o plugin de janela livre foi gerado com sucesso!");
                    }

                    print "<script>parent.location.href='".URL."index.php?action=show&secao=gestao_janela_livre'</script>";
                
                }else{
                    print msg_alert("O registro não foi salvo.".$this->getConexao()->getErro());
                }
            }else{
                print msg_alert("O registro não foi salvo.".$this->getConexao()->getErro());
            }
        
        
        } else {
            // UPDATE
            $sql = "UPDATE janela_livre SET ";
            $sql .="jan_liv_nome = '". $model->getjan_nome() ."'";
            $sql .=", jan_liv_quantidade = ". $model->getjan_quantidade() ."";
            $sql .=" WHERE  jan_liv_id = ". $model->getjanela_id()."";
            
            $error = $this->getConexao()->executeNonQuery($sql);
            if ($error!=-1){
                if($this->atualizarJanelaAreas($area, $model->getjanela_id())){
                
                    $codigo = htmlspecialchars('');
                    $para = $model->getemail();

                    $assunto = "Código e Instruções do Plugin de Notícias Janela Livre do NoticiadorWeb";

                    $mensagem = "

					<h1 class=\"default_title\">Plugin de Notícias Janela Livre do NoticiadorWeb</h1>

					<p>
						Você acabou de gerar um código para ser utilizado no plugin de notícias do NoticiadorWeb. O código gerado para o seu plugin é: <span style=\"font-size: 14px; font-weight: bold\">".$model->getjanela_id()."</span>
					</p>

					<p>
						Seguem as instruções para utilização do plugin de janela livre do NoticiadorWeb:
					</p>

					<p>
						Para utilizar o plugin de Janela Livre do NoticiadorWeb primeiro chame a biblioteca do plugin no site onde irá utilizá-lo a partir do endereço abaixo:
						<br />
						https://noticiadorweb.com.br/scripts/jquery.noticiadorweb.js

					</p>

					<p>
						Em seguida, crie uma <code>&lt;div&gt;</code> vazia na página e defina um id para a mesma.
					</p>

					<p>
						<code>
							&lt;div id=\"id\"&gt;&lt;/div&gt;
						</code>
					</p>

					<p>
						Depois, chame o plugin do NoticiadorWeb nesse id. Segue abaixo um exemplo de chamada do plugin.
					</p>

					<pre>
						<code>

	jQuery(document).ready(

		function(){

			jQuery('#nwebPdLDestaques').noticiadorWeb(
				{
					\"idScript\": 0
					, \"qtdNoticias\": 3
					, \"urlExibicaoNoticia\": 'https://www.siteondeserainstaladooplugin.com/noticias/index.php'
					, \"urlParametrosListagem\": true
					, \"classContainerNoticias\": 'nw_lista_noticias'
					, \"htmlContainerNoticias\": '' +
									'&lt;ul class=\&quot;nw_lista_noticias\&quot;&gt;' +
									'&lt;/ul&gt;'
					, \"htmlNoticia\": '' +
									'&lt;li class=\&quot;nw_noticia\&quot;&gt;' +
									'&lt;span class=\&quot;nw_thumbnail\&quot;&gt;' +
									'&lt;a class=\&quot;nw_link_noticia\&quot; href=\&quot;#linknoticia#\&quot; target=\&quot;_blank\&quot;&gt;' +
									'#thumbnail#' +
									'&lt;/a&gt;' +
									'&lt;/span&gt;' +
									'&lt;span class=\&quot;nw_titulo\&quot;&gt;' +
									'&lt;a class=\&quot;nw_link_noticia\&quot; href=\&quot;#linknoticia#\&quot; target=\&quot;_blank\&quot;&gt;' +
									'#titulo#' +
									'&lt;/a&gt;' +
									'&lt;/span&gt;' +
									'&lt;span class=\&quot;nw_resumo\&quot;&gt;' +
									'&lt;a class=\&quot;nw_link_noticia\&quot; href=\&quot;#linknoticia#\&quot; target=\&quot;_blank\&quot;&gt;' +
									'#resumo#' +
									'&lt;/a&gt;' +
									'&lt;span class=\&quot;nw_leiamais\&quot;&gt;' +
									'&lt;a class=\&quot;nw_link_noticia\&quot; href=\&quot;#linknoticiador#\&quot; target=\&quot;_blank\&quot;&gt;' +
									' Leia Mais...' +
									'&lt;/a&gt;' +
									'&lt;/span&gt;' +
									'&lt;/span&gt;' +
									'&lt;/li&gt;'
					, \"urlAltThumb\" : 'https://www.siteondeserainstaladooplugin.com/images/imagempadrao.png'
					, \"thumbWidth\": 305
					, \"thumbHeight\": 250
				}
			);

		}

	);

						</code>
					</pre>

					<p>
						Os parâmetros possíveis de se utilizar no noticiador são:
					</p>

					<table>
						<tr>
							<th style=\"font-weight: bold!important;\">'parametro' : 'valor padrão'</th>
							<th style=\"font-weight: bold!important;\">=></th>
							<th style=\"font-weight: bold!important;\">descrição</th>
						</tr>
						<tr>
							<td>'idScript' : ''</td>
							<td>=></td>
							<td>id do script gerado no NoticiadorWeb</td>
						</tr>
						<tr>
							<td>'qtdNoticias' : ''</td>
							<td>=></td>
							<td>quantidade de notícias a ser exibida no script (0 equivale a todas notícias da área)</td>
						</tr>
						<tr>
							<td>'htmlNoticia' : ''</td>
							<td>=></td>
							<td>html do modelo a ser exibido com tags descritas abaixo</td>
						</tr>
						<tr>
							<td>'thumbWidth' : 100</td>
							<td>=></td>
							<td>largura do thumbnail</td>
						</tr>
						<tr>
							<td>'thumbHeight' : 100</td>
							<td>=></td>
							<td>altura do thumbnail</td>
						</tr>
						<tr>
							<td>'paginacao' : false</td>
							<td>=></td>
							<td>se o script possui paginação</td>
						</tr>
						<tr>
							<td>'pgQtdNoticiasPagina' : 1</td>
							<td>=></td>
							<td>quantidade de notícias a ser exibida por página</td>
						</tr>
						<tr>
							<td>'pgQtdPaginasExibidas' : 5</td>
							<td>=></td>
							<td>quantidade de páginas a ser exibida no script antes das setas</td>
						</tr>
						<tr>
							<td>'pgBorda' : false</td>
							<td>=></td>
							<td>número da página tem borda</td>
						</tr>
						<tr>
							<td>'pgBordaCor' : 'none'</td>
							<td>=></td>
							<td>cor da borda no número da página</td>
						</tr>
						<tr>
							<td>'pgBordaHoverCor' : 'none'</td>
							<td>=></td>
							<td>cor da borda no número da página no hover</td>
						</tr>
						<tr>
							<td>'pgTextoCor' : '#666'</td>
							<td>=></td>
							<td>cor do texto no número da página</td>
						</tr>
						<tr>
							<td>'pgFundoCor' : 'none'</td>
							<td>=></td>
							<td>cor do fundo do texto no número da página</td>
						</tr>
						<tr>
							<td>'pgTextoHoverCor' : '#000'</td>
							<td>=></td>
							<td>cor do texto no número da página no hover</td>
						</tr>
						<tr>
							<td>'pgFundoHoverCor' : 'none'</td>
							<td>=></td>
							<td>cor do fundo do texto no número da página no hover</td>
						</tr>
					</table>

					<p>
						O html pode conter as seguintes tags:
					</p>

					<table>
						<tr>
							<td>#data#</td>
							<td>=></td>
							<td>data de postagem da notícia</td>
						</tr>
						<tr>
							<td>#id#</td>
							<td>=></td>
							<td>id da notícia</td>
						</tr>
						<tr>
							<td>#link#</td>
							<td>=></td>
							<td>link informado no cadastro da notícia</td>
						</tr>
						<tr>
							<td>#linknoticiador#</td>
							<td>=></td>
							<td>link da notícia no noticiador</td>
						</tr>
						<tr>
							<td>#resumo#</td>
							<td>=></td>
							<td>resumo da notícia</td>
						</tr>
						<tr>
							<td>#thumbnail#</td>
							<td>=></td>
							<td>link do thumbnail</td>
						</tr>
						<tr>
							<td>#titulo#</td>
							<td>=></td>
							<td>título da notícia</td>
						</tr>
					</table>

					<p>
						Para mais parâmetros, consulte a biblioteca do plugin.
					</p>

								 ";

                    $headers = "MIME-Version: 1.0\r\n";
                    $headers .= "Content-type:text/html;charset=iso-8859-1\r\n";
                    $headers .= "From: Noticiador Web <bisa@bisa.com.br>\r\n";
                    //mail($para,$assunto,$mensagem,$headers);
                    
                    Email::sendEmail($para, $assunto, $mensagem);
                    
                    print msg_alert("As alterações no grupo de notícias para o plugin de janela livre foram realizadas com sucesso! Você receberá instruções no endereço de email informado.");
                    print "<script>parent.location.href='".URL."index.php?action=show&secao=gestao_janela_livre'</script>";
                
                }else{
                    print msg_alert("O registro não foi salvo.".$this->getConexao()->getErro());
                }
            
            }else{
                print msg_alert("O registro não foi salvo.".$this->getConexao()->getErro());
            }
        
        }	
    }

    function load($key) {
        $sql = "SELECT * FROM janela_livre WHERE jan_liv_id = ".$key;
		
		$table = $this->getConexao()->executeQuery($sql);
		if ($table->RowCount() > 0) {	
			$result = new janela_livre();
			$result->bind($table->getRow(0));
			if ($result != null){					
          $view = new view_janela_livre($this);
          $view->setModel($result);
          $view->show();
			}
		} 
		return null;        
    }

    function delete($key) {
        
        $sql = "DELETE FROM janela_livre WHERE jan_liv_id = ".$key;
		$error = $this->getConexao()->executeNonQuery($sql);
    
        $sql = "DELETE FROM janela_livre_area WHERE janela_livre_id = ".$key;
		$error2 = $this->getConexao()->executeNonQuery($sql);
    
		if ($error != -1 || $error2 != -1) {
			$controller_arquivo = new controller_arquivo();
        	$controller_arquivo->setConexao(TConexao::getInstance());
        
			print msg_alert("Código excluído com sucesso!");
			$url = "index.php?".PARAMETER_NAME_ACTION."=show&";
			$url .= PARAMETER_NAME_FILE."=gestao_janela_livre";

			print "<script>parent.location.href='".$url."'</script>";
		} else {
			print msg_alert("O código não pôde ser excluído. ".$this->getConexao()->getErro());
		}
        
    }

    function create() {}
    
    function atualizarJanelaAreas($area, $janela_id) {
        $sql = 'DELETE FROM janela_livre_area WHERE janela_livre_id='.$janela_id;		
        $this->getConexao()->executeNonQuery($sql);		

		for($i=0;$i<sizeof($area);$i++){										
			$sql = 'INSERT INTO janela_livre_area(janela_livre_id, area_id) values('.$janela_id.','.$area[$i].')';
			$this->getConexao()->executeNonQuery($sql);
		}
		return true;

    }
	
    /**
     * Obter as áreas associadas para cada cliente passado no parâmetro cliente. As áreas sem associação com cliente receberão como parâmetro "COLAB"
     * Caso seja passado o parâmetro usuario, a função retorna os clientes do usuário informado.
     * Caso não seja passado o parâmetro usuario, a função retorna os clientes do usuário atualmente logado no sistema.
     * André Alves - 27/01/2012
     * 
     * @param string $cliente Id do cliente
     * @param int $usuario Id do usuário
     * @return object Lista com os clientes
     */
    /*function obterAreasDoUsuario($cliente = '', $usuario = '') { Função deslocada para o TController - Jorge Roberto Mantis 6383
        
        $where = '';
        
        if($usuario == '') {
            $res = controller_seguranca::getInstance()->identificarUsuario();
            $usuario = $res->getID();    
        }
        
        if($cliente != '') {
            $where .= ' AND Clie_Codigo = "'.$cliente.'"';
        }
        
        $sql = 'SELECT 
                  area_id,
                  are_descricao,
                  are_observacao 
                FROM
                  vw_usuarios_areas_clientes 
                WHERE usuario_id = '.$usuario.
                $where.'                   
                ORDER BY are_descricao';
        
        $result = $this->getConexao()->executeQuery($sql);
        
        if ($result != null) {
            return $result;
        }
        
        return null;
    }*/
    
    
    /**
     * Obter a lista dos clientes associados ao usuário.
     * Caso seja passado o parâmetro usuario, a função retorna os clientes do usuário informado.
     * Caso não seja passado o parâmetro usuario, a função retorna os clientes do usuário atualmente logado no sistema.
     * André Alves - 27/01/2012
     * 
     * @param int $usuario Id do usuário
     * @return object Lista com os clientes
     */
    function obterClientesDoUsuario($usuario = '') {
        
        if($usuario == '') {
            $res = controller_seguranca::getInstance()->identificarUsuario();
            $usuario = $res->getID();    
        }        
        
        $sql = 'SELECT 
                  CLIE_Codigo,
                  CLIE_Nome 
                FROM
                  vw_usuarios_areas_clientes 
                WHERE usuario_id = '.$usuario.' 
                GROUP BY CLIE_Codigo,CLIE_Nome';
        
        $result = $this->getConexao()->executeQuery($sql);
        
        if ($result != null) {
            return $result;
        }
        
        return null;
    
    }
    
    function checarJanelaArea($janela_id, $area_id){
	   
        $sql = "SELECT * FROM janela_livre_area WHERE janela_livre_id = ". $janela_id ." and area_id = ".$area_id;
		$janela = $this->getConexao()->executeQuery($sql);

        if ($janela->RowCount() > 0) {
        	return " checked";
        }
        
        return "";
	}
    
    
    function process(){
 		parent::process();

		switch($this->getAction()) {
			 case "save":
	 			$usuario = controller_seguranca::getInstance()->identificarUsuario();
				
				$save = new janela();
				if (isset($_POST["id"])) {
				    $save->setjanela_id($_POST["id"]);
				}
				$save->setjan_nome($_POST["Fjan_nome"]);	
				$save->setjan_quantidade($_POST["Fjan_quantidade"]);
				if ($usuario != null){
					$save->setusuario_id($usuario->getID());
				}else{
					$save->setusuario_id(1);
				}
                $save->setemail($_POST["Fjan_email"]);  
				$this->save($save);
				break;
		}
	 }

} // fim classe

?>
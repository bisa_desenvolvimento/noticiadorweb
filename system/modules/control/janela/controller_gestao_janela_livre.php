<?
class controller_gestao_janela_livre extends TController
{
	function show() {
			$view = new view_gestao_janela_livre($this);
			$view->show();	
	}
	function save($model){
		
	}
	
	function load($key){
	}
	
	function delete($key){
		
	}
	
	function create(){
		
	}	

	
	function obterTodos() {
		$usuario = controller_seguranca::getInstance()->identificarUsuario();
        $sql = 'SELECT janela_livre.*, usuario.usu_email FROM janela_livre, usuario WHERE janela_livre.usuario_id = usuario.usuario_id ';
		$sql .= '  AND jan_liv_id IS NOT NULL ';
		if ((isset($_POST['Fjan_nome']))&&($_POST['Fjan_nome']!="")){
			$sql .= " AND jan_liv_nome LIKE '%".$_POST['Fjan_nome']."%'";
		}
		if ($usuario != null)
			$sql .= ' AND usuario.usuario_id ='.$usuario->getID();
		$sql .= ' ORDER BY jan_liv_id';

        $table = $this->getConexao()->executeQuery($sql);
        
        if ($table->RowCount() > 0) {
        	return $table;
        }
        
        return null;
    }
	
	function obterAreas($janela_id="") {
	
        $sql = 'SELECT * FROM area ';
		
		if  ($janela_id!=""){
			$sql .= ' WHERE area_id in(SELECT area_id FROM janela_livre_area WHERE janela_livre_id='.$janela_id.')';
		}
		
		$sql .= ' ORDER BY are_descricao ';
		
        $table_area = $this->getConexao()->executeQuery($sql);

        if ($table_area->RowCount() > 0) {
        	return $table_area;
        }
        
        return null;
    }

}
?>
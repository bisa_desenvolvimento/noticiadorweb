<?php

class Controller_estatistica_email_por_area extends TController {
	
    function show() { 
	$view = new view_estatistica_email_por_area($this);
	$view->show();
    }

    function save($model) {}

    function load($key) {}

    function delete($key) {}

    function create() {}
	
    function obterAreas(){
        $usuario = controller_seguranca::getInstance()->identificarUsuario();
                
        $sql = "SELECT 
                  area_id,
                  are_descricao 
                FROM
                  area
                WHERE area_id IN 
                  (SELECT 
                    area_id 
                  FROM
                    `vw_usuarios_areas_clientes` 
                  WHERE tipo = 'C' 
                    AND usuario_id = ".$usuario->getId().")";
        $result = $this->getConexao()->executeQuery($sql);
        return $result;
    }
    
    function obterQuantidadeEmails($area) {
        $sql = 'SELECT 
                  COUNT(DISTINCT C.ass_email) AS total
                FROM
                  assinante_area B 
                  INNER JOIN assinantes C 
                    ON C.assinante_id = B.assinante_id 
                WHERE B.excluido = 0 
                  AND B.area_id = '.$area;
        $table = $result = $this->getConexao()->executeQuery($sql);
        $row = $table->getRow(0);
        return $row->total;
    }

} // fim classe

?>
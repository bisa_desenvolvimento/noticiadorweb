<?

//error_reporting(0);

class controller_gestao_resumo_newsletter extends TController {

	function show() {
		$view = new view_gestao_resumo_newsletter($this);
		$view->show();
	}

	function save($model){

	}

	function load($key){

		$conEmail = mysqli_connect(DB_SERVER, DB_USER, DB_PASS,DB_DATASET) or die(mysqli_error($conEmail));
    	$dbEmail  = mysqli_select_db($conEmail,DB_DATASET_BISAWEB)or die(mysqli_error($conEmail));

		$chave = $_GET['chave'];
		$usuario = $_SESSION['USUARIO'];

		$sql = "SELECT
				  titulo,
				  MIN(envio_email.data) AS data_inicio,
				  MAX(envio_email.data_envio) AS data_final,
				  usuario,
				  COUNT(id) AS total_email,
				  SUM(CASE WHEN STATUS = 2 THEN 1 ELSE 0 END) AS total_enviados
				FROM
				  envio_email 
				WHERE envio_email.usuario = '".$usuario."'
				AND envio_email.chave = '".$chave."'";
				$table = mysqli_query($conEmail,$sql);
				$row = mysqli_fetch_object($table);

		mysqli_close($conEmail);
		//var_dump($sql);die();

		$usuario_logado = $row->usuario;
		$titulo_newsletter = $row->titulo;

		$data_inicio =  date("d/m/Y G:i:s",strtotime($row->data_inicio));
		$data_fim = date("d/m/Y G:i:s",strtotime($row->data_final));
		$inicio = DateTime::createFromFormat('d/m/Y H:i:s', $data_inicio);
		$fim = DateTime::createFromFormat('d/m/Y H:i:s', $data_fim);
		$intervalo = $inicio->diff($fim);

		$conteudo = '

		<b>Prezado(a) Editor(a)/Administrador(a):</b> '.$row->usuario.',<br><br>

		O NoticiadorWeb acabou de despachar a sua newsletter e abaixo você tem um resumo do que foi enviado:<br><br>

		<b>Titulo da news: </b>'./*utf8_decode(*/$row->titulo/*)*/.'<br>
		<b>Total de emails despachados: </b>'.$row->total_email.'.<br>
		<b>Quantidade de emails enviados: </b>'.$row->total_enviados.'.<br>
		<b>Quantidade de emails não enviados: </b>'.($row->total_email - $row->total_enviados).'.<br>
		<!--<b>Esta notícia já foi visualizada </b> vezes.<br><br>-->

		<b>Início do envio: </b>'.date("d-m-Y G:i:s",strtotime($row->data_inicio)).'.<br>
		<b>Final do envio: </b>'.date("d-m-Y G:i:s",strtotime($row->data_final)).'.<br>
		<b>Tempo do processo de envio da newsletter:</b> '.$intervalo->format(
			'%D dia(s), %H hora(s), %I minuto(s) e %S segundo(s).').'<br><br>



		<!-- Quaisquer dúvidas, críticas ou sugestões, enviar email para: <b>suporte@bisa.com.br</b> ou acesse nosso site: <b>www.bisa.com.br</b> e faça contato.<br><br>

		Atenciosamente,<br><br>

		<b>BisaWeb Tecnologia</b><br><br><br>-->

		';

		//$conteudo .= '<font size="1" face="verdana, arial">Caso você não esteja visualizando, <a href="http://noticiadorweb.com.br/users/historico/'.$chave.'.html" title="Veja no seu browser" target="_blank">clique aqui</a></font><br><br>';

		$pagina = "https://www.noticiadorweb.com.br/users/historico/".$chave.".html";

		$conteudo_pagina = file_get_contents($pagina, FILE_TEXT);

		if (isset($_GET['emails_destino'])) {

			$conteudo_email = $conteudo;

			$conteudo_email .= utf8_decode($conteudo_pagina);

			$email_from = $usuario_logado;
			$emails_to = $_GET['emails_destino'];
			$email_replay_to = $usuario_logado;

			if ($this->enviarRelatorio($chave, $email_from, $emails_to, $email_replay_to, $titulo_newsletter, $conteudo_email)) {
				echo "Email enviado com sucesso!<br><br>";
			} else {
				echo "Erro durante a tentativa de envio, favor tentar novamente!<br><br>";
			}

			//require_once(DIR_PLUGINS . SMTP_LIB);

			//$email = $_GET['email'];
			//$to = $email;

			/*
			$subject = $row->titulo;
			$headers = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html';
			$headers .= 'charset=iso-8859-1' . '\r\n';
			$headers .= 'X-Priority: 1\r\n';
			$headers .= 'From: NoticiadorWeb <news@' . STMP_HOST . '>\r\n';
			$headers .= 'Return-Path: ' .  $email . '\r\n';
			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPAuth = SMTP_AUTH;
			$mail->Host = STMP_HOST;
			$mail->Port = SMTP_PORT;
			$mail->Username = SMTP_USER;
			$mail->Password = SMTP_PASS;

			$mail->SetFrom('news@' . STMP_HOST, 'NoticiadorWeb');
			$mail->Subject = $subject;
			$mail->MsgHTML($conteudo);
			$mail->AddAddress($to);
			*/

		}

		$conteudo = utf8_encode($conteudo);

		$conteudo .= $conteudo_pagina;

		echo $conteudo;
	}

	function enviarRelatorio($chave, $email_envio, $emails_destino, $email_resposta, $titulo, $conteudo) {

		$headers = "MIME-Version: 1.0\r\n";
		$headers .="X-mailer: PHP/" . phpversion() . "\r\n";
		$headers .="X-Priority: 1\r\n";
		$headers .="Content-Type: text/html; charset=ISO-8859-1\r\n";
		
		$headers .="Reply-To: " . $email_resposta . "\r\n";

		$headers .= "To: " . $emails_destino . "\r\n";
		$headers .= "From: " . $email_envio . "\r\n";
		$headers .= "Return-Path: " . $email_envio . "\r\n";

		$title = "RELATÓRIO DE ENVIO DA NEWSLETTER: ".$titulo;

		$conteudo = "<br/><font size='1' face='verdana, arial'>Caso você não esteja visualizando, <a href='".URL.DIR_USERS . "historico/$chave.html' title='Veja no seu browser' target='_blank'>clique aqui</a></font><br/><br/>".$conteudo;

		if (mail($email_envio, $title, $conteudo, $headers)) {

			return true;
		} else {
			return false;
		}
	}

	function delete($key){

	}

	function create(){

	}

	function obterNoticias() {

		$usuario = controller_seguranca::getInstance()->identificarUsuario();

		$usuario = $_SESSION['USUARIO'];

		$conEmail = mysqli_connect(DB_SERVER, DB_USER, DB_PASS,DB_DATASET) or die(mysqli_error($conEmail));
    	$dbEmail  = mysqli_select_db($conEmail,DB_DATASET)or die(mysqli_error($conEmail));

		$sql = "SELECT  DISTINCT chave, email, data, titulo, COUNT(chave) total, SUM(CASE WHEN STATUS = 2 THEN 1 ELSE 0 END) enviados, SUM(CASE WHEN abrir_email IS NOT NULL THEN 1 ELSE 0 END) visualizados FROM envio_email WHERE usuario = '".$usuario."' GROUP BY chave ORDER BY DATA DESC";

		$result = mysqli_query($conEmail,$sql);

		$table = array();

		while ($row = mysqli_fetch_object($result)) {
			$table[] = $row;
		}

		/*
		echo "<pre>";
		var_dump($table);
		die();
		var_dump($sql);
		die();
		*/
		/*
		$table = $this->getConexao()->executeQuery($sql);
		if ($table->RowCount() > 0) {
			return $table;
		}
		*/

		mysqli_close($conEmail);

		return $table;

	}

	function carregarnoticia(){
		$view = new view_gestao_resumo_newsletter($this);
		$result = $view->montarTabelaNoticiasCadastradas();
		echo $result;
	}

	function process() {
		parent::process();
		switch ($this->getAction()) {
			case "carregarnoticia":
			$this->carregarNoticia();
			break;

		}

	}
}
?>
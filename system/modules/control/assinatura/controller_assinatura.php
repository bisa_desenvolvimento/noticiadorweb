<?
class Controller_assinatura extends TController
{
	function show() {
	         $view = new view_assinatura($this);
	         $view->show();
	}

	function save($model){
	 
   $area = "";
		if (isset($_POST['area'])){
			$area = $_POST['area'];
		}
   
		if ($model->getID() == null) {
			$sql = "INSERT INTO assinatura (
					usuario_id
					, ass_corTexto
					, ass_labelTexto
					, ass_fonteTexto
					, ass_corBotao
					, ass_labelBotao
					, ass_larguraCaixa
                    , ass_email
					)";
			$sql .=" VALUES (".$model->getusuario_id()." , '"
          .$model->getass_corTexto()."','"
					.$model->getass_labelTexto()."','"
					.$model->getass_fonteTexto(). "', '"
					.$model->getass_corBotao(). "', '"
					.$model->getass_labelBotao()."', "
					.$model->getass_larguraCaixa().", '"
                    .$model->getass_email()."')";
			$error = $this->getConexao()->executeNonQuery($sql);
			$assinatura_id=$this->getConexao()->getLastID();
      if ($error!=-1){
        
          for($i=0;$i<sizeof($area);$i++){
						$sql = 'INSERT INTO assinatura_area(assinatura_id, area_id) values('.$assinatura_id.','.$area[$i].')';
            $error = $this->getConexao()->executeNonQuery($sql);
          }     
          
   		 	if ($error!=-1){
			     
            $codigo = $this->geraCodigo($assinatura_id);
            $para = $model->getass_email();
            //se foi preenchido o email é enviado
            if(strlen($para)!=0){
            $assunto = "Código de assinatura do Noticiador Web";
            $mensagem = "<strong>Este é o código gerado pelo Noticiador Web. Insira o código abaixo no local desejado de sua página.</strong><br /><br />".str_replace('<','&lt;',$codigo);
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type:text/html;charset=iso-8859-1\r\n";
            $headers .= "From: Noticiador Web <relatorio@" . STMP_HOST . ">\r\n";
            Email::sendEmail($para,$assunto,$mensagem);
            //mail($para,$assunto,$mensagem,$headers);
            }
            print msg_alert("Código gerado com sucesso!");
          	print "<script>window.location.href='index.php?action=show&secao=gestao_assinatura'</script>";
    			}
      
      }else{
				print msg_alert("Não foi possível gerar o código. ".$this->getConexao()->getErro());		?>

<?			}


		} else {
		    // UPDATE
			$sql = "UPDATE assinatura SET ";
			$sql .="ass_labelTexto = '". $model->getass_labelTexto() ."'";
			$sql .=",ass_fonteTexto = '". $model->getass_fonteTexto()  ."'";
			$sql .=",ass_corBotao = '". $model->getass_corBotao() ."'";
			$sql .=",ass_labelBotao = '". $model->getass_labelBotao() ."'";
			$sql .=",ass_corTexto = '". $model->getass_corTexto() ."'";
			$sql .=",ass_larguraCaixa = ". $model->getass_larguraCaixa() ."";
            $sql .=",ass_email = '". $model->getass_email() ."'";
			$sql .=" WHERE  assinatura_id = ". $model->getID()."";

			$error = $this->getConexao()->executeNonQuery($sql);
			if ($error!=-1){
			 
        $sql = 'DELETE FROM assinatura_area WHERE assinatura_id = '.$model->getID();
            $error = $this->getConexao()->executeNonQuery($sql);
            
        for($i=0;$i<sizeof($area);$i++){
  				$sql = 'INSERT INTO assinatura_area(assinatura_id, area_id) values('.$model->getID().','.$area[$i].')';
          $error = $this->getConexao()->executeNonQuery($sql);
        }
       
        print msg_alert("Código alterado com sucesso!");
        $controller_arquivo = new controller_arquivo();
        $controller_arquivo->setConexao(TConexao::getInstance());

				$url = "index.php?".PARAMETER_NAME_ACTION."=show&";
				$url .= "&".PARAMETER_NAME_FILE."=gestao_assinatura";

				print "<script>parent.location.href='".$url."'</script>";//redirect($url);
			}else{
				print msg_alert("Não foi possível alterar o código. ".$this->getConexao()->getErro());
			}
		}
	}
  
  function obterCodigo() {
    $sql = 'SELECT MAX(assinatura_id)+1 AS cod FROM assinatura;';
    $table = $this->getConexao()->executeQuery($sql);
    $row = $table->getRow(0);
    return $row->cod;
  }

	function load($key){ 
		$sql = "SELECT * FROM assinatura WHERE assinatura_id= ".$key;
		
		$table = $this->getConexao()->executeQuery($sql);
		if ($table->RowCount() > 0) {	
			$result = new assinatura();
			$result->bind($table->getRow(0));
			if ($result != null){					
          $view = new view_assinatura($this);
          $view->setModel($result);
          $view->show();
			}
		} 
		return null;

	}

	function delete($key){
		
		$sql = "DELETE FROM assinatura WHERE assinatura_id = ".$key;
               $error = $this->getConexao()->executeNonQuery($sql);
               $sql = "DELETE FROM assinatura_area WHERE assinatura_id = ".$key;
		$error1 = $this->getConexao()->executeNonQuery($sql);
           	if (($error != -1) && ($error1 != -1)) {
			$controller_arquivo = new controller_arquivo();
        	$controller_arquivo->setConexao(TConexao::getInstance());
        
			print msg_alert("Código excluído com sucesso!");
			$url = "index.php?".PARAMETER_NAME_ACTION."=show&";
			$url .= PARAMETER_NAME_FILE."=gestao_assinatura";

			print "<script>parent.location.href='".$url."'</script>";
		} else {
			print msg_alert("O código não pôde ser excluído. ".$this->getConexao()->getErro());
		}

	}

	function create(){ }
  
  function checarAssinaturaArea($assinatura_id, $area_id){
	$sql = "SELECT * FROM assinatura_area WHERE assinatura_id = ". $assinatura_id ." and area_id=".$area_id;
        $janela = $this->getConexao()->executeQuery($sql);
        if ($janela->RowCount() > 0) {
        	return " checked";
        }
        return "";
	}

	function geraCodigo($assinatura_id){
    $controller_arquivo = new controller_arquivo();
    $controller_arquivo->setConexao(TConexao::getInstance());
    $sql = 'SELECT * FROM assinatura WHERE assinatura_id='.$assinatura_id;
    $table_assinatura = $this->getConexao()->executeQuery($sql);
    $codigo_gerado = "Não foi possível gerar o código para assinatura da newsletter";
    if ($table_assinatura->RowCount() > 0) {
  		$row_assinatura = $table_assinatura->getRow(0);
  		$url = URL.'index.php?'.PARAMETER_NAME_FILE.'=assinar_newsletter&'.PARAMETER_NAME_ACTION.'=save';
      $codigo_gerado = '<div id="newsletter"><br />';
      $codigo_gerado .= '<form name="frm_news" id="frm_news" method="post" action="'.$url.'">';
      $codigo_gerado .= '<input name="assinatura" type="hidden" value="'.$assinatura_id.'" />';
      $codigo_gerado .= '<label style="color:#'.$row_assinatura->ass_corTexto.';">'.$row_assinatura->ass_labelTexto.'</label><br />';
      $codigo_gerado .= '<input name="nome" type="text" value="Seu nome" size="'.$row_assinatura->ass_larguraCaixa.'" /><br />';
      $codigo_gerado .= '<input name="email" type="text" value="Seu email" size="'.$row_assinatura->ass_larguraCaixa.'" />';
      $table_areas = $this->obterAreasAssinatura($assinatura_id);
      for($i = 0; $i < $table_areas->RowCount(); $i++) {
        $row = $table_areas->getRow($i);
        $codigo_gerado .= '<input name="areas[]" type="hidden" value="'.$row->area_id.'" />';
      }
      $codigo_gerado .= '<input name="enviar" type="button" style="color:#'.$row_assinatura->ass_fonteTexto.'; background-color:#'.$row_assinatura->ass_corBotao.'; border:1px #'.$row_assinatura->ass_corBotao.' solid" value="'.$row_assinatura->ass_labelBotao.'"  onclick="is_email(email.value);" />';
      $codigo_gerado .= '</form>';
      $codigo_gerado .= '</div>';
      $codigo_gerado .= '<script>';
      $codigo_gerado .= 'function is_email(email){ er = /^[a-zA-Z0-9][a-zA-Z0-9._-]+@([a-zA-Z0-9._-]+.)[a-zA-Z-0-9]{2}/;';
      $codigo_gerado .= '  if(er.exec(email)) {';
      $codigo_gerado .= '    document.frm_news.submit();';
      $codigo_gerado .= '  } else { ';
      $codigo_gerado .= '    alert("E-mail inválido.");'; 
      $codigo_gerado .= '  } ';
      $codigo_gerado .= '}</script>';
    }
    return $codigo_gerado;
	}

	function obterAreas() {
            $usuario = controller_seguranca::getInstance()->identificarUsuario();
            $sql = 'SELECT area.* FROM area, area_usuario
        		WHERE AREA.area_id = area_usuario.area_id
        		AND area_usuario.usuario_id = '.$usuario->getID();
	          $sql .= ' order by are_descricao';
            $table_area = $this->getConexao()->executeQuery($sql);

            if ($table_area->RowCount() > 0) {
        	return $table_area;
            }

            return null;
        }
        
  function obterAreasAssinatura($assinatura_id) {
            $usuario = controller_seguranca::getInstance()->identificarUsuario();
            $sql = 'SELECT area_id FROM assinatura_area WHERE assinatura_id = '.$assinatura_id;
	          $table_area = $this->getConexao()->executeQuery($sql);

            if ($table_area->RowCount() > 0) {
        	return $table_area;
            }

            return null;
        }

	function process(){
 		parent::process();

		switch($this->getAction()) {
			 case "save":
	 			$usuario = controller_seguranca::getInstance()->identificarUsuario();
				$save = new assinatura();
				if (isset($_POST["id"]))
				$save->setID($_POST["id"]);
				$save->setass_corTexto($_POST["Fass_corTexto"]);
				$save->setass_labelTexto($_POST["Fass_labelTexto"]);
				$save->setass_fonteTexto($_POST["Fass_corTextoBotao"]);
				$save->setass_corBotao($_POST["Fass_corBotao"]);
				$save->setass_labelBotao($_POST["Fass_labelBotao"]);
				$save->setass_larguraCaixa($_POST["Fass_larguraCaixa"]);
				$save->setass_email($_POST["Fass_email"]);
                $save->setusuario_id($usuario->getID());
                $this->save($save);
				break;

                                case "optin":
                                $this->setOptin($_POST["txtoptin"]);
                                break;
		}
	 }

         public function getOptin($envia = false) {
        $usuario = controller_seguranca::getInstance()->identificarUsuario();

        $arquivo = DIR_DATA.'mensagensoptin/'.$usuario->getEmail().'.txt';
        if(file_exists($arquivo)) {

            $fp = fopen($arquivo, "r");
            $texto = '';
            while(!feof($fp)) {
                $texto .= fgetc($fp);
            }
            fclose($fp);
        } else {
            $fp = fopen(DIR_DATA.'mensagensoptin/padrao-'.$_SESSION['SESSION_LANG'].'.txt', "r");
            $texto = '';
            while(!feof($fp)) {
                $texto .= fgetc($fp);
            }
            fclose($fp);
        }

        //$texto = utf8_decode($texto);

        if($envia) {
            $texto .= translate('<br /><br /><br />Se você não clicar no link <a href="[link]" target="_blank">[link]</a> estamos presumindo que você deseja continuar a receber nossas informações, e será um prazer manter o seu endereço em nosso cadastro.');
            $link = URL.'index.php?action=show&secao=remover_usuario_email&id='.$email;
            $tags = array('[link]','[nome]','[email]');
            $valores = array($link,$usuario->getNome(),$usuario->getEmail());
            $mensagem = str_replace($tags, $valores, $texto);
            return $mensagem;
        }

        return $texto;
    }

    private function setOptin($texto) {
        $usuario = controller_seguranca::getInstance()->identificarUsuario();
        $arquivo = DIR_DATA.'mensagensoptin/'.$usuario->getEmail().'.txt';
        $fp = fopen($arquivo, "w");
        if(fwrite($fp, $texto)) {
            echo 'ok';
        }else{
            echo 'erro';
        }
        fclose($fp);
    }
    
    /**
     * Obter as áreas associadas para cada cliente passado no parâmetro cliente. As áreas sem associação com cliente receberão como parâmetro "COLAB"
     * Caso seja passado o parâmetro usuario, a função retorna os clientes do usuário informado.
     * Caso não seja passado o parâmetro usuario, a função retorna os clientes do usuário atualmente logado no sistema.
     * André Alves - 27/01/2012
     * 
     * @param string $cliente Id do cliente
     * @param int $usuario Id do usuário
     * @return object Lista com os clientes
     */
    /*function obterAreasDoUsuario($cliente = '', $usuario = '') { Função deslocada para o TController - Jorge Roberto Mantis 6383
        
        $where = '';
        
        if($usuario == '') {
            $res = controller_seguranca::getInstance()->identificarUsuario();
            $usuario = $res->getID();    
        }
        
        if($cliente != '') {
            $where .= ' AND Clie_Codigo = "'.$cliente.'"';
        }
        
        $sql = 'SELECT 
                  area_id,
                  are_descricao,
                  are_observacao 
                FROM
                  vw_usuarios_areas_clientes 
                WHERE usuario_id = '.$usuario.
                $where.'                   
                ORDER BY are_descricao';
        
        $result = $this->getConexao()->executeQuery($sql);
        
        if ($result != null) {
            return $result;
        }
        
        return null;
    }*/
    
    
    /**
     * Obter a lista dos clientes associados ao usuário.
     * Caso seja passado o parâmetro usuario, a função retorna os clientes do usuário informado.
     * Caso não seja passado o parâmetro usuario, a função retorna os clientes do usuário atualmente logado no sistema.
     * André Alves - 27/01/2012
     * 
     * @param int $usuario Id do usuário
     * @return object Lista com os clientes
     */
    function obterClientesDoUsuario($usuario = '') {
        
        if($usuario == '') {
            $res = controller_seguranca::getInstance()->identificarUsuario();
            $usuario = $res->getID();    
        }        
        
        $sql = 'SELECT 
                  CLIE_Codigo,
                  CLIE_Nome 
                FROM
                  vw_usuarios_areas_clientes 
                WHERE usuario_id = '.$usuario.' 
                GROUP BY CLIE_Codigo,CLIE_Nome';
        
        $result = $this->getConexao()->executeQuery($sql);
        
        if ($result != null) {
            return $result;
        }
        
        return null;
    
    }


}
?>
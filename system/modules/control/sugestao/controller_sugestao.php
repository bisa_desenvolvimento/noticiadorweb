<?php

class Controller_sugestao extends TController {
	
    function show() {
        $view = new view_sugestao($this);
    	$view->show();
    }

    function save($model) {
        
        $controller_arquivo = new controller_arquivo();
        $controller_arquivo->setConexao(TConexao::getInstance());
        $url="";
        $area = null;
        
        if(isset ($_POST['area'])) {
            $area = $_POST['area'];    
        }
        
        $thumbnail_anterior="";
        $sql = "UPDATE noticia SET not_titulo='".$model->getnot_titulo()."', not_publicacao='".$model->getnot_publicacao()."', not_validade='".$model->getnot_validade()."', 
                not_chamada='".$model->getnot_chamada()."', not_texto='".$model->getnot_texto()."', not_link='".$model->getnot_link()."' , not_autorizada=".$model->getnot_autorizada().", 
                not_thumbnail='".$thumbnail_anterior."' WHERE  noticia_id= ".$model->getnoticia_id();
        $id = "&id=".$model->getnoticia_id();		
        $error = $this->getConexao()->executeNonQuery($sql);
                
        if ($error != -1) {
        
            for($i=0;$i<sizeof($area);$i++){										
                $sql = 'INSERT INTO noticia_area (noticia_id, area_id,not_aut) values('.$model->getnoticia_id().','.$area[$i].','.$model->getnot_autorizada(0).')';
                $error = $this->getConexao()->executeNonQuery($sql);
            }
              
            echo msg_alert("Notícia salva com sucesso!");
            print "<script>window.location.href='".URL."'</script>";

        } else {
            print msg_alert("Problema ao salvar a notícia, por favor tente bovamente mais tarde.".$this->getConexao()->getErro());
        }
        
    }

    function load($key) {
        
        $sql = "SELECT * FROM noticia WHERE noticia_id= ".$key;

		$table = $this->getConexao()->executeQuery($sql);
		if ($table->RowCount() > 0) {	
			$result = new noticia();
			$result->bind($table->getRow(0));
			if ($result != null){					
                $view = new view_sugestao($this);
                $view->setModel($result);
                $view->show();
			}
		} 
		return null;
        
    }

    function delete($key) {}

    function create() {}
    
    function busca() {
        $view = new view_sugestao($this);
    	$view->busca();
    }
    
    function inserirNoticia($noticia) {
        
        $sql = "INSERT INTO noticia (
                  not_autor_id,
                  not_data,
                  not_titulo,
                  not_chamada,
                  not_texto,
                  not_publicacao,
                  not_validade,
                  not_autorizada,
                  not_link,
                  not_thumbnail
                ) 
                VALUES
                  (
                    (SELECT 
                      usuario_id 
                    FROM
                      usuario 
                    WHERE usu_login = '".$noticia['autor']."'),
                    '".$noticia['publicacao']."',
                    '".$noticia['titulo']."',
                    '".$noticia['resumo']."',
                    '".$noticia['noticia']."',
                    '".$noticia['publicacao']."',
                    '".$noticia['limite']."',
                    '2',
                    '',
                    ''
                  )";
        
        $this->getConexao()->executeNonQuery($sql);
        return $this->getConexao()->getLastID();
            
    }
    
    private function existeUsuario($user, $pass) {
        $sql = 'SELECT 
                  usuario_id 
                FROM
                  usuario 
                WHERE usu_login = "'.$user.'" 
                  AND usu_senha = "'.$pass.'"';
                  
        $usuario = $this->getConexao()->executeQuery($sql);

        if ($usuario->RowCount() > 0) {
            $row = $usuario->getRow(0);
            return $row->usuario_id;
        }
        return false;
    } 
    
    /**
     * Obter a lista dos clientes associados ao usuário.
     * Caso seja passado o parâmetro usuario, a função retorna os clientes do usuário informado.
     * Caso não seja passado o parâmetro usuario, a função retorna os clientes do usuário atualmente logado no sistema.
     * André Alves - 27/01/2012
     * 
     * @param int $usuario Id do usuário
     * @return object Lista com os clientes
     */
    function obterClientesDoUsuario($usuario = '') {
        
        if($usuario == '') {
            $res = controller_seguranca::getInstance()->identificarUsuario();
            $usuario = $res->getID();    
        }        
        
        $sql = 'SELECT 
                  CLIE_Codigo,
                  CLIE_Nome 
                FROM
                  vw_usuarios_areas_clientes 
                WHERE usuario_id = '.$usuario.' 
                GROUP BY CLIE_Nome';
        
        $result = $this->getConexao()->executeQuery($sql);
        
        if ($result != null) {
            return $result;
        }
        
        return null;
    
    }
    
    /**
     * Obter as áreas associadas para cada cliente passado no parâmetro cliente. As áreas sem associação com cliente receberão como parâmetro "COLAB"
     * Caso seja passado o parâmetro usuario, a função retorna os clientes do usuário informado.
     * Caso não seja passado o parâmetro usuario, a função retorna os clientes do usuário atualmente logado no sistema.
     * André Alves - 27/01/2012
     * 
     * @param string $cliente Id do cliente
     * @param int $usuario Id do usuário
     * @return object Lista com os clientes
     */
    /*function obterAreasDoUsuario($cliente = '', $usuario = '') { Função deslocada para o TController - Jorge Roberto Mantis 6383
        
        $where = '';
        
        if($usuario == '') {
            $res = controller_seguranca::getInstance()->identificarUsuario();
            $usuario = $res->getID();    
        }
        
        if($cliente != '') {
            $where .= ' AND Clie_Codigo = "'.$cliente.'"';
        }
        
        $sql = 'SELECT 
                  area_id,
                  are_descricao,
                  are_observacao 
                FROM
                  vw_usuarios_areas_clientes 
                WHERE usuario_id = '.$usuario.
                $where.'                   
                ORDER BY are_descricao';
        
        $result = $this->getConexao()->executeQuery($sql);
        
        if ($result != null) {
            return $result;
        }
        
        return null;
    }*/
    
    function process() {
        parent::process();

        switch ($this->getAction()) {
            case "save":
                $save = new noticia();
				
                $save->setnoticia_id($_POST["id"]);
				$save->setnot_autor_id($_POST["autor"]);

				$sql = "SELECT * FROM perfil_usuario WHERE usuario_id= ".$_POST["autor"];
                $perfil_usuario = $this->getConexao()->executeQuery($sql);

				if ($perfil_usuario->RowCount() > 0) {
					$row_perfil = $perfil_usuario->getRow(0);
					if ($row_perfil->perfil_id==PERFIL_COLABORADOR){
						$save->setnot_autorizada(0);
					}else{
						$save->setnot_autorizada(1);	
					}
		        }else{
					$save->setnot_autorizada(1);	
				}
        
				$save->setnot_titulo(filtroTexto($_POST["FTitulo"]));
				$save->setnot_data(date("Y")."-".date("m")."-".date("d"));
                $save->setnot_publicacao(DateConvert($_POST["FDatapublicacao"]));
				$save->setnot_validade(DateConvert($_POST["FDatafinal"]));
				$save->setnot_chamada(filtroTexto($_POST["FResumomateria"]));
				$save->setnot_texto(mysqli_real_escape_string($this->getConexao(),$_POST["FMateriacompleta"]));
				if (isset($_POST["FLinkmateriacompleta"]))
					$save->setnot_link(filtroTexto($_POST["FLinkmateriacompleta"]));
				if (isset($_POST["FThumbnail"]))
					$save->setnot_thumbnail($_POST["FThumbnail"]);	
				$this->save($save);
                break;
            
            case "ajaxLista":
            
                $idUsuario = $this->existeUsuario($_POST['user'],$_POST['pass']); 
                if($idUsuario) {
                    $view = new view_sugestao($this);
                    print $view->listarAreas($idUsuario);
                } else {
                    echo "<strong style='color:red'>Login ou senha incorreto. Tente logar novamente.</strong><br />";
                    exit;
                }
                break;
                
        }
    }
    
} // fim classe

?>
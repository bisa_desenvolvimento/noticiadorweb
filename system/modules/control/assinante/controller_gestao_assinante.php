<?php

class controller_gestao_assinante extends TController {

	function show() {
		$view = new view_gestao_assinante($this);
		$view->show();
	}

	function verificarEmail($email) {
		$idUsuarioLogado = controller_seguranca::getInstance()->identificarUsuario()->getId();
		// require_once("view/NovaConexao.php");
		$conexao = new NovaConexao();
		$conexao->Conecta();
		$sql = '
			SELECT
				assinante_id,
				ass_email,
				ass_excluido
			FROM
				assinantes
			WHERE
		';
		/*
		$sql .= '
			--	assinante_id IN (
			--		SELECT
			--			assinante_id
			--		FROM
			--			assinante_area
			--		WHERE
			--			area_id IN (
			--				SELECT
			--					area_id
			--				FROM
			--					`vw_usuarios_areas_clientes`
			--				WHERE
			--					usuario_id = ' . $idUsuarioLogado . '
			--					AND tipo = "C"
			--			)
			--	)
			--	AND
		';
		*/
		$sql .= '
				ass_email = "' . $email . '"
		';
		$sql .= '
				AND assinante_id <> ' . $_SESSION['idalterar'] . '
		';
		$query  = mysqli_query($this->getConexao(),$sql);
		if (mysqli_num_rows($query) > 0) {
			$ret['existe'] = true;
		}else{
			$ret['existe'] = false;
		}
		//$ret['idalterar'] = $_SESSION['idalterar'];
		//$ret['sql'] = $sql;

		echo json_encode($ret);
		sleep(1);
	}

	function save($model) {

	}

	function load($key) {
	}

	function delete($key) {

	}

	function create() {

	}


	function obterTodos() {

		//  $sql = 'SELECT ass.assinante_id,ass.ass_email FROM ASSINANTE ass ';


		//	 $sql = 'SELECT distinct ASSINANTE.assinante_id, ass_email FROM ASSINANTE LEFT JOIN ASSINANTE_AREA on ASSINANTE.assinante_id=ASSINANTE_AREA.assinante_id ';
		// $sql .= ' where ass_excluido <> 1 ';
		if ((isset($_POST['FEmail'])) && ($_POST['FEmail'] != "")) {
			$sql = 'SELECT ass.assinante_id,ass.ass_email FROM assinantes ass ';
			$sql .= " where ass.ass_email ='" . $_POST['FEmail'] . "'  and ass.ass_excluido <> 1";

		}
		if ((isset($_POST['FArea'])) && ($_POST['FArea'] != '0')) {
			// $sql .= " and ASSINANTE.assinante_id in (SELECT assinante_id FROM ASSINANTE_AREA where area_id = ". $_POST['FArea'] .")";
			$sql = 'SELECT ass.assinante_id,ass.ass_email FROM assinantes ass ';
			$sql .= "inner join assinante_area a on(a.assinante_id = ass.assinante_id)";
			$sql .= "inner join area ar on(ar.area_id = a.area_id)  where ar.area_id =" . $_POST['FArea'] . " and ass.ass_excluido <> 1";
		}
		//$sql .= ' ORDER BY ass_email';*/
		echo $sql;
		$table = $this->getConexao()->executeQuery($sql);

		if ($table->RowCount() > 0) {
			return $table;
		}

		return null;
	}

	function obterAreas($assinante_id = "") {

		$sql = 'SELECT * FROM area AR ';

		if ($assinante_id != "") {
			//	 $sql .= ' WHERE area_id in(SELECT area_id FROM ASSINANTE_AREA WHERE assinante_id='.$assinante_id.')';
			$sql .= 'INNER JOIN assinante_area A ON(A.area_id = AR.area_id) WHERE A.assinante_id=' . $assinante_id . '';
		}

		$sql .= ' ORDER BY are_descricao ';

		$table_area = $this->getConexao()->executeQuery($sql);

		if ($table_area->RowCount() > 0) {
			return $table_area;
		}

		return null;
	}

	function obterAreasUsuarioAtual($usuario_id) {

		//$usuario = controller_seguranca::getInstance()->identificarUsuario();

		if ($usuario_id != null) {
			$sql = "SELECT A.* FROM area A INNER JOIN area_usuario B ON A.area_id = B.area_id WHERE B.usuario_id = " . $usuario_id . " ORDER BY A.are_descricao";

			$table = $this->getConexao()->executeQuery($sql);

			if ($table->RowCount() > 0) {
				return $table;
			}
		}
		return null;
	}

	function obterAreasAssinante($assinante) {

		if ($assinante != null) {

			$sql = "SELECT a.area_id, a.are_descricao, a.are_observacao,b.excluido FROM area a, assinante_area b where a.area_id=b.area_id and b.assinante_id=" . $assinante . " order by are_descricao";
			$table = $this->getConexao()->executeQuery($sql);

			if ($table->RowCount() > 0) {
				return $table;
			}
		}
		return null;

	}

	function obterEmails($area) {

		$sql = "select a.ass_email from assinantes a	";
		$sql .= "inner join assinante_area ass on (ass.assinante_id = a.assinante_id)";
		$sql .= "where area_id=" . $area;


		$table_area = $this->getConexao()->executeQuery($sql);

		if ($table_area->RowCount() > 0) {
			return $table_area;
		}

		return null;
	}

	function AlterandoEmailComBloqueiosController() {
		//var_dump($_POST);die;
		if (isset($_POST['bloqueo'])) {
			$areas = $_POST['area'];
			$bloqueo = $_POST['bloqueo'];
			$bloqueoTotal = 0;
			for ($index = 0; $index < count($areas); $index++) {
				for ($index1 = 0; $index1 < count($bloqueo); $index1++) {

					if ($areas[$index] == $bloqueo[$index1]) {
						$sql = " update assinante_area set excluido=1 where assinante_id =" . $_SESSION['idalterar'] . " and area_id=" . $areas[$index];
						//echo $sql."<br/>";
						//$resultado=$conexao->Query($sql);
						$bloqueoTotal++;
						$this->getConexao()->executeNonQuery($sql);
						break;
					} else {
						if ($index1 == ((count($bloqueo)) - 1)) {
							$sql = " update assinante_area set excluido=0  where assinante_id =" . $_SESSION['idalterar'] . " and area_id=" . $areas[$index];
							//echo $sql."<br/>";
							//$resultado=$conexao->Query($sql);
							$this->getConexao()->executeNonQuery($sql);

						}
					}
				}


			}
			//echo  "<script>alert('".$bloqueoTotal."  ".count($areas)."')</script>";
			if ($bloqueoTotal == count($areas)) {
				$sqlbloqueototal = " update assinantes set ass_excluido=1  where assinante_id =" . $_SESSION['idalterar'];
				$this->getConexao()->executeNonQuery($sqlbloqueototal);
			} else {
				$sqldesbloqueototal = " update assinantes set ass_excluido=0  where assinante_id =" . $_SESSION['idalterar'];
				$this->getConexao()->executeNonQuery($sqldesbloqueototal);
			}
		} else {
			$sql = " update assinante_area set excluido=0  where assinante_id =" . $_SESSION['idalterar'];
			//echo $sql."<br/>";
			//$resultado=$conexao->Query($sql);
			$this->getConexao()->executeNonQuery($sql);

		}
		$sql1 = " update assinantes set ass_email='" . $_POST['emailnovo'] . "' where assinante_id =" . $_SESSION['idalterar'];
		//$resultado=$conexao->Query($sql1);
		$this->getConexao()->executeNonQuery($sql1);
		$this->getConexao()->executeNonQuery($sql);


	}

	public function AlterandoEmailController() {

		$sql4 = " update assinantes set ass_email='" . $_SESSION['emailalterar'] . "' where assinante_id=" . $_SESSION['idalterar'];
		$this->getConexao()->ExecuteNonQuery($sql4);
		$sql5 = "update assinante_area set excluido =" . $_SESSION['excluido'] . " where assinante_id=" . $_SESSION['idalterar'] . " and area_id=" . $_SESSION['area'];
		$this->getConexao()->ExecuteNonQuery($sql5);

		//Atualiza a tabela ASSINANTE pra um desbloqueo total
		$sqldesbloqueototal = "SELECT * FROM assinante_area where assinante_id=" . $_SESSION['idalterar'] . " and excluido=0";
		$table = $this->getConexao()->ExecuteQuery($sqldesbloqueototal);

		if ($table->RowCount() > 0) {
			$sqldesbloqueototal = "update assinantes set ass_excluido=0 where assinante_id=" . $_SESSION['idalterar'];
			$this->getConexao()->ExecuteNonQuery($sqldesbloqueototal);
		} //Atualiza a tabela ASSINANTE pra um bloqueo total
		else {
			$sqlbloqueototal = "update assinantes set ass_excluido=1 where assinante_id=" . $_SESSION['idalterar'];
			$this->getConexao()->ExecuteNonQuery($sqlbloqueototal);
		}


	}

	public function ExcluirEmailController($id)
	{
		$sql4 = " delete from assinantes where assinante_id=" . $id;
		$this->getConexao()->ExecuteNonQuery($sql4);
		$sql5 = " delete from assinantes where assinante_id=" . $id;
		$this->getConexao()->ExecuteNonQuery($sql5);

	}


	function obterAreasEditorDoUsuario() {
		$usuario = controller_seguranca::getInstance()->identificarUsuario();

		$sql = 'SELECT
						area_id,
						are_descricao 
					 FROM
						`vw_usuarios_areas_clientes` 
					 WHERE usuario_id = ' . $usuario->getID() . '
						AND tipo = "C" 
					 GROUP BY area_id 
					 ORDER BY are_descricao';

		$result = $this->getConexao()->executeQuery($sql);

		if ($result != null) {
			return $result;
		}

		return null;
	}


}

?>
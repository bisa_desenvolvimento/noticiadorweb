<?

class Controller_assinar_newsletter extends TController {

	function show() { }

	function save($model) { }

	function load($key) { }

	function delete($key) { }

	function create() { }

  function assinar_news($nome, $email, $areas, $assinatura = '0') {

    // VALIDAÇÃO RECAPTCHA
    $url = "https://www.google.com/recaptcha/api/siteverify";
    $respon = $_POST['g-recaptcha-response'];

    $data = array('secret' => "6LfS6qYZAAAAAAkiKEat0_l88yWfsq5GcimAZgqw", 'response' => $respon);

    $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)

            )
    );
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    $jsom = json_decode($result);
    
      if ($jsom->success) {
      // reCaptcha sucesso 

      // Bruno Magnata Mantis: 03899 (Validar E-mail)
      if (!filter_var($email, FILTER_VALIDATE_EMAIL) === true) {
          print msg_alert("Formato de email errado, favor verificar se o email foi digitado corretamente!");
          print "<script>history.back();</script>";
          exit;
      }


    //existeEspaco procura se existe ou não algum espaço na string
   // $existeEspaco = strstr($email, ' ');

    //variavel validarPontoEmail verifica se o usuario digitou ou não algum ponto no email
   // $validarPontoEmail = count(explode(".", $email));

    //espressão regular para analizar se o padrão esta correto
    // $conta = "^[a-zA-Z0-9\_.-]+@";
    // $domino = "[a-zA-Z0-9\_-]+.";
    // $extensao = "([a-zA-Z]{2,4})$";
    // $pattern = $conta.$domino.$extensao;

    // $user = strstr($email, '@', true);
    // $validarPontoAntesArroba = substr($user,-1);

  //  if (!ereg($pattern,$email) or $validarPontoEmail < 2 or $existeEspaco == true or $validarPontoAntesArroba == "."){
      ?>
    <!--  <script> 
        alert("Formato de email errado, favor verificar se o email foi digitado corretamente.");
        window.history.back();
      </script> -->
      <?
     // exit();
  //  }  
      // echo ('vai');


    $email_assinante = $email;
    $senha = explode("@",$email_assinante);
    $senha = sha1($senha[0]);
    $novasAreasCadastradas = array ();
    $assinante_id = $this->checarEmail($email_assinante);

    
        // caso nao exista o email cadastrado
        if ($assinante_id == null){
          // inseri e-mail em "ASSINANTE"
          $sql = "INSERT INTO assinantes (ass_nome, ass_email, ass_senha, ass_confirmado, ass_excluido)
                  VALUES ('".$nome."', '".$email_assinante."', '".$senha."', '1', '0')";
                  
                  $error = $this->getConexao()->executeNonQuery($sql);
          
          if ($error != -1) { // INSERI EM "ASSINANTE_AREA"
            for ($i=0;$i<sizeof($areas);$i++){	// para cada área]
          
              $sql = "INSERT INTO assinante_area(assinante_id, area_id)
                      VALUES ('".$this->checarEmail($email_assinante).
                      "', '".$areas[$i]."')";
              $error = $this->getConexao()->executeNonQuery($sql);
            }
            $novasAreasCadastradas = $areas;
          }
          $assinatura = 0;
          if ($error != -1) {
              $this->enviarEmail($assinatura,$nome,$email_assinante,$novasAreasCadastradas);
              print msg_alert("E-mail cadastrado com sucesso!");
              print "<script>history.back();</script>";//redirect($url);
            }
          }
        else { // caso o e-mail já exista no cadastro e nao exista na área selecionada inserir
        
          $error = 0;
          
          for ($i=0;$i<sizeof($areas);$i++){	// para cada área
            if (!$this->checarEmailArea($assinante_id, $areas[$i])){
              $sql = "INSERT INTO assinante_area(assinante_id, area_id)
                      VALUES ('".$this->checarEmail($email_assinante).
                      "', '".$areas[$i]."')";
              $novasAreasCadastradas [] = $areas[$i];
              $error = $this->getConexao()->executeNonQuery($sql);
            }
          } 
          if (!empty($novasAreasCadastradas)) {
              if($error != -1){
              $this->enviarEmail($assinatura,$nome,$email_assinante,$novasAreasCadastradas);
              print msg_alert("E-mail cadastrado com sucesso!");
              print "<script>history.back();</script>";
              }else{
                  print msg_alert("Registro não foi salvo.".$this->getConexao()->getErro());
              }
            // print "<script>history.back();</script>";//redirect($url);
            }else{
              print msg_alert("O E-mail já existe nosso registro!");
              print "<script>history.back();</script>";
            }
          }
      } else {
        //reCaptcha invalido
        echo "<script>alert(\"Marque a opção 'Não sou um robô', para poder validar sua inscrição em nossa Newsletter!\");</script>";
        // print msg_alert("Marque a opção 'Não sou um robô', para poder validar sua inscrição em nossa Newsletter!");
        print "<script>history.back();</script>";
        exit;
      }
    }
  
  function enviarEmail($assinatura,$nome,$email_assinante,$areas){
            // sempre será enviado um email de Confirmação de assinatura ao assinante

            $headers = "MIME-Version: 1.0\r\n";
            $headers .="X-Sender: Noticiador Web <no-reply@" . STMP_HOST . "> \r\n";
            $headers .="X-mailer: PHP/" . phpversion() . " \r\n";
            $headers .="X-Priority: 1 \r\n";
            $headers .="Content-Type: text/html; charset=ISO-8859-1\r\n";
            $headers .="Reply-To: Noticiador Web <no-reply@" . STMP_HOST . "> \r\n";
            $from= "From: Noticiador Web <no-reply@" . STMP_HOST . "> \r\n";
            $return="Return-Path: Noticiador Web <no-reply@" . STMP_HOST . "> \r\n";


            $sql = 'SELECT are_descricao FROM area WHERE area_id in ('.  implode(",", $areas).') ';
           $areasDescricao = $this->getConexao()->executeQuery($sql);

           // se nao é 0 o será enviado um email também ao editor responsável pelo script
          
          if($assinatura != '0') {
            
            $sql = 'SELECT 
                      ass_email 
                    FROM
                      assinatura WHERE assinatura_id = '.$assinatura;
            $table = $this->getConexao()->executeQuery($sql);
            if ($table->RowCount()>0){

            $row = $table->getRow(0);
            $email = $row->ass_email;

            //$nomeEditor = $row->usu_nome;
            //}else{
            //    print msg_alert("O script nao existe");
            //    exit();
            //}
            //guarda o ordem das headers
            $headersEditor = "To: $email \r\n";
            $headersEditor .= $from;
            $headersEditor .= $return;
          
             //email pro editor avisando de um novo cadastro nas suas áreas de interesse
             $mensagem = "O usuário <strong>".$nome."</strong> realizou o cadastro do e-mail <strong>".$email_assinante."</strong> em sua newsletter.<br /><br />";
             $assunto = "Novo e-mail cadastrado na newsletter";
             Email::sendEmail($email,$assunto,$mensagem);
             //mail($email,$assunto,$mensagem,$headers.$headersEditor);
            
            //guarda o ordem das headers
            $headersAssinante = "To: $email_assinante \r\n";
            $headersAssinante .= $from;
            $headersAssinante .= $return;

             //email pro assinante da newsletter
             $assunto = "Confirmação de assinatura";
             $mensagem = $this->getOptin($email, '', $email_assinante,$areasDescricao);
             Email::sendEmail($email_assinante,$assunto,$mensagem);
             //mail($email_assinante,$assunto,$mensagem,$headers.$headersAssinante);
          //  echo "envio email desde el scrip ----> ".$email.$assunto.$mensagem.$headers."sql ------><br/>".$sql;
           }else{
               
             //email pro editor avisando de um novo cadastro nas suas áreas de interesse
             $mensagem = "O usu&#225;rio <strong>".$nome."</strong> realizou o cadastro do e-mail <strong>".$email_assinante."</strong> em sua newsletter.<br /><br />";
             $assunto = "Novo e-mail cadastrado na newsletter";

                //procura todos os editores das areas da noticia
               $sql = "SELECT usu_email FROM usuario NATURAL JOIN area_usuario  NATURAL JOIN area WHERE area.area_id IN (".implode(",",$areas).") AND usuario.usu_perfil<>2 GROUP BY usu_email";
               $table = $this->getConexao()->executeQuery($sql);
               
               for ($index = 0; $index < $table->RowCount(); $index++) {
               $row = $table->getRow($index);
               $email = $row->usu_email;
               $headersEditor = "To: $email \r\n";
               $headersEditor .= "From: Noticiador Web <no-reply@" . STMP_HOST . "> \r\n";
               $headersEditor .= "Return-Path: Noticiador Web <no-reply@" . STMP_HOST . "> \r\n";
               Email::sendEmail($email,$assunto,$mensagem);
               //mail($email,$assunto,$mensagem,$headers.$headersEditor);
               //echo "envio email desde el home ----> ".$email.$assunto.$mensagem.$headers.$headersEditor."sql ------><br/>".$sql;
               }
               //echo "sql";
           }

			}

  }

  function checarEmail($email){
    $sql = "SELECT * FROM assinantes WHERE ass_email = '". $email ."'";
    $assinante = $this->getConexao()->executeQuery($sql);
    if ($assinante->RowCount() > 0) {
      return $assinante->getRow(0)->assinante_id;
    }
    return null;
	}

	function checarEmailArea($assinante_id, $area_id){
      $sql = "SELECT * FROM assinante_area WHERE assinante_id = ". $assinante_id ." and area_id=".$area_id;
      $assinante = $this->getConexao()->executeQuery($sql);
      
      if ($assinante->RowCount() > 0) {
         return true;
      } else {
         return false;
      }
   }

	function process(){
 		parent::process();
		switch($this->getAction()) {
			 case "save":
				if (isset ($_POST["areas"])){
					$nome_email = (isset($_POST["nome"])) ? $_POST["nome"] : "";
					$this->assinar_news($nome_email,$_POST["email"],$_POST["areas"],$_POST['assinatura']);
				}
				break;
		}
	}

  public function getOptin($email,$nome,$email_assinante,$areasDescricao) {
      $arquivo = DIR_DATA.'mensagensoptin/'.$email.'.txt';
      if(file_exists($arquivo)) {
          $fp = fopen($arquivo, "r");
          $texto = '';
          while(!feof($fp)) {
            $texto .= fgetc($fp);
          }
          fclose($fp);
      } else {
          $fp = fopen(DIR_DATA.'mensagensoptin/padrao-pt-BR.txt', "r");
          $texto = '';
          while(!feof($fp)) {
            $texto .= fgetc($fp);
          }
          fclose($fp);
      }
      echo "<pre>";
      $texto .= translate('<br /><br /><br />Se você não clicar no link <a href="[link]" target="_blank">[link]</a> estamos presumindo que você deseja continuar a receber nossas informações, e será um prazer manter o seu endereço em nosso cadastro.');
      $texto = utf8_decode($texto);
      $link = URL.'index.php?action=show&secao=remover_usuario_email&id='.$email_assinante;
      $tags = array('[link]','[nome]','[email]');
      $nome.=" (editor da(s) area(s): ";
      for ($index = 0; $index < $areasDescricao->RowCount(); $index++) {
        $nome.=$areasDescricao->getRow($index)->are_descricao;
      }
      $nome.=") ";
      $valores = array($link,$nome,$email);
      $mensagem = str_replace($tags, $valores, $texto);     

      return $mensagem;
    }

}
?>
<?
class Controller_ajustar_email_assinantes extends TController {

    function show() {
        $view = new view_ajustar_assinante($this);
        $view->show();
    }

    function save($datosassinante) {
        // atualiza no banco ou bloquea o email selecionado.
        $assinante=explode(",", $datosassinante);
        $sql = "UPDATE assinantes SET ass_email='" . $assinante[1] . "', ass_senha='" . $assinante[2]. "', ass_confirmado='".$assinante[3]."' , ass_excluido='".$assinante[4]."' where assinante_id=".$assinante[0].";";
        $this->getConexao()->executeNonQuery($sql);
        $sql = "UPDATE assinante_area SET excluido='".$assinante[4]."' where assinante_id=".$assinante[0].";";
        $this->getConexao()->executeNonQuery($sql);
        echo '<span class="ui-icon ui-icon-notice" style="float: left"></span><label style="float: left;">E-mail Atualizado no banco com sucesso!</label>';
         }

    function load($key) {
        set_time_limit(0);
        // carrega a tabela com Ajax quando é clickado o botao ajustar assinantes
        $table = $this->AjustarEmail($key);
        $view = new view_ajustar_assinante($this); 
        $view->getAjustarEmail($table);
    }
    function AjustarEmail($key){
        return $key;
    }

    function delete($emails) {   
        // Apaga da tabela assinante os emails selecionados e seu relacionamento com a tabela assinante_area
        $sql = "DELETE FROM assinantes WHERE  assinante_id in (" . $emails." )";
        $this->getConexao()->executeNonQuery($sql);
        $sql = "DELETE FROM assinante_area WHERE  assinante_id in (" . $emails." )";
        $this->getConexao()->executeNonQuery($sql);
        echo '<span class="ui-icon ui-icon-notice" style="float: left"></span><label style="float: left;">Email(s) Apagado(s) do banco de dados!</label>';
    }

     // Bruno Magnata 15/09/16 Mantis[4600]
    function deleteEmail($area,$emails) {
        // Apaga da tabela assinante os emails selecionados e seu relacionamento com a tabela assinante_area
        $sql = "DELETE FROM assinantes WHERE  assinante_id in (" . $emails." )";
        $this->getConexao()->executeNonQuery($sql);

        $sql = "DELETE FROM assinante_area WHERE  assinante_id in (" . $emails." ) and area_id in (" . $area." )  ";
        $this->getConexao()->executeNonQuery($sql);
        echo '<span class="ui-icon ui-icon-notice" style="float: left"></span><label style="float: left;">Email(s) Apagado(s) do banco de dados!</label>';
    }


    function create() {

    }

    
    function pegarEmails($areas,$paginacao) {
        // Bruno Magnata 22/08/16 Mantis[4600]
        $sql = "SELECT  DISTINCT C.assinante_id, SUBSTRING(C.ass_email,1,20) AS ass_email, C.ass_excluido FROM area A INNER JOIN assinante_area B ON A.area_id = B.area_id INNER JOIN assinantes C ON C.assinante_id = B.assinante_id WHERE A.area_id IN (".$areas. ") 
            AND C.ass_email NOT REGEXP '^[a-zA-Z0-9][a-zA-Z0-9._-]*[a-zA-Z0-9._-]@[a-zA-Z0-9][a-zA-Z0-9._-]*[a-zA-Z0-9]\\.[a-zA-Z]{2,4}$'
            LIMIT ".$paginacao.",500";
        //var_dump($sql);die();
        $assinantes = $this->getConexao()->executeQuery($sql);
        // echo $sql;
        return $assinantes;
    }

    function pegarTodosEmails($areas,$limit){
        $sql = "SELECT count(*) as total FROM area A INNER JOIN assinante_area B ON A.area_id = B.area_id INNER JOIN assinantes C ON C.assinante_id = B.assinante_id WHERE A.area_id IN (".$areas. ")";
        $total = $this->getConexao()->executeQuery($sql);
        return $total;
    }

    function carregar($areas,$total){
// carrega a tabela com Ajax quando é clickado o botao ajustar assinantes
        $table = $this->pegarEmails($areas,$total);
        $view = new view_ajustar_assinante($this);
        $view->getAjustarEmail($table);
    }
    function obterAreas() {
        $usuario = controller_seguranca::getInstance()->identificarUsuario();

        // $sql = 'SELECT area.area_id FROM area, area_usuario
        //                 WHERE area.area_id = area_usuario.area_id
        //                 AND area_usuario.usuario_id = ' . $usuario->getID();
        // $sql .= ' order by are_descricao';

        // Bruno Magnata 22/09/16 Mantis[4600] 
        $sql = 'SELECT 
                  area_id
                FROM
                  vw_usuarios_areas_clientes 
                WHERE usuario_id = '. $usuario->getID();

        $table_area = $this->getConexao()->executeQuery($sql);

        if ($table_area->RowCount() > 0) {
            return $table_area;
        }

        return null;
    }

    function obterTotal() {
        // Bruno Magnata 16/09/16 Mantis[4600] {Problema com SQL - LINUX}
        $usuario = controller_seguranca::getInstance()->identificarUsuario();
        $sql = "select count(*) as total from assinantes where assinante_id in (SELECT  assinante_id FROM assinante_area where area_id in (SELECT A.area_id FROM area A , area_usuario B where B.usuario_id = ".$usuario->getID()." and A.area_id = B.area_id order by area_id))";
        $table_area = $this->getConexao()->executeQuery($sql);
        if ($table_area->RowCount() > 0) {
            return $table_area;
        }
        return null;
    }

    




function process() {
        parent::process();
        switch ($this->getAction()) {
        case "save":
       $item = $_GET["id"];
       $this->save($item);
        break;

        case "carregar":
       $areas= $_GET["areas"];
       $total = $_GET["total"];
       $this->carregar($areas,$total);
       break;
    }
    }
}?>
<?php

class Controller_gestao_layout_cliente extends TController {
	
    function show() {
        $view = new view_gestao_layout_cliente($this);
    	$view->show();
    }

    function save($model) {
        exit(var_dump($_POST));
    }

    function load($key) {
        $view = new view_gestao_layout_cliente($this);
        $view->showLayout();
    }

    function delete($key) {}

    function create() {}
    
    function getCodigo($cli) {
        $sql = 'SELECT 
                  cli_layout 
                FROM
                  cliente_layout 
                WHERE cliente_id = "'.$cli.'"';
                
        $cliente_layout = $this->getConexao()->executeQuery($sql);

		if ($cliente_layout->RowCount() > 0) {
			$row_layout = $cliente_layout->getRow(0);
			return $row_layout->cli_layout;		
		}
		return null;		
    }
    
    function obterClientes() {
        $usuario = controller_seguranca::getInstance()->identificarUsuario();
        
        $sql = 'SELECT 
                  * 
                FROM
                  vw_clientes_ativos';
                  
        if($this->obterPerfilUsuario($usuario->getID()) == PERFIL_EDITOR) {
            $sql .= ' WHERE CLIE_Codigo IN 
                      (SELECT 
                        cliente_id 
                      FROM
                        cliente_usuario 
                      WHERE usuario_id = '.$usuario->getID().')';
        }
      
        $table = $this->getConexao()->executeQuery($sql);
        if ($table->RowCount() > 0) {
            return $table;
        }
        return null;
    }
    
    function obterPerfilUsuario($usuario){

		$sql = "SELECT * FROM perfil_usuario WHERE usuario_id= ".$usuario;

		$perfil_usuario = $this->getConexao()->executeQuery($sql);

		if ($perfil_usuario->RowCount() > 0) {
			$row_perfil = $perfil_usuario->getRow(0);
			return $row_perfil->perfil_id;		
		}
		return null;		

	}
    
    function salvarClienteLayout($cliente, $layout) {
        
        $sql = "DELETE 
                FROM cliente_layout
                WHERE cliente_id = '".$cliente."'";
		$this->getConexao()->executeNonQuery($sql);
        
        $sql = "INSERT INTO cliente_layout (cliente_id, cli_layout) 
                VALUES
                  ('".$cliente."', '".$layout."')";    
        $this->getConexao()->executeNonQuery($sql);
        
        echo '<script>alert("Layout salvo com sucesso.")</script>';
        
        $view = new view_gestao_layout_cliente($this);
        $view->show();        
        
    }
    
    function process() {
        parent::process();

        switch ($this->getAction()) {
            case "save":
                $cliente = $_POST['cliente'];
                $layout = addslashes($_POST['FCodigo']);
                $this->salvarClienteLayout($cliente, $layout);
                break;
            
            case "ajaxLista":
                break;
            
                
        }
    }   
    
} // fim classe

?>
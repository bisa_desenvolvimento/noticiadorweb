<?
class Controller_cliente extends TController
{
	function show() { 
		$view = new view_cliente($this);
		$view->show();
	}
	
	function save($model){
		// INSERT
		$emails = $model->getEmails();
		
		if (sizeof($emails)){
			$error = "";
			$sql = "INSERT INTO cliente(cli_codigo, cli_nome, cli_contrato, cli_site) 
										VALUES ('".$model->getCli_codigo()."',
												'".$model->getCli_nome()."',
												'".$model->getCli_contrato()."',												
												'".$model->getCli_site()."')";
			$error = $this->getConexao()->executeNonQuery($sql);
			if ($error != -1) {
				$clienteId = $this->getClienteIdInserted($model->getCli_codigo(), $model->getCli_nome(), $model->getCli_site(), $model->getCli_contrato());
				for ($i=0;$i<sizeof($emails);$i++){	// para cada e-mail selecionada
					$sql = "INSERT INTO cliente_usuario(cliente_id, usuario_id) 
							VALUES (".$clienteId.","
									 .$emails[$i].")";
					$error = $this->getConexao()->executeNonQuery($sql);
				}						
			} 
			if ($error != -1) {
				$controller_arquivo = new controller_arquivo();
        		$controller_arquivo->setConexao(TConexao::getInstance());
        		
				print msg_alert("Cliente inserido com sucesso!");
				$url = "index.php?".PARAMETER_NAME_ACTION."=show&";
				$url .= PARAMETER_NAME_FILE."=cliente";
	
				print "<script>parent.location.href='".$url."'</script>";//redirect($url);
			} else {
				print msg_alert("N�o foi poss�vel incluir o cliente!".$this->getConexao()->getErro());
			}
		}else{
			print msg_alert("Selecione pelo menos um editor!");			
		}
	}

	function load($key){ 
		$sql = "SELECT * FROM cliente WHERE cliente_id= ".$key;
		
		$table = $this->getConexao()->executeQuery($sql);
		if ($table->RowCount() > 0) {	
			$result = new cliente();
			$result->bind($table->getRow(0));
			if ($result != null){					
                $view = new view_cliente($this);
                $view->setModel($result);
                $view->show();
			}
		} 
		return null;

	}

	function delete($key){
		$sql = "DELETE FROM noticia WHERE  noticia_id = ".$key;
		$this->getConexao()->executeNonQuery($sql);

	}

	function create(){ }
	
	function process(){
 		parent::process();

		switch($this->getAction()) {
			 case "save":
				$save = new cliente();
				$save->setEmails($_POST["emails"]);
				$save->setCli_codigo($_POST["FCodigo"]);
				$save->setCli_contrato($_POST["FContrato"]);
				$save->setCli_nome($_POST["FNome"]);
				$save->setCli_site($_POST["FSite"]);
				$this->save($save);
				break;
		}
	 }

	function obterEditores() {
        $sql = 'SELECT usuario.* FROM usuario, perfil_usuario
        		WHERE usuario.usuario_id = perfil_usuario.usuario_id
        		  AND perfil_usuario.perfil_id = 3';
		$sql .= ' order by usu_nome';        
        $table_editor = $this->getConexao()->executeQuery($sql);

        if ($table_editor->RowCount() > 0) {
        	return $table_editor;
        }
        
        return null;
    }
    
    function getClienteIdInserted($codigo, $nome, $site, $contrato){
        $sql = "SELECT cliente_id FROM cliente ";
		$sql .= "where cli_codigo='".$codigo."'
				   AND cli_nome='".$nome."'
				   AND cli_site='".$site."'
				   AND cli_contrato='".$contrato."'";
		
        $clienteobj = $this->getConexao()->executeQuery($sql);

        if ($clienteobj->RowCount() > 0) {
		        $row = $clienteobj->getRow(0);
            	 return $row->cliente_id;       	
        }        
        return false;		
    }

}
?>
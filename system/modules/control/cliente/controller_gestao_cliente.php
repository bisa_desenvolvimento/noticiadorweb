<?
class Controller_gestao_cliente extends TController {
    
    function show() { 
        $view = new view_gestao_cliente($this);
        $view->show();
    }
    
    function save($model) { }
    
    function load($key) { 
        $view = new view_gestao_cliente($this);
        $view->showGestao();
    }
    
    function delete($key){ }
    
    function create(){ }

    function obterClientes() {
        $usuario = controller_seguranca::getInstance()->identificarUsuario();
        
        $sql = 'SELECT 
                  * 
                FROM
                  vw_clientes_ativos';
                  
        if($this->obterPerfilUsuario($usuario->getID()) == PERFIL_EDITOR) {
            $sql .= ' WHERE CLIE_Codigo IN 
                      (SELECT 
                        cliente_id 
                      FROM
                        cliente_usuario 
                      WHERE usuario_id = '.$usuario->getID().')';
        }
      
        $table = $this->getConexao()->executeQuery($sql);
        if ($table->RowCount() > 0) {
            return $table;
        }
        return null;
    }
    
    function obterAreasAssociadas($cliente) {
        $sql = 'SELECT 
                  area.* 
                FROM
                  area
                  NATURAL JOIN cliente_area 
                WHERE cliente_id = "'.$cliente.'"
                ORDER BY are_descricao';        
        $table = $this->getConexao()->executeQuery($sql);
        if ($table->RowCount() > 0) {
            return $table;
        }
        return null;
    }
    
    function obterAreasNaoAssociadas($cliente, $filtro = "") {
        $sql = 'SELECT 
                  * 
                FROM
                  area
                WHERE area_id NOT IN 
                  (SELECT DISTINCT 
                    (area_id) 
                  FROM
                    cliente_area 
                  WHERE cliente_id = "'.$cliente.'") ';
                  
        if(isset($filtro) && $filtro != "") {
            $sql = 'AND are_descricao LIKE "%'.$filtro.'%"';
        }
                  
        $sql .= 'ORDER BY are_descricao';        
        $table = $this->getConexao()->executeQuery($sql);
        if ($table->RowCount() > 0) {
            return $table;
        }
        return null;
    }
    
    function salvarClienteAreas($cliente, $areas) {
        if($areas!=null){
        $area = explode(',',$areas);
        
      } 
      
        
        $sql = "DELETE 
                FROM cliente_area
                WHERE cliente_id = '".$cliente."'";
		$this->getConexao()->executeNonQuery($sql);

        foreach($area as $are) {
            $sql = "INSERT INTO cliente_area (area_id, cliente_id) 
                    VALUES
                      ('".$are."', '".$cliente."')";    
            $this->getConexao()->executeNonQuery($sql);
        }
        
         
           
        $view = new view_gestao_cliente($this);
        $view->show();   
        
    }
    
    function obterPerfilUsuario($usuario){

		$sql = "SELECT * FROM perfil_usuario WHERE usuario_id= ".$usuario;

		$perfil_usuario = $this->getConexao()->executeQuery($sql);

		if ($perfil_usuario->RowCount() > 0) {
			$row_perfil = $perfil_usuario->getRow(0);
			return $row_perfil->perfil_id;		
		}
		return null;		

	}
    
    function process(){  
        parent::process();
        switch($this->getAction()) {
            case "save":
                $cliente = $_POST['cliente'];
                $areas = $_POST['areas'];
                $this->salvarClienteAreas($cliente, $areas);
                break;
                
            case "ajaxLista":
                $view = new view_gestao_cliente($this);
                // Prepara o filtro por nome
                print $view->listarAreasCadastradas($_GET['filtro']);
                break;

            case "associa":
                $linkCliente = $_POST['linkCliente'];
                $idCliente = $_POST['idCliente'];
                $this->associarLink($linkCliente, $idCliente);
                
                break;
        }
    }

    function associarLink($linkCliente, $idCliente){
        $usuario = controller_seguranca::getInstance()->identificarUsuario();
        $sql = 'UPDATE acrwclie
                SET link_Noticias = "'.$linkCliente.'"
                WHERE CLIE_Codigo ="'.$idCliente.'"';
        $this->getConexao()->executeNonQuery($sql);
        $view = new view_gestao_cliente($this);
        $view->show(); 
        
     
    }

    function pegarLinkCliente($id){
        
        $sql = "SELECT link_Noticias
                        FROM acrwclie
                        WHERE '$id' = CLIE_Codigo";

        
        
        $link = $this->getConexao()->executeQuery($sql);
        if ($link->RowCount() > 0) {
            
            return $link->getRow(0)->link_Noticias;
        }
        return "";
    }

}
?>
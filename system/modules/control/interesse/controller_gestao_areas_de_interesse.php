<?
class controller_gestao_areas_de_interesse extends TController
{
	function process() {
    	parent::process();
    	
    	switch ($this->getAction()) {
    		case "save":
    			$object = new Area();
    			$object->setDescricao(filtroTexto($_POST["txtDescricao"]));
    			$object->setObservacao(filtroTexto($_POST["txtObservacao"]));
                $object->setCliente(filtroTexto($_POST["radCliente"]));
    			
    			if (!empty($_POST["id"])) {
                            // se existe o id da area na variavel post é atualiçar os dados.
    			    $object->setID($_POST["id"]);
    			}
                                // salva o atualiza no banco de dados
				$result = $this->save($object);
				// se o objeto é null foi uma inserção.
				if ($object->getID() == null){
                                        // pega o ultimo id da area inserida
					$object->setID($_SESSION['AREA_ID']);
					unset($_SESSION['AREA_ID']);

                                }
                                // apaga todos os relacionamentos da tabela area_usuario com os editores
				//$result = $this->limparEditores($object->getID());

				$cbUsuario = obterListaCheckbox("cbUsuario");

				foreach($cbUsuario as $key) {
					$this->associarUsuarioArea($object->getID(), $key);
				}

				if(!count($cbUsuario)) {
				  $this->associarUsuarioArea($object->getID(), $object->getID(), $_SESSION['USUARIO']);
				}
					
        		if ($result != -1)
        			print msg_alert("Registro salvo.");
        		else 
        			print msg_alert("Registro não foi salvo.\\n".$this->getConexao()->getErro());
        			
				$this->show();
                                
    	    	break;    	
    	    	
    	    case "ajaxLista": 
    	    	$view = new view_gestao_areas_de_interesse($this);
    	    	// Prepara o filtro por nome
    	    	$filtro = "are_descricao LIKE '%" . $_GET['filtro'] . "%'";
    	    	print $view->listarAreasCadastradas($filtro);
    	    	break;
    	    	   
		}        
    }
    
    private function associarUsuarioArea($area, $usuario, $remetente = false) {
		
		if($remetente){
            $sql = "SELECT usuario_id FROM usuario WHERE usu_login = '".$remetente."'";
            $table = $this->getConexao()->executeQuery($sql);
            $usuario = $table->getRow(0)->usuario_id;

            $sql = "SELECT count(*) as resultado FROM area_usuario WHERE area_id = '".$area."' AND usuario_id = '".$usuario."'";
            $result = $this->getConexao()->executeQuery($sql);
        }

        

        if($result->getRow(0)->resultado = 0){          

         $sql = "INSERT INTO area_usuario(area_id, usuario_id) VALUES ($area, $usuario)";
        
        return $this->getConexao()->executeNonQuery($sql);

        }

        return true;
		
        
    }
    
    public function limparEditores($key) {
    	$sql = "DELETE FROM area_usuario WHERE area_id = $key";
        
        return $this->getConexao()->executeNonQuery($sql);
    }
    
	function show() {
		$view = new view_gestao_areas_de_interesse($this);
		$view->show();
	}
	
	function save($model){
	   
              
            $area_id ='';
            $sql='';
		if ($model->getID() == null) {
		    // INSERT
                    $usuario = controller_seguranca::getInstance()->identificarUsuario();
		    $sql = "INSERT INTO area (are_descricao, are_observacao) VALUES ('".$model->getDescricao()."', '".$model->getObservacao()."');";
                    $this->getConexao()->executeNonQuery($sql);
                    $area_id = $this->getConexao()->executeQuery("SELECT MAX(area_id) as area_id FROM area");
					$_SESSION['AREA_ID'] = $area_id->getRow(0)->area_id;
                    $sql = "INSERT INTO cliente_area (area_id, cliente_id) VALUES ('".$area_id->getRow(0)->area_id."', '".$model->getCliente()."');";
                    return $this->getConexao()->executeNonQuery($sql);

		} else {
		    // UPDATE
            
		    $sql = "UPDATE area SET are_descricao = '".$model->getDescricao()."', are_observacao = '".$model->getObservacao()."' WHERE area_id = ".$model->getID();
                    $this->getConexao()->executeNonQuery($sql);
                    
                    $sql = "UPDATE cliente_area SET cliente_id = ".$model->getCliente()." WHERE area_id = ".$model->getID();                   
                    return $this->getConexao()->executeNonQuery($sql);
		}
		
		
	}
	
	function load($key){
        $sql = "SELECT * FROM area WHERE area_id = ".$key;
        $table = $this->getConexao()->executeQuery($sql);
        if ($table->RowCount() > 0) {
            $result = new Area();
            $result->bind($table->getRow(0));
            if ($result != null) {
                //var_dump($result);
         /* Jorge Roberto Mantis 5941*/
              $sql = "SELECT cliente_id FROM cliente_area WHERE area_id = ".$key;
              $table = $this->getConexao()->executeQuery($sql);
              $result->setCliente($table->getRow(0)->cliente_id);
          /* Jorge Roberto Mantis 5941*/
                $view = new view_gestao_areas_de_interesse($this);
                $view->setModel($result);
                $view->show();
            }
            return $result;
        } 
        return null;

	}
	
	function delete($key){
        $usuario = controller_seguranca::getInstance()->identificarUsuario();
        //$this->limparEditores($key);
		//$sql = "DELETE FROM AREA WHERE area_id = $key";
        $sql = "DELETE FROM cliente_area WHERE area_id = ".$key;
        $this->getConexao()->executeNonQuery($sql);
        print msg_alert("Registro removido.");
        $this->show();
	}
	
	function create(){
		
	}	
	
	function obterListaEditores($idArea = null) {
	    $sql = "SELECT * FROM vw_area_usuario";
	    if ($idArea != null)
	    	$sql .= "WHERE area_id = $idArea";
    	$sql .= " GROUP BY usu_nome";
            
	    $result = $this->getConexao()->executeQuery($sql);
	    
	    if ($result != null) {
	        return $result;
	    }
               
	    return null;
	}
	
	function obterAreasCadastradas($filtro = null) {
		$usuario = controller_seguranca::getInstance()->identificarUsuario();
		if (empty($filtro))
		{
			$sql = "SELECT area.* FROM area, area_usuario
					WHERE area.area_id = area_usuario.area_id
					  AND area_usuario.usuario_id = ".$usuario->getID().
				  " ORDER BY are_descricao";	
		} else {
			$sql = "SELECT * FROM area WHERE $filtro ORDER BY are_descricao";	
		}
	    
	    $result = $this->getConexao()->executeQuery($sql);
	    
	    if ($result != null) {
	        return $result;
	    }
	    
	    return null;
	}
    
    function obterAreasEditorDoUsuario() {
        $usuario = controller_seguranca::getInstance()->identificarUsuario();
        
        $sql = 'SELECT 
                  area_id,
                  are_descricao,
                  are_observacao 
                FROM
                  `vw_usuarios_areas_clientes` 
                WHERE usuario_id = '.$usuario->getID().' 
                  AND tipo = "C" 
                GROUP BY area_id 
                ORDER BY are_descricao';
        
        $result = $this->getConexao()->executeQuery($sql);
        
        if ($result != null) {
            return $result;
        }
        
        return null;
    }
    
    /**
     * Função para obter a lista de clientes ativos associados ao usuário atualmente logado
     * 
     * @access public
     * @return Lista de clientes ativos associados ao usuário
     */
    public function obterClientes() {
        $usuario = controller_seguranca::getInstance()->identificarUsuario();
        
        $sql = 'SELECT 
                  * 
                FROM
                  vw_clientes_ativos';
                  
        if($this->obterPerfilUsuario($usuario->getID()) == PERFIL_EDITOR) {
            $sql .= ' WHERE CLIE_Codigo IN 
                      (SELECT 
                        cliente_id 
                      FROM
                        cliente_usuario 
                      WHERE usuario_id = '.$usuario->getID().')';
        }
      
        $table = $this->getConexao()->executeQuery($sql);
        if ($table->RowCount() > 0) {
            return $table;
        }
        return null;
    }
    
    /**
     * Função para obter o id do perfil do usuário
     * 
     * @access private
     * @param int $usuario Id do usuário
     * @return int Id do perfil do usuário
     */
    private function obterPerfilUsuario($usuario){

		$sql = "SELECT * FROM perfil_usuario WHERE usuario_id= ".$usuario;

		$perfil_usuario = $this->getConexao()->executeQuery($sql);

		if ($perfil_usuario->RowCount() > 0) {
			$row_perfil = $perfil_usuario->getRow(0);
			return $row_perfil->perfil_id;		
		}
		return null;		

	}
    
    /**
     * Obter a lista dos clientes associados ao usuário.
     * Caso seja passado o parâmetro usuario, a função retorna os clientes do usuário informado.
     * Caso não seja passado o parâmetro usuario, a função retorna os clientes do usuário atualmente logado no sistema.
     * André Alves - 27/01/2012
     * 
     * @param int $usuario Id do usuário
     * @return object Lista com os clientes
     */
    function obterClientesDoUsuario($usuario = '') {
        
        
        if($usuario == '') {
            $res = controller_seguranca::getInstance()->identificarUsuario();
            $usuario = $res->getID();    
        }        
        
        $sql = 
                'SELECT CLIE_Codigo, CLIE_Nome 
                FROM vw_usuarios_areas_clientes 
                WHERE usuario_id = '.$usuario.'
                GROUP BY CLIE_Codigo, CLIE_Nome';
                
                
        $result = $this->getConexao()->executeQuery($sql);

        if ($result != null) {
            return $result;
            
        }
        
        return null;
    
    }
    
    /**
     * Obter as áreas associadas para cada cliente passado no parâmetro cliente. As áreas sem associação com cliente receberão como parâmetro "COLAB"
     * Caso seja passado o parâmetro usuario, a função retorna os clientes do usuário informado.
     * Caso não seja passado o parâmetro usuario, a função retorna os clientes do usuário atualmente logado no sistema.
     * André Alves - 27/01/2012
     * 
     * @param string $cliente Id do cliente
     * @param int $usuario Id do usuário
     * @return object Lista com os clientes
     */
    /*function obterAreasDoUsuario($cliente = '', $usuario = '') { Função deslocada para o TController - Jorge Roberto Mantis 6383
        
        $where = '';
        
        if($usuario == '') {
            $res = controller_seguranca::getInstance()->identificarUsuario();
            $usuario = $res->getID();    
        }
        
        if($cliente != '') {
            $where .= ' AND Clie_Codigo = "'.$cliente.'"';
        }
        
        $sql = 'SELECT 
                  area_id,
                  are_descricao,
                  are_observacao 
                FROM
                  vw_usuarios_areas_clientes 
                WHERE usuario_id = '.$usuario.
                $where.'                   
                ORDER BY are_descricao';
        
        $result = $this->getConexao()->executeQuery($sql);
        
        if ($result != null) {
            return $result;
        }
        
        return null;
    }*/
    
}
?>
<?
class controller_assinantemenu extends TController {

    public function show() { 
    	$view = new view_assinantemenu($this);
		$view->show();
    }
    
    public function save($model) { }
    
    public function delete($model) { }
    
    public function create() { }
    
    public function process() { 
    	parent::process();
    }
    
    public function load($key) { }
    
    public function obterMenu() {
    
    }
    
	public function perfilUsuario() {
	    $usuario = controller_seguranca::getInstance()->identificarUsuario();
	    if ($usuario != null) {
		    $sql = "SELECT * FROM perfil_usuario WHERE usuario_id=".$usuario->getID();
		    $table = $this->getConexao()->executeQuery($sql);
		    if ($table->RowCount() > 0) {
	    	    return $table->getRow(0)->perfil_id;
	    	}
	    }
	    return null;
    }
}
?>
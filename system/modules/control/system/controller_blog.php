<?
class controller_blog extends TController {

    public function show() { }
    
    public function save($model) { }
    
    public function delete($model) { }
    
    public function create() { }
    
    public function process() { 
    	switch($this->getAction()) {
    	    case "show":
				//print "chamando process<br><br>";
                $this->verificaComentarios();
    	    	$view = new view_blog($this);
    	    	$view->show();
    	    	break;
    	}
    }
    
    public function load($key) { }
    
    public function perfilUsuario() {
	    $usuario = controller_seguranca::getInstance()->identificarUsuario();
	    if ($usuario != null) {
		    $sql = "SELECT * FROM perfil_usuario WHERE usuario_id=".$usuario->getID();
		    $table = $this->getConexao()->executeQuery($sql);
		    if ($table->RowCount() > 0) {
	    	    return $table->getRow(0)->perfil_id;
	    	}
	    }
	    return null;
    }
    
    public function obterMenu() {
    
    	$usuario = controller_seguranca::getInstance()->identificarUsuario();
    
    	if ($usuario == null)
    		$sql = "SELECT DISTINCT arq_descricao, aca_descricao, arq_chave, arq_arquivo FROM vw_anonimo_arquivo WHERE aca_descricao = 'show'";
    	else
	    	$sql = "SELECT DISTINCT arq_descricao, aca_descricao, arq_chave, arq_arquivo FROM vw_usuario_arquivo WHERE aca_descricao = 'show' 
                        AND arq_arquivo <> 'adm_gestao_arquivos' AND  arq_arquivo <> 'adm_gestao_perfil' AND usuario_id = ".$usuario->getID();
    	
    	$table = $this->getConexao()->executeQuery($sql);
    	
    	if ($table->RowCount() > 0) {
    	    return $table;
    	}
    	
        return null;
    }
    
    public function integracaoQualinfo() {
	    $usuario = controller_seguranca::getInstance()->identificarUsuario();
	    if ($usuario != null) {
		    $sql = "SELECT * FROM cliente_usuario WHERE usuario_id=".$usuario->getID();
		    $table = $this->getConexao()->executeQuery($sql);
		    if ($table->RowCount() > 0) {
	    	    return true;
	    	}
	    }
	    return false;
    }

    /*Jorge Roberto - Mantis 1647

    Verifica se alguma área que o usuário é responsável possui algum comentário a ser aprovado */
    public function verificaComentarios() {

            $usuario = controller_seguranca::getInstance()->identificarUsuario();
            if($usuario){
                $sql = 'SELECT 
                      n.not_titulo,
                      n.noticia_id                 
                    FROM
                      noticia n
                      INNER JOIN usuario u 
                        ON n.not_autor_id = u.usuario_id
                      INNER JOIN noticia_area na 
                        ON n.noticia_id = na.noticia_id
                        INNER JOIN comentario
                        ON n.`noticia_id` = comentario.`noticia_id`  
                    WHERE n.not_excluida = 0 
                    AND na.area_id IN 
                          (SELECT 
                            area_id 
                          FROM
                            vw_usuarios_areas_clientes 
                          WHERE tipo = "C" 
                            AND usuario_id = '.$usuario->getID().') AND comentario.`com_dataaprovacao` IS NULL
                              LIMIT 1';

                $table = $this->getConexao()->executeQuery($sql);


                if ($table->RowCount() > 0) {
                    $table = $table->getRow(0);                
                    $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=comentario&id=".$table->noticia_id;
                    print msg_alert("A notícia \"" .$table->not_titulo."\" tem algum(ns) comentário(s) para serem liberados. Você vai ser direcionado para a tela de aprovação dos comentários da matéria");
                    print redirect($url);
                }

                return null;

            }


        }
}
?>
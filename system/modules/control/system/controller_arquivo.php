<?
class controller_arquivo extends TController
{
    function __construct (  )
    {
    }
    
    function __destroy ( ) {
        
    }
    
    function process() {
	
    }
    
    function load($key) {
        $sql = "SELECT * FROM ARQUIVO WHERE arq_chave = '".htmlentities($key)."'";
        
        $table = $this->getConexao()->executeQuery($sql);
        
        if ($table->RowCount() > 0) {
            $arr = $table->Rows();
            $retorno =  new Arquivo();
            $retorno->setChave($arr[0]->arq_chave);
            $retorno->setArquivo($arr[0]->arq_arquivo);
            $retorno->setDescricao($arr[0]->arq_descricao);
            $retorno->setParent($arr[0]->arq_parent);
            return $retorno;
        }
        
        throw new Exception("Não foi possível encontrar o arquivo solicitado");
    }
    
    public function show() { }
    
    public function save($model) { }
    
    public function delete($model) { }
    
    public function create() { }
    
    public function getArquivoByNome($filename) {
        $sql = "SELECT * FROM ARQUIVO WHERE arq_arquivo = '".htmlentities($filename)."'";
        
        $table = $this->getConexao()->executeQuery($sql);
        if ($table->RowCount() > 0) {
            $arr = $table->Rows();
            $retorno =  new Arquivo();
            $retorno->bind($arr[0]);
            return $retorno;
        }
        
        return null;
    }
    
    public function getFormPostUrl($arquivo) {
//        	$sql = "SELECT B.* FROM ARQUIVO A INNER JOIN ARQUIVO B ON A.ARQUIVO_ID = B.ARQ_PARENT WHERE A.arq_chave = '".htmlentities($arquivo)."'";
//        	
//        	$table = $this->getConexao()->executeQuery($sql);
//        	if ($table->RowCount() > 0) {
//        	    $arr = $table->Rows();
//        	    $arquivo = new Arquivo();
//        	    $arquivo->bind($arr[0]);
//        	    
//        	    return $arquivo->getChave();
//        	}
        	
//        	return null;
            return $arquivo;
    }
    
    public function integracaoQualinfo() {
	    $usuario = controller_seguranca::getInstance()->identificarUsuario();
	    if ($usuario != null) {
		    $sql = "SELECT * FROM cliente_usuario WHERE usuario_id=".$usuario->getID();
		    $table = $this->getConexao()->executeQuery($sql);
		    if ($table->RowCount() > 0) {
	    	    return true;
	    	}
	    }
	    return false;
    }
    
}
?>
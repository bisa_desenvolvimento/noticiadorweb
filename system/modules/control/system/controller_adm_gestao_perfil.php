<?
class controller_adm_gestao_perfil extends TController
{
	function show() {
		$view = new view_adm_gestao_perfil($this);
		$view->show();
	}
	
	function save($model){
		if ($model->getID() != null) {
			$sql = "UPDATE PERFIL SET per_descricao = '".$model->getDescricao()."' WHERE perfil_id = ".$model->getID();
		} else {
			$sql = "INSERT INTO PERFIL (per_descricao) VALUES ('".$model->getDescricao()."')";
		}
		
		return $this->getConexao()->executeNonQuery($sql);
	}
	
	function load($key){
		$sql = "SELECT * FROM PERFIL WHERE perfil_id = " . $key;
		$table = $this->getConexao()->executeQuery($sql);
		if ($table->RowCount() > 0) {
			$result = new Perfil();
			$result->bind($table->getRow(0));
			if ($result != null) {
				$view = new view_adm_gestao_perfil($this);
				$view->setModel($result);
				$view->show();
			}
			return $result;
		}
		return null;	
	}
	
	function delete($key){
		$this->getConexao()->executeNonQuery("DELETE FROM PERFIL_ARQUIVO_ACAO WHERE perfil_id = $key");
		$this->getConexao()->executeNonQuery("DELETE FROM PERFIL WHERE perfil_id = $key");
		print msg_alert(translate("Registro excluido com sucesso"));
		
		$this->show();
	}
	
	function create(){
		
	}	
	
	function obterListaArquivosEAcoes($id=null){
		if (empty($id))
			$id = "null";
		
		$sql = "
		SELECT DISTINCT
			B.acao_id, B.arquivo_id, B.aca_descricao, B.arq_descricao, A.perfil_id
		FROM
			PERFIL_ARQUIVO_ACAO A
			INNER JOIN VW_ACOES B ON A.acao_id = B.acao_id AND A.arquivo_id = B.arquivo_id
		WHERE
			A.perfil_id = $id
		UNION ALL
		SELECT DISTINCT
			acao_id, arquivo_id, aca_descricao, arq_descricao, NULL
		FROM
			VW_ACOES C
		WHERE
			NOT EXISTS(SELECT 1 FROM PERFIL_ARQUIVO_ACAO WHERE perfil_id = $id AND arquivo_id = C.arquivo_id AND acao_id = C.acao_id)
		ORDER BY
			arquivo_id";
		
		$result = $this->getConexao()->executeQuery($sql);
	    
	    if ($result != null) {
	        return $result;
	    }
	    
	    return null;
	
	}
	
	function process() {
	    parent::process();
	    
	    switch($this->getAction()) {
	        case "save":
	        	$perfil = new Perfil();
	        	$perfil->setDescricao(filtroTexto($_POST["txtDescricao"]));
	        	
	        	if (isset($_POST["id"])) {
	        	    $perfil->setID($_POST['id']);
	        	}
	        	
	        	$error = $this->save($perfil);
	        	
				if (!isset($_POST["id"])) 
					$perfil->setID($this->getConexao()->getLastID());
				
				if ($perfil->getID() != 0) {
					$sql = "DELETE FROM PERFIL_ARQUIVO_ACAO WHERE perfil_id = ".$perfil->getID();
					$this->getConexao()->executeNonQuery($sql);
					
					if (isset($_POST["cbArquivoAcao"])) {
    					$acoes = $_POST["cbArquivoAcao"];
    					
    					foreach($acoes as $acao) {
    						$par = explode("_", $acao);
    						$sql = "INSERT INTO PERFIL_ARQUIVO_ACAO(arquivo_id, acao_id, perfil_id) VALUES (".$par[0].", ".$par[1].", ".$perfil->getID().")";
    						$this->getConexao()->executeNonQuery($sql);
    					}
					}
				}
				
				if ($error != -1){
					print msg_alert(translate("Registro salvo com sucesso"));
				} else {
					print msg_alert(translate("N�o foi possivel salvar").$this->getConexao()->getErro());
				}
				
				$this->show();
	        	break;
	    }
	}
}
?>
<?
class controller_seguranca extends TController {
        private $FUsuario = null;
        
        private static $FInstance = null;
        
        #       Constructor
        private function __construct ( )
        {
        }
        
        static function getInstance() {
            if (self::$FInstance == null)
                self::$FInstance = new controller_seguranca();
                
            return self::$FInstance;
        }
        
    function show(){
        
    }

    function openAfterLogin() {
        $link = $_REQUEST['api'];
        switch ($link) {
            case "noticia":
                print redirect("index.php?" . PARAMETER_NAME_ACTION . "=show&" . PARAMETER_NAME_FILE . "=" . $_REQUEST['api'] . "&api=" . $_REQUEST['api']);
                break;
            case "assinante":
                print redirect("index.php?" . PARAMETER_NAME_ACTION . "=show&" . PARAMETER_NAME_FILE . "=" . $_REQUEST['api'] . "&api=" . $_REQUEST['api']);
                break;
            case "gestao_newsletter":
                print redirect("index.php?" . PARAMETER_NAME_ACTION . "=show&" . PARAMETER_NAME_FILE . "=" . $_REQUEST['api'] . "&api=" . $_REQUEST['api']);
                break;

            default:
                print redirect("index.php?" . PARAMETER_NAME_ACTION . "=show&" . PARAMETER_NAME_FILE . "=" . $_REQUEST['api']);
                break;
        }
    }
        
    function save($model){
                if ($model->getID() == null) {
                    // CADASTRA NA TABELA USUARIO
                    $sql = "
                        INSERT INTO usuario(usu_nome, usu_login, usu_senha, usu_cookie, usu_email, usu_perfil) VALUES
                        ('".$model->getNome()."', '".$model->getLogin()."', '".$model->getSenha()."', '".$model->getCookie()."', '".$model->getEMail()."', 3)";
                    $error = $this->getConexao()->executeNonQuery($sql);
                        //-------------------------------
                    $usuario_id = $this->getLastId("usuario","usuario_id");
                    if (($error != -1) && ($usuario_id)) {
                        //CADASTRA NA TABELA COLABORADOR
                        $sql = "INSERT INTO colaborador (usuario_id)";
                        $sql .=" VALUES ('" . $usuario_id. "')";

                        $error = $this->getConexao()->executeNonQuery($sql);
                        if ($error != -1) {
                            //CADASTRA NA TABELA PERFIL_USUARIO COMO COLABORADOR
                            $sql = "INSERT INTO perfil_usuario(perfil_id, usuario_id)";
                            $sql .=" VALUES (3,'" . $usuario_id . "')"; /* .PERFIL_EDITOR}. */
                            $error = $this->getConexao()->executeNonQuery($sql);
                                if ($error != -1) {
                                    // CADASTRA UMA NOVA AREA DE INTERESSE COM O NOME DO USUARIO
                                    $sql = 'insert into area (are_descricao) values("' . $model->getNome() . '")';
                                    $error = $this->getConexao()->executeNonQuery($sql);
                                        if($error!= -1){
                                        //PEGA DA TABELA AREA O ULTIMO ID INSERIDO
                                        $area_id = $this->getLastId("area","area_id");
                                        // ASSOCIA O USUARIO COM A AREA CADASTRADA
                                        $sql = 'insert into area_usuario (usuario_id, area_id) values(' . $usuario_id . ',' . $area_id . ')';
                                        $error = $this->getConexao()->executeNonQuery($sql);
                                         if($error!= -1){
                                         // CADASTRA O MODELO E O RODAPÉ
                                         $this->cadastroModeloRemoto($model->getID(), $area_id);
                                         //CADASTRA AS PERMISSOES DO USUARIO COMO EDITOR
                                         $this->permissaoEditor($usuario_id);
                                                     
                                        }else{
                                        
                                        print msg_alert("Registro não foi salvo." . $this->getConexao()->getErro());
                                        }

                                        }else{
                                        print msg_alert("Registro não foi salvo." . $this->getConexao()->getErro());
                                       }
                                  
                                } else {
                                    print msg_alert("Registro não foi salvo." . $this->getConexao()->getErro());
                                }
                            
                        } else {
                            print msg_alert("Registro não foi salvo." . $this->getConexao()->getErro());
                        }
                    } else {
                        print msg_alert("Registro não foi salvo." . $this->getConexao()->getErro());
                    }


                    //-------------------------------
                } else {
                    // UPDATE
                    $sql = "UPDATE usuario SET 
                    usu_nome = '".$model->getNome()."', 
                    usu_login = '".$model->getLogin()."', 
                    usu_senha = '".$model->getSenha()."', 
                    usu_cookie = '".$model->getCookie()."' WHERE usuario_id = ".$model->getID();

                    //checka se está accessando remotamente
                       if($this->checkCadastroRemoto()){
                        //verifica no banco se já tem um modelo cadastrado previamente
                        $sql1="SELECT DISTINCT A.modelo_id, A.mod_nome  FROM modelo A INNER JOIN modelo_area B ON A.modelo_id = B.modelo_id INNER JOIN area_usuario C ON C.area_id = B.area_id INNER JOIN usuario D ON D.usuario_id = C.usuario_id WHERE D.usuario_id = ".$model->getID()." AND mod_nome IN (".MODELOS_PORTAL.")";
                        //   $sql1="SELECT  r.modelo_id FROM rodape r LEFT OUTER JOIN modelo m ON m.modelo_id=r.modelo_id AND m.mod_nome IN  ( ".MODELOS_PORTAL." ) WHERE r.usuario_id=".$model->getID()."  ";
                        //$sql1 = "UPDATE modelo SET mod_nome = '".base64_decode($_REQUEST['template'])."', mod_descricao = '".  base64_decode($_REQUEST['logo'])."' WHERE modelo_id = (SELECT modelo_id FROM (SELECT modelo.modelo_id FROM modelo INNER JOIN rodape ON modelo.modelo_id = rodape.modelo_id WHERE rodape.usuario_id=".$model->getID().") AS temp)";

                        $modelosCadastrados = $this->getConexao()->executeQuery($sql1);
                        $modelosPortal= explode(",", MODELOS_PORTAL);
                        $modelosAtualizar = array();
                        
                        for ($index = 0; $index < $modelosCadastrados->RowCount(); $index++) {
                                $row = $modelosCadastrados->getRow($index);
                                if (in_array("'".$row->mod_nome."'", $modelosPortal)) {
                                $modelosAtualizar [] = $row->modelo_id;
                                }
                        }
                        // atualiza os modelos se o usuário tem todos os layouts cadastrados, também em caso de adicionar novos layouts
                        if($modelosCadastrados->RowCount()>=count($modelosPortal)){
                            // atualiza o template (layout) e o logo é salvado na descricao
                            $sql2 = "UPDATE modelo SET mod_descricao = '" . base64_decode($_REQUEST['logo']) . "', mod_empresa= '" . base64_decode($_REQUEST['empresa']) . "' WHERE modelo_id IN (".  implode(",", $modelosAtualizar).")";
                                    $table = $this->getConexao()->executeNonQuery($sql2);
                               
                        }else{
                            
                            //pega as área de interesse do usuário
                            $sql3 = "SELECT a.area_id FROM area a INNER JOIN area_usuario au ON a.area_id=au.area_id WHERE au.usuario_id =".$model->getID();
                            $table = $this->getConexao()->executeQuery($sql3);
                            $area_id=$table->getRow(0)->area_id;
                            if($modelosCadastrados->RowCount()>0)
                            $this->apagaModelos($modelosCadastrados);
                            //cadastra o modelo pra ele
                            $this->cadastroModeloRemoto($model->getID(),$area_id);
                        }
                        
                       }
                       
                }
                
                $this->getConexao()->executeNonQuery($sql);
                
    }
        
        function load($key){
    }
        
        function delete($key){
    }
        
        function create(){
    }

     function cadastroModeloRemoto($usuario_id,$area_id){
                //INSERE O MODELO NA TABELA MODELO
                $modelos = explode(",", MODELOS_PORTAL);
                for ($index = 0; $index < count($modelos); $index++) {
                $sqlmodelo = "INSERT INTO modelo (mod_nome,mod_descricao, mod_empresa) VALUES (".$modelos[$index].",'".mysql_real_escape_string(base64_decode($_REQUEST['logo']))."','".base64_decode($_REQUEST['empresa'])."' )";
                $error = $this->getConexao()->executeNonQuery($sqlmodelo);
                if($error!= -1){
                   //PEGA O ULTIMO ID MODELO INSERIDO
                    $modelo_id = $this->getLastId("modelo", "modelo_id");
                    //INSERE O RODAPE NA TABELA RODAPE
                    $sqlrodape = "INSERT INTO rodape (rod_texto,modelo_id, usuario_id) VALUES ( '".mysql_real_escape_string(base64_decode($_REQUEST['rodape']))."', ".$modelo_id.",  ".$usuario_id." )";
                    $error = $this->getConexao()->executeNonQuery($sqlrodape);
                    if($error!= -1){
                    //associa o modelo com a area de interesse
                    $sqlModeloArea = "INSERT INTO modelo_area (modelo_id,area_id) VALUES (".$modelo_id.",".$area_id.")";
                    $error = $this->getConexao()->executeNonQuery($sqlModeloArea);
                    }else{
                    print msg_alert("Registro não foi salvo." . $this->getConexao()->getErro());
                    }
                }else{
                print msg_alert("Registro não foi salvo." . $this->getConexao()->getErro());
                }
                }

                
            }
     function apagaModelos($modelos){
                //INSERE O MODELO NA TABELA MODELO

                $apagarModelos= array();
                    for ($index = 0;$index < $modelos->RowCount();$index++) {
                    $apagarModelos[]=$modelos->getRow($index)->modelo_id;
                    }
                
                $sqlmodelo = "DELETE FROM modelo WHERE modelo_id IN (".implode(",", $apagarModelos).")";
                $error = $this->getConexao()->executeNonQuery($sqlmodelo);
                if($error!= -1){
                    //APAGA O RODAPE
                    
                    $sqlrodape = "DELETE FROM rodape WHERE modelo_id IN (".implode(",", $apagarModelos).")";
                    $error = $this->getConexao()->executeNonQuery($sqlrodape);
                    
                    if($error!= -1){
                    //associa o modelo com a area de interesse
                    $sqlModeloArea = "DELETE FROM modelo_area WHERE modelo_id IN modelo_id IN (".implode(",", $apagarModelos).")";
                    $error = $this->getConexao()->executeNonQuery($sqlModeloArea);
                    }else{
                    print msg_alert("Registro não foi apagado." . $this->getConexao()->getErro());
                    }
                }else{
                print msg_alert("Registro não foi apagado." . $this->getConexao()->getErro());
                }
                


            }

    function getLastId($table, $id) {
        $sql = "SELECT MAX($id) as id FROM $table ";
        $lastid = $this->getConexao()->executeQuery($sql);
        $row = $lastid->getRow(0);
        return $row->id;
    }


    
     function permissaoEditor($usuario_id) {

      $sql = '
        INSERT INTO permissoes (permissao_id, usuario_id, acao_id, per_arquivo) VALUES
            (NULL , '.$usuario_id.', 1, "alterar_areas_noticia"),
            (NULL , '.$usuario_id.', 2, "alterar_areas_noticia"),
            (NULL , '.$usuario_id.', 3, "alterar_areas_noticia"),
            (NULL , '.$usuario_id.', 4, "alterar_areas_noticia"),
            (NULL , '.$usuario_id.', 5, "alterar_areas_noticia"),
            (NULL , '.$usuario_id.', 1, "assinante"),
            (NULL , '.$usuario_id.', 2, "assinante"),
            (NULL , '.$usuario_id.', 3, "assinante"),
            (NULL , '.$usuario_id.', 4, "assinante"),
            (NULL , '.$usuario_id.', 5, "assinante"),
            (NULL , '.$usuario_id.', 1, "assinar_newsletter"),
            (NULL , '.$usuario_id.', 2, "assinar_newsletter"),
            (NULL , '.$usuario_id.', 3, "assinar_newsletter"),
            (NULL , '.$usuario_id.', 4, "assinar_newsletter"),
            (NULL , '.$usuario_id.', 5, "assinar_newsletter"),
            (NULL , '.$usuario_id.', 1, "cadastrar_comentario"),
            (NULL , '.$usuario_id.', 2, "cadastrar_comentario"),
            (NULL , '.$usuario_id.', 3, "cadastrar_comentario"),
            (NULL , '.$usuario_id.', 4, "cadastrar_comentario"),
            (NULL , '.$usuario_id.', 5, "cadastrar_comentario"),
            (NULL , '.$usuario_id.', 6, "cadastrar_comentario"),
            (NULL , '.$usuario_id.', 10, "cadastrar_comentario"),
            (NULL , '.$usuario_id.', 1, "gestao_colaborador"),
            (NULL , '.$usuario_id.', 4, "gestao_colaborador"),
            (NULL , '.$usuario_id.', 1, "gestao_noticia"),
            (NULL , '.$usuario_id.', 2, "gestao_noticia"),
            (NULL , '.$usuario_id.', 3, "gestao_noticia"),
            (NULL , '.$usuario_id.', 4, "gestao_noticia"),
            (NULL , '.$usuario_id.', 5, "gestao_noticia"),
            (NULL , '.$usuario_id.', 1, "girar_noticia"),
            (NULL , '.$usuario_id.', 2, "girar_noticia"),
            (NULL , '.$usuario_id.', 3, "girar_noticia"),
            (NULL , '.$usuario_id.', 4, "girar_noticia"),
            (NULL , '.$usuario_id.', 5, "girar_noticia"),
            (NULL , '.$usuario_id.', 1, "indicar_noticia"),
            (NULL , '.$usuario_id.', 2, "indicar_noticia"),
            (NULL , '.$usuario_id.', 3, "indicar_noticia"),
            (NULL , '.$usuario_id.', 4, "indicar_noticia"),
            (NULL , '.$usuario_id.', 5, "indicar_noticia"),
            (NULL , '.$usuario_id.', 6, "indicar_noticia"),
            (NULL , '.$usuario_id.', 10, "indicar_noticia"),
            (NULL , '.$usuario_id.', 4, "inserir_email"),
            (NULL , '.$usuario_id.', 1, "noticia"),
            (NULL , '.$usuario_id.', 2, "noticia"),
            (NULL , '.$usuario_id.', 3, "noticia"),
            (NULL , '.$usuario_id.', 4, "noticia"),
            (NULL , '.$usuario_id.', 5, "noticia"),
            (NULL , '.$usuario_id.', 3, "remover_usuario_email"),
            (NULL , '.$usuario_id.', 4, "remover_usuario_email"),
            (NULL , '.$usuario_id.', 1, "visualizar_noticia"),
            (NULL , '.$usuario_id.', 2, "visualizar_noticia"),
            (NULL , '.$usuario_id.', 3, "visualizar_noticia"),
            (NULL , '.$usuario_id.', 4, "visualizar_noticia"),
            (NULL , '.$usuario_id.', 5, "visualizar_noticia")';
      
      $sql2 = '
        INSERT INTO permissoes (permissao_id, usuario_id, acao_id, per_arquivo) VALUES
            (NULL , '.$usuario_id.', 1, "assinatura"),
            (NULL , '.$usuario_id.', 2, "assinatura"),
            (NULL , '.$usuario_id.', 3, "assinatura"),
            (NULL , '.$usuario_id.', 4, "assinatura"),
            (NULL , '.$usuario_id.', 5, "assinatura"),
            (NULL , '.$usuario_id.', 4, "colaborador"),
            (NULL , '.$usuario_id.', 1, "comentario"),
            (NULL , '.$usuario_id.', 2, "comentario"),
            (NULL , '.$usuario_id.', 3, "comentario"),
            (NULL , '.$usuario_id.', 4, "comentario"),
            (NULL , '.$usuario_id.', 5, "comentario"),
            (NULL , '.$usuario_id.', 1, "gestao_areas_de_interesse"),
            (NULL , '.$usuario_id.', 2, "gestao_areas_de_interesse"),
            (NULL , '.$usuario_id.', 3, "gestao_areas_de_interesse"),
            (NULL , '.$usuario_id.', 4, "gestao_areas_de_interesse"),
            (NULL , '.$usuario_id.', 5, "gestao_areas_de_interesse"),
            (NULL , '.$usuario_id.', 11, "gestao_areas_de_interesse"),
            (NULL , '.$usuario_id.', 1, "gestao_assinante"),
            (NULL , '.$usuario_id.', 2, "gestao_assinante"),
            (NULL , '.$usuario_id.', 3, "gestao_assinante"),
            (NULL , '.$usuario_id.', 4, "gestao_assinante"),
            (NULL , '.$usuario_id.', 5, "gestao_assinante"),
            (NULL , '.$usuario_id.', 1, "gestao_assinatura"),
            (NULL , '.$usuario_id.', 2, "gestao_assinatura"),
            (NULL , '.$usuario_id.', 3, "gestao_assinatura"),
            (NULL , '.$usuario_id.', 4, "gestao_assinatura"),
            (NULL , '.$usuario_id.', 5, "gestao_assinatura"),
            (NULL , '.$usuario_id.', 2, "gestao_colaborador"),
            (NULL , '.$usuario_id.', 3, "gestao_colaborador"),
            (NULL , '.$usuario_id.', 5, "gestao_colaborador"),
            (NULL , '.$usuario_id.', 1, "gestao_janela"),
            (NULL , '.$usuario_id.', 2, "gestao_janela"),
            (NULL , '.$usuario_id.', 3, "gestao_janela"),
            (NULL , '.$usuario_id.', 4, "gestao_janela"),
            (NULL , '.$usuario_id.', 5, "gestao_janela"),
            (NULL , '.$usuario_id.', 1, "gestao_newsletter"),
            (NULL , '.$usuario_id.', 2, "gestao_newsletter"),
            (NULL , '.$usuario_id.', 3, "gestao_newsletter"),
            (NULL , '.$usuario_id.', 4, "gestao_newsletter"),
            (NULL , '.$usuario_id.', 5, "gestao_newsletter"),
            (NULL , '.$usuario_id.', 6, "gestao_newsletter"),
            (NULL , '.$usuario_id.', 7, "gestao_newsletter"),
            (NULL , '.$usuario_id.', 8, "gestao_newsletter"),
            (NULL , '.$usuario_id.', 9, "gestao_newsletter"),
            (NULL , '.$usuario_id.', 10, "gestao_newsletter"),
            (NULL , '.$usuario_id.', 11, "gestao_newsletter"),
            (NULL , '.$usuario_id.', 1, "importacao_noticia"),
            (NULL , '.$usuario_id.', 2, "importacao_noticia"),
            (NULL , '.$usuario_id.', 3, "importacao_noticia"),
            (NULL , '.$usuario_id.', 4, "importacao_noticia"),
            (NULL , '.$usuario_id.', 5, "importacao_noticia"),
            (NULL , '.$usuario_id.', 1, "janela"),
            (NULL , '.$usuario_id.', 2, "janela"),
            (NULL , '.$usuario_id.', 3, "janela"),
            (NULL , '.$usuario_id.', 4, "janela"),
            (NULL , '.$usuario_id.', 5, "janela"),
            (NULL , '.$usuario_id.', 1, "newsletter"),
            (NULL , '.$usuario_id.', 2, "newsletter"),
            (NULL , '.$usuario_id.', 3, "newsletter"),
            (NULL , '.$usuario_id.', 4, "newsletter"),
            (NULL , '.$usuario_id.', 4, "gestao_resumo_newsletter"),
            (NULL , '.$usuario_id.', 5, "newsletter")';
        $this->getConexao()->executeNonQuery($sql);
        $this->getConexao()->executeNonQuery($sql2);
    }

    function checkCadastroRemoto(){
        // checka que todas as variaveis sejan enviadas na peticao pra poder cadastrar o novo usuario o atualizar lo
        if(isset ($_REQUEST['nome'])&&isset ($_REQUEST['template'])&&isset ($_REQUEST['empresa'])&&isset ($_REQUEST['logo'])&&isset ($_REQUEST['0643e1bdb28d5604230a59b61e9f3fa7'])&&$_REQUEST['6fc1a4d8a307761725b7fa935305b456']&&isset ($_REQUEST['rodape']))
        return true;
        else
        return false;
    }

    function checkLoginRemoto(){
        if(isset ($_REQUEST["0643e1bdb28d5604230a59b61e9f3fa7"])&&isset ($_REQUEST["6fc1a4d8a307761725b7fa935305b456"]))
            return true;
        else
            return false;
    }

    function identificarUsuario() {
        //if (!empty($_COOKIE['user_key'])){
        //        $cookie = $_COOKIE['user_key'];        
        
        //if (!empty($cookie)&&isset($_SESSION['USUARIO'])) {
        if (isset($_SESSION['USUARIO'])) {   
            $sql = "SELECT * FROM usuario WHERE usu_login = '".$_SESSION['USUARIO']."'";
            
            $table = $this->getConexao()->executeQuery($sql);
            
            if ($table->RowCount() > 0) {
                
                // Cria nova chave
                $usuario = new Usuario();
                $usuario->bind($table->getRow(0));
                // var_dump($usuario);
                // die("USUARIO");
                return $usuario;
                

            }

        }else{
            return null;
        }
    }

    function validarUsuario() {

        if  ($this->checkLoginRemoto()){
            $login = filtroTexto($_REQUEST["0643e1bdb28d5604230a59b61e9f3fa7"]);
            $senha = filtroTexto($_REQUEST["6fc1a4d8a307761725b7fa935305b456"]);
        }else{
            $login = filtroTexto($_POST["txtLogin"]);
            $senha =base64_encode(filtroTexto($_POST["txtSenha"]));
        }
        
        $sql = "SELECT * FROM usuario WHERE usu_login = '$login' AND usu_senha = '$senha'";
        
        $table = $this->getConexao()->executeQuery($sql);
        
        if ($table->RowCount() > 0) {
            $usuario = new Usuario();
            $usuario->bind($table->getRow(0));

            if ($usuario != null) {
                if ($this -> associacaoUsuarioCliente($usuario)) {
                    $_SESSION["SESSION_LANG"] = $usuario->getIdioma();
                    $_SESSION["USUARIO"] = $usuario->getEMail();
                } else {
                    setcookie("user_key", NULL);
                    unset($_SESSION['USUARIO']);
                }
            }

           
            return $usuario;
        }else{
            if($this->checkCadastroRemoto()){
                $usuario = new Usuario();
                $usuario->setLogin($login);
                $usuario ->setEMail($login);
                $usuario->setSenha($senha);
                $usuario->setNome($_REQUEST['nome']);
                if ($usuario != null) {
                    $_SESSION["SESSION_LANG"] = "pt-BR";
                    $_SESSION["USUARIO"]=$usuario->getEMail();
                }
                return $usuario;
            }
        }

        return null;
    }

    function associacaoUsuarioCliente($usuario) {

        $sql = "
            SELECT
                usuario.*
                , acrwclie.*
            FROM
                usuario
                , cliente_usuario
                , acrwclie
            WHERE
                usuario.usuario_id = cliente_usuario.usuario_id
                AND cliente_usuario.cliente_id = acrwclie.CLIE_codigo
                AND usuario.usuario_id = '".$usuario -> getID()."'
                ";

        $table = $this->getConexao()->executeQuery($sql);

        if ($table->RowCount() > 0) {
            return true;
        } else {
            return false;
        }

    }

/*

INSERT INTO
 acrwclie_new (
  CLIE_Codigo
  , CLIE_Nome
  , CLIE_NomeParaRelatorios
  , CLIE_Sigla
  , CONT_PrazoCarencia
  , CONT_Situacao
  , CONTASVENCIDAS
 )
SELECT
 T1.CLIE_Codigo
 , T1.CLIE_Nome
 , T1.CLIE_NomeParaRelatorios
 , T1.CLIE_Sigla
 , T1.CONT_PrazoCarencia
 , T1.CONT_Situacao
 , T1.CONTASVENCIDAS
FROM
 acrwclie_mirror AS T1
-- ORDER BY
 -- T1.CLIE_Codigo ASC
ON DUPLICATE KEY
UPDATE
 CLIE_Nome = T1.CLIE_Nome
 , CLIE_NomeParaRelatorios = T1.CLIE_NomeParaRelatorios
 , CLIE_Sigla = T1.CLIE_Sigla
 , CONT_PrazoCarencia = T1.CONT_PrazoCarencia
 , CONT_Situacao = T1.CONT_Situacao
 , CONTASVENCIDAS = T1.CONTASVENCIDAS

INSERT INTO
 acrwclie_mirror
SELECT
 DISTINCT T1.CLIE_Codigo
 , T1.CLIE_Nome
 , T1.CLIE_NomeParaRelatorios
 , T1.CLIE_Sigla
 , T1.CONT_PrazoCarencia
 , T1.CONT_Situacao
 , T1.CONTASVENCIDAS
FROM
 acrwregistrador AS T1
WHERE
 T1.SIST_Codigo = 'NWE'
 -- AND T1.CONT_Situacao <> 9
-- ORDER BY
 -- T1.CLIE_Codigo ASC
ON DUPLICATE KEY
UPDATE
 CLIE_Nome = T1.CLIE_Nome
 , CLIE_NomeParaRelatorios = T1.CLIE_NomeParaRelatorios
 , CLIE_Sigla = T1.CLIE_Sigla
 , CONT_PrazoCarencia = T1.CONT_PrazoCarencia
 , CONT_Situacao = T1.CONT_Situacao
 , CONTASVENCIDAS = T1.CONTASVENCIDAS

*/
 
    function condicaoAdimplencia($usuario, $file) {

        $booClienteAdimplente = false;

        $sql = "
            SELECT
                usuario.*
                , acrwclie.*
            FROM
                usuario
                , cliente_usuario
                , acrwclie
            WHERE
                usuario.usuario_id = cliente_usuario.usuario_id
                AND cliente_usuario.cliente_id = acrwclie.CLIE_codigo
                AND usuario.usuario_id = '".$usuario -> getID()."'
                ";

        $table = $this->getConexao()->executeQuery($sql);

        if ($table->RowCount() > 0) {

            for ($intLinha = 0; $intLinha < $table -> RowCount(); $intLinha++) {

                $dtPrazoCarencia = $table -> getRow($intLinha) -> CONT_PrazoCarencia;

                $intContaSituacao = $table -> getRow($intLinha) -> CONT_Situacao;

                $intContasVencidas = $table -> getRow($intLinha) -> CONTASVENCIDAS;

                if (isset($_REQUEST["print_debug"])) {
                    print $sql . "<br><br>";
                    print $table -> RowCount() . "<br><br><pre>";
                    print_r($table -> getRow($intLinha));
                    print "</pre><br>";

                }

                if (($intContaSituacao != 9
                    && $intContaSituacao != null
                    && $intContasVencidas <= LIMITE_ATRAZO_ADIMPLENCIA
                    && $intContasVencidas != null)
                    || (strtotime(date("Y-m-d H:i:s")) < strtotime($dtPrazoCarencia))
                    && !$booClienteAdimplente) {
                    $booClienteAdimplente = true;
                }

            }

        }

        if ($file == "gestao_newsletter") {
            if ($booClienteAdimplente)
                return true;
            else
                return false;
        } else {
            return true;
        }

    }

    function validarAcao(&$file, &$action){

        switch ($action) {

            case "processUser":
                $user = $this->validarUsuario();
                if ($user == null) {
                    print msg_alert('Usuário ou senha inválidos');
                    print redirect("index.php");
                } else {
                    if (isset($_SESSION["USUARIO"])) {
                        $user->setCookie(gerarChave());
                        $this->save($user);
                        setcookie("user_key", $user->getCookie());
                        if(isset ($_REQUEST['api'])){
                            $this->openAfterLogin();
                            print redirect("index.php?".PARAMETER_NAME_ACTION."=".$_REQUEST['action']."&".PARAMETER_NAME_FILE."=".$_REQUEST['secao']);
                        }
                    } else {
                        print msg_alert('Seu usuário não está associado a um cliente, procure o suporte da BisaWeb.');
                    }
                    print redirect("index.php");
                }
                break;

            case "logoff":
                setcookie("user_key", NULL);
                unset($_SESSION['USUARIO']);
                print redirect("index.php");
                break;

            case "login":
                $view = new view_seguranca($this);
                $view->show();
                $file = null;
                break;

            default:

                $usuario = $this->identificarUsuario();

                if ($usuario != null) { 

                    $booAdimplente = false;
                    if ($this->condicaoAdimplencia($usuario, $file)) {
                        $booAdimplente = true;
                    }
                    
                    $sql = "SELECT * FROM perfil_usuario WHERE usuario_id = '".$usuario->getID()."'";
                    $perfil = $this->getConexao()->executeQuery($sql);
                    $perfil = $perfil->getRow(0);
                    $perfil = $perfil->perfil_id;
                    
                    if($file != 'principal' && $file != 'colaborador' && $file != 'resetar_senha'  && $file != 'api' && $file != 'redefinir_senha' && 
                       $file != 'janela_cli' && $file != 'assinar_newsletter' && $file != 'buscar_noticia' && $file != 'exibir_noticia' && 
                       $file != 'comentario' && $file != 'gestao_cliente' && $file != 'janela_livre' && $file != 'gestao_janela_livre' && $file != 'janela_livre_cli'&& $file != 'api_noticias' && $file != 'gestao_resumo_newsletter' && $file != 'estatistica_email_por_area') {

                        if (!$booAdimplente) {

                            print msg_alert('Procure o departamento financeiro da BisaWeb.');
                            $view = new view_seguranca($this);
                            $view->show();
                            $file = null;

                        } else {

                            if($perfil != 1) {
                            
                                $sql = "SELECT * FROM acao WHERE aca_descricao = '".$action."'";
                                $acao = $this->getConexao()->executeQuery($sql);
                                $acao = $acao->getRow(0);
                                $sql = "SELECT * FROM permissoes WHERE usuario_id = '".$usuario->getID()."' AND acao_id = '".$acao->acao_id."' AND per_arquivo = '".$file."'";
                                
                                //if ($usuario != null) {
                                    //Usuario logado               
                                    //$sql = "SELECT * FROM vw_usuario_arquivo WHERE usu_cookie = '".$usuario->getCookie()."' AND aca_descricao = '".$action."' AND arq_chave = '".$file."'";    
                                //} else {
                                    //Usuario nao logado
                                    //$sql = "SELECT * FROM vw_anonimo_arquivo WHERE aca_descricao = '".$action."' AND arq_chave = '".$file."'";
                                //}
                                
                                $table = $this->getConexao()->executeQuery($sql);
                                if ($table->RowCount() == 0) {
                                    //print msg_alert("Usuário não tem permissão para acessar");
                                    if (LOGIN_ON_ACCESS_DENIED != "1") {
                                        $action = ACTION_DEFAULT;
                                        $file = FILE_DEFAULT;
                                    } else {
                                        $action = ACTION_LOGIN;
                                        $this->validarAcao(carregarArquivo(), $action);
                                    }
                                }

                            }

                        }
                    }

                } else {

                    
                    //Usuario nao logado
                    //$sql = "SELECT * FROM vw_anonimo_arquivo WHERE aca_descricao = '".$action."' AND arq_chave = '".$file."'";
                    $file = str_replace(" ", "", $file);
                    if ($file != 'principal' && $file != 'blog' && $file != 'sugestao' && $file != 'colaborador' && $file != 'remover_usuario_email' && 
                        $file != 'resetar_senha'  && $file != 'api' && $file != 'redefinir_senha' && $file != 'janela_cli' && $file != 'assinar_newsletter' && 
                        $file != 'buscar_noticia' && $file != 'exibir_noticia' && $file != 'janela_livre_cli' && $file != 'api_noticias' &&  
                        $file != 'exibir_noticia' && ($file != 'comentario' || $action!="save") && !array_key_exists('selecaoMiniatura',$_REQUEST)) {
                  

                        print msg_alert("Usuário não tem permissão para acessar");
                        if (LOGIN_ON_ACCESS_DENIED != "1") {
                            $action = ACTION_DEFAULT;
                            $file = FILE_DEFAULT;
                        } else {
                            $action = ACTION_LOGIN;
                            @$this->validarAcao(carregarArquivo(), $action);
                        }
                    }    

                }

                break;
        }
    }
}

?>
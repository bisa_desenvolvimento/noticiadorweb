<?
class controller_adm_gestao_arquivos extends TController
{
	function show() {
		$view = new view_adm_gestao_arquivos($this);
		$view->show();
	}
	
	function save($model){
		if ($model->getID() != null) {
			$sql = "UPDATE ARQUIVO SET arq_arquivo = '".$model->getArquivo()."', arq_chave='".$model->getChave()."', arq_descricao='".$model->getDescricao()."' WHERE arquivo_id = ".$model->getID();
		} else {
			$sql = "INSERT INTO ARQUIVO (arq_arquivo, arq_chave, arq_descricao) VALUES ('".$model->getArquivo()."', '".$model->getChave()."', '".$model->getDescricao()."')";
		}
		
		return $this->getConexao()->executeNonQuery($sql);
	}
	
	function load($key){
		$sql = "SELECT * FROM ARQUIVO WHERE arquivo_id = " . $key;
		$table = $this->getConexao()->executeQuery($sql);
		if ($table->RowCount() > 0) {
			$result = new Arquivo();
			$result->bind($table->getRow(0));
			if ($result != null) {
				$view = new view_adm_gestao_arquivos($this);
				$view->setModel($result);
				$view->show();
			}
			return $result;
		}
		return null;
	}
	
	function delete($key){
		$table = $this->getConexao()->executeQuery("SELECT * FROM PERFIL_ARQUIVO_ACAO WHERE arquivo_id = $key");
		if ($table != null) {
			if ($table->RowCount() > 0) {
				print msg_alert(translate("Registro não pode ser excluido por estar em uso."));
			} else {
				$this->getConexao()->executeNonQuery("DELETE FROM ARQUIVO_ACAO WHERE arquivo_id = $key");
				$this->getConexao()->executeNonQuery("DELETE FROM ARQUIVO WHERE arquivo_id = $key");
				print msg_alert(translate("Registro excluido com sucesso"));	
			}
		} else {
			$this->getConexao()->executeNonQuery("DELETE FROM ARQUIVO_ACAO WHERE arquivo_id = $key");
			$this->getConexao()->executeNonQuery("DELETE FROM ARQUIVO WHERE arquivo_id = $key");
			print msg_alert(translate("Registro excluido com sucesso"));
		}
		
		$this->show();
	}
	
	function create(){
		
	}	
	
	function process() {
		parent::process();
		
		switch($this->getAction()) {
			case "save":
				$arquivo = new Arquivo();
				$arquivo->setArquivo(filtroTexto($_POST["txtNome"]));
				$arquivo->setDescricao(filtroTexto($_POST["txtDescricao"]));
				$arquivo->setChave(gerarChave(filtroTexto($_POST["txtDescricao"])));
				
				if (isset($_POST["id"])) 
					$arquivo->setID($_POST["id"]);
					
				$error = $this->save($arquivo);
				if (!isset($_POST["id"])) 
					$arquivo->setID($this->getConexao()->getLastID());
				
				if ($arquivo->getID() != 0) {
					$sql = "DELETE FROM ARQUIVO_ACAO WHERE arquivo_id = ".$arquivo->getID();
					if (isset($_POST["cbAcao"]))
					{
						$acoes = $_POST["cbAcao"];
						$this->getConexao()->executeNonQuery($sql);
						foreach($acoes as $acao) {
							$sql = "INSERT INTO ARQUIVO_ACAO(arquivo_id, acao_id, arq_aca_permite_anonimo) VALUES (".$arquivo->getID().", $acao, 0)";
							$this->getConexao()->executeNonQuery($sql);
						}	
					}
					
					
				}
				
				if ($error != -1){
					print msg_alert(translate("Registro salvo com sucesso"));
				} else {
					print msg_alert(translate("Não foi possivel salvar").$this->getConexao()->getErro());
				}
				
				$this->show();
				break;
		}
	}
	
	function obterListaAcoes($idArquivo = null) {
		if (empty($idArquivo)) $idArquivo = "null";
		
		$sql = "SELECT DISTINCT A.acao_id, A.aca_descricao, B.arquivo_id, B.arq_aca_permite_anonimo FROM ACAO A INNER JOIN ARQUIVO_ACAO B ON A.acao_id = B.acao_id WHERE B.arquivo_id = $idArquivo UNION SELECT DISTINCT A.acao_id, A.aca_descricao, NULL, NULL FROM ACAO A WHERE A.acao_id NOT IN (SELECT acao_id FROM ARQUIVO_ACAO WHERE arquivo_id = $idArquivo)";

	    $result = $this->getConexao()->executeQuery($sql);
	    
	    if ($result != null) {
	        return $result;
	    }
	    
	    return null;
	}
	
	function obterListaArquivosCadastrados() {
		$sql = "SELECT DISTINCT * FROM ARQUIVO ORDER BY arq_arquivo";
	    
	    $result = $this->getConexao()->executeQuery($sql);
	    
	    if ($result != null) {
	        return $result;
	    }
	    
	    return null;
	}
}
?>
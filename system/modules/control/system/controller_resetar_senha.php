<?
class controller_resetar_senha extends TController {

    function process() { 
    	parent::process();
    	
    	switch ($this->getAction()) {
    		case "save":
        
          if($this->verificaEmail($_POST['txtEmail'])) {
            
            $sql = "SELECT * FROM usuario WHERE usu_email = '".$_POST['txtEmail']."'";
            $table = $this->getConexao()->executeQuery($sql);
            $row = $table->getRow(0);
            
            //Enviar email
            $titulo = 'Redefini��o de senha NoticiadorWeb';
            
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "From: Noticiador Web <www-data@portalbisa.com.br>\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
            
            $link = URL.'index.php?'.PARAMETER_NAME_ACTION.'=show&'.PARAMETER_NAME_FILE.'=redefinir_senha&email='.$row->usu_email.'&cod='.$row->usu_senha;
            $mensagem = '<html><body>';
            $mensagem .= 'Ol� '.$row->usu_nome.',<br /><br />';
            $mensagem .= 'Algu�m solicitou que sua senha do NoticiadorWeb fosse alterada.<br /><br />';
            $mensagem .= 'Caso n�o tenha sido voc�, n�o tem com o que se preocupar. Ignore este email e nada ser� alterado.<br /><br />';
            $mensagem .= 'Caso tenha realmente solicitado a altera��o de senha clique no link abaixo:<br />';
            $mensagem .= '<a href="'.$link.'&nome='.urlencode($row->usu_nome).'">'.$link.'</a><br /><br />';
            $mensagem .= 'Atenciosamente,<br />NoticiadorWeb';
            $mensagem .= '</body></html>';
            
            Email::sendEmail($row->usu_email,$titulo,$mensagem);
            //mail($row->usu_email, $titulo, $mensagem,$headers);
            print msg_alert("Verifique seu email e siga os passos para a redifini��o da senha.");
          } else {
            print msg_alert("Email n�o encontrado em nosso sistema.");
          }
          
          $view = new view_resetar_senha($this);
          print "<script>parent.location.href='index.php'</script>";
          break;
    	}        
    }

    public function show() { 
      $view = new view_resetar_senha($this);
      $view->show();
    }
    
    public function save($model) {
      
    }
    
    public function load($key) { }
    
    public function delete($model) { }
    
    public function create() { }
    
    private function verificaEmail($email) {
      
      $sql = "SELECT * FROM usuario WHERE usu_email = '$email'";
      $table = $this->getConexao()->executeQuery($sql);
      
      if($table->RowCount() > 0) {
        $row = $table->getRow(0);
        return $row->usu_senha;
      } else {
        return false;
      }
      
    }
}
?>
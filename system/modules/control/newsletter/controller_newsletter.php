<?
class controller_newsletter extends TController {
	function show() {
		$view = new view_newsletter($this);
		$view->show();
	}

	function save($model) {
	    
		if ($model->getID() == null) {
                    $usuario = controller_seguranca::getInstance()->identificarUsuario();
			$sql = "INSERT INTO modelo (mod_descricao, mod_nome, cliente_id) VALUES ('" . $model->getDescricao() . "', '" . $model->getNome() . "', '". $model->getCliente() ."' )";
            $resmodelo = $this->getConexao()->executeNonQuery($sql);
            $modid = $this->getConexao()->getLastID();
            //$sqlr = "INSERT INTO rodape (rod_texto, modelo_id, usuario_id) VALUES ('".$model->getRodape()."', '".$modid."', '".$usuario->getID()."')";            
		} else {
			$sql = "UPDATE modelo SET mod_descricao = '" . $model->getDescricao() . "', mod_nome = '" . $model->getNome() . "', cliente_id = '". $model->getCliente() ."'  WHERE modelo_id = " . $model->getID();
            $resmodelo = $this->getConexao()->executeNonQuery($sql);
            $modid = $model->getID();
            //$sqlr = "UPDATE rodape SET rod_texto='". $model->getRodape() ."'  WHERE modelo_id =". $model->getID() ." AND usuario_id = ".$model->getID();
		}
        //$this->getConexao()->executeNonQuery($sqlr);
        
        if($resmodelo != -1) {
            return $modid;    
        } else {
            return -1;
        }
		
	}

	function load($key) {
		$sql = "SELECT * FROM modelo WHERE modelo_id = " . $key;
		$table = $this->getConexao()->executeQuery($sql);
		if ($table->RowCount() > 0) {
			$result = new Modelo();
			$result->bind($table->getRow(0));
			if ($result != null) {
				$view = new view_newsletter($this);
				$view->setModel($result);
				$view->show();
			}
			return $result;
		}
		return null;
	}

	function obterAreasChecked($idModelo, $idArea) {
		$sql = 'SELECT * FROM modelo_area
				WHERE modelo_area.modelo_id = '.$idModelo.
			 	 ' AND modelo_area.area_id = '.$idArea;

        $noticia_area = $this->getConexao()->executeQuery($sql);

        if ($noticia_area->RowCount() > 0){

	        	return " checked ";
		}

        return null;
	}

	function delete($key) {
		// Remove os arquivos do diretorio
		$sql = "SELECT mod_nome FROM modelo WHERE modelo_id = $key";
		
		$table = $this->getConexao()->executeQuery($sql);
		if ($table->RowCount() > 0) {
			$row = $table->getRow(0);
			$dir = DIR_MODELOS.$row->mod_nome."/";
			
			if(file_exists($dir))

                            full_rmdir($dir);
		}

		$sql = "DELETE FROM modelo WHERE modelo_id = $key";

		$result = $this->getConexao()->executeNonQuery($sql);

		if ($result != -1) {
			print msg_alert("Registro removido.");
		} else
			print msg_alert("Registro não removido."."\\n" . $this->getConexao()->getErro());

		$this->show();
	}

	function create() {

	}

	function process() {
		parent :: process();

		switch ($this->getAction()) {
			case "save" :
                if (!isset($_GET['tipo']) && empty($_GET['tipo'])) {
				$usuario = controller_seguranca::getInstance()->identificarUsuario();
				$modelo = new Modelo();
				$modelo->setNome(filtroTexto($_POST["txtNome"]));
				$modelo->setDescricao(filtroTexto($_POST["txtDescricao"]));
                //$modelo->setRodape(filtroTexto($_POST["txtRodape"]));
                $modelo->setCliente($_POST["radCliente"]);
                
                if (!empty ($_POST["id"])) {
					$modelo->setID($_POST["id"]);
				}

				$result = $this->save($modelo);


                    // Jorge Roberto Mantis 5658 - Adicionar parâmetros a criacao do modelo do zero
                    $tipoConteudo = $_POST["tipoConteudo"] === "0" ? "<# materiaCompleta #>":"<# body #>"; // Se o conteudo da Newsletter sera noticia completa ou resumo

                    if (!empty ($_POST["headerLink"])) {
                       $headerLink   =  $_POST["headerLink"] ?: "#"; // Link relacionado ao header da Newsletter
                    }
                    if (!empty ($_POST["footerLink"])) {
                       $footerLink   = $_POST["footerLink"] ?: "#";
                    }
				
				//if ($modelo->getID() == null) {
//					$modelo->setID($this->getConexao()->getLastID());
//				}

				$sql = "DELETE FROM cliente_modelo WHERE modelo_id = ".$result;
				$this->getConexao()->executeNonQuery($sql);

				if(isset($_POST['cliente'])) {
					foreach($_POST['cliente'] as $cliente) {
						$sql = "INSERT INTO cliente_modelo(cliente_id, modelo_id) VALUES (".$cliente.", ".$result.")";
                        $this->getConexao()->executeNonQuery($sql);
					}
				}

				if ($result != -1) {
					// Prepara o arquivo
					if (isset ($_FILES["fileUpload"]) && !empty($_FILES["fileUpload"]["name"])) {
						// Move arquivo para diretorio de modelos
						if (!file_exists(DIR_MODELOS . filtroTexto($_POST["txtNome"])))
							mkdir(DIR_MODELOS.filtroTexto($_POST["txtNome"]));

						// Descomprime os arquivos
						$checkDecompress = unzip($_FILES["fileUpload"]["tmp_name"], DIR_MODELOS.filtroTexto($_POST["txtNome"])."/", false, false);

						if ($checkDecompress) {
							print msg_alert("Modelo salvo com sucesso.");	
						} else {
							print msg_alert("Não foi possivel salvar:"." \\n" . $this->getConexao()->getErro());	
						}
					}else{

            if(isset ($_POST['txtheader'])&&isset ($_POST['txtcontent'])&&isset ($_POST['txtfooter'])){
               $pastadestino= DIR_MODELOS . filtroTexto($_POST["txtNome"]);
               $pastaorigem=DIR_MODELOS."uploadajax/";
                if (!file_exists($pastadestino)){
                    // echo $pastadestino;
                    // die();  
                    mkdir($pastadestino,0777,true);
                    @rename($pastaorigem.$_POST['txtheader'], $pastadestino."/header.jpg");
                    @copy($pastadestino."/header.jpg",$pastadestino."/preview.jpg" );
                    @rename($pastaorigem.$_POST['txtcontent'], $pastadestino."/content.jpg");
                    @rename($pastaorigem.$_POST['txtfooter'], $pastadestino."/footer.jpg");
                }
                  

                  $buffer = '';
                  $arquivo = "modelo.html"; //nome do arquivo
                  $url = $pastadestino."/".$arquivo; //aqui coloca la ruta donde se encuentra el archivo
                  $fp = fopen($url, "a");

                  $modelo='<html>
                  <head>
                      <meta charset="utf-8">
                      <title></title>
                       <style type="text/css">
		                  @media only screen and (max-device-width: 480px) {
		                      table[class=conteudo] { 
		                          width:320px !important;
		                      }
		                      
		                      td[class=header] img {
		                          height:auto !important;
		                          width:100% !important;
		                      }

		                  }
		                  </style>
                  </head>
                  <body>
                      <a href="'. $headerLink .'">
                          <table border="0" cellpadding="0" cellspacing="0" class="conteudo">
                             <tr>
                                 <td ><img src="<# img_dir #>header.jpg" alt="Padrao">
                                  </td>
                             </tr>
                          </table>
                      </a>                       
                      <table background="<# img_dir #>content.jpg" width="640" border="0" cellpadding="0" cellspacing="0" class="conteudo">
                        <tr>
                            <td>					   
                            <table width="320" border="0" cellspacing="0" cellpadding="20" align="center">
                              <tr>
                                <td><# noticias #></td>
                              </tr>
                            </table>            					    
                            </td>
                          </tr>
                      </table>                     
                    <a href="'. $footerLink .'">
                        <table width="600" border="0" cellpadding="0" cellspacing="0" class="conteudo">
                            <tr>
                              <td height="<# img_dir_heigth_rodape #>" valign="top" >
                                               <table width="550" align="center" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                           <img src="<# img_dir #>footer.jpg" alt="Padrao">
                                          <td height="65" valign="top" align="center"><br /><br />
                                              <p><font size="2" face="Arial, Helvetica, sans-serif"><# rodape #></font></p>
                                                          </td>
                                        </tr>
                                      </table></td>
                            </tr>
                        </table>
                    </a>
                  </body>
                  </html>';
                  $noticia_modelo = '<dt><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#000000"><strong><# title #></strong></font></dt><dd>'.$tipoConteudo.'</dd><br />';
                  //file_put_contents($url, $modelo);
                  fwrite($fp, $modelo . PHP_EOL);
                  fclose($fp);
                  // escreve o arquivo modelo_noticia.html
                  $arquivo = "modelo_noticia.html"; //nome do arquivo
                  $url = $pastadestino."/".$arquivo; //aqui coloca la ruta donde se encuentra el archivo
                  $fm = fopen($url, "a");
                  $modelo_noticia= '<dt><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#000000"><strong><# title #></strong></font></dt><dd>'.$tipoConteudo.'</dd><br />';
                  fwrite($fm, $modelo_noticia . PHP_EOL);
                  fclose($fm);


                  if($this->apagarPastaComConteudo($pastaorigem,true)){
                    print msg_alert("Modelo salvo com sucesso.");
                  }else{
                    print msg_alert("Nãoa foi possivel salvar1:"." \\n" . $this->getConexao()->getErro());
                  }
            }

                                        }




				} else {
					print msg_alert("Não foi possivel salvar2:"." \\n" . $this->getConexao()->getErro());
				}
				print redirect("index.php");
                                }else{

                             // list of valid extensions, ex. array("jpeg", "xml", "bmp")
                            $allowedExtensions = array("jpg");
                            // max file size in bytes
                            $sizeLimit = 1 * 1024 * 1024;

			    if (!file_exists( DIR_MODELOS."uploadajax/")){
			    mkdir(DIR_MODELOS."uploadajax/");}


                            $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
                            $result = $uploader->handleUpload(DIR_USERS.'layouts/uploadajax/',true);

                            // to pass data through iframe you will need to encode all html tags
                            echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
                            //rename(DIR_MODELOS."uploadajax/", DIR_MODELOS."uploadajax2/");
                                }
				break;

                      


		}
	}

	function obterAreasCadastradas() {
		$usuario = controller_seguranca::getInstance()->identificarUsuario();

		$sql = 'SELECT DISTINCT area.area_id, area.are_descricao, area.are_observacao FROM area, area_usuario
        		WHERE area.area_id = area_usuario.area_id';
  
        $sql .= ' AND area_usuario.usuario_id = '.$usuario->getID();
    
  	
	    $result = $this->getConexao()->executeQuery($sql);

	    if ($result != null) {
	        return $result;
	    }

	    return null;
	}

	function apagarPastaComConteudo($dir, $borrarme)
        {
            
    if(!$dh = opendir($dir)) return;
    
    while (false !== ($obj = readdir($dh)))
    {
        if($obj=='.' || $obj=='..') continue;
        if (!unlink($dir.'/'.$obj)) borrar_directorio($dir.'/'.$obj, true);
    }
    closedir($dh);
    if ($borrarme)
    {
        if(rmdir($dir))
            return true;
    }
    }

    function obterModelosCadastrados() {
		$usuario = controller_seguranca::getInstance()->identificarUsuario();
		
		if ($usuario != null) {
		    $sql = "SELECT 
                      * 
                    FROM
                      modelo 
                    WHERE cliente_id IN 
                      (SELECT 
                        cliente_id 
                      FROM
                        cliente_usuario 
                      WHERE usuario_id = '".$usuario->getID()."') 
                      AND mod_nome NOT IN (".utf8_decode(MODELOS_PADROES).", ".utf8_decode(MODELOS_PORTAL).") 
                    ORDER BY mod_nome";            
            
            $table = $this->getConexao()->executeQuery($sql);
			if ($table->RowCount() > 0) {
				return $table;
			}
		}
		return null;
	}
        
	function obterTodosModelosCadastrados(){
            $sql = "SELECT DISTINCT A.mod_nome FROM modelo A 
                        INNER JOIN modelo_area B ON A.modelo_id = B.modelo_id 
                        INNER JOIN area_usuario C ON C.area_id = B.area_id 
                        INNER JOIN usuario D ON D.usuario_id = C.usuario_id 
                        ORDER BY A.mod_nome";
           
			$table = $this->getConexao()->executeQuery($sql);
			if ($table->RowCount() > 0) {
				return $table;
			}
        }
        
        
    /**
     * Função para obter a lista de clientes ativos associados ao usuário atualmente logado
     * 
     * @access public
     * @return Lista de clientes ativos associados ao usuário
     */
    public function obterClientes() {
        $usuario = controller_seguranca::getInstance()->identificarUsuario();
        
        $sql = 'SELECT 
                  * 
                FROM
                  vw_clientes_ativos';
                  
        if($this->obterPerfilUsuario($usuario->getID()) == PERFIL_EDITOR) {
            $sql .= ' WHERE CLIE_Codigo IN 
                      (SELECT 
                        cliente_id 
                      FROM
                        cliente_usuario 
                      WHERE usuario_id = '.$usuario->getID().')';
        }
      
        $table = $this->getConexao()->executeQuery($sql);
        if ($table->RowCount() > 0) {
            return $table;
        }
        return null;
    }
    
    
    /**
     * Função para obter o id do perfil do usuário
     * 
     * @access private
     * @param int $usuario Id do usuário
     * @return int Id do perfil do usuário
     */
    private function obterPerfilUsuario($usuario){

		$sql = "SELECT * FROM perfil_usuario WHERE usuario_id= ".$usuario;

		$perfil_usuario = $this->getConexao()->executeQuery($sql);

		if ($perfil_usuario->RowCount() > 0) {
			$row_perfil = $perfil_usuario->getRow(0);
			return $row_perfil->perfil_id;		
		}
		return null;		

	}
    
}


/**
 * Handle file uploads via XMLHttpRequest
 */
class qqUploadedFileXhr {
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    function save($path) {
        $input = fopen("php://input", "r");
        $temp = tmpfile();
        $realSize = stream_copy_to_stream($input, $temp);
        fclose($input);

        if ($realSize != $this->getSize()){
            return false;
        }

        $target = fopen($path, "w");
        fseek($temp, 0, SEEK_SET);
        stream_copy_to_stream($temp, $target);
        fclose($target);

        return true;
    }
    function getName() {
        return $_GET['qqfile'];
    }
    function getSize() {
        if (isset($_SERVER["CONTENT_LENGTH"])){
            return (int)$_SERVER["CONTENT_LENGTH"];
        } else {
            throw new Exception('Getting content length is not supported.');
        }
    }
}

/**
 * Handle file uploads via regular form post (uses the $_FILES array)
 */
class qqUploadedFileForm {
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    function save($path) {
        if(!move_uploaded_file($_FILES['qqfile']['tmp_name'], $path)){
            return false;
        }
        return true;
    }
    function getName() {
        return ($_FILES['qqfile']['name']);
    }
    function getSize() {
        return $_FILES['qqfile']['size'];
    }
}

class qqFileUploader {
    private $allowedExtensions = array();
    private $sizeLimit = 10485760;
    private $file;

    function __construct(array $allowedExtensions = array(), $sizeLimit = 10485760){
        $allowedExtensions = array_map("strtolower", $allowedExtensions);

        $this->allowedExtensions = $allowedExtensions;
        $this->sizeLimit = $sizeLimit;

        $this->checkServerSettings();

        if (isset($_GET['qqfile'])) {
            $this->file = new qqUploadedFileXhr();
        } elseif (isset($_FILES['qqfile'])) {
            $this->file = new qqUploadedFileForm();
        } else {
            $this->file = false;
        }
    }

    private function checkServerSettings(){
        $postSize = $this->toBytes(ini_get('post_max_size'));
        $uploadSize = $this->toBytes(ini_get('upload_max_filesize'));

        if ($postSize < $this->sizeLimit || $uploadSize < $this->sizeLimit){
            $size = max(1, $this->sizeLimit / 1024 / 1024) . 'M';
            die("{'error':'increase post_max_size and upload_max_filesize to $size'}");
        }
    }

    private function toBytes($str){
        $val = trim($str);
        $last = strtolower($str[strlen($str)-1]);
        switch($last) {
            case 'g': $val *= 1024;
            case 'm': $val *= 1024;
            case 'k': $val *= 1024;
        }
        return $val;
    }

    /**
     * Returns array('success'=>true) or array('error'=>'error message')
     */
    function handleUpload($uploadDirectory, $replaceOldFile = FALSE, $filename = null){
        if (!is_writable($uploadDirectory)){
            
            chmod($uploadDirectory, 0777);
        }

        if (!$this->file){
            return array('error' => 'No files were uploaded.');
        }

        $size = $this->file->getSize();

        if ($size == 0) {
            return array('error' => translate('O limite do arquivo e 1MB'));
        }

        if ($size > $this->sizeLimit) {
            return array('error' =>  translate('O limite do arquivo e 1MB'));
        }

       $pathinfo = pathinfo($this->file->getName());
        if($filename==null){
        $filename = $pathinfo['filename'];
        }

        //$filename = md5(uniqid());
        $ext = $pathinfo['extension'];

        if($this->allowedExtensions && !in_array(strtolower($ext), $this->allowedExtensions)){
            $these = implode(', ', $this->allowedExtensions);
            return array('error' => 'File has an invalid extension, it should be one of '. $these . '.');
        }

        if(!$replaceOldFile){
            /// don't overwrite previous files that were uploaded
            while (file_exists($uploadDirectory . $filename . '.' . $ext)) {
                $filename .= rand(10, 99);
            }
        }

        if ($this->file->save($uploadDirectory . $filename . '.' . $ext)){
            return array('success'=>true,'file'=>$_GET['tipo']);
        } else {
            return array('error'=> 'Could not save uploaded file.' .
                'The upload was cancelled, or server error encountered');
        }

    }
    
}


?>
<?
class controller_news_letters_enviadas extends TController {
	function show() {
		$view = new view_news_letters_enviadas($this);
		$view->show();
	}
	
	function save($obj) {
	}
	
	function load($id) {
	}
	
	function delete($id) {
	}
	
	function create() {
	}
	
	function listarNewslettersEnviadas() {
		$usuario = controller_seguranca::getInstance()->identificarUsuario();
		
		$sql = "SELECT A.newsletter_id, DATE_FORMAT(A.new_data_envio, '%d/%m/%Y %H:%i') as new_data_envio, A.new_chave, A.modelo_id 
                FROM newsletter A INNER JOIN newsletter_area B ON A.newsletter_id = B.newsletter_id INNER JOIN area_usuario C ON C.area_id = B.area_id 
                WHERE C.usuario_id = ".$usuario->getID();
		
		$table = $this->getConexao()->executeQuery($sql);
		
		if ($table->RowCount() > 0)
		{
			return $table;	
		}
		
		return null;
	}
}
?>
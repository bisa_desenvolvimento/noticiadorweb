<?

class controller_gestao_newsletter extends TController {

    function show() {
		$_SESSION['chave_novo_newsletter'] = gerarChave();
        $this->selecionarModel();
    }

    function save($model) {
        print "Chegou aqui";
    }

    function load($key) {

    }

    function delete($key) {

    }

    function create() {

    }

    function preview() {
        $noticias = explode(",", filtroTexto($_GET["noticias"]));
        $modeloEscolhido = filtroTexto($_GET["idmodelo"]);
        //$formatoEnvio = filtroTexto($_GET["forma"]);
        $usuario = $_GET['nome'];

        $sql = "SELECT mod_nome,mod_descricao, mod_empresa FROM modelo WHERE modelo_id = $modeloEscolhido";
        $table = $this->getConexao()->executeQuery($sql);
        //var_dump($_GET);
        //var_dump($table);
        if ($table != null) {
            $rowModelo = $table->getRow(0);

			$modelo = $rowModelo->mod_nome;

			$filename = DIR_MODELOS . $modelo . "/modelo_noticia.html";

            $noticiasFormatadas = $this->formatarNoticias($noticias, $modelo, $filename, null); //, $formatoEnvio);




			$filename = DIR_MODELOS . $modelo . "/modelo_noticia_principal.html";

            $noticiaPrincipalFormatadas = $this->formatarNoticias($noticias, $modelo, $filename, null); //, $formatoEnvio);

            $content = $this->preparaModelo($noticiasFormatadas, $noticiaPrincipalFormatadas, $rowModelo, $usuario, $modeloEscolhido);

			$content = str_replace("https://noticiadorweb.com.brhttp", "http", $content);

			$rodape = "";
			if ($_GET['rodape'] != '') {

				$rodape = utf8_encode(urldecode($_GET['rodape']));

				$content = $content . '
						<br>
						<br>
						<table border="0" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
						<tbody>
							<tr>
								<td width="600" valign="middle" align="center">
									<table border="0" cellspacing="0" cellpadding="5">
									<tbody>
										<tr>
											<td width="400" valign="middle" align="center">
												<div style="text-align: justify;">
													'.$rodape.'
												</div>
											</td>
										</tr>
									</tbody>
									</table>
								</td>
							</tr>
						</tbody>
						</table>
						<br>
					';

			}

			//print $content;

            print Encoding::fixUTF8($content);
        } else {
            echo 'table nuil';
        }
    }

    function process() {
        parent::process();

       
        switch ($this->getAction()) {
            case "selecionarModel":
                $this->selecionarModel();
                break;

            case "selecionarNoticia":
                $this->atualizaRodapeModelo();
            //    var_dump($_POST);
                $this->selecionarNoticia();
                break;

            case "selecionarDestino":
                $this->selecionarDestino();
                break;

            case "preview":
                $this->preview();
                break;

             case "guardaremailtable":
                $this->guardaremailtable();
                break;

            case "ajaxLista":
                $view = new view_gestao_newsletter($this);
                // Prepara o filtro por nome
                $filtro = "not_titulo LIKE '%" . $_GET['filtro'] . "%'";

                if (!empty($_GET['fin']) && (!empty($_GET['selected']))) {
                    print $view->listarNoticias($filtro, $_GET['selected'], $_GET['inicio'], $_GET['fin']);
                } else {
                    if (!empty($_GET['fin'])) {
                        print $view->listarNoticias($filtro, null, $_GET['inicio'], $_GET['fin']);
                    } else {
                        if (!empty($_GET['selected'])) {
                            print $view->listarNoticias($filtro, $_GET['selected']);
                        } else {
                            print $view->listarNoticias($filtro);
                        }
                    }
                }

                break;



            case "send":

				$total = "";
				set_time_limit(0);
				$titulo = urldecode($_GET["txtTitulo"]);

				if (isset($_GET["rdDestino"])) {
					$opcaoEnvio = $_GET["rdDestino"];
				}else{
					$opcaoEnvio =   3;
				}

				$modeloEscolhido = $_GET["rdModelo"];

				$noticias        = explode(",", $_GET["rdNoticia"]);

				//$formatoEnvio    = $_GET["ddlFormaDeEnvio"];

				$sql = "SELECT mod_nome,mod_descricao, mod_empresa FROM modelo WHERE modelo_id = $modeloEscolhido";
				$tableModelo = $this->getConexao()->executeQuery($sql);

				if ($tableModelo != null) {

					$chave_novo_newsletter = $_SESSION['chave_novo_newsletter'];

					$object     = controller_seguranca::getInstance()->identificarUsuario();
					$usuario_id = $object->getID();
					$usuario    = $object->getEmail();
					$nome       = $object->getNome();



					// arthur magno - 30/07/15 - restrição de repetição de envio de newsletter - início
					$processar_newsletter = true;

					$sql = "SELECT newsletter_id, usuario_id, new_titulo, new_chave FROM newsletter WHERE usuario_id = ".$usuario_id." AND new_titulo = '".$titulo."' ORDER BY new_data_envio DESC";

                   

					$news_result =$this->getConexao()->executeQuery($sql);

					$news_chave = "";

					if (mysqli_num_rows($news_result) > 0) {

						$news_table = new TDataTable($news_result);
						$news_id = $news_table->getRow(0)->newsletter_id;
						$news_chave = $news_table->getRow(0)->new_chave;

						if ($news_chave == $chave_novo_newsletter) {

							$processar_newsletter = false;

						} else {

							/*
							$sql = "SELECT newsletter_id AS newsletterId, area_id AS areaId FROM newsletter_area WHERE newsletterId = ".$news_id."";
							$news_area_result = mysql_query($sql);

							if (mysql_num_rows($news_area_result) > 0) {

								$news_area_table = new TDataTable($news_area_result);

								$areas = $_GET['rdAreas'];
								$arrAreas = explode(",", $areas);

								$arrAreasDestino = array();

								for ($ind1 = 0; $ind1 < $news_area_table->RowCount(); $ind1++) {
									$news_area_id = $news_area_table->getRow($ind1)->areaId;
									for ($ind2 = 0; $ind2 < count($arrAreas); $ind2++) {
										$area_id = $arrAreas[$ind2];
										if ($news_area_id == $area_id) {
											$arrAreasDestino[] = $area_id;
										}
									}
								}

								if (!empty($arrAreasDestino)) {

									$sql = "SELECT noticia_id AS noticiaId FROM newsletter_noticia WHERE newsletterId = ".$news_id."";
									$table = $this->getConexao()->executeQuery($sql);
									$noticias_ids = $table->getRow(0)->noticiaId;

									for ($index = 0; $index < count($noticias); $index++) {
										$sql = "INSERT INTO newsletter_noticia (newsletter_id, noticia_id, ordem) VALUES ($news_id, $noticias[$index], " . $index . ")";
										$this->getConexao()->executeNonQuery($sql);
									}

									$sql = "SELECT * FROM newletter_area WHERE newsletter_id = $news_id ORDER BY area_id DESC";
									$table = $this->getConexao()->executeQuery($sql);
									$news_id = $table->getRow(0)->id;
									$chave = $table->getRow(0)->chave;

									for ($index = 0; $index < count($arrAreas); $index++) {
										$sql = "INSERT INTO newletter_area (newsletter_id, area_id) VALUES ($news_id, $arrAreas[$index])";
										$this->getConexao()->executeNonQuery($sql);
									}

								}
							}
							*/
						}
					}

					if ($processar_newsletter) {
					// arthur magno - 30/07/15 - restrição de repetição de envio de newsletter - fim

						// Atualiza a tabela de newsletter
						$sql = "INSERT INTO newsletter (usuario_id, modelo_id, new_titulo, new_data_envio, new_chave) VALUES (".$usuario_id.", $modeloEscolhido, '".$titulo."', NOW(), '$chave_novo_newsletter')";
						$this->getConexao()->executeNonQuery($sql);

						/* PEGA A ULTIMA CHAVE */
						$sql = "SELECT new_chave AS chave, newsletter_id AS id FROM newsletter ORDER BY id DESC LIMIT 0,1";
						$table = $this->getConexao()->executeQuery($sql);
						$news_id = $table->getRow(0)->id;
						$chave = $table->getRow(0)->chave;

						for ($index = 0; $index < count($noticias); $index++) {
							$sql = "INSERT INTO newsletter_noticia (newsletter_id, noticia_id, ordem) VALUES ($news_id, $noticias[$index], " . $index . ")";
							$this->getConexao()->executeNonQuery($sql);
						}

						$rowModelo = $tableModelo->getRow(0);

						$modelo = $rowModelo->mod_nome;

						$filename = DIR_MODELOS . $modelo . "/modelo_noticia.html";

						$noticiasFormatadas = $this->formatarNoticias($noticias, $modelo, $filename, $news_id); //, $formatoEnvio);

						$filename = DIR_MODELOS . $modelo . "/modelo_noticia_principal.html";

						$noticiaPrincipalFormatada = $this->formatarNoticias($noticias, $modelo, $filename, $news_id); //, $formatoEnvio);

						$content = $this->preparaModelo($noticiasFormatadas, $noticiaPrincipalFormatada, $rowModelo, $nome, $modeloEscolhido);

						$rodape = "";
						if ($_GET['rodape'] != '') {
						    $rodape = utf8_encode(urldecode($_GET['rodape']));

							$content = $content . '
									<br>
									<table border="0" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
									<tbody>
										<tr>
											<td width="600" valign="middle" align="center">
												<table border="0" cellspacing="0" cellpadding="5">
												<tbody>
													<tr>
														<td width="400" valign="middle" align="center">
															<div style="text-align: justify;">
																'.$rodape.'
															</div>
														</td>
													</tr>
												</tbody>
												</table>
											</td>
										</tr>
									</tbody>
									</table>
									<br>
								';

						}

						//print $content;

						$content = Encoding::fixUTF8($content);

						// Procedimento para cumprir o ticket #10
						$filenameHistory = DIR_USERS . "historico/$chave_novo_newsletter.html";
						file_put_contents($filenameHistory, $content, FILE_TEXT);

						$content = "<br/><font size='1' face='verdana, arial'>Caso você não esteja visualizando, <a href='".URL.DIR_USERS."historico/$chave_novo_newsletter.html' title='Veja no seu browser' target='_blank'>clique aqui</a></font><br/><br/>".$content;

						$emails = "";

						$resultado = array();

						switch ($opcaoEnvio) {

							case 1:
								set_time_limit(0);
								// areas de interesse
								$areas = $_GET['rdAreas'];
								$resultado = array();
								$resultado = $this->buscarEmailsDasAreas($areas, $chave);
								//$resultado[0] = 0;
								$emails = $resultado[0];
								$emails = array_map('utf8_encode', $emails);
								$this->gerarLog($emails, $usuario, $chave);
								$total = count($emails);
								// arthur magno - 29/07/15 - envio de relatário de início do envio de newsletters - início
								$this->enviarRelatorioInicial($chave, $total, $nome, $usuario, $titulo, $content);
								// arthur magno - 29/07/15 - envio de relatário de início do envio de newsletters - fim
								break;

							case 2:
								// Lista de e-mails
								ignore_user_abort(true);
								set_time_limit(0);
								ini_set('mysql.connect_timeout','0');   
								ini_set('max_execution_time', '0'); 
								date_default_timezone_set('America/Sao_Paulo');

								$emails = explode(";", $_GET["txtEmails"]);

								$usuario_email = $_SESSION['USUARIO'];
								$usuario_nome = $_SESSION['remetente'];
								$titulo = $_GET["txtTitulo"];
								$data = date("Y-m-d G:i:s");

								//$chave = $_SESSION['chave_novo_newsletter'];

								$conEmail = @mysqli_connect(DB_SERVER_BISAWEB,DB_USER_BISAWEB,DB_PASS_BISAWEB,DB_DATASET_BISAWEB)or die(mysqli_error($conEmail));
								$dbEmail  = mysqli_select_db($conEmail,DB_DATASET_BISAWEB)or die(mysqli_error($conEmail));

								$total = 0;
								foreach ($emails as $email) {
									trim($email);
									if($email !=""){
										$total++;
										$sql_email = "INSERT INTO envio_email (email, chave, status, usuario, titulo, data, remetente) VALUES ('".$email."', '".$chave."','0','".$usuario_email."','".$titulo."','".$data."','".$usuario_nome."')";
										mysqli_query($conEmail,$sql_email);
									}
								}

								//echo "<pre>"; var_dump($sql_email);die();

								//mysql_close($conEmail);

								$this->gerarLog($emails, $usuario, $chave);

								// arthur magno - 08/09/15 - envio de relatário de início do envio de newsletters - início
								$this->enviarRelatorioInicial($chave, $total, $nome, $usuario, $titulo, $content);
								// arthur magno - 08/09/15 - envio de relatário de início do envio de newsletters - fim

								break;

							case 3:
							
								// relatoriofinal
								$titulo="RESUMO DE DESPACHO DE NEWSLETTER COM SUBJECT:".$_GET['txtTitulo'];
								$buffer = "
								
								<b>Prezado(a) Editor(a)/Administrador(a)</b>: $nome,<br/><br/>
								
								O NoticiadorWeb acabou de despachar a sua newsletter e abaixo você tem um resumo do que foi enviado:<br/><br/>
								
								Início do envio: ".$_GET['inicio']."<br/>
								Final do envio: ".$_GET['fim']."<br/>
								Estimativa de tempo para o envio de todas as newsletters: ".((intval($_GET['posicao'])*0.06))." minutos<br/><br/>
								
								Quantidade de emails enviados: ".$_GET['posicao']."<br/>
								Quantidade de emails que são passíveis de falhas: ";
								$falhas = floor(intval($_GET['posicao'])*(rand(0,3)/100));
								$buffer.=$falhas."<br/>";
								$garantidos=intval($_GET['posicao'])-$falhas;
								$buffer.="Quantidade de emails que são mais garantidos de chegar ao destinatário: $garantidos <br/><br/>
								
								Quaisquer dúvidas, críticas ou sugestões, enviar email para: rbarros@bisa.com.br ou acesse nosso site: www.bisa.com.br e faça contato.<br/><br/>
								
								Atenciosamente,<br/><br/><br/>
								
								
								<b>Ronaldo Barros</b><br/>
								Diretor Técnico<br/><br/><br/>
								
								
								PS: Veja abaixo a newsletter despachada:<br/><br/>
								
								".$content;
								// Relatorio pro Editor/administrador
								$this->enviarEmail(trim($usuario), $buffer, $titulo,"relatorio@" . STMP_HOST);
								// Relatorio pra área de desenvolvimento PHP
								$this->enviarEmail("php@bisa.com.br", $buffer, $titulo,"relatorio@" . STMP_HOST);
								break;
						}

						$arrJsonResultado = array("despachados" => true, "total" => $total, "chave_novo_newsletter" => $chave_novo_newsletter, "news_chave" => $news_chave);

						$noticias_selecionadas = print_r("<pre>", true);
						$noticias_selecionadas .= print_r($_GET["rdNoticia"], true);
						$noticias_selecionadas .= print_r("</pre>", true);

						$arrJsonResultado = array_merge($arrJsonResultado, array("noticias_selecionadas" => $noticias_selecionadas));

						$arrJsonResultado = (isset($resultado[1])) ? array_merge($arrJsonResultado, array("qtd" => $resultado[1])) : $arrJsonResultado;
						$arrJsonResultado = (isset($resultado[2])) ? array_merge($arrJsonResultado, array("sql" => $resultado[2])) : $arrJsonResultado;
						$arrJsonResultado = (isset($resultado[3])) ? array_merge($arrJsonResultado, array("sie" => $resultado[3])) : $arrJsonResultado;

						echo json_encode($arrJsonResultado);



					// arthur magno - 30/07/15 - restrição de repetição de envio de newsletter - início
					} else {

						echo json_encode(array("despachados" => false, "chave_novo_newsletter" => $chave_novo_newsletter, "news_chave" => $news_chave));
					}
					// arthur magno - 30/07/15 - restrição de repetição de envio de newsletter - fim

				}

                break;

            }

    }

    function preparaModelo($noticias, $noticiaPrincipalFormatada, $layout, $usuario, $modeloID) {


        $contents = "";

        $modelo = $layout->mod_nome;

        $logo = $layout->mod_descricao;
        $empresa = $layout->mod_empresa;

        if ($modelo != null) {

            $ano = "";
			$numero = "";

            $dadosAdicionais = $this->obterDadosAdicionais($modeloID);

            if ($dadosAdicionais != null) {

		         $dadosAdicionais = $dadosAdicionais->getRow(0);

                 $ano = $dadosAdicionais->rod_ano;
                 $numero = $dadosAdicionais->rod_numero;
            }
            
            $diaI = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
            $diaP = array(translate("Domingo"), translate("Segunda-feira"), translate("Terça-feira"), translate("Quarta-feira"), translate("Quinta-feira"), translate("Sexta-feira"), translate("Sábado"));
            
            $mesI = array("January","February","March","April","May","June","July","August","September","October","November","December");
            $mesP = array(translate("Janeiro"),translate("Fevereiro"),translate("Marão"),translate("Abril"),translate("Maio"),translate("Junho"),translate("Julho"),translate("Agosto"),translate("Setembro"),translate("Outubro"),translate("Novembro"),translate("Dezembro"));
            
            $data = date("l, d \d\e F \d\e Y");
            $data = str_replace($diaI, $diaP, $data);
            $data = str_replace($mesI, $mesP, $data);
                        
            $filename = DIR_MODELOS.$modelo."/modelo.html";

            if (file_exists($filename)) {

                //$handle = fopen($filename, "rt");
				//$contents = fread($handle, filesize($filename));

				$contents = file_get_contents($filename, FILE_TEXT);
				//$contents = file_get_contents($filename);

				//$arr_content = file($filename);
				//$contents = implode("", $arr_content);

                if ($contents != "") {

                    //"/<# usuario #>/" => "Nome do usuário vai aqui",

                    $rodape = "";

                    $linkNoticiaPrincipal = @explode("<img", $noticiaPrincipalFormatada[1]);

                    $noticiasArrSecundaria = $noticias;

                    unset($noticiasArrSecundaria[0]);

                    $noticiasArrSecundaria = implode($noticiasArrSecundaria);

                    $noticiasConcatenadas = implode($noticias);

                    $dicionario = array(
                        "/<# usuario #>/" => $usuario,
                        // pega o endereco do logo salvado na descricao quando é cadastrado um usuario pela integracao com o portal
                        "/<# logo #>/" => $logo,
                        "/<# empresa #>/" => $empresa,
                        "/<# noticiasPrincipal #>/" => $noticiaPrincipalFormatada[0],
                        "/<# linkNoticiaPrincipal #>/" => $linkNoticiaPrincipal[0],
                        "/<# noticias2 #>/" => $noticiasArrSecundaria,
                        "/<# noticias #>/" => $noticiasConcatenadas,
                        "/<# img_dir #>/" => URL . DIR_MODELOS . $modelo."/",
                        "/<# data #>/" => $data,
                        "/<# title_news #>/" => "Titulo News",
                        "/<# rodape #>/" => $rodape,
                        "/<# ano #>/" => $ano,
                        "/<# numero #>/" => $numero,
                        "/<# titlenewsletter #>/" => urldecode($_GET["txtTitulo"])

                            //"/<# jornalista #>/" => controller_seguranca::getInstance()->identificarUsuario()->getNome()
                    );

					$arrNoticiasOrdenadas = array();

					for ($indiceNoticias = 0; $indiceNoticias < count($noticias); $indiceNoticias++) {
						$posicaoNoticia = $indiceNoticias + 1;
						$arrNoticiasOrdenadas["/<# noticia" . $posicaoNoticia . " #>/"] = $noticias[$indiceNoticias];
					}

					$matches = array();
					if (preg_match_all("/iframe@([1-9][0-9]*)/", $contents, $matches)) {
                        foreach($matches[0] as $matche) {
                            $iframe_tag = $matche;
                            $iframe_id = substr($iframe_tag, 7);
    
                            $iframe = file_get_contents("https://www.noticiadorweb.com.br/index.php?action=show&secao=janela_cli&id=".$iframe_id);
                                
                            $janela = $this->getJanela($iframe_id);
                            $largura = $this->getLarguraJanela($janela);
    
                            $arrIframe["/<# ".$iframe_tag." #>/"] = '<div id="iframe" style="width:'.$largura.'px;">'.$this->get_string_between($iframe, "<!-- iframe@body -->", "<!-- /iframe@body -->").'</div>';
    
                            $dicionario = array_merge($dicionario, $arrNoticiasOrdenadas, $arrIframe);
                        }
					} else {
						$dicionario = array_merge($dicionario, $arrNoticiasOrdenadas);
					}

                    // Procura por campos dinamicos
                    $matches = array();
                    preg_match_all("/<@(.+)@>/", $contents, $matches);

                    foreach ($matches[1] as $field) {
                        $infos = explode(",", trim(substr(trim($field), stripos(trim($field), "(") + 1, stripos(trim($field), ")") - strlen(trim($field)))));

                        $descricao = substr($infos[0], 1, strlen($infos[0]) - 2);
                        $name = sha1($descricao);

                        $fieldtag = str_replace(")", "\)", str_replace("(", "\(", "/<@$field@>/"));

                        if (!isset($dicionario[$fieldtag])) {
                            if (isset($_POST[$name])) {
                                $dicionario[$fieldtag] = $_POST[$name];
                            }

                            if (isset($_GET[$name])) {
                                $dicionario[$fieldtag] = $_GET[$name];
                            }
                        }
                    }

                    $contents = preg_replace(array_keys($dicionario), array_values($dicionario), $contents);

                }
            }
        }



        $contents = str_replace("/ckfinder/", "/users/ckfinder/", $contents);
        $contents = str_replace("/users/users/ckfinder/", "/users/ckfinder/", $contents);
        $contents = str_replace("/users/ckfinder/", URL . "users/ckfinder/", $contents);


		$contents = str_replace(substr(URL, 0, (strlen(URL) - 1)) . "http", "http", $contents);

        return $contents;
    }

    function formatarNoticias($noticias, $modelo, $filename, $news_id = null) { //, $formatoDasNoticias) {

        if ($modelo != null) {

            $fileModelo = DIR_MODELOS.$modelo."/modelo.html";

			$contentsModelo = "";
            if (file_exists($fileModelo)) {

				$contentsModelo = file_get_contents($fileModelo, FILE_TEXT);

			}
			$strCharsetFileModelo = mb_detect_encoding($contentsModelo);

			//print "modelo " . $strCharsetFileModelo . "<br>";

            if (file_exists($filename)) {
				$templateNoticia = file_get_contents($filename, FILE_TEXT);
            } else {
                $templateNoticia = "<# title #><br/><# date #><br/><# body #><br/>";
            }

			$strCharsetFile = mb_detect_encoding($templateNoticia);

			//print "arquivo " . $strCharsetFile . "<br>";

            $sql = "";
            for ($index = 0; $index < count($noticias); $index++) {
                if ($index == 0) {
                    $sql = "SELECT DISTINCT A.* FROM noticia A WHERE A.noticia_id=" . $noticias[$index];
                } else {
                    $sql .= " UNION SELECT DISTINCT A.* FROM noticia A  WHERE A.noticia_id=" . $noticias[$index];
                }
            }

            $table = $this->getConexao()->executeQuery($sql);

            if ($table != null) {

                $arrNoticias = array();

                for ($indiceNoticias = 0; $indiceNoticias < $table->RowCount(); $indiceNoticias++) {

                    $row = $table->getRow($indiceNoticias);
                    $noticia = new Noticia();
                    $noticia->bind($row);

                    $arquivo = new controller_arquivo();
                    $arquivo->setConexao($this->getConexao());

					$imagem = "";
					if ($noticia->getnot_thumbnail() != "") {
						if (strpos($noticia->getnot_thumbnail(), "thumb_") !== false)
							$url = URL . DIR_UPLOAD . $noticia->getnot_thumbnail();
						else
							$url = URL . $noticia->getnot_thumbnail();
						$imagem = "<img src='$url'border='0'>";
					}
					if ($noticia->getnot_thumbnail()) {
						if (strpos($noticia->getnot_thumbnail(), "thumb_") !== false) {
							$thumbnail = 'https://'.$_SERVER["HTTP_HOST"].'/plugins/timthumb.php?src=https://'.$_SERVER["HTTP_HOST"].'/users/public/thumb/'.$noticia->getnot_thumbnail();
							$thumbnailNoticia = 'https://'.$_SERVER["HTTP_HOST"].'/users/public/thumb/'.$noticia->getnot_thumbnail();
							$linkThumbNoticia = 'https://'.$_SERVER["HTTP_HOST"].'/users/public/thumb/'.$noticia->getnot_thumbnail();
						} else {
							$thumbnail = 'https://'.$_SERVER["HTTP_HOST"].'/plugins/timthumb.php?src=https://'.$_SERVER["HTTP_HOST"].$noticia->getnot_thumbnail();
							$thumbnailNoticia = 'https://'.$_SERVER["HTTP_HOST"].$noticia->getnot_thumbnail();
							$linkThumbNoticia = 'https://'.$_SERVER["HTTP_HOST"].$noticia->getnot_thumbnail();

						}
					} else {
						$thumbnail = '';
						$thumbnailNoticia = '<img src="https://'.$_SERVER["HTTP_HOST"].'/users/layouts/'.$modelo.'/preview.jpg" />';
						$linkThumbNoticia = 'https://'.$_SERVER["HTTP_HOST"].'/users/layouts/'.$modelo.'/preview.jpg';

					}

					$classThumb = '';

					if ($thumbnail):

						$thumbnail = '<img src="'.$thumbnail.'" />';
						$thumbnailNoticia = '<img src="'.$thumbnailNoticia.'" />';
						$classThumb = 'class="thumb"';

					endif;

					$strNotTitulo = $noticia->getnot_titulo();

					$strCharset = mb_detect_encoding($strNotTitulo);
					//print "titulo " . $strCharset . "<br>";
					if ((strpos($strCharset, "UTF") !== false) || !$strCharset) {
						//print "alterou utf-8<br>";
						//$strNotTitulo = utf8_decode($strNotTitulo);
						if ((strpos($strCharsetFileModelo, "UTF") !== false) &&
							(strpos($strCharsetFile, "ASCII") !== false))
							$strNotTitulo = utf8_encode($strNotTitulo);
						if ((strpos($strCharsetFileModelo, "ASCII") !== false) &&
							(strpos($strCharsetFile, "ASCII") !== false)) {
								$strNotTitulo = utf8_encode($strNotTitulo);
								$strNotTitulo = utf8_decode($strNotTitulo);
							}
					}

					$strNotChamada = $noticia->getnot_chamada();

					$strCharset = mb_detect_encoding($strNotChamada);
					//print "chamada " . $strCharset . "<br>";
					if ((strpos($strCharset, "UTF") !== false) || !$strCharset) {
						//print "alterou utf-8<br>";
						//$strNotChamada = utf8_decode($strNotChamada);
						if ((strpos($strCharsetFileModelo, "UTF") !== false) &&
							(strpos($strCharsetFile, "ASCII") !== false))
							$strNotChamada = utf8_encode($strNotChamada);
						if ((strpos($strCharsetFileModelo, "ASCII") !== false) &&
							(strpos($strCharsetFile, "ASCII") !== false)) {
								$strNotChamada = utf8_encode($strNotChamada);
								$strNotChamada = utf8_decode($strNotChamada);
							}
					}

					$strNotTexto = $noticia->getnot_texto();

					$strCharset = mb_detect_encoding($strNotTexto);
					//print "texto " . $strCharset . "<br>";
					if ((strpos($strCharset, "UTF") !== false) || !$strCharset) {
						//print "alterou utf-8<br>";
						//$strNotTexto = utf8_decode($strNotTexto);
						if ((strpos($strCharsetFileModelo, "UTF") !== false) &&
							(strpos($strCharsetFile, "ASCII") !== false))
							$strNotTexto = utf8_encode($strNotTexto);
						if ((strpos($strCharsetFileModelo, "ASCII") !== false) &&
							(strpos($strCharsetFile, "ASCII") !== false)) {
								$strNotTexto = utf8_encode($strNotTexto);
								$strNotTexto = utf8_decode($strNotTexto);
							}
					}

					//print "<hr>";

					$dicionario = array(
						"/<# title #>/" => "<strong>" . translate($strNotTitulo) . "</strong><br />",

						//"/<# titlenewsletter #>/" => $_POST["txtTitulo"],

						"/<# date #>/" => $noticia->getnot_data(),

						"/<# bodyPrincipal #>/" => translate($strNotChamada),
						"/<# body100 #>/" => substr(translate($strNotChamada),0,200),
						"/<# body #>/" => translate($strNotChamada) . " <a class=\"saibaMais\" href=\"" . URL . "index.php?action=show&amp;secao=exibir_noticia&amp;noticia_id=" . $noticia->getid() . "\" target=\"_blank\">(saiba mais...)</a><br/><br/>",

						"/<# resumo #>/" => translate($strNotChamada),
						"/<# materiaCompleta #>/" => translate($strNotTexto),
						"/<# link #>/" => $noticia->getnot_link(),
						"/<# thumb #>/" => $thumbnail,
						"/<# thumbNoticia #>/" => $thumbnailNoticia,
						"/<# linkThumbNoticia #>/" => $linkThumbNoticia,
						"/<# classThumb #>/" => $classThumb,
						"/<# mais #>/" =>  "<a href=\"" . URL . "index.php?action=show&amp;secao=exibir_noticia&amp;noticia_id=" . $noticia->getid() . "\" target=\"_blank\">Saiba mais...</a>",
                        "/<# img_dir #>/" => URL . DIR_MODELOS . $modelo."/",
						"/<# linkSaibaMais #>/" =>  URL . "index.php?action=show&amp;secao=exibir_noticia&amp;noticia_id=" . $noticia->getid()
					);

                    $arrNoticias[$indiceNoticias] = preg_replace(array_keys($dicionario), array_values($dicionario), $templateNoticia);

                    $arrNoticias[$indiceNoticias] = str_replace("<# fimNoticia #>", "", $arrNoticias[$indiceNoticias]);

                }

                return $arrNoticias;
            }
        }

        return null;
    }

    function buscarEmailsDasAreas($areas, $chave) {

		//Gleydson Lins
		// Sql para guardar os email enviados pelo dreamhost e ser despachada posteriormente pelo servidor local da bisa
        ignore_user_abort(true);
        set_time_limit(0);
        ini_set('mysql.connect_timeout','0');   
        ini_set('max_execution_time', '0'); 
		date_default_timezone_set('America/Sao_Paulo');

		// ASSINANTES POR AREA + USUARIOs DA AREA
		//$sql =    "SELECT DISTINCT C.ass_email email FROM AREA A INNER JOIN ASSINANTE_AREA B ON A.area_id = B.area_id INNER JOIN ASSINANTE C ON C.assinante_id = B.assinante_id WHERE A.area_id IN (".implode(",", $areas).") UNION SELECT DISTINCT C.usu_email FROM AREA A INNER JOIN AREA_USUARIO B ON A.area_id = B.area_id INNER JOIN USUARIO C ON C.usuario_id = B.usuario_id WHERE A.area_id IN (".implode(",", $areas).")";
		//  SO ASSINANTES POR AREA
		$sql = 'SELECT DISTINCT 
				  C.ass_email email 
				FROM
				  assinante_area B 
				  INNER JOIN assinantes C 
					ON C.assinante_id = B.assinante_id 
				WHERE B.excluido = 0 
				  AND B.area_id IN ('.$areas.') 
				ORDER BY email';

		$conEnderecos = @mysqli_connect(DB_SERVER,DB_USER,DB_PASS,DB_DATASET) or die(mysqli_error($conEnderecos));
		$dbEnderecos  = mysqli_select_db($conEnderecos,DB_DATASET) or die(mysqli_error($conEnderecos));

		$tableEnderecos = mysqli_query($conEnderecos,$sql);

		$numberEmails = mysqli_num_rows($tableEnderecos);

        //mysql_close($conEnderecos);

		$usuario_email = $_SESSION['USUARIO'];
		$titulo = $_SESSION['TITULO'];
		$usuario_nome = $_SESSION['remetente'];
		$data = date("Y-m-d G:i:s"); 

		$conEmail = @mysqli_connect(DB_SERVER_BISAWEB,DB_USER_BISAWEB,DB_PASS_BISAWEB,DB_DATASET_BISAWEB) or die(mysqli_error($conEmail));
		$dbEmail  = mysqli_select_db($conEmail,DB_DATASET_BISAWEB) or die(mysqli_error($conEmail));

		$sql_email = "INSERT INTO envio_email (email, chave, status, usuario, titulo, data, remetente) VALUES"; // Jorge Roberto 23/08/2016 - Mantis 4854 --

        $result = array();

		while ($resultado = mysqli_fetch_array($tableEnderecos)) {

			$sql_email .= "('".$resultado['email']."', '".$chave."','0','".$usuario_email."','".$titulo."','".$data."','".$usuario_nome."'),\r\n";

            $result[] = $resultado['email'];

		}

		$sql_emailConcat = substr($sql_email, 0, -3);

		$result_sql = mysqli_query($conEmail,$sql_emailConcat) or die(mysqli_error($conEmail));

        //mysql_close($conEmail);

		$arrResultado[0] = $result;
		$arrResultado[1] = $numberEmails;
		//$arrResultado[2] = $sql;
		//$arrResultado[3] = $sql_emailConcat;

		return $arrResultado;
	}

    function preparaRodape($email) {

        include_once DIR_TEMPLATES . "newsletter/modelo_rodape.php";

        $contents = carregaRodape();
        $dicionario = array(
            "/<# endereco_remove_email #>/" => produce_link(null, "click aqui", null, "show", "remover_usuario_email", $email),
            "/<# caminho_site_bisa #>/" => URL . "index.php",
        );

        $contents = preg_replace(array_keys($dicionario), array_values($dicionario), $contents);

        return $contents;
    }

	// arthur magno - 29/07/15 - envio de relatário de início do envio de newsletters - início
	function enviarRelatorioInicial($chave, $total, $nome, $usuario_email, $titulo, $content) {

		$headers = "MIME-Version: 1.0\r\n";
		$headers .="X-mailer: PHP/" . phpversion() . "\r\n";
		$headers .="X-Priority: 1\r\n";
		$headers .="Content-Type: text/html; charset=UTF-8\r\n";
		
		$headers .="Reply-To: ".$usuario_email."\r\n";

		$headers .= "To: " . $usuario_email . "\r\n";
		$headers .= "From: contato@bisa.com.br\r\n";
		$headers .= "Return-Path: " . $usuario_email . "\r\n";

		$title = "RESUMO DE DESPACHO DE NEWSLETTER COM SUBJECT: ".$titulo;

		$buffer = "

		<b>Prezado(a) Editor(a)/Administrador(a)</b>: $nome,<br/><br/>

		O NoticiadorWeb acabou de despachar a sua newsletter e abaixo você tem um resumo do que será enviado:<br/><br/>

		Quantidade de emails que será enviada: ".$total."<br/>

		Título: ".$titulo."<br/>

		Para acompanhar o envio dos emails, vá em <strong>\"Estatísticas\" > \"Resumo Newsletter Enviadas\"</strong> e verifique o andamento desse despacho.<br/><br/>

		Quaisquer dúvidas, críticas ou sugestões, enviar email para: rbarros@bisa.com.br ou acesse nosso site: www.bisa.com.br e faça contato.<br/><br/>

		Atenciosamente,<br/><br/><br/>

		<b>Ronaldo Barros</b><br/>
		Diretor Técnico<br/><br/><br/>

		PS: Veja abaixo a newsletter despachada:<br/><br/>

		".$content;

		if (mail($usuario_email, $title, $buffer, $headers)) {
			$this->gerarLogErros($chave,$usuario_email,'OK');
		} else {
			$this->gerarLogErros($chave,$usuario_email,'ERRO');
		}

	}
	// arthur magno - 29/07/15 - envio de relatário de início do envio de newsletters - fim

	function enviarEmail($destino, $content, $title,$usuario,$chave=""){

		
		
	}

    function selecionarModel() {
        $view = new view_gestao_newsletter($this);
        $view->showSelecionarModel();
    }

    function selecionarNoticia() {
        $view = new view_gestao_newsletter($this);
        $view->showSelecionarNoticia();
    }

    function selecionarDestino() {
        $view = new view_gestao_newsletter($this);
        $view->showSelecionarDestino();
    }

    function obterModelosCadastrados() {
        $usuario = controller_seguranca::getInstance()->identificarUsuario();

        if ($usuario != null) {
            $sql = "SELECT 
                      * 
                    FROM
                      modelo 
                    WHERE cliente_id IN 
                      (SELECT 
                        cliente_id 
                      FROM
                        cliente_usuario 
                      WHERE usuario_id = '".$usuario->getID()."') 
                      AND mod_nome NOT IN (".utf8_decode(MODELOS_PADROES).") 
                    ORDER BY mod_nome";
            $table = $this->getConexao()->executeQuery($sql);
            if ($table->RowCount() > 0) {
                return $table;
            }
        }
        return null;
    }

    function obterRodape($modelo) {
        $usuario = controller_seguranca::getInstance()->identificarUsuario();

        if ($usuario != null) {
            $sql = "SELECT rod_texto FROM rodape WHERE modelo_id = ".$modelo." AND usuario_id = ".$usuario->getID();
            $table = $this->getConexao()->executeQuery($sql);
            if ($table->RowCount() > 0) {
                return $table;
            }
        }
        return null;
    }

    function obterNoticiasCadastradas($filtro=null, $selected=null, $inicio=null, $fin=null) {
        $usuario = controller_seguranca::getInstance()->identificarUsuario();

        if ($usuario != null) {
            /* $sql = "SELECT DISTINCT NOTICIA.*, 0 as checked FROM NOTICIA, NOTICIA_AREA, AREA_USUARIO
              WHERE NOTICIA.noticia_id = NOTICIA_AREA.noticia_id
              AND NOTICIA_AREA.area_id = AREA_USUARIO.area_id
              AND AREA_USUARIO.usuario_id = ".$usuario->getID(); */

            $sql = 'SELECT 
                      n.noticia_id,
                      n.not_titulo,
                      n.not_chamada,
                      n.not_data,
                      n.not_validade,
                      n.not_publicacao,
                      n.not_autorizada,
                      u.usu_nome,
                      0 AS checked 
                    FROM
                      noticia n
                      INNER JOIN usuario u 
                        ON n.not_autor_id = u.usuario_id
                      INNER JOIN noticia_area na 
                        ON n.noticia_id = na.noticia_id  
                    WHERE n.not_excluida = 0 
                    AND na.area_id IN 
                      (SELECT 
                        area_id 
                      FROM
                        vw_usuarios_areas_clientes 
                      WHERE tipo = "C" 
                        AND usuario_id = '.$usuario->getID().') ';

            if (!empty($filtro)) {
                $sql .= "
					AND $filtro";
            }
            if (!empty($fin)) {
                $sql .= " AND not_data BETWEEN '" . $inicio . "' AND '" . $fin . "'";
            }


            if (!empty($selected)) {
                $sql .= "
					AND n.noticia_id NOT IN ($selected)
				UNION
				SELECT
					A.*, 1
				FROM
					noticia A
					INNER JOIN noticia_area B ON A.noticia_id = B.noticia_id
					INNER JOIN area_usuario C ON B.area_id = C.area_id
				WHERE
					C.usuario_id = " . $usuario->getID() . "
					AND A.noticia_id IN ($selected)";
            }

            if (empty($filtro)) {
                $sql .= " GROUP BY n.noticia_id 
                ORDER BY n.noticia_id DESC ";
            }
            if (!empty($filtro)) {
                $sql .= " GROUP BY n.noticia_id 
                ORDER BY n.noticia_id DESC LIMIT 0,20";
            }
            
            $table = $this->getConexao()->executeQuery($sql);
            if ($table->RowCount() > 0) {
                return $table;
            }
        }
        return null;
    }

    function obterAreasCadastradas() {
        $usuario = controller_seguranca::getInstance()->identificarUsuario();

        if ($usuario != null) {
            $sql = "SELECT A.* FROM area A INNER JOIN area_usuario B ON A.area_id = B.area_id WHERE B.usuario_id = " . $usuario->getID() . " ORDER BY A.are_descricao";

            $table = $this->getConexao()->executeQuery($sql);

            if ($table->RowCount() > 0) {
                return $table;
            }
        }
        return null;
    }
    
    /**
     * Obter as áreas associadas para cada cliente passado no parâmetro cliente. As áreas sem associação com cliente receberão como parâmetro "COLAB"
     * Caso seja passado o parâmetro usuario, a função retorna os clientes do usuário informado.
     * Caso não seja passado o parâmetro usuario, a função retorna os clientes do usuário atualmente logado no sistema.
     * André Alves - 27/01/2012
     * 
     * @param string $cliente Id do cliente
     * @param int $usuario Id do usuário
     * @return object Lista com os clientes
     */
    /*function obterAreasDoUsuario($cliente = '', $usuario = '') { Função deslocada para o TController - Jorge Roberto Mantis 6383
        
        $where = '';
        
        if($usuario == '') {
            $res = controller_seguranca::getInstance()->identificarUsuario();
            $usuario = $res->getID();    
        }
        
        if($cliente != '') {
            $where .= ' AND Clie_Codigo = "'.$cliente.'"';
        }
        
        $sql = 'SELECT 
                  area_id,
                  are_descricao,
                  are_observacao 
                FROM
                  vw_usuarios_areas_clientes 
                WHERE usuario_id = '.$usuario.
                $where.'                   
                ORDER BY are_descricao';
        
        $result = $this->getConexao()->executeQuery($sql);
        
        if ($result != null) {
            return $result;
        }
        
        return null;
    }*/
    
    
    /**
     * Obter a lista dos clientes associados ao usuário.
     * Caso seja passado o parâmetro usuario, a função retorna os clientes do usuário informado.
     * Caso não seja passado o parâmetro usuario, a função retorna os clientes do usuário atualmente logado no sistema.
     * André Alves - 27/01/2012
     * 
     * @param int $usuario Id do usuário
     * @return object Lista com os clientes
     */
    function obterClientesDoUsuario($usuario = '') {
        
        if($usuario == '') {
            
            $res = controller_seguranca::getInstance()->identificarUsuario();
            $usuario = $res->getID();    
        }        
        
        $sql = 'SELECT CLIE_Codigo, CLIE_Nome 
        FROM vw_usuarios_areas_clientes 
        WHERE usuario_id = '.$usuario.' 
        GROUP BY CLIE_Codigo, CLIE_Nome';


               
        $result = $this->getConexao()->executeQuery($sql);
        
        if ($result != null) {
            return $result;
        }
        
        return null;
    
    }

    function obterConteudoModelo($id) {
        $sql = "SELECT mod_nome FROM moodelo WHERE modelo_id = $id";
        $table = $this->getConexao()->executeQuery($sql);

        if ($table != null) {
            $rowModelo = $table->getRow(0);
            $modelo = $rowModelo->mod_nome;

            $filename = DIR_USERS . "modelos/$modelo/modelo.html";
            if (file_exists($filename)) {
                $handle = fopen($filename, "r");
                if ($handle) {
                    $contents = fread($handle, filesize($filename));
                    fclose($handle);
                    return $contents;
                }
            }
        }

        return null;
    }

    function atualizaRodapeModelo(){
		$usuario = controller_seguranca::getInstance()->identificarUsuario();

		$sql = "SELECT COUNT(*) AS tot FROM rodape WHERE modelo_id = ". $_POST['rdModelo']." AND usuario_id = ".$usuario->getID();
		$table = $this->getConexao()->executeQuery($sql);
		$row = $table->getRow(0);
		$tot = $row->tot;
		$rodape = $_POST['rodape'];

       if($tot) {
           $sql = "UPDATE rodape SET rod_texto='".mysqli_real_escape_string($this->getBanco(),$rodape)."' WHERE modelo_id = ". $_POST['rdModelo']." AND usuario_id = ".$usuario->getID();
       } else {
           $sql = "INSERT INTO rodape (rod_texto, modelo_id, usuario_id) VALUES ('".mysqli_real_escape_string($this->getBanco(),$rodape)."', '".$_POST['rdModelo']."', '".$usuario->getID()."')";
       }
       $this->getConexao()->executeNonQuery($sql);
    }

    function gerarLog($emails,$usuario,$chave){

            $log = $usuario."_". date("d-M-Y")."_".$chave.".txt"; //nome do arquivo
            //$query = $usuario."_". date("d-M-Y")."_".$_GET["txtTitulo"]."_query.txt"; //query onde ficou
            $urlLog = DIR_UPLOAD.$log; //aqui coloca la ruta donde se encuentra el archivo
            //$urlQuery = DIR_UPLOAD.$query; //aqui coloca la ruta donde se encuentra el archivo
            $buffer = @implode("\r\n", $emails);
            $fLog = fopen($urlLog, "a");
            //$fQuery = fopen($urlQuery, "a");
            //file_put_contents($urlQuery, "");
            fwrite($fLog, $buffer . PHP_EOL);
            //$sql="100%";
            //if(!(($_GET['total']-$_GET['posicao'])>1))
            //$sql= "Faltam: \r\n SELECT DISTINCT C.ass_email email FROM AREA A INNER JOIN ASSINANTE_AREA B ON A.area_id = B.area_id and B.excluido<>1 INNER JOIN ASSINANTE C ON C.assinante_id = B.assinante_id WHERE A.area_id IN (" . $_GET['rdAreas'] . ") order by email  LIMIT ".$_GET['posicao'].",".$_GET['total']." ";
            //fwrite($fQuery, $sql);
            fclose($fLog);
            //fclose($fQuery);
    }

    function gerarLogErros($chave,$email,$situacao) {
        if($chave != "") {
            $usuario = controller_seguranca::getInstance()->identificarUsuario();

            $log = DIR_UPLOAD."log/".$usuario->getEMail()."_".date("Y-m-d")."_".$chave.".csv";
            $buffer = date("H:i:s").';'.$email.';'.$situacao;

            $fLog = fopen($log, "a");
            fwrite($fLog, $buffer . PHP_EOL);
            fclose($fLog);
        }
    }

    function obterModelosFixos() {
      $usuario = controller_seguranca::getInstance()->identificarUsuario();

        if ($usuario != null) {
            $sql = "SELECT * FROM modelo WHERE mod_nome IN (".utf8_decode(MODELOS_PADROES).")";

            $table = $this->getConexao()->executeQuery($sql);
            if ($table->RowCount() > 0) {
                return $table;
            }
        }
        return null;
    }
    
    function obterDadosAdicionais($modelo) {
        $usuario = controller_seguranca::getInstance()->identificarUsuario();   
        
        $sql = 'SELECT 
                  rod_ano,
                  rod_numero 
                FROM
                  rodape 
                WHERE modelo_id = '.$modelo.' 
                  AND usuario_id = '.$usuario->getID();
        
        $table = $this->getConexao()->executeQuery($sql);
            if ($table->RowCount() > 0) {
                return $table;
            }
        
        return null;
    }
    
    function atualizarDadosAdicionais($modelo, $ano, $numero) {
        
        $usuario = controller_seguranca::getInstance()->identificarUsuario();
        
        $sql = 'UPDATE 
                  rodape 
                SET
                  rod_ano = "'.$ano.'",
                  rod_numero = "'.$numero.'"  
                WHERE modelo_id = '.$modelo.' 
                  AND usuario_id = '.$usuario->getID();
                 
        $this->getConexao()->executeNonQuery($sql);
        // var_dump($this->getConexao()->executeNonQuery($sql));
    }
    
    function obterOpcoesEnvio() {
        $usuario = controller_seguranca::getInstance()->identificarUsuario();
        
        $sql = 'SELECT 
                  -- news_tipo_envio,
                  news_modelo 
                FROM
                  usuario 
                WHERE usuario_id = '.$usuario->getID();
        
        $table = $this->getConexao()->executeQuery($sql);
        if ($table->RowCount() > 0) {
            return $table;
        }
        return null;
    }
    
    function atualizarOpcoesEnvio($modelo) { //($tipoEnvio, $modelo) {
        $usuario = controller_seguranca::getInstance()->identificarUsuario();
        
        $sql = 'UPDATE 
                  usuario 
                SET
                  -- news_tipo_envio = './*$tipoEnvio.*/',
                  news_modelo = '.$modelo.' 
                WHERE usuario_id = '.$usuario->getID();
                
        $this->getConexao()->executeNonQuery($sql);
        
    }

    function guardaremailtable(){

        var_dump($_GET['txtEmails']);

    }

    /*
    * Retorna substring que esta entre $start e $end na string $string
    *
    * @param $string string que se deseja retirar a subtring
    * @param $start string que antecede o inicio da substring (primeira ocorrencia)
    * @param $end string que procede o fim da substring (primeira ocorrencia)
    *
    * @return string Retorna uma substring de $string
    */

    function get_string_between($string, $start, $end){
		$string = " ".$string;
		$ini = strpos($string,$start);
		if ($ini == 0) return "";
		$ini += strlen($start);   
		$len = strpos($string,$end,$ini) - $ini;
		return substr($string,$ini,$len);
	}

	/*
    * Pega uma janela do banco de dados
    *
    * @param $janela_id id da janela
    * @return Object Retorna o objeto da janela cujo id é igual a $janela_id
    */
    public function getJanela ($janela_id)
    {
        $sql = 'SELECT * FROM janela WHERE janela_id='.$janela_id;
        $table_janela = $this->getConexao()->executeQuery($sql);

        $row_janela = null;

        if ($table_janela->RowCount() > 0) {
            $row_janela = $table_janela->getRow(0);
        }

        return $row_janela;
    }

    /*
    * Dada uma janela, pega a altura da mesma
    *
    * @param $janela Objeto janela
    * @return integer Altura da janela
    */
    public function getAlturaJanela ($janela)
    {
        if (isset($janela)) {
            return $janela->jan_altura;
        }
    }

    /*
    * Dada uma janela, pega a largura da mesma
    *
    * @param $janela Objeto janela
    * @return integer Largura da janela
    */
    public function getLarguraJanela ($janela)
    {
        if (isset($janela)) {
            return $janela->jan_largura;
        }
    }
    
}

?>
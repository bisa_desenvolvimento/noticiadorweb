<?


class Controller_api extends TController {

    private $base_url = "https://noticiadorweb.com.br/";

    function show() {
        die(__method__);
    }

    function save($dados=false){
        die(__method__);
    }

    function load($model){
        die(__method__);
    }

    function delete($dados=false){
        die(__method__);
    }

    function create($dados=false){
        die(__method__);
    }

    function process(){

        header("Access-Control-Allow-Headers: Token, Cliente-Key, Cache-Control");
        header("Accept: application/json");
        header("Access-Control-Allow-Methods: POST, OPTIONS");

        parent :: process();

        $parametros  = [];
        
        if(isset($_POST['areas'])){            
             foreach ($_POST['areas'] as $area) {
                $dados = explode(':',$area);                
                $parametros['areas'][$dados[0]] = (isset($dados[1]))?(integer)$dados[1]:0;
            }
        }

        if(isset($_POST['noticias'])){                                      
            $parametros['noticias'] = implode(',', $_POST['noticias']);        
        }

        switch ($this->getAction()) {
            case 'inteira':
                $this->inteira($parametros);
                break;
            case 'resumo':
                $this->resumo($parametros);
                break;                            
            case 'site':
                $parametros  = [];
        
                if(isset($_POST['areas'])){ 
                              
                    $parametros['areas'] = $_POST['areas'];

                    if(isset($_POST['quantidade'])){ 
                        $parametros['quantidade'] = $_POST['quantidade'];
                    }
                }

                $parametros['site'] = true;                                
               
                $this->resumo($parametros);

                break;
            default:
                 echo json_encode(['status' => false, 'msg' => 'Url Invalida!']);
                 break;
        }
    }

    public function inteira($parametros)
    {           
        $retorno = array();

        $cont = 0;

        if(count($parametros['noticias']) > 0){

            $table_noticia = $this->pegarNoticias($parametros['noticias']);            

            for($i = 0; $i < $table_noticia->RowCount(); $i++) { 

                $noticias = $table_noticia->getRow($i);

                $imagem = ($noticias->imagem)? $this->base_url.$noticias->imagem.'':null;
                // $imagem = ($noticias->imagem)? $this->base_url.$noticias->imagem.'':"https://www.noticiadorweb.com.br/users/ckfinder/imprensa@bisa.com.br_images/14_05_2019_114925.jpg";
                $link   = ($noticias->link)? $noticias->link."noticia_id=".$noticias->id."&idGN=0&qtdNot=0":null;

                $retorno[$cont]['status']             = true;
                $retorno[$cont]['id']                 = $noticias->id;
                $retorno[$cont]['area']               = $noticias->area;
                $retorno[$cont]['data']               = date('d/m/Y', strtotime($noticias->data));
                $retorno[$cont]['titulo']             = $noticias->titulo;
                $retorno[$cont]['noticia']            = $noticias->texto;
                $retorno[$cont]['link']               = $link;
                $retorno[$cont]['autor']              = $noticias->autor;
                $retorno[$cont]['acessos']            = $noticias->acessos;
                $retorno[$cont]['imagem']             = $imagem;

                $cont++;

            }

        }


        if(count($parametros['areas']) > 0){

            $table_areas   = $this->pegarNoticiasAreas($parametros['areas']);

            for($i = 0; $i < $table_areas->RowCount(); $i++) { 
                
                $noticias = $table_areas->getRow($i);

                $imagem = ($noticias->imagem)? $this->base_url.$noticias->imagem.'':null;
                // $imagem = ($noticias->imagem)? $this->base_url.$noticias->imagem.'':"https://www.noticiadorweb.com.br/users/ckfinder/imprensa@bisa.com.br_images/14_05_2019_114925.jpg";
                $link   = ($noticias->link)? $noticias->link."noticia_id=".$noticias->id."&idGN=0&qtdNot=0":null;

                $retorno[$cont]['status']             = true;
                $retorno[$cont]['id']                 = $noticias->id;
                $retorno[$cont]['area']               = $noticias->area;
                $retorno[$cont]['data']               = date('d/m/Y', strtotime($noticias->data));
                $retorno[$cont]['titulo']             = $noticias->titulo;
                $retorno[$cont]['noticia']            = $noticias->texto;
                $retorno[$cont]['link']               = $link;
                $retorno[$cont]['autor']              = $noticias->autor;
                $retorno[$cont]['acessos']            = $noticias->acessos;
                $retorno[$cont]['imagem']             = $imagem;

                $cont++;

            }            
        }

        if(count($retorno) == 0){
            $retorno['status'] =  false;
            $retorno['error'] = "As áreas/noticias solicitadas não foram encontradas!";
        }

       echo json_encode($retorno,JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG | JSON_ERROR_NONE | JSON_ERROR_DEPTH | JSON_ERROR_STATE_MISMATCH | JSON_ERROR_CTRL_CHAR | JSON_ERROR_SYNTAX | JSON_ERROR_RECURSION | JSON_PARTIAL_OUTPUT_ON_ERROR | JSON_BIGINT_AS_STRING | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

        
    }

    public function resumo($parametros)
    {
        $retorno = array();

        $cont = 0;

        if(count($parametros['noticias']) > 0){

            $table_noticia = $this->pegarNoticias($parametros['noticias']);            

            for($i = 0; $i < $table_noticia->RowCount(); $i++) { 

                $noticias = $table_noticia->getRow($i);
                $imagem = ($noticias->imagem)? $this->base_url.$noticias->imagem.'':null;
                // $imagem = ($noticias->imagem)? $this->base_url.$noticias->imagem.'':"https://www.noticiadorweb.com.br/users/ckfinder/imprensa@bisa.com.br_images/14_05_2019_114925.jpg";

                $link   = ($noticias->link)? $noticias->link."noticia_id=".$noticias->id."&idGN=0&qtdNot=0":null;

                $retorno[$cont]['status']             = true;
                $retorno[$cont]['id']                 = $noticias->id;
                $retorno[$cont]['area']               = $noticias->area;
                $retorno[$cont]['data']               = date('d/m/Y', strtotime($noticias->data));
                $retorno[$cont]['titulo']             = $noticias->titulo;
                $retorno[$cont]['noticia']            = mb_strimwidth($noticias->resumo, 0, 255, "...");
                $retorno[$cont]['link']               = $link;
                $retorno[$cont]['autor']              = $noticias->autor;
                $retorno[$cont]['acessos']            = $noticias->acessos;
                $retorno[$cont]['imagem']             = $imagem;
                
                $cont++;

            }

        }


        if(count($parametros['areas']) > 0){
            
            $table_areas   = isset($parametros['site']) ? $this->pegarNoticiasSite($parametros['areas'],$parametros['quantidade']):$this->pegarNoticiasAreas($parametros['areas']);            

            for($i = 0; $i < $table_areas->RowCount(); $i++) {                 

                $noticias = $table_areas->getRow($i);

                $imagem = ($noticias->imagem)? $this->base_url.$noticias->imagem.'':null;
                // $imagem = ($noticias->imagem)? $this->base_url.$noticias->imagem.'':"https://www.noticiadorweb.com.br/users/ckfinder/imprensa@bisa.com.br_images/14_05_2019_114925.jpg";
                $link   = ($noticias->link)? $noticias->link."noticia_id=".$noticias->id."&idGN=0&qtdNot=0":null;

                $retorno[$cont]['status']             = true;
                $retorno[$cont]['id']                 = $noticias->id;
                $retorno[$cont]['area']               = $noticias->area;
                $retorno[$cont]['data']               = date('d/m/Y', strtotime($noticias->data));
                $retorno[$cont]['titulo']             = $noticias->titulo;
                $retorno[$cont]['noticia']            = mb_strimwidth($noticias->resumo, 0, 255, "...");
                $retorno[$cont]['link']               = $link;
                $retorno[$cont]['autor']              = $noticias->autor;
                $retorno[$cont]['acessos']            = $noticias->acessos;
                $retorno[$cont]['imagem']             = $imagem;

                $cont++;

            }

        }

        if(count($retorno) == 0){
            $retorno['status'] =  false;
            $retorno['error'] = "As áreas/noticias solicitadas não foram encontradas!";
        }

        $retorno = mb_convert_encoding($retorno, 'UTF-8', 'latin1');
        $json = json_encode($retorno, JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

        if (json_last_error() !== JSON_ERROR_NONE) {
            echo 'Erro na codificação JSON: ' . json_last_error_msg();
        } else {
            echo $json;
        }
    }

    public function pegarNoticias($noticias)
    {
        mysql_query("SET NAMES 'utf8'");
        mysql_query('SET character_set_connection=utf8');
        mysql_query('SET character_set_client=utf8');
        mysql_query('SET character_set_results=utf8');

        $sql = "SELECT 
                  noticia.noticia_id AS id,
                  noticia_area.area_id AS area,
                  noticia.not_titulo AS titulo,
                  noticia.not_data AS data,
                  noticia.not_chamada AS resumo,
                  noticia.not_texto AS texto,
                  noticia.not_publicacao AS DATA,
                  noticia.not_link_abrir AS link,
                  noticia.not_thumbnail AS imagem,
                  usuario.usu_nome AS autor,
                  noticia.not_acessos AS acessos,
                  noticia_area.`area_id`
                FROM
                  noticia 
                  INNER JOIN usuario 
                    ON not_autor_id = usuario_id 
                  INNER JOIN noticia_area
                    ON noticia.`noticia_id` = noticia_area.`noticia_id`
                WHERE not_autorizada = '1' 
                  AND not_excluida = '0' 
                 AND noticia.noticia_id in(".$noticias.")";        

        $table = $this->getConexao()->executeQuery($sql);

        return $table;
    }

    public function pegarNoticiasAreas($areas)
    {           
        // mysql_query("SET NAMES 'utf8'");
        // mysql_query('SET character_set_connection=utf8');
        // mysql_query('SET character_set_client=utf8');
        // mysql_query('SET character_set_results=utf8');

        $sql = $this->sqlResumo();

        $cont = 0;

        foreach ($areas as $key => $value) {

            if($cont == 0){

                $limit = ($value)? " LIMIT ".$value."": ""; 

                $sql .= " AND CONCAT(noticia_area.`noticia_id`,noticia_area.`area_id`) IN (SELECT * FROM (SELECT CONCAT(noticia_area.`noticia_id`,noticia_area.`area_id`) FROM noticia_area WHERE noticia_area.`area_id` = ".$key." ORDER BY noticia_area.`noticia_id` DESC {$limit}) AS tb".$cont.")";
                
            }else{

                $limit = ($value)? " LIMIT ".$value."": ""; 

                $sql .= " OR CONCAT(noticia_area.`noticia_id`,noticia_area.`area_id`) IN (SELECT * FROM (SELECT CONCAT(noticia_area.`noticia_id`,noticia_area.`area_id`) FROM noticia_area WHERE noticia_area.`area_id` = ".$key." ORDER BY noticia_area.`noticia_id` DESC {$limit}) AS tb".$cont.")";
            }
            

            $cont++;           
        }     

        $sql .= "ORDER BY id DESC";    
        
        $table = $this->getConexao()->executeQuery($sql);

        return $table;
    }

    public function pegarNoticiasSite($areas,$quantidade)
    {     

          
        mysql_query("SET NAMES 'utf8'");
        mysql_query('SET character_set_connection=utf8');
        mysql_query('SET character_set_client=utf8');
        mysql_query('SET character_set_results=utf8');

        $sql = $this->sqlResumo();

        $cont = 0;

        
        foreach ($areas as $key => $value) {

            if($cont == 0){                 

                $sql .= " AND CONCAT(noticia_area.`noticia_id`,noticia_area.`area_id`) IN (SELECT * FROM (SELECT CONCAT(noticia_area.`noticia_id`,noticia_area.`area_id`) FROM noticia_area WHERE noticia_area.`area_id` = ".$value." ORDER BY noticia_area.`noticia_id` DESC) AS tb".$cont.")";
                
            }else{
                
                $sql .= " OR CONCAT(noticia_area.`noticia_id`,noticia_area.`area_id`) IN (SELECT * FROM (SELECT CONCAT(noticia_area.`noticia_id`,noticia_area.`area_id`) FROM noticia_area WHERE noticia_area.`area_id` = ".$value." ORDER BY noticia_area.`noticia_id` DESC) AS tb".$cont.")";
            }
            

            $cont++;           
        }   

        $sql .= "
                    GROUP BY id 

                    ORDER BY id DESC ";

                    if($quantidade > 0)
                    
               $sql .=" LIMIT {$quantidade}";
                   

        
        $table = $this->getConexao()->executeQuery($sql);

        return $table;
    }

    public function sqlResumo()
    {           
        
        $sqlResumo = "SELECT 
                  noticia.noticia_id AS id,
                  noticia_area.area_id AS area,
                  noticia.not_titulo AS titulo,
                  noticia.not_data AS data,
                  noticia.not_chamada AS resumo,
                  noticia.not_texto AS texto,
                  noticia.not_publicacao AS DATA,
                  noticia.not_link_abrir AS link,
                  noticia.not_thumbnail AS imagem,
                  usuario.usu_nome AS autor,
                  noticia.not_acessos AS acessos,
                  noticia_area.`area_id`
                FROM
                  noticia 
                  INNER JOIN usuario 
                    ON not_autor_id = usuario_id 
                  INNER JOIN noticia_area
                    ON noticia.`noticia_id` = noticia_area.`noticia_id`
                WHERE not_autorizada = '1' 
                  AND not_excluida = '0' ";        

        return $sqlResumo;
    }
}
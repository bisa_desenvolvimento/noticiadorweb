<?

class Controller_api_noticias extends TController {

	public function show() {

	}

	function load($key){

	}

	function save($model){
	}

	function delete($key){
	}

	function create(){
	}

	function obterDadosNoticia() {

		$noticia_id = $_REQUEST['noticia_id'];

		$sql = 'SELECT 
                  noticia.noticia_id AS id,
                  noticia.not_titulo AS titulo,
                  noticia.not_chamada AS resumo,
                  noticia.not_texto AS texto,
                  noticia.not_publicacao AS data,
                  noticia.not_link AS link,
                  noticia.not_thumbnail AS thumbnail,
                  usuario.usu_nome AS autor,
				  noticia.not_acessos AS acessos
                FROM
                  noticia
				INNER JOIN usuario ON not_autor_id = usuario_id
                WHERE 
                  noticia.noticia_id = '.$noticia_id;

        $table = $this->getConexao()->executeQuery($sql);
        $rows  = $table->Rows();

        $encoded_rows = array();

		$arrDadosNoticia = array();

        foreach($rows as $row) {

            $arrDadosNoticia[] = array_map('utf8_encode', (array)$row);
        }

		$encoded_rows['noticias'] = $arrDadosNoticia;

        $json = json_encode($encoded_rows);

		header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Key');
		header('Access-Control-Allow-Origin: *');
        echo $json;
    }

	function obterNoticia(){

		$mensagemDegub = "";

		$noticia_id = $_REQUEST['noticia_id'];

		$sum = isset($_REQUEST['sum']) ? "sum" : "";

		$qtdRes = isset($_REQUEST['qtdRes']) ? intval($_REQUEST['qtdRes']) : 0;

		//print $qtdRes;

		//$mensagemDegub = $mensagemDegub . "qtdRes: " . $qtdRes . "<br />";

		$qtdTxt = isset($_REQUEST['qtdTxt']) ? intval($_REQUEST['qtdTxt']) : 0;

		//print $qtdTxt;

		$imgAlt = isset($_REQUEST['imgAlt']) ? $_REQUEST['imgAlt'] : "";

		$strTituloNoticia = "";
		$strLink = "";
		$strAutor = "";
		$strData = "";
		$strAcessos = "";

		//$mensagemDegub = $mensagemDegub . "qtdTxt: " . $qtdTxt . "<br />";

		$sql = 'SELECT 
                  noticia.noticia_id AS id,
                  noticia.not_titulo AS titulo,
                  noticia.not_chamada AS resumo,
                  noticia.not_texto AS texto,
                  noticia.not_publicacao AS data,
                  noticia.not_link AS link,
                  noticia.not_thumbnail AS thumbnail,
                  usuario.usu_nome AS autor,
				  noticia.not_acessos AS acessos
                FROM
                  noticia
				INNER JOIN usuario ON not_autor_id = usuario_id
                WHERE 
                  noticia.noticia_id = '.$noticia_id;

        $table = $this->getConexao()->executeQuery($sql);
        $rows  = $table->Rows();

        $encoded_rows = array();

		$arrDadosNoticia = array();

        foreach($rows as $row) {

			if ($qtdRes > 0) {
				$row->resumo = substr(strip_tags($row->resumo), 0, $qtdRes);
				$posUltimoEspaco = strrpos($row->resumo, " ");
				$row->resumo = substr($row->resumo, 0, $posUltimoEspaco) . " ...";
				//$mensagemDegub = $mensagemDegub . "resumo: " . $row->resumo . "<br />";
			}

			if ($qtdTxt > 0) {
				$row->texto = substr(strip_tags($row->texto), 0, $qtdTxt);
				$posUltimoEspaco = strrpos($row->texto, " ");
				$row->texto = substr($row->texto, 0, $posUltimoEspaco) . " ...";
				//$mensagemDegub = $mensagemDegub . "texto: " . $row->texto . "<br />";
			}

			$strTituloNoticia = $row->titulo;
			$strLink = trim($row->link);

			if (strpos($strLink, "https://") === false) {
				$strLink = 'https://' . $row->link;
			}
			if ($strLink == "https://") {
				$strLink = 'Nenhuma Página Informada';
			}
			$strAutor = $row->autor;
			$strData = DateExibe($row->data);

			if ($sum != "") {
				$sqlUpdateAcessos = "UPDATE noticia SET not_acessos = (not_acessos+1) WHERE noticia_id = '".$noticia_id."';";
				$this->getConexao()->executeNonQuery($sqlUpdateAcessos);
				$row->acessos++; 
			}

			$strAcessos = $row->acessos;

			if ($row->thumbnail != "") {
				$strPathOfThumbnail = 'https://noticiadorweb.com.br/plugins/timthumb.php?src=https://noticiadorweb.com.br/users/public/thumb/';
				if (strpos($row->thumbnail, "thumb_") === false) {
					$strPathOfThumbnail = 'https://noticiadorweb.com.br/plugins/timthumb.php?src=https://noticiadorweb.com.br/';
				}
				if (strpos($row->thumbnail, "https://") !== false) {
					$strPathOfThumbnail = 'https://noticiadorweb.com.br/plugins/timthumb.php?src=';
				}
				$row->thumbnail = $strPathOfThumbnail . $row->thumbnail;
			} else {
				$row->thumbnail = 'https://noticiadorweb.com.br/plugins/timthumb.php?src=' . $imgAlt;
			}

			$strTextoNoticia = utf8_decode($row->texto);
			$strTextoNoticia = str_replace("/users/ckfinder/", "https://www.noticiadorweb.com.br/users/ckfinder/", $strTextoNoticia);
			$row->texto = $strTextoNoticia;

            $arrDadosNoticia[] = array_map('utf8_encode', (array)$row);
        }

		$encoded_rows['noticias'] = $arrDadosNoticia;



	    // $sql = 'SELECT * FROM comentario WHERE com_exibe = 1 AND noticia_id = "'.$_REQUEST['noticia_id'].'" order by com_data desc ';
		// $listaComentarios = $this->getConexao()->executeQuery($sql);
		$listaComentarios = 0;
        //echo '<pre>';
        //exit(var_dump($listaComentarios));
        //echo '</pre>';

		$comentarios = array();

        for ($i = 0; $i < $listaComentarios; $i++) { // $listaComentarios->rowCount()
			$comentario = $listaComentarios->getRow($i);

			//echo '<pre>';
			//exit(var_dump($comentario));
			//echo '</pre>';

			$comentarios[$i] = array();

			$comentarios[$i]["nome"] = $comentario->com_nome;
			$comentarios[$i]["comentario"] = $comentario->com_comentario;

			//echo '<pre>';
			//exit(var_dump($comentarios));
			//echo '</pre>';

			$comentarios[$i] = array_map('utf8_encode', $comentarios[$i]);
        }

		$encoded_rows['comentarios'] = $comentarios;

		$encoded_rows['qtdComentarios'] = $listaComentarios; //$listaComentarios->rowCount();

		$encoded_rows['baseUrlOrigem'] = isset($_REQUEST['urlOrigem']) ? $_REQUEST['urlOrigem'] : "";



		$strUrlShareBar = URL . "?secao=api_noticias&action=ShareBar&noticia_id=" . $_REQUEST['noticia_id'] . "&noticia_titulo=" . urlencode($strTituloNoticia) . "&urlOrigem=" . urlencode($_REQUEST['urlOrigem']) . "&url=" . urlencode(URL);
		//print $strUrlShareBar;
		$encoded_rows['ShareBar'] = $strUrlShareBar;



		$strUrlMetaDados = URL . "?secao=api_noticias&action=MetaDados&noticia_id=" . $_REQUEST['noticia_id'] . "&noticia_titulo=" . urlencode($strTituloNoticia) . "&autor=" . urlencode($strAutor) . "&data=" . urlencode($strData) . "&acessos=" . $strAcessos . "&link=" . urlencode($strLink) . "&urlOrigem=" . urlencode($_REQUEST['urlOrigem'])."&url=" . urlencode(URL);
		//print $strUrlMetaDados;
		$encoded_rows['MetaDados'] = $strUrlMetaDados;



		$strUrlFormComentar = URL . "?secao=api_noticias&action=FormComentar&noticia_id=" . $_REQUEST['noticia_id'] . "&noticia_titulo=" . urlencode($strTituloNoticia) . "&urlOrigem=" . urlencode($_REQUEST['urlOrigem']) . "&url=" . urlencode(URL);
		//print $strUrlFormComentar;
		$encoded_rows['FormComentar'] = $strUrlFormComentar;



		$strUrlShareComment = URL . "?secao=api_noticias&action=ShareComment&noticia_id=" . $_REQUEST['noticia_id'] . "&noticia_titulo=" . urlencode($strTituloNoticia) . "&urlOrigem=" . urlencode($_REQUEST['urlOrigem']) . "&url=" . urlencode(URL);
		//print $strUrlShareComment;
		$encoded_rows['ShareComment'] = $strUrlShareComment;



		$strUrlRequisitarScriptBeforeShareBar = "https://noticiadorweb.com.br/index.php?action=ScriptBeforeShareBar&secao=api_noticias";

		$encoded_rows['ScriptBeforeShareBar'] = $strUrlRequisitarScriptBeforeShareBar;

		$strUrlRequisitarScriptBeforeMetaDados = "https://noticiadorweb.com.br/index.php?action=ScriptBeforeMetaDados&secao=api_noticias";

		$encoded_rows['ScriptBeforeMetaDados'] = $strUrlRequisitarScriptBeforeMetaDados;

		$strUrlRequisitarScriptBeforeFormComentar = "https://noticiadorweb.com.br/index.php?action=ScriptBeforeFormComentar&secao=api_noticias";

		$encoded_rows['ScriptBeforeFormComentar'] = $strUrlRequisitarScriptBeforeFormComentar;

		$strUrlRequisitarScriptAfterShareBar = "https://noticiadorweb.com.br/index.php?action=ScriptAfterShareBar&secao=api_noticias";

		$encoded_rows['ScriptAfterShareBar'] = $strUrlRequisitarScriptAfterShareBar;

		$strUrlRequisitarScriptAfterFormComentar = "https://noticiadorweb.com.br/index.php?action=ScriptAfterFormComentar&secao=api_noticias";

		$encoded_rows['ScriptAfterFormComentar'] = $strUrlRequisitarScriptAfterFormComentar;



		$encoded_rows['mensagemDegub'] = $mensagemDegub;

        //echo '<pre>';
        //exit(var_dump($encoded_rows));
        //echo '</pre>';

        $json = json_encode($encoded_rows);

		header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Key');
		header('Access-Control-Allow-Origin: *');
        echo $json;
	}

	public function obterListaNoticias() {

		$mensagemDegub = "";

		$idGN = isset($_REQUEST['idGN']) ? intval($_REQUEST['idGN']) : 98;

		$qtdNot = isset($_REQUEST['qtdNot']) ? intval($_REQUEST['qtdNot']) : 0;

		$qtdRes = isset($_REQUEST['qtdRes']) ? intval($_REQUEST['qtdRes']) : 0;

		//print $qtdRes;

		//$mensagemDegub = $mensagemDegub . "qtdRes: " . $qtdRes . "<br />";

		$qtdTxt = isset($_REQUEST['qtdTxt']) ? intval($_REQUEST['qtdTxt']) : 0;

		//print $qtdTxt;

		//$mensagemDegub = $mensagemDegub . "qtdTxt: " . $qtdTxt . "<br />";

		$imgAlt = isset($_REQUEST['imgAlt']) ? $_REQUEST['imgAlt'] : "";

		$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;

		$page = $_REQUEST['page'] == 0 ? 1 : $page;

		$init = $qtdNot * ($page - 1);

		$sql = 'SELECT 
				  noticia.noticia_id AS id,
				  noticia.not_titulo AS titulo,
				  noticia.not_chamada AS resumo,
				  noticia.not_texto AS texto,
				  noticia.not_publicacao AS data,
				  noticia.not_link AS link,
				  noticia.not_thumbnail AS thumbnail 
				FROM
				  noticia 
				WHERE EXISTS 
				  (SELECT 
					* 
				  FROM
					noticia_area 
				  WHERE noticia.noticia_id = noticia_area.noticia_id 
					AND noticia_area.area_id IN 
					(SELECT 
					  area_id 
					FROM
					  janela_livre_area 
					WHERE janela_livre_id = '.$idGN.')) 
				  AND noticia.not_autorizada = 1 
				  AND noticia.not_excluida = 0 
				ORDER BY noticia.noticia_id DESC';

		if ($qtdNot != 0) {
			$sql .= ' LIMIT ' . $init.', ' . $qtdNot;
		}

		$table = $this->getConexao()->executeQuery($sql);
		$rows  = $table->Rows();

		$encoded_rows = array();

		$arrNoticias = array();

		foreach($rows as $row) {

			if ($qtdRes > 0) {
				$row->resumo = substr(strip_tags($row->resumo), 0, $qtdRes);
				$posUltimoEspaco = strrpos($row->resumo, " ");
				$row->resumo = substr($row->resumo, 0, $posUltimoEspaco) . " ...";
				//$mensagemDegub = $mensagemDegub . "resumo: " . $row->resumo . "<br />";
			}

			if ($qtdTxt > 0) {
				$row->texto = substr(strip_tags($row->texto), 0, $qtdTxt);
				$posUltimoEspaco = strrpos($row->texto, " ");
				$row->texto = substr($row->texto, 0, $posUltimoEspaco) . " ...";
				//$mensagemDegub = $mensagemDegub . "texto: " . $row->texto . "<br />";
			}

			if ($row->thumbnail != "") {
				$strPathOfThumbnail = 'https://noticiadorweb.com.br/plugins/timthumb.php?src=https://noticiadorweb.com.br/users/public/thumb/';
				if (strpos($row->thumbnail, "thumb_") === false) {
					$strPathOfThumbnail = 'https://noticiadorweb.com.br/plugins/timthumb.php?src=https://noticiadorweb.com.br/';
				}
				if (strpos($row->thumbnail, "https://") !== false) {
					$strPathOfThumbnail = 'https://noticiadorweb.com.br/plugins/timthumb.php?src=';
				}
				$row->thumbnail = $strPathOfThumbnail . $row->thumbnail;
			} else {
				$row->thumbnail = 'https://noticiadorweb.com.br/plugins/timthumb.php?src=' . $imgAlt;
			}

			$strTextoNoticia = utf8_decode($row->texto);
			$strTextoNoticia = str_replace("/users/ckfinder/", "https://www.noticiadorweb.com.br/users/ckfinder/", $strTextoNoticia);
			$row->texto = $strTextoNoticia;

			$arrNoticias[] = array_map('utf8_encode', (array) $row);
		}

		//print "count(\$arrNoticias):<pre>";
		//print_r(count($arrNoticias));
		//print "</pre>";

		if (isset($_REQUEST["urlNoticia"])) {

			$intQtdNoticias = count($arrNoticias);

			for ($intIndiceNoticias = 0; $intIndiceNoticias < $intQtdNoticias; $intIndiceNoticias++) {

				$strUrlNoticia = strtok($_REQUEST["urlNoticia"], '?') . "?noticia_id=" . $arrNoticias[$intIndiceNoticias]['id'] . "&idGN=" . $_REQUEST["idGN"] . "&qtdNot=" . $_REQUEST["qtdNot"];

				//print "strUrlNoticia:<pre>";
				//print_r($strUrlNoticia);
				//print "</pre>";

				$arrNoticias[$intIndiceNoticias]["urlNoticia"] = $strUrlNoticia;
			}
		}

		$encoded_rows['noticias'] = $arrNoticias;

		$sql = 'SELECT 
				  COUNT(*) AS total
				FROM
				  noticia 
				WHERE EXISTS 
				  (SELECT 
					* 
				  FROM
					noticia_area 
				  WHERE noticia.noticia_id = noticia_area.noticia_id 
					AND noticia_area.area_id IN 
					(SELECT 
					  area_id 
					FROM
					  janela_livre_area 
					WHERE janela_livre_id = '.$idGN.')) 
				  AND noticia.not_autorizada = 1 
				  AND noticia.not_excluida = 0';

		$table = $this->getConexao()->executeQuery($sql);
		$rows  = $table->Rows();
		$total = $rows[0]->total;

		$encoded_rows['total'] = $total;

		$encoded_rows['mensagemDegub'] = $mensagemDegub;

		$json = json_encode($encoded_rows);

		header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Key');
		header('Access-Control-Allow-Origin: *');
		echo $json;
	}

	public function obterAreasNoticia($id) {
		$sql = "SELECT area_id, are_descricao FROM noticia_area NATURAL JOIN area WHERE noticia_id = '".$id."';";
		return $table = $this->getConexao()->executeQuery($sql);
    }

    function process(){
 	 	parent::process();
    
	 	switch($this->getAction()) {
	 		 case "ScriptBeforeAll":
				$view = new view_api_noticias($this);
				$view->scriptBeforeAll();
             break;
	 		 case "obterDadosNoticia":
				$this->obterDadosNoticia();
             break;
	 		 case "obterNoticia":
				$this->obterNoticia();
             break;
	 		 case "ScriptBeforeShareBar":
				$view = new view_api_noticias($this);
				$view->scriptBeforeShareBar();
             break;
	 		 case "ShareBar":
				$view = new view_api_noticias($this);
				$view->shareBar();
             break;
	 		 case "ScriptAfterShareBar":
				$view = new view_api_noticias($this);
				$view->scriptAfterShareBar();
             break;
	 		 case "ScriptBeforeMetaDados":
				$view = new view_api_noticias($this);
				$view->scriptBeforeMetaDados();
             break;
	 		 case "MetaDados":
				$view = new view_api_noticias($this);
				$view->metaDados($_REQUEST);
             break;
	 		 case "ScriptBeforeFormComentar":
				$view = new view_api_noticias($this);
				$view->scriptBeforeFormComentar();
             break;
	 		 case "FormComentar":
				$view = new view_api_noticias($this);
				$view->formComentar();
             break;
	 		 case "ScriptAfterFormComentar":
				$view = new view_api_noticias($this);
				$view->scriptAfterFormComentar();
             break;
	 		 case "ShareComment":
				$view = new view_api_noticias($this);
				$view->shareComment();
             break;
	 		 case "obterListaNoticias":
				$this->obterListaNoticias();
             break;
	 		 case "ScriptAfterAll":
				$view = new view_api_noticias($this);
				$view->scriptAfterAll();
             break;
		}
	}

}
?>
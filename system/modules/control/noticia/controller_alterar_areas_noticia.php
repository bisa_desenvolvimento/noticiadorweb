<?
class controller_alterar_areas_noticia extends TController
{
	function show() {
		$view = new view_alterar_areas_noticia($this);
		$view->show();
	}
	
	function process() {
    	parent::process();
    	
    	switch ($this->getAction()) {
    		case "save":
            	if (array_key_exists("area", $_POST)) {
			 		$areas = $_POST["area"];
			 	} else {
			 		$areas = array();
			 	}
    			$this->salvar($areas, $_POST["noticia_id"]);
    	    	break;    	
    	    	
    	    	   
		}        
    }
    
	function salvar($areas, $idNoticia){
	            $usuario_id = controller_seguranca::getInstance()->identificarUsuario()->getPerfil();
                $not_autorizada=1;
                if($usuario_id==2){
                $not_autorizada=0;
                }
                $sql = "DELETE FROM noticia_area WHERE noticia_id = ".$idNoticia;
		$error = $this->getConexao()->executeNonQuery($sql);
		if ($error != -1) {
			foreach ($areas as $id) {
				$sql = "INSERT INTO noticia_area (noticia_id, area_id, not_aut) values (".$idNoticia.",".$id.",".$not_autorizada.")";
				$error = $this->getConexao()->executeNonQuery($sql);
			}
            
                        	$sql = "UPDATE noticia SET not_autorizada=".$not_autorizada." WHERE noticia_id=".$idNoticia.";";
				$error2 = $this->getConexao()->executeNonQuery($sql);
                
		}
		if (($error != -1)&&($error != -1)) {
			print msg_alert("áreas da notícia salvas com sucesso!");		
				
			$controller_arquivo = new controller_arquivo();
        	$controller_arquivo->setConexao(TConexao::getInstance());
        	
			$url = "index.php?".PARAMETER_NAME_ACTION."=show&";
			$url .= PARAMETER_NAME_FILE."=gestao_noticia";

			print "<script>parent.location.href='".$url."'</script>";//redirect($url);
		} else {
			print msg_alert("As áreas da notícia não poderam ser alteradas. ".$this->getConexao()->getErro());
		}
	}    
	
	function save($model){

	}
	
	function load($key){
        $sql = "SELECT * FROM area WHERE area_id = ".$key;
        $table = $this->getConexao()->executeQuery($sql);
        if ($table->RowCount() > 0) {
            $result = new Area();
            $result->bind($table->getRow(0));
            if ($result != null) {
                $view = new view_gestao_areas_de_interesse($this);
                $view->setModel($result);
                $view->show();
            }
            return $result;
        } 
        return null;

	}
	
	function delete($key){
		$sql = "DELETE FROM area WHERE area_id = $key";
        
        $this->getConexao()->executeNonQuery($sql);
        print msg_alert("Registro removido.");
        $this->show();
	}
	
	function create(){
		
	}		
	
	function obterAreas() {
		$usuario = controller_seguranca::getInstance()->identificarUsuario();
        $sql = 'SELECT * FROM area ';
		 $sql .= ' where area_id in (select area_id from area_usuario where usuario_id ='.$usuario->getID().')
				  ORDER BY are_descricao'; 	
	    $result = $this->getConexao()->executeQuery($sql);
        
	    
	    if ($result != null) {
	        return $result;
	    }
	    
	    return null;
	}
	
	function obterAreasCadastradas($idNoticia, $idArea) {
        
		$sql = 'SELECT * FROM noticia_area 
				WHERE noticia_area.noticia_id = '.$idNoticia.
			 	 ' AND noticia_area.area_id = '.$idArea;

        $noticia_area = $this->getConexao()->executeQuery($sql);

        if ($noticia_area->RowCount() > 0){

	        	return " checked ";
		}
		
        return null;
	}
	
	function obterNoticia($noticia_id) {
        $sql = 'SELECT * FROM noticia where noticia_id = '.$noticia_id;
        $table_noticia = $this->getConexao()->executeQuery($sql);
		
        if ($table_noticia->RowCount() > 0) {
        	return $table_noticia;
        }
        
        return null;
    }
    
    /**
     * Obter as áreas associadas para cada cliente passado no parâmetro cliente. As áreas sem associação com cliente receberão como parâmetro "COLAB"
     * Caso seja passado o parâmetro usuario, a função retorna os clientes do usuário informado.
     * Caso não seja passado o parâmetro usuario, a função retorna os clientes do usuário atualmente logado no sistema.
     * André Alves - 27/01/2012
     * 
     * @param string $cliente Id do cliente
     * @param int $usuario Id do usuário
     * @return object Lista com os clientes
     */
    /*function obterAreasDoUsuario($cliente = '', $usuario = '') { Função deslocada para o TController - Jorge Roberto Mantis 6383
        
        $where = '';
        
        if($usuario == '') {
            $res = controller_seguranca::getInstance()->identificarUsuario();
            $usuario = $res->getID();    
        }
        
        if($cliente != '') {
            $where .= ' AND Clie_Codigo = "'.$cliente.'"';
        }
        
        $sql = 'SELECT 
                  area_id,
                  are_descricao,
                  are_observacao 
                FROM
                  vw_usuarios_areas_clientes 
                WHERE usuario_id = '.$usuario.
                $where.'                   
                ORDER BY are_descricao';
        
        $result = $this->getConexao()->executeQuery($sql);
        
        if ($result != null) {
            return $result;
        }
        
        return null;
    }*/
    
    
    /**
     * Obter a lista dos clientes associados ao usuário.
     * Caso seja passado o parâmetro usuario, a função retorna os clientes do usuário informado.
     * Caso não seja passado o parâmetro usuario, a função retorna os clientes do usuário atualmente logado no sistema.
     * André Alves - 27/01/2012
     * 
     * @param int $usuario Id do usuário
     * @return object Lista com os clientes
     */
    function obterClientesDoUsuario($usuario = '') {
        
        if($usuario == '') {
            $res = controller_seguranca::getInstance()->identificarUsuario();
            $usuario = $res->getID();    
        }        
        
        $sql = 'SELECT 
                  CLIE_Codigo,
                  CLIE_Nome 
                FROM
                  vw_usuarios_areas_clientes 
                WHERE usuario_id = '.$usuario.' 
                GROUP BY
                CLIE_Codigo,
                CLIE_Nome';
        
        $result = $this->getConexao()->executeQuery($sql);
        
        if ($result != null) {
            return $result;
        }
        
        return null;
    
    }
}

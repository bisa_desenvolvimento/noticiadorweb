<?

class Controller_comentario extends TController
{
	function show() { 
		$view = new view_comentario($this);
		$view->show();
	}
	
	function save($model) {

        require_once(DIR_PLUGINS.'captchaV2.php');      
        $resp = _recaptcha_http_post ($_POST['g-recaptcha-response'],$_SERVER['REMOTE_ADDR']);       
            
        if (!$resp->success) {        
        
            $_SESSION['comentario'] = $_POST;
            $_SESSION['comentario']['erro'] = $resp->error;
            
            print msg_alert("Imagem de confirmação errada.");

            $url = $_REQUEST["urlOrigem"]."?noticia_id=".$model->getNoticia_id()."&action=show&secao=exibir_noticia";
			print "<script>parent.location.href='".$url."'</script>";
        
        } else {

            $infoUsuario ="";
            echo "<pre>";

            foreach ($_SERVER as $key=>$value){
                $infoUsuario .= $key." => ".$value."<br>";
            }

            $sql = "INSERT INTO comentario
						(com_nome,
						 com_email,
						 com_fone,
						 com_comentario,
						 noticia_id,
						 com_exibe,
						 com_data,
						 com_notificacao,
						 com_infousuario)
                    VALUES ('".$model->getCom_nome()."',
                            '".$model->getCom_email()."',
                            '".$model->getCom_fone()."',
                            '".mysql_escape_string(strip_tags($model->getCom_comentario()))."',
                            '".$model->getNoticia_id()."',
                            '1',
                            '".$model->getCom_data()."',
                            '".$model->getCom_notificacao()."',
                            '".$infoUsuario."')";


            $error = $this->getConexao()->executeNonQuery($sql);


        
            if ($error!=-1){
                $this->inseriMailAssinanteArea($model->getCom_email(),$_POST['areas'][0],$model->getCom_nome());
                print msg_alert("Comentário salvo com sucesso.");
                $url = $_REQUEST["urlOrigem"]."?noticia_id=".$model->getNoticia_id()."&action=show&secao=exibir_noticia";
    			print "<script>parent.location.href='".$url."'</script>";
            } else {
                print msg_alert("Comentário não salvo.".mysql_error());
                $url = $_REQUEST["urlOrigem"]."?noticia_id=".$model->getNoticia_id()."&action=show&secao=exibir_noticia";
               // print "<script> history.back();</script>";
                        
            }                     
        
        }   
                        
	}
   
   function indicar() {
      
   }

	function salvar($comentariosID,$aceptarID, $idNoticia){
//
//            echo "nuevos comentarios aprovados";
            $comentariosNovos=( array_diff($comentariosID,$aceptarID ));
//            var_dump($comentariosNovos);
//             echo "nuevos comentarios NO aprovados";
             $comentariosDesaprovados=( array_diff($aceptarID,$comentariosID));
//             var_dump( $comentariosDesaprovados);
//            echo "comentarios seleccionados";
             $comentariosSelecionados=$comentariosID;
//             var_dump($comentariosSelecionados);
//            echo "que ya estaban checkeados en la base de datos";
             $comentariosVelhos=$aceptarID;
//             var_dump($comentariosVelhos);


                $sql = "UPDATE comentario SET com_exibe = 0 WHERE noticia_id=$idNoticia";
                $error = $this->getConexao()->executeNonQuery($sql);
               // echo $sql;

                if(sizeof($comentariosID) > 0) {
                    $ids = implode(",",$comentariosID);
                    $sql = "UPDATE comentario SET com_exibe = 1, com_dataaprovacao = null WHERE comentario_id in ($ids)";
                    $this->getConexao()->executeNonQuery($sql);

                    $sql = "UPDATE comentario SET com_dataaprovacao = NOW() where com_dataaprovacao is null and noticia_id=$idNoticia";
                    $error = $this->getConexao()->executeNonQuery($sql);
                } else {

                    $sql = "UPDATE comentario SET com_dataaprovacao = NOW() WHERE noticia_id=$idNoticia";
                    $error = $this->getConexao()->executeNonQuery($sql);
                }


		if ($error != -1) {
			print msg_alert("Registro(s) salvo(s)!");
				
			$controller_arquivo = new controller_arquivo();
			$controller_arquivo->setConexao(TConexao::getInstance());
			
			$url = "index.php?".PARAMETER_NAME_ACTION."=show&";
			$url .= PARAMETER_NAME_FILE."=comentario";
			$url .= "&id=".$idNoticia;
			
			print "<script>parent.location.href='".$url."'</script>";//redirect($url);

			if (!empty($comentariosNovos)) {
				$fromComentario = $this->pegarComentariosNotificacao($comentariosNovos);
				$totalNovos= $fromComentario->RowCount();
				if (!empty($comentariosVelhos)) {
					$toComentario = $this->pegarComentariosNotificacao($comentariosVelhos);
					$informacao = array();

					for ($j = 0; $j < $totalNovos; $j++) {

						$informacao["comentario"][] = $fromComentario->getRow($j)->com_comentario;
						$informacao["nome"][] = $fromComentario->getRow($j)->com_nome;
					}

					for ($i = 0; $i < $toComentario->RowCount(); $i++) {
						$row = $toComentario->getRow($i);
						$this->enviarEmail($row->com_email, $informacao["nome"], $informacao["comentario"], $idNoticia, $_POST['not_titulo'], "notificacao");
					   // echo $email . " < ---- enviado para este email<br/><br/>";
					}
						//echo "entre ellos mismos --------------------------------------------------------------------------";
						$informacao = array();
						$dados = $fromComentario;
						if($totalNovos>1){
						for ($i = 0; $i < $totalNovos; $i++) {
						$row = $fromComentario->getRow($i);
						$email = $row->comentario_id;
						for ($j = 0; $j < $totalNovos; $j++) {
							if ($email != $dados->getRow($j)->comentario_id) {
								$informacao["comentario"][] = $dados->getRow($j)->com_comentario;
								$informacao["nome"][] = $dados->getRow($j)->com_nome;
							}
						}

						$this->enviarEmail($row->com_email, $informacao["nome"], $informacao["comentario"], $idNoticia, $_POST['not_titulo'], "notificacao");
						unset($informacao["comentario"]);
						unset($informacao["nome"]);
					   // echo $email = $row->com_email . " < ---- enviado para este email<br/><br/>";
						}

						}
				}


            }
                       
		} else {
			print msg_alert("O(s) registro(s) não foi salvo(s). ".$this->getConexao()->getErro());
		}
	}

	function obterComentarios($noticia_id) {
        $sql = 'SELECT * FROM comentario where  noticia_id = '.$noticia_id. " order by com_data desc";
        $table_comentario = $this->getConexao()->executeQuery($sql);
		
        if ($table_comentario->RowCount() > 0) {
        	return $table_comentario;
        }
        
        return null;
    }
	
	function obterNoticia($noticia_id) {
        $sql = 'SELECT * FROM noticia where noticia_id = '.$noticia_id;
        $table_noticia = $this->getConexao()->executeQuery($sql);
		
        if ($table_noticia->RowCount() > 0) {
        	return $table_noticia;
        }
        
        return null;
    }

	
	function load($key){ 
		$sql = "SELECT * FROM comentario WHERE comentario_id= ".$key;
		
		$table = $this->getConexao()->executeQuery($sql);
		if ($table->RowCount() > 0) {	
			$result = new comentario();
			$result->bind($table->getRow(0));
			if ($result != null){					
                $view = new view_comentario($this);
                $view->show();
			}
		} 
		return null;

	}
	
	function obterComentarioAprovado($comentario_id){
	
        $sql = 'SELECT * FROM comentario where com_exibe=1 and comentario_id='.$comentario_id;

        $noticia_area = $this->getConexao()->executeQuery($sql);

        if ($noticia_area->RowCount() > 0){

	        	return "checked";
		}
		
        return null;
    }

	function delete($key){
		$sql = "DELETE FROM comentario WHERE  comentario_id = ".$key;
		$this->getConexao()->executeNonQuery($sql);

	}
	
	function atualizarComentarios($array, $noticia_id){
	
		$sql = "UPDATE comentario SET com_exibe=0 where noticia_id = ".$noticia_id;
		//$this->getConexao()->executeNonQuery($sql);
			
		for($i=0;$i<sizeof($array);$i++){
			$sql .= "; UPDATE comentario SET com_exibe=1 where comentario_id = ".$array[$i];
            $sql .= "; UPDATE comentario SET com_dataaprovacao = NOW() where com_dataaprovacao is null and comentario_id = ".$array[$i];
			$this->getConexao()->executeNonQuery($sql);	
		}

	}

	function create(){ }

	function process(){
 		parent::process();

		switch($this->getAction()) {
			case "save":
                $save = new comentario();
				$strNome = utf8_decode($_POST["Fnome"]);
				if (isset($_REQUEST["setCharset"]))
					if ($_REQUEST["setCharset"] != "")
						$strNome = utf8_decode($_POST["Fnome"]);
				$save->setCom_nome($strNome);
                $save->setCom_email($_POST["Femail"]);
                $save->setCom_fone($_POST["Ffone"]);
				$strComentario = utf8_decode($_POST["Fcomentario"]);;
				if (isset($_REQUEST["setCharset"]))
					if ($_REQUEST["setCharset"] == "utf8")
						$strComentario = utf8_decode($strComentario);
                $save->setCom_comentario($strComentario);
                $save->setCom_data(date("Y-m-d H:i:s"));
                $save->setNoticia_id($_POST["Fnoticia"]);
//                var_dump($_POST['Fnotificacao']);
//                exit ();
                if (isset($_POST['Fnotificacao'])) 
                $save->setCom_notificacao($_POST['Fnotificacao']);
                else
                $save->setCom_notificacao(0);
                $this->save($save);
                $editores = $this->pegarEditores($_POST['areas']);
                for ($i=0;$i<$editores->RowCount();$i++){
                    $row=$editores->getRow($i);
                    $this->enviarEmail($row->usu_email, $_POST["Fnome"], $_POST["Fcomentario"], $_POST["Fnoticia"],$_POST['not_titulo'],"notificacao");
                }
      
				break;

			case "salvar":
                if (array_key_exists("comentarios", $_POST)) {
			 		$comentarios = $_POST["comentarios"];
                                        if(array_key_exists("aceptar", $_POST)){
                                            $aceptar = $_POST["aceptar"];
                                        }else{$aceptar = array();}
			 	} else {
			 		$comentarios = array();
                                        $aceptar = array();
			 	}
				$this->salvar($comentarios,$aceptar, $_POST["noticia_id"]);
				break;
		}
	 }

    function getOptin($email,$nome,$email_assinante,$areasDescricao) {
        $arquivo = DIR_DATA.'mensagensoptin/'.$email.'.txt';
        if(file_exists($arquivo)) {
            $fp = fopen($arquivo, "r");
            $texto = '';
            while(!feof($fp)) {
                $texto .= fgetc($fp);
            }
            fclose($fp);
        } else {
            $fp = fopen(DIR_DATA.'mensagensoptin/padrao-pt-BR.txt', "r");
            $texto = '';
            while(!feof($fp)) {
                $texto .= fgetc($fp);
            }
            fclose($fp);
        }
        echo "<pre>";
        $texto .= nl2br(translate('<br /><br /><br />Se você não clicar no link <a href="[link]" target="_blank">[link]</a> estamos presumindo que você deseja continuar a receber nossas informações, e será um prazer manter o seu endereço em nosso cadastro.'));
        $texto = utf8_decode($texto);
        $link = URL.'index.php?action=show&secao=remover_usuario_email&id='.$email_assinante;
        $tags = array('[link]','[nome]','[email]');
        $nome.=" (editor da(s) area(s): ";
        for ($index = 0; $index < $areasDescricao->RowCount(); $index++) {
            $nome.=$areasDescricao->getRow($index)->are_descricao;
        }
        $nome.=") ";
        $valores = array($link,$nome,$email);
        $mensagem = str_replace($tags, $valores, $texto);

        return $mensagem;
    }

	function enviarEmail($email,$usuario,$comentario,$noticia,$titulo,$tipo){

	   $content="";
	   $title="";

		switch ($tipo) {
			case "autorizacao":

				$content="<b>Autorização</b><br/>";
				$content.="<b>A noticia: </b>".utf8_decode($titulo)." tem novos comentarios.<br/>";
				$content.="<b>Usuario: </b>".utf8_decode($usuario)."<br/><b> Fiz o seguinte comentário: </b>".utf8_decode($comentario)."<br/>";
				$content.="<br/><b> na noticia: </b><a href='".URL."index.php?action=show&secao=exibir_noticia&noticia_id=".$noticia."'>".utf8_decode($titulo)."</a>";
				$content.="<br/> Faca login no Noticiador e clique <a href='".URL."index.php?action=show&secao=comentario&id=".$noticia."'>aqui</a> ";
				$title="Novo comentario no NoticiadorWeb 2.0";

				break;
			case "notificacao":
				$content = "O leitor <b>".utf8_decode($usuario)."</b> deixou na notícia <a href='".URL."index.php?action=show&secao=exibir_noticia&noticia_id=".$noticia."'><strong>".utf8_decode($titulo)."</strong></a>
				 o seguinte comentário: <br /><br />".utf8_decode($comentario);
				$title="Novo comentário em notícia do NoticiadorWeb 2.0";
				break;

			default:
				break;
		}
            
            $headers = "MIME-Version: 1.0\n";
            $headers .="X-Sender:no-reply@smtp.ownserver.noticiadorweb.com.br\n";
            $headers .="X-mailer: PHP/" . phpversion() . "\n";
            $headers .="X-Priority: 1\n";
            $headers .="Content-Type: text/html; charset=ISO-8859-1\n";
            $headers .="Reply-To:  no-reply@smtp.ownserver.noticiadorweb.com.br \n";

            $headers .= "To: $email\n";
            $headers .= "From: no-reply@smtp.ownserver.noticiadorweb.com.br \n";
            $headers .= "Return-Path: no-reply@smtp.ownserver.noticiadorweb.com.br \n";
            mail($email, $title,$content, $headers);
           //echo $content."<br/>".$email;
        }

    function enviarEmailAssinatura($nome,$email_assinante,$areas){
        // sempre será enviado um email de Confirmação de assinatura ao assinante

        $headers = "MIME-Version: 1.0\r\n";
        $headers .="X-Sender: Noticiador Web <no-reply@" . STMP_HOST . "> \r\n";
        $headers .="X-mailer: PHP/" . phpversion() . " \r\n";
        $headers .="X-Priority: 1 \r\n";
        $headers .="Content-Type: text/html; charset=ISO-8859-1\r\n";
        $headers .="Reply-To: Noticiador Web <no-reply@" . STMP_HOST . "> \r\n";
        $from= "From: Noticiador Web <no-reply@" . STMP_HOST . "> \r\n";
        $return="Return-Path: Noticiador Web <no-reply@" . STMP_HOST . "> \r\n";


        $sql = 'SELECT are_descricao FROM area WHERE area_id in ('.$areas .') ';

        $areasDescricao = $this->getConexao()->executeQuery($sql);


            $sql = 'SELECT 
                      usu_email
                    FROM
                      usuario 
                      JOIN area_usuario ON usuario.`usuario_id` = area_usuario.`usuario_id` 
                    WHERE area_usuario.`area_id` ='.$areas;

            $table = $this->getConexao()->executeQuery($sql);



            if ($table->RowCount()>0){

                $row = $table->getRow(0);
                $email = $row->ass_email;

                $headersEditor = "To: $email \r\n";
                $headersEditor .= $from;
                $headersEditor .= $return;

                //email pro editor avisando de um novo cadastro nas suas áreas de interesse
                $mensagem = "O usuário <strong>".utf8_decode($nome)."</strong> realizou o cadastro do e-mail <strong>".$email_assinante."</strong> em sua newsletter.<br /><br />";
                $assunto = "Novo e-mail cadastrado na newsletter";
                mail($email,$assunto,$mensagem,$headersEditor);


                //guarda o ordem das headers
                $headersAssinante = "To: $email_assinante \r\n";
                $headersAssinante .= $from;
                $headersAssinante .= $return;

                //email pro assinante da newsletter
                $assunto = "Confirmação de assinatura";
                $mensagem = $this->getOptin($email, '', $email_assinante,$areasDescricao);
                mail($email_assinante,$assunto,$mensagem,$headersAssinante);

        }

    }

    function inseriMailAssinanteArea($emailInserir, $areaInserir,$nomeAssinante){

                    $senha = explode("@",$emailInserir);
                    $senha = sha1($senha[0]);

                    // caso nao exista o email cadastrado
                    $assinante_id = $this->checarEmail($emailInserir);
                    

                    if (!$assinante_id){ // inseri e-mail em "ASSINANTE"
                        $sql = "INSERT INTO assinantes (ass_nome, ass_email, ass_senha, ass_confirmado, ass_excluido) 
										VALUES ('".$nomeAssinante."','".$emailInserir.
                            "', '".$senha.
                            "', '1".
                            "', '0')";
                        $error = $this->getConexao()->executeNonQuery($sql);

                        if ($error != -1) { // INSERI EM "ASSINANTE_AREA"
                            $sql = "INSERT INTO assinante_area(assinante_id, area_id) 
											VALUES ('".$this->checarEmail($emailInserir).
                                "', '".$areaInserir."')";
                             $this->getConexao()->executeNonQuery($sql);

                            $this->enviarEmailAssinatura($nomeAssinante,$emailInserir,$areaInserir);
                        }

                    }else{ // caso o e-mail já exista no cadastro e nao exista na área selecionada inserir
                        if (!$this->checarEmailArea($assinante_id, $areaInserir)){

                            $sql = "INSERT INTO assinante_area(assinante_id, area_id) 
											VALUES ('".$this->checarEmail($emailInserir).
                                "', '".$areaInserir."')";
                             $this->getConexao()->executeNonQuery($sql);
                        }
                    }

    }

    function checarEmail($email){
        $sql = "SELECT * FROM assinantes WHERE ass_email = '". $email ."'";

        $assinante = $this->getConexao()->executeQuery($sql);

        if ($assinante->RowCount() > 0) {
            return $assinante->getRow(0)->assinante_id;
        }

        return null;
    }

    function checarEmailArea($assinante_id, $area_id){
        $sql = "SELECT * FROM assinante_area WHERE assinante_id = ". $assinante_id ." and area_id=".$area_id;

        $assinante = $this->getConexao()->executeQuery($sql);

        if ($assinante->RowCount() > 0) {
            return true;
        }

        return false;
    }

    function pegarEditores($areas){
            $sql="SELECT usu_email from usuario, area_usuario where area_usuario.usuario_id = usuario.usuario_id AND area_usuario.area_id IN (".implode(",", $areas).") AND usuario.usu_perfil!=2";
            $table = $this->getConexao()->executeQuery($sql);
            return $table;
        }

    function pegarComentariosNotificacao($idComentario){
             $sql = "SELECT * FROM comentario WHERE comentario_id IN (" . implode(",",$idComentario ). ") AND com_notificacao=1 AND com_exibe=1";
                    $comentario = $this->getConexao()->executeQuery($sql);

                    return $comentario;
        }
	

}
?>
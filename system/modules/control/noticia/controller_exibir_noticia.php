<?
class controller_exibir_noticia extends TController {
    
    function show() {
			$view = new view_exibir_noticia($this);
			$view->show();	
	  }
  
   	function save($model){}
   	function load($key){}
   	function delete($key){}
   	function create(){}
    
    public function contarVisualizacao($id) {
      $sql = "UPDATE noticia SET not_acessos = (not_acessos+1) WHERE noticia_id = '".$id."';";
      $this->getConexao()->executeNonQuery($sql);
    }
    
    public function obterNoticia($id) {
      $sql = "SELECT noticia.*, usuario.usu_nome FROM noticia INNER JOIN usuario ON not_autor_id = usuario_id 
                WHERE noticia_id = '".$id."' AND not_autorizada = '1' AND not_excluida = '0'"; 
              //AND DATE(not_validade) > '".date('Y-m-d')."';";
      $table = $this->getConexao()->executeQuery($sql);
      if($table->rowCount()>0) {
        return $table->getRow(0);  
      } else {
        return false;
      }
    }
    
    public function obterAreasJanela($id) {
      $sql = "SELECT area_id, are_descricao FROM janela_area NATURAL JOIN area WHERE janela_id = '".$id."';";
      return $table = $this->getConexao()->executeQuery($sql);
    }
    public function obterAreasNoticia($id) {
      $sql = "SELECT area_id, are_descricao FROM noticia_area NATURAL JOIN area WHERE noticia_id = '".$id."';";
      return $table = $this->getConexao()->executeQuery($sql);
    }
    
    public function obterNoticiasArea($id) {
      $sql = "SELECT noticia_id, not_data, not_titulo FROM noticia NATURAL JOIN noticia_area 
              WHERE area_id = '".$id."' AND not_autorizada = '1' AND not_excluida = '0' 
              AND DATE(not_validade) > '2010-31-01' ORDER BY not_data DESC LIMIT 0,5;";
      return $table = $this->getConexao()->executeQuery($sql);
    }
    
    function obterComentarioAprovado($id){
	    // $sql = 'SELECT * FROM comentario WHERE com_exibe = 1 AND noticia_id = "'.$id.'" order by com_data desc ';
      // return $table = $this->getConexao()->executeQuery($sql);
      return false;
    }
    
    function indicar($nome, $email, $anome, $aemail, $id, $link, $linkOrigem) {
       
        $noticia = $this->obterNoticia($id);
        if ($noticia != null) {
            $para = $aemail;

            $headers = "MIME-Version: 1.0\r\n";
            $headers .="X-mailer: PHP/" . phpversion() . "\r\n";
            $headers .="X-Priority: 1\r\n";
            $headers .="Content-Type: text/html; charset=ISO-8859-1\r\n";
            $headers .="Reply-To: noticiadorweb@bisa.com.br\r\n";
            $headers .="To: $para\r\n";
            $headers .="From: Noticiador Web <noticiadorweb@bisa.com.br>\r\n";
            $headers .="Return-Path: noticiadorweb@bisa.com.br\r\n";

            $assunto = "Indicação de Notícia";
            $mensagem = "Olá <strong>" . $anome . "</strong>,<br />";
            $mensagem .= $nome . " lhe enviou a notícia abaixo por meio do noticiador web.<br /><br />";
            $mensagem .= '<h2>' . $noticia->not_titulo . '</h2>';
            $mensagem .= '<p>' . $noticia->not_chamada . '</p>';
			if ($linkOrigem != "")  {
				$mensagem .= '<p><strong>Link da Notícia no site de Origem</strong><br /><a href="' . $linkOrigem . '">' . $linkOrigem . '</a></p>';
			}
            $mensagem .= '<p><strong>Link da Notícia no NoticiadorWeb</strong><br /><a href="' . $link . '">' . $link . '</a></p>';

         //   echo $para. "|" . $assunto. "|" . $mensagem. "|" .$headers;
            
            
            if (mail($para, $assunto, $mensagem, $headers)) {
                print msg_alert("Notícia indicada!");
                print "<script>history.back();</script>";
            } else {
                print msg_alert("Não foi possível indicar essa notícia!");
                print "<script>history.back();</script>";
            }
        } else {
            print msg_alert("Não foi possível indicar essa notícia!");
            print "<script>history.back();</script>";
        }
    }
    
    function process(){
 	 	parent::process();
    
	 	switch($this->getAction()) {
	 		 case "indicar":
             $nome = $_POST["Inome"];
             $email = $_POST["Iemail"];
             $anome = $_POST["Ianome"];
             $aemail = $_POST["Iaemail"];
             $id = $_POST["Inoticia"];
             $link = $_POST["Ilink"];
             $linkOrigem = (isset($_POST["IlinkOrigem"])) ? $_POST["IlinkOrigem"] : "";
             $this->indicar($nome, $email, $anome, $aemail, $id, $link, $linkOrigem);
             break;
      }
    }

    public function pegarLinkCliente($id){
        
        $sql = "SELECT link_Noticias
                        FROM acrwclie
                        WHERE '$id' = CLIE_Codigo";

        
        
        $link = $this->getConexao()->executeQuery($sql);
        if ($link->RowCount() > 0) {
            
            return $link->getRow(0)->link_Noticias;
        }
        return "";
    }


}
?>
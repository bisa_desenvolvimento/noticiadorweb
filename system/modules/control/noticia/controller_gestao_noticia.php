<?
class controller_gestao_noticia extends TController {
	function show() {
			$view = new view_gestao_noticia($this);
			$view->show();	
	}
	
	function save($model){
		
	}
	
	function load($key){
		
	}
	
	function delete($key){
		
	}
	
	function create(){
		
	}

	function obterTodos($pag) {
		$usuario = controller_seguranca::getInstance()->identificarUsuario();	
        $sql = 'SELECT * FROM noticia inner join usuario on noticia.not_autor_id=usuario.usuario_id ';
		$sql .= ' where noticia_id is not null and not_excluida <> 1 ';
		if ($this->obterPerfilUsuario($usuario->getID())==PERFIL_COLABORADOR){
				$sql .= ' and not_autor_id = '.$usuario->getID();
				$sql .= ' and not_autorizada <> 1';
/*		}else{
			$sql .= ' and not_autor_id in ( select usuario_id from AREA_USUARIO where area_id in (select area_id from AREA_USUARIO where usuario_id='.$usuario->getID().'))';
		}*/		
		}else{
			$sql .= ' and (noticia_id in ( select noticia_id from noticia_area where area_id in (select area_id from area_usuario where usuario_id='.$usuario->getID().')) or (not_autor_id ='.$usuario->getID().'))';
		}

		if ((isset($_REQUEST['FDataInicial']))&&($_REQUEST['FDataInicial']!="")){
			$sql .= " AND not_data >='".DateConvert($_REQUEST['FDataInicial'])."'";
		}
		if (isset($_REQUEST['FDataFinal']) &&($_REQUEST['FDataFinal']!="")){
			$sql .= " AND not_data <='".DateConvert($_REQUEST['FDataFinal'])."'";		
		}
		if ((isset($_REQUEST['FMes']))&&($_REQUEST['FMes']!="0")){
			$sql .= " AND substr(not_data,1,4) ='". substr($_REQUEST['FMes'],3,4)."' and substr(not_data,6,2)='". substr($_REQUEST['FMes'],0,2) ."'";
		}
		if ((isset($_REQUEST['FArea']))&&($_REQUEST['FArea']!='0')){
		    $_SESSION['FArea'] = $_REQUEST['FArea'];
			$sql .= " AND noticia_id in (select noticia_id from noticia_area where area_id = ". $_REQUEST['FArea'] .")";
		}
        if ((isset($_SESSION['FArea']))&&($_SESSION['FArea']!='0')){
			$sql .= " AND noticia_id in (select noticia_id from noticia_area where area_id = ". $_SESSION['FArea'] .")";
		}
		
		if ((isset($_REQUEST['FPalavra']))&&($_REQUEST['FPalavra']!="")){

            $palavra = preg_replace("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/i","",$_REQUEST['FPalavra']);
            $palavra = trim($palavra);
            $palavra = strip_tags($palavra);
            $palavra = addslashes($palavra);

            $sql .= " AND (not_titulo LIKE '%".$palavra."%'";
	        $sql .= " OR not_chamada LIKE '%".$palavra."%'";
	        $sql .= " OR not_texto LIKE '%".$palavra."%')";
		}
        
        $total = $this->getConexao()->executeQuery($sql);
        $_SESSION['totalnoticias'] = $total->RowCount(); 
        
        $pag = NOTICIAS_PAGINA*$pag-NOTICIAS_PAGINA;
        $sql .= ' ORDER BY noticia_id DESC LIMIT '.$pag.','.NOTICIAS_PAGINA;
		echo $sql;
        $table = $this->getConexao()->executeQuery($sql);
        
        if ($table->RowCount() > 0) {
        	return $table;
        }
        return null;
    }
	
	function obterAreas() {
		$usuario = controller_seguranca::getInstance()->identificarUsuario();
        $sql = 'SELECT * FROM area ';
		$sql .= ' where area_id in (SELECT 
                        area_id 
                      FROM
                        vw_usuarios_areas_clientes 
                      WHERE tipo = "C" 
                        AND usuario_id = '.$usuario->getID().')'; // mudar para pegar a sessao do usuario

        $table_area = $this->getConexao()->executeQuery($sql);

        if ($table_area->RowCount() > 0) {
        	return $table_area;
        }
        
        return null;
    }
	
	function obterNoticiaArea($noticia_id, $area_id){
		$usuario = controller_seguranca::getInstance()->identificarUsuario();

        $sql = 'SELECT * FROM noticia_area where noticia_id='.$noticia_id.' and area_id='.$area_id;

        $noticia_area = $this->getConexao()->executeQuery($sql);

        if ($noticia_area->RowCount() > 0){
			if((!$this->obterNoticiaAutorizada($noticia_id))&&($this->obterPerfilUsuario($usuario->getID())==PERFIL_COLABORADOR)){
	        	return " checked ";
			}
			elseif(($this->obterNoticiaAutorizada($noticia_id))&&($this->obterPerfilUsuario($usuario->getID())!=PERFIL_COLABORADOR)){
					return " checked ";
			}
        }
        
        return null;
    }

	function obterPerfilUsuario($usuario){

		$sql = "SELECT * FROM perfil_usuario WHERE usuario_id= ".$usuario;

		$perfil_usuario = $this->getConexao()->executeQuery($sql);

		if ($perfil_usuario->RowCount() > 0) {
			$row_perfil = $perfil_usuario->getRow(0);
			return $row_perfil->perfil_id;		
		}
		return null;		

	}

	function obterNoticiaAutorizada($noticia){

		$sql = "SELECT not_autorizada FROM noticia WHERE noticia_id= ".$noticia;

		$noticia = $this->getConexao()->executeQuery($sql);

		if ($noticia->RowCount() > 0) {
			$row_noticia = $noticia->getRow(0);
			if ($row_noticia->not_autorizada != 1){
				return false;
			}else{
				return true;
			}
		}
		return true;		

	}



	// $delete = array de noticias da consulta tela de gestao de noticias
	// $insert = array de todas as noticias com suas respectivas areas selecionadas
	function atualizarAreas($delete, $insert) {
		$usuario = controller_seguranca::getInstance()->identificarUsuario();

		// deleta todas as noticias contidas na tela de consulta
		for($i=0;$i<sizeof($delete);$i++){
	        $sql = 'delete from noticia_area where noticia_id='.$delete[$i];
			$this->getConexao()->executeNonQuery($sql);
		}
		
		// inseri todas as areas selecionadas para cada noticia existente na consulta
		for($i=0;$i<sizeof($insert);$i++){		
			$inserir = explode("_",$insert[$i]);			

			if ($this->obterPerfilUsuario($usuario->getID())!=PERFIL_COLABORADOR){
				$this->atualizaNoticiaAutorizada($inserir[1]);
			}

			$sql = 'insert into noticia_area (noticia_id, area_id) values('.$inserir[1].','.$inserir[0].')';
			$this->getConexao()->executeNonQuery($sql);
		}

    }

	function excluirNoticias($delete) {
		// marca como excluida as noticias selecionadas
		for($i=0;$i<sizeof($delete);$i++){			
	        $sql = 'update noticia set not_excluida=1 where noticia_id='.$delete[$i];
			$this->getConexao()->executeNonQuery($sql);
		}
    }

	function atualizaNoticiaAutorizada($noticia_id) {
		// marca como autorizadas as noticias
	        $sql = 'update noticia set not_autorizada=1 where noticia_id='.$noticia_id;
			$this->getConexao()->executeNonQuery($sql);
    }

	function obterDatasNoticias() {	
		$sql = "SELECT distinct concat(substr(not_data,6,2),'/',substr(not_data,1,4))meses FROM noticia ";
      	$data_noticia = $this->getConexao()->executeQuery($sql);

        if ($data_noticia->RowCount() > 0) {
        	return $data_noticia;
        }
        return null;
	}
    
    ## Novas funções
    
    function obterNoticias($pag) {
        $usuario = controller_seguranca::getInstance()->identificarUsuario();
        
        $sql = 'SELECT 
                  n.noticia_id,
                  n.not_titulo,
                  n.not_data,
                  n.not_validade,
                  n.not_publicacao,
                  n.not_autorizada,
                  n.not_acessos,
                  u.usu_nome,
                  MAX(na.not_aut) AS autorizada 
                FROM
                  noticia n
                  INNER JOIN usuario u 
                    ON n.not_autor_id = u.usuario_id
                  INNER JOIN noticia_area na 
                    ON n.noticia_id = na.noticia_id  
                WHERE n.not_excluida = 0 ';
        
        
        if($this->obterPerfilUsuario($usuario->getID()) == PERFIL_COLABORADOR) {
			$sql .= ' AND not_autor_id = '.$usuario->getID();
		}else {
			$sql .= 'AND na.area_id IN 
                      (SELECT 
                        area_id 
                      FROM
                        vw_usuarios_areas_clientes 
                      WHERE tipo = "C" 
                        AND usuario_id = '.$usuario->getID().') ';
						
		}
         
        if ((isset($_REQUEST['FDataInicial']))&&($_REQUEST['FDataInicial']!="")){
			$sql .= " AND not_data >= '".DateConvert($_REQUEST['FDataInicial'])."'";
			
		}
		
        if (isset($_REQUEST['FDataFinal']) &&($_REQUEST['FDataFinal']!="")){
			$sql .= " AND not_data <= '".DateConvert($_REQUEST['FDataFinal'])."'";		
		}

		if ((isset($_REQUEST['FArea']) && $_REQUEST['FArea']!='0') || (isset($_SESSION['FArea'])) ) {
		
       // if (isset($_REQUEST['FArea'])&&$_REQUEST['FArea']!='0') {


       //    if (!isset($_REQUEST['FArea'])){ 
       //       $_REQUEST['FArea'] =  $_SESSION['FArea'];            
       //      }

       //      if ($_REQUEST['FArea']!='0') {
    			// $_SESSION['FArea'] = $_REQUEST['FArea'];
       //          $sql .= " AND n.noticia_id IN 
       //                    (SELECT 
       //                      noticia_id 
       //                    FROM
       //                      noticia_area 
       //                    WHERE area_id = ". $_REQUEST['FArea'] .")";
       //      } else {
       //          $_SESSION['FArea'] = $_REQUEST['FArea'];
       //      }
			  if (!isset($_REQUEST['FArea'])){ 
             $_REQUEST['FArea'] =  $_SESSION['FArea'];            
            }

            if ($_REQUEST['FArea']!='0')  {             
          $_SESSION['FArea'] = $_REQUEST['FArea'];
                $sql .= " AND n.noticia_id IN 
                          (SELECT 
                            noticia_id 
                          FROM
                            noticia_area 
                          WHERE area_id = ". $_REQUEST['FArea'] .")";
            } else {
                $_SESSION['FArea'] = $_REQUEST['FArea'];
            }
		}
        
  //       if (isset($_REQUEST['FPalavra']) && $_REQUEST['FPalavra']!=""){   
  //           $palavra = preg_replace("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/i","",$_REQUEST['FPalavra']);
  //           $palavra = trim($palavra);
  //           $palavra = strip_tags($palavra);
  //           $palavra = addslashes($palavra);
  //           $array = explode(" ",$_REQUEST['FPalavra']);

  //          	if(isset($_REQUEST['FExato']) && $_REQUEST['FExato'] != null) {
		// 		$sql .= " AND LOCATE('".htmlentities($_REQUEST['FPalavra'], ENT_QUOTES, 'utf-8')."', not_titulo)";
		// 		$sql .= " OR LOCATE('".htmlentities($_REQUEST['FPalavra'], ENT_QUOTES, 'utf-8')."', not_chamada)";
		// 		$sql .= " OR LOCATE('".htmlentities($_REQUEST['FPalavra'], ENT_QUOTES, 'utf-8')."', not_texto)";
		// 	} else {
		// 		$sql .=	"AND (not_titulo LIKE '%".trim($_REQUEST['FPalavra'])."%' ";	
		// 		$sql .=	"OR not_chamada LIKE '%".trim($_REQUEST['FPalavra'])."%' ";
		// 		$sql .=	"OR not_texto LIKE '%".trim($_REQUEST['FPalavra'])."%' ";
		// 		if(sizeof($array) >= 1) {
		// 			for($i=0; $i<sizeof($array); $i++) {
		// 				$sql .=	" OR not_titulo LIKE '%".$array[$i]."%'";
		// 				$sql .=	" OR not_chamada LIKE '%".$array[$i]."%'";
		// 				$sql .=	" OR not_texto LIKE '%".$array[$i]."%'";
		// 			}
		// 		}
		// 		$sql .= ")";
		// 	}
		// }
		if (isset($_REQUEST['FPalavra']) && $_REQUEST['FPalavra']!=""){   
            $palavra = preg_replace("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/i","",$_REQUEST['FPalavra']);
            $palavra = trim($palavra);
            $palavra = strip_tags($palavra);
            $palavra = addslashes($palavra);
            //$palavra = utf8_decode($palavra);
            $array = explode(" ",$_REQUEST['FPalavra']);

            //die($palavra);

            if(isset($_REQUEST['FExato']) && $_REQUEST['FExato'] != null) {
		        $sql .= " AND (LOCATE('".$palavra."', not_titulo)";
		        $sql .= " OR LOCATE('".$palavra."', not_chamada)";
		        $sql .= " OR LOCATE('".$palavra."', not_texto))";
		      } else {
		        $sql .= "AND (not_titulo LIKE '%".trim($_REQUEST['FPalavra'])."%' ";  
		        $sql .= "OR not_chamada LIKE '%".trim($_REQUEST['FPalavra'])."%' ";
		        $sql .= "OR not_texto LIKE '%".trim($_REQUEST['FPalavra'])."%' ";
		        if(sizeof($array) >= 1) {
		          for($i=0; $i<sizeof($array); $i++) {
		            $sql .= " OR not_titulo LIKE '%".$array[$i]."%'";
		            $sql .= " OR not_chamada LIKE '%".$array[$i]."%'";
		            $sql .= " OR not_texto LIKE '%".$array[$i]."%'";
		          }
		        }
		        $sql .= ")";
		      }
		    }

        $sql .= " GROUP BY n.noticia_id ORDER BY autorizada, n.noticia_id DESC";
        // mysqli_set_charset($this->getConexao(),'ISO-8859-1');    
			
        
		// var_dump($sql);
		
		

        $total = $this->getConexao()->executeQuery($sql);
        
		
        $_SESSION['totalnoticias'] = $total->RowCount(); 
        
        $pag = NOTICIAS_PAGINA*$pag-NOTICIAS_PAGINA;
        $sql .= ' LIMIT '.$pag.','.NOTICIAS_PAGINA;
        
        $table = $this->getConexao()->executeQuery($sql);
        
        if ($table->RowCount() > 0) {
        	return $table;
        }
        
        return null;
        
    }
    
    /*function obterAreasDoUsuario() { Função deslocada para o TController - Jorge Roberto Mantis 6383
        $usuario = controller_seguranca::getInstance()->identificarUsuario();
        
        $sql = 'SELECT 
					area_id,
					are_descricao,
					are_observacao 
                FROM
					`vw_usuarios_areas_clientes`
                WHERE usuario_id = '.$usuario->getID().' 
                	GROUP BY area_id 
                	ORDER BY are_descricao';
        
        $result = $this->getConexao()->executeQuery($sql);
        
        if ($result != null) {
            return $result;
        }
        return null;
    }*/

    /*Jorge Roberto - Mantis 1647

    Verifica se alguma área que o usuário é responsável possui algum comentário a ser aprovado */

    function verificarComentariosPendentes($id)
    {
       $sql =  "SELECT 
                  comentario.`comentario_id` 
                FROM
                  comentario 
                WHERE 
                   comentario.`com_dataaprovacao` IS NULL 
                   AND  com_exibe=1
                  AND comentario.`noticia_id` =".$id;

        $result = $this->getConexao()->executeQuery($sql);

        if ($result->RowCount() > 0) {
            return $result;
        }
        return null;


    }
}
?>
<?
class Controller_noticia extends TController {
	function show() {
		$view = new view_noticia($this);
		$view->show();}

	function save($model) {
        $controller_arquivo = new controller_arquivo();
        $controller_arquivo->setConexao(TConexao::getInstance());
        $url="";
        $area = null;
        


        $urlNoticia = "index.php?".PARAMETER_NAME_ACTION."=show&";
		$urlNoticia .= PARAMETER_NAME_FILE."=exibir_noticia";
		$urlNoticia .= "&noticia_id=" . $model->getnoticia_id();

        if(isset($_POST['area'])) {
            $area = array_unique($_POST['area']);
        }
        
        $idNews = $model->getnoticia_id();

        if ($model->getnoticia_id()== null) {
			// IkaroSales - 02-06-2016
			if(!strlen(stristr($model->getnot_link(), "https://")) > 0) {
				$urlNot = "https://".$model->getnot_link();
			} else {
				$urlNot = $model->getnot_link();
			}
			
			$sql = "INSERT INTO noticia(not_autor_id, not_titulo, not_data, not_publicacao, not_validade, not_chamada,not_texto, not_link, not_autorizada, not_thumbnail, not_link_abrir) VALUES ('".$model->getnot_autor_id()."', '".$model->getnot_titulo()."', '".$model->getnot_data()."', '".$model->getnot_publicacao()."', '".$model->getnot_validade()."', '".$model->getnot_chamada()."', '".$model->getnot_texto()."', '".$urlNot."',".$model->getnot_autorizada().",'".$model->getnot_thumbnail()."','".$model->getnot_link_abrir()."')";
            $id = "";
            $twitter = "";
        } else {
            $thumbnail = '';
            if($model->getnot_thumbnail()) {
                $thumbnail = ", not_thumbnail='".$model->getnot_thumbnail()."'";    
            }
	            $sql = "UPDATE noticia SET not_titulo='".$model->getnot_titulo()."', not_publicacao='".$model->getnot_publicacao()."', not_validade='".$model->getnot_validade()."', 
	                    not_chamada='".$model->getnot_chamada()."', not_texto='".$model->getnot_texto()."', not_link='".$model->getnot_link()."' , 
	                    not_autorizada=".$model->getnot_autorizada().$thumbnail.", not_link_abrir='". $model->getnot_link_abrir()."' WHERE  noticia_id= ".$model->getnoticia_id();
            
            $id = "&id=".$model->getnoticia_id();	

            $twitter = $model->getnoticia_id();
            
            $sqlDel = 'DELETE FROM noticia_area WHERE noticia_id = '.$model->getnoticia_id();
            $this->getConexao()->executeNonQuery($sqlDel);
        }

		if(isset($_POST['FLinkNoticia']) && $_POST['FLinkNoticia'] != NULL) {
			
			$error = $this->getConexao()->executeNonQuery($sql);
			$lastId = $this->getConexao()->getLastID();

			if($model->getnoticia_id() == null) {
				$sql  = "UPDATE noticia ";
				$sql .= "SET not_link = CONCAT('".URL.$urlNoticia.$lastId."') ";
				$sql .= "WHERE noticia_id = ".$lastId;				
				$error = $this->getConexao()->executeNonQuery($sql);
			} else {
				$sql  = "UPDATE noticia ";
				$sql .= "SET not_link = '".URL.$urlNoticia."' ";
				$sql .= "WHERE noticia_id = '".$model->getnoticia_id()."'";
				$error = $this->getConexao()->executeNonQuery($sql);
			}
	    } else {
			$error = $this->getConexao()->executeNonQuery($sql);
		}

        if ($error != -1) {
            if ($model->getnoticia_id()== null) {
                $sql = 'SELECT MAX(noticia_id) AS id, not_titulo as titulo FROM noticia GROUP BY titulo ORDER BY id DESC';
                $lastid = $this->getConexao()->executeQuery($sql);
                $lastid = $lastid->getRow(0);
                $lastId = $lastid->id;
                $lastTitulo = $lastid->titulo;
            } else {
                $lastId = $model->getnoticia_id();
                $lastTitulo = $model->getnot_titulo();
            }
            
            if($twitter == "") {
                $twitter = $lastId;
            }
            
            for($i=0;$i<sizeof($area);$i++){
                $sql = 'INSERT INTO noticia_area (noticia_id, area_id,not_aut) values('.$lastId.','.$area[$i].','.$model->getnot_autorizada(0).')';
                $error = $this->getConexao()->executeNonQuery($sql);
            }
            
            //$twitter=1;
            $_SESSION['twitter']=$twitter;
            
            if(isset($_POST['api'])){  
                echo msg_alert("Notícia salva com sucesso!");
                print "<script>window.close();</script>";
            } else {
                $url = "index.php?".PARAMETER_NAME_ACTION."=show&".PARAMETER_NAME_FILE."=gestao_noticia".$id;
                ##############################################################################################
                # Action ao terminar, ao termina todo o processo de cadastro ou alteração ele passa por aqui #
                ##############################################################################################
            }

            print "<script>window.location.href='".$url."'</script>";

        } else {
            print msg_alert("Registro não foi salvo.".$this->getConexao()->getErro());
        }
    }

	function load($key){
		$sql = "SELECT * FROM noticia WHERE noticia_id= ".$key;

		$table = $this->getConexao()->executeQuery($sql);
		if ($table->RowCount() > 0) {	
			$result = new noticia();
			$result->bind($table->getRow(0));
			if ($result != null){					
                $view = new view_noticia($this);
                $view->setModel($result);
                $view->show();
			}
		} 
		return null;
	}

	function delete($key){
		$sql = "DELETE FROM noticia WHERE  noticia_id = ".$key;
		$error = $this->getConexao()->executeNonQuery($sql);
		if ($error != -1) {
			$controller_arquivo = new controller_arquivo();
        	$controller_arquivo->setConexao(TConexao::getInstance());
        
			print msg_alert("Notícia excluída com sucesso!");
			$url = "index.php?".PARAMETER_NAME_ACTION."=show&";
			$url .= PARAMETER_NAME_FILE."=gestao_noticia";

			print "<script>parent.location.href='".$url."'</script>";//redirect($url);
		} else {
			print msg_alert("A notícia não pôde ser excluída. ".$this->getConexao()->getErro());
		}}

	function create(){ }

	function apagarImagem($imagem,$noticia){
	    $caminho = "users/public/thumb/";
        if (is_file($caminho.$imagem)){
            unlink(str_replace("/","\\",$caminho).$imagem);
        }
        	
        $sql = "UPDATE noticia SET not_thumbnail = '' WHERE  noticia_id = ".$noticia;
        if($this->getConexao()->executeNonQuery($sql)) {
            echo 'ok';    
        }}

	function selecaoMiniatura(){
?>
		<html>
			<head>
				<title></title>
				<link rel="stylesheet" href="scripts/jquery-ui-1.8.14.custom.css" type="text/css">
				<script type="text/javascript" src="scripts/jquery-1.5.1.min.js"></script>
				<script>
					function checarSelecaoMiniatura(strOrigem) {
						var notFThumbnailVal = '';
						var notThumbnailVBoxVal = '';

						notFThumbnailVal = $("#FThumbnailVal", window.parent.document).attr("value");
						notThumbnailVBoxVal = $("#cke_67_textInput", window.parent.document).attr("value");
						if (!notThumbnailVBoxVal)
							notThumbnailVBoxVal = $("#cke_69_textInput", window.parent.document).attr("value");

						notFThumbnailVal = jQuery.trim(notFThumbnailVal);
						notThumbnailVBoxVal = jQuery.trim(notThumbnailVBoxVal);

						var boolChecar = false;

						if (notFThumbnailVal != "" && notThumbnailVBoxVal != "")
							if (notFThumbnailVal == notThumbnailVBoxVal)
								boolChecar = true;
						if (boolChecar) {
							$("#ckbSelecionarMiniatura", window.parent.document).attr("checked", true);
						} else {
							$("#ckbSelecionarMiniatura", window.parent.document).attr("checked", false);
						}
					}
					var inputLastValue = "";
					function checarMudancaInput() {
						if ($("#cke_67_textInput", window.parent.document).length) {
							if ($("#cke_67_textInput", window.parent.document).attr("value") != inputLastValue) {
								inputLastValue = $("#cke_67_textInput", window.parent.document).attr("value");
								checarSelecaoMiniatura("checarMudancaInput");
							}
						} else {
							if ($("#cke_69_textInput", window.parent.document).length) {
								if ($("#cke_69_textInput", window.parent.document).attr("value") != inputLastValue) {
									inputLastValue = $("#cke_69_textInput", window.parent.document).attr("value");
									checarSelecaoMiniatura("checarMudancaInput");
								}
							}
						}
					}

					$(document).ready(
						function(){
							setInterval("checarMudancaInput()", 750);
							$("#ckbSelecionarMiniatura", window.parent.document).click(
								function(event) {
									event.preventDefault;
									var notFThumbnailVal = '';
									var notThumbnailVBoxVal = '';

									/*
									if ($('#ckbSelecionarMiniatura').attr('checked')) {

										alert("checado");

									} else {

										alert("deschecado");

									}
									*/

									notFThumbnailVal = $("#FThumbnailVal", window.parent.document).attr("value");
									notThumbnailVBoxVal = $("#cke_67_textInput", window.parent.document).attr("value");
									if (!notThumbnailVBoxVal)
										notThumbnailVBoxVal = $("#cke_69_textInput", window.parent.document).attr("value");

									//alert(notThumbnailVBoxVal);

									notFThumbnailVal = jQuery.trim(notFThumbnailVal);
									notThumbnailVBoxVal = jQuery.trim(notThumbnailVBoxVal);

									var boolAtribuirValor = true;
									if (notThumbnailVBoxVal != "") {
										if (notFThumbnailVal != "") {
											if (notFThumbnailVal == notThumbnailVBoxVal) {
												boolAtribuirValor = false;
											}
										}
									}/* Fix erro da miniatura else {
										alert("É necessário escolher uma imagem antes!");
										boolAtribuirValor = false;
									}*/
									if (boolAtribuirValor) {
										$("#ckbSelecionarMiniatura", window.parent.document).attr("checked", true);
										$("#FThumbnailVal", window.parent.document).attr("value", notThumbnailVBoxVal);

										var urlPluginThumbnail = "/plugins/timthumb.php";
										var paramURLThumbnail = "?src=https://noticiadorweb.com.br/users/public/thumb/";
										if (notThumbnailVBoxVal.indexOf("thumb_") == -1)
											paramURLThumbnail = "?src=https://noticiadorweb.com.br/";
										if (notThumbnailVBoxVal.indexOf("https://") != -1)
											paramURLThumbnail = "?src=";
										var paramDimensionsThumbnail = "&w=100&h=100";

										$("#imgFThumbnail", window.parent.document).attr(
											"src"
											, urlPluginThumbnail + paramURLThumbnail + notThumbnailVBoxVal + paramDimensionsThumbnail
										);

										$("#thumbY", window.parent.document).show();

									} else {
										$("#ckbSelecionarMiniatura", window.parent.document).attr("checked", false);
										$("#FThumbnailVal", window.parent.document).attr("value", "");
										$("#thumbY", window.parent.document).hide();
										$("#imgFThumbnail", window.parent.document).attr("src", "");
									}
								}
							);
						}
					);
				</script>
				<style>
				</style>
			</head>
			<body>
			</body>
		</html>

<?

	}

	function process(){
 		parent::process();
		switch($this->getAction()) {
			case "save":
			if (isset($_REQUEST["selecaoMiniatura"])) {
				$this->selecaoMiniatura();
			} else {
				$usuario = controller_seguranca::getInstance()->identificarUsuario();
				$save = new noticia();
				if (isset($_POST["id"])) {
					$save->setnoticia_id($_POST["id"]);
				} else {
					$save->setnoticia_id(null);
				}
				$save->setnot_autor_id($usuario->getID());
				$sql = "SELECT * FROM perfil_usuario WHERE usuario_id= ".$usuario->getID();
				$perfil_usuario = $this->getConexao()->executeQuery($sql);

				if ($perfil_usuario->RowCount() > 0) {
					$row_perfil = $perfil_usuario->getRow(0);
					if ($row_perfil->perfil_id==PERFIL_COLABORADOR) {
						$save->setnot_autorizada(0);
					} else {
						$save->setnot_autorizada(1);
					}
				} else {
					$save->setnot_autorizada(1);
				}
				$save->setnot_titulo(filtroTexto($_POST["FTitulo"]));
				$save->setnot_data(date("Y")."-".date("m")."-".date("d"));
				$save->setnot_publicacao(DateConvert($_POST["FDatapublicacao"]));
				$save->setnot_validade(DateConvert($_POST["FDatafinal"]));
				$save->setnot_chamada(filtroTexto($_POST["FResumomateria"]));
				$save->setnot_texto($this->getBanco()->real_escape_string($_POST["FMateriacompleta"]));
				if (isset($_POST["FLinkmateriacompleta"]) && $_POST["FLinkmateriacompleta"] != null){
					$save->setnot_link(filtroTexto($_POST["FLinkmateriacompleta"]));
					
				}
				
				$save->setnot_link_abrir($_POST["clientesN"]);

				$fullPathThumbnail = "";
				if (isset($_FILES["FThumbnail"])) {
					$foto = $_FILES["FThumbnail"];
					if (preg_match("/^image\/(pjpeg|jpeg|png|gif)$/", $foto["type"])) { // Verifica se o arquivo é uma imagem
						preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext); // Pega extensão da imagem
						$caminho = "users/public/thumb/";
						if (!is_dir($caminho)) {
							mkdir($caminho,777);
						}
						$nome_imagem = 'thumb_' . md5(uniqid(time())) . "." . $ext[1]; // Gera um nome único para a imagem
						$caminho_imagem = $caminho . $nome_imagem; // Caminho de onde ficará a imagem
						if (move_uploaded_file($foto["tmp_name"], $caminho_imagem)) {
							$save->setnot_thumbnail($nome_imagem);
						}
					}
				} else {
					$fullPathThumbnail = $_POST["FThumbnailVal"];
				}
				$save->setnot_thumbnail($fullPathThumbnail);
				$this->save($save);
			}
			break;
			case "apagarImagem":
				$imagem = $_POST["image"];
				$noticia = $_POST["noticia"];
				$this->apagarImagem($imagem, $noticia);
				break;
		}
	}

	public function upload_image($arquivo){
		global $ext_valida;
		#Instanciamos nossa classe   
		$upload        = new upload( $ext_valida, DIR_UPLOAD );   
		  
		#Verificamos se foi enviado algum arquivo   
		$resposta_upload = $upload -> arquivo( $arquivo );   
		#Verificamos se a extensão do arquivo está no array de extensões permitidas   
		$resposta_upload = $upload -> verifica_ext();   
		#Verificamos se o tamanho do arquivo é menor do que o permitido   
		$resposta_upload = $upload -> tamanho_k( UPLOAD_SIZE );   
		#Verificamos se o width de nosso arquivo é menor que o permitido   
		//$resposta_upload = $upload -> tamanho_x( LARGURA_IMAGE );   
		#Verificamos se o height de nosso arquivo é menor que o permitido   
		//$resposta_upload = $upload -> tamanho_y( ALTURA_IMAGE );    
		#Damos um nome para nosso arquivo   
		$resposta_upload = $upload -> arq_nome( 'noticia_'.time(), 'renomeia' );
		#Salvamos o arquivo   
		$resposta_upload = $upload -> salva();   
		#Finalizamos a operação   
		$resposta_upload = $upload -> finaliza();   
		
		#Mostrando uma mensagem agradavel para usuario ao final da transaçaõ   
		if(!$resposta_upload[0]){
				$des_url_foto = $upload ->arq_n_final;
				$des_url_thumbnail = $upload ->arq_n_final;
				criar_thumbnail(DIR_UPLOAD.$des_url_thumbnail,DIR_UPLOAD,LARGURA_THUMBNAIL,'thumb_',$upload ->retorna_ext());
				unlink(DIR_UPLOAD.$des_url_thumbnail);
				$des_url_thumbnail = 'thumb_'.$upload ->arq_n_final;
				return $des_url_thumbnail;
		} else {
			return false;			
		}
	}
  
    function obterAreasCadastradas($filtro = null) {
        $usuario = controller_seguranca::getInstance()->identificarUsuario();
        
        if (empty($filtro) || $filtro == "") {
            $sql = 'SELECT DISTINCT area.area_id, area.are_descricao, area.are_observacao FROM area, area_usuario
            WHERE AREA.area_id = area_usuario.area_id';
            if(isset($usuario)) {
                $sql .= ' AND area_usuario.usuario_id = '.$usuario->getID();
            }        
                $sql .= ' order by area.are_descricao LIMIT 0,25';	
        } else {
            $sql = "SELECT * FROM area WHERE $filtro ORDER BY are_descricao";	
        }
        
        $result = $this->getConexao()->executeQuery($sql);
        
        if ($result != null) {
            return $result;
        }
        
        return null;
    }

    /*function obterAreasDoUsuario($cliente = '', $usuario = '') {   Função deslocada para o TController - Jorge Roberto Mantis 6383
      //   $where = '';
      //   $sql = '';
      //   if($usuario == '') {
      //       $res = controller_seguranca::getInstance()->identificarUsuario();
      //       $usuario = $res->getID();
      //   }
      //   if($cliente != '') {
      //       $where .= ' AND Clie_Codigo = "'.$cliente.'"';
      //   }

      //   if ($res->getPerfil() == '2' || $res->getPerfil() == '3') {
      //   	$sql = ' SELECT area_id, are_descricao, are_observacao FROM `area`
					 // WHERE area_id IN (SELECT DISTINCT area_id FROM vw_area_usuario WHERE usuario_id = '.$usuario.')';
      //   }else{
      //   	$sql = 'SELECT
      //             area_id,
      //             are_descricao
      //           FROM
      //             vw_usuarios_areas_clientes
      //           WHERE usuario_id = '.$usuario.
      //           $where.'
      //           GROUP BY are_descricao';
      //   }

      //   $result = $this->getConexao()->executeQuery($sql);
      //   if ($result != null) {
      //       return $result;
      //   }
      //   return null;
    		$where = '';

		if($usuario == '') {
			$res = controller_seguranca::getInstance()->identificarUsuario();
			$usuario = $res->getID();
		}

		if($cliente != '') {
			$where .= ' AND Clie_Codigo = "'.$cliente.'"';
		}

		$sql = 'SELECT
				  area_id,
				  are_descricao,
				  are_observacao
				FROM
				  vw_usuarios_areas_clientes
				WHERE usuario_id = '.$usuario.
				$where.'
				ORDER BY are_descricao';

		$result = $this->getConexao()->executeQuery($sql);

		if ($result != null) {
			return $result;
		}

		return null;
    }*/

    function obterClientesDoUsuario($usuario = '') {
        
        if($usuario == '') {
            $res = controller_seguranca::getInstance()->identificarUsuario();
            $usuario = $res->getID();
        }

		

        $sql = 'SELECT CLIE_Codigo, CLIE_Nome 
		FROM vw_usuarios_areas_clientes 
		WHERE usuario_id = 1092 
		GROUP BY CLIE_Codigo, CLIE_Nome
		';


				


        // $sql = 'SELECT
        //           CLIE_Codigo,
        //           CLIE_Nome
        //         FROM
        //           vw_usuarios_areas_clientes
        //         WHERE usuario_id = '.$usuario.' AND CLIE_Codigo = '.$codigo.'
        //         GROUP BY CLIE_Nome';

        $result = $this->getConexao()->executeQuery($sql);

        if ($result != null) {
            return $result;
        }
        
        return null;
    
    }

    function obterClientesDoUsuario2($usuario = '') {

        if($usuario == '') {
            $res = controller_seguranca::getInstance()->identificarUsuario();
            $usuario = $res->getID();    
        }        
        
        $sql = 'SELECT 
                  CLIE_Codigo,
                  CLIE_Nome,
                  link_Noticias
                FROM
                  vw_usuarios_areas_clientes 
                WHERE usuario_id = '.$usuario.' 
                GROUP BY CLIE_Nome';
        
        $result = $this->getConexao()->executeQuery($sql);
        
        if ($result != null) {
            return $result;
        }
        
        return null;
    
    }

    function obterAreasDaNoticia($id) {
        $sql = 'SELECT 
                  area_id 
                FROM
                  noticia_area 
                WHERE noticia_id = '.$id;
                
        $result = $this->getConexao()->executeQuery($sql);
        $areas = array();
        foreach($result->Rows() as $area) {
            $areas[] = $area->area_id;
        }
        return $areas;
    }

    function obterLink($CLIE_Codigo){
 
    	$sql = 'SELECT link_Noticias
    			FROM acrwclie
    			WHERE  CLIE_Codigo = "'.$CLIE_Codigo.'"';
    	$result = $this->getConexao()->executeQuery($sql);
    	
    	return $result;
    }

    function obterNomeCliente($link){
    	$sql = 'SELECT CLIE_Nome
    			FROM acrwclie
    			WHERE  link_Noticias = "'.$link.'"';
    	$result = $this->getConexao()->executeQuery($sql);
    	
    	return $result;
    }
}
?>
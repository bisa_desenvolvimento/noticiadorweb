<?
class Controller_cadastrar_comentario extends TController
{
	function show() { 
		$view = new view_cadastrar_comentario($this);
		$view->show();
	}

	function save($model){
	}

	function load($key){ 
	}

	function delete($key){
	}

	function create(){ }
	
	function obterNoticia($noticia_id){
		$sql = "select * from noticia ";
		if ($noticia_id!=""){
			$sql .= " where noticia_id= ".$noticia_id;
		}
		$noticia= $this->getConexao()->executeQuery($sql);
		if ($noticia->RowCount() > 0) {
        	return $noticia;
        }

        return null;
	}

	function obterAreas($noticia_id) {
        $sql = 'SELECT * FROM noticia_area where noticia_id = '.$noticia_id;
        $table_area = $this->getConexao()->executeQuery($sql);
		
        if ($table_area->RowCount() > 0) {
        	return $table_area;
        }
        
        return null;
    }
    
    function inseriMailAssinanteArea($emailInserir, $areaInserir){
	
		    // INSERT
			$emails = explode(";",$emailInserir);

			$areas = $areaInserir;			
			if (sizeof($areas)){
				$error = "";
				for ($i=0;$i<sizeof($areas);$i++){	// para cada �rea selecionada
					$conta_invalidos = 0;	
					$conta_inseridos = 0;		
					for ($j=0;$j<sizeof($emails);$j++){ // para cada e-mail selecionado
	
						$email_assinante = $emails[$j];
						$senha = explode("@",$email_assinante);
						$senha = sha1($senha[0]);
						$inserido=0;
						$existente=1;	
						$retorno="";				
						
							
							// caso nao exista o email cadastrado
							$assinante_id = $this->checarEmail($email_assinante);
							if (!$assinante_id){ // inseri e-mail em "ASSINANTE"
								$sql = "INSERT INTO assinantes (ass_email, ass_senha, ass_confirmado, ass_excluido) 
										VALUES ('".$email_assinante.
										"', '".$senha.
										"', '1".
										"', '0')";
								$error = $this->getConexao()->executeNonQuery($sql);
								
								$conta_inseridos++;
								
								if ($error != -1) { // INSERI EM "ASSINANTE_AREA"
									$sql = "INSERT INTO assinante_area(assinante_id, area_id) 
											VALUES ('".$this->checarEmail($email_assinante).
											"', '".$areas[$i]."')";
										$error = $this->getConexao()->executeNonQuery($sql);
										$inserido=1;
										$existente=0;
								}
								
										
							}else{ // caso o e-mail j� exista no cadastro e nao exista na �rea selecionada inserir
								if (!$this->checarEmailArea($assinante_id, $areas[$i])){
									
									$sql = "INSERT INTO assinante_area(assinante_id, area_id) 
											VALUES ('".$this->checarEmail($email_assinante).
											"', '".$areas[$i]."')";
										$error = $this->getConexao()->executeNonQuery($sql);
										$inserido=1;
										$existente=0;
									
								}
								
							}
						//$retorno .= "<tr><td>".$this->getNomeArea($areas[$i])."<td>".$inserido."</td>"."<td>".$existente."</td>"."</td></tr>";
						
							
					} // fim do For de e-mails
					
				}// fim do For de �reas
				$emails_inseridos = 0;	
				$emails_validos = sizeof($emails) - $conta_invalidos;
				$emails_inseridos =  $conta_inseridos;
			
		
			if ($error != -1) {
			
				//$resultado = "<table><tr><td>".translate("Area")."<td>".translate("Incluidos")."</td><td>".translate("Existentes")."</td></td></tr>".$retorno."</table>";				
				//echo "<div id='emailsInseridos'>".$resultado."</div>";
				
			} else {
				print msg_alert("Registro n�o foi salvo.".$this->getConexao()->getErro());
			}
		}else{
			print msg_alert("Selecione uma �rea");			
		}

	
	}
	

	function checarEmail($email){
		$sql = "SELECT * FROM assinantes WHERE ass_email = '". $email ."'";
		
        $assinante = $this->getConexao()->executeQuery($sql);

        if ($assinante->RowCount() > 0) {
        	return $assinante->getRow(0)->assinante_id;
        }
        
        return null;
	}
	
	function checarEmailArea($assinante_id, $area_id){
		$sql = "SELECT * FROM assinante_area WHERE assinante_id = ". $assinante_id ." and area_id=".$area_id;
				
        $assinante = $this->getConexao()->executeQuery($sql);

        if ($assinante->RowCount() > 0) {
        	return true;
        }
        
        return false;
	}	

	
	function contarAcessos($noticia_id){
		$sql = "update noticia ";
		$sql .= " set not_acessos = not_acessos + 1";
		$sql .= " where noticia_id =".$noticia_id;

		$this->getConexao()->executeNonQuery($sql);
	}
	
	function tituloNoticia($noticia_id){
		$sql = "select not_titulo ";
		$sql .= " FROM noticia";
		$sql .= " where noticia_id =".$noticia_id;

		$titulo = $this->getConexao()->executeQuery($sql);
		
		if ($titulo->RowCount() > 0) {
        	return $titulo->getRow(0)->not_titulo;
        }
        
        return "";
	}
	
	function enviarComentario($noticia_id){
		$sql = "insert into comentario (com_nome, com_fone, com_email, com_comentario, noticia_id)";
		$sql .= " values ('".filtroTexto($_POST['FNome'])."','".filtroTexto($_POST['FFone'])."','".
					$_POST['FMail']."','".filtroTexto($_POST['FComentario'])."',".$noticia_id.")";


		$erro = $this->getConexao()->executeNonQuery($sql);
		
		$table_area = $this->obterAreas($noticia_id);	// obtem todas as areas relacionadas com a noticia	
		$areas="";
		if ($table_area != null){							
			for($j = 0; $j < $table_area->RowCount(); $j++) {
				$row_area = $table_area->getRow($j);
				$areas[$j]= $row_area->area_id;
			}
		}
		$this->inseriMailAssinanteArea($_POST['FMail'], $areas); // inseri o e-mail do comentarista nas areas da noticia, caso nao seja cadastrado.

		if($erro != -1)
			print msg_alert("Coment�rio inserido com sucesso!");
	}
	
	function obterComentarios($noticia_id) {
        $sql = 'SELECT * FROM comentario where com_exibe=1 and noticia_id = '.$noticia_id. " order by com_data desc";
        $table_comentario = $this->getConexao()->executeQuery($sql);
		
        if ($table_comentario->RowCount() > 0) {
        	return $table_comentario;
        }
        
        return null;
    }	

	
	function process(){
 		parent::process();

		switch($this->getAction()) {
			 case "save":
		}
	 }

}
?>
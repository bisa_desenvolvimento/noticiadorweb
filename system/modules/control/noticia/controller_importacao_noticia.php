<?
class controller_importacao_noticia extends TController
{

	//https://www.qualinfo.com.br/qualinfo/not_integracao.php?i=330

	function show() {
			$view = new View_importacao_noticia($this);
			$view->show();	
	}
	
	function save($dataInicial){
		$usuario = controller_seguranca::getInstance()->identificarUsuario();
		$this->importarNoticias($usuario->getID(), $dataInicial);
	}
	
	function load($key){
	}
	
	function delete($key){
		
	}
	
	function create(){
		
	}
	
	function process(){
 		parent::process();

		switch($this->getAction()) {
			 case "save":
				$this->save($_POST["FDataInicial"]);
				break;
		}
	 }

	function recuperarURL($usuario_id, $dataInicial = null) {
		//seleciona o site do cadastro do cliente
		$sql = "SELECT cliente.cli_site FROM cliente, cliente_usuario
				WHERE cliente.cliente_id = cliente_usuario.cliente_id
				  AND cliente_usuario.usuario_id = ".$usuario_id;
		$table = $this->getConexao()->executeQuery($sql);
		if ($table->RowCount() > 0) {
        	$row = $table->getRow(0);
        	$site = $row->cli_site;
        	//seleciona o código da última notícia inserida
        	$sql = "SELECT MAX(noticia_qualinfo.qualinfo_id) as qualinfo_id from noticia_qualinfo, noticia_area, area_usuario 
        			WHERE noticia_qualinfo.noticia_id = noticia_area.noticia_id 
        			  AND noticia_area.area_id = area_usuario.area_id
        			  AND area_usuario.usuario_id = ".$usuario_id;
        	$table = $this->getConexao()->executeQuery($sql);
        	if ($table->RowCount() > 0) {
        		$row = $table->getRow(0);
        		$ultimaNoticia = $row->qualinfo_id;
        	} 
        	if($ultimaNoticia == '') {
        		$ultimaNoticia = 0;
        	}
        	if(substr($site,0,7) != 'https://') {
        		$site = "https://".$site;
        	}
        //	$site = $site."/qualinfo/not_integracao.php?i=".$ultimaNoticia;
        //mudar
        	if($dataInicial != null){
        		$site = $site."/qualinfo/not_integracao.php?d=".DateConvert($dataInicial);
        	} else {
        		$site = $site."/qualinfo/not_integracao.php?i=".$ultimaNoticia;	
        	}
        	echo $site;
        	return $site;        	
        } else {
        	return null;
        }        		
	}
	
	function importarNoticias($usuario_id, $dataInicial = null) {
		$url = $this->recuperarURL($usuario_id, $dataInicial);		
		$xml_file = $url;
		
		$current = '';
		$in_item = FALSE;
		$id = '';
		$item = array();
		
		$codigo = '';
		$categoriacodigo = '';
		$categoriadescricao = '';
		$titulo = '';
		$introducao = '';
		$corpo = '';
		$datapublicacao = '';
		$cadastrodata = '';
		$cadastrohora = '';
		
	
		function openElements( $parser, $element, $attributes ) {
			global $current, $in_item;
		    if( $element == 'ITEM' ) {
		       $in_item = TRUE;
		    }	    
		    $current = $element;
		}
		
		function closeElements( $parser, $element ) {
			global $current, $in_item, $item, $codigo, $categoriacodigo , $categoriadescricao, $titulo, $introducao, $corpo, $datapublicacao, $cadastrodata, $cadastrohora;
		    if( $element == 'ITEM' ) {
		    	$subitem = array();
		    	$subitem["codigo"] = $codigo;
		    	$subitem["categoriacodigo"] = $categoriacodigo;
		    	$subitem["categoriadescricao"] = $categoriadescricao;
		    	$subitem["titulo"] = $titulo;
		    	$subitem["introducao"] = $introducao;
		    	$subitem["corpo"] = $corpo;
		    	$subitem["datapublicacao"] = $datapublicacao;
		    	$subitem["cadastrodata"] = $cadastrodata;
		    	$subitem["cadastrohora"] = $cadastrohora;
		    	$item[] =  $subitem;
		       	$in_item = FALSE;
		       	$codigo = '';
				$categoriacodigo = '';
				$categoriadescricao = '';
				$titulo = '';
				$introducao = '';
				$corpo = '';
				$datapublicacao = '';
				$cadastrodata = '';
				$cadastrohora = '';
		    } else {
		       $current = '';
		    }
		}
		
		function selectElements( $parser, $data ) {
			global $in_item, $id, $current, $codigo, $categoriacodigo , $categoriadescricao, $titulo, $introducao, $corpo, $datapublicacao, $cadastrodata, $cadastrohora;
			if( $in_item ) {
				if( $current == 'CODIGO' ) {
					$codigo = $data;
				} elseif ( $current == 'CATEGORIACODIGO' ) {
					$categoriacodigo = $data;
				} elseif ( $current == 'CATEGORIADESCRICAO' ) {
					$categoriadescricao = $categoriadescricao.$data;
				} elseif ( $current == 'TITULO' ) {
					$titulo = $titulo.$data;
				} elseif ( $current == 'INTRODUCAO' ) {
					$introducao = $introducao.$data;
				} elseif ( $current == 'CORPO' ) {
					$corpo = $corpo.$data;
				} elseif ( $current == 'DATAPUBLICACAO' ) {
					$datapublicacao = $data;
				} elseif ( $current == 'CADASTRODATA' ) {
					$cadastrodata = $data;
				} elseif ( $current == 'CADASTROHORA' ) {
					$cadastrohora = $data;
				}
		    }
		}		
		
		$parser    =    xml_parser_create();
		xml_set_element_handler( $parser, 'openElements', 'closeElements' );
		xml_set_character_data_handler( $parser, 'selectElements' );
		$xml_data    =    file( $xml_file );
		foreach( $xml_data as $data ) {
		    xml_parse( $parser, $data );
		}
		 
		global $item;
		$this->inserirDados($item, $usuario_id);   	   	
    }           
    
	function inserirDados($item, $usuario_id) {
		function substituirCaracteres($string) {
			$string = str_replace("'", "\'", $string);
			$string = str_replace(chr(142).chr(156), "á", $string); //á
			$string = str_replace(chr(142).chr(184), "é", $string); //é
			$string = str_replace(chr(142).chr(21), "ç", $string); //ç
			$string = str_replace(chr(142).chr(230), "õ", $string); //õ
			$string = str_replace(chr(142).chr(44), "ã", $string); //ã
			$string = str_replace(chr(142).chr(173), "ú", $string); //ú
			$string = str_replace(chr(142).chr(167), "ê", $string); //ê
			$string = str_replace(chr(142).chr(166), "ó", $string); //ó
			$string = str_replace(chr(142).chr(252), "í", $string); //í			
			$string = str_replace(chr(142).chr(45), "-", $string); //-
			return $string;
		}
		
		
	    foreach( $item as $value ) {
	    	$area_id = $this->recuperarIDArea($value['categoriacodigo']);
	    	if($area_id == null) {
	    		echo "area_id == null<br>";
	    		$area = new Area();
	    		$area->setDescricao($value['categoriadescricao']);
	    		$area_id = $this->inserirArea($area, $value['categoriacodigo']);
	    	}
	    	echo "area_id == ".$area_id."<br>";
	    	if($area_id != null) {
	    		$noticia = new noticia();
	    		$noticia->setnot_autor_id($usuario_id);	    		
	    		$noticia->setnot_chamada(substituirCaracteres($value['introducao']));
	    		$noticia->setnot_data($value['datapublicacao']);
	    		$noticia->setnot_texto(substituirCaracteres($value['corpo'] ));
	    		$noticia->setnot_titulo(substituirCaracteres($value['titulo']));
	    		$this->inserirNoticia($noticia, $value['codigo']);
	    	}
	    }
	}		
	
	function recuperarIDArea($codigoCategoria) {
		echo "recuperarIDArea<br>";
		$sql = "SELECT area_id FROM area_categoria WHERE categoria_id = ".$codigoCategoria;
		echo $sql."<br>";
		$table = $this->getConexao()->executeQuery($sql);
		if ($table->RowCount() > 0) {
			$row = $table->getRow(0);
			return $row->area_id;
		} else {
			return null;
		}
	}
    
    function inserirArea($area, $categoria_id) {
    	echo "inserirArea<br>";
	    $sql = "INSERT INTO area (are_descricao, are_observacao) VALUES ('".$area->getDescricao()."', '".$area->getObservacao()."')";
	    echo $sql."<br>";
	    $sucesso = $this->getConexao()->executeNonQuery($sql);
	    if($sucesso != -1) {
	    	$sql = "INSERT INTO area_usuario (area_id, categoria_id) VALUES (".$this->getConexao()->getLastID().", ".$categoria_id.")";
	    	echo $sql."<br>";
	    	$sucesso = $this->getConexao()->executeNonQuery($sql);
	    	return $this->getConexao()->getLastID();
	    } else {
	    	return null;
	    }
    }
    
    function inserirNoticia($noticia, $qualinfo_id) {
    	echo "inserirNoticia<br>";
    	$sql = "INSERT INTO noticia
    			(not_autor_id, not_titulo, not_data, not_validade, not_chamada, not_texto, not_link) 
    			VALUES 
    			('".$noticia->getnot_autor_id()."', '".$noticia->getnot_titulo()."', '".$noticia->getnot_data()."', '".$noticia->getnot_validade()."', '".$noticia->getnot_chamada()."', '".$noticia->getnot_texto()."', '".$noticia->getnot_link()."')";
    	echo $sql."<br>";
    	$sucesso = $this->getConexao()->executeNonQuery($sql);
	    if($sucesso != -1) {
	    	$sql = "INSERT INTO noticia_qualinfo (noticia_id, qualinfo_id) VALUES (".$this->getConexao()->getLastID().", ".$qualinfo_id.")";
	    	echo $sql."<br>";
	    	$sucesso = $this->getConexao()->executeNonQuery($sql);
	    } 	
    }

	

}
?>
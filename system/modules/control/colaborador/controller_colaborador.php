<?

class controller_colaborador extends TController {

    function show() {
        $view = new view_colaborador($this);
        $view->show();
    }

    function save($model) {

		$strComoSoubeOutros = "";

		$strComoSoubePOutros = "";

        if ($model->getusuario_id() == null) {

            // INSERT
            $area = null;
            if (!isset($_POST['area'])) {
                print msg_alert("Selecione uma �rea de Interesse!");
            } else {

				$strComoSoubeOutros = $model->getUSUA_ComoSoubeOutros();

				$strComoSoubePOutros = $model->getUSUA_ComoSoubePOutros();

                $area = $_POST['area'];

                if ($this->checarEmail($model->getUSUA_Email())) {

                    print msg_alert("O e-mail que voc� digitou foi encontrado no nosso cadastro verifique se voc� digitou corretamente o email ou digite outro email !!!");
                } else {

                    $sql = "INSERT INTO usuario(usu_email, usu_nome, usu_senha, usu_login,usu_idioma)";
                    $sql .=" VALUES ('" . $model->getUSUA_Email()
                            . "' , '" . $model->getUSUA_Nome() . "','"
                            . base64_encode($model->getUSUA_Senha()) . "','" . $model->getUSUA_Email() . "','" . $model->getUSUA_Idioma() . "')";

                    $error = $this->getConexao()->executeNonQuery($sql);
                    $usuario_id = $this->getUsuarioIdInserted($model->getUSUA_Email());

                    if (($error != -1) && ($usuario_id)) {
                        $sql = "INSERT INTO colaborador (
								usuario_id
								, USUA_ComoSoube
								, USUA_CompSoube
								, USUA_FlagEditor
								, USUA_Endereco 
								, USUA_Bairro
								, USUA_Cidade 
								, USUA_UF 
								, USUA_Fone
								, USUA_EmailEmpresa
								, USUA_Cargo 
								, USUA_Empresa 
								, USUA_ComoSoubeOutros
								, USUA_ComoSoubePOutros
								, USUA_CEP
								)";
                        $sql .=" VALUES ('" . $usuario_id
                                . "' , '" . $model->getUSUA_ComoSoube() . "','"
                                . $model->getUSUA_CompSoube() . "','0', '"
                                . $model->getUSUA_Endereco() . "', '"
                                . $model->getUSUA_Bairro() . "', '"
                                . $model->getUSUA_Cidade() . "' ,"
                                . $model->getUSUA_UF() . " , '"
                                . $model->getUSUA_Fone() . "','"
                                . $model->getUSUA_EmailEmpresa() . "', '"
                                . $model->getUSUA_Cargo() . "', '"
                                . $model->getUSUA_Empresa() . "','"
                                . $strComoSoubeOutros . "', '"
                                . $strComoSoubePOutros . "','"
                                . $model->getUSUA_CEP() . "')";

                        $error = $this->getConexao()->executeNonQuery($sql);
                        if ($error != -1) {
                            $sql = "INSERT INTO perfil_usuario(perfil_id, usuario_id)";
                            $sql .=" VALUES (2,'" . $usuario_id . "')"; /* .PERFIL_COLABORADOR}. */
                            $error = $this->getConexao()->executeNonQuery($sql);
                            if ($error != -1) {
                                for ($i = 0; $i < sizeof($area); $i++) {
                                    $sql = 'insert into area_usuario (usuario_id, area_id) values(' . $usuario_id . ',' . $area[$i] . ')';
                                    $error = $this->getConexao()->executeNonQuery($sql);
                                }
                                if ($error != -1) {
                                    $this->permissaoColaborador($usuario_id);
                                    print msg_alert("Registro salvo.");
                                    print "<script>parent.location.href='index.php'</script>";
                                } else {
                                    print msg_alert("Registro n�o foi salvo." . $this->getConexao()->getErro());
                                }
                            } else {
                                print msg_alert("Registro n�o foi salvo." . $this->getConexao()->getErro());
                            }
                        } else {
                            print msg_alert("Registro n�o foi salvo." . $this->getConexao()->getErro());
                        }
                    } else {
                        print msg_alert("Registro n�o foi salvo." . $this->getConexao()->getErro());
                    }
                }
            }
        } else {

			//$strComoSoubeOutros = $model->getUSUA_ComoSoubeOutros();

			//$strComoSoubePOutros = $model->getUSUA_ComoSoubePOutros();

            // UPDATE
            $sql = "UPDATE colaborador SET ";
            $sql .="USUA_ComoSoube = '" . $model->getUSUA_ComoSoube() . "'";
            $sql .=",USUA_CompSoube = '" . $model->getUSUA_CompSoube() . "'";
            $sql .=",USUA_FlagEditor = 0";
            $sql .=",USUA_Endereco = '" . $model->getUSUA_Endereco() . "'";
            $sql .=",USUA_Bairro = '" . $model->getUSUA_Bairro() . "'";
            $sql .=",USUA_Cidade = '" . $model->getUSUA_Cidade() . "'";
            $sql .=",USUA_UF = " . $model->getUSUA_UF();
            $sql .=",USUA_Fone = '" . $model->getUSUA_Fone() . "'";
            $sql .=",USUA_EmailEmpresa = '" . $model->getUSUA_EmailEmpresa() . "'";
            $sql .=",USUA_Cargo = '" . $model->getUSUA_Cargo() . "'";
            $sql .=",USUA_Empresa = '" . $model->getUSUA_Empresa() . "'";
            $sql .=",USUA_ComoSoubeOutros = '" . $strComoSoubeOutros . "'";
            $sql .=",USUA_ComoSoubePOutros = '" . $strComoSoubePOutros . "'";
            $sql .=",USUA_CEP = '" . $model->getUSUA_CEP() . "'";
            $sql .=" WHERE  usuario_id = '" . $model->getusuario_id() . "'";

            $error = $this->getConexao()->executeNonQuery($sql);
            if ($error != -1) {
                $sql = "UPDATE usuario SET ";
                $sql .=" usu_nome = '" . $model->getUSUA_Nome() . "',";
                $sql .=" usu_idioma = '" . $model->getUSUA_Idioma() . "'";
                if (isset($_POST["alterasenha"]) && ($_POST["alterasenha"] != "")) {
                    $sql .=",usu_senha = '" . base64_encode($model->getUSUA_Senha()) . "'";
                }
                $sql .=" WHERE  usuario_id = " . $model->getusuario_id();
                $error = $this->getConexao()->executeNonQuery($sql);
                if ($error != -1) {
                    $area = $_POST['area'];

                    $sql = 'DELETE FROM area_usuario WHERE usuario_id = ' . $model->getusuario_id();
                    $this->getConexao()->executeNonQuery($sql);

                    for ($i = 0; $i < sizeof($area); $i++) {
                        $sql = 'insert into area_usuario (usuario_id, area_id) values(' . $model->getusuario_id() . ',' . $area[$i] . ')';
                        $error = $this->getConexao()->executeNonQuery($sql);
                    }

                    print msg_alert("Registro salvo.");
                    print "<script>parent.location.href='index.php'</script>";

                    $_SESSION['SESSION_LANG'] = $model->getUSUA_Idioma();
                } else {
                    print msg_alert("Registro n�o foi salvo." . $this->getConexao()->getErro());
                }
            } else {
                print msg_alert("Registro n�o foi salvo." . $this->getConexao()->getErro());
            }
        }
//		echo $sql;		
//		exit;
    }

    function load($key) {
        $sql = "SELECT * FROM colaborador LEFT JOIN usuario ON colaborador.usuario_id= usuario.usuario_id where usuario.usuario_id='" . $key . "'";
        $table = $this->getConexao()->executeQuery($sql);
        if ($table->RowCount() > 0) {

            $result = new colaborador();
            $result->bind($table->getRow(0));
            if ($result != null) {
                $view = new view_colaborador($this);
                $view->setModel($result);
                $view->show();
            }
        }
        return null;
    }

    function delete($key) {
        $sql = "DELETE * FROM colaborador WHERE  USUA_Email= " . $key;
        $this->getConexao()->executeNonQuery($sql);
    }

    function create() {

    }

    function process() {
        parent::process();

        switch ($this->getAction()) {
            case "save":

                $save = new COLABORADOR();
                if (isset($_POST["id"]))
                    $save->setusuario_id($_POST["id"]);

                $save->setUSUA_Email($_POST["FUSUA_Email"]);
                if (isset($_POST["FUSUA_ComoSoube"]))
                    $save->setUSUA_ComoSoube(filtroTexto($_POST["FUSUA_ComoSoube"]));
                if (isset($_POST["FUSUA_CompSoube"]))
                    $save->setUSUA_CompSoube(filtroTexto($_POST["FUSUA_CompSoube"]));

                $save->setUSUA_Senha(filtroTexto($_POST["FUSUA_Senha"]));
                $save->setUSUA_Nome(filtroTexto($_POST["FUSUA_Nome"]));
                $save->setUSUA_Endereco(filtroTexto($_POST["FUSUA_Endereco"]));
                $save->setUSUA_Bairro(filtroTexto($_POST["FUSUA_Bairro"]));
                $save->setUSUA_Cidade(filtroTexto($_POST["FUSUA_Cidade"]));
                $save->setUSUA_Idioma(filtroTexto($_POST["FUSUA_Idioma"]));
                if (trim($_POST["FUSUA_UF"]) != "") {
                    $save->setUSUA_UF(filtroTexto($_POST["FUSUA_UF"]));
                } else {
                    $save->setUSUA_UF("null");
                }
                $save->setUSUA_Fone(filtroTexto($_POST["FUSUA_Fone"]));
                $save->setUSUA_EmailEmpresa($_POST["FUSUA_EmailEmpresa"]);
                $save->setUSUA_Cargo(filtroTexto($_POST["FUSUA_Cargo"]));
                $save->setUSUA_Empresa(filtroTexto($_POST["FUSUA_Empresa"]));
				$strComoSoubeOutros = "";
                if (isset($_POST["FUSUA_ComoSoubeOutros"]))
					if ($_POST["FUSUA_ComoSoubeOutros"] != "")
						$strComoSoubeOutros = filtroTexto($_POST["FUSUA_ComoSoubeOutros"]);
				$save->setUSUA_ComoSoubeOutros($strComoSoubeOutros);
				$strComoSoubePOutros = "";
                if (isset($_POST["FUSUA_ComoSoubePOutros"]))
					if ($_POST["FUSUA_ComoSoubePOutros"] != "")
						$strComoSoubePOutros = filtroTexto($_POST["FUSUA_ComoSoubePOutros"]);
				$save->setUSUA_ComoSoubePOutros($strComoSoubePOutros);
                $save->setUSUA_CEP($_POST["FUSUA_CEP"]);

				//print_r($save);

				//exit;

                $this->save($save);
//				 echo "aqui";
//				 exit;
                break;

            case "ajaxLista":
                $view = new view_colaborador($this);
                // Prepara o filtro por nome
                $filtro = "are_descricao LIKE '%" . $_GET['filtro'] . "%'";
                print $view->listarAreasCadastradas($filtro);
                break;
        }
    }

    function obterEstados($estado_id="") {
        $sql = 'SELECT * FROM estado ';
        if ($estado_id != "") {
            $sql .= 'where uf_id=' . $estado_id;
        }
        $estado = $this->getConexao()->executeQuery($sql);

        if ($estado->RowCount() > 0) {
            return $estado;
        }

        return null;
    }

    function checarEmail($email) {
        $sql = "SELECT * FROM usuario ";
        $sql .= "where usu_email='" . $email . "'";

        $emailobj = $this->getConexao()->executeQuery($sql);

        if ($emailobj->RowCount() > 0) {
            return true;
        }
        return false;
    }

    function getUsuarioIdInserted($email) {
        $sql = "SELECT usuario_id FROM usuario ";
        $sql .= "where usu_email='" . $email . "'";

        $emailobj = $this->getConexao()->executeQuery($sql);

        if ($emailobj->RowCount() > 0) {
            $row = $emailobj->getRow(0);
            return $row->usuario_id;
        }
        return false;
    }

    function obterAreas($usuario_id) {
        $sql = 'SELECT DISTINCT area.area_id, area.are_descricao FROM area, area_usuario
        		';
        if ($usuario_id) {
            $sql .= 'WHERE area_usuario.usuario_id = ' . $usuario_id;
        }
        $sql .= ' order by area.are_descricao';
        $table_area = $this->getConexao()->executeQuery($sql);

        if ($table_area->RowCount() > 0) {
            return $table_area;
        }

        return null;
    }

    function obterAreasCadastradas($filtro = "") {
        $usuario = controller_seguranca::getInstance()->identificarUsuario();

        if (empty($filtro) || $filtro == "") {
            $sql = 'SELECT DISTINCT area.area_id, area.are_descricao, area.are_observacao FROM area, area_usuario
        		WHERE area.area_id = area_usuario.area_id';
            if (isset($usuario)) {
                $sql .= ' AND area_usuario.usuario_id = ' . $usuario->getID();
            }
            $sql .= ' order by area.are_descricao';
        } else {

            $sql = "SELECT DISTINCT area.area_id, area.are_descricao, area.are_observacao FROM area, area_usuario
        		WHERE $filtro  ORDER BY area.are_descricao ";
            //$sql = "SELECT * FROM AREA WHERE ORDER BY are_descricao";
        }



//         if (empty($filtro) || $filtro == "") {
//
//            if(($usuario->getPerfil()!=3)&&isset($usuario)){
//            $sql = 'SELECT DISTINCT AREA.area_id, AREA.are_descricao, AREA.are_observacao FROM AREA, AREA_USUARIO
//        		WHERE AREA.area_id = AREA_USUARIO.area_id';
//            }else{
//                $sql .= ' AND AREA_USUARIO.usuario_id = ' . $usuario->getID();
//            }
//            $sql .= ' order by AREA.are_descricao';
//        } else {
//
//            $sql = "SELECT DISTINCT AREA.area_id, AREA.are_descricao, AREA.are_observacao FROM AREA, AREA_USUARIO
//        		WHERE $filtro  ORDER BY AREA.are_descricao ";
//            //$sql = "SELECT * FROM AREA WHERE ORDER BY are_descricao";
//        }


        $result = $this->getConexao()->executeQuery($sql);

        if ($result != null) {
            return $result;
        }

        return null;
    }

    function obterAreasNaoSelecionadas($filtro = null) {
        $usuario = controller_seguranca::getInstance()->identificarUsuario();

        if (empty($filtro) || $filtro == "") {
            $sql = 'SELECT DISTINCT area.area_id, area.are_descricao, area.are_observacao FROM area, area_usuario
        		';
            if (isset($usuario)) {
                $sql .= 'WHERE area_usuario.usuario_id != ' . $usuario->getID();
            }
            $sql .= ' ORDER BY area.are_descricao';
        } else {
            $sql = "SELECT * FROM area WHERE $filtro ORDER BY are_descricao";
        }

        $result = $this->getConexao()->executeQuery($sql);

        if ($result != null) {
            return $result;
        }

        return null;
    }

    private function permissaoColaborador($usuario_id) {

        $sql = 'INSERT INTO permissoes (permissao_id, usuario_id, acao_id, per_arquivo) VALUES
                         (NULL , ' . $usuario_id . ', 1, "alterar_areas_noticia"),
                         (NULL , ' . $usuario_id . ', 2, "alterar_areas_noticia"),
                         (NULL , ' . $usuario_id . ', 3, "alterar_areas_noticia"),
                         (NULL , ' . $usuario_id . ', 4, "alterar_areas_noticia"),
                         (NULL , ' . $usuario_id . ', 5, "alterar_areas_noticia"),
                         (NULL , ' . $usuario_id . ', 1, "assinante"),
                         (NULL , ' . $usuario_id . ', 2, "assinante"),
                         (NULL , ' . $usuario_id . ', 3, "assinante"),
                         (NULL , ' . $usuario_id . ', 4, "assinante"),
                         (NULL , ' . $usuario_id . ', 5, "assinante"),
                         (NULL , ' . $usuario_id . ', 1, "assinar_newsletter"),
                         (NULL , ' . $usuario_id . ', 2, "assinar_newsletter"),
                         (NULL , ' . $usuario_id . ', 3, "assinar_newsletter"),
                         (NULL , ' . $usuario_id . ', 4, "assinar_newsletter"),
                         (NULL , ' . $usuario_id . ', 5, "assinar_newsletter"),
                         (NULL , ' . $usuario_id . ', 1, "cadastrar_comentario"),
                         (NULL , ' . $usuario_id . ', 2, "cadastrar_comentario"),
                         (NULL , ' . $usuario_id . ', 3, "cadastrar_comentario"),
                         (NULL , ' . $usuario_id . ', 4, "cadastrar_comentario"),
                         (NULL , ' . $usuario_id . ', 5, "cadastrar_comentario"),
                         (NULL , ' . $usuario_id . ', 6, "cadastrar_comentario"),
                         (NULL , ' . $usuario_id . ', 10, "cadastrar_comentario"),
                         (NULL , ' . $usuario_id . ', 1, "gestao_colaborador"),
                         (NULL , ' . $usuario_id . ', 4, "gestao_colaborador"),
                         (NULL , ' . $usuario_id . ', 1, "gestao_noticia"),
                         (NULL , ' . $usuario_id . ', 2, "gestao_noticia"),
                         (NULL , ' . $usuario_id . ', 3, "gestao_noticia"),
                         (NULL , ' . $usuario_id . ', 4, "gestao_noticia"),
                         (NULL , ' . $usuario_id . ', 5, "gestao_noticia"),
                         (NULL , ' . $usuario_id . ', 1, "girar_noticia"),
                         (NULL , ' . $usuario_id . ', 2, "girar_noticia"),
                         (NULL , ' . $usuario_id . ', 3, "girar_noticia"),
                         (NULL , ' . $usuario_id . ', 4, "girar_noticia"),
                         (NULL , ' . $usuario_id . ', 5, "girar_noticia"),
                         (NULL , ' . $usuario_id . ', 1, "indicar_noticia"),
                         (NULL , ' . $usuario_id . ', 2, "indicar_noticia"),
                         (NULL , ' . $usuario_id . ', 3, "indicar_noticia"),
                         (NULL , ' . $usuario_id . ', 4, "indicar_noticia"),
                         (NULL , ' . $usuario_id . ', 5, "indicar_noticia"),
                         (NULL , ' . $usuario_id . ', 6, "indicar_noticia"),
                         (NULL , ' . $usuario_id . ', 10, "indicar_noticia"),
                         (NULL , ' . $usuario_id . ', 4, "inserir_email"),
                         (NULL , ' . $usuario_id . ', 1, "noticia"),
                         (NULL , ' . $usuario_id . ', 2, "noticia"),
                         (NULL , ' . $usuario_id . ', 3, "noticia"),
                         (NULL , ' . $usuario_id . ', 4, "noticia"),
                         (NULL , ' . $usuario_id . ', 5, "noticia"),
                         (NULL , ' . $usuario_id . ', 3, "remover_usuario_email"),
                         (NULL , ' . $usuario_id . ', 4, "remover_usuario_email"),
                         (NULL , ' . $usuario_id . ', 1, "visualizar_noticia"),
                         (NULL , ' . $usuario_id . ', 2, "visualizar_noticia"),
                         (NULL , ' . $usuario_id . ', 3, "visualizar_noticia"),
                         (NULL , ' . $usuario_id . ', 4, "visualizar_noticia"),
                         (NULL , ' . $usuario_id . ', 5, "visualizar_noticia")';

        $this->getConexao()->executeNonQuery($sql);
    }

    function trocaIdioma($idioma, $usuario) {

        $query = "UPDATE usuario SET usu_idioma='" . $idioma . "' WHERE usuario_id='" . $usuario . "'";

        $this->getConexao()->executeNonQuery($query);
    }

}

?>
<?

class controller_gestao_colaborador extends TController {

    function process() {
        parent::process();
        
        switch ($this->getAction()) {
            case "ajaxLista":
                $this->obterAreasNaoSelecionadas();
                $view = new view_gestao_colaborador($this);
                $filtro = "are_descricao LIKE '%" . $_GET['filtro'] . "%'";
                print $view->listarAreasNaoSelecionadas($filtro);
            break;
            
            case "enviarsenha":
                $this->enviarSenha($_GET['email'],$_GET['senha'],$_GET['usuario']);
                echo msg_alert("Senha enviada pra seu email");
            break;
        }
    
    }

    function show() {
        $view = new view_gestao_colaborador($this);
        $view->show();	
    }
	

	function save($model){}
	

	function load($key){}
	

	function delete($key){
        $sql = "DELETE FROM usuario WHERE usuario_id = $key";
        $this->getConexao()->executeNonQuery($sql);
        $sql = "DELETE FROM cliente_usuario WHERE usuario_id = $key";
        $this->getConexao()->executeNonQuery($sql);
        $sql = "DELETE FROM area_usuario WHERE usuario_id = $key";
        $this->getConexao()->executeNonQuery($sql);
        $sql = "DELETE FROM perfil_usuario WHERE usuario_id = $key";
        $this->getConexao()->executeNonQuery($sql);
        
        print msg_alert("Registro removido.");
        $url = "index.php?".PARAMETER_NAME_ACTION."=show&";
		$url .= PARAMETER_NAME_FILE."=gestao_colaborador";
		print "<script>parent.location.href='".$url."'</script>";//redirect($url);
	}
	

	function create(){}
    	

    function obterTodos($perfil,$usuario_id='') {
    
        if($perfil == 1) {
            $sql = 'SELECT DISTINCT u.* FROM usuario u INNER JOIN perfil_usuario p ON u.usuario_id = p.usuario_id ';
            $sql .= 'WHERE u.usu_excluido <> 1';
            
            if ((isset($_REQUEST['FUSUA_Email']))&&($_REQUEST['FUSUA_Email']!="")) {
                $sql .= " AND u.usu_email ='".$_REQUEST['FUSUA_Email']."'";
            }
            
            if ((isset($_POST['FUSUA_Perfil']))&&($_POST['FUSUA_Perfil']!="")) {
                $sql = 'SELECT u.* FROM usuario u INNER JOIN perfil_usuario p ON u.usuario_id = p.usuario_id ';
                $sql .= 'WHERE u.usu_excluido <> 1 AND p.perfil_id IN ('.$perfil.')';
            }
        }
         
        if($perfil == 3) {
            $sql = "SELECT * FROM usuario WHERE usuario_id IN (SELECT DISTINCT(usuario_id) FROM area_usuario WHERE area_id IN (SELECT area_id FROM area_usuario WHERE usuario_id = '".$usuario_id."')) AND usu_perfil IN (2,3) AND usu_excluido <> 1";
            if ((isset($_REQUEST['FUSUA_Email']))&&($_REQUEST['FUSUA_Email']!="")) {
                $sql .= " AND usu_email ='".$_REQUEST['FUSUA_Email']."'";
            }  
        }
        $table = $this->getConexao()->executeQuery($sql);

        if ($table->RowCount() > 0) {
            return $table;
        }        
        return null;
    }
	

    function obterAreas($usuario_id) {
        $sql = 'SELECT area.* FROM area, area_usuario
                WHERE area.area_id = area_usuario.area_id
                AND area_usuario.usuario_id = '.$usuario_id.'		
                order by are_descricao';
        $table_area = $this->getConexao()->executeQuery($sql);

        if ($table_area->RowCount() > 0) {
        return $table_area;
        }
        return null;
    }
	

	function obterPerfis() {
        $sql = 'SELECT * FROM perfil';
        $table_perfil = $this->getConexao()->executeQuery($sql);
        
        if ($table_perfil->RowCount() > 0) {
        return $table_perfil;
        }
        return null;
    }
	

	function obterComboPerfis() {
    
        $usuario = controller_seguranca::getInstance()->identificarUsuario();  
        $usuario_id = $usuario->getID();          
        $cod = $this->PegarPerfil($usuario_id);
        
        if ($cod== 1 || $cod==4) {
            $sql = 'SELECT * FROM perfil ';
            $table_perfil = $this->getConexao()->executeQuery($sql);
            if ($table_perfil->RowCount() > 0) {
                return $table_perfil;
            }
        }
        
        if ($cod== 2 || $cod==5) {
            $sql = 'SELECT * FROM perfil where perfil_id=2 OR perfil_id=5';
            $table_perfil = $this->getConexao()->executeQuery($sql);
            if ($table_perfil->RowCount() > 0) {
                return $table_perfil;
            }
        }
        
        if ($cod==3) {
            $sql = 'SELECT * FROM perfil where perfil_id=2 OR perfil_id=3';
            $table_perfil = $this->getConexao()->executeQuery($sql);
            if ($table_perfil->RowCount() > 0) {
                return $table_perfil;
            }
        }	

        
        return null;
    }
	

    function obterUsuarioArea($usuario_id, $area_id) {
        $sql = 'SELECT * FROM vw_area_usuario where usuario_id='.$usuario_id.' and area_id='.$area_id;
        $usuario_area = $this->getConexao()->executeQuery($sql);
        if ($usuario_area->RowCount() > 0) {
            return "checked";
        }
        return null;
    }
  
    function obterUsuarioAreaColaborador($usuario_id, $area_id) {
        $sql = 'SELECT * FROM vw_area_usuario where usuario_id='.$usuario_id.' and area_id='.$area_id;
        $usuario_area = $this->getConexao()->executeQuery($sql);
        if ($usuario_area->RowCount() > 0) {
            return "checked";
        }
        return null;
    }

    function getPerfilUsuario($usuario_id, $perfil_id) {
        $sql = 'SELECT * FROM perfil_usuario where usuario_id='.$usuario_id.' and perfil_id='.$perfil_id;
        $usuario_area = $this->getConexao()->executeQuery($sql);
        if ($usuario_area->RowCount() > 0) {
            return "selected";
        }
        return null;
    }
    
    function getNumeroPerfilUsuario($usuario_id) {
        $sql = 'SELECT * FROM perfil_usuario where usuario_id='.$usuario_id;
        $usuario = $this->getConexao()->executeQuery($sql);
        if ($usuario->RowCount() > 0) {
            $usuario = $usuario->getRow(0);
        return $usuario->perfil_id;
        }
        return null;
    }

    function atualizarAreas($usuario_id, $areas) {
        $sql = 'DELETE from area_usuario where usuario_id='.$usuario_id;
        $this->getConexao()->executeNonQuery($sql);
        for($i=0;$i<sizeof($areas);$i++){			
            $sql = 'INSERT INTO area_usuario (usuario_id, area_id) values('.$usuario_id.','.$areas[$i].')';
            $this->getConexao()->executeNonQuery($sql);
        }
    }
	

    function atualizarPerfil($usuario_id, $perfil_id) {
        $sql = "DELETE FROM permissoes WHERE usuario_id = ".$usuario_id;
        $this->getConexao()->executeNonQuery($sql);
        $sql = 'UPDATE perfil_usuario SET perfil_id = '.$perfil_id.' WHERE  usuario_id = '.$usuario_id;
        $this->getConexao()->executeNonQuery($sql);
        $sql = 'UPDATE usuario SET usu_perfil = '.$perfil_id.' WHERE  usuario_id = '.$usuario_id;
        $this->getConexao()->executeNonQuery($sql);
        if($perfil_id == 2 || $perfil_id == 3 || $perfil_id == 5) {
            $this->permissaoColaborador($usuario_id);
            if($perfil_id == 3) {
                $this->permissaoEditor($usuario_id);                
            }
        }
    }

    function atualizarClientes($usuario, $clientes_associados, $clientes_nao_associados) {

		if ($clientes_associados != "")
			$clientes_associados = explode(',', $clientes_associados);
		//print_r($clientes_associados);

		if ($clientes_nao_associados != "")
			$clientes_nao_associados = explode(',', $clientes_nao_associados);
		//print_r($clientes_nao_associados);

		if (is_array($clientes_associados)) {
			foreach($clientes_associados as $cli) {

				$sql = 'DELETE 
						FROM
						  cliente_usuario 
						WHERE usuario_id = '.$usuario.'
						AND cliente_id = '.$cli.'
						';
				$this->getConexao()->executeNonQuery($sql);

			}
		}

		if (is_array($clientes_nao_associados)) {
			foreach($clientes_nao_associados as $cli) {
				$sql = "INSERT INTO cliente_usuario (cliente_id, usuario_id) 
						VALUES
						  ('".$cli."', '".$usuario."')";    
				$this->getConexao()->executeNonQuery($sql);
			}
		}
    }

    function atualizarPermissoes($usuario_id, $permissoes) {
        
        $i=0;
        $sql = "DELETE FROM permissoes WHERE usuario_id = ".$usuario_id;
        $this->getConexao()->executeNonQuery($sql);
        
        foreach($permissoes as $arquivo => $acoes) {
             if(substr($arquivo,0,4) == "arq_") {
                
                for($i=0;$i<sizeof($acoes);$i++) {
                    $sql = "INSERT INTO permissoes (usuario_id, acao_id, per_arquivo) VALUES (".$usuario_id.", ".$acoes[$i].", '".substr($arquivo,4)."');";
                    $this->getConexao()->executeNonQuery($sql);
                }
             }
        } 
    }
	

    function excluirUsuario($usuario_id) {	
        $sql = 'update usuario set usu_excluido=1 where usuario_id='.$usuario_id[0];
        $this->getConexao()->executeNonQuery($sql);
    }
    
    function verificarPermissao($usuario_id,$arquivo,$acao) {
        $sql = "SELECT * FROM permissoes WHERE usuario_id = ".$usuario_id." AND per_arquivo = '".$arquivo."' AND acao_id = ".$acao;
        $aaa = $this->getConexao()->executeNonQuery($sql);
        if($aaa >= 1) {
            return "checked='checked'";
        } else {
            return "";
        }
    }
    

    private function permissaoColaborador($usuario_id) {
      

      $sql = 'INSERT INTO permissoes (permissao_id, usuario_id, acao_id, per_arquivo) VALUES
                         (NULL , '.$usuario_id.', 1, "alterar_areas_noticia"),
                         (NULL , '.$usuario_id.', 2, "alterar_areas_noticia"),
                         (NULL , '.$usuario_id.', 3, "alterar_areas_noticia"),
                         (NULL , '.$usuario_id.', 4, "alterar_areas_noticia"),
                         (NULL , '.$usuario_id.', 5, "alterar_areas_noticia"),
                         (NULL , '.$usuario_id.', 1, "assinante"),
                         (NULL , '.$usuario_id.', 2, "assinante"),
                         (NULL , '.$usuario_id.', 3, "assinante"),
                         (NULL , '.$usuario_id.', 4, "assinante"),
                         (NULL , '.$usuario_id.', 5, "assinante"),
                         (NULL , '.$usuario_id.', 1, "assinar_newsletter"),
                         (NULL , '.$usuario_id.', 2, "assinar_newsletter"),
                         (NULL , '.$usuario_id.', 3, "assinar_newsletter"),
                         (NULL , '.$usuario_id.', 4, "assinar_newsletter"),
                         (NULL , '.$usuario_id.', 5, "assinar_newsletter"),
                         (NULL , '.$usuario_id.', 1, "cadastrar_comentario"),
                         (NULL , '.$usuario_id.', 2, "cadastrar_comentario"),
                         (NULL , '.$usuario_id.', 3, "cadastrar_comentario"),
                         (NULL , '.$usuario_id.', 4, "cadastrar_comentario"),
                         (NULL , '.$usuario_id.', 5, "cadastrar_comentario"),
                         (NULL , '.$usuario_id.', 6, "cadastrar_comentario"),
                         (NULL , '.$usuario_id.', 10, "cadastrar_comentario"),
                         (NULL , '.$usuario_id.', 1, "gestao_colaborador"),
                         (NULL , '.$usuario_id.', 4, "gestao_colaborador"),
                         (NULL , '.$usuario_id.', 1, "gestao_noticia"),
                         (NULL , '.$usuario_id.', 2, "gestao_noticia"),
                         (NULL , '.$usuario_id.', 3, "gestao_noticia"),
                         (NULL , '.$usuario_id.', 4, "gestao_noticia"),
                         (NULL , '.$usuario_id.', 5, "gestao_noticia"),
                         (NULL , '.$usuario_id.', 1, "girar_noticia"),
                         (NULL , '.$usuario_id.', 2, "girar_noticia"),
                         (NULL , '.$usuario_id.', 3, "girar_noticia"),
                         (NULL , '.$usuario_id.', 4, "girar_noticia"),
                         (NULL , '.$usuario_id.', 5, "girar_noticia"),
                         (NULL , '.$usuario_id.', 1, "indicar_noticia"),
                         (NULL , '.$usuario_id.', 2, "indicar_noticia"),
                         (NULL , '.$usuario_id.', 3, "indicar_noticia"),
                         (NULL , '.$usuario_id.', 4, "indicar_noticia"),
                         (NULL , '.$usuario_id.', 5, "indicar_noticia"),
                         (NULL , '.$usuario_id.', 6, "indicar_noticia"),
                         (NULL , '.$usuario_id.', 10, "indicar_noticia"),
                         (NULL , '.$usuario_id.', 4, "inserir_email"),
                         (NULL , '.$usuario_id.', 1, "noticia"),
                         (NULL , '.$usuario_id.', 2, "noticia"),
                         (NULL , '.$usuario_id.', 3, "noticia"),
                         (NULL , '.$usuario_id.', 4, "noticia"),
                         (NULL , '.$usuario_id.', 5, "noticia"),
                         (NULL , '.$usuario_id.', 3, "remover_usuario_email"),
                         (NULL , '.$usuario_id.', 4, "remover_usuario_email"),
                         (NULL , '.$usuario_id.', 1, "visualizar_noticia"),
                         (NULL , '.$usuario_id.', 2, "visualizar_noticia"),
                         (NULL , '.$usuario_id.', 3, "visualizar_noticia"),
                         (NULL , '.$usuario_id.', 4, "visualizar_noticia"),
                         (NULL , '.$usuario_id.', 5, "visualizar_noticia")';
                         

        $this->getConexao()->executeNonQuery($sql);      
    }
    
    private function permissaoEditor($usuario_id) {
      

      $sql = 'INSERT INTO permissoes (permissao_id, usuario_id, acao_id, per_arquivo) VALUES
                        (NULL , '.$usuario_id.', 1, "assinatura"),
                        (NULL , '.$usuario_id.', 2, "assinatura"),
                        (NULL , '.$usuario_id.', 3, "assinatura"),
                        (NULL , '.$usuario_id.', 4, "assinatura"),
                        (NULL , '.$usuario_id.', 5, "assinatura"),
                        (NULL , '.$usuario_id.', 4, "colaborador"),
                        (NULL , '.$usuario_id.', 1, "comentario"),
                        (NULL , '.$usuario_id.', 2, "comentario"),
                        (NULL , '.$usuario_id.', 3, "comentario"),
                        (NULL , '.$usuario_id.', 4, "comentario"),
                        (NULL , '.$usuario_id.', 5, "comentario"),
                        (NULL , '.$usuario_id.', 1, "gestao_areas_de_interesse"),
                        (NULL , '.$usuario_id.', 2, "gestao_areas_de_interesse"),
                        (NULL , '.$usuario_id.', 3, "gestao_areas_de_interesse"),
                        (NULL , '.$usuario_id.', 4, "gestao_areas_de_interesse"),
                        (NULL , '.$usuario_id.', 5, "gestao_areas_de_interesse"),
                        (NULL , '.$usuario_id.', 11, "gestao_areas_de_interesse"),
                        (NULL , '.$usuario_id.', 1, "gestao_assinante"),
                        (NULL , '.$usuario_id.', 2, "gestao_assinante"),
                        (NULL , '.$usuario_id.', 3, "gestao_assinante"),
                        (NULL , '.$usuario_id.', 4, "gestao_assinante"),
                        (NULL , '.$usuario_id.', 5, "gestao_assinante"),
                        (NULL , '.$usuario_id.', 1, "gestao_assinatura"),
                        (NULL , '.$usuario_id.', 2, "gestao_assinatura"),
                        (NULL , '.$usuario_id.', 3, "gestao_assinatura"),
                        (NULL , '.$usuario_id.', 4, "gestao_assinatura"),
                        (NULL , '.$usuario_id.', 5, "gestao_assinatura"),
                        (NULL , '.$usuario_id.', 2, "gestao_colaborador"),
                        (NULL , '.$usuario_id.', 3, "gestao_colaborador"),
                        (NULL , '.$usuario_id.', 5, "gestao_colaborador"),
                        (NULL , '.$usuario_id.', 1, "gestao_janela"),
                        (NULL , '.$usuario_id.', 2, "gestao_janela"),
                        (NULL , '.$usuario_id.', 3, "gestao_janela"),
                        (NULL , '.$usuario_id.', 4, "gestao_janela"),
                        (NULL , '.$usuario_id.', 5, "gestao_janela"),
                        (NULL , '.$usuario_id.', 1, "gestao_newsletter"),
                        (NULL , '.$usuario_id.', 2, "gestao_newsletter"),
                        (NULL , '.$usuario_id.', 3, "gestao_newsletter"),
                        (NULL , '.$usuario_id.', 4, "gestao_newsletter"),
                        (NULL , '.$usuario_id.', 5, "gestao_newsletter"),
                        (NULL , '.$usuario_id.', 6, "gestao_newsletter"),
                        (NULL , '.$usuario_id.', 7, "gestao_newsletter"),
                        (NULL , '.$usuario_id.', 8, "gestao_newsletter"),
                        (NULL , '.$usuario_id.', 9, "gestao_newsletter"),
                        (NULL , '.$usuario_id.', 10, "gestao_newsletter"),
                        (NULL , '.$usuario_id.', 11, "gestao_newsletter"),
                        (NULL , '.$usuario_id.', 1, "importacao_noticia"),
                        (NULL , '.$usuario_id.', 2, "importacao_noticia"),
                        (NULL , '.$usuario_id.', 3, "importacao_noticia"),
                        (NULL , '.$usuario_id.', 4, "importacao_noticia"),
                        (NULL , '.$usuario_id.', 5, "importacao_noticia"),
                        (NULL , '.$usuario_id.', 1, "janela"),
                        (NULL , '.$usuario_id.', 2, "janela"),
                        (NULL , '.$usuario_id.', 3, "janela"),
                        (NULL , '.$usuario_id.', 4, "janela"),
                        (NULL , '.$usuario_id.', 5, "janela"),
                        (NULL , '.$usuario_id.', 1, "newsletter"),
                        (NULL , '.$usuario_id.', 2, "newsletter"),
                        (NULL , '.$usuario_id.', 3, "newsletter"),
                        (NULL , '.$usuario_id.', 4, "newsletter"),
                        (NULL , '.$usuario_id.', 5, "newsletter")';
                        
        $this->getConexao()->executeNonQuery($sql);           
    }


    function obterAreasNaoSelecionadas($filtro = null) {
        $usuario = controller_seguranca::getInstance()->identificarUsuario();

        if (empty($filtro) || $filtro == "") {
            $sql = 'SELECT DISTINCT area.area_id, area.are_descricao, area.are_observacao FROM area, area_usuario
        		';
            if (isset($usuario)) {
                $sql .= 'WHERE area_usuario.usuario_id != ' . $usuario->getID();
            }
            $sql .= ' ORDER BY area.are_descricao';
        } else {
            $sql = "SELECT * FROM area WHERE $filtro ORDER BY are_descricao";
        }

        $result = $this->getConexao()->executeQuery($sql);

        if ($result != null) {
            return $result;
        }

        return null;
    }

    function enviarSenha($email,$senha,$usuario){
            $content = "Recuperacao da senha: <br><b>Usuario: </b>$usuario<br><b>Senha:</b>".base64_decode($senha)."";
            $title = "Senha usuario: $usuario do NoticiadorWeb 2.0";
            $headers = "MIME-Version: 1.0\n";
            $headers .="X-Sender:bisa@bisa.com.br\n";
            $headers .="X-mailer: PHP/" . phpversion() . "\n";
            $headers .="X-Priority: 1\n";
            $headers .="Content-Type: text/html; charset=ISO-8859-1\n";
            $headers .="Reply-To: " . $email . "\n";

            $headers .= "To: $email\n";
            $headers .= "From: " . EMAIL_ORIGEM . "\n";
            $headers .= "Return-Path: " . EMAIL_ORIGEM . "\n";
            Email::sendEmail($email, $title, $content);
            //mail($email, $title,$content, $headers);
        }
        


    function obterClientes($usuario) {
        $sql = 'SELECT 
                  *
                FROM
                  vw_clientes_ativos';
        $table = $this->getConexao()->executeQuery($sql);
        if ($table->RowCount() > 0) {
            return $table;
        }
        return null;
    }
    
    

    function obterClientesAssociados($user) {
        $usuario = controller_seguranca::getInstance()->identificarUsuario();
        $usuario_id = $usuario->getID();
        $cod = $this->PegarPerfil($usuario_id);
        
        
        $sql = 'SELECT 
                  cli.CLIE_Codigo,
                  cli.CLIE_Nome
                FROM
                  vw_clientes_ativos cli 
                  INNER JOIN cliente_usuario usu 
                    ON cli.CLIE_Codigo = usu.cliente_id 
                WHERE usu.usuario_id = '.$user;
        
        if($cod == 3) { 
            $sql .= ' AND cli.CLIE_Codigo IN 
                     (SELECT 
                       cliente_id 
                     FROM
                       cliente_usuario 
                     WHERE usuario_id = '.$usuario_id.')';
        }
        
        $table = $this->getConexao()->executeQuery($sql);
        if ($table->RowCount() > 0) {
            return $table;
        }
        return null;
    }
    
    function obterClientesNaoAssociados($user) {
        $usuario = controller_seguranca::getInstance()->identificarUsuario();
        $usuario_id = $usuario->getID();
        $cod = $this->PegarPerfil($usuario_id);
        
        $sql = 'SELECT 
                  CLIE_Codigo,
                  CLIE_Nome 
                FROM
                  vw_clientes_ativos 
                WHERE CLIE_Codigo NOT IN 
                  (SELECT 
                    cli.CLIE_Codigo 
                  FROM
                    vw_clientes_ativos cli 
                    INNER JOIN cliente_usuario usu 
                      ON cli.CLIE_Codigo = usu.cliente_id 
                  WHERE usu.usuario_id = '.$user.')';
                  
        if($cod == 3) { 
            $sql .= ' AND CLIE_Codigo IN 
                      (SELECT 
                        cliente_id 
                      FROM
                        cliente_usuario 
                      WHERE usuario_id = '.$usuario_id.')';
        }
        
        $table = $this->getConexao()->executeQuery($sql);
        if ($table->RowCount() > 0) {
            return $table;
        }
        return null;
    }

    function obterUsuarios() {
        
        $usuario = controller_seguranca::getInstance()->identificarUsuario();
        $usuario_id = $usuario->getID();
        $cod = $this->PegarPerfil($usuario_id);
        
        $sql = 'SELECT DISTINCT u.*
        FROM usuario u
        INNER JOIN perfil_usuario p ON u.usuario_id = p.usuario_id
        WHERE u.usu_excluido <> 1';

if(isset($_REQUEST['FUSUA_Email']) && !empty($_REQUEST['FUSUA_Email'])) {
    $sql .= ' AND u.usu_email = "'. $_REQUEST['FUSUA_Email'] . '"';
}

if(isset($_POST['FUSUA_Perfil']) && !empty($_POST['FUSUA_Perfil'])) {
    $sql .= ' AND p.perfil_id = '. $_POST['FUSUA_Perfil'];
}

if($cod == 3) {
    /*$sql .= ' AND u.usuario_id IN 
              (SELECT 
                usuario_id 
              FROM
                area_usuario 
              WHERE area_id IN 
                (SELECT 
                  area_id 
                FROM
                  cliente_area 
                WHERE cliente_id IN 
                  (SELECT 
                    cliente_id 
                  FROM
                    cliente_usuario 
                  WHERE usuario_id = '.$usuario_id.')))
                  AND usu_perfil = 2';*/
    $sql .= ' AND u.usu_perfil <> 1';
}

$sql .= ' ORDER BY u.usu_nome';
        $table = $this->getConexao()->executeQuery($sql);
        
        if ($table->RowCount() > 0) {
            return $table;
        }
        return null;
    }
    
    function obterUsuarioPorEmail($email) {
        $sql = 'SELECT * FROM usuario WHERE usu_email = "'.$email.'"';
        
        $table = $this->getConexao()->executeQuery($sql);
        if ($table->RowCount() > 0) {
            return $table;
        }
        return null;
    }
        
}
?>
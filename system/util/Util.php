<?

function obterFullTable($nomeTabela, $order = null){
	$sql = "SELECT DISTINCT * FROM $nomeTabela";
	
	if ($order != null)
		$sql .= " ORDER BY $order";
		
	return TConexao::getInstance()->executeQuery($sql);
}

function translate($string, $booInverter = false) {

	//return $string;//utf8_encode()

	// Identifica o idioma atual
	/*
	if (isset($_SESSION["SESSION_LANG"])) {
	   
    	$idioma = $_SESSION["SESSION_LANG"];
    	
    	// Carrega arquivo de idiomas
    	$filename = DIR_LANGUAGES."$idioma.txt";
    	
    	if (file_exists($filename)) {
    		$handle = fopen($filename, "r");
    		if ($handle) {
    			$contents = fread($handle, filesize($filename));
    			$linhas = explode("\n", $contents);
    			for($i = 0; $i < count($linhas); $i++) {
    				$dicionario = explode("|", $linhas[$i]);
    				if (trim(html_entity_decode($dicionario[0])) == trim($string))
    					return html_entity_decode($dicionario[1]);
    			}
    		}
    		
    		fclose($handle);
    	}
    	
    	return $string;//iconv("UTF-8", "ISO-8859-1", $string);
    }else{
		//$string = iconv("ISO-8859-1", "UTF-8", $string); //utf8_encode($string);
        return $string;
    }
	*/

	$strCharset = mb_detect_encoding($string);

	if (strpos(URL, "teste") !== false) {

		if ((strpos($strCharset, "UTF") !== false) || !$strCharset) {
			if (!$booInverter)
				$string = utf8_encode($string);
			else
				$string = utf8_decode($string);
		}

		$strCharset = mb_detect_encoding($string);

		if (strpos($strCharset, "ASCII") !== false) {
			if (!$booInverter) {
				$string = utf8_encode($string);
				$string = utf8_decode($string);
			} else {
				$string = utf8_decode($string);
				$string = utf8_encode($string);
			}
		}

	}

	return $string;
}

function carregarAction() {
	
    if (!empty($_GET[PARAMETER_NAME_ACTION]))
    	return $_GET[PARAMETER_NAME_ACTION];   	
    else if (!empty($_POST[PARAMETER_NAME_ACTION]))
    	return $_POST[PARAMETER_NAME_ACTION];
    	
    return ACTION_DEFAULT;
}

function carregarArquivo() {
    if (!empty($_GET[PARAMETER_NAME_FILE])) 
    	return $_GET[PARAMETER_NAME_FILE];
    else if (!empty($_POST[PARAMETER_NAME_FILE])) 
    	return $_POST[PARAMETER_NAME_FILE];

    return FILE_DEFAULT;
}

function add_html_head($title = null, $menu=null) {
	if (file_exists(DIR_TEMPLATES."system/head.php")) {
		require_once(DIR_TEMPLATES."system/head.php");
	}
}

function add_html_headPrincipal($title = null, $menu=null) {
	if (file_exists(DIR_TEMPLATES."system/headPrincipal.php")) {
		require_once(DIR_TEMPLATES."system/headPrincipal.php");
	}
}

function add_html_tail() {
	if (file_exists(DIR_TEMPLATES."system/tail.php")) {
		require_once(DIR_TEMPLATES."system/tail.php");
	}
}

function add_html_tailPrincipal() {
	if (file_exists(DIR_TEMPLATES."system/tailPrincipal.php")) {
		require_once(DIR_TEMPLATES."system/tailPrincipal.php");
	}
}

function add_html_includes() {
	foreach( $GLOBALS['array_css_files'] as $value )
		print "<link rel=\"stylesheet\" href=\"".DIR_SCRIPTS.$value."\" type=\"text/css\">\n";

	foreach( $GLOBALS['array_js_files'] as $value )
		print "<script type=\"text/javascript\" src=\"".DIR_SCRIPTS.$value."\"></script>\n";
		
}

function add_action_data($action=null, $file=null, $id=null) {
	$buffer = "";
	if ($action != null)
		$buffer .="<input type=\"hidden\" name=\"".PARAMETER_NAME_ACTION."\" value=\"$action\"/>\n";
	else
		$buffer .="<input type=\"hidden\" name=\"".PARAMETER_NAME_ACTION."\" value=\"".carregarAction()."\" />\n";
		
	if($file != null) 
		$buffer .= "<input type=\"hidden\" name=\"".PARAMETER_NAME_FILE."\" value=\"$file\"/>\n";
	else
		$buffer .= "<input type=\"hidden\" name=\"".PARAMETER_NAME_FILE."\" value=\"".carregarArquivo()."\" />\n";
	
	if($id != null) 
		$buffer .= "<input type=\"hidden\" name=\"id\" value=\"$id\" />\n";

	print $buffer;
}

function add_hidden_last_post() {
	$buffer = "";
	foreach($_POST as $key => $value) {
		if (($key != PARAMETER_NAME_ACTION) && ($key != PARAMETER_NAME_FILE)) {
			 $buffer .= "<input type=\"hidden\" name=\"$key\" value='".filtroTexto($value)/*utf8_encode()*/."' />\n";
		}
	}
	
	print $buffer;
}

function add_hidden_api(){
    if (isset ($_REQUEST['api']))
    echo "<input type=\"hidden\" name=\"api\" value='1' />";
}

function checkapi(){
    if(isset ($_REQUEST['api']))
        return true;
    else
        return false;
}

function msg_alert($msg) {

	//header('Content-type: text/html; charset=iso-8859-1' /*'Content-type: text/html; charset=ISO-8859-1'*/);

	return "<script>alert('".translate($msg)."')</script>";
}

function msg_alert_thumb($msg) {
	return "<script>alert('".$msg."')
                //window.location='index.php?action=show&secao=noticia';
            window.history.back();
                </script>";
}


function msg_question($msg) {
	return "<script>confirm('".translate($msg)."');</script>";
}

function redirect($url) {
    return "<script>window.location = '$url';</script>";
}

function gerarChave($seed = null) {
	$chave = "";
	if ($seed != null)
		$chave = sha1($seed);
	$chave = sha1(time());
	//print "chave gerada: ".$chave."<br/>";
	return $chave;
}

/*
function gerarChave($seed = null) {
	if ($seed != null)
		return sha1($seed);
		
	return sha1(time());
}
*/

function gerarURL($file = null, $action = null) {
	if($file == null)
		$file = carregarArquivo();
		
	if ($action == null)
		$action = carregarAction();
		 
	return "index.php?".PARAMETER_NAME_ACTION."=".$action."&".PARAMETER_NAME_FILE."=".$file;
}

function produce_id_link($id) {
	$url = "index.php?".PARAMETER_NAME_ACTION."=".carregarAction();
	$url .= "&".PARAMETER_NAME_FILE."=".carregarArquivo();
	$url .= "&id=".$id;
		
	return urlencode($url);
}

function produce_combo($name, $array, $attributes = array()) {
	$buffer = "<select name=\"$name\" ";
	
	foreach( $attributes as $key => $value ) {
		$buffer .= 	$key."=\"".$value."\"";
	}
	
	$buffer .= ">";
	
	foreach( $array as $key => $value ) {
		$buffer .= 	"<option value=\"$key\">".$value."</option>";
	}
	
	$buffer .= "</select>";
	
	return $buffer;
}

function produce_link($url, $content, $attributes = null, $action=null, $file=null, $id = null, $link=null) {
	if ($url == null) {
		$url = PARAMETER_NAME_ACTION."=";
		if ($action == null)
			$url .= carregarAction();
		else
			$url .= $action;
			
		$url .= "&".PARAMETER_NAME_FILE."=";
		
		if ($file == null)
			$url .= carregarArquivo();
		else
			$url .= $file;
			
		if ($id != null)
			$url .= "&id=".$id;
			
		$url = URL."index.php?".str_replace(array("&"), array("&amp;"), $url);
	}
	
	if ($link!=null){
		$buffer = $content;
		$buffer .= "<a href=\"$url\" ";

		if ($attributes != null)	
			foreach( $attributes as $key => $value ) {
				$buffer .= 	$key."=\"".$value."\"";
			}
			
		$buffer .= ">".$link."</a>";

	}else{
		$buffer = "<a href=\"$url\" ";

		if ($attributes != null)	
			foreach( $attributes as $key => $value ) {
				$buffer .= 	$key."=\"".$value."\"";
			}
			
		$buffer .= ">".$content."</a>";
	}
	
	return $buffer;
}

function produce_icon($name, $descricao=null) {	
	if (empty($descricao))
		$descricao = $name;
			
	return "<img src=\"".DIR_ICONS."$name\" border=\"0\" alt=\"$descricao\"/>";
}

function obterListaCheckbox($prefixo) {
    $result = array();
    foreach($_POST as $key => $value) {
        $split = explode("_", $key);
        if ($split[0] == $prefixo)
        	$result[] = $value;
    }
    return $result;
}

// Ordena as noticias segum o usuario.
function obterListaCheckboxOrdenados($prefixo, $ordem){
    
    $result = array();
    foreach($_POST as $key => $value) {
        $split = explode("_", $key);
        if ($split[0] == $prefixo){
			foreach($_POST as $key2 => $value2) {
				$split2 = explode("_", $key2);
				if($split2[0]==$ordem){
					if ($split[1] == $split2[1]){
						$result[$value2-1] = $value;
					}
				}
			}
		}
	}

	//retorna um array com as noticias ordenadas
    return $result; 
}

function filtroTexto($texto) {
	 return str_replace ( array ( '&', '"', "'" ), array ( '&amp;' , '&quot;', '&apos;' ), $texto );
   // return urlencode($texto);
}

function somenteNumero($texto) {
    return filtroTexto($texto);
}

function elimina_acentos($cadena){
	$tofind = "�����������������������������������������������������";
	$replac = "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn";
	return(strtr($cadena,$tofind,$replac));
}

function DateConvert($old_date) { 

	$_year = substr($old_date,6,4); 
	$_month = substr($old_date,3,2); 
	$_day = substr($old_date,0,2); 
	
	$new_date =  $_year."-".$_month."-".$_day;
	
	return $new_date; 
} 

function DateExibe($old_date) { 
	$_year = substr($old_date,0,4); 
	$_month = substr($old_date,5,2); 
	$_day = substr($old_date,8,2); 
	
	$new_date = completaZerosEsquerda(2,$_day)."/".completaZerosEsquerda(2,$_month)."/".$_year;
	
	return $new_date; 
}

function completaZerosEsquerda($zeros, $str){
	$strzeros = "";
	for ($i=0;$i<$zeros;$i++){
		$strzeros = $strzeros.'0';
	}
	$str=$strzeros.$str;
	return (substr($str, strlen($str)-($zeros),$zeros));
}

function checkEmail($eMailAddress) {
//if (preg_match("/^[A-Za-z0-9!#$%&\'*+-/=?^_`{|}~]+@[A-Za-z0-9-]+(\.[A-Za-z0-9-]+)+[A-Za-z]$/", $eMailAddress)){
    if(filter_var($eMailAddress, FILTER_VALIDATE_EMAIL)){
        return true;
        
	}else{
        return false;
	}
}

function criar_thumbnail($origem,$destino='./',$largura='300',$pre='',$formato='JPEG') {  

    switch($formato)  
    {  
        case 'jpg':  
            $tn_formato = 'jpg';
            break;  
		case 'JPEG':  
            $tn_formato = 'jpg';  
            break;  
        case 'PNG':  
            $tn_formato = 'png';  
            break;  
        case 'png':  
            $tn_formato = 'png';  
            break;  
		case 'gif':  
            $tn_formato = 'gif';  
            break;  

		case 'GIF':  
            $tn_formato = 'gif';  
            break;  
    }  
	
    $ext = @split("[/\\.]",strtolower($origem));  
    $n = count($ext)-1;  
    $ext = $ext[$n];  

    $arr = @split("[/\\]",$origem);  
    
                
    $n = count($arr)-1;  
    $arra = explode('.',$arr[$n]);  
    $n2 = count($arra)-1;  
    $tn_name = str_replace('.'.$arra[$n2],'',$arr[$n]);  
    $destino = $destino.$pre.$tn_name.'.'.$tn_formato;  

    if ($ext == 'jpg' || $ext == 'jpeg'){  
        $im = imagecreatefromjpeg($origem);
    }elseif($ext == 'png'){  
        $im = imagecreatefrompng($origem);  
    }elseif($ext == 'gif'){  
         $im = imagecreatefromgif($origem);
    }  

    $w = imagesx($im);  
    $h = imagesy($im);  
    if ($w > $h)  
    {  
        $nw = $largura;  
        $nh = ($h * $largura)/$w;  
    }else{  
        $nh = $largura;  
        $nw = ($w * $largura)/$h;  
    }  
    if(function_exists('imagecopyresampled'))  
    {  
        if(function_exists('imageCreateTrueColor'))  
        {  
            $ni = imageCreateTrueColor($nw,$nh);  
        }else{  
            $ni    = imagecreate($nw,$nh);  
        }  
        if(!@imagecopyresampled($ni,$im,0,0,0,0,$nw,$nh,$w,$h))  
        {  
            imagecopyresized($ni,$im,0,0,0,0,$nw,$nh,$w,$h);  
        }  
    }else{  
        $ni    = imagecreate($nw,$nh);  
        imagecopyresized($ni,$im,0,0,0,0,$nw,$nh,$w,$h);  
    }  
    if($tn_formato=='jpg'){  
        imagejpeg($ni,$destino,60);  
    }elseif($tn_formato=='png'){  
        imagepng($ni,$destino);  
    }elseif($tn_formato=='gif'){  
        imagegif($ni,$destino);  
    }   
}  

function parseFile($content, $arr){
    
}

function unzip($src_file, $dest_dir=false, $create_zip_name_dir=true, $overwrite=true) {
	if(function_exists("zip_open")) {  
		if(!is_resource(zip_open($src_file))) {
			$src_file=dirname($_SERVER['SCRIPT_FILENAME'])."/".$src_file;
		}
     
		if (is_resource($zip = zip_open($src_file))) {         
			$splitter = ($create_zip_name_dir === true) ? "." : "/";
			if ($dest_dir === false)
				$dest_dir = substr($src_file, 0, strrpos($src_file, $splitter))."/";
        
			// Create the directories to the destination dir if they don't already exist
			create_dirs($dest_dir);

			// For every file in the zip-packet
			$valido = true;
			while ($zip_entry = zip_read($zip)) {
				// Now we're going to create the directories in the destination directories
			  
				// If the file is not in the root dir
				$pos_last_slash = strrpos(zip_entry_name($zip_entry), "/");
				if ($pos_last_slash !== false) {
					// Create the directory where the zip-entry should be saved (with a "/" at the end)
					create_dirs($dest_dir.substr(zip_entry_name($zip_entry), 0, $pos_last_slash+1));
				}

				// Open the entry
				if (zip_entry_open($zip,$zip_entry,"r")) {
				
					// The name of the file to save on the disk
					$file_name = $dest_dir.zip_entry_name($zip_entry);
				  
					$array_extensao = explode(".", $file_name);
				  
					if ((strtolower($array_extensao[count($array_extensao) - 1]) == "html") || 
						(strtolower($array_extensao[count($array_extensao) - 1]) == "htm") || 
						(strtolower($array_extensao[count($array_extensao) - 1]) == "jpg") ||
						(strtolower($array_extensao[count($array_extensao) - 1]) == "png") ||
						(strtolower($array_extensao[count($array_extensao) - 1]) == "gif")) {
						// Check if the files should be overwritten or not
						if ($overwrite === true || $overwrite === false && !is_file($file_name)) {
							// Get the content of the zip entry
							$fstream = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));          
						   
							if(!is_dir($file_name)) {
								$extensao = substr($file_name,-3);
								if($extensao == 'htm') {
									$file_name = $file_name.'l';
								}
							   
								file_put_contents($file_name, $fstream );   
							}          					
							// Set the rights
							$valido = $valido && file_exists($file_name); 
						}
					}
				
					// Close the entry
					zip_entry_close($zip_entry);
				}     
			}
			// Close the zip-file
			zip_close($zip);
          
			return $valido;
		} else {
			echo "No Zip Archive Found.";
			return false;
		}
    
		return false;
	} else {
		if(version_compare(phpversion(), "5.2.0", "<"))
			$infoVersion="(use PHP 5.2.0 or later)";
     
		echo "You need to install/enable the php_zip.dll extension $infoVersion";
	}
}

function create_dirs($path) {
  if (!is_dir($path)) {
    $directory_path = "";
    $directories = explode("/",$path);
    array_pop($directories);
  
    foreach($directories as $directory) {
      $directory_path .= $directory."/";
      if (!is_dir($directory_path)) {
        mkdir($directory_path);
        chmod($directory_path, 777);
      }
    }
  }
}

function full_rmdir($dirname){
    if ($dirHandle = opendir($dirname)){
        $old_cwd = getcwd();
        @chdir($dirname);

        while ($file = readdir($dirHandle)){
            if ($file == '.' || $file == '..') continue;

            if (is_dir($file)){
                if (!full_rmdir($file)) return false;
            }else{
                if (!unlink($file)) return false;
            }
        }

        closedir($dirHandle);
        chdir($old_cwd);
        if (!rmdir($dirname)) return false;

        return true;
    }else{
        return false;
    }
}

?>
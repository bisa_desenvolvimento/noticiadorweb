<?

class Email {

    public static function sendEmail($to, $subject, $message) {
	   
        require_once(DIR_PLUGINS . SMTP_LIB);
        $mail = new PHPMailer();
        
        $mail->IsSMTP();
        $mail->SMTPAuth = SMTP_AUTH;
        $mail->Host = STMP_HOST;
        $mail->Port =  SMTP_PORT;
        $mail->Username = SMTP_USER;
        $mail->Password = SMTP_PASS;
        
        //$mail->SetFrom('name@yourdomain.com', 'Web App');
        $mail->Subject = $subject;
        $mail->MsgHTML($message);
        $mail->AddAddress($to);
        
        if($mail->Send()) {
            //echo "Message sent!";
            return true;
        } else {
            //echo "Mailer Error: " . $mail->ErrorInfo;
            return false;
        }
    }

}

?>
<?

class Upload {

    var $erro = array();   
    var $arquivo = '';   
       
    var $ext_aceita  = array();   
       
    var $arq_kb      = '';   
    var $max_kb      = '';   
    var $max_altura  = '';   
    var $max_largura = '';   
       
    var $arq_ext        = '';   
    var $arq_n_original = '';   
    var $arq_n_final    = '';   
  
    var $dir_final = '';   
       
    /***************************************  
        ** Iniciando a classe  
    ***************************************/  
    function upload( $arq_ext, $caminho_salva ){           
       
        $this -> erro    = array();   
        $this -> arquivo = '';   
           
        $this -> ext_aceita  = $arq_ext;   
           
        $this -> arq_kb      = '';   
        $this -> max_kb      = '';   
        $this -> max_altura  = '';   
        $this -> max_largura = '';   
           
        $this -> arq_ext     = $arq_ext;   
           
        $this -> arq_n_original = '';   
        $this -> arq_n_final    = '';   
       
        $this -> dir_final = $caminho_salva;   
        $this -> mime = array(   
                "application/hta"               => "hta",   
                "application/java"              => "class",   
                "application/java-byte-code"    => "class",   
                "application/msword"            => "doc",   
                "application/octet-stream"      => "*",   
                "application/octet-stream"      => "bin",   
                "application/octet-stream"      => "class",   
                "application/octet-stream"      => "dms",   
                "application/octet-stream"      => "exe",   
                "application/octet-stream"      => "lha",   
                "application/octet-stream"      => "lzh",   
                "application/pdf"               => "pdf",   
                "application/rtf"               => "rtf",   
                "application/vnd.ms-excel"      => "xla",   
                "application/vnd.ms-excel"      => "xlc",   
                "application/vnd.ms-excel"      => "xlm",   
                "application/vnd.ms-excel"      => "xls",   
                "application/vnd.ms-excel"      => "xlt",   
                "application/vnd.ms-excel"      => "xlw",   
                "application/vnd.ms-powerpoint" => "pot",   
                "application/vnd.ms-powerpoint" => "pps",   
                "application/vnd.ms-powerpoint" => "ppt",   
                "application/winhlp"            => "hlp",   
                "application/x-compressed"      => "zip",   
                "application/x-gzip"            => "gz",   
                "application/x-java-class"      => "class",   
                "application/x-javascript"      => "js",   
                "application/x-mplayer2"        => "asx",   
                "application/x-msaccess"        => "mdb",   
                "application/x-tar"             => "tar",   
                "application/x-zip-compressed"  => "zip",   
                "application/xml"               => "xml",   
                "application/zip"               => "zip",   
                "audio/basic"                   => "au",   
                "audio/basic"                   => "snd",   
                "audio/mid"                     => "mid",   
                "audio/mid"                     => "rmi",   
                "audio/mpeg"                    => "mp3",   
                "audio/x-mpegurl"               => "m3u",   
                "audio/x-pn-realaudio"          => "ra",   
                "audio/x-pn-realaudio"          => "ram",   
                "audio/x-wav"                   => "wav",   
                "image/bmp"                     => "bmp",   
                "image/gif"                     => "gif",   
                "image/jpeg"                    => "jpe",   
                "image/jpeg"                    => "jpeg",   
                "image/jpeg"                    => "jpg",
                "image/pjpeg"                   => "jpg",   
                "image/x-icon"                  => "ico",   
                "multipart/x-zip"               => "zip",   
                "text/asp"                      => "asp",   
                "application/x-php"             => "php",   
                "text/css"                      => "css",   
                "text/html"                     => "htm",   
                "text/html"                     => "html",   
                "text/html"                     => "stm",   
                "text/plain"                    => "bas",   
                "text/plain"                    => "c",   
                "text/plain"                    => "h",   
                "text/plain"                    => "txt",   
                "application/x-download"        => "txt",   
                "text/richtext"                 => "rtx",   
                "text/x-component"              => "htc",   
                "text/xml"                      => "xml",   
                "video/mpeg"                    => "mp2",   
                "video/mpeg"                    => "mpa",   
                "video/mpeg"                    => "mpe",   
                "video/mpeg"                    => "mpeg",   
                "video/mpeg"                    => "mpg",   
                "video/mpeg"                    => "mpv2",   
                "video/quicktime"               => "mov",   
                "video/quicktime"               => "qt",   
                "video/x-la-asf"                => "lsf",   
                "video/x-la-asf"                => "lsx",   
                "video/x-ms-asf"                => "asf",   
                "video/x-ms-asf"                => "asr",   
                "video/x-ms-asf"                => "asx",   
                "video/x-ms-asf"                => "asx",   
                "video/x-msvideo"               => "avi",   
                "video/x-ms-wmv"                => "wmv",   
                "video/x-sgi-movie"             => "movie",   
                "x-world/x-vrml"                => "flr",   
                "x-world/x-vrml"                => "vrml",   
                "x-world/x-vrml"                => "wrl",   
                "x-world/x-vrml"                => "wrz",   
                "x-world/x-vrml"                => "xaf",   
                "x-world/x-vrml"                => "xof",   
                "image/png"                     => "png",
				"image/x-png"                   => "png",   
                "application/x-shockwave-flash" => "swf",          
                ""              => "erro"  
            );             
    }   
       
    /*  
      
      
    Verificamos se algum arquivo foi enviado.  
      
    Caso algum arquivo tenha sido enviado, setamos nossas variaveis iniciais.  
    */  
    /***************************************  
        ** Function para identificar o arquivo  
    ***************************************/       
    function arquivo( $arquivo ){   
  
    $arquivo = isset( $arquivo ) ? $arquivo : false;   
  
        if( !$arquivo ||  $arquivo == '' ){   
           
            $this -> erro[] = "Nenhum arquivo selecionado para upload";   
               
  
        }else{   
            $this -> arq_ext        = $arquivo["type"];   
            $this -> arq_n_temp     = $arquivo["tmp_name"];   
            $this -> arq_kb         = $arquivo["size"];   
            $this -> arq_n_original = $arquivo["name"];   
           
        }   
           
    }   
    function retorna_nome(){   
       
        return $this -> arq_n_original;   
    }   
    /*  
      
      
    Verificamo se a extens�o do arquivo enviado esta no ARRAY ->'ext_aceita'  
    Case n�o esteja, abortamos o envio do arquivo.  
    */  
    /***************************************  
        ** Function para verificar extens�o  
    ***************************************/  
    function verifica_ext(){   
       
        if( count( $this -> erro ) == 0 ){   
            for( $ext = 0; $ext < count( $this -> ext_aceita ); $ext ++ ){   
               
                if( !in_array( $this -> mime[ $this -> arq_ext ], $this -> ext_aceita ) ){   
                   
                    $this -> erro[] = "Arquivo em formato inv�lido!   
                                A imagem deve ser jpg, jpeg,   
                                bmp, gif ou png e voc� enviou ".$this -> arq_ext ." | ".$this -> mime[ $this -> arq_ext ].".Envie outro arquivo";   
  
                           
                }   
            }      
        }   
    }   
       
    /*  
      
      
    Verificamos se o tamanho do arquivo n�o � maior [ em bites ] do que o tamanho m�ximo  
    permitido em ->'$tamanho_max' na chamada do objeto.  
    */  
    /***************************************  
        ** Function verificar tamanho do arquivo  
    ***************************************/  
    function tamanho_k( $tamanho_max ){   
       
        if( count( $this -> erro ) == 0 ){   
            if( $this -> arq_kb > $tamanho_max ){   
               
               
                $tamanho_max = $tamanho_max / 1024;   
                $tamanho_max = number_format( $tamanho_max );   
                $this -> erro[] = translate("Arquivo em tamanho muito grande!   
            A imagem deve ser de no m�ximo ") . $tamanho_max . translate(" k.   
            Envie outro arquivo");   
       
            }   
        }   
    }   
       
    /*  
      
      
    Verificamos se o X [ width ] n�o � maior do que o permitido para envio.  
    */  
    /***************************************  
        ** Function para verificar X do arquivo  
    ***************************************/  
    function tamanho_x( $tamanho_max_x ){   
        if( count( $this -> erro ) == 0 ){   
            $tamanho_X = getimagesize( $this -> arq_n_temp  );   
               
                if( $tamanho_X[0] > $tamanho_max_x){   
                   
                    $this -> erro[] = "Largura da imagem n�o deve   
                        ultrapassar " . $tamanho_max_x . " pixels";   
       
                }   
        }   
    }   
    function retorna_kb(){   
       
        return $this -> arq_kb;   
    }      
    /*  
      
      
    Verificamos se o Y [ height ] n�o � maior do que o permitido para envio.  
    */  
    /***************************************  
        ** Function para verificar Y do arquivo  
    ***************************************/  
    function tamanho_y( $tamanho_max_y ){   
       
        if( count( $this -> erro ) == 0 ){   
            $tamanho_Y = getimagesize( $this -> arq_n_temp  );   
               
                if( $tamanho_Y[1] > $tamanho_max_y){   
                   
                    $this -> erro[] = "Altura da imagem n�o deve   
                        ultrapassar " . $tamanho_max_y . " pixels";   
       
                }   
        }   
    }   
       
    /*  
      
      
    Aqui daremos um nome para o arquivo, se nenhum nome for dado,  
    Ser� gerado um nome único com 32 caracteres, que ser� o MD5 da data atual.  
      
    Caso tenha sido estanciado um nome para o arquivo, este nome ser� dado ao   
    arquivo, substituindo o nome temporario.  
    */  
    /***************************************  
        ** Function tpara renomear o arquivo  
    ***************************************/  
    function arq_nome( $nome, $tipo ){   
       
        if( count( $this -> erro ) == 0 ){   
           
           
            switch( $tipo ){   
               
                case 'ramdom':   
  
                        return $this -> arq_n_final = md5(uniqid(time())) . "." . $this -> mime[ $this -> arq_ext ];       
                       
                break;   
                case 'original':   
                       
                        $this -> arq_n_final = $this -> arq_n_original;   

  
                break;   
                case 'renomeia':   
                       
                        $this -> arq_n_final = $nome . "." . $this -> mime[ $this -> arq_ext ];   
                           
                break;         
            }   
        }   
           
    }   
    function retorna_ext(){   
       
        return $this -> mime[ $this -> arq_ext ];   
    }   
    /*  
      
      
    Salvaremos o arquivo no diretorio definido.  
    */  
    /***************************************  
        ** Function para salvar arquivo  
    ***************************************/  
    function salva(){   
       
        if( count( $this -> erro ) == 0 ){   
            if( !is_dir( $this -> dir_final ) ){   
               
                #$this -> erro[] = "O diretorio que voc� est� tentando salvar o arquivo, n�o existe.";             
                /*  
                  
                  
                Caso diretorio do upload n�o exista, vamos criar ;_)  
                */  
                mkdir( $this -> dir_final, 0777 );   
            }   
            $mover_arquivo = move_uploaded_file( $this -> arq_n_temp, $this -> dir_final. $this -> arq_n_final );   
            if( !$mover_arquivo ){   
               
                $this -> erro[] = "Erro salvando arquivo";   
  
            }

        }
    }

    function finaliza(){

        if( count( $this -> erro ) == 0 ){   

            return $this -> erro[] = false;   
        }else{   

            return $this -> erro;   
        }
    }

}

?>
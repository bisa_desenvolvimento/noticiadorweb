<?php



// Inicialização
require_once $strTargetReferenceDir."system/configuration/configuration_initialization.php";
require_once $strTargetReferenceDir."system/execution/initialization.php";



// Execução
require_once $strTargetReferenceDir."system/configuration/configuration_execution.php";
require_once $strTargetReferenceDir."system/execution/execution.php";



// Finalização
require_once $strTargetReferenceDir."system/configuration/configuration_finalization.php";
require_once $strTargetReferenceDir."system/execution/finalization.php";



?>
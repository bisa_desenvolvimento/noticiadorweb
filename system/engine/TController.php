<?
abstract class TController extends TBase
{
	private $FAction = null;
	
	function getAction() {
	   return $this->FAction;
	}
	
	function setAction($value) {
		$this->FAction = $value;
	}
	
	
	function process() {
		switch ($this->getAction()) {
			case "show":
				$this->show();
				break;
			case "load":
				if (isset($_REQUEST["id"])) {
					$id = filtroTexto($_REQUEST["id"]);
					$this->load($id);
				} else {
					$this->load(0);
				}
				break;
			case "delete":
				$item = $_GET["id"];
				$this->delete($item);
				break;
				// Bruno Magnata 15/09/16 Mantis[4600]
			case "deleteEmail":
				$area = $_GET["area"];
				$emails = $_GET["id"];
				$this->deleteEmail($area, $emails);
				break;
			case "verificarEmail":
				$email = $_REQUEST["email"];
				$this->verificarEmail($email);
				break;
				case "busca":
				$this->busca();
				break;
	   }	
	}
	
	abstract public function show();
	public function verificarEmail($email){}

	abstract public function save($model);
	
	abstract public function load($key);
	
	abstract public function delete($key);
	
	abstract public function create();

	public function deleteEmail($area,$emails) {
        // Apaga da tabela assinante os emails selecionados e seu relacionamento com a tabela assinante_area
        $sql = "DELETE FROM assinantes WHERE  assinante_id in (" . $emails." )";
        $this->getConexao()->executeNonQuery($sql);

        $sql = "DELETE FROM assinante_area WHERE  assinante_id in (" . $emails." ) and area_id in (" . $area." )  ";
        $this->getConexao()->executeNonQuery($sql);
        echo '<span class="ui-icon ui-icon-notice" style="float: left"></span><label style="float: left;">Email(s) Apagado(s) do banco de dados!</label>';
    }
	
	function busca() {}
	
	function obterMenu() {
	
		$usuario = controller_seguranca::getInstance()->identificarUsuario();
	
		if ($usuario == null)
			$sql = "SELECT DISTINCT arq_descricao, aca_descricao, arq_chave, arq_arquivo, arq_exibe FROM vw_anonimo_arquivo WHERE  arq_exibe=1 and aca_descricao = 'show' AND arq_chave != '".FILE_DEFAULT."'";
		else
			$sql = "SELECT DISTINCT arq_descricao, aca_descricao, arq_chave, arq_arquivo, arq_exibe FROM vw_usuario_arquivo WHERE arq_exibe=1 and aca_descricao = 'show' AND arq_chave != '".FILE_DEFAULT."' AND usuario_id = ".$usuario->getID();
		
		$table = $this->getConexao()->executeQuery($sql);
		
		if ($table->RowCount() > 0) {
			return $table;
		}
		
		return null;
	}

    /**
     * Obter as áreas associadas para cada cliente passado no parâmetro cliente. As áreas sem associação com cliente receberão como parâmetro "COLAB"
     * Caso seja passado o parâmetro usuario, a função retorna os clientes do usuário informado.
     * Caso não seja passado o parâmetro usuario, a função retorna os clientes do usuário atualmente logado no sistema.
     * André Alves - 27/01/2012
     *
     * @param string $cliente Id do cliente
     * @param int $usuario Id do usuário
     * @return object Lista com os clientes
     */
    function obterAreasDoUsuario($cliente = '', $usuario = '') {

        $where = '';

        if($usuario == '') {
            $res = controller_seguranca::getInstance()->identificarUsuario();
            $usuario = $res->getID();
        }

        if($cliente != '') {
            $where .= ' AND Clie_Codigo = "'.$cliente.'"';
        }

        $sql = 'SELECT 
                  area_id,
                  are_descricao,
                  are_observacao 
                FROM
                  vw_usuarios_areas_clientes 
                WHERE usuario_id = '.$usuario.
            $where.'                   
              GROUP BY area_id  ORDER BY are_descricao';

        $result = $this->getConexao()->executeQuery($sql);
		
        if ($result != null) {
            return $result;
        }

        return null;
    }
	
}
?>
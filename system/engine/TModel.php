<?
abstract class TModel extends TBase {
	private $FID = null;
	
	function getID() {
		return $this->FID;
	}
	
	function setID($value){
		$this->FID = $value;
	}
	
	abstract public function bind($arr);
}
?>
<?
abstract class TView extends TBase {

		private $FController;
		private $FModel;
				
		function __construct ( $controller, $model = null ) {
				$this->setController($controller);
				if ($model != null)
						$this->setModel($model);
		}
		
		function __destroy ( ) {
		}
		
		protected function getController() {
			return $this->FController;
		}
		
		protected function setController($value) {
			$this->FController = $value;
		}
		
		public function getModel() {
			return $this->FModel;
		}
		
		public function setModel($value) {
			$this->FModel = $value;
		}
		
		public function getID() {
			if ($this->getModel() != null) {
				return $this->getModel()->getID();
			}
			
			return null;
		}
		
		abstract public function show();
		
		function add_form_process_url() {
				$buffer_url = "index.php";
				
				$controller_arquivo = new controller_arquivo();
				$controller_arquivo->setConexao(TConexao::getInstance());
				$chave = $controller_arquivo->getFormPostUrl(carregarArquivo());
				
				if ($chave != null) {
					$buffer_url .= "?".PARAMETER_NAME_FILE."=".$chave;
				}
				
				return $buffer_url;
		}
		
		public function obterMenu() {
			$table = $this->getController()->obterMenu();
			
			$buffer = "";
			if ($table != null) {
				for($i = 0; $i < $table->RowCount(); $i++) {
					$row = $table->getRow($i);

					$url = "index.php?".PARAMETER_NAME_ACTION."=".$row->aca_descricao."&".PARAMETER_NAME_FILE."=".$row->arq_chave;
									   
					$buffer .= "<li>".produce_link($url, $row->arq_descricao)."</li>";

				}
			}
			
		return $buffer;
		}

		public function perfilUsuario(){
			 
		$resultado = $this->Retorno();
		return $resultado ;
		
		}
		
			
		function Retorno(){
		
		 $usuario = controller_seguranca::getInstance()->identificarUsuario();  
		 
		 if($usuario != null) {
		 
			$usuario_id = $usuario->getID();
		 
			 if (($usuario_id)== null){
			 $codusuario = 0;
			 }else{
			 $codusuario =  $usuario->getID();
			 }   
			
		 } else {
			$codusuario = 0;
		 }		 
		 
		 $banco = mysqli_connect(DB_SERVER,DB_USER,DB_PASS,DB_DATASET);
		 $sqlusuario = "select perfil_id from perfil_usuario where usuario_id=".$codusuario."";
		 $query = mysqli_query( $banco,$sqlusuario);	 
		 $arrResultado = mysqli_fetch_row($query);
			 /* if(isset($arrResultado['INDICE'][0])) {
						$intperfil = $arrResultado['INDICE'][0];
				} else {
						$intperfil = 0;
				}*/
			  
			  if(isset($arrResultado[0])) {
						$intperfil = $arrResultado[0];
				} else {
						$intperfil = 0;
				}
			  				
				return $intperfil;
				
		}
		
		
}
?>
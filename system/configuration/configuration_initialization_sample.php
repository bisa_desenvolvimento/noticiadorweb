<?php



function exception_handler($exception) {
  echo "<h1>O sistema se comportou de maneira indevida e gerou a seguinte excessão : " , $exception->getMessage(), "</h1>\n";
}

function myErrorHandler($errno, $errstr, $errfile, $errline) {
    switch ($errno) {
    case E_ALL:
    case E_USER_ERROR:
        echo "<b>ERRO</b> [$errno] $errstr<br />\n";
        echo "  Ocorreu um erro fatal na linha $errline in file $errfile";
        echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
        echo "Aborting...<br />\n";
        exit(1);
        break;

    case E_USER_WARNING:
        echo "<b>My WARNING</b> [$errno] $errstr<br />\n";
        break;

    case E_USER_NOTICE:
        echo "<b>My NOTICE</b> [$errno] $errstr<br />\n";
        break;

    default:
        echo "Unknown error type: [$errno] $errstr<br />\n";
        break;
    }

    /* Don't execute PHP internal error handler */
    return true;
}

function debug($msg) {
	if (SHOW_DEBUG_INFO == "1")
		print "<font color=\"red\">".$msg."</font><br/>\n";
}

function show_debug_info() {
	global $action, $arquivo;
	if (SHOW_DEBUG_INFO == "1") {
		print "<h3>ACTION</h3>";
		print "<pre>";
		print_r($action);
		print "</pre>";
		
		print "<h3>ARQUIVO</h3>";
		print "<pre>";
		print_r($arquivo);
		print "</pre>";
		
		print "<h3>GET</h3>";
		print "<pre>";
		print_r($_GET);
		print "</pre>";
		
		print "<h3>POST</h3>";
		print "<pre>";
		print_r($_POST);
		print "</pre>";
		
        print "<h3>SESSION</h3>";
		print "<pre>";
		print_r($_SESSION);
		print "</pre>";
    
		print "<h3>COOKIE</h3>";
		print "<pre>";
		print_r($_COOKIE);
		print "</pre>";
		
		print "<h3>SERVER</h3>";
		print "<pre>";
		print_r($_SERVER);
		print "</pre>";
	}
}



error_reporting(E_ALL ^ E_DEPRECATED);

ini_set("memory_limit","512M");

set_time_limit(0);

date_default_timezone_set('America/Sao_Paulo');

session_start();



// Constantes do Sistema
$host = explode('.',$_SERVER["HTTP_HOST"]);
define("ENVIRONMENT",$host[0]);

// Debug

define("SHOW_DEBUG_INFO", (isset($_REQUEST["sdi"])) ? $_REQUEST["sdi"] : "0");

// SMTP

define("SMTP_LIB", "phpmailer/class.phpmailer.php");
define("SMTP_AUTH", true);
define("STMP_HOST", "email-smtp.us-east-1.amazonaws.com");
define("SMTP_PORT", 587);
define("SMTP_USER", "AKIA4GCZDZAETU6UDC5Q");
define("SMTP_PASS", "");


/*
 Conexão ao servidor da BISAWEB para as estat�sticas de envio
*/
define("DB_SERVER_BISAWEB_SERV", "mysql.ownserver.noticiadorweb.com.br");
define("DB_DATASET_BISAWEB_SERV", "noticiador");
define("DB_USER_BISAWEB_SERV", "bisa");
define("DB_PASS_BISAWEB_SERV", "");
define("DB_TYPE_BISAWEB_SERV", "MYSQL");


/*
 Conexão com a base de dados do NoticiadorWeb
*/

define("DB_DATASET", "noticiadorweb");

//Local
 define("DB_SERVER", "localhost");
 define("DB_USER", "root");
 define("DB_PASS", "");
 define("DB_TYPE", "MYSQL");

/*
 /Conexão com a base de dados do NoticiadorWeb para emails
*/
define("DB_SERVER_BISAWEB", DB_SERVER);
define("DB_DATASET_BISAWEB", DB_DATASET);
define("DB_USER_BISAWEB", DB_USER);
define("DB_PASS_BISAWEB", DB_PASS);
define("DB_TYPE_BISAWEB", DB_TYPE);



// Raiz Web

define("URL", 'https://'.$_SERVER["HTTP_HOST"].'/');

// Diretários

define("DIR_SERVER_FOLDER","");
define("DIR", $strTargetReferenceDir);

define("DIR_SYSTEM", 			DIR."system/");
define("DIR_PLUGINS", 			DIR."plugins/");
define("DIR_SCRIPTS", 			DIR."scripts/");
define("DIR_IMAGES", 			DIR."images/");
define("DIR_DATA", 				DIR."data/");
define("DIR_USERS", 			DIR."users/");

define("DIR_UTIL", 				DIR_SYSTEM."util/");
define("DIR_SOURCE", 			DIR_SYSTEM."source/");
define("DIR_ENGINE", 			DIR_SYSTEM."engine/");
define("DIR_MODULES", 			DIR_SYSTEM."modules/");

define("DIR_ICONS", 			DIR_IMAGES."icons/");
define("DIR_LAYOUT", 			DIR_IMAGES."layout/");

define("DIR_LANGUAGES",			DIR_DATA."languages/");

define("DIR_MODELOS", 			DIR_USERS."layouts/");
define("DIR_UPLOAD", 			DIR_USERS."upload/");

define("DIR_MODEL", 			DIR_MODULES."model/");
define("DIR_VIEW", 				DIR_MODULES."view/");
define("DIR_CONTROL", 			DIR_MODULES."control/");
define("DIR_TEMPLATES", 		DIR_MODULES."templates/");



?>
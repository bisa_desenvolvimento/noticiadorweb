<?php



// Versão

define("VERSION", "3.0");



// Prefixos MVC

define("PREFIX_CONTROLLER", 	"controller_");
define("PREFIX_VIEW", 			"view_");
define("PREFIX_MODEL", 			"model_");



// Parâmetros de Requisição

define("PARAMETER_NAME_FILE", "secao");
define("PARAMETER_NAME_ACTION", "action");



// Ações Default

define("FILE_DEFAULT", "principal");
define("ACTION_DEFAULT", "show");
define("LOGIN_ON_ACCESS_DENIED", "1");
define("ACTION_LOGIN", "login");



// Configurações Customizadas

define("PERFIL_COLABORADOR", "2");
define("PERFIL_EDITOR", "3");
define("PERFIL_ADMINISTRADOR", "1");
define("NEWS_LETTER_TITULO", "Newsletter");
define("EMAIL_ORIGEM", "noticiadorweb@bisa.com.br"); //define("EMAIL_ORIGEM", "noticiadorweb@smtp.ownserver.noticiadorweb.com.br");



// Upload

define("UPLOAD_SIZE", "1024000"); //512 KB
define("ALTURA_IMAGE", "800"); //480 PX
define("LARGURA_IMAGE", "600"); //640 PX
define("LARGURA_THUMBNAIL", "200"); //200 PX



// Paginação

define("NOTICIAS_PAGINA", "20");



// Extenções de Imagem Permitidas

$ext_valida	= array( 'gif', 'jpg', 'bmp', 'png' );



// MODELOS

define("MODELOS_PADROES", "'Branco','Noticiador'");
define("MODELOS_PORTAL","'layout_01','layout_02', 'layout_03'");



// Scripts

$array_css_files	= array("reset.css", "screen.css", "jquery-ui-1.8.14.custom.css","colorpicker.css","jquery.simpledialog.0.1.css", "fileuploader.css", "visualize.css", "visualize-light.css", "demo_table.css");
$array_js_files		= array("scripts.js","funcForms.js", "ajax.js", "scripts.dw.js", "jquery-1.5.1.min.js", "jquery.tablesorter.min.js", "jquery.tablesorter.pager.js", "ckeditor/ckeditor.js", "jquery-ui-1.8.14.custom.min.js", "jcarousellite_1.0.1.min.js", "colorpicker.js", "jquery.simpledialog.0.1.min.js", "jquery.visualize.js", "excanvas.js", "jquery.dataTables.min.js", "jquery.base64.min.js");



// Parâmetros Módulos

define("LIMITE_ATRAZO_ADIMPLENCIA", 0);



?>
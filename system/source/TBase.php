<?
class TBase {
	private $conexao = null;
	
	/* Seta qual objeto de conexao utilizar */
	function setConexao($obj) {
		$this->conexao = $obj;
	}
	
	/* Retorna o objeto de conexao utilizado */
	function getConexao() {
		return $this->conexao;
	}
	
	/* Verifica se foi indicado um objeto de conexao */
	function getConexaoOK() {
		if(isset($this->conexao)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function getUltimoId($tabela, $campo) {
		$intRetorno = 0;
																																	   
		$strSQL = "SELECT max($campo) indice FROM $tabela";
																															   
		$this->conexao->setSQL($strSQL);
		$arrResultado = $this->conexao->setAbrirSQL();
																															   
		if(isset($arrResultado['INDICE'][0])) {
				$intRetorno = $arrResultado['INDICE'][0];
		} else {
				$intRetorno = 0;
		}
																															   
		return $intRetorno;
	}

    function getBanco(){
        $banco = mysqli_connect(DB_SERVER, DB_USER, DB_PASS,DB_DATASET);
        return $banco;
    }
	
 	function PegarPerfil($id_usuario){
	  	$banco = mysqli_connect(DB_SERVER, DB_USER, DB_PASS,DB_DATASET);
	  	
		//mysql_set_charset('ISO-8859-1', $conexao);
		$sqlusuario = "select perfil_id from perfil_usuario where usuario_id=".$id_usuario."";
		/* $this->objConexao->setSQL($sqlusuario);
		$arrResultado = $this->objConexao->setAbrirSQL();

		if(isset($arrResultado['INDICE'][0])) {
			$intperfil = $arrResultado['INDICE'][0];
		} else {
			$intperfil = 0;
		}
		
		return $intperfil;*/
		$query = mysqli_query($banco,$sqlusuario);	 
		$arrResultado = mysqli_fetch_row($query);	
	 	if(isset($arrResultado[0])) {
			$intperfil = $arrResultado[0];
		} else {
			$intperfil = 0;
		}
	  				
		return $intperfil;	
				
	}
	
	
	
}
?>
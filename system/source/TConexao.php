<?
class TConexao {
	private $FConexao 	= null;
	private static $FInstance = null;
	private $FLastID	= null;

	public static function getInstance() {
		if (self::$FInstance == null) {
			debug("Criando nova instancia de conexao");
			self::$FInstance = new TConexao();
		}
		//print "getInstance<br><br>";
		return self::$FInstance;
	}

	#	Constructor
	private function __construct ( ) {
	}

	function getLastID() {
		return $this->FLastID;
	}

	function abrir () { 
		debug("Tentando iniciar conexao");
		if ($this->isConectado())
			$this->fechar();
			
		switch(DB_TYPE) {
			case "MYSQL":
				// Inicia a conexao com o mysql
				$this->FConexao = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, $this->FConexao);// or die ("Não foi possível se conectar à base de dados.<br />Por favor, tente novamente mais tarde.<br />Caso o erro persista, entre em contato com nosso suporte técnico. (Erro: ".mysql_errno($this->FConexao).")<br /><br />Obrigado.");
				mysqli_select_db($this->FConexao,DB_DATASET);// or die ("Não foi possível se conectar à base de dados.<br />Por favor, tente novamente mais tarde.<br />Caso o erro persista, entre em contato com nosso suporte técnico. (Erro: ".mysql_errno($this->FConexao).")<br /><br />Obrigado.");
				
				break;
		}
	}
	
	function fechar() {
		debug("Tentando encerrar conexao");
		
		switch(DB_TYPE) {
			case "MYSQL":
				mysqli_close($this->FConexao);
				break;
		}
		
		$this->FConexao = null;
		debug("Conexão encerrada");
	}
	
	function getErro() {
		switch(DB_TYPE) {
			case "MYSQL":
				// Inicia a conexao com o mysql
				return htmlentities(mysqli_error($this->FConexao), ENT_QUOTES);
				break;   
		}
	}
	
	function isConectado() {
		return $this->FConexao != null;
	}
	
	function getConexao() {
		if (!$this->isConectado()) {
			$this->abrir();
		}
		
		return $this->FConexao;
	}
	
	function executeNonQuery($SQL){
		debug("Executando non query ".$SQL);
		$connection = $this->getConexao();
		switch(DB_TYPE) {
			case "MYSQL":
				$result = $connection->query($SQL);
				if ($result === false){
					
					echo "<pre>";
					echo $SQL;
					echo "</pre>";

					echo "<font color=\"red\">Não foi possivel executar1 a query: " . mysqli_error($connection)."<br/></font>";

					echo "<pre>";
					print_r(mysqli_error_list($connection));
					echo "</pre>";
				}

				$this->FLastID = mysqli_insert_id($this->FConexao);
				 return mysqli_affected_rows($result);
				break;
		}
	}
	
	function executeQuery($SQL){
		debug("Executando ".$SQL);
       $connection = $this->getConexao();
		switch(DB_TYPE) {
			case "MYSQL":
				$result = $connection->query($SQL);
				// var_dump($SQL);
				
				if ($result === false){

					echo "<pre>";
					echo $SQL;
					echo "</pre>";

					echo "<font color=\"red\">Não foi possivel executar1 a query: " . mysqli_error($connection)."<br/></font>";

					echo "<pre>";
					print_r(mysqli_error_list($connection));
					echo "</pre>";
				}
				
					
				$dataTable = new TDataTable($result);
				mysqli_free_result($result);
				
				return $dataTable;
				break;
				// die();
		}
	}
	
}
?>
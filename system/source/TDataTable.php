<?
#doc
#	classname:	TDataTable
#	scope:		PUBLIC
#
#/doc

class TDataTable {
	private $FRows	= array();
	
	function __construct ( $resource ) {
		switch(DB_TYPE){
			case "MYSQL":
				while ($row = mysqli_fetch_assoc($resource)) {
					$this->FRows[] = new TDataRow($row);
				}
				break;
		}
	}
	
	function Rows() {
		return $this->FRows;
	}
	
	function RowCount() {
		return count($this->FRows);
	}
	
	function bind($object, $line) {
		throw new Exception("Nao implementado");
	}
	
	function getRow($pos) {
		return $this->FRows[$pos];
	}
}
?>
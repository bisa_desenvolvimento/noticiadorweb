<?php



function obterNomesDeArquivos($strRootDir, $boolIsRecursive = false) {

	$arrStrNamesElements = scandir($strRootDir);

	$arrStrNamesFiles = array();

	foreach ($arrStrNamesElements as $strElements) {
	    if (!is_dir($strRootDir.$strElements)) {
	        $arrStrNamesFiles[] = $strRootDir.$strElements;
	    } else {
	        if ($boolIsRecursive) {
	        	if (($strElements != ".") && ($strElements != "..") && ($strElements != ".svn")) {
	        		$arrStrNamesFiles = array_merge($arrStrNamesFiles, obterNomesDeArquivos($strRootDir.$strElements."/", true));
	            }
	        }
	    }
	}

	return $arrStrNamesFiles;
}



// Classes

//print "files util<br><pre>";
//print_r(obterNomesDeArquivos(DIR_UTIL, true));
//print "</pre><br>";

foreach (obterNomesDeArquivos(DIR_UTIL, true) as $strNameFile) {
	if (file_exists($strNameFile))
		require_once($strNameFile);
}

//print "files source<br><pre>";
//print_r(obterNomesDeArquivos(DIR_SOURCE, true));
//print "</pre><br>";

foreach (obterNomesDeArquivos(DIR_SOURCE, true) as $strNameFile) {
	if (file_exists($strNameFile))
		require_once($strNameFile);
}

//print "files engine<br><pre>";
//print_r(obterNomesDeArquivos(DIR_ENGINE, true));
//print "</pre><br>";

foreach (obterNomesDeArquivos(DIR_ENGINE, true) as $strNameFile) {
	if (file_exists($strNameFile))
		require_once($strNameFile);
}

//print "files modules<br><pre>";
//print_r(obterNomesDeArquivos(DIR_MODULES, true));
//print "</pre><br>";

/*
foreach (obterNomesDeArquivos(DIR_MODULES, true) as $strNameFile) {
	if (file_exists($strNameFile))
		require_once($strNameFile);
}
*/

//print "files model<br><pre>";
//print_r(obterNomesDeArquivos(DIR_MODEL, true));
//print "</pre><br>";

foreach (obterNomesDeArquivos(DIR_MODEL, true) as $strNameFile) {
	//print "file $strNameFile<br>";
	if (file_exists($strNameFile))
		require_once($strNameFile);
}

//print "files view<br><pre>";
//print_r(obterNomesDeArquivos(DIR_VIEW, true));
//print "</pre><br>";

foreach (obterNomesDeArquivos(DIR_VIEW, true) as $strNameFile) {
	//print "file $strNameFile<br>";
	if (file_exists($strNameFile))
		require_once($strNameFile);
}

//print "files control<br><pre>";
//print_r(obterNomesDeArquivos(DIR_CONTROL, true));
//print "</pre><br>";

foreach (obterNomesDeArquivos(DIR_CONTROL, true) as $strNameFile) {
	//print "file $strNameFile<br>";
	if (file_exists($strNameFile))
		require_once($strNameFile);
}



// Tradução

if (isset($_GET["lang"]))
	$lang = $_GET["lang"];
else if (isset($_POST["lang"]))
	$lang = $_POST["lang"];



// Conexão é Base de Dados

$conexao = TConexao::getInstance();
$conexao -> abrir();



?>
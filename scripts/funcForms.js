
/***************************
EDGARDO BALDUCCIO FALBO
05/2003 - 06/2003
PROJETO NAJ
Métodos cliente de Formulário.
***************************/


var dbacao='DB_INSERT';

// Validar Hora
var reTime1 = /^\d{2}:\d{2}$/;
var reTime2 = /^([0-1]\d|2[0-3]):[0-5]\d$/;
var reTime3 = /^(0[1-9]|1[0-2]):[0-5]\d$/;
var reTime4 = /^\d+:[0-5]\d:[0-5]\d$/;
var reTime5 = /^\d+:[0-5]\d:[0-5]\.\d{3}\d$/;

function doTime(pStr, pFmt, objeto)
{
	eval("reTime = reTime" + pFmt);
	if (reTime.test(pStr)) {
		return true
	} else if (pStr != null && pStr != "") {
		alert(pStr + " NÃO é um horário/tempo válido.");
		objeto.focus();
	}
} // doTime
//FORMATA SALÁRIO

function testeIntervaloDate(dataInicio, dataFim)
		{
		  
		  datDate1= dataInicio.value
		  datDate1=datDate1.substring(6,10)+datDate1.substring(3,5)+datDate1.substring(0,2);
		  datDate2=dataFim.value;
		  datDate2=datDate2.substring(6,10)+datDate2.substring(3,5)+datDate2.substring(0,2);

		
		  if((parseInt(datDate2)-parseInt(datDate1))>=0 ) 
		  {
		  	return true;
		  }
		  else if (dataInicio.value=='')
		  {
		  	return true;
		  }else
		  {
		  	alert("Data menor que a Primeira Data");
			dataFim.focus();
			return false;
		  }
		}

function verificaPrimeiradata(dataInicio, dataFinal){
	if (dataFinal.value!=''){
			if (dataInicio.value==''){
				alert('Énecessário informar as duas datas ou nenhuma!');
				dataInicio.focus();
				return false;
				}
		}
	return true;
}
							
function formata_salario_real(cur,len) 
{ 
   n='__0123456789'; 
   d=cur.value; 
   l=d.length; 
   r=''; 
   if (l > 0) 
   { 
    z=d.substr(0,l-1); 
    s=''; 
    a=2; 
    for (i=0; i < l; i++) 
    { 
        c=d.charAt(i); 
        if (n.indexOf(c) > a) 
        { 
            a=1; 
            s+=c; 
        }; 
    }; 
    l=s.length; 
    t=len-1; 
    if (l > t) 
    { 
        l=t; 
        s=s.substr(0,t); 
    }; 
    if (l > 2) 
    { 
        r=s.substr(0,l-2)+','+s.substr(l-2,2); 
    } 
    else 
    { 
        if (l == 2) 
        { 
            r='0,'+s; 
        } 
        else 
        { 
            if (l == 1) 
            { 
                r='0,0'+s; 
            }; 
        }; 
    }; 
    if (r == '') 
    { 
        r='0,00'; 
    } 
    else 
    { 
        l=r.length; 
        if (l > 6) 
        { 
            j=l%3; 
            w=r.substr(0,j); 
            wa=r.substr(j,l-j-6); 
            wb=r.substr(l-6,6); 
            if (j > 0) 
            { 
                w+='.'; 
            }; 
            k=(l-j)/3-2; 
            for (i=0; i < k; i++) 
            { 
                w+=wa.substr(i*3,3)+'.'; 
            }; 
            r=w+wb; 
        }; 
    }; 
   }; 
   if (r.length <= len) 
   { 
    cur.value=r; 
   } 
   else 
   { 
    cur.value=z; 
   }; 
   return true; 
}; 

//FUNÇÕES INUTEIS
/* SO PRA VITALMED ESTA FUNCAO */
function validaSecNum(){
	if(document.forms[0].txtCaracteres.value == document.forms[0].confi.value){
		return true;
	}
	else{
		alert('O número ingressado não coincide com o número de segurança.')
		return false;
	}
}
/* ATE AQUI */

function getSelectedButton(buttonGroup){
	for ( var i = 0; i < buttonGroup.length; i++){
		if (buttonGroup[i].checked){
			return i;	
		}
	}
	return 0;
}

function soNumerico(e)
{
	if (e.keyCode < 48 || e.keyCode > 57)
			e.keyCode = 0;
}


function QualquerMascara(src, mask) 
{
  var i = src.value.length;
  var saida = mask.substring(0,1);
  var texto = mask.substring(i)
if (texto.substring(0,1) != saida) 
  {
	src.value += texto.substring(0,1);
  }
}


function validaForm(indForm,lang) {
/*
Valida todos los inputs do formulário de tipo texto e password (que não estejam vazios)
parametros:
indForm = indice do formulário, do array de formulários da página (1º -> 0, 2º -> 1, etc.).
*/
   
	var validate = true;
	var frm = document.forms[indForm];
	for (i = 0; i < frm.elements.length; i++){
		if ((frm.elements[i].type == "text" || frm.elements[i].type == "password" || frm.elements[i].type == "textarea") && trim(frm.elements[i].value) == "" && frm.elements[i].required=='true'){
			
                         if(lang=="pt-BR"){
                         alert("Por favor, preencha o campo " + frm.elements[i].nomeDesc);
                         }
                         if(lang=="es-ES"){
                         alert("Por favor llene el campo " + frm.elements[i].nomeDesc);           
                         }
			frm.elements[i].focus();
			validate = false;
			break
		}
		
		
		if(frm.elements[i].type == "textarea"){
			total = 999999; 
			tam = frm.elements[i].value.length; 
			if (tam > total){ 
				aux = frm.elements[i].value; 
                            if(lang=="pt-BR"){
                            alert('São permitidos no máximo 999.999 caracteres no campo ' + frm.elements[i].nomeDesc +'.');
                            }
                            if(lang=="es-ES"){
                            alert('Son permitidos un maximo de 999.999 caracteres en el campo ' + frm.elements[i].nomeDesc +'.');           
                            }
                                frm.elements[i].focus();
				validate = false;
				break
			} 
		}

		if(frm.elements[i].type == "text" && frm.elements[i].isDate=="true" && trim(frm.elements[i].value)!=""){
			if(!isValidDate(frm.elements[i].value)){
				frm.elements[i].focus();
				validate = false;
				break
			}
		}

		if(frm.elements[i].type == "text" && frm.elements[i].isHour=="true" && trim(frm.elements[i].value)!=""){
			if(!isValidHora(frm.elements[i].value)){
				frm.elements[i].focus();
				validate = false;
				break
			}
		}

		if(frm.elements[i].type == "text" && frm.elements[i].isMail=="true" && trim(frm.elements[i].value)!=""){
			if(!validateEmail(frm.elements[i])){
				
                                if(lang=="pt-BR"){
                                
				alert("Endereço inválido no campo " + frm.elements[i].nomeDesc);
				}
                                if(lang=="es-ES"){
                                alert("Dirección inválida en el campo " + frm.elements[i].nomeDesc);           
                                }
            
            
				frm.elements[i].focus();
				validate = false;
				break
			}
		}

		if(frm.elements[i].type == "radio" && frm.elements[i].required=='true'){
			// Para incluir um radio button nessa lista, é necessário marcar somente o primeiro como required

			nomeDesc = frm.elements[i].nomeDesc;
			objRadio = frm.all[frm.elements[i].name];
			tamanho = objRadio.length;
			marcado = false;

			for(indice=0; indice < tamanho; indice++){
				if(objRadio[indice].checked == true){
					marcado = true;
				}
			}

			if(marcado == false){
			   
            
				if(lang=="pt-BR"){
				alert("Énecessário marcar uma das opções do campo " + nomeDesc);
				}
                                if(lang=="es-ES"){
                                alert("Es necesario marcar una de las opciones de campo " + nomeDesc);           
                                }
				
				frm.elements[i].focus();
				validate = false;
			
			}
		}

		if(frm.elements[i].type == "select-one" && frm.elements[i].required=='true'){
			if(frm.elements[i].value=="0"){
			   
                                if(lang=="pt-BR"){
                                alert("Por favor, selecione " + frm.elements[i].nomeDesc);
                                }
                                if(lang=="es-ES"){
                                alert("Por favor, seleccione " + frm.elements[i].nomeDesc);           
                                }
				frm.elements[i].focus();
				validate = false;
				
			}
		}

		if(frm.elements[i].type == "select-multiple" && frm.elements[i].required=='true'){
			if(frm.elements[i].length == 0){
				
                                if(lang=="pt-BR"){
                                alert("Por favor, selecione " + frm.elements[i].nomeDesc);
                                }
                                if(lang=="es-ES"){
                                alert("Por favor, seleccione " + frm.elements[i].nomeDesc);           
                                }
				frm.elements[i].focus();
				validate = false;
				
			}
		}

		if(frm.elements[i].isCPF=='true' && frm.elements[i].value!=''){
			if(!validarCPF_CNPJ(frm.elements[i].value)){
				//alert("Confira o CPF/CNPJ, o preenchido não é valido.")
				frm.elements[i].focus();
				validate = false;

			}
		}
        }
		
	return validate;
}


function emailCheck (emailStr) {

var emailPat=/^(.+)@(.+)$/
var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]"
var validChars="\[^\\s" + specialChars + "\]"
var quotedUser="(\"[^\"]*\")"
var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/
var atom=validChars + '+'
var word="(" + atom + "|" + quotedUser + ")"
var userPat=new RegExp("^" + word + "(\\." + word + ")*$")
var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")
var matchArray=emailStr.match(emailPat)

if (matchArray==null) {
	return false
}
var user=matchArray[1]
var domain=matchArray[2]

if (user.match(userPat)==null) {
    return false
}

var IPArray=domain.match(ipDomainPat)
if (IPArray!=null) {
	  for (var i=1;i<=4;i++) {
	    if (IPArray[i]>255) {
		return false
	    }
    }
    return true
}

var domainArray=domain.match(domainPat)
if (domainArray==null) {
    return false
}

var atomPat=new RegExp(atom,"g")
var domArr=domain.match(atomPat)
var len=domArr.length
if (domArr[domArr.length-1].length<2 || 
    domArr[domArr.length-1].length>3) {
    return false
}

if (len<2) {
   //var errStr="E-mail inválido"
   //alert(errStr)
   return false
	}
return true;
}

function setAcao(sAcao,validate,indForm,doSubmit){
/* 
setea a ação do formulário segundo o caso ('DB_INSERT', 'DB_DELETE', 'DB_UPDATE')
parametros:
  sAcao = ação a realizar
  validate = se precisa que se validem os inputs com 'validaForm', 'validate' sim, '!validate' não valida
  indForm = indice do formulario no array de formularios da pagina (1º -> 0, 2º -> 1, etc.)
  doSubmit = 'true' -> submite o formulário, 'false' ou qualquer outro valor, não.
*/

	document.forms[indForm].acao.value = sAcao;
  //alert(document.forms[indForm].acao.value);
  if(validate == 'validate'){
    if(validaForm(indForm))
      if(doSubmit=='true')
        document.forms[indForm].submit();
		return true;
  }
  else{
    if(doSubmit=='true')
      document.forms[indForm].submit();
  }
}

function killReg(cod,msg){
/*
manda os parâmetros para o servlet para eliminar o registro com o Id -> cod
parametros:
cod = codigo
msg = mensagem de confirmação
*/
  if(confirm(msg)){
    var frm = document.forms[0];
    frm.codigo.value = cod;
    frm.acao.value = 'DB_DELETE';
    frm.submit();
  }
}

function editReg(cod,desc){
/*
prepara o formulário para edição
parametros:
cod = codigo do registro que vai ser editado
desc = nova descrição
*/
  var frm = document.forms[0];
  //setAcao('DB_UPDATE','',0,'false');
  dbacao = 'DB_UPDATE';
  frm.grupo.value = desc;
  frm.codigo.value = cod;
  frm.botIncluir.value = 'Alterar Nome';
  divAltera.style.display = 'none';
  divCancel.style.display = 'block';
}

function cancelEdit(indForm){
 	var frm = document.forms[indForm];
	for (i = 0; i < frm.elements.length; i++) {
		if ((frm.elements[i].type == "text" || frm.elements[i].type == "password")){
      frm.elements[i].value='';
		}
    if(frm.elements[i].type.indexOf("select")!=-1 && frm.elements[i].required=='true'){
      frm.elements[i].value=0
    }
	}
	divAltera.style.display = 'block';
  divCancel.style.display = 'none';
  dbacao = 'DB_INSERT';
  frm.botIncluir.value = 'Inserir';
}

function passCheckParameters(objCheck,varToPass,doSubmit)
{
varToPass.value = "";
var a = objCheck.length;
  for(var b=0;b<a;b++)
  {
    if(objCheck[b].checked)
      varToPass.value += objCheck[b].value + ",";
  }
  //var c = varToPass.value;
  if(varToPass.value.substring((varToPass.value).length - 1,(varToPass.value).length) == ",")
    varToPass.value = (varToPass.value).substring(0,(varToPass.value).length - 1);
  //alert(varToPass.value);
  if(doSubmit)
  	document.forms[0].submit();
}

function valida_mask(obj,mask){
  str = mask;
  fim = str.length;
  valor = obj.value;
 	posicao = valor.length-1;
  posicao2 = valor.length;
  tipo = str.charAt(posicao);
  if (tipo == "a") {
    if ((valor.charCodeAt(posicao) >= 48) && (valor.charCodeAt(posicao) <= 57) ||
		(valor.charCodeAt(posicao) >= 65) && (valor.charCodeAt(posicao) <= 90) ||
		(valor.charCodeAt(posicao) >= 97) && (valor.charCodeAt(posicao) <= 122)) {
				tipo = str.charAt(posicao+1);
				if ((tipo!="a") && (tipo!="9")) {
					 obj.value=obj.value+tipo;
				}
				return true;
	  }else{ 
			obj.value = obj.value.substr(0,posicao);
	  }
  }else {
		 if (tipo == "9") {
		 		if ((valor.charCodeAt(posicao) >= 48) && (valor.charCodeAt(posicao) <= 57)) {
					 tipo = str.charAt(posicao+1);
					 if ((tipo!="a") && (tipo!="9") ) {
					 		obj.value=obj.value+tipo;
					 }
				}else{ 
					 obj.value = obj.value.substr(0,posicao);
				}
		 }else {
		 		 if (posicao==0 && tipo!=obj.value){
				 		obj.value=tipo + obj.value
				 }else{	
		 	 	 		if (tipo==valor.charAt(posicao)) {
				 			 return true;		
         		}else{
				 			 obj.value = obj.value.substr(0,posicao);
				    }
				 }
		 }
  }	  
  obj.value = obj.value.substr(0,fim-1);
   return true;
}

 function isValidDate(dateStr) 
 {

 if(dateStr=='')
    return true;
// Checks for the following valid date formats: MM/DD/YY   MM/DD/YYYY   MM-DD-YY   MM-DD-YYYY
	var mes_atual = new Date().getMonth() + 1;
	var ano_atual = new Date().getYear();
	var nr = "1234567890";
    var simb ="/";

 	if (dateStr.length == 10)
 	{       
    	for (ii=0;ii < dateStr.length;ii++)
        {
        	st = dateStr.substring(ii,ii+1);
            if ((ii == 2) || (ii == 5))
            {
            	if (simb.indexOf(st) < 0)
                {
                      	
                  if(lang=="pt-BR"){
      				      alert("Data preenchida incorretamente!");
      				}
                  if(lang=="es-ES"){
                        alert("Fecha introducida incorrectamente!");
                  }
             
                    return false;
                }
             }
             else
             {
             	if (nr.indexOf(st) < 0)
                {
                	if(lang=="pt-BR"){
      				      alert("Data deve ser numérica!");
      				}
                  if(lang=="es-ES"){
                        alert("La fecha debe de ser numérica!");
                  }
                   
                    return false;
                }
             }
        }
    }
    else
    {
      
      if(lang=="pt-BR"){
      alert("A data deve ser no formato 'dd/mm/aaaa'");
      }
      if(lang=="es-ES"){
      alert("La fecha debe de ser en formato 'dd/mm/aaaa'");
      }
    	
        return false;
    }
	
	var datePat = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{4})$/; // requires 4 digit year
	var matchArray = dateStr.match(datePat); // is the format 	
	var day = matchArray[1]; // parse date into variables
	var month = matchArray[3];
	var year = matchArray[4];
	
	if (month < 1 || month > 12) 
	{ // check month range
      if(lang=="pt-BR"){
      alert("Más deve ser entre 1 e 12.");
      }
      if(lang=="es-ES"){
      alert("El mes debe de ser entre 1 y 12.");
      }
		
		return false;
	}
	if (day < 1 || day > 31) 
	{
      if(lang=="pt-BR"){
      alert("Dia deve ser entre 1 e 31.");
      }
      if(lang=="es-ES"){
      alert("El Da debe de ser entre 1 y 31.");
      }
		
		return false;
	}
	if ((month==4 || month==6 || month==9 || month==11) && day==31) 
	{
		if(lang=="pt-BR"){
      alert("El Mes "+month+" não tem 31 dias !");
      }
      if(lang=="es-ES"){
      alert("El Mes "+month+" no tiene 31 dias !");
      }
      
		return false;
	}
	if (month == 2) 
	{ // check for february 29th
		var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
		if (day>29 || (day==29 && !isleap)) 
		{
			
         if(lang=="pt-BR"){
         alert("Fevereiro de " + year + " não tem " + day + " dias !");
         }
         if(lang=="es-ES"){
         alert("Febrero de " + year + " no tiene " + day + " días !");
         }
         
			return false;
	    }
	}
	if (year < 1900) 
	{
	   
      if(lang=="pt-BR"){
         alert("O ano deve ser superior a 1900.");
         }
         if(lang=="es-ES"){
         alert("El año debe ser superior a 1900.");
         }
		
		return false;
	}
				
	return true;	
}


function validarCPF_CNPJ(strCPF){
   var elimina_texto = strCPF;
   var texto_limpo   = "";
   for (var k = 0; k <= elimina_texto.length; k++) {
       if (elimina_texto.charAt(k) >=0 & elimina_texto.charAt(k) <=9)
           texto_limpo = texto_limpo + elimina_texto.charAt(k);
   }
   strCPF = texto_limpo;
   var carac  = texto_limpo.length;
   if (carac == 11){ 
		return testa_cpf(texto_limpo);
   }
   if (carac == 14){ 
		return testa_cnpj(texto_limpo);
   }
   else {
     
         if(lang=="pt-BR"){
         window.alert("Digite um numero correto para validar o CPF (11 numeros) ou /CNPJ (14 numeros)");
         }
         if(lang=="es-ES"){
         window.alert("Digite un número correcto para validar el CPF (11 números) o /CNPJ (14 números)");
         }
     
     
     //obj.value = '';
	 //obj.focus();
	 return false;
   }
}

function testa_cpf(verificar){
   var digito = verificar.toString();
 // DIGITO 1
   var soma1 = 0;
       soma1 = soma1 + 10 * digito.charAt(0);
       soma1 = soma1 +  9 * digito.charAt(1);
       soma1 = soma1 +  8 * digito.charAt(2);
       soma1 = soma1 +  7 * digito.charAt(3);
       soma1 = soma1 +  6 * digito.charAt(4);
       soma1 = soma1 +  5 * digito.charAt(5);
       soma1 = soma1 +  4 * digito.charAt(6);
       soma1 = soma1 +  3 * digito.charAt(7);
       soma1 = soma1 +  2 * digito.charAt(8);
   var divisao1    = soma1 / 11;
   var multiplica1 = parseInt(divisao1,10) * 11;
   var resto1      = soma1 - multiplica1;
   if (resto1 <= 1) {var digi1 = 0} else {var digi1 = 11 - resto1}
 // DIGITO 2
   var soma2 = 0;
       soma2 = soma2 + 11 * digito.charAt(0);
       soma2 = soma2 + 10 * digito.charAt(1);
       soma2 = soma2 +  9 * digito.charAt(2);
       soma2 = soma2 +  8 * digito.charAt(3);
       soma2 = soma2 +  7 * digito.charAt(4);
       soma2 = soma2 +  6 * digito.charAt(5);
       soma2 = soma2 +  5 * digito.charAt(6);
       soma2 = soma2 +  4 * digito.charAt(7);
       soma2 = soma2 +  3 * digito.charAt(8);
       soma2 = soma2 +  2 * digi1       
   var divisao2    = soma2 / 11
   var multiplica2 = parseInt(divisao2,10) * 11
   var resto2      = soma2 - multiplica2
   if (resto2 <= 1) {var digi2 = 0} else {var digi2 = 11 - resto2}
   if (digito.charAt(9) == digi1.toString() & digito.charAt(10) == digi2.toString()) {return(true)
   } else {window.alert("CPF inválido.");return(false)}}

function testa_cnpj(verificar){
   var digito = verificar.toString()
 // DIGITO 1
   var soma1 = 0
       soma1 = soma1 + 5 * digito.charAt(0)
       soma1 = soma1 + 4 * digito.charAt(1)
       soma1 = soma1 + 3 * digito.charAt(2)
       soma1 = soma1 + 2 * digito.charAt(3)
       soma1 = soma1 + 9 * digito.charAt(4)
       soma1 = soma1 + 8 * digito.charAt(5)
       soma1 = soma1 + 7 * digito.charAt(6)
       soma1 = soma1 + 6 * digito.charAt(7)
       soma1 = soma1 + 5 * digito.charAt(8)
       soma1 = soma1 + 4 * digito.charAt(9)
       soma1 = soma1 + 3 * digito.charAt(10)
       soma1 = soma1 + 2 * digito.charAt(11)
   var divisao1    = soma1 / 11
   var multiplica1 = parseInt(divisao1,10) * 11
   var resto1      = soma1 - multiplica1
   if (resto1 <= 1) {var digi1 = 0} else {var digi1 = 11 - resto1}
 // DIGITO 2
   var soma2 = 0
       soma2 = soma2 + 6 * digito.charAt(0)
       soma2 = soma2 + 5 * digito.charAt(1)
       soma2 = soma2 + 4 * digito.charAt(2)
       soma2 = soma2 + 3 * digito.charAt(3)
       soma2 = soma2 + 2 * digito.charAt(4)
       soma2 = soma2 + 9 * digito.charAt(5)
       soma2 = soma2 + 8 * digito.charAt(6)
       soma2 = soma2 + 7 * digito.charAt(7)
       soma2 = soma2 + 6 * digito.charAt(8)
       soma2 = soma2 + 5 * digito.charAt(9)
       soma2 = soma2 + 4 * digito.charAt(10)
       soma2 = soma2 + 3 * digito.charAt(11)
       soma2 = soma2 + 2 * digi1       
   var divisao2    = soma2 / 11
   var multiplica2 = parseInt(divisao2,10) * 11
   var resto2      = soma2 - multiplica2
   if (resto2 <= 1) {var digi2 = 0} else {var digi2 = 11 - resto2}
   if (digito.charAt(12) == digi1.toString() & digito.charAt(13) == digi2.toString()){return(true)
   } else {window.alert("CNPJ inválido.");return(false)}}


function trim(s){
	s = s.replace(/^\s*/,'').replace(/\s*$/, '');
	return s;
}

function processDataChk(chkBox,servlet,dbAcao){
	if(document.getElementById('tbl').rows.length==1){
		
         if(lang=="pt-BR"){
         alert('O resultado da busca esta vazio.');
         }
         if(lang=="es-ES"){
         alert("El resultado de busqueda está vacio");
         }
      
		return;
	}
	else if(chkBox){
		var chk = false;
		if(chkBox.length>1){
			for(var f=0;f < chkBox.length;f++){
				if(chkBox[f].checked)
					chk = true;
			}
			if(!chk){
				
         if(lang=="pt-BR"){
         alert('Não há nenhum item escolhido.')
         }
         if(lang=="es-ES"){
           alert('No ha escogido ningún elemento.')
         }
          
            
				return;
			}	
		}
		else{
			if(!chkBox.checked){
			
         
         if(lang=="pt-BR"){
         alert('Não há nenhum item escolhido.')
         }
         if(lang=="es-ES"){
           alert('No ha escogido ningún elemento.')
         }
				return;
				}	
			}
	}

		document.forms[0].action=servlet;
		document.forms[0].acao.value = dbAcao;
		document.forms[0].submit();
}

function soNumerico(e,decimal,texto){
	if(!decimal){
		if(e.keyCode < 48 || e.keyCode > 57)
			e.keyCode = 0;
	}else{

		if((e.keyCode == 44 || e.keyCode == 46) && texto.indexOf(".")==-1){
			e.keyCode = 46;
		}
		else if((e.keyCode == 44 || e.keyCode == 46) && texto.indexOf(".")!=-1){
			e.keyCode = 0;
		}
		else if(e.keyCode < 48 || e.keyCode > 57){
			e.keyCode = 0;
		}

	}
}

function toUpper(e){
	if ((e.keyCode >= 97 && e.keyCode <= 122) || (e.keyCode >= 224 && e.keyCode <= 249)) {
	e.keyCode = e.keyCode - 32	
	}
}

function validateEmail(obj) {//campo opcional
	var emailStr = obj.value;

	if (obj.value.length > 0){
		var reg1 = /(@.*@)|(\.\.)|(@\.)|(\.@)|(^\.)/; // not valid
		var reg2 = /^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?)$/; // valid

		if ( !reg1.test( emailStr ) && reg2.test( emailStr ) ) {
			return true;
		}
		else {
	//		alert(msg);
			obj.focus();
			return false;
		}
	}
	else{
		return true;
	}

}


var itemsSelecionadosAjax = new Array();
var ordem = 1;
function createAjaxObject() {
	var http_request = false;

	if (window.XMLHttpRequest) { // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType) {
			http_request.overrideMimeType('text/xml');
		}
	} else if (window.ActiveXObject) { // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				http_request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {}
		}
	}
	
	return http_request;
}

function updateList(file, action, div, filtro) {
	var requestUrl = "index.php?secao=" + file + "&action=" + action + "&filtro=" + filtro + "&type=ajax";
	
	if (itemsSelecionadosAjax != null) {
		if (itemsSelecionadosAjax.length > 0)
			requestUrl += "&selected=" + itemsSelecionadosAjax;
	}
	
	var objDiv = document.getElementById(div);
	
	ajaxObj = createAjaxObject();
	
	ajaxObj.onreadystatechange = function() {
		if (ajaxObj.readyState == 4) {
            if (ajaxObj.status == 200) {
                objDiv.innerHTML = ajaxObj.responseText;
            } else {
                alert('N�o foi possivel pegar o request de volta.');
            }
		};
	}
	
	ajaxObj.open('GET', requestUrl, true);
	ajaxObj.send(null);
}

function updateListComData(file, action, div, filtro,inicio,fin) {
	var requestUrl = "index.php?secao=" + file + "&action=" + action + "&filtro=" + filtro + "&inicio=" + inicio+ "&fin="+ fin + "&type=ajax";
	
	if (itemsSelecionadosAjax != null) {
		if (itemsSelecionadosAjax.length > 0)
			requestUrl += "&selected=" + itemsSelecionadosAjax;
	}
	
	var objDiv = document.getElementById(div);
	
	ajaxObj = createAjaxObject();
	
	ajaxObj.onreadystatechange = function() {
		if (ajaxObj.readyState == 4) {
            if (ajaxObj.status == 200) {
                objDiv.innerHTML = ajaxObj.responseText;
            } else {
                alert('N�o foi possivel pegar o request de volta.');
            }
		};
	}
	
	ajaxObj.open('GET', requestUrl, true);
	ajaxObj.send(null);
}

function despacharComAjax(file, action, div,titulo,destino,modelo,noticias,formaenvio,emails,areas) {
	var requestUrl = "index.php?secao=" + file + "&action=" + action + "&txtTitulo=" + titulo + "&rdDestino=" + destino  + "&rdModelo=" + modelo+ "&rdNoticia=" + noticias + "&ddlFormaDeEnvio=" + formaenvio + "&txtEmails=" + emails + "&rdAreas=" + areas+  "&type=ajax";
	
	
	var objDiv = document.getElementById(div);
	
	ajaxObj = createAjaxObject();
	
	ajaxObj.onreadystatechange = function() {
		if (ajaxObj.readyState == 4) {
            if (ajaxObj.status == 200) {
                objDiv.innerHTML = ajaxObj.responseText;
            } else {
                alert('N�o foi possivel pegar o request de volta.');
            }
		};
	}
	
	ajaxObj.open('GET', requestUrl, true);
	ajaxObj.send(null);
}

function despacharComAjax3(file, action, div,titulo,destino,modelo,noticias,formaenvio,emails,areas){
    $.get("index.php?secao=" + file + "&action=" + action + "&txtTitulo=" + titulo + "&rdDestino=" + destino  + "&rdModelo=" + modelo+ "&rdNoticia=" + noticias + "&ddlFormaDeEnvio=" + formaenvio + "&txtEmails=" + emails + "&rdAreas=" + areas+  "&type=ajax", { "func": "getNameAndTime" },
   function(data){
     alert(data.contador.value); // John
     console.log(data.total.value); //  2pm
     //alert(data.test);
     
   }, "json");  
   
}

function addCheckBoxSelecionadoSelecionados(obj) {
	if (obj.checked) {
		addToArray(obj.value, itemsSelecionadosAjax);
	} else {
		removeFromArray(obj.value, itemsSelecionadosAjax);
	}
}

function addToArray(item, array) {
	for(i = 0; i < array.length; i++)
		if (array[i] == item)
			return false;
               
	array.length++;
	array[array.length - 1] = item;
	
	return true;
                
        
}

function removeFromArray(item, array) {
	var newArray = new Array();
	
	for(i = 0; i < array.length; i++) {
		if (array[i] != item) {
			newArray.length++;
			newArray[newArray.length - 1] = array[i];
		}
	}
	
	array.length = 0;
	for(i = 0; i < newArray.length; i++) {
		array.length++;
		array[array.length - 1] = newArray[i];
	}
}


	function selecionarOrdem(objCheck, objText) {
		if (objCheck.checked) {
			document.getElementById(objText).value = ordem++;
		} else {
			document.getElementById(objText).value = '';
			ordem--;
		}
	}
        
        
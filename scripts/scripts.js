/**********   Limita Caracteres no Text Area   ***********/
function LimitarCaracter(qtd, campoMensagem, campoContador) { 

    Campo = document.getElementById(campoMensagem); 

    Display = document.getElementById(campoContador);     
    
    if(Campo.value.length > qtd){ 
		 
    } else {
        Caracteres = qtd - Campo.value.length;  
    }
    
     

    Display.innerHTML = Caracteres; 

    
} 

function limparTexto(texto){
	var pattern 	= "[A-Za-z0-9]";
	var i 			= 0;
	var buf 		= '';
	
	for (i = 0; i < texto.length; i++){
		if (texto.substr(i, 1).match(pattern))
			buf = buf.concat(texto.substr(i, 1));
	}
	
	return buf;

}

// onKeyUp="this.value = formatar(this.value,'99/99/9999')" 
function formatar(text, mask){
	var buf 		= '';

	text = limparTexto(text);
	
	// Verifica se o texto não é maior que a mascara	
	if (text.length > mask.length) {
		text = text.substring(0, mask.length);
	}
	
	//Indice dos caracteres da mascara
	var i;
	
	//Indice dos caracteres do texto
	var j = 0;
	
	var pattern = '';
	
	// Percorre a mascara
	for (i = 0; i < mask.length; i++){
		
		// Verifica se ainda existe palavra para verificar
		if (j < text.length){
			elementText = text.substr(j, 1);
			elementMask = mask.charAt(i);
			
			pattern = '';
			// Pesquisa por um filtro na mascara
			switch(elementMask){
				case 'A':
					pattern = '[A-Za-z]';
					break;
					
				case '#':
					pattern = '[A-Za-z0-9]';
					break;
					
				case '9':
					pattern = '[0-9]';
					break;
			}
			
			// Verifica se encontrou algum filtro na mascara
			if (pattern.length > 0){
				// Verifica se o elementoAtual do texto é válido
				if (elementText.match(pattern)) { 
						buf += elementText;
					}
					
				// Avanca para o proximo elemento do texto
				j++;		 
			} else {
				// Apenas adiciona o simbolo desconhecido da mascara no buffer
				buf = buf.concat(elementMask);
			}
			
			
		}
	}
	
	return buf;
}

function show(obj, arr) {
	for(i = 0; i < arr.length; i++) {
		tmp = document.getElementById(arr[i]);
		
		if (tmp != null) {
			tmp.style['visibility'] = "hidden";
			tmp.style['position'] = "absolute";

		}
	}
	
	tmp = document.getElementById(obj);
	
	if (tmp != null) {
		tmp.style['visibility'] = "visible";
		tmp.style['position'] = "relative";
	}
}




function pegarData(){
hora = new Date()
var dia = hora.getDate();
var mes = hora.getMonth();
var ano = hora.getFullYear();
horas = hora.getHours()
minutos = hora.getMinutes()
segundos = hora.getSeconds()
if (mes <= 9) mes = "0" + mes;
if (horas >= 12) tempo = " p.m.";
else tempo = " a.m.";
if (horas > 12) horas -= 12;
if (horas == 0) horas = 12;
if (minutos <= 9) minutos = "0" + minutos;
if (segundos <= 9) segundos = "0" + segundos;
return (dia +"/"+ (hora.getMonth() + 1) +"/"+ ano + ' ' + horas + ":" + minutos + ":" + segundos + tempo);
}
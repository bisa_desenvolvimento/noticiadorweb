/**
 *
 * noticiadoWeb - plugin jQuery para o NoticiadorWeb.
 * @requires jQuery v1.2 or above
 *
 * https://www.noticiadorweb.com.br/
 *
 * Copyright (c) 2016 BisaWeb
 *
 * Version: 2.0.0
 *
 */

(
	function($) {

		$.fn.noticiadorWeb = function(options) {

			return this.each(

				function() {

					var o = $.extend(
						{
							'idScript' : ''
							, 'idNoticia' : ''
							, 'qtdNoticias' : 0
							, 'urlOrigemNoticias' : 'https://noticiadorweb.com.br/index.php'
							, 'urlExibicaoNoticia' : 'https://noticiadorweb.com.br/index.php'
							, 'urlParametrosExibicaoNoticiador' : '&action=show&secao=exibir_noticia'
							, 'urlExibicaoNoticiaDadosScript' : false
							, 'urlParametrosListagem' : false
							, 'qtdCaracteresResumo' : 140
							, 'qtdCaracteresTexto' : 140
							, 'classContainerNoticias' : 'nw_lista_noticias'
							, 'htmlContainerNoticias' : ''
							, 'htmlNoticia' : ''
							, 'urlAltThumb' : ''
							, 'thumbWidth' : 100
							, 'thumbHeight' : 100
							, 'dimensoesThumbnailsEspecificos' : []
							, 'paginacao' : false
							, 'pgQtdNoticiasPagina' : 1
							, 'pgQtdPaginasExibidas' : 5
							, 'pgBorda' : false
							, 'pgBordaCor' : 'none'
							, 'pgBordaHoverCor' : 'none'
							, 'pgTextoCor' : '#666'
							, 'pgFundoCor' : 'none'
							, 'pgTextoHoverCor' : '#000'
							, 'pgFundoHoverCor' : 'none'
						}
						, options
					);

					var objElement;

					objElement = $(this);

					//Adiciona os elementos necessários ao objElement
					objElement.html('');
					if (o.htmlContainerNoticias != "") {
						objElement.append(o.htmlContainerNoticias);
					} else {
						objElement.append('<div class="' + o.classContainerNoticias + '"></div>');
					}
					if (o.paginacao) {
						objElement.append(
							'<script src="scripts/jquery.paginate.js"></script>' +
							'<link rel="stylesheet" type="text/css" href="scripts/jquery.paginate.css" media="screen"/>' +
							'<div id="nw_paginacao"></div>'
						);
					}

					//Verifica se os parâmetros obrigatários foram passados
					//if(o.idScript == '' || o.qtdNoticias === '' || o.htmlNoticia == '') {
					//    this.append('<span style="color:red">Informar "idScript", "qtdNoticias" e "htmlNoticia" ao plugin do Noticiador.</span>');
					//    return false;
					//}

					var ipage;

					if (o.paginacao) {
						ipage = 1;
						consultaAJAX(objElement, o, o.pgQtdNoticiasPagina, '', o.urlAltThumb);
					} else {
						consultaAJAX(objElement, o, o.qtdNoticias, '', o.urlAltThumb);
					}

				}
			);

		};

		consultaAJAX = function(objElement, o, qtdNot, page, imgAlt) {
			if (page == '') {
				page = 0
			}

			var qtdCaracResumo = o.qtdCaracteresResumo;
			//console.log("qtdCaracResumo: " + qtdCaracResumo + "\n");
			var qtdCaracTexto = o.qtdCaracteresTexto;
			//console.log("qtdCaracTexto: " + qtdCaracTexto + "\n");

			var urlSite = '';

			if (o.idNoticia == '') {
				urlSite = o.urlOrigemNoticias + '?action=obterListaNoticias&secao=api_noticias&qtdRes=' + qtdCaracResumo + '&qtdTxt=' + qtdCaracTexto;
				idGN = o.idScript;
			} else {
				urlSite = o.urlOrigemNoticias + '?action=obterNoticia&secao=api_noticias&qtdRes=' + qtdCaracResumo + '&qtdTxt=' + qtdCaracTexto;
				idGN = o.idNoticia;
			}

			//console.log("urlSite: " + urlSite + "\n");

			//objElement.find('.' + o.classContainerNoticias + '').html('');
			$.ajax(
				{
					url: urlSite
					//, type: 'POST'
					//, dataType: 'jsonp'
					, dataType: 'json'
					, data: {
						idGN: idGN
						, qtdNot: qtdNot
						, imgAlt: imgAlt
						, page: page
					}
					, success: function(data) {
						//alert("ok 1");
						//alert(JSON.stringify(data));
						//console.log(JSON.stringify(data));
						noticiadorweb(objElement, o, data);
					}
					, complete: function(){
						//alert("ok 2");
					}
				}
			)
			/*
			.done(
				function(data) {
					console.log(data);
					alert(data);
				}
			)
			*/
			;
		}

		noticiadorweb = function(objElement, o, data) {

			if (o.paginacao) {
				var totalNoticias = data.total;
				var totalPaginas = Math.ceil(totalNoticias / o.pgQtdNoticiasPagina);
				var display = (totalPaginas > o.pgQtdPaginasExibidas) ? o.pgQtdPaginasExibidas : totalPaginas;
				var rotate = (totalPaginas > o.pgQtdPaginasExibidas) ? true : false;  
				console.log(totalNoticias);
				$("#nw_paginacao").paginate(
					{
						count						: totalPaginas
						, start						: ipage
						, display					: display
						, border					: o.pgBorda
						, border_color				: o.pgBordaCor
						, border_hover_color		: o.pgBordaHoverCor
						, text_color				: o.pgTextoCor
						, background_color			: o.pgFundoCor
						, text_hover_color			: o.pgTextoHoverCor
						, background_hover_color	: o.pgFundoHoverCor
						, rotate					: rotate
						, images					: false
						, mouse						: 'press'
						, onChange					: function(page) {
							ipage = page;
							consultaAJAX(o.pgQtdNoticiasPagina, page - 1);
						}
					}
				);
			}

			//objElement.find('.' + o.classContainerNoticias + '').html('');
			$.each(
				data.noticias
				, function(index, noticia) {
					if (noticia.titulo) {
						objElement.find('.' + o.classContainerNoticias + '').append(montaHTML(o, noticia));    
					}
				}
			);
			objElement.find('.' + o.classContainerNoticias + '').append('<div id="fim"></div>');
		};

		//HTML DE CADA BLOCO DA NOTICIA <li>
		montaHTML = function(o, noticia) {

			var data = noticia.data;
			var data2 = data.split(' ');
			var data3 = data2[0].split('-');
			var dataFormat = data3[2] + '/' + data3[1] + '/' + data3[0];

			var notData = o.htmlNoticia.replace(new RegExp('#data#', 'g'), dataFormat);
			var notId = notData.replace(new RegExp('#id#', 'g'), noticia.id); 
			var notLink = notId.replace(new RegExp('#link#', 'g'), noticia.link);
			var notParametrosListagem = '';
			if (o.urlParametrosListagem || o.urlExibicaoNoticiaDadosScript)
				notParametrosListagem = '&idGN=' + o.idScript + '&qtdNot=' + o.qtdNoticias;
			var notLinkNoticiador = notLink.replace(new RegExp('#linknoticiador#', 'g'), o.urlExibicaoNoticia + '?' + 'noticia_id=' + noticia.id + notParametrosListagem + o.urlParametrosExibicaoNoticiador);
			var notLinkNoticia = notLinkNoticiador.replace(new RegExp('#linknoticia#', 'g'), o.urlExibicaoNoticia + '?' + 'noticia_id=' + noticia.id + notParametrosListagem + o.urlParametrosExibicaoNoticiador);
			var notResumo = notLinkNoticia.replace(new RegExp('#resumo#', 'g'), noticia.resumo);

			var strImgThumbnail = '';
			strImgThumbnail = '<img src="' + noticia.thumbnail + '&w=' + o.thumbWidth + '&h=' + o.thumbHeight + '" />';
			if (noticia.thumbnail != "") {
				//alert("noticia.thumbnail: " + noticia.thumbnail);
				/*
				var strPathOfThumbnail = '';
				strPathOfThumbnail = 'https://noticiadorweb.com.br/plugins/timthumb.php?src=https://noticiadorweb.com.br/users/public/thumb/';
				if (noticia.thumbnail.indexOf("thumb_") == -1) {
					strPathOfThumbnail = 'https://noticiadorweb.com.br/plugins/timthumb.php?src=https://noticiadorweb.com.br/';
				}
				if (noticia.thumbnail.indexOf("https://") != -1) {
					strPathOfThumbnail = 'https://noticiadorweb.com.br/plugins/timthumb.php?src=';
				}
				*/
				//alert("strPathOfThumbnail: " + strPathOfThumbnail);
			} else {
				if (o.urlAltThumb != '') {
					//strImgThumbnail = '<img src="' + noticia.thumbnail + o.urlAltThumb + '&w=' + o.thumbWidth + '&h=' + o.thumbHeight + '" />';
				}
			}

			var notThumbnail = notResumo.replace(new RegExp('#thumbnail#', 'g'), strImgThumbnail);
			var notTitulo = notThumbnail.replace(new RegExp('#titulo#', 'g'), noticia.titulo);
			var notTexto = notTitulo.replace(new RegExp('#texto#', 'g'), noticia.texto);

			return notTexto;
		}

	}
)(jQuery);
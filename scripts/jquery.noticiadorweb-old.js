/**
 * noticiadoWeb - plugin jQuery para o NoticiadorWeb.
 * @requires jQuery v1.2 or above
 *
 * https://www.noticiadorweb.com.br/
 *
 * Copyright (c) 2014 André Almeida (andalvalmeida@gmail.com)
 *
 * Version: 1.0.0
 * Note: 
 */
 // TODO: CORRIGIR EXIBIÇÃO DE 2 SCRIPTS NA  MESMA PÁGINA

(
	function($) {

		$.fn.noticiadorWeb = function(options) {

			 var o = $.extend(
				{
					'idScript' : ''
					, 'idNoticia' : ''
					, 'qtdNoticias' : 0
					, 'htmlNoticia' : ''
					, 'thumbWidth' : 100
					, 'thumbHeight' : 100
					, 'linkSite' : ''
					, 'paginacao' : false
					, 'pgQtdNoticiasPagina' : 1
					, 'pgQtdPaginasExibidas' : 5
					, 'pgBorda' : false
					, 'pgBordaCor' : 'none'
					, 'pgBordaHoverCor' : 'none'
					, 'pgTextoCor' : '#666'
					, 'pgFundoCor' : 'none'
					, 'pgTextoHoverCor' : '#000'
					, 'pgFundoHoverCor' : 'none'
				}
				, options
			);

			//Verifica se os parâmetros obrigatários foram passados
			//if(o.idScript == '' || o.qtdNoticias === '' || o.htmlNoticia == '') {
			//    this.append('<span style="color:red">Informar "idScript", "qtdNoticias" e "htmlNoticia" ao plugin do Noticiador.</span>');
			//    return false;
			//}

			//Adiciona os elementos necessários à div
			div = this;
			div.html('');
			div.append('<ul class="nw_lista_noticias"></ul>');
			if (o.paginacao) {
				div.append(
					'<script src="scripts/jquery.paginate.js"></script>' +
					'<link rel="stylesheet" type="text/css" href="scripts/jquery.paginate.css" media="screen"/>' +
					'<div id="nw_paginacao"></div>'
				);
			}

			//HTML DE CADA BLOCO DA NOTICIA <li>
			montaHTML = function(noticia) {

				var data = noticia.data;
				var data2 = data.split(' ');
				var data3 = data2[0].split('-');
				var dataFormat = data3[2] + '/' + data3[1] + '/' + data3[0];

				var notData = o.htmlNoticia.replace(new RegExp('#data#', 'g'), dataFormat);
				var notId = notData.replace(new RegExp('#id#', 'g'), noticia.id); 
				var notLink = notId.replace(new RegExp('#link#', 'g'), noticia.link);
				var notLinkNoticiador = notLink.replace(new RegExp('#linknoticiador#', 'g'), 'https://noticiadorweb.com.br/index.php?action=show&secao=exibir_noticia&noticia_id=' + noticia.id);
				var notLinkSite = notLinkNoticiador.replace(new RegExp('#linksite#', 'g'), o.linkSite + noticia.id);
				var notResumo = notLinkSite.replace(new RegExp('#resumo#', 'g'), noticia.resumo);

				var strImgThumbnail = '';
				if (noticia.thumbnail != "") {
					//alert("noticia.thumbnail: " + noticia.thumbnail);
					var strPathOfThumbnail = 'https://noticiadorweb.com.br/plugins/timthumb.php?src=https://noticiadorweb.com.br/users/public/thumb/';
					if (noticia.thumbnail.indexOf("thumb_") == -1) {
						strPathOfThumbnail = 'https://noticiadorweb.com.br/plugins/timthumb.php?src=https://noticiadorweb.com.br/';
					}
					if (noticia.thumbnail.indexOf("https://") != -1) {
						strPathOfThumbnail = 'https://noticiadorweb.com.br/plugins/timthumb.php?' + 'src=';
					}
					//alert("strPathOfThumbnail: " + strPathOfThumbnail);
					strImgThumbnail = '<img src="' + strPathOfThumbnail + noticia.thumbnail + '&w=' + o.thumbWidth + '&h=' + o.thumbHeight + '" />'
				}

				var notThumbnail = notResumo.replace(new RegExp('#thumbnail#', 'g'), strImgThumbnail);
				var notTitulo = notThumbnail.replace(new RegExp('#titulo#', 'g'), noticia.titulo);
				var notTexto = notTitulo.replace(new RegExp('#texto#', 'g'), noticia.texto);

				return notTexto;
			}

			consultaAJAX = function(qtd, page) {
				if (page == '') {
					page = 0
				};

				var urlSite = '';

				if (o.idNoticia == '') {
					urlSite = 'https://noticiadorweb.com.br/?action=show&secao=janela_livre_cli&callback=noticiadorweb';
					urlSite = 'https://noticiadorweb.com.br/?action=show&secao=janela_livre_cli';
					id = o.idScript;
				} else {
					urlSite = 'https://noticiadorweb.com.br/?action=load&secao=janela_livre_cli&callback=noticiadorweb';
					urlSite = 'https://noticiadorweb.com.br/?action=load&secao=janela_livre_cli';
					id = o.idNoticia;
				}

				div.find('.nw_lista_noticias').html('');
				$.ajax(
					{
						url: urlSite
						//, type: 'POST'
						//, dataType: 'jsonp'
						, dataType: 'json'
						, data: {
							id: id
							, qtd: qtd
							, page: page
						}
						, success: function(data) {
							//alert("ok 1");
							//alert(data);
							noticiadorweb(data);
						}
						, complete: function(){
							//alert("ok 2");
						}
					}
				)
				/*
				.done(
					function(data) {
						console.log(data);
						alert(data);
					}
				)
				*/
				;
			}

			if (o.paginacao) {
				consultaAJAX(o.pgQtdNoticiasPagina);
			} else {
				consultaAJAX(o.qtdNoticias);
			}

			ipage = 1;

			noticiadorweb = function(data) {
				if (o.paginacao) {
					var totalNoticias = data.total;
					var totalPaginas = Math.ceil(totalNoticias / o.pgQtdNoticiasPagina);
					var display = (totalPaginas > o.pgQtdPaginasExibidas) ? o.pgQtdPaginasExibidas : totalPaginas;
					var rotate = (totalPaginas > o.pgQtdPaginasExibidas) ? true : false;  
					console.log(totalNoticias);
					$("#nw_paginacao").paginate(
						{
							count						: totalPaginas
							, start						: ipage
							, display					: display
							, border					: o.pgBorda
							, border_color				: o.pgBordaCor
							, border_hover_color		: o.pgBordaHoverCor
							, text_color				: o.pgTextoCor
							, background_color			: o.pgFundoCor
							, text_hover_color			: o.pgTextoHoverCor
							, background_hover_color	: o.pgFundoHoverCor
							, rotate					: rotate
							, images					: false
							, mouse						: 'press'
							, onChange					: function(page) {
								ipage = page;
								consultaAJAX(o.pgQtdNoticiasPagina, page - 1);
							}
						}
					);
				}

				div.find('.nw_lista_noticias').html('');
				$.each(
					data
					, function(index, noticia) {
						if (noticia.titulo) {
							div.find('.nw_lista_noticias').append(montaHTML(noticia));    
						}
					}
				);
				div.find('.nw_lista_noticias').append('<div id="fim"></div>');
			};

		};
	}
)(jQuery);
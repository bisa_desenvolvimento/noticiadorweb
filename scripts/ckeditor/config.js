/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config )
{
    // Define changes to default configuration here. For example:
	
    // config.uiColor = '#AADC6E';
  
    config.toolbar = 'MyToolbar';
    
    

    config.toolbar_MyToolbar =
        [
        ['Preview','Maximize'],
        ['Bold','Italic','Underline','Strike','-','Subscript','Superscript','Blockquote'],
        ['NumberedList','BulletedList'],
        ['Link','Unlink','Anchor'],
        ['Image','Table','HorizontalRule'],
        '/',
        ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
        ['Format','Font','FontSize','-','TextColor','BGColor'],
    ];
    
    config.toolbarStartupExpanded = false;
    config.resize_enabled = false;
    config.height = 500;
  
  
	//config.language =idioma;
  
  
    config.filebrowserBrowseUrl = 'scripts/ckeditor/ckfinder/ckfinder.html',
    config.filebrowserImageBrowseUrl = 'scripts/ckeditor/ckfinder/ckfinder.html?type=Images',
    config.filebrowserFlashBrowseUrl = 'scripts/ckeditor/ckfinder/ckfinder.html?type=Flash',
    config.filebrowserUploadUrl = 'scripts/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    config.filebrowserImageUploadUrl = 'scripts/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
    config.filebrowserFlashUploadUrl = 'scripts/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'

  
};



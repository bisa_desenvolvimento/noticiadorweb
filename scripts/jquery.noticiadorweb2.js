/**
 * noticiadoWeb - plugin jQuery para o NoticiadorWeb.
 * @requires jQuery v1.2 or above
 *
 * https://www.noticiadorweb.com.br/
 *
 * Copyright (c) 2014 André Almeida (andalvalmeida@gmail.com)
 *
 * Version: 1.0.0
 * Note: 
 */
 // TODO: CORRIGIR EXIBIÇÃO DE 2 SCRIPTS NA  MESMA PÁGINA

(function($) {
$.fn.noticiadorWeb2 = function(options) {
    
     var o = $.extend( {
            'idScript' : '',
            'qtdNoticias' : '',
            'htmlNoticia' : '',
            'paginacao' : false,
            'pgQtdPaginasExibidas' : 5,
            'pgBorda' : false,
            'pgBordaCor' : 'none', 
            'pgBordaHoverCor' : 'none',
            'pgTextoCor' : '#666',
            'pgFundoCor' : 'none',
            'pgTextoHoverCor' : '#000',
            'pgFundoHoverCor' : 'none'                  
        }, options);
    
    
    //Verifica se os parâmetros obrigatários foram passados
    if(o.idScript == '' || o.qtdNoticias == '' || o.htmlNoticia == '') {
        this.append('<span style="color:red">Informar "idScript", "qtdNoticias" e "htmlNoticia" ao plugin do Noticiador.</span>');
        return false;
    }
    
    //Adiciona os elementos necessários à div
    div2 = this;
    console.log(div2);
    div2.append('<ul class="nw_lista_noicias"></ul>');
    if(o.paginacao) {
        div2.append('<link rel="stylesheet" type="text/css" href="scripts/jquery.paginate.css" media="screen"/>' +
                '<script src="scripts/jquery.paginate.js"></script>' +
                '<div id="nw_paginacao"></div>');
    }
    
                    
    //HTML DE CADA BLOCO DA NOTICIA <li>
    montaHTML = function(noticia) {
        
        var notData = o.htmlNoticia.replace('#data#', noticia.data);
        var notId = notData.replace('#id#', noticia.id); 
        var notLink = notId.replace('#link#', noticia.link);
        var notResumo = notLink.replace('#resumo#', noticia.resumo);
        var notThumbnail = notResumo.replace('#thumbnail#', noticia.thumbnail);
        var notTitulo = notThumbnail.replace('#titulo#', noticia.titulo);
        
        return notTitulo;               
    }
 
    consultaAJAX = function(id, qtd, page) { console.log(div2);
        if(page == '') {page = 0};
        div2.find('.nw_lista_noicias').html('');
        $.ajax({
            url: 'https://noticiadorweb.com.br/?action=show&secao=janela_livre_cli&callback=noticiadorweb',
            type: 'POST',
            dataType: 'jsonp',
            async: false,
            data: { id: id, qtd: qtd, page: page }
        });    console.log(div2);
    }
    
    console.log(div2);
    consultaAJAX(o.idScript,o.qtdNoticias);
    console.log(div2);
    
    ipage = 1;
    noticiadorweb = function(data){
        console.log(data)
        console.log(div2)
        if(o.paginacao) {
            var totalNoticias = data.total;
            var totalPaginas = Math.ceil(totalNoticias/o.qtdNoticias);
            var display = (totalPaginas > o.pgQtdPaginasExibidas) ? o.pgQtdPaginasExibidas : totalPaginas;
            var rotate = (totalPaginas > o.pgQtdPaginasExibidas) ? true : false;  
            
            $("#nw_paginacao").paginate({
        		count 		           : totalPaginas,
        		start 		           : ipage,
        		display                : display,
                border				   : o.pgBorda,
                border_color           : o.pgBordaCor,
                border_hover_color     : o.pgBordaHoverCor, 
    			text_color  		   : o.pgTextoCor,
    			background_color       : o.pgFundoCor,	
    			text_hover_color  	   : o.pgTextoHoverCor,
    			background_hover_color : o.pgFundoHoverCor, 
                rotate                 : rotate,
    			images		           : false,
    			mouse		           : 'press',
        		onChange     		   : function(page){
        									ipage = page;
                                            consultaAJAX(o.idScript,o.qtdNoticias,page-1);
        								  }
            });
        }
        div2.find('.nw_lista_noicias').html('');
        $.each(data, function(index, noticia){
            if(noticia.titulo) {
                div2.find('.nw_lista_noicias').append(montaHTML(noticia));    
            }
        console.log('script 2');
        });
    };

};
})( jQuery );
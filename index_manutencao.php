<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">


    <title>NoticiadorWeb</title>
    
    <style type="text/css">
      .centro{
        width: 80%;
        margin: 0 auto;
        
        text-align: center;
        margin-top: 15%;
      }
      .titulo{
        font-size: 50px;
      }
      .texto{
        font-size: 30px;
        color: #858585;
      }
      a {
        text-decoration: none;
      }
    </style>
 
  </head>

  <body>


      <div class="centro">
        <p><img src="https://www.bisaweb.com.br/wp-content/uploads/2012/05/NoticiadosWeb3.png"></p>
        <h1 class="titulo">Sistema em manutenção</h1>
        <p class="texto">Por favor, tente mais tarde!<br>
        Para mais informações entre em contato com a <a href="https://www.bisaweb.com.br" target="_BLANK">BisaWeb</a>.<br>
        Fone: (81) 3312-7070  |  E-mail: suporte@bisa.com.br
        </p>
      </div>

    </div><!-- /.container -->
  </body>
</html>
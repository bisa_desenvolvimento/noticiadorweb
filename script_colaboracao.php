<?php 
/**
* 
*/
class Colaboracao
{
    var $inbox;
	
	function Colaboracao( $hostname, $username, $password )  
	{  
	 $this->inbox = imap_open($hostname,$username,$password) or die('Cannot connect to Gmail: ' . imap_last_error());  
	}  

	public function getMails($param)  
	{  
		/* ALL - return all messages matching the rest of the criteria
	    ANSWERED - match messages with the \\ANSWERED flag set
	    BCC "string" - match messages with "string" in the Bcc: field
	    BEFORE "date" - match messages with Date: before "date"
	    BODY "string" - match messages with "string" in the body of the message
	    CC "string" - match messages with "string" in the Cc: field
	    DELETED - match deleted messages
	    FLAGGED - match messages with the \\FLAGGED (sometimes referred to as Important or Urgent) flag set
	    FROM "string" - match messages with "string" in the From: field
	    KEYWORD "string" - match messages with "string" as a keyword
	    NEW - match new messages
	    OLD - match old messages
	    ON "date" - match messages with Date: matching "date"
	    RECENT - match messages with the \\RECENT flag set
	    SEEN - match messages that have been read (the \\SEEN flag is set)
	    SINCE "date" - match messages with Date: after "date"
	    SUBJECT "string" - match messages with "string" in the Subject:
	    TEXT "string" - match messages with text "string"
	    TO "string" - match messages with "string" in the To:
	    UNANSWERED - match messages that have not been answered
	    UNDELETED - match messages that are not deleted
	    UNFLAGGED - match messages that are not flagged
	    UNKEYWORD "string" - match messages that do not have the keyword "string"
	    UNSEEN - match messages which have not been read yet*/	  
	 return imap_search($this->inbox,$param); 
	} 

	public function getMailHeader($mail) 
	{ 
	   $message = array(); 	  
	   
	   $header = imap_header($this->inbox, $mail); 
	   	      
	   $message['subject']     =   iconv_mime_decode($header->subject,0,"UTF-8");
	   $message['sender']      =   $header->from[0]->personal;
	   $message['fromaddress'] =   $header->from[0]->mailbox.'@'.$header->from[0]->host; 	    	  
	   $message['date']        =   $header->date; 
	   
	   return $message; 
	} 


	public function readMailBody($messageid) 
	{ 
	   
        /* get information specific to this email */
        $overview = imap_fetch_overview($this->inbox,$messageid,0);
        $message = quoted_printable_decode(imap_fetchbody($this->inbox,$messageid,1.2));
         if($message == ""){         	
         	$message = quoted_printable_decode(imap_fetchbody($this->inbox,$messageid,1));
         }
         $structure = imap_fetchstructure($this->inbox,$messageid);
        if($structure->parts[0]->encoding == 3 ||$structure->encoding == 3 )
        {        	
            $message=imap_base64($message);

        }
        if($structure->parts[0]->encoding == 4 ||$structure->encoding == 4) 
        {        	
            $message = imap_qprint($message);
        }

        if($structure->parts[1]->disposition == "ATTACHMENT")
        {        	
          if(preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $structure->parts[1]->parameters[0]->value, $ext))
			{

				$anexo = imap_base64(imap_fetchbody($this->inbox, $messageid, 2));

				$caminho = "users/public/thumb/";

				if (!is_dir($caminho))
				{
					mkdir($caminho,777);
				}

				$nome_imagem = 'thumb_' . md5(uniqid(time())) . "." . $ext[1]; // Gera um nome único para a imagem
				$noticia['thumb'] = $caminho . $nome_imagem; // Caminho de onde ficará a imagem
				
				file_put_contents($noticia['thumb'],$anexo);
			} 			

        }	

        $message2  = quoted_printable_decode(imap_fetchbody($this->inbox,$messageid,0));
        $date      = explode(':',$message2);
        $date2     = date('d-m-Y h:i:s',strtotime($date[8].':00:00'));


        if($overview[0]->subject == "USR:Site01_Comms Complete")
        {
            $res['date'] = $date2;
            $res['body'] = $message;
        }

        $noticia['area']         = explode(",", $this->getTags("areas",$message));           
        $noticia['noticia']      = $this->getTags("noticia",$message);               
        $noticia['publicar']     = $this->getTags("publicar",$message);       
        $noticia['publicar_ate'] = $this->getTags("publicar_ate",$message);
        $noticia['link']         = $this->getTags("link",$message);         
        $noticia['chamada']      = $this->getTags("chamada",$message);

        return $noticia;       
            
	} 

	private function check_type($structure) ## CHECK THE TYPE  
	{  
	  if($structure->type == 1)  
	    {  	    	
	     return(true); ## YES THIS IS A MULTI-PART MESSAGE  
	    }  
	 else  
	    {  	    	
	     return(false); ## NO THIS IS NOT A MULTI-PART MESSAGE  
	    }  
	} 

	
	private function getTags($tag,$message)  
	{  	

		$message = preg_split('[#'.$tag.'#]', $message, -1, PREG_SPLIT_OFFSET_CAPTURE);	

		$message = $message[1];	
	
		if(!$message)
			return false;
		
		return trim($message[0]);			 
	}

	public function closeIMAPConnection()
	{
		imap_close($this->inbox);
	}
	

}


class Conexao_NoticiadorWeb
{
    var $con;
	
	function Conexao_NoticiadorWeb($db)  
	{  
	 $this->con = mysqli_connect( $db['host'], $db['user'], $db['password'],$db['base'] ) or die ( mysqli_error($this->con) );
    //    mysqli_select_db( $db['base'], $this->con) or die ( mysqli_error($this->con) );  
	}  


	function verificarUsuario( $usuario )  
	{  
	  $sql ="SELECT usuario_id FROM usuario WHERE `usu_excluido` = 0 and usu_login = '{$usuario}'";    
		$result =  mysqli_query($this->con,$sql); 

		if(mysqli_num_rows($result) > 0)
		{
			$result = mysqli_fetch_assoc($result);
			return $result['usuario_id'];
		}

		return false;
	} 

	function cadastraNoticia( $noticia )  
	{  		
		
	  $sql ="INSERT INTO `noticiadorweb`.`noticia` (										  
			  `not_autor_id`,
			  `not_data`,
			  `not_titulo`,
			  `not_chamada`,										  
			  `not_texto`,
			  `not_publicacao`,
			  `not_validade`,
			  `not_link`,
			  `not_autorizada`,
			  `not_acessos`,
			  `not_excluida`,
			  `not_thumbnail`
			) 
			VALUES
			  (			    
			    '".$noticia['id_autor']."',
			    '".date("Y-m-d G:i:s") ."',
			    '".utf8_decode($noticia['subject']) ."',
			    '".utf8_decode($noticia['chamada']) ."',										    
			    '".utf8_decode($noticia['noticia']) ."',
			    '".$this->validateDate($noticia['publicar']) ."',
			    '".$this->validateDate($noticia['publicar_ate']) ."',
			    '".$this->validateLink($noticia['link'])."',
			    '10',
			    '0',
			    '0',
			    '".$noticia['thumb'] ."'
			  ) ;";		
		 $query = mysqli_query($this->con,$sql);

		return mysqli_insert_id($this->con); 

	} 

	public function associarAreaNoticia($noticia,$area)
	{
         $sql ="INSERT INTO `noticiadorweb`.`noticia_area` (										  
			  `noticia_id`,
			  `area_id`,
			  `not_aut`		
			) 
			VALUES
			  (			    
			    '".$noticia."',
			    '".$area."',
			    '10'
			  ) ;";			
   
		  mysqli_query($this->con,$sql);
	    
	}

	function enviarEmail($email){

		
		require_once("plugins/phpmailer/class.phpmailer.php");

		$mail = new PHPMailer();
        
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->Host = "smtp.ownserver.noticiadorweb.com.br";
        $mail->Port =  587;
        $mail->Username = "conectividade";
        $mail->Password = "c0n3ctivid@d3";
        $mail->CharSet = 'UTF-8';        
        $mail->SetFrom('noticiadorweb@bisa.com.br', 'NoticiadorWeb');
        $mail->Subject = "Notícia cadastrada com sucesso!";
        $mail->MsgHTML("Sua notícia foi salva com sucesso e agora aguardará a aprovação do responsável");
        $mail->AddAddress($email);
        
        if($mail->Send()) {
            echo "Email enviado!";
            return true;
        } else {
            echo "Mailer Error: " . $mail->ErrorInfo;
            return false;
        }
                          			
    }

	public function validateDate($data)
	{
         $data = str_replace("/", "-", $data);
	    return date('Y-m-d',strtotime($data));
	    
	}

	public function validateLink($link)
	{	    
	    return $link ?: "https://noticiadorweb.com.br/index.php?action=show&secao=exibir_noticia&noticia_id=";
	}
	
}

set_time_limit(4000); 
// Connect to gmail
 $hostname = '{imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX';
 $username = 'noticiadorweb@bisa.com.br';
 $password = 'tecnologia';


$obj = new Colaboracao($hostname,$username,$password);
$emails = $obj->getMails('UNSEEN TO "noticiadorweb@bisa.com.br"');


$db['host']     = "localhost"; 
$db['user']     = "root";
$db['password'] = "tecnologia";
$db['base']     = "noticiadorweb";

$noticiador = new Conexao_NoticiadorWeb($db);
  
$output = '';

if($emails)
{

	foreach($emails as $mail)
	{		
         
	    $corpoMail  = $obj->readMailBody($mail);

	    $headerInfo = $obj->getMailHeader($mail);

	    $noticia    = array_merge($corpoMail,$headerInfo); 

	    $resultado  = $noticiador->verificarUsuario($noticia['fromaddress']);	
        
        echo $resultado;


		if($resultado)
		{   
			
			$noticia['id_autor'] = $resultado;
									
			$idNoticia = $noticiador->cadastraNoticia($noticia);

			foreach ($noticia['area'] as $key => $value) {

				$noticiador->associarAreaNoticia($idNoticia,$value);
			}

			$noticiador->enviarEmail($noticia['fromaddress']);			

		}	   	     
    
	}

} else {
	echo "Caixa vazia!";
}
       $obj->closeIMAPConnection();
 
?>
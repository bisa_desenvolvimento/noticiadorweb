<?php
/*
 * Plugin do captcha V2 para ser usado no NoticiadorWeb
 *
/**
 * The reCAPTCHA server URL's
 */


/**
 * Submits an HTTP POST to a reCAPTCHA server
 * @param string $response
 * @param string $remote
 * @return array response
 */
function _recaptcha_http_post($response, $remote) {

         $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, [
        'secret' => "6LdS7GgUAAAAAD7fmOVOUUnxEVbrEYing4pNhjh2",
        'response' => $response,
        'remoteip' => $remote
        ]);
        
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $resp = json_decode(curl_exec($ch));
        
        curl_close($ch);

        return $resp;
}

?>

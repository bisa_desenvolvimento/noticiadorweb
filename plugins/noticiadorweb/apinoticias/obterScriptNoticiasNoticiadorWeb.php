<?php



$strExecutionOutPut = "";

ob_start();

print "\n\n##########################################################################################\n";
print date("################################## Y-m-d H:i:s ###################################\n");
print "##########################################################################################\n\n";

$ip = array(
    "REMOTE_ADDR" => null,
    "REMOTE_PROXY" => null,
    "HTTP_CF_CONNECTING_IP" => null,
    "REMOTE_HOST_BY_ADDR" => null,
    "HTTP_REFERER" => null
);

if (isset($_SERVER["REMOTE_ADDR"])) {
    $ip["REMOTE_ADDR"] = trim($_SERVER["REMOTE_ADDR"]);
}
if (isset($_SERVER["REMOTE_PROXY"])) {
    $ip["REMOTE_PROXY"] = trim($_SERVER["REMOTE_PROXY"]);
}
if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
    $ip["HTTP_CF_CONNECTING_IP"] = trim($_SERVER["HTTP_CF_CONNECTING_IP"]);
}
if (isset($_SERVER["REMOTE_HOST_BY_ADDR"])) {
    $ip["REMOTE_HOST_BY_ADDR"] = trim($_SERVER["REMOTE_HOST_BY_ADDR"]);
}
if (isset($_SERVER["HTTP_REFERER"])) {
    $ip["HTTP_REFERER"] = trim($_SERVER["HTTP_REFERER"]);
}

print "<pre>";
print_r($ip);
print "</pre>";

if (!empty($_SERVER["REMOTE_PROXY"])) {
    $rs = $_SERVER["REMOTE_PROXY"];
} else if (!empty($_SERVER["HTTP_CF_CONNECTING_IP"])) {
    $rs = $_SERVER["HTTP_CF_CONNECTING_IP"];
} else if (!empty($_SERVER["REMOTE_ADDR"])) {
    $rs = $_SERVER["REMOTE_ADDR"];
}

if (strpos($rs, ", ")) {
    $ips = explode(", ", $rs);

    $rs = $ips[0];
    //$rs = implode(", ", $ips);
}

$dns = gethostbyaddr($rs);

print $rs."<br>".$dns;

print "\n\n##########################################################################################\n";
print date("################################## Y-m-d H:i:s ###################################\n");
print "##########################################################################################\n\n";

$strExecutionOutPut .= ob_get_contents();

ob_end_clean();

$fileExecutionLog = @fopen("requisicoes.log", "a+");
if (@fwrite($fileExecutionLog, $strExecutionOutPut))
    fclose($fileExecutionLog);



require_once("NoticiasNoticiadorWeb.php");

$arrStrScripts = array();

$arrStrScripts["strScriptNoticiasNoticiadorWeb"] = NoticiasNoticiadorWeb :: obterScriptNoticiasNoticiadorWeb();

//print "arrStrScripts:<pre>";
//print_r($arrStrScripts);
//print "</pre>";

echo json_encode($arrStrScripts);


?>
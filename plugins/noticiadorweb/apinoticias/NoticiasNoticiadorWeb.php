<?php



class NoticiasNoticiadorWeb {

	public static function obterScriptNoticiasNoticiadorWeb() {

		$strScriptNoticiasNoticiadorWeb = '';

		$strScriptNoticiasNoticiadorWeb = '

			class NoticiasNoticiadorWeb {

		';

		$strScriptNoticiasNoticiadorWeb .= '

				public static function obterNoticias($strIdGNPadrao, $strQtdNotPadrao, $strQtdNotIndexPadrao, $strImgAltPadrao, $booContar = false, $strSetCharset = "") {

					//print "_REQUEST:<pre>";
					//print_r($_REQUEST);
					//print "</pre>";

					//$arrParams = func_get_args();

					//$strQtdNotPadrao = 0;
					//if (isset($arrParams[0]))
					//	$strQtdNotPadrao = $arrParams[0];

					//$strIdGNPadrao = 0;
					//if (isset($arrParams[1]))
					//	$strIdGNPadrao = $arrParams[1];

					$arrNoticiasNoticiadorWeb = array();

					//echo "\$_REQUEST[\"qtdNot\"]: " . $_REQUEST["qtdNot"] . "</ br>";
					//echo "\$strQtdNotPadrao: " . $strQtdNotPadrao . "</ br>";

					$idGN = (isset($_REQUEST["idGN"])) ? $_REQUEST["idGN"] : $strIdGNPadrao;

					$qtdNot = $strQtdNotPadrao;

					$qtdNotIndex = (isset($_REQUEST["qtdNotIndex"])) ? $_REQUEST["qtdNotIndex"] : $strQtdNotIndexPadrao;

					$imgAlt = (isset($_REQUEST["imgAlt"])) ? $_REQUEST["imgAlt"] : $strImgAltPadrao;

					$noticia_id = (isset($_REQUEST["noticia_id"])) ? $_REQUEST["noticia_id"] : "";

					if ($noticia_id != "") {

						$sum = "";
						if ($booContar)
							$sum = "&sum";

						$strUrlRequisitarNoticia = "https://noticiadorweb.com.br/index.php?action=obterNoticia&secao=api_noticias" . "&idGN=" . $idGN . "&qtdNot=" . $qtdNot . "&imgAlt=" . $imgAlt . $sum . "&noticia_id=" . $noticia_id;

						$strUrlRequisitarNoticia .= "&urlOrigem="."https://".$_SERVER["HTTP_HOST"]."".strtok($_SERVER["REQUEST_URI"], "?");

						//echo $strUrlRequisitarNoticia;

						$arrDadosNoticia = json_decode(file_get_contents($strUrlRequisitarNoticia, FILE_TEXT), true);

						//print "<br>arrDadosNoticia:<pre>";
						//print_r($arrDadosNoticia);
						//print "</pre>";

						$arrNoticiasNoticiadorWeb["noticia"] = (isset($arrDadosNoticia["noticias"])) ? $arrDadosNoticia["noticias"][0] : array();

						$arrNoticiasNoticiadorWeb["noticia"]["ScriptBeforeShareBar"] = file_get_contents($arrDadosNoticia["ScriptBeforeShareBar"], FILE_TEXT);

						$arrNoticiasNoticiadorWeb["noticia"]["ShareBar"] = file_get_contents($arrDadosNoticia["ShareBar"], FILE_TEXT);

						$arrNoticiasNoticiadorWeb["noticia"]["ScriptAfterShareBar"] = file_get_contents($arrDadosNoticia["ScriptAfterShareBar"], FILE_TEXT);

						$arrNoticiasNoticiadorWeb["noticia"]["ScriptBeforeMetaDados"] = file_get_contents($arrDadosNoticia["ScriptBeforeMetaDados"], FILE_TEXT);

						//echo $arrDadosNoticia["MetaDados"];
						$arrNoticiasNoticiadorWeb["noticia"]["MetaDados"] = file_get_contents($arrDadosNoticia["MetaDados"]);
						//echo $arrNoticiasNoticiadorWeb["noticia"]["MetaDados"];

						$arrNoticiasNoticiadorWeb["noticia"]["ScriptBeforeFormComentar"] = file_get_contents($arrDadosNoticia["ScriptBeforeFormComentar"], FILE_TEXT);

						if ($strSetCharset != "")
							$arrDadosNoticia["FormComentar"] .=  "&setCharset=" . $strSetCharset;

						$arrNoticiasNoticiadorWeb["noticia"]["FormComentar"] = file_get_contents($arrDadosNoticia["FormComentar"], FILE_TEXT);

						$arrNoticiasNoticiadorWeb["noticia"]["ScriptAfterFormComentar"] = file_get_contents($arrDadosNoticia["ScriptAfterFormComentar"], FILE_TEXT);

						$arrStrScript = json_decode(file_get_contents($arrDadosNoticia["ShareComment"], FILE_TEXT), true);

						$arrNoticiasNoticiadorWeb["noticia"]["strScriptShareComment"] = $arrStrScript["strScriptShareComment"];

						$arrNoticiasNoticiadorWeb["noticia"]["comentarios"] = $arrDadosNoticia["comentarios"];

						$arrNoticiasNoticiadorWeb["baseUrlOrigem"] = $arrDadosNoticia["baseUrlOrigem"];

					} else {

						$qtdNot = $qtdNotIndex;

					}

					$page = (isset($_REQUEST["page"])) ? $_REQUEST["page"] : 0;

					$strUrlNoticia = strtok($_SERVER["REQUEST_URI"], "?");

					$strUrlRequisitarListaNoticias = "https://noticiadorweb.com.br/index.php?action=obterListaNoticias&secao=api_noticias" . "&idGN=" . $idGN . "&qtdNot=" . $qtdNot . "&imgAlt=" . $imgAlt . "&page=" . $page . "&urlNoticia=" . $strUrlNoticia;

					//echo $strUrlRequisitarListaNoticias;

					$arrListaNoticias = json_decode(file_get_contents($strUrlRequisitarListaNoticias, FILE_TEXT), true);

					//print "arrListaNoticias:<pre>";
					//print_r($arrListaNoticias);
					//print "</pre>";

					$arrNoticiasNoticiadorWeb["noticias"] = $arrListaNoticias["noticias"];

					return $arrNoticiasNoticiadorWeb;
				}

		';

		$strScriptNoticiasNoticiadorWeb .= '

			}

		';

		return $strScriptNoticiasNoticiadorWeb;
	}

}



?>
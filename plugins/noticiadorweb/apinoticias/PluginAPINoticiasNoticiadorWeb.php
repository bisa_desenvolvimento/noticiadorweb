<?php



class PluginAPINoticiasNoticiadorWeb {

	public static function obterScriptPluginAPINoticiasNoticiadorWeb() {

		$strScriptNoticiasNoticiadorWeb = '';

		$strScriptNoticiasNoticiadorWeb .= '

			class PluginAPINoticiasNoticiadorWeb {

		';

		$strScriptNoticiasNoticiadorWeb .= '

				public static function obterNoticias($intIDGN, $intQtdNotListagem, $intQtdNotRel, $strDefaultThumbnail, $strCharsetComent = "", $booContar = true) {

					//print "_REQUEST:<pre>";
					//print_r($_REQUEST);
					//print "</pre>";

					$arrNoticiasNoticiadorWeb = array();

					$idGN = $intIDGN;

					$qtdNotIndex = $intQtdNotListagem;

					$qtdNot = $intQtdNotRel;

					$imgAlt = $strDefaultThumbnail;

					$noticia_id = (isset($_REQUEST["noticia_id"])) ? $_REQUEST["noticia_id"] : "";

					if ($noticia_id != "") {

						$sum = "";
						if ($booContar)
							$sum = "&sum";

						$strUrlRequisitarNoticia = "https://noticiadorweb.com.br/index.php?action=obterNoticia&secao=api_noticias" . "&idGN=" . $idGN . "&qtdNot=" . $qtdNot . "&imgAlt=" . $imgAlt . $sum . "&noticia_id=" . $noticia_id;

						$strUrlRequisitarNoticia .= "&urlOrigem="."https://".$_SERVER["HTTP_HOST"]."".strtok($_SERVER["REQUEST_URI"], "?");

						//echo $strUrlRequisitarNoticia;

						$arrDadosNoticia = json_decode(file_get_contents($strUrlRequisitarNoticia, FILE_TEXT), true);

						//print "<br>arrDadosNoticia:<pre>";
						//print_r($arrDadosNoticia);
						//print "</pre>";

						$arrNoticiasNoticiadorWeb["noticia"] = (isset($arrDadosNoticia["noticias"])) ? $arrDadosNoticia["noticias"][0] : array();

						$arrNoticiasNoticiadorWeb["noticia"]["ShareBar"] = file_get_contents($arrDadosNoticia["ShareBar"], FILE_TEXT);

						//echo $arrDadosNoticia["MetaDados"];
						$arrNoticiasNoticiadorWeb["noticia"]["MetaDados"] = file_get_contents($arrDadosNoticia["MetaDados"]);
						//echo $arrNoticiasNoticiadorWeb["noticia"]["MetaDados"];

						if ($strCharsetComent != "")
							$arrDadosNoticia["FormComentar"] .=  "&setCharset=" . $strCharsetComent;

						$arrNoticiasNoticiadorWeb["noticia"]["FormComentar"] = file_get_contents($arrDadosNoticia["FormComentar"], FILE_TEXT);

						$arrStrScript = json_decode(file_get_contents($arrDadosNoticia["ShareComment"], FILE_TEXT), true);

						$arrNoticiasNoticiadorWeb["noticia"]["strScriptShareComment"] = $arrStrScript["strScriptShareComment"];

						$arrNoticiasNoticiadorWeb["noticia"]["comentarios"] = $arrDadosNoticia["comentarios"];

						$arrNoticiasNoticiadorWeb["baseUrlOrigem"] = $arrDadosNoticia["baseUrlOrigem"];

					} else {

						$qtdNot = $qtdNotIndex;

					}

					$page = (isset($_REQUEST["page"])) ? $_REQUEST["page"] : 0;

					$strUrlNoticia = strtok($_SERVER["REQUEST_URI"], "?");

					$strUrlRequisitarListaNoticias = "https://noticiadorweb.com.br/index.php?action=obterListaNoticias&secao=api_noticias" . "&idGN=" . $idGN . "&qtdNot=" . $qtdNot . "&imgAlt=" . $imgAlt . "&page=" . $page . "&urlNoticia=" . $strUrlNoticia;

					//echo $strUrlRequisitarListaNoticias;

					$arrListaNoticias = json_decode(file_get_contents($strUrlRequisitarListaNoticias, FILE_TEXT), true);

					//print "arrListaNoticias:<pre>";
					//print_r($arrListaNoticias);
					//print "</pre>";

					$arrNoticiasNoticiadorWeb["noticias"] = $arrListaNoticias["noticias"];

					return $arrNoticiasNoticiadorWeb;
				}

		';

		$strScriptNoticiasNoticiadorWeb .= '

				public static function montarTituloPagina($strParametros = "") {

					//print "<br>strParametros:<pre>";
					//print_r($strParametros);
					//print "</pre>";

					$arrParametros = json_decode($strParametros, true);

					//print "<br>arrParametros:<pre>";
					//print_r($arrParametros);
					//print "</pre>";

					$strSeparador = (isset($arrParametros["strSeparador"])) ? $arrParametros["strSeparador"] : " - ";

					$titulo_noticia_noticiadorweb = "";

					if (isset($_REQUEST["noticia_id"])) {

						$arrDadosNoticia = array();

						$strUrlRequisitarDadosNoticia = "https://noticiadorweb.com.br/index.php?action=obterDadosNoticia&secao=api_noticias&noticia_id=" . $_REQUEST["noticia_id"];

						//echo $strUrlRequisitarDadosNoticia;

						$arrDadosNoticia = json_decode(file_get_contents($strUrlRequisitarDadosNoticia, FILE_TEXT), true);

						//print "<br>arrDadosNoticia:<pre>";
						//print_r($arrDadosNoticia);
						//print "</pre>";

						$arrDadosNoticia["noticia"] = $arrDadosNoticia["noticias"][0];

						$titulo_noticia_noticiadorweb = $arrDadosNoticia["noticia"]["titulo"];

						$titulo_noticia_noticiadorweb = ($titulo_noticia_noticiadorweb != "") ? $titulo_noticia_noticiadorweb . $strSeparador : "";

					}

					return $titulo_noticia_noticiadorweb;
				}

		';

		$strScriptNoticiasNoticiadorWeb .= '

				public static function montarScriptsHeader() {

					$strScriptsHeader = "";

					$strUrlRequisitarScriptBeforeAll = "https://noticiadorweb.com.br/index.php?action=ScriptBeforeAll&secao=api_noticias";

					$strScriptsHeader = utf8_encode(file_get_contents($strUrlRequisitarScriptBeforeAll, FILE_TEXT));

					if (isset($_REQUEST["noticia_id"])) {

						$strUrlRequisitarScriptBeforeShareBar = "https://noticiadorweb.com.br/index.php?action=ScriptBeforeShareBar&secao=api_noticias";

						$strScriptsHeader .= utf8_encode(file_get_contents($strUrlRequisitarScriptBeforeShareBar, FILE_TEXT));

						$strUrlRequisitarScriptBeforeMetaDados = "https://noticiadorweb.com.br/index.php?action=ScriptBeforeMetaDados&secao=api_noticias";

						$strScriptsHeader .= utf8_encode(file_get_contents($strUrlRequisitarScriptBeforeMetaDados, FILE_TEXT));

						$strUrlRequisitarScriptBeforeFormComentar = "https://noticiadorweb.com.br/index.php?action=ScriptBeforeFormComentar&secao=api_noticias";

						$strScriptsHeader .= utf8_encode(file_get_contents($strUrlRequisitarScriptBeforeFormComentar, FILE_TEXT));

					}

					return $strScriptsHeader;
				}

		';

		$strScriptNoticiasNoticiadorWeb .= '

				public static function montarScriptsFooter() {

					$strScriptsFooter = "";

					if (isset($_REQUEST["noticia_id"])) {

						$strUrlRequisitarScriptAfterShareBar = "https://noticiadorweb.com.br/index.php?action=ScriptAfterShareBar&secao=api_noticias";

						$strScriptsFooter .= utf8_encode(file_get_contents($strUrlRequisitarScriptAfterShareBar, FILE_TEXT));

						$strUrlRequisitarScriptAfterFormComentar = "https://noticiadorweb.com.br/index.php?action=ScriptAfterFormComentar&secao=api_noticias";

						$strScriptsFooter .= utf8_encode(file_get_contents($strUrlRequisitarScriptAfterFormComentar, FILE_TEXT));

					}

					$strUrlRequisitarScriptAfterAll = "https://noticiadorweb.com.br/index.php?action=ScriptAfterAll&secao=api_noticias";

					$strScriptsFooter .= utf8_encode(file_get_contents($strUrlRequisitarScriptAfterAll, FILE_TEXT));

					return $strScriptsFooter;
				}

		';

		$strScriptNoticiasNoticiadorWeb .= '

				public static function montarPagina($strParametros = "") {

					//print "<br>strParametros:<pre>";
					//print_r($strParametros);
					//print "</pre>";

					$arrParametros = json_decode($strParametros, true);

					//print "<br>arrParametros:<pre>";
					//print_r($arrParametros);
					//print "</pre>";

					$intIDGN = $arrParametros["intIDGN"];
					$intQtdNotListagem = (isset($arrParametros["intQtdNotListagem"])) ? $arrParametros["intQtdNotListagem"] : 0;
					$intQtdColListagem = (isset($arrParametros["intQtdColListagem"])) ? $arrParametros["intQtdColListagem"] : 2;
					$intQtdNotRel = (isset($arrParametros["intQtdNotRel"])) ? $arrParametros["intQtdNotRel"] : 4;
					$strDefaultThumbnail = (isset($arrParametros["strDefaultThumbnail"])) ? $arrParametros["strDefaultThumbnail"] : "";
					$strCharsetComent = (isset($arrParametros["strCharsetComent"])) ? $arrParametros["strCharsetComent"] : "";
					$booContarViews = (isset($arrParametros["booContarViews"])) ? $arrParametros["booContarViews"] : true;

					//print "<br>intIDGN:<pre>";
					//print_r($intIDGN);
					//print "</pre>";

					//print "<br>intQtdNotListagem:<pre>";
					//print_r($intQtdNotListagem);
					//print "</pre>";

					//print "<br>intQtdColListagem:<pre>";
					//print_r($intQtdColListagem);
					//print "</pre>";

					//print "<br>intQtdNotRel:<pre>";
					//print_r($intQtdNotRel);
					//print "</pre>";

					//print "<br>strDefaultThumbnail:<pre>";
					//print_r($strDefaultThumbnail);
					//print "</pre>";

					//print "<br>strCharsetComent:<pre>";
					//print_r($strCharsetComent);
					//print "</pre>";

					//print "<br>booContarViews:<pre>";
					//print_r($booContarViews);
					//print "</pre>";

					$strPagina = "";

					$strPagina .= "<!-- PluginAPINoticias --><div id=\'nweb_PluginAPINoticias\'><div id=\'nweb_noticias\'><div id=\'nweb_coluna\' class=\'nweb_coluna_esquerda\'>";

					$arrNoticiasNoticiadorWeb = @self :: obterNoticias($intIDGN, $intQtdNotListagem, $intQtdNotRel, $strDefaultThumbnail, $strCharsetComent, $booContarViews);

					if (isset($_REQUEST["noticia_id"])) {

						$strPagina .= "";

						if (isset($arrNoticiasNoticiadorWeb["noticia"])) {

							$strPagina .= "<div id=\'nweb_coluna_esquerda\'>";

							$strPagina .= "<h1>";
							$strPagina .= $arrNoticiasNoticiadorWeb["noticia"]["titulo"];
							$strPagina .= "</h1>";

							$strPagina .= utf8_encode($arrNoticiasNoticiadorWeb["noticia"]["ShareBar"]);

							$strPagina .= "<h3>";
							$strPagina .= $arrNoticiasNoticiadorWeb["noticia"]["resumo"];
							$strPagina .= "</h3>";

							$strPagina .= "<div id=\"newb_texto2\">";
							$strPagina .= $arrNoticiasNoticiadorWeb["noticia"]["texto"];
							$strPagina .= "</div>";

							$strPagina .= utf8_encode($arrNoticiasNoticiadorWeb["noticia"]["MetaDados"]);

							$strPagina .= "<div id=\'nweb_comentarios\'>";

							$comentarios = $arrNoticiasNoticiadorWeb["noticia"]["comentarios"];

							if (count($comentarios) > 0) {

								for ($i = 0; $i < count($comentarios); $i++) {

									$comentario = $comentarios[$i];
									$strPagina .= "<div class=\'nweb_coment\'>";
									$strPagina .= "<p>";
									$strPagina .= "<strong>";
									$strPagina .= $comentario["nome"];
									$strPagina .= "</strong>";
									$strPagina .= " disse:";
									$strPagina .= "</p>";
									$strPagina .= "<span>";
									$strPagina .= $comentario["comentario"];
									$strPagina .= "</span>";
									$strPagina .= "</div>";
								}

							} else {

								$strPagina .= "<div class=\'nweb_coment\'>";
								$strPagina .= "<p>";
								$strPagina .= "' . utf8_encode('Esta notícia ainda não tem comentários.') . '";
								$strPagina .= "</p>";
								$strPagina .= "</div>";

							}

							$strPagina .= utf8_encode($arrNoticiasNoticiadorWeb["noticia"]["FormComentar"]);

							$strPagina .= "</div>";

							ob_start();

							eval($arrNoticiasNoticiadorWeb["noticia"]["strScriptShareComment"]);

							$strPagina .= ob_get_contents();

							ob_end_clean();

							$strPagina .= "</div>";

							$strPagina .= "</div>";

							$strPagina .= "<div id=\'nweb_coluna\'>";

							$strPagina .= "<div id=\'nweb_coluna_direita\'>";

							$strPagina .= "<h2 class=\'nweb_titulo_direita\'>' . utf8_encode('Outras Notícias') . '</h2>";

							$strPagina .= "<ul class=\'nweb_lista_noticias_listagem\'>";

							$conta_linhas = 1;
							$conta_colunas = 0;
							$abre_linha = false;

							for ($intIndiceNoticias = 0; $intIndiceNoticias < count($arrNoticiasNoticiadorWeb["noticias"]); $intIndiceNoticias++) {

								$strImgThumbnail = "<img src=\'" . $arrNoticiasNoticiadorWeb["noticias"][$intIndiceNoticias]["thumbnail"] . "&w=50&h=50\' />";

								if ($conta_colunas == 0) {
									$strPagina .= "<div id=\'nweb_linha-" . $conta_linhas . "-outras\' class=\'nweb_linhaListagemOutras\'>";
									$conta_linhas++;
									$abre_linha = true;
								}

								$strPagina .= "<li>";
								$strPagina .= "<div class=\'nweb_chamadaNoticiaNoticadorWeb\'>";
								$strPagina .= "<div class=\'nweb_colunaElementoNoticia\'>";
								$strPagina .= $strImgThumbnail;
								$strPagina .= "</div>";
								$strPagina .= "<div class=\'nweb_colunaElementoNoticia nweb_colunaDireita\'>";
								$strPagina .= "<h3>";
								$strPagina .= "<a href=\'";
								$strPagina .= $arrNoticiasNoticiadorWeb["noticias"][$intIndiceNoticias]["urlNoticia"];
								$strPagina .= "\'>";
								$strPagina .= $arrNoticiasNoticiadorWeb["noticias"][$intIndiceNoticias]["titulo"];
								$strPagina .= "</a>";
								$strPagina .= "</h3>";
								$strPagina .= "</div>";
								$strPagina .= "</div>";
								$strPagina .= "</li>";

								if ($conta_colunas == $intQtdColListagem - 1) {
									$strPagina .= "</div>";
									$conta_colunas = -1;
									$abre_linha = false;
								}

								$conta_colunas++;
							}

							if ($abre_linha) {
								$strPagina .= "</div>";
							}

							$strPagina .= "</ul>";

							$strPagina .= "<div id=\'nweb_botaoExibirMais\' class=\'nweb_botaoExibirMais\'>";

							$strPagina .= "<a class=\'nweb_botaoExibirMais\' href=\'";
							$strPagina .= $arrNoticiasNoticiadorWeb["baseUrlOrigem"]; 
							$strPagina .= "\'>' . utf8_encode('Exibir todas as notícias') . '</a>";

							$strPagina .= "</div>";

						}

					} else {

						$strPagina .= "<div id=\'nweb_coluna_unica\'>";

						$strPagina .= "<ul class=\'nweb_lista_noticias_listagem\'>";

						$conta_linhas = 1;
						$conta_colunas = 0;
						$fecha_linha = false;

						for ($intIndiceNoticias = 0; $intIndiceNoticias < count($arrNoticiasNoticiadorWeb["noticias"]); $intIndiceNoticias++) {

							$strImgThumbnail = "<img src=\'" . $arrNoticiasNoticiadorWeb["noticias"][$intIndiceNoticias]["thumbnail"] . "&w=50&h=50\' />";

							if ($conta_colunas == 0) {
								$strPagina .= "<div id=\'nweb_linha-" . $conta_linhas . "\' class=\'nweb_linhaListagem\'>";
								$conta_linhas++;
								$fecha_linha = true;
							}

							$strPagina .= "<li>";
							$strPagina .= "<div class=\'nweb_chamadaNoticiaNoticadorWeb\'>";
							$strPagina .= "<div class=\'nweb_colunaElementoNoticia\'>";
							$strPagina .= $strImgThumbnail;
							$strPagina .= "</div>";
							$strPagina .= "<div class=\"nweb_colunaElementoNoticia nweb_colunaDireita\">";
							$strPagina .= "<h3>";
							$strPagina .= "<a href=\'";
							$strPagina .= $arrNoticiasNoticiadorWeb["noticias"][$intIndiceNoticias]["urlNoticia"];
							$strPagina .= "\'>";
							$strPagina .= $arrNoticiasNoticiadorWeb["noticias"][$intIndiceNoticias]["titulo"];
							$strPagina .= "</a>";
							$strPagina .= "</h3>";
							$strPagina .= "</div>";
							$strPagina .= "</div>";
							$strPagina .= "</li>";

							if ($conta_colunas == $intQtdColListagem - 1) {
								$strPagina .= "</div>";
								$conta_colunas = -1;
								$fecha_linha = false;
							}

							$conta_colunas++;
						}

						if ($fecha_linha) {
							$strPagina .= "</div>";
						}

						$strPagina .= "</ul>";

					}

					$strPagina .= "</div></div></div></div><!-- PluginAPINoticias -->";

                    $strPagina .= "
                    <script>
                        let img = document.getElementById(\'newb_texto2\').getElementsByTagName(\'img\');
                        for (let i = 0; i < img.length; i++) {
                            img[i].src = img[i].src.replace(\'www.\', \'\').replace(\'http://noticiadorweb.com.br\', \'\')
                            .replace(\'https//\', \'https://\');
                        }
                    </script>";
                    
                    return $strPagina;
               }

		';

		$strScriptNoticiasNoticiadorWeb .= '

			}

		';

		return $strScriptNoticiasNoticiadorWeb;
	}

}



?>